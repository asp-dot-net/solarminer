namespace EurosolarReporting
{
    partial class SolarMinerMaintenance
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SolarMinerMaintenance));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.table8 = new Telerik.Reporting.Table();
            this.textBox127 = new Telerik.Reporting.TextBox();
            this.textBox126 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.txtInstallBookingDate = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.txtMtceCost = new Telerik.Reporting.TextBox();
            this.txtMtceDiscount = new Telerik.Reporting.TextBox();
            this.txtMtceBalance = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox92 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.64975845813751221D), Telerik.Reporting.Drawing.Unit.Cm(0.66145849227905273D));
            this.textBox15.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox15.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox15.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox15.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox15.Style.Color = System.Drawing.Color.SteelBlue;
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Name = "Calibri";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox15.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.Value = "I\t";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0203123092651367D), Telerik.Reporting.Drawing.Unit.Cm(0.66145855188369751D));
            this.textBox42.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Dotted;
            this.textBox42.Style.Color = System.Drawing.Color.Black;
            this.textBox42.Style.Font.Bold = true;
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.397844314575195D), Telerik.Reporting.Drawing.Unit.Cm(0.66145855188369751D));
            this.textBox56.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox56.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox56.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox56.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox56.Style.Color = System.Drawing.Color.SteelBlue;
            this.textBox56.Style.Font.Bold = true;
            this.textBox56.Style.Font.Name = "Calibri";
            this.textBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox56.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox56.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox56.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox56.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox56.Value = "confirm that the details above are correct and the Solar Miner Assistant has reso" +
    "lved and serviced";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(297D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.textBox23,
            this.textBox25,
            this.textBox24,
            this.textBox7,
            this.textBox17,
            this.textBox30,
            this.textBox8,
            this.table8,
            this.textBox32,
            this.textBox43,
            this.textBox44,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50,
            this.table6,
            this.table3,
            this.textBox10,
            this.textBox20,
            this.textBox51,
            this.textBox16,
            this.textBox52,
            this.textBox12,
            this.textBox13,
            this.textBox54,
            this.textBox53,
            this.textBox11,
            this.table1,
            this.table2,
            this.table4,
            this.textBox55,
            this.textBox1,
            this.textBox2});
            this.detail.Name = "detail";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Style.Color = System.Drawing.Color.Black;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(3.2114584445953369D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3997995853424072D), Telerik.Reporting.Drawing.Unit.Cm(1.0997995138168335D));
            this.textBox23.Style.BorderColor.Bottom = System.Drawing.Color.Transparent;
            this.textBox23.Style.BorderColor.Default = System.Drawing.Color.LightBlue;
            this.textBox23.Style.BorderColor.Left = System.Drawing.Color.Transparent;
            this.textBox23.Style.BorderColor.Top = System.Drawing.Color.Transparent;
            this.textBox23.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox23.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox23.Style.Color = System.Drawing.Color.White;
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Name = "Calibri";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox23.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.Value = "   PROJECT NO       :";
            // 
            // textBox25
            // 
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.5D), Telerik.Reporting.Drawing.Unit.Cm(4.31165885925293D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox25.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox25.Style.BorderColor.Top = System.Drawing.Color.LightBlue;
            this.textBox25.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox25.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox25.Style.Color = System.Drawing.Color.White;
            this.textBox25.Style.Font.Bold = true;
            this.textBox25.Style.Font.Name = "Calibri";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.Value = "";
            // 
            // textBox24
            // 
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.5D), Telerik.Reporting.Drawing.Unit.Cm(3.2114584445953369D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(1.0997990369796753D));
            this.textBox24.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox24.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox24.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox24.Style.Color = System.Drawing.Color.White;
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.Font.Name = "Calibri";
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.Value = "";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(5.3114590644836426D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3997995853424072D), Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D));
            this.textBox7.Style.BorderColor.Bottom = System.Drawing.Color.Transparent;
            this.textBox7.Style.BorderColor.Default = System.Drawing.Color.LightBlue;
            this.textBox7.Style.BorderColor.Left = System.Drawing.Color.Transparent;
            this.textBox7.Style.BorderColor.Top = System.Drawing.Color.LightBlue;
            this.textBox7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox7.Style.Color = System.Drawing.Color.White;
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Calibri";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.Value = "   CALL DATE         :";
            // 
            // textBox17
            // 
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(4.3114585876464844D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3997995853424072D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.textBox17.Style.BorderColor.Bottom = System.Drawing.Color.Transparent;
            this.textBox17.Style.BorderColor.Default = System.Drawing.Color.LightBlue;
            this.textBox17.Style.BorderColor.Left = System.Drawing.Color.Transparent;
            this.textBox17.Style.BorderColor.Top = System.Drawing.Color.LightBlue;
            this.textBox17.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox17.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox17.Style.Color = System.Drawing.Color.White;
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.Font.Name = "Calibri";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox17.Value = "   MANUAL NO     :";
            // 
            // textBox30
            // 
            this.textBox30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.5D), Telerik.Reporting.Drawing.Unit.Cm(5.311859130859375D));
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D));
            this.textBox30.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.textBox30.Style.BorderColor.Top = System.Drawing.Color.LightBlue;
            this.textBox30.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox30.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox30.Style.Color = System.Drawing.Color.White;
            this.textBox30.Style.Font.Bold = true;
            this.textBox30.Style.Font.Name = "Calibri";
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox30.Value = "";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D), Telerik.Reporting.Drawing.Unit.Cm(8.4614582061767578D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.4000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269D));
            this.textBox8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox8.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox8.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox8.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox8.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox8.Style.Color = System.Drawing.Color.Black;
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Calibri";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.Value = "SYSTEM INSTALLED";
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9844646453857422D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.758681058883667D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.1085233688354492D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.2488846778869629D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.88269186019897461D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.8553013801574707D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.3352205753326416D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.3317488431930542D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.38084232807159424D)));
            this.table8.Body.SetCellContent(2, 0, this.textBox127);
            this.table8.Body.SetCellContent(3, 0, this.textBox126);
            this.table8.Body.SetCellContent(2, 3, this.textBox78);
            this.table8.Body.SetCellContent(3, 3, this.txtInstallBookingDate);
            this.table8.Body.SetCellContent(2, 2, this.textBox14);
            this.table8.Body.SetCellContent(3, 2, this.textBox18);
            this.table8.Body.SetCellContent(2, 1, this.textBox19);
            this.table8.Body.SetCellContent(3, 1, this.textBox21);
            this.table8.Body.SetCellContent(4, 0, this.textBox22);
            this.table8.Body.SetCellContent(1, 0, this.textBox29);
            this.table8.Body.SetCellContent(0, 0, this.textBox45);
            this.table8.Body.SetCellContent(1, 1, this.textBox31, 1, 3);
            this.table8.Body.SetCellContent(0, 1, this.textBox46, 1, 3);
            this.table8.Body.SetCellContent(4, 1, this.textBox26, 1, 3);
            tableGroup1.Name = "tableGroup13";
            tableGroup2.Name = "group5";
            tableGroup3.Name = "group4";
            tableGroup4.Name = "tableGroup14";
            this.table8.ColumnGroups.Add(tableGroup1);
            this.table8.ColumnGroups.Add(tableGroup2);
            this.table8.ColumnGroups.Add(tableGroup3);
            this.table8.ColumnGroups.Add(tableGroup4);
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox45,
            this.textBox46,
            this.textBox29,
            this.textBox31,
            this.textBox127,
            this.textBox19,
            this.textBox14,
            this.textBox78,
            this.textBox126,
            this.textBox21,
            this.textBox18,
            this.txtInstallBookingDate,
            this.textBox22,
            this.textBox26});
            this.table8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.57999998331069946D), Telerik.Reporting.Drawing.Unit.Cm(9.1499996185302734D));
            this.table8.Name = "table8";
            tableGroup6.Name = "group18";
            tableGroup7.Name = "group17";
            tableGroup8.Name = "group28";
            tableGroup9.Name = "group29";
            tableGroup10.Name = "group6";
            tableGroup5.ChildGroups.Add(tableGroup6);
            tableGroup5.ChildGroups.Add(tableGroup7);
            tableGroup5.ChildGroups.Add(tableGroup8);
            tableGroup5.ChildGroups.Add(tableGroup9);
            tableGroup5.ChildGroups.Add(tableGroup10);
            tableGroup5.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup5.Name = "detailTableGroup7";
            this.table8.RowGroups.Add(tableGroup5);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.100553512573242D), Telerik.Reporting.Drawing.Unit.Cm(4.3994350433349609D));
            this.table8.Style.Font.Name = "Calibri";
            this.table8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            // 
            // textBox127
            // 
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9844648838043213D), Telerik.Reporting.Drawing.Unit.Inch(0.33522063493728638D));
            this.textBox127.Style.Color = System.Drawing.Color.White;
            this.textBox127.Style.Font.Bold = true;
            this.textBox127.Style.Font.Name = "Calibri";
            this.textBox127.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox127.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox127.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox127.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox127.Value = "Roof Type";
            // 
            // textBox126
            // 
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9844648838043213D), Telerik.Reporting.Drawing.Unit.Inch(0.33174887299537659D));
            this.textBox126.Style.Color = System.Drawing.Color.White;
            this.textBox126.Style.Font.Bold = true;
            this.textBox126.Style.Font.Name = "Calibri";
            this.textBox126.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox126.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox126.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox126.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox126.StyleName = "";
            this.textBox126.Value = "House Type";
            // 
            // textBox78
            // 
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2488851547241211D), Telerik.Reporting.Drawing.Unit.Inch(0.3352205753326416D));
            this.textBox78.Style.Color = System.Drawing.Color.White;
            this.textBox78.Style.Font.Bold = false;
            this.textBox78.Style.Font.Italic = false;
            this.textBox78.Style.Font.Name = "Calibri";
            this.textBox78.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox78.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox78.Value = "=Fields.RoofAngle";
            // 
            // txtInstallBookingDate
            // 
            this.txtInstallBookingDate.Name = "txtInstallBookingDate";
            this.txtInstallBookingDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2488851547241211D), Telerik.Reporting.Drawing.Unit.Inch(0.3317488431930542D));
            this.txtInstallBookingDate.Style.Color = System.Drawing.Color.White;
            this.txtInstallBookingDate.Style.Font.Bold = false;
            this.txtInstallBookingDate.Style.Font.Italic = false;
            this.txtInstallBookingDate.Style.Font.Name = "Calibri";
            this.txtInstallBookingDate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.txtInstallBookingDate.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.txtInstallBookingDate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtInstallBookingDate.Value = "=Fields.InstallBookingDate";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1085233688354492D), Telerik.Reporting.Drawing.Unit.Inch(0.3352205753326416D));
            this.textBox14.Style.Color = System.Drawing.Color.White;
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Calibri";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.StyleName = "";
            this.textBox14.Value = "Roof Type";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1085233688354492D), Telerik.Reporting.Drawing.Unit.Inch(0.3317488431930542D));
            this.textBox18.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox18.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox18.Style.Color = System.Drawing.Color.White;
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.Font.Name = "Calibri";
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox18.StyleName = "";
            this.textBox18.Value = "House Type";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7586801052093506D), Telerik.Reporting.Drawing.Unit.Inch(0.3352205753326416D));
            this.textBox19.Style.Color = System.Drawing.Color.White;
            this.textBox19.Style.Font.Bold = false;
            this.textBox19.Style.Font.Italic = false;
            this.textBox19.Style.Font.Name = "Calibri";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.StyleName = "";
            this.textBox19.Value = "=Fields.RoofType";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7586801052093506D), Telerik.Reporting.Drawing.Unit.Inch(0.3317488431930542D));
            this.textBox21.Style.Color = System.Drawing.Color.White;
            this.textBox21.Style.Font.Bold = false;
            this.textBox21.Style.Font.Italic = false;
            this.textBox21.Style.Font.Name = "Calibri";
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.StyleName = "";
            this.textBox21.Value = "=Fields.HouseType";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9844651222229004D), Telerik.Reporting.Drawing.Unit.Inch(0.38084247708320618D));
            this.textBox22.Style.Color = System.Drawing.Color.White;
            this.textBox22.Style.Font.Bold = true;
            this.textBox22.Style.Font.Name = "Calibri";
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox22.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox22.StyleName = "";
            this.textBox22.Value = "Assigned To";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9844648838043213D), Telerik.Reporting.Drawing.Unit.Cm(0.85530132055282593D));
            this.textBox29.Style.Color = System.Drawing.Color.White;
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Name = "Calibri";
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.StyleName = "";
            this.textBox29.Value = "Inverter";
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9844651222229004D), Telerik.Reporting.Drawing.Unit.Cm(0.88269203901290894D));
            this.textBox45.Style.Color = System.Drawing.Color.White;
            this.textBox45.Style.Font.Bold = true;
            this.textBox45.Style.Font.Name = "Calibri";
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox45.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox45.StyleName = "";
            this.textBox45.Value = "Panels";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.1160888671875D), Telerik.Reporting.Drawing.Unit.Cm(0.8553013801574707D));
            this.textBox31.Style.Color = System.Drawing.Color.White;
            this.textBox31.Style.Font.Bold = false;
            this.textBox31.Style.Font.Italic = false;
            this.textBox31.Style.Font.Name = "Calibri";
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox31.StyleName = "";
            this.textBox31.Value = "=fields.InverterDetailsName";
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.1160888671875D), Telerik.Reporting.Drawing.Unit.Cm(0.88269186019897461D));
            this.textBox46.Style.Color = System.Drawing.Color.White;
            this.textBox46.Style.Font.Bold = false;
            this.textBox46.Style.Font.Italic = false;
            this.textBox46.Style.Font.Name = "Calibri";
            this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox46.StyleName = "";
            this.textBox46.Value = "=fields.PanelBrandName";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.1160888671875D), Telerik.Reporting.Drawing.Unit.Inch(0.38084232807159424D));
            this.textBox26.Style.Color = System.Drawing.Color.White;
            this.textBox26.Style.Font.Bold = false;
            this.textBox26.Style.Font.Italic = false;
            this.textBox26.Style.Font.Name = "Calibri";
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.StyleName = "";
            this.textBox26.Value = "=Fields.installername";
            // 
            // textBox32
            // 
            this.textBox32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.311457633972168D), Telerik.Reporting.Drawing.Unit.Cm(6.7498908042907715D));
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5000004768371582D), Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269D));
            this.textBox32.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox32.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox32.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox32.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox32.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox32.Style.Color = System.Drawing.Color.Black;
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.Font.Name = "Calibri";
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox32.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox32.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox32.Value = "SITE DIAGRAM";
            // 
            // textBox43
            // 
            this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.100000381469727D), Telerik.Reporting.Drawing.Unit.Cm(14.461458206176758D));
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4000003337860107D), Telerik.Reporting.Drawing.Unit.Cm(0.85000002384185791D));
            this.textBox43.Style.Color = System.Drawing.Color.SteelBlue;
            this.textBox43.Style.Font.Bold = true;
            this.textBox43.Style.Font.Name = "Calibri";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox43.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox43.Value = "North Side";
            // 
            // textBox44
            // 
            this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.100000381469727D), Telerik.Reporting.Drawing.Unit.Cm(15.31165885925293D));
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4000003337860107D), Telerik.Reporting.Drawing.Unit.Cm(0.89979970455169678D));
            this.textBox44.Style.Color = System.Drawing.Color.SteelBlue;
            this.textBox44.Style.Font.Bold = true;
            this.textBox44.Style.Font.Name = "Calibri";
            this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox44.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox44.Value = "West Side";
            // 
            // textBox47
            // 
            this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.100000381469727D), Telerik.Reporting.Drawing.Unit.Cm(16.211658477783203D));
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4000003337860107D), Telerik.Reporting.Drawing.Unit.Cm(0.76438379287719727D));
            this.textBox47.Style.Color = System.Drawing.Color.SteelBlue;
            this.textBox47.Style.Font.Bold = true;
            this.textBox47.Style.Font.Name = "Calibri";
            this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox47.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox47.Value = "Other";
            // 
            // textBox48
            // 
            this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.69999885559082D), Telerik.Reporting.Drawing.Unit.Cm(14.461458206176758D));
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(0.84999978542327881D));
            this.textBox48.Style.Color = System.Drawing.Color.SteelBlue;
            this.textBox48.Style.Font.Bold = true;
            this.textBox48.Style.Font.Name = "Calibri";
            this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox48.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox48.Value = "Volts";
            // 
            // textBox49
            // 
            this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.69999885559082D), Telerik.Reporting.Drawing.Unit.Cm(15.31165885925293D));
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(0.89979970455169678D));
            this.textBox49.Style.Color = System.Drawing.Color.SteelBlue;
            this.textBox49.Style.Font.Bold = true;
            this.textBox49.Style.Font.Name = "Calibri";
            this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox49.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox49.Value = "Volts";
            // 
            // textBox50
            // 
            this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.69999885559082D), Telerik.Reporting.Drawing.Unit.Cm(16.211658477783203D));
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(0.76438373327255249D));
            this.textBox50.Style.Color = System.Drawing.Color.SteelBlue;
            this.textBox50.Style.Font.Bold = true;
            this.textBox50.Style.Font.Name = "Calibri";
            this.textBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox50.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(6D);
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox50.Value = "Volts";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.1779158115386963D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.5996882915496826D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.5996882915496826D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.5335423946380615D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.181769847869873D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.34447431564331055D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.3149605393409729D)));
            this.table6.Body.SetCellContent(0, 1, this.textBox33);
            this.table6.Body.SetCellContent(0, 4, this.textBox34);
            this.table6.Body.SetCellContent(0, 2, this.textBox35);
            this.table6.Body.SetCellContent(0, 0, this.textBox36);
            this.table6.Body.SetCellContent(0, 3, this.textBox37);
            this.table6.Body.SetCellContent(1, 0, this.txtMtceCost);
            this.table6.Body.SetCellContent(1, 1, this.txtMtceDiscount);
            this.table6.Body.SetCellContent(1, 2, this.txtMtceBalance);
            this.table6.Body.SetCellContent(1, 3, this.textBox38);
            this.table6.Body.SetCellContent(1, 4, this.textBox39);
            tableGroup11.Name = "group1";
            tableGroup12.Name = "tableGroup1";
            tableGroup13.Name = "group";
            tableGroup14.Name = "group3";
            tableGroup15.Name = "tableGroup2";
            this.table6.ColumnGroups.Add(tableGroup11);
            this.table6.ColumnGroups.Add(tableGroup12);
            this.table6.ColumnGroups.Add(tableGroup13);
            this.table6.ColumnGroups.Add(tableGroup14);
            this.table6.ColumnGroups.Add(tableGroup15);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox36,
            this.textBox33,
            this.textBox35,
            this.textBox37,
            this.textBox34,
            this.txtMtceCost,
            this.txtMtceDiscount,
            this.txtMtceBalance,
            this.textBox38,
            this.textBox39});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5692707896232605D), Telerik.Reporting.Drawing.Unit.Inch(5.929999828338623D));
            this.table6.Name = "table6";
            tableGroup17.Name = "group2";
            tableGroup18.Name = "group7";
            tableGroup16.ChildGroups.Add(tableGroup17);
            tableGroup16.ChildGroups.Add(tableGroup18);
            tableGroup16.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup16.Name = "detailTableGroup";
            this.table6.RowGroups.Add(tableGroup16);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.092604637145996D), Telerik.Reporting.Drawing.Unit.Inch(0.65943485498428345D));
            this.table6.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.table6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.table6.Style.Color = System.Drawing.Color.White;
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5996882915496826D), Telerik.Reporting.Drawing.Unit.Inch(0.34447431564331055D));
            this.textBox33.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox33.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox33.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox33.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox33.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox33.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox33.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox33.Style.Color = System.Drawing.Color.White;
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.Font.Name = "Calibri";
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox33.Value = "Discount";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.181769847869873D), Telerik.Reporting.Drawing.Unit.Inch(0.34447431564331055D));
            this.textBox34.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox34.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox34.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox34.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox34.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox34.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox34.Style.Color = System.Drawing.Color.White;
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.Font.Name = "Calibri";
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox34.Value = "Received By";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5996882915496826D), Telerik.Reporting.Drawing.Unit.Inch(0.34447431564331055D));
            this.textBox35.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox35.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox35.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox35.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox35.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox35.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox35.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox35.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox35.Style.Color = System.Drawing.Color.White;
            this.textBox35.Style.Font.Bold = true;
            this.textBox35.Style.Font.Name = "Calibri";
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox35.StyleName = "";
            this.textBox35.Value = "Balance";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1779158115386963D), Telerik.Reporting.Drawing.Unit.Inch(0.34447431564331055D));
            this.textBox36.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox36.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox36.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox36.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox36.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox36.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox36.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox36.Style.Color = System.Drawing.Color.White;
            this.textBox36.Style.Font.Bold = true;
            this.textBox36.Style.Font.Name = "Calibri";
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox36.Value = "Amount";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5335426330566406D), Telerik.Reporting.Drawing.Unit.Inch(0.34447431564331055D));
            this.textBox37.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox37.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox37.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox37.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox37.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox37.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox37.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox37.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox37.Style.Font.Bold = true;
            this.textBox37.Style.Font.Name = "Calibri";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.StyleName = "";
            this.textBox37.Value = "Paid By";
            // 
            // txtMtceCost
            // 
            this.txtMtceCost.Name = "txtMtceCost";
            this.txtMtceCost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1779146194458008D), Telerik.Reporting.Drawing.Unit.Inch(0.31496050953865051D));
            this.txtMtceCost.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtMtceCost.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.txtMtceCost.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.txtMtceCost.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtMtceCost.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtMtceCost.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtMtceCost.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtMtceCost.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtMtceCost.Style.Font.Bold = false;
            this.txtMtceCost.Style.Font.Name = "Calibri";
            this.txtMtceCost.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.txtMtceCost.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtMtceCost.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtMtceCost.StyleName = "";
            this.txtMtceCost.Value = "=Fields.MtceCost";
            // 
            // txtMtceDiscount
            // 
            this.txtMtceDiscount.Name = "txtMtceDiscount";
            this.txtMtceDiscount.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5996873378753662D), Telerik.Reporting.Drawing.Unit.Inch(0.31496050953865051D));
            this.txtMtceDiscount.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtMtceDiscount.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.txtMtceDiscount.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.txtMtceDiscount.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtMtceDiscount.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtMtceDiscount.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtMtceDiscount.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtMtceDiscount.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtMtceDiscount.Style.Font.Bold = false;
            this.txtMtceDiscount.Style.Font.Name = "Calibri";
            this.txtMtceDiscount.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.txtMtceDiscount.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtMtceDiscount.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtMtceDiscount.StyleName = "";
            this.txtMtceDiscount.Value = "";
            // 
            // txtMtceBalance
            // 
            this.txtMtceBalance.Name = "txtMtceBalance";
            this.txtMtceBalance.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5996873378753662D), Telerik.Reporting.Drawing.Unit.Inch(0.31496050953865051D));
            this.txtMtceBalance.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.txtMtceBalance.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.txtMtceBalance.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.txtMtceBalance.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtMtceBalance.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtMtceBalance.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtMtceBalance.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtMtceBalance.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtMtceBalance.Style.Font.Bold = false;
            this.txtMtceBalance.Style.Font.Name = "Calibri";
            this.txtMtceBalance.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.txtMtceBalance.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.txtMtceBalance.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtMtceBalance.StyleName = "";
            this.txtMtceBalance.Value = "";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5335426330566406D), Telerik.Reporting.Drawing.Unit.Inch(0.31496050953865051D));
            this.textBox38.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox38.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox38.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox38.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox38.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox38.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox38.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox38.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox38.Style.Font.Bold = false;
            this.textBox38.Style.Font.Name = "Calibri";
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.StyleName = "";
            this.textBox38.Value = "=Fields.FPTransType";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1817705631256104D), Telerik.Reporting.Drawing.Unit.Inch(0.3149605393409729D));
            this.textBox39.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox39.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox39.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox39.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox39.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox39.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox39.Style.Font.Bold = false;
            this.textBox39.Style.Font.Name = "Calibri";
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.StyleName = "";
            this.textBox39.Value = "=Fields.MtceRecByName";
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9909086227416992D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.168006420135498D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9348628520965576D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9965956211090088D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.35322269797325134D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.8482048511505127D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.86023050546646118D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.33238941431045532D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.36016735434532166D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox84);
            this.table3.Body.SetCellContent(4, 0, this.textBox83);
            this.table3.Body.SetCellContent(3, 0, this.textBox92);
            this.table3.Body.SetCellContent(1, 0, this.textBox3);
            this.table3.Body.SetCellContent(2, 0, this.textBox4);
            this.table3.Body.SetCellContent(2, 1, this.textBox40);
            this.table3.Body.SetCellContent(2, 2, this.textBox5);
            this.table3.Body.SetCellContent(2, 3, this.textBox6);
            this.table3.Body.SetCellContent(0, 1, this.textBox86, 1, 3);
            this.table3.Body.SetCellContent(1, 1, this.textBox9, 1, 3);
            this.table3.Body.SetCellContent(4, 1, this.textBox85, 1, 3);
            this.table3.Body.SetCellContent(3, 1, this.textBox93, 1, 3);
            tableGroup19.Name = "tableGroup4";
            tableGroup20.Name = "tableGroup5";
            tableGroup21.Name = "group8";
            tableGroup22.Name = "group11";
            this.table3.ColumnGroups.Add(tableGroup19);
            this.table3.ColumnGroups.Add(tableGroup20);
            this.table3.ColumnGroups.Add(tableGroup21);
            this.table3.ColumnGroups.Add(tableGroup22);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox84,
            this.textBox86,
            this.textBox3,
            this.textBox9,
            this.textBox4,
            this.textBox40,
            this.textBox5,
            this.textBox6,
            this.textBox92,
            this.textBox93,
            this.textBox83,
            this.textBox85});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.60000002384185791D), Telerik.Reporting.Drawing.Unit.Cm(3.2114584445953369D));
            this.table3.Name = "table3";
            tableGroup24.Name = "group9";
            tableGroup25.Name = "group15";
            tableGroup26.Name = "group16";
            tableGroup27.Name = "group12";
            tableGroup28.Name = "group10";
            tableGroup23.ChildGroups.Add(tableGroup24);
            tableGroup23.ChildGroups.Add(tableGroup25);
            tableGroup23.ChildGroups.Add(tableGroup26);
            tableGroup23.ChildGroups.Add(tableGroup27);
            tableGroup23.ChildGroups.Add(tableGroup28);
            tableGroup23.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup23.Name = "detailTableGroup2";
            this.table3.RowGroups.Add(tableGroup23);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.090373992919922D), Telerik.Reporting.Drawing.Unit.Cm(4.3647150993347168D));
            this.table3.Style.BackgroundImage.Repeat = Telerik.Reporting.Drawing.BackgroundRepeat.NoRepeat;
            this.table3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            // 
            // textBox84
            // 
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9909090995788574D), Telerik.Reporting.Drawing.Unit.Inch(0.35322281718254089D));
            this.textBox84.Style.Color = System.Drawing.Color.White;
            this.textBox84.Style.Font.Bold = true;
            this.textBox84.Style.Font.Name = "Calibri";
            this.textBox84.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox84.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox84.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox84.Value = "Name";
            // 
            // textBox83
            // 
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.99090838432312D), Telerik.Reporting.Drawing.Unit.Inch(0.36016732454299927D));
            this.textBox83.Style.Color = System.Drawing.Color.White;
            this.textBox83.Style.Font.Bold = true;
            this.textBox83.Style.Font.Name = "Calibri";
            this.textBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox83.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox83.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox83.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox83.StyleName = "";
            this.textBox83.Value = "Email";
            // 
            // textBox92
            // 
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.99090838432312D), Telerik.Reporting.Drawing.Unit.Inch(0.33238944411277771D));
            this.textBox92.Style.Color = System.Drawing.Color.White;
            this.textBox92.Style.Font.Bold = true;
            this.textBox92.Style.Font.Name = "Calibri";
            this.textBox92.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox92.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox92.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox92.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox92.StyleName = "";
            this.textBox92.Value = "Contact";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9909090995788574D), Telerik.Reporting.Drawing.Unit.Cm(0.84820479154586792D));
            this.textBox3.Style.Color = System.Drawing.Color.White;
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Calibri";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.StyleName = "";
            this.textBox3.Value = "Address";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9909086227416992D), Telerik.Reporting.Drawing.Unit.Cm(0.86023050546646118D));
            this.textBox4.Style.Color = System.Drawing.Color.White;
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Calibri";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.StyleName = "";
            this.textBox4.Value = "Suburb";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.16800594329834D), Telerik.Reporting.Drawing.Unit.Cm(0.86023050546646118D));
            this.textBox40.Style.Color = System.Drawing.Color.White;
            this.textBox40.Style.Font.Name = "Calibri";
            this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox40.StyleName = "";
            this.textBox40.Value = "=Fields.InstallCity";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9348640441894531D), Telerik.Reporting.Drawing.Unit.Cm(0.86023050546646118D));
            this.textBox5.Style.Color = System.Drawing.Color.White;
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.StyleName = "";
            this.textBox5.Value = "=Fields.InstallState";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.996596097946167D), Telerik.Reporting.Drawing.Unit.Cm(0.86023050546646118D));
            this.textBox6.Style.Color = System.Drawing.Color.White;
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.StyleName = "";
            this.textBox6.Value = "=Fields.InstallPostCode";
            // 
            // textBox86
            // 
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.099465370178223D), Telerik.Reporting.Drawing.Unit.Inch(0.35322281718254089D));
            this.textBox86.Style.Color = System.Drawing.Color.White;
            this.textBox86.Style.Font.Name = "Calibri";
            this.textBox86.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox86.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox86.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox86.Value = "=Fields.Contact";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.099465370178223D), Telerik.Reporting.Drawing.Unit.Cm(0.84820479154586792D));
            this.textBox9.Style.Color = System.Drawing.Color.White;
            this.textBox9.Style.Font.Name = "Calibri";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.StyleName = "";
            this.textBox9.Value = "= TrimStart(Fields.InstallAddress)";
            // 
            // textBox85
            // 
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.099465370178223D), Telerik.Reporting.Drawing.Unit.Inch(0.36016732454299927D));
            this.textBox85.Style.Color = System.Drawing.Color.White;
            this.textBox85.Style.Font.Name = "Calibri";
            this.textBox85.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox85.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox85.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox85.StyleName = "";
            this.textBox85.Value = "=Fields.ContEmail";
            // 
            // textBox93
            // 
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.099465370178223D), Telerik.Reporting.Drawing.Unit.Inch(0.33238944411277771D));
            this.textBox93.Style.Color = System.Drawing.Color.White;
            this.textBox93.Style.Font.Name = "Calibri";
            this.textBox93.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox93.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox93.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox93.StyleName = "";
            this.textBox93.Value = "=Fields.CustPhone";
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.399999618530273D), Telerik.Reporting.Drawing.Unit.Cm(13.711457252502441D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7000002861022949D), Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269D));
            this.textBox10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox10.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox10.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox10.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox10.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox10.Style.Color = System.Drawing.Color.Black;
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Calibri";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox10.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.Value = "PANEL CONFIGURATION";
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.66145831346511841D), Telerik.Reporting.Drawing.Unit.Cm(14.311457633972168D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.4002137184143066D), Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269D));
            this.textBox20.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox20.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox20.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox20.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox20.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox20.Style.Color = System.Drawing.Color.Black;
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.Font.Name = "Calibri";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(20D);
            this.textBox20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.Value = "PAYMENT FOR SERVICE";
            // 
            // textBox51
            // 
            this.textBox51.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Cm(17.69999885559082D));
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox51.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox51.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox51.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox51.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox51.Style.Color = System.Drawing.Color.SteelBlue;
            this.textBox51.Style.Font.Bold = true;
            this.textBox51.Style.Font.Name = "Calibri";
            this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox51.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox51.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox51.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox51.Value = "Customer Input  :";
            // 
            // textBox16
            // 
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Cm(18.394166946411133D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.299999237060547D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Calibri";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox16.Value = "";
            // 
            // textBox52
            // 
            this.textBox52.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Cm(19.561458587646484D));
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox52.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox52.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox52.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox52.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox52.Style.Color = System.Drawing.Color.SteelBlue;
            this.textBox52.Style.Font.Bold = true;
            this.textBox52.Style.Font.Name = "Calibri";
            this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox52.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox52.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox52.Value = "Fault Identified  :";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.77771151065826416D), Telerik.Reporting.Drawing.Unit.Cm(22.111457824707031D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.322286605834961D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Calibri";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox12.Value = "";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.82645833492279053D), Telerik.Reporting.Drawing.Unit.Cm(20.299999237060547D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.273540496826172D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Calibri";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox13.Value = "";
            // 
            // textBox54
            // 
            this.textBox54.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.82645833492279053D), Telerik.Reporting.Drawing.Unit.Cm(23.161458969116211D));
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9999992847442627D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.textBox54.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox54.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox54.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox54.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox54.Style.Color = System.Drawing.Color.SteelBlue;
            this.textBox54.Style.Font.Bold = true;
            this.textBox54.Style.Font.Name = "Calibri";
            this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox54.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox54.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox54.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox54.Value = "Work Done  :";
            // 
            // textBox53
            // 
            this.textBox53.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Cm(21.361457824707031D));
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.textBox53.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox53.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox53.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox53.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox53.Style.Color = System.Drawing.Color.SteelBlue;
            this.textBox53.Style.Font.Bold = true;
            this.textBox53.Style.Font.Name = "Calibri";
            this.textBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox53.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox53.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox53.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox53.Value = "Action Required  :";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.82645833492279053D), Telerik.Reporting.Drawing.Unit.Cm(23.899999618530273D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.273540496826172D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Name = "Calibri";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox11.Value = "";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.64975839853286743D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.0203123092651367D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(14.397844314575195D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60854196548461914D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox41, 1, 3);
            tableGroup29.Name = "tableGroup";
            tableGroup29.ReportItem = this.textBox15;
            tableGroup30.Name = "tableGroup3";
            tableGroup30.ReportItem = this.textBox42;
            tableGroup31.Name = "tableGroup6";
            tableGroup31.ReportItem = this.textBox56;
            this.table1.ColumnGroups.Add(tableGroup29);
            this.table1.ColumnGroups.Add(tableGroup30);
            this.table1.ColumnGroups.Add(tableGroup31);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox41,
            this.textBox15,
            this.textBox42,
            this.textBox56});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.51145839691162109D), Telerik.Reporting.Drawing.Unit.Cm(26.161457061767578D));
            this.table1.Name = "table1";
            tableGroup32.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup32.Name = "detailTableGroup1";
            this.table1.RowGroups.Add(tableGroup32);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.067914962768555D), Telerik.Reporting.Drawing.Unit.Cm(1.2700004577636719D));
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.067914962768555D), Telerik.Reporting.Drawing.Unit.Cm(0.60854190587997437D));
            this.textBox41.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox41.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox41.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox41.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox41.Style.Color = System.Drawing.Color.SteelBlue;
            this.textBox41.Style.Font.Bold = true;
            this.textBox41.Style.Font.Name = "Calibri";
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox41.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox41.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox41.Value = "the solar system in regards to the details in Customer Input.";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0691649913787842D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.7835402488708496D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.34395831823349D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox57);
            this.table2.Body.SetCellContent(0, 1, this.textBox59);
            tableGroup33.Name = "tableGroup7";
            tableGroup34.Name = "tableGroup8";
            this.table2.ColumnGroups.Add(tableGroup33);
            this.table2.ColumnGroups.Add(tableGroup34);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox57,
            this.textBox59});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.51145839691162109D), Telerik.Reporting.Drawing.Unit.Cm(27.661457061767578D));
            this.table2.Name = "table2";
            tableGroup35.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup35.Name = "detailTableGroup3";
            this.table2.RowGroups.Add(tableGroup35);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8527050018310547D), Telerik.Reporting.Drawing.Unit.Cm(0.34395831823349D));
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0691654682159424D), Telerik.Reporting.Drawing.Unit.Cm(0.34395831823349D));
            this.textBox57.Style.Color = System.Drawing.Color.SteelBlue;
            this.textBox57.Style.Font.Bold = true;
            this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox57.Value = "Customer Signature:";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7835392951965332D), Telerik.Reporting.Drawing.Unit.Cm(0.34395831823349D));
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.67020845413208D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.228959321975708D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.34395831823349D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox58);
            this.table4.Body.SetCellContent(0, 1, this.textBox61);
            tableGroup36.Name = "tableGroup9";
            tableGroup37.Name = "tableGroup10";
            this.table4.ColumnGroups.Add(tableGroup36);
            this.table4.ColumnGroups.Add(tableGroup37);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox58,
            this.textBox61});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.799999237060547D), Telerik.Reporting.Drawing.Unit.Cm(27.661457061767578D));
            this.table4.Name = "table4";
            tableGroup38.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup38.Name = "detailTableGroup4";
            this.table4.RowGroups.Add(tableGroup38);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8991680145263672D), Telerik.Reporting.Drawing.Unit.Cm(0.34395831823349D));
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.67020845413208D), Telerik.Reporting.Drawing.Unit.Cm(0.34395831823349D));
            this.textBox58.Style.Color = System.Drawing.Color.SteelBlue;
            this.textBox58.Style.Font.Bold = true;
            this.textBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox58.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox58.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox58.Value = "Date";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.228959321975708D), Telerik.Reporting.Drawing.Unit.Cm(0.34395831823349D));
            // 
            // textBox55
            // 
            this.textBox55.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.30000066757202148D), Telerik.Reporting.Drawing.Unit.Cm(28.861457824707031D));
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.27937126159668D), Telerik.Reporting.Drawing.Unit.Cm(0.49999856948852539D));
            this.textBox55.Style.Color = System.Drawing.Color.White;
            this.textBox55.Style.Font.Bold = true;
            this.textBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox55.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox55.Value = "ABN 93 603 941 475 Phone: 1300 285 885 www.solarminer.com.au info@solarminer.com." +
    "au";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(9.9944772955495864E-05D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5885410308837891D), Telerik.Reporting.Drawing.Unit.Cm(1.3799000978469849D));
            this.textBox1.Style.Color = System.Drawing.Color.White;
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(35D);
            this.textBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox1.Value = "Maintenance";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D), Telerik.Reporting.Drawing.Unit.Cm(1.3885416984558106D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.5D), Telerik.Reporting.Drawing.Unit.Cm(1.3385418653488159D));
            this.textBox2.Style.Color = System.Drawing.Color.White;
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(35D);
            this.textBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox2.Value = "Contract";
            // 
            // SolarMinerMaintenance
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "SolarMinerMaintenance";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.Table table8;
        private Telerik.Reporting.TextBox textBox127;
        private Telerik.Reporting.TextBox textBox126;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox txtInstallBookingDate;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox txtMtceCost;
        private Telerik.Reporting.TextBox txtMtceDiscount;
        private Telerik.Reporting.TextBox txtMtceBalance;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
    }
}