namespace EurosolarReporting
{
    partial class SmTaxInvoiceNew
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SmTaxInvoiceNew));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.txtpanel = new Telerik.Reporting.TextBox();
            this.txtsys = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.txtstate = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.txtinvoicedate = new Telerik.Reporting.TextBox();
            this.txtinvoiceno = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.txtinverters = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.table7 = new Telerik.Reporting.Table();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.txtstcince = new Telerik.Reporting.TextBox();
            this.txtvicrebate = new Telerik.Reporting.TextBox();
            this.txtvicloanDisc = new Telerik.Reporting.TextBox();
            this.txtnetcost = new Telerik.Reporting.TextBox();
            this.txtdepositepaid = new Telerik.Reporting.TextBox();
            this.txtbaldue = new Telerik.Reporting.TextBox();
            this.txttotal = new Telerik.Reporting.TextBox();
            this.txtnote = new Telerik.Reporting.TextBox();
            this.txtinvDate = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.206D), Telerik.Reporting.Drawing.Unit.Cm(1.584D));
            this.textBox32.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox32.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox32.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.206D), Telerik.Reporting.Drawing.Unit.Cm(1.415D));
            this.textBox29.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox29.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // txtpanel
            // 
            this.txtpanel.Name = "txtpanel";
            this.txtpanel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.359D), Telerik.Reporting.Drawing.Unit.Cm(1.584D));
            this.txtpanel.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.txtpanel.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtpanel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtpanel.StyleName = "";
            // 
            // txtsys
            // 
            this.txtsys.Name = "txtsys";
            this.txtsys.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.359D), Telerik.Reporting.Drawing.Unit.Cm(1.415D));
            this.txtsys.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.txtsys.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtsys.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtsys.StyleName = "";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.984D), Telerik.Reporting.Drawing.Unit.Cm(0.915D));
            this.textBox6.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox6.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.747D), Telerik.Reporting.Drawing.Unit.Cm(0.915D));
            this.textBox11.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.Value = "=Fields.Contact";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.984D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox8.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox8.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.773D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox10.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.Value = "=Fields.CustPhone";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.288D), Telerik.Reporting.Drawing.Unit.Cm(0.915D));
            this.textBox33.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox33.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox33.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox44
            // 
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.739D), Telerik.Reporting.Drawing.Unit.Cm(0.915D));
            this.textBox44.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox44.StyleName = "";
            this.textBox44.Value = "= TrimStart(Fields.InstallAddress)";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.408D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox42.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox42.Style.Font.Bold = true;
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox42.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox42.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // txtstate
            // 
            this.txtstate.Name = "txtstate";
            this.txtstate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.773D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.txtstate.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.txtstate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtstate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtstate.StyleName = "";
            this.txtstate.Value = "=Fields.InstallState";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(297D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.table1,
            this.table2,
            this.table4,
            this.table5,
            this.table6,
            this.table7,
            this.txtstcince,
            this.txtvicrebate,
            this.txtvicloanDisc,
            this.txtnetcost,
            this.txtdepositepaid,
            this.txtbaldue,
            this.txttotal,
            this.txtnote,
            this.txtinvDate});
            this.detail.Name = "detail";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.079D), Telerik.Reporting.Drawing.Unit.Cm(0.053D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(296D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.884D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.932D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.99D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.529D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.635D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox2);
            this.table1.Body.SetCellContent(0, 1, this.txtinvoicedate);
            this.table1.Body.SetCellContent(0, 4, this.txtinvoiceno);
            this.table1.Body.SetCellContent(0, 3, this.textBox3);
            this.table1.Body.SetCellContent(0, 2, this.textBox40);
            tableGroup1.Name = "tableGroup";
            tableGroup2.Name = "tableGroup1";
            tableGroup3.Name = "group18";
            tableGroup4.Name = "group1";
            tableGroup5.Name = "group3";
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.ColumnGroups.Add(tableGroup4);
            this.table1.ColumnGroups.Add(tableGroup5);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox2,
            this.txtinvoicedate,
            this.textBox40,
            this.textBox3,
            this.txtinvoiceno});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.7D), Telerik.Reporting.Drawing.Unit.Cm(2.8D));
            this.table1.Name = "table1";
            tableGroup6.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup6.Name = "detailTableGroup";
            this.table1.RowGroups.Add(tableGroup6);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.235D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.884D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.textBox2.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            // 
            // txtinvoicedate
            // 
            this.txtinvoicedate.Name = "txtinvoicedate";
            this.txtinvoicedate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.932D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.txtinvoicedate.Style.Color = System.Drawing.Color.Red;
            this.txtinvoicedate.Style.Font.Bold = true;
            this.txtinvoicedate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.txtinvoicedate.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.txtinvoicedate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            // 
            // txtinvoiceno
            // 
            this.txtinvoiceno.Name = "txtinvoiceno";
            this.txtinvoiceno.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.529D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.txtinvoiceno.Style.Color = System.Drawing.Color.Red;
            this.txtinvoiceno.Style.Font.Bold = true;
            this.txtinvoiceno.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.txtinvoiceno.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.txtinvoiceno.StyleName = "";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.99D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.textBox3.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox3.StyleName = "";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9D), Telerik.Reporting.Drawing.Unit.Cm(0.635D));
            this.textBox40.Style.Color = System.Drawing.Color.Red;
            this.textBox40.Style.Font.Bold = true;
            this.textBox40.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox40.StyleName = "";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.206D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.359D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.587D)));
            this.table2.Body.SetCellContent(0, 1, this.txtinverters);
            this.table2.Body.SetCellContent(0, 0, this.textBox52);
            tableGroup8.Name = "group5";
            tableGroup8.ReportItem = this.textBox32;
            tableGroup7.ChildGroups.Add(tableGroup8);
            tableGroup7.Name = "group9";
            tableGroup7.ReportItem = this.textBox29;
            tableGroup10.Name = "group17";
            tableGroup10.ReportItem = this.txtpanel;
            tableGroup9.ChildGroups.Add(tableGroup10);
            tableGroup9.Name = "group16";
            tableGroup9.ReportItem = this.txtsys;
            this.table2.ColumnGroups.Add(tableGroup7);
            this.table2.ColumnGroups.Add(tableGroup9);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox52,
            this.txtinverters,
            this.textBox29,
            this.textBox32,
            this.txtsys,
            this.txtpanel});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(13.9D));
            this.table2.Name = "table2";
            tableGroup11.Name = "group21";
            this.table2.RowGroups.Add(tableGroup11);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.565D), Telerik.Reporting.Drawing.Unit.Cm(4.585D));
            // 
            // txtinverters
            // 
            this.txtinverters.Name = "txtinverters";
            this.txtinverters.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.359D), Telerik.Reporting.Drawing.Unit.Cm(1.587D));
            this.txtinverters.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.txtinverters.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtinverters.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtinverters.StyleName = "";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.206D), Telerik.Reporting.Drawing.Unit.Cm(1.587D));
            this.textBox52.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox52.Style.Font.Bold = true;
            this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox52.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox52.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox52.StyleName = "";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.984D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.747D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.277D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.968D)));
            this.table4.Body.SetCellContent(1, 0, this.textBox13);
            this.table4.Body.SetCellContent(0, 0, this.textBox16);
            this.table4.Body.SetCellContent(0, 1, this.textBox18);
            this.table4.Body.SetCellContent(1, 1, this.textBox43);
            tableGroup12.Name = "tableGroup2";
            tableGroup12.ReportItem = this.textBox6;
            tableGroup13.Name = "tableGroup4";
            tableGroup13.ReportItem = this.textBox11;
            this.table4.ColumnGroups.Add(tableGroup12);
            this.table4.ColumnGroups.Add(tableGroup13);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox16,
            this.textBox18,
            this.textBox13,
            this.textBox43,
            this.textBox6,
            this.textBox11});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.182D), Telerik.Reporting.Drawing.Unit.Cm(5.1D));
            this.table4.Name = "table4";
            tableGroup15.Name = "group12";
            tableGroup16.Name = "group6";
            tableGroup14.ChildGroups.Add(tableGroup15);
            tableGroup14.ChildGroups.Add(tableGroup16);
            tableGroup14.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup14.Name = "detailTableGroup1";
            this.table4.RowGroups.Add(tableGroup14);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.731D), Telerik.Reporting.Drawing.Unit.Cm(2.161D));
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.984D), Telerik.Reporting.Drawing.Unit.Cm(0.968D));
            this.textBox13.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox13.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox13.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.StyleName = "";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.984D), Telerik.Reporting.Drawing.Unit.Cm(0.277D));
            this.textBox16.StyleName = "";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.747D), Telerik.Reporting.Drawing.Unit.Cm(0.277D));
            this.textBox18.StyleName = "";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.747D), Telerik.Reporting.Drawing.Unit.Cm(0.968D));
            this.textBox43.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox43.Style.Font.Bold = false;
            this.textBox43.Style.Font.Name = "Calibri";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox43.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox43.StyleName = "";
            this.textBox43.Value = "=Fields.ContMobile";
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.984D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.773D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.291D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.95D)));
            this.table5.Body.SetCellContent(0, 0, this.textBox9);
            this.table5.Body.SetCellContent(0, 1, this.textBox12);
            this.table5.Body.SetCellContent(1, 0, this.textBox19);
            this.table5.Body.SetCellContent(1, 1, this.textBox20);
            tableGroup17.Name = "tableGroup3";
            tableGroup17.ReportItem = this.textBox8;
            tableGroup18.Name = "tableGroup7";
            tableGroup18.ReportItem = this.textBox10;
            this.table5.ColumnGroups.Add(tableGroup17);
            this.table5.ColumnGroups.Add(tableGroup18);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox9,
            this.textBox12,
            this.textBox19,
            this.textBox20,
            this.textBox8,
            this.textBox10});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.202D), Telerik.Reporting.Drawing.Unit.Cm(5.1D));
            this.table5.Name = "table5";
            tableGroup19.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup19.Name = "detailTableGroup3";
            tableGroup20.Name = "group2";
            this.table5.RowGroups.Add(tableGroup19);
            this.table5.RowGroups.Add(tableGroup20);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.758D), Telerik.Reporting.Drawing.Unit.Cm(2.14D));
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.984D), Telerik.Reporting.Drawing.Unit.Cm(0.291D));
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.773D), Telerik.Reporting.Drawing.Unit.Cm(0.291D));
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.984D), Telerik.Reporting.Drawing.Unit.Cm(0.95D));
            this.textBox19.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox19.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox19.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.StyleName = "";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.773D), Telerik.Reporting.Drawing.Unit.Cm(0.95D));
            this.textBox20.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox20.Style.Font.Bold = false;
            this.textBox20.Style.Font.Name = "Calibri";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.StyleName = "";
            this.textBox20.Value = "=Fields.ContEmail";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.288D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.739D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.277D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.968D)));
            this.table6.Body.SetCellContent(1, 0, this.textBox14);
            this.table6.Body.SetCellContent(1, 1, this.textBox17);
            this.table6.Body.SetCellContent(0, 0, this.textBox21);
            this.table6.Body.SetCellContent(0, 1, this.textBox22);
            tableGroup21.Name = "tableGroup2";
            tableGroup21.ReportItem = this.textBox33;
            tableGroup22.Name = "tableGroup4";
            tableGroup22.ReportItem = this.textBox44;
            this.table6.ColumnGroups.Add(tableGroup21);
            this.table6.ColumnGroups.Add(tableGroup22);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox21,
            this.textBox22,
            this.textBox14,
            this.textBox17,
            this.textBox33,
            this.textBox44});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(9.7D));
            this.table6.Name = "table6";
            tableGroup24.Name = "group12";
            tableGroup25.Name = "group6";
            tableGroup23.ChildGroups.Add(tableGroup24);
            tableGroup23.ChildGroups.Add(tableGroup25);
            tableGroup23.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup23.Name = "detailTableGroup1";
            this.table6.RowGroups.Add(tableGroup23);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.027D), Telerik.Reporting.Drawing.Unit.Cm(2.161D));
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.288D), Telerik.Reporting.Drawing.Unit.Cm(0.968D));
            this.textBox14.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox14.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.StyleName = "";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.739D), Telerik.Reporting.Drawing.Unit.Cm(0.968D));
            this.textBox17.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox17.StyleName = "";
            this.textBox17.Value = "=Fields.InstallCity";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.288D), Telerik.Reporting.Drawing.Unit.Cm(0.277D));
            this.textBox21.StyleName = "";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.739D), Telerik.Reporting.Drawing.Unit.Cm(0.277D));
            this.textBox22.StyleName = "";
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.408D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.773D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.291D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.95D)));
            this.table7.Body.SetCellContent(0, 0, this.textBox36);
            this.table7.Body.SetCellContent(0, 1, this.textBox38);
            this.table7.Body.SetCellContent(1, 0, this.textBox39);
            this.table7.Body.SetCellContent(1, 1, this.textBox41);
            tableGroup26.Name = "tableGroup3";
            tableGroup26.ReportItem = this.textBox42;
            tableGroup27.Name = "tableGroup7";
            tableGroup27.ReportItem = this.txtstate;
            this.table7.ColumnGroups.Add(tableGroup26);
            this.table7.ColumnGroups.Add(tableGroup27);
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox36,
            this.textBox38,
            this.textBox39,
            this.textBox41,
            this.textBox42,
            this.txtstate});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11D), Telerik.Reporting.Drawing.Unit.Cm(9.7D));
            this.table7.Name = "table7";
            tableGroup28.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup28.Name = "detailTableGroup3";
            tableGroup29.Name = "group2";
            this.table7.RowGroups.Add(tableGroup28);
            this.table7.RowGroups.Add(tableGroup29);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.181D), Telerik.Reporting.Drawing.Unit.Cm(2.14D));
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.408D), Telerik.Reporting.Drawing.Unit.Cm(0.291D));
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.773D), Telerik.Reporting.Drawing.Unit.Cm(0.291D));
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.408D), Telerik.Reporting.Drawing.Unit.Cm(0.95D));
            this.textBox39.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox39.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox39.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.StyleName = "";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.773D), Telerik.Reporting.Drawing.Unit.Cm(0.95D));
            this.textBox41.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox41.StyleName = "";
            this.textBox41.Value = "=Fields.InstallPostCode";
            // 
            // txtstcince
            // 
            this.txtstcince.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.6D), Telerik.Reporting.Drawing.Unit.Cm(14.7D));
            this.txtstcince.Name = "txtstcince";
            this.txtstcince.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtstcince.Style.Color = System.Drawing.Color.White;
            this.txtstcince.Value = "";
            // 
            // txtvicrebate
            // 
            this.txtvicrebate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.6D), Telerik.Reporting.Drawing.Unit.Cm(15.7D));
            this.txtvicrebate.Name = "txtvicrebate";
            this.txtvicrebate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtvicrebate.Style.Color = System.Drawing.Color.White;
            this.txtvicrebate.Value = "";
            // 
            // txtvicloanDisc
            // 
            this.txtvicloanDisc.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.6D), Telerik.Reporting.Drawing.Unit.Cm(16.8D));
            this.txtvicloanDisc.Name = "txtvicloanDisc";
            this.txtvicloanDisc.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtvicloanDisc.Style.Color = System.Drawing.Color.White;
            this.txtvicloanDisc.Value = "";
            // 
            // txtnetcost
            // 
            this.txtnetcost.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.6D), Telerik.Reporting.Drawing.Unit.Cm(17.8D));
            this.txtnetcost.Name = "txtnetcost";
            this.txtnetcost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtnetcost.Style.Color = System.Drawing.Color.White;
            this.txtnetcost.Value = "";
            // 
            // txtdepositepaid
            // 
            this.txtdepositepaid.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.6D), Telerik.Reporting.Drawing.Unit.Cm(18.8D));
            this.txtdepositepaid.Name = "txtdepositepaid";
            this.txtdepositepaid.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtdepositepaid.Style.Color = System.Drawing.Color.White;
            this.txtdepositepaid.Value = "";
            // 
            // txtbaldue
            // 
            this.txtbaldue.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.6D), Telerik.Reporting.Drawing.Unit.Cm(19.8D));
            this.txtbaldue.Name = "txtbaldue";
            this.txtbaldue.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txtbaldue.Style.Color = System.Drawing.Color.White;
            this.txtbaldue.Value = "";
            // 
            // txttotal
            // 
            this.txttotal.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.6D), Telerik.Reporting.Drawing.Unit.Cm(13.1D));
            this.txttotal.Name = "txttotal";
            this.txttotal.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.txttotal.Style.Color = System.Drawing.Color.White;
            this.txttotal.Style.Font.Bold = true;
            this.txttotal.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.txttotal.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txttotal.Value = "";
            // 
            // txtnote
            // 
            this.txtnote.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.2D), Telerik.Reporting.Drawing.Unit.Cm(21.2D));
            this.txtnote.Name = "txtnote";
            this.txtnote.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.981D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
            this.txtnote.Style.Color = System.Drawing.Color.White;
            this.txtnote.Style.Font.Bold = true;
            this.txtnote.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtnote.Value = "textBox1";
            // 
            // txtinvDate
            // 
            this.txtinvDate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.9D), Telerik.Reporting.Drawing.Unit.Inch(11.2D));
            this.txtinvDate.Name = "txtinvDate";
            this.txtinvDate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.866D), Telerik.Reporting.Drawing.Unit.Inch(0.3D));
            this.txtinvDate.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.txtinvDate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtinvDate.Value = "textBox1";
            // 
            // SmTaxInvoiceNew
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "SmTaxInvoice";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210.79D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox txtinvoicedate;
        private Telerik.Reporting.TextBox txtinvoiceno;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox txtinverters;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox txtpanel;
        private Telerik.Reporting.TextBox txtsys;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox txtstate;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox txtstcince;
        private Telerik.Reporting.TextBox txtvicrebate;
        private Telerik.Reporting.TextBox txtvicloanDisc;
        private Telerik.Reporting.TextBox txtnetcost;
        private Telerik.Reporting.TextBox txtdepositepaid;
        private Telerik.Reporting.TextBox txtbaldue;
        private Telerik.Reporting.TextBox txttotal;
        private Telerik.Reporting.TextBox txtnote;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox txtinvDate;
    }
}