namespace EurosolarReporting
{
    partial class NInvoice
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NInvoice));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.txtdate = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.lblCommissionGST = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.lblDepositPaid = new Telerik.Reporting.TextBox();
            this.lblBalanceDue = new Telerik.Reporting.TextBox();
            this.lblNetCost = new Telerik.Reporting.TextBox();
            this.lblTotalQuotePrice = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.lblrebate = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.txtinverters = new Telerik.Reporting.TextBox();
            this.txtheader = new Telerik.Reporting.TextBox();
            this.table7 = new Telerik.Reporting.Table();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.htmlTextBox2 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox3 = new Telerik.Reporting.HtmlTextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // txtdate
            // 
            this.txtdate.Height = Telerik.Reporting.Drawing.Unit.Cm(29.700000762939453D);
            this.txtdate.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.table1,
            this.table2,
            this.table3,
            this.table4,
            this.table5,
            this.table6,
            this.txtheader,
            this.table7,
            this.htmlTextBox2,
            this.htmlTextBox3,
            this.textBox1});
            this.txtdate.Name = "txtdate";
            this.txtdate.Style.Font.Name = "Calibri";
            this.txtdate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.700000762939453D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.pictureBox1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.pictureBox1.Style.Font.Name = "Calibri";
            this.pictureBox1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.pictureBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.pictureBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9368753433227539D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.2652096748352051D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.95000004768371582D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.95000004768371582D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox3);
            this.table1.Body.SetCellContent(0, 1, this.textBox5);
            this.table1.Body.SetCellContent(1, 0, this.textBox2);
            this.table1.Body.SetCellContent(1, 1, this.textBox4);
            tableGroup1.Name = "tableGroup";
            tableGroup2.Name = "tableGroup1";
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox3,
            this.textBox5,
            this.textBox2,
            this.textBox4});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.6000000238418579D), Telerik.Reporting.Drawing.Unit.Cm(4D));
            this.table1.Name = "table1";
            tableGroup4.Name = "group";
            tableGroup5.Name = "group1";
            tableGroup3.ChildGroups.Add(tableGroup4);
            tableGroup3.ChildGroups.Add(tableGroup5);
            tableGroup3.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup3.Name = "detailTableGroup";
            this.table1.RowGroups.Add(tableGroup3);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.2020845413208D), Telerik.Reporting.Drawing.Unit.Cm(1.9000000953674316D));
            this.table1.Style.Font.Name = "Calibri";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.936875581741333D), Telerik.Reporting.Drawing.Unit.Cm(0.95000004768371582D));
            this.textBox3.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox3.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox3.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox3.Style.Color = System.Drawing.Color.White;
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Calibri";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox3.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox3.Value = "";
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.2652082443237305D), Telerik.Reporting.Drawing.Unit.Cm(0.95000004768371582D));
            this.textBox5.Style.Color = System.Drawing.Color.White;
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Name = "Calibri";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox5.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "=Fields.Contact";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.936875581741333D), Telerik.Reporting.Drawing.Unit.Cm(0.95000004768371582D));
            this.textBox2.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox2.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox2.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox2.Style.Color = System.Drawing.Color.White;
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Name = "Calibri";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox2.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox2.StyleName = "";
            this.textBox2.Value = "";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.2652082443237305D), Telerik.Reporting.Drawing.Unit.Cm(0.95000004768371582D));
            this.textBox4.Style.Color = System.Drawing.Color.White;
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Name = "Calibri";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox4.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox4.StyleName = "";
            this.textBox4.Value = "=Fields.ContMobile";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.4870836734771729D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.1118755340576172D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.949999988079071D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.949999988079071D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox6);
            this.table2.Body.SetCellContent(0, 1, this.textBox7);
            this.table2.Body.SetCellContent(1, 0, this.textBox8);
            this.table2.Body.SetCellContent(1, 1, this.textBox9);
            tableGroup6.Name = "tableGroup";
            tableGroup7.Name = "tableGroup1";
            this.table2.ColumnGroups.Add(tableGroup6);
            this.table2.ColumnGroups.Add(tableGroup7);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11D), Telerik.Reporting.Drawing.Unit.Cm(3.9000000953674316D));
            this.table2.Name = "table2";
            tableGroup9.Name = "group";
            tableGroup10.Name = "group1";
            tableGroup8.ChildGroups.Add(tableGroup9);
            tableGroup8.ChildGroups.Add(tableGroup10);
            tableGroup8.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup8.Name = "detailTableGroup";
            this.table2.RowGroups.Add(tableGroup8);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5989589691162109D), Telerik.Reporting.Drawing.Unit.Cm(1.8999999761581421D));
            this.table2.Style.Font.Name = "Calibri";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4870836734771729D), Telerik.Reporting.Drawing.Unit.Cm(0.949999988079071D));
            this.textBox6.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox6.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox6.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox6.Style.Color = System.Drawing.Color.White;
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Name = "Calibri";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox6.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.111875057220459D), Telerik.Reporting.Drawing.Unit.Cm(0.949999988079071D));
            this.textBox7.Style.Color = System.Drawing.Color.White;
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Name = "Calibri";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox7.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.Value = "=Fields.CustPhone";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4870836734771729D), Telerik.Reporting.Drawing.Unit.Cm(0.949999988079071D));
            this.textBox8.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox8.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox8.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox8.Style.Color = System.Drawing.Color.White;
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Name = "Calibri";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox8.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox8.StyleName = "";
            this.textBox8.Value = "";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.111875057220459D), Telerik.Reporting.Drawing.Unit.Cm(0.949999988079071D));
            this.textBox9.Style.Color = System.Drawing.Color.White;
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Calibri";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox9.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.StyleName = "";
            this.textBox9.Value = "=Fields.ContEmail";
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.5400002002716064D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.2122912406921387D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.94999992847442627D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.95000004768371582D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox10);
            this.table3.Body.SetCellContent(0, 1, this.textBox11);
            this.table3.Body.SetCellContent(1, 0, this.textBox12);
            this.table3.Body.SetCellContent(1, 1, this.textBox13);
            tableGroup11.Name = "tableGroup";
            tableGroup12.Name = "tableGroup1";
            this.table3.ColumnGroups.Add(tableGroup11);
            this.table3.ColumnGroups.Add(tableGroup12);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.6999999284744263D), Telerik.Reporting.Drawing.Unit.Cm(8.1000003814697266D));
            this.table3.Name = "table3";
            tableGroup14.Name = "group";
            tableGroup15.Name = "group1";
            tableGroup13.ChildGroups.Add(tableGroup14);
            tableGroup13.ChildGroups.Add(tableGroup15);
            tableGroup13.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup13.Name = "detailTableGroup";
            this.table3.RowGroups.Add(tableGroup13);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.7522916793823242D), Telerik.Reporting.Drawing.Unit.Cm(1.9000000953674316D));
            this.table3.Style.Font.Name = "Calibri";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5400002002716064D), Telerik.Reporting.Drawing.Unit.Cm(0.95000004768371582D));
            this.textBox10.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox10.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox10.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox10.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox10.Style.Color = System.Drawing.Color.White;
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Name = "Calibri";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox10.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox10.Value = "";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.2122917175292969D), Telerik.Reporting.Drawing.Unit.Cm(0.95000004768371582D));
            this.textBox11.Style.Color = System.Drawing.Color.White;
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Name = "Calibri";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox11.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.Value = "= TrimStart(Fields.InstallAddress)";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5400002002716064D), Telerik.Reporting.Drawing.Unit.Cm(0.95000004768371582D));
            this.textBox12.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox12.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox12.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox12.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox12.Style.Color = System.Drawing.Color.White;
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Calibri";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox12.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox12.StyleName = "";
            this.textBox12.Value = "";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.2122917175292969D), Telerik.Reporting.Drawing.Unit.Cm(0.95000004768371582D));
            this.textBox13.Style.Color = System.Drawing.Color.White;
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Calibri";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox13.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.StyleName = "";
            this.textBox13.Value = "=Fields.InstallCity";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9633340835571289D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.0060410499572754D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.95000004768371582D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.95000004768371582D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox14);
            this.table4.Body.SetCellContent(0, 1, this.textBox15);
            this.table4.Body.SetCellContent(1, 0, this.textBox16);
            this.table4.Body.SetCellContent(1, 1, this.textBox17);
            tableGroup16.Name = "tableGroup";
            tableGroup17.Name = "tableGroup1";
            this.table4.ColumnGroups.Add(tableGroup16);
            this.table4.ColumnGroups.Add(tableGroup17);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.899999618530273D), Telerik.Reporting.Drawing.Unit.Cm(8.1000003814697266D));
            this.table4.Name = "table4";
            tableGroup19.Name = "group";
            tableGroup20.Name = "group1";
            tableGroup18.ChildGroups.Add(tableGroup19);
            tableGroup18.ChildGroups.Add(tableGroup20);
            tableGroup18.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup18.Name = "detailTableGroup";
            this.table4.RowGroups.Add(tableGroup18);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.9693746566772461D), Telerik.Reporting.Drawing.Unit.Cm(1.9000000953674316D));
            this.table4.Style.Font.Name = "Calibri";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9633336067199707D), Telerik.Reporting.Drawing.Unit.Cm(0.95000004768371582D));
            this.textBox14.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox14.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox14.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox14.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox14.Style.Color = System.Drawing.Color.White;
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Calibri";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox14.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox14.Value = "";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.0060405731201172D), Telerik.Reporting.Drawing.Unit.Cm(0.95000004768371582D));
            this.textBox15.Style.Color = System.Drawing.Color.White;
            this.textBox15.Style.Font.Bold = true;
            this.textBox15.Style.Font.Name = "Calibri";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox15.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.Value = "=Fields.InstallState";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.96333384513855D), Telerik.Reporting.Drawing.Unit.Cm(0.95000004768371582D));
            this.textBox16.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox16.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox16.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox16.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox16.Style.Color = System.Drawing.Color.White;
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Font.Name = "Calibri";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox16.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox16.StyleName = "";
            this.textBox16.Value = "";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.0060405731201172D), Telerik.Reporting.Drawing.Unit.Cm(0.95000004768371582D));
            this.textBox17.Style.Color = System.Drawing.Color.White;
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.Font.Name = "Calibri";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox17.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox17.StyleName = "";
            this.textBox17.Value = "=Fields.InstallPostCode";
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.9422967433929443D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9843733310699463D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.6933338642120361D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.8256247043609619D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.46041521430015564D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.73812329769134521D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.94979113340377808D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.89687508344650269D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.84395688772201538D)));
            this.table5.Body.SetCellContent(0, 0, this.textBox19);
            this.table5.Body.SetCellContent(2, 0, this.textBox18);
            this.table5.Body.SetCellContent(3, 0, this.textBox24);
            this.table5.Body.SetCellContent(5, 2, this.lblCommissionGST);
            this.table5.Body.SetCellContent(4, 0, this.textBox30);
            this.table5.Body.SetCellContent(5, 0, this.textBox27, 1, 2);
            this.table5.Body.SetCellContent(3, 1, this.lblDepositPaid, 1, 2);
            this.table5.Body.SetCellContent(4, 1, this.lblBalanceDue, 1, 2);
            this.table5.Body.SetCellContent(2, 1, this.lblNetCost, 1, 2);
            this.table5.Body.SetCellContent(0, 1, this.lblTotalQuotePrice, 1, 2);
            this.table5.Body.SetCellContent(1, 0, this.textBox38);
            this.table5.Body.SetCellContent(1, 1, this.lblrebate, 1, 2);
            tableGroup21.Name = "tableGroup2";
            tableGroup22.Name = "tableGroup3";
            tableGroup23.Name = "tableGroup4";
            this.table5.ColumnGroups.Add(tableGroup21);
            this.table5.ColumnGroups.Add(tableGroup22);
            this.table5.ColumnGroups.Add(tableGroup23);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox19,
            this.lblTotalQuotePrice,
            this.textBox38,
            this.lblrebate,
            this.textBox18,
            this.lblNetCost,
            this.textBox24,
            this.lblDepositPaid,
            this.textBox30,
            this.lblBalanceDue,
            this.textBox27,
            this.lblCommissionGST});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.699999809265137D), Telerik.Reporting.Drawing.Unit.Cm(11.399999618530273D));
            this.table5.Name = "table5";
            tableGroup25.Name = "group2";
            tableGroup26.Name = "group7";
            tableGroup27.Name = "group3";
            tableGroup28.Name = "group4";
            tableGroup29.Name = "group6";
            tableGroup30.Name = "group5";
            tableGroup24.ChildGroups.Add(tableGroup25);
            tableGroup24.ChildGroups.Add(tableGroup26);
            tableGroup24.ChildGroups.Add(tableGroup27);
            tableGroup24.ChildGroups.Add(tableGroup28);
            tableGroup24.ChildGroups.Add(tableGroup29);
            tableGroup24.ChildGroups.Add(tableGroup30);
            tableGroup24.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup24.Name = "detailTableGroup1";
            this.table5.RowGroups.Add(tableGroup24);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6200037002563477D), Telerik.Reporting.Drawing.Unit.Cm(6.423825740814209D));
            this.table5.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.table5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.table5.Style.Font.Name = "Calibri";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9422945976257324D), Telerik.Reporting.Drawing.Unit.Cm(1.8256251811981201D));
            this.textBox19.Style.Color = System.Drawing.Color.White;
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Name = "Calibri";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox19.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.Value = "";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.942296028137207D), Telerik.Reporting.Drawing.Unit.Cm(0.73812353610992432D));
            this.textBox18.Style.BorderColor.Default = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox18.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox18.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox18.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.Font.Name = "Calibri";
            this.textBox18.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox18.StyleName = "";
            this.textBox18.Value = "";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.942296028137207D), Telerik.Reporting.Drawing.Unit.Cm(0.94979113340377808D));
            this.textBox24.Style.BorderColor.Default = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox24.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox24.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox24.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.Font.Name = "Calibri";
            this.textBox24.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.StyleName = "";
            this.textBox24.Value = "";
            // 
            // lblCommissionGST
            // 
            this.lblCommissionGST.Name = "lblCommissionGST";
            this.lblCommissionGST.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6933335065841675D), Telerik.Reporting.Drawing.Unit.Cm(0.84395688772201538D));
            this.lblCommissionGST.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.lblCommissionGST.Style.Font.Bold = true;
            this.lblCommissionGST.Style.Font.Name = "Calibri";
            this.lblCommissionGST.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.lblCommissionGST.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.lblCommissionGST.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblCommissionGST.Style.Visible = true;
            this.lblCommissionGST.StyleName = "";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9422957897186279D), Telerik.Reporting.Drawing.Unit.Cm(0.89687514305114746D));
            this.textBox30.Style.BorderColor.Default = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox30.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox30.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox30.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox30.Style.Font.Bold = true;
            this.textBox30.Style.Font.Name = "Calibri";
            this.textBox30.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox30.StyleName = "";
            this.textBox30.Value = "";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9266681671142578D), Telerik.Reporting.Drawing.Unit.Cm(0.84395694732666016D));
            this.textBox27.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Font.Name = "Calibri";
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox27.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox27.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox27.StyleName = "";
            this.textBox27.Value = "";
            // 
            // lblDepositPaid
            // 
            this.lblDepositPaid.Name = "lblDepositPaid";
            this.lblDepositPaid.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6777069568634033D), Telerik.Reporting.Drawing.Unit.Cm(0.94979095458984375D));
            this.lblDepositPaid.Style.BorderColor.Default = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.lblDepositPaid.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.lblDepositPaid.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.lblDepositPaid.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.lblDepositPaid.Style.Font.Bold = true;
            this.lblDepositPaid.Style.Font.Name = "Calibri";
            this.lblDepositPaid.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.lblDepositPaid.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.lblDepositPaid.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.lblDepositPaid.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblDepositPaid.StyleName = "";
            this.lblDepositPaid.Value = "";
            // 
            // lblBalanceDue
            // 
            this.lblBalanceDue.Name = "lblBalanceDue";
            this.lblBalanceDue.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6777069568634033D), Telerik.Reporting.Drawing.Unit.Cm(0.89687502384185791D));
            this.lblBalanceDue.Style.BorderColor.Default = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.lblBalanceDue.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.lblBalanceDue.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.lblBalanceDue.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.lblBalanceDue.Style.Font.Bold = true;
            this.lblBalanceDue.Style.Font.Name = "Calibri";
            this.lblBalanceDue.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.lblBalanceDue.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.lblBalanceDue.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.lblBalanceDue.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblBalanceDue.StyleName = "";
            // 
            // lblNetCost
            // 
            this.lblNetCost.Name = "lblNetCost";
            this.lblNetCost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6777069568634033D), Telerik.Reporting.Drawing.Unit.Cm(0.73812335729599D));
            this.lblNetCost.Style.BorderColor.Default = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.lblNetCost.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.lblNetCost.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.lblNetCost.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.lblNetCost.Style.Font.Bold = true;
            this.lblNetCost.Style.Font.Name = "Calibri";
            this.lblNetCost.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.lblNetCost.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.lblNetCost.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.lblNetCost.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblNetCost.StyleName = "";
            // 
            // lblTotalQuotePrice
            // 
            this.lblTotalQuotePrice.Name = "lblTotalQuotePrice";
            this.lblTotalQuotePrice.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6777069568634033D), Telerik.Reporting.Drawing.Unit.Cm(1.8256247043609619D));
            this.lblTotalQuotePrice.Style.Color = System.Drawing.Color.White;
            this.lblTotalQuotePrice.Style.Font.Bold = true;
            this.lblTotalQuotePrice.Style.Font.Name = "Calibri";
            this.lblTotalQuotePrice.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.lblTotalQuotePrice.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(22D);
            this.lblTotalQuotePrice.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.lblTotalQuotePrice.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblTotalQuotePrice.Value = "";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9422945976257324D), Telerik.Reporting.Drawing.Unit.Inch(0.4604153037071228D));
            this.textBox38.Style.BorderColor.Default = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox38.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox38.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox38.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox38.Style.Font.Bold = true;
            this.textBox38.Style.Font.Name = "Calibri";
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox38.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.StyleName = "";
            this.textBox38.Value = "";
            // 
            // lblrebate
            // 
            this.lblrebate.Name = "lblrebate";
            this.lblrebate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6777069568634033D), Telerik.Reporting.Drawing.Unit.Inch(0.46041515469551086D));
            this.lblrebate.Style.BorderColor.Default = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.lblrebate.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.lblrebate.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.lblrebate.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.lblrebate.Style.Font.Bold = true;
            this.lblrebate.Style.Font.Name = "Calibri";
            this.lblrebate.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.lblrebate.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.lblrebate.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.lblrebate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.lblrebate.StyleName = "";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.3018753528594971D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7.4612517356872559D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.3859342336654663D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(3.2567753791809082D)));
            this.table6.Body.SetCellContent(0, 0, this.textBox22);
            this.table6.Body.SetCellContent(0, 1, this.textBox23);
            this.table6.Body.SetCellContent(1, 0, this.textBox26);
            this.table6.Body.SetCellContent(1, 1, this.txtinverters);
            tableGroup31.Name = "tableGroup";
            tableGroup32.Name = "tableGroup1";
            this.table6.ColumnGroups.Add(tableGroup31);
            this.table6.ColumnGroups.Add(tableGroup32);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox22,
            this.textBox23,
            this.textBox26,
            this.txtinverters});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.7999999523162842D), Telerik.Reporting.Drawing.Unit.Cm(13.000000953674316D));
            this.table6.Name = "table6";
            tableGroup34.Name = "group";
            tableGroup35.Name = "group1";
            tableGroup33.ChildGroups.Add(tableGroup34);
            tableGroup33.ChildGroups.Add(tableGroup35);
            tableGroup33.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup33.Name = "detailTableGroup";
            this.table6.RowGroups.Add(tableGroup33);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.763127326965332D), Telerik.Reporting.Drawing.Unit.Cm(4.6427092552185059D));
            this.table6.Style.Font.Name = "Calibri";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3018753528594971D), Telerik.Reporting.Drawing.Unit.Cm(1.3859341144561768D));
            this.textBox22.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox22.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox22.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox22.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox22.Style.Color = System.Drawing.Color.White;
            this.textBox22.Style.Font.Bold = true;
            this.textBox22.Style.Font.Name = "Calibri";
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox22.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox22.Value = "";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.4612512588500977D), Telerik.Reporting.Drawing.Unit.Cm(1.3859341144561768D));
            this.textBox23.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox23.Style.Color = System.Drawing.Color.White;
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Name = "Calibri";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox23.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox23.Value = "=Fields.PanelDetails";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3018753528594971D), Telerik.Reporting.Drawing.Unit.Cm(3.25677490234375D));
            this.textBox26.Style.BorderColor.Default = System.Drawing.Color.White;
            this.textBox26.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox26.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox26.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox26.Style.Color = System.Drawing.Color.White;
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.Font.Name = "Calibri";
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox26.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox26.StyleName = "";
            this.textBox26.Value = "";
            // 
            // txtinverters
            // 
            this.txtinverters.Name = "txtinverters";
            this.txtinverters.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.4612517356872559D), Telerik.Reporting.Drawing.Unit.Cm(3.25677490234375D));
            this.txtinverters.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.txtinverters.Style.Color = System.Drawing.Color.White;
            this.txtinverters.Style.Font.Bold = true;
            this.txtinverters.Style.Font.Name = "Calibri";
            this.txtinverters.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.txtinverters.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.txtinverters.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtinverters.StyleName = "";
            // 
            // txtheader
            // 
            this.txtheader.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.4415690004825592D), Telerik.Reporting.Drawing.Unit.Inch(0.15748031437397003D));
            this.txtheader.Name = "txtheader";
            this.txtheader.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.2828402519226074D), Telerik.Reporting.Drawing.Unit.Inch(0.47244098782539368D));
            this.txtheader.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.txtheader.Style.Font.Bold = true;
            this.txtheader.Style.Font.Name = "Calibri";
            this.txtheader.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(28D);
            this.txtheader.Value = "INVOICE";
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.547111988067627D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.39774227142334D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.5838773250579834D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.543749988079071D)));
            this.table7.Body.SetCellContent(0, 0, this.textBox41);
            this.table7.Body.SetCellContent(0, 1, this.textBox42);
            this.table7.Body.SetCellContent(0, 2, this.textBox43);
            tableGroup36.Name = "tableGroup1";
            tableGroup37.Name = "group";
            tableGroup38.Name = "tableGroup2";
            this.table7.ColumnGroups.Add(tableGroup36);
            this.table7.ColumnGroups.Add(tableGroup37);
            this.table7.ColumnGroups.Add(tableGroup38);
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox41,
            this.textBox42,
            this.textBox43});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.1215852499008179D), Telerik.Reporting.Drawing.Unit.Inch(9.0000009536743164D));
            this.table7.Name = "table7";
            tableGroup40.Name = "group3";
            tableGroup39.ChildGroups.Add(tableGroup40);
            tableGroup39.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup39.Name = "detailTableGroup";
            this.table7.RowGroups.Add(tableGroup39);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(191.22978210449219D), Telerik.Reporting.Drawing.Unit.Inch(0.543749988079071D));
            this.table7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table7.Style.Font.Name = "Calibri";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5471134185791016D), Telerik.Reporting.Drawing.Unit.Inch(0.543749988079071D));
            this.textBox41.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox41.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox41.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox41.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox41.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox41.Style.Color = System.Drawing.Color.Black;
            this.textBox41.Style.Font.Name = "Calibri";
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox41.StyleName = "";
            this.textBox41.Value = "A/c Name: Arise Solar \r\nBSB: 034001 \r\nA/c No. 504046";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.397742748260498D), Telerik.Reporting.Drawing.Unit.Inch(0.543749988079071D));
            this.textBox42.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox42.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox42.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox42.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox42.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox42.Style.Color = System.Drawing.Color.Black;
            this.textBox42.Style.Font.Name = "Calibri";
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox42.StyleName = "";
            this.textBox42.Value = "(Master / Visa cards)\r\nwith 1% surcharge";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5838773250579834D), Telerik.Reporting.Drawing.Unit.Inch(0.543749988079071D));
            this.textBox43.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox43.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox43.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox43.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox43.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox43.Style.Color = System.Drawing.Color.Black;
            this.textBox43.Style.Font.Name = "Calibri";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox43.StyleName = "";
            this.textBox43.Value = "Payable to \"Arise Solar\" and mail it to\r\nPO BOX 3079\r\n Sunnybank South QLD 4109";
            // 
            // htmlTextBox2
            // 
            this.htmlTextBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(9.6456689834594727D));
            this.htmlTextBox2.Name = "htmlTextBox2";
            this.htmlTextBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.5372295379638672D), Telerik.Reporting.Drawing.Unit.Inch(0.278740257024765D));
            this.htmlTextBox2.Style.BorderColor.Top = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(134)))), ((int)(((byte)(198)))));
            this.htmlTextBox2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox2.Style.Font.Name = "Calibri";
            this.htmlTextBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.htmlTextBox2.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.htmlTextBox2.Value = "<span style=\"font-size: 12px; color: #00b1f1\"><strong>Payment Schedules:</strong>" +
    " </span><span style=\"font-size: 12px\">This Invoice needs to be paid on the day o" +
    "f installation&nbsp;</span>";
            // 
            // htmlTextBox3
            // 
            this.htmlTextBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Inch(10D));
            this.htmlTextBox3.Name = "htmlTextBox3";
            this.htmlTextBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.4703011512756348D), Telerik.Reporting.Drawing.Unit.Inch(1.220471978187561D));
            this.htmlTextBox3.Style.Font.Name = "Calibri";
            this.htmlTextBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.htmlTextBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox3.Value = resources.GetString("htmlTextBox3.Value");
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.47244095802307129D), Telerik.Reporting.Drawing.Unit.Inch(0.66929131746292114D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.2828402519226074D), Telerik.Reporting.Drawing.Unit.Inch(0.27566927671432495D));
            this.textBox1.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(136)))), ((int)(((byte)(201)))));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "Calibri";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "INVOICE";
            // 
            // NInvoice
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtdate});
            this.Name = "Invoice";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.Style.Font.Name = "open sp";
            this.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(21D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection txtdate;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox lblCommissionGST;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox lblDepositPaid;
        private Telerik.Reporting.TextBox lblBalanceDue;
        private Telerik.Reporting.TextBox lblNetCost;
        private Telerik.Reporting.TextBox lblTotalQuotePrice;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox txtinverters;
        private Telerik.Reporting.TextBox lblrebate;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.HtmlTextBox htmlTextBox2;
        private Telerik.Reporting.HtmlTextBox htmlTextBox3;
        private Telerik.Reporting.TextBox txtheader;
        private Telerik.Reporting.TextBox textBox1;
    }
}