namespace EurosolarReporting
{
    partial class PerformaInvoice
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PerformaInvoice));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.txtinvoicenum = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.table7 = new Telerik.Reporting.Table();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.txtstate = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.txtinverters = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.txtpanel = new Telerik.Reporting.TextBox();
            this.txtsys = new Telerik.Reporting.TextBox();
            this.txttotal = new Telerik.Reporting.TextBox();
            this.txtbaldue = new Telerik.Reporting.TextBox();
            this.txtdepositepaid = new Telerik.Reporting.TextBox();
            this.txtnetcost = new Telerik.Reporting.TextBox();
            this.txtvicloanDisc = new Telerik.Reporting.TextBox();
            this.txtvicrebate = new Telerik.Reporting.TextBox();
            this.txtstcince = new Telerik.Reporting.TextBox();
            this.txtinvoicedate = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.txtnote = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(297D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.txtinvoicenum,
            this.table4,
            this.table5,
            this.table6,
            this.table7,
            this.table2,
            this.txttotal,
            this.txtstcince,
            this.txtvicrebate,
            this.txtvicloanDisc,
            this.txtnetcost,
            this.txtdepositepaid,
            this.txtbaldue,
            this.txtinvoicedate,
            this.txtnote});
            this.detail.Name = "detail";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // txtinvoicenum
            // 
            this.txtinvoicenum.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.1000003814697266D), Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D));
            this.txtinvoicenum.Name = "txtinvoicenum";
            this.txtinvoicenum.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269D));
            this.txtinvoicenum.Style.Color = System.Drawing.Color.White;
            this.txtinvoicenum.Style.Font.Bold = true;
            this.txtinvoicenum.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.txtinvoicenum.Value = "";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9843757152557373D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.7468729019165039D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.27742066979408264D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.968132734298706D)));
            this.table4.Body.SetCellContent(1, 0, this.textBox13);
            this.table4.Body.SetCellContent(0, 0, this.textBox16);
            this.table4.Body.SetCellContent(0, 1, this.textBox18);
            this.table4.Body.SetCellContent(1, 1, this.textBox43);
            tableGroup1.Name = "tableGroup2";
            tableGroup1.ReportItem = this.textBox6;
            tableGroup2.Name = "tableGroup4";
            tableGroup2.ReportItem = this.textBox11;
            this.table4.ColumnGroups.Add(tableGroup1);
            this.table4.ColumnGroups.Add(tableGroup2);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox16,
            this.textBox18,
            this.textBox13,
            this.textBox43,
            this.textBox6,
            this.textBox11});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Cm(3.8000001907348633D));
            this.table4.Name = "table4";
            tableGroup4.Name = "group12";
            tableGroup5.Name = "group6";
            tableGroup3.ChildGroups.Add(tableGroup4);
            tableGroup3.ChildGroups.Add(tableGroup5);
            tableGroup3.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup3.Name = "detailTableGroup1";
            this.table4.RowGroups.Add(tableGroup3);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.73124885559082D), Telerik.Reporting.Drawing.Unit.Cm(2.1610414981842041D));
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9843757152557373D), Telerik.Reporting.Drawing.Unit.Cm(0.968132734298706D));
            this.textBox13.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox13.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox13.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.StyleName = "";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9843757152557373D), Telerik.Reporting.Drawing.Unit.Cm(0.27742066979408264D));
            this.textBox16.StyleName = "";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.74687385559082D), Telerik.Reporting.Drawing.Unit.Cm(0.27742066979408264D));
            this.textBox18.StyleName = "";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.74687385559082D), Telerik.Reporting.Drawing.Unit.Cm(0.96813267469406128D));
            this.textBox43.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox43.Style.Font.Bold = false;
            this.textBox43.Style.Font.Name = "Calibri";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox43.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox43.StyleName = "";
            this.textBox43.Value = "=Fields.ContMobile";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9843757152557373D), Telerik.Reporting.Drawing.Unit.Cm(0.91548812389373779D));
            this.textBox6.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox6.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.74687385559082D), Telerik.Reporting.Drawing.Unit.Cm(0.91548812389373779D));
            this.textBox11.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.Value = "=Fields.Contact";
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9843748807907105D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.7733325958251953D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.29104158282279968D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.94979172945022583D)));
            this.table5.Body.SetCellContent(0, 0, this.textBox9);
            this.table5.Body.SetCellContent(0, 1, this.textBox12);
            this.table5.Body.SetCellContent(1, 0, this.textBox19);
            this.table5.Body.SetCellContent(1, 1, this.textBox20);
            tableGroup6.Name = "tableGroup3";
            tableGroup6.ReportItem = this.textBox8;
            tableGroup7.Name = "tableGroup7";
            tableGroup7.ReportItem = this.textBox10;
            this.table5.ColumnGroups.Add(tableGroup6);
            this.table5.ColumnGroups.Add(tableGroup7);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox9,
            this.textBox12,
            this.textBox19,
            this.textBox20,
            this.textBox8,
            this.textBox10});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.199999809265137D), Telerik.Reporting.Drawing.Unit.Cm(3.8000001907348633D));
            this.table5.Name = "table5";
            tableGroup8.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup8.Name = "detailTableGroup3";
            tableGroup9.Name = "group2";
            this.table5.RowGroups.Add(tableGroup8);
            this.table5.RowGroups.Add(tableGroup9);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.7577075958251953D), Telerik.Reporting.Drawing.Unit.Cm(2.1404163837432861D));
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9843751192092896D), Telerik.Reporting.Drawing.Unit.Cm(0.29104164242744446D));
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.7733330726623535D), Telerik.Reporting.Drawing.Unit.Cm(0.29104164242744446D));
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9843751192092896D), Telerik.Reporting.Drawing.Unit.Cm(0.94979172945022583D));
            this.textBox19.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox19.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox19.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.StyleName = "";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.7733330726623535D), Telerik.Reporting.Drawing.Unit.Cm(0.94979178905487061D));
            this.textBox20.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox20.Style.Font.Bold = false;
            this.textBox20.Style.Font.Name = "Calibri";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.StyleName = "";
            this.textBox20.Value = "=Fields.ContEmail";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9843751192092896D), Telerik.Reporting.Drawing.Unit.Cm(0.899583101272583D));
            this.textBox8.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox8.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.7733330726623535D), Telerik.Reporting.Drawing.Unit.Cm(0.899583101272583D));
            this.textBox10.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.Value = "=Fields.CustPhone";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.2882299423217773D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.73921012878418D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.27742066979408264D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.968132734298706D)));
            this.table6.Body.SetCellContent(1, 0, this.textBox14);
            this.table6.Body.SetCellContent(1, 1, this.textBox17);
            this.table6.Body.SetCellContent(0, 0, this.textBox21);
            this.table6.Body.SetCellContent(0, 1, this.textBox22);
            tableGroup10.Name = "tableGroup2";
            tableGroup10.ReportItem = this.textBox33;
            tableGroup11.Name = "tableGroup4";
            tableGroup11.ReportItem = this.textBox44;
            this.table6.ColumnGroups.Add(tableGroup10);
            this.table6.ColumnGroups.Add(tableGroup11);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox21,
            this.textBox22,
            this.textBox14,
            this.textBox17,
            this.textBox33,
            this.textBox44});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(8.59999942779541D));
            this.table6.Name = "table6";
            tableGroup13.Name = "group12";
            tableGroup14.Name = "group6";
            tableGroup12.ChildGroups.Add(tableGroup13);
            tableGroup12.ChildGroups.Add(tableGroup14);
            tableGroup12.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup12.Name = "detailTableGroup1";
            this.table6.RowGroups.Add(tableGroup12);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.027440071105957D), Telerik.Reporting.Drawing.Unit.Cm(2.1610414981842041D));
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2882297039031982D), Telerik.Reporting.Drawing.Unit.Cm(0.968132734298706D));
            this.textBox14.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox14.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.StyleName = "";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.73921012878418D), Telerik.Reporting.Drawing.Unit.Cm(0.968132734298706D));
            this.textBox17.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox17.StyleName = "";
            this.textBox17.Value = "=Fields.InstallCity";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2882297039031982D), Telerik.Reporting.Drawing.Unit.Cm(0.27742066979408264D));
            this.textBox21.StyleName = "";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.73921012878418D), Telerik.Reporting.Drawing.Unit.Cm(0.27742066979408264D));
            this.textBox22.StyleName = "";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2882297039031982D), Telerik.Reporting.Drawing.Unit.Cm(0.91548812389373779D));
            this.textBox33.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox33.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox33.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox44
            // 
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.73921012878418D), Telerik.Reporting.Drawing.Unit.Cm(0.91548812389373779D));
            this.textBox44.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox44.StyleName = "";
            this.textBox44.Value = "= TrimStart(Fields.InstallAddress)";
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.4077081680297852D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.7733330726623535D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.29104161262512207D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.94979172945022583D)));
            this.table7.Body.SetCellContent(0, 0, this.textBox36);
            this.table7.Body.SetCellContent(0, 1, this.textBox38);
            this.table7.Body.SetCellContent(1, 0, this.textBox39);
            this.table7.Body.SetCellContent(1, 1, this.textBox41);
            tableGroup15.Name = "tableGroup3";
            tableGroup15.ReportItem = this.textBox42;
            tableGroup16.Name = "tableGroup7";
            tableGroup16.ReportItem = this.txtstate;
            this.table7.ColumnGroups.Add(tableGroup15);
            this.table7.ColumnGroups.Add(tableGroup16);
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox36,
            this.textBox38,
            this.textBox39,
            this.textBox41,
            this.textBox42,
            this.txtstate});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11D), Telerik.Reporting.Drawing.Unit.Cm(8.6999998092651367D));
            this.table7.Name = "table7";
            tableGroup17.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup17.Name = "detailTableGroup3";
            tableGroup18.Name = "group2";
            this.table7.RowGroups.Add(tableGroup17);
            this.table7.RowGroups.Add(tableGroup18);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.1810417175292969D), Telerik.Reporting.Drawing.Unit.Cm(2.1404163837432861D));
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4077081680297852D), Telerik.Reporting.Drawing.Unit.Cm(0.29104161262512207D));
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.7733330726623535D), Telerik.Reporting.Drawing.Unit.Cm(0.29104164242744446D));
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.7733330726623535D), Telerik.Reporting.Drawing.Unit.Cm(0.94979178905487061D));
            this.textBox41.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox41.StyleName = "";
            this.textBox41.Value = "=Fields.InstallPostCode";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4077081680297852D), Telerik.Reporting.Drawing.Unit.Cm(0.89958304166793823D));
            this.textBox42.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox42.Style.Font.Bold = true;
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox42.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox42.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // txtstate
            // 
            this.txtstate.Name = "txtstate";
            this.txtstate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.7733330726623535D), Telerik.Reporting.Drawing.Unit.Cm(0.899583101272583D));
            this.txtstate.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.txtstate.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtstate.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtstate.StyleName = "";
            this.txtstate.Value = "=Fields.InstallState";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.2055070400238037D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.6976227760314941D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.5870089530944824D)));
            this.table2.Body.SetCellContent(0, 1, this.txtinverters);
            this.table2.Body.SetCellContent(0, 0, this.textBox52);
            tableGroup20.Name = "group5";
            tableGroup20.ReportItem = this.textBox32;
            tableGroup19.ChildGroups.Add(tableGroup20);
            tableGroup19.Name = "group9";
            tableGroup19.ReportItem = this.textBox29;
            tableGroup22.Name = "group17";
            tableGroup22.ReportItem = this.txtpanel;
            tableGroup21.ChildGroups.Add(tableGroup22);
            tableGroup21.Name = "group16";
            tableGroup21.ReportItem = this.txtsys;
            this.table2.ColumnGroups.Add(tableGroup19);
            this.table2.ColumnGroups.Add(tableGroup21);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox52,
            this.txtinverters,
            this.textBox29,
            this.textBox32,
            this.txtsys,
            this.txtpanel});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.1000000238418579D), Telerik.Reporting.Drawing.Unit.Cm(13.199999809265137D));
            this.table2.Name = "table2";
            tableGroup23.Name = "group21";
            this.table2.RowGroups.Add(tableGroup23);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.9031295776367188D), Telerik.Reporting.Drawing.Unit.Cm(4.5852079391479492D));
            // 
            // txtinverters
            // 
            this.txtinverters.Name = "txtinverters";
            this.txtinverters.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.6976222991943359D), Telerik.Reporting.Drawing.Unit.Cm(1.5870088338851929D));
            this.txtinverters.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.txtinverters.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtinverters.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtinverters.StyleName = "";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2055058479309082D), Telerik.Reporting.Drawing.Unit.Cm(1.5870087146759033D));
            this.textBox52.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox52.Style.Font.Bold = true;
            this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox52.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox52.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox52.StyleName = "";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2055058479309082D), Telerik.Reporting.Drawing.Unit.Cm(1.5835951566696167D));
            this.textBox32.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox32.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox32.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2055058479309082D), Telerik.Reporting.Drawing.Unit.Cm(1.4146037101745606D));
            this.textBox29.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox29.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // txtpanel
            // 
            this.txtpanel.Name = "txtpanel";
            this.txtpanel.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.6976222991943359D), Telerik.Reporting.Drawing.Unit.Cm(1.5835952758789063D));
            this.txtpanel.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.txtpanel.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtpanel.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtpanel.StyleName = "";
            // 
            // txtsys
            // 
            this.txtsys.Name = "txtsys";
            this.txtsys.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.6976222991943359D), Telerik.Reporting.Drawing.Unit.Cm(1.41460382938385D));
            this.txtsys.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.txtsys.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.txtsys.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtsys.StyleName = "";
            // 
            // txttotal
            // 
            this.txttotal.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.799999237060547D), Telerik.Reporting.Drawing.Unit.Cm(12.599998474121094D));
            this.txttotal.Name = "txttotal";
            this.txttotal.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9999992847442627D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.txttotal.Style.Color = System.Drawing.Color.White;
            this.txttotal.Style.Font.Bold = true;
            this.txttotal.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.txttotal.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.txttotal.Value = "";
            // 
            // txtbaldue
            // 
            this.txtbaldue.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.799999237060547D), Telerik.Reporting.Drawing.Unit.Cm(19.200000762939453D));
            this.txtbaldue.Name = "txtbaldue";
            this.txtbaldue.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9999995231628418D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.txtbaldue.Style.Color = System.Drawing.Color.White;
            this.txtbaldue.Value = "";
            // 
            // txtdepositepaid
            // 
            this.txtdepositepaid.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.799999237060547D), Telerik.Reporting.Drawing.Unit.Cm(18.200000762939453D));
            this.txtdepositepaid.Name = "txtdepositepaid";
            this.txtdepositepaid.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9999995231628418D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.txtdepositepaid.Style.Color = System.Drawing.Color.White;
            this.txtdepositepaid.Value = "";
            // 
            // txtnetcost
            // 
            this.txtnetcost.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.799999237060547D), Telerik.Reporting.Drawing.Unit.Cm(17.19999885559082D));
            this.txtnetcost.Name = "txtnetcost";
            this.txtnetcost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9999995231628418D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.txtnetcost.Style.Color = System.Drawing.Color.White;
            this.txtnetcost.Value = "";
            // 
            // txtvicloanDisc
            // 
            this.txtvicloanDisc.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.799999237060547D), Telerik.Reporting.Drawing.Unit.Cm(16.19999885559082D));
            this.txtvicloanDisc.Name = "txtvicloanDisc";
            this.txtvicloanDisc.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9999995231628418D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.txtvicloanDisc.Style.Color = System.Drawing.Color.White;
            this.txtvicloanDisc.Value = "";
            // 
            // txtvicrebate
            // 
            this.txtvicrebate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.799999237060547D), Telerik.Reporting.Drawing.Unit.Cm(15.100000381469727D));
            this.txtvicrebate.Name = "txtvicrebate";
            this.txtvicrebate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9999995231628418D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.txtvicrebate.Style.Color = System.Drawing.Color.White;
            this.txtvicrebate.Value = "";
            // 
            // txtstcince
            // 
            this.txtstcince.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.799999237060547D), Telerik.Reporting.Drawing.Unit.Cm(14.09999942779541D));
            this.txtstcince.Name = "txtstcince";
            this.txtstcince.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9999995231628418D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.txtstcince.Style.Color = System.Drawing.Color.White;
            this.txtstcince.Value = "";
            // 
            // txtinvoicedate
            // 
            this.txtinvoicedate.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.600000381469727D), Telerik.Reporting.Drawing.Unit.Cm(2.5D));
            this.txtinvoicedate.Name = "txtinvoicedate";
            this.txtinvoicedate.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.581040620803833D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.txtinvoicedate.Style.Color = System.Drawing.Color.White;
            this.txtinvoicedate.Value = "textBox2";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4077081680297852D), Telerik.Reporting.Drawing.Unit.Cm(0.949791669845581D));
            this.textBox39.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox39.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox39.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.StyleName = "";
            // 
            // txtnote
            // 
            this.txtnote.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(20.5D));
            this.txtnote.Name = "txtnote";
            this.txtnote.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.98104190826416D), Telerik.Reporting.Drawing.Unit.Cm(0.39999979734420776D));
            this.txtnote.Style.Color = System.Drawing.Color.White;
            this.txtnote.Style.Font.Bold = true;
            this.txtnote.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.txtnote.Value = "textBox1";
            // 
            // PerformaInvoice
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "PerformaInvoice";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(212D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.TextBox txtinvoicenum;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox txtstate;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox txtinverters;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox txtpanel;
        private Telerik.Reporting.TextBox txtsys;
        private Telerik.Reporting.TextBox txttotal;
        private Telerik.Reporting.TextBox txtstcince;
        private Telerik.Reporting.TextBox txtvicrebate;
        private Telerik.Reporting.TextBox txtvicloanDisc;
        private Telerik.Reporting.TextBox txtnetcost;
        private Telerik.Reporting.TextBox txtdepositepaid;
        private Telerik.Reporting.TextBox txtbaldue;
        private Telerik.Reporting.TextBox txtinvoicedate;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox txtnote;
    }
}