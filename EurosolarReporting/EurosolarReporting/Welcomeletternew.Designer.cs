namespace EurosolarReporting
{
    partial class Welcomeletternew
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Welcomeletternew));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup48 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup49 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup50 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup51 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup52 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup53 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup54 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup55 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup56 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup57 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup58 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup59 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.txtbasiccost = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.pictureBox3 = new Telerik.Reporting.PictureBox();
            this.pictureBox4 = new Telerik.Reporting.PictureBox();
            this.pictureBox5 = new Telerik.Reporting.PictureBox();
            this.pictureBox6 = new Telerik.Reporting.PictureBox();
            this.textBox191 = new Telerik.Reporting.TextBox();
            this.tblcustomer = new Telerik.Reporting.Table();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.tblsales = new Telerik.Reporting.Table();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.htmlTextBox7 = new Telerik.Reporting.HtmlTextBox();
            this.pictureBox7 = new Telerik.Reporting.PictureBox();
            this.pictureBox8 = new Telerik.Reporting.PictureBox();
            this.pictureBox9 = new Telerik.Reporting.PictureBox();
            this.pictureBox10 = new Telerik.Reporting.PictureBox();
            this.pictureBox11 = new Telerik.Reporting.PictureBox();
            this.pictureBox12 = new Telerik.Reporting.PictureBox();
            this.tblsitedetail = new Telerik.Reporting.Table();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.txtProjectNumber = new Telerik.Reporting.TextBox();
            this.tblsitedetails1 = new Telerik.Reporting.Table();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.txtmeter = new Telerik.Reporting.TextBox();
            this.txtswitch = new Telerik.Reporting.TextBox();
            this.tblsitedetails2 = new Telerik.Reporting.Table();
            this.txtmeterphase = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.tblsysinsdetail = new Telerik.Reporting.Table();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.txtInverterdetail1 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.txtotherdetail = new Telerik.Reporting.TextBox();
            this.table11 = new Telerik.Reporting.Table();
            this.txtreq = new Telerik.Reporting.TextBox();
            this.txtcompl = new Telerik.Reporting.TextBox();
            this.txtextra = new Telerik.Reporting.TextBox();
            this.txtfullretail = new Telerik.Reporting.TextBox();
            this.txtdiscount2 = new Telerik.Reporting.TextBox();
            this.txtstc = new Telerik.Reporting.TextBox();
            this.txtgst = new Telerik.Reporting.TextBox();
            this.txtdeposit = new Telerik.Reporting.TextBox();
            this.txtlessdeposit = new Telerik.Reporting.TextBox();
            this.table9 = new Telerik.Reporting.Table();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.table10 = new Telerik.Reporting.Table();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.txtprojectnote = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5D), Telerik.Reporting.Drawing.Unit.Cm(1.2435417175292969D));
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(22D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.Value = "CUSTOMER DETAILS";
            // 
            // txtbasiccost
            // 
            this.txtbasiccost.Name = "txtbasiccost";
            this.txtbasiccost.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1536650657653809D), Telerik.Reporting.Drawing.Unit.Cm(0.6646580696105957D));
            this.txtbasiccost.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtbasiccost.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtbasiccost.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtbasiccost.Value = "=Fields.lblbasiccost";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(1782D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.pictureBox2,
            this.pictureBox3,
            this.pictureBox4,
            this.pictureBox5,
            this.pictureBox6,
            this.textBox191,
            this.tblcustomer,
            this.tblsales,
            this.htmlTextBox7,
            this.pictureBox7,
            this.pictureBox8,
            this.pictureBox9,
            this.pictureBox10,
            this.pictureBox11,
            this.pictureBox12,
            this.tblsitedetail,
            this.txtProjectNumber,
            this.tblsitedetails1,
            this.tblsitedetails2,
            this.tblsysinsdetail,
            this.txtotherdetail,
            this.table11,
            this.table9,
            this.table10,
            this.textBox83,
            this.textBox81,
            this.txtprojectnote});
            this.detail.Name = "detail";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(29.700000762939453D));
            this.pictureBox2.MimeType = "image/jpeg";
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.pictureBox2.Value = ((object)(resources.GetObject("pictureBox2.Value")));
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(59.400001525878906D));
            this.pictureBox3.MimeType = "image/jpeg";
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.pictureBox3.Value = ((object)(resources.GetObject("pictureBox3.Value")));
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(89.1001968383789D));
            this.pictureBox4.MimeType = "image/jpeg";
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.pictureBox4.Value = ((object)(resources.GetObject("pictureBox4.Value")));
            // 
            // pictureBox5
            // 
            this.pictureBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(118.80040740966797D));
            this.pictureBox5.MimeType = "image/jpeg";
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.pictureBox5.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.Normal;
            this.pictureBox5.Value = ((object)(resources.GetObject("pictureBox5.Value")));
            // 
            // pictureBox6
            // 
            this.pictureBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(148.50062561035156D));
            this.pictureBox6.MimeType = "image/jpeg";
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
            this.pictureBox6.Value = ((object)(resources.GetObject("pictureBox6.Value")));
            // 
            // textBox191
            // 
            this.textBox191.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.59999942779541D), Telerik.Reporting.Drawing.Unit.Cm(30.500001907348633D));
            this.textBox191.Name = "textBox191";
            this.textBox191.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.2012495994567871D), Telerik.Reporting.Drawing.Unit.Cm(8.5D));
            this.textBox191.Style.Color = System.Drawing.Color.White;
            this.textBox191.Style.Font.Name = "Calibri";
            this.textBox191.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12.5D);
            this.textBox191.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(1.5D);
            this.textBox191.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox191.Value = resources.GetString("textBox191.Value");
            // 
            // tblcustomer
            // 
            this.tblcustomer.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(8.5D)));
            this.tblcustomer.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60854172706604D)));
            this.tblcustomer.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.81750017404556274D)));
            this.tblcustomer.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.76458358764648438D)));
            this.tblcustomer.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.76458364725112915D)));
            this.tblcustomer.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.92333316802978516D)));
            this.tblcustomer.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.79104173183441162D)));
            this.tblcustomer.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.84395772218704224D)));
            this.tblcustomer.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.764583170413971D)));
            this.tblcustomer.Body.SetCellContent(0, 0, this.textBox30);
            this.tblcustomer.Body.SetCellContent(2, 0, this.textBox32);
            this.tblcustomer.Body.SetCellContent(4, 0, this.textBox34);
            this.tblcustomer.Body.SetCellContent(6, 0, this.textBox36);
            this.tblcustomer.Body.SetCellContent(1, 0, this.textBox6);
            this.tblcustomer.Body.SetCellContent(3, 0, this.textBox2);
            this.tblcustomer.Body.SetCellContent(5, 0, this.textBox7);
            this.tblcustomer.Body.SetCellContent(7, 0, this.textBox8);
            tableGroup1.Name = "tableGroup3";
            tableGroup1.ReportItem = this.textBox5;
            this.tblcustomer.ColumnGroups.Add(tableGroup1);
            this.tblcustomer.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox30,
            this.textBox6,
            this.textBox32,
            this.textBox2,
            this.textBox34,
            this.textBox7,
            this.textBox36,
            this.textBox8,
            this.textBox5});
            this.tblcustomer.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.2999999523162842D), Telerik.Reporting.Drawing.Unit.Cm(20.200000762939453D));
            this.tblcustomer.Name = "tblcustomer";
            tableGroup3.Name = "group13";
            tableGroup4.Name = "group14";
            tableGroup5.Name = "group15";
            tableGroup6.Name = "group16";
            tableGroup7.Name = "group17";
            tableGroup8.Name = "group18";
            tableGroup9.Name = "group19";
            tableGroup10.Name = "group20";
            tableGroup2.ChildGroups.Add(tableGroup3);
            tableGroup2.ChildGroups.Add(tableGroup4);
            tableGroup2.ChildGroups.Add(tableGroup5);
            tableGroup2.ChildGroups.Add(tableGroup6);
            tableGroup2.ChildGroups.Add(tableGroup7);
            tableGroup2.ChildGroups.Add(tableGroup8);
            tableGroup2.ChildGroups.Add(tableGroup9);
            tableGroup2.ChildGroups.Add(tableGroup10);
            tableGroup2.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup2.Name = "detailTableGroup2";
            this.tblcustomer.RowGroups.Add(tableGroup2);
            this.tblcustomer.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5D), Telerik.Reporting.Drawing.Unit.Cm(7.5216665267944336D));
            this.tblcustomer.Style.Color = System.Drawing.Color.White;
            this.tblcustomer.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5D), Telerik.Reporting.Drawing.Unit.Cm(0.60854172706604D));
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox30.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.079999998211860657D);
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox30.Value = "PROJECT NUMBER";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5D), Telerik.Reporting.Drawing.Unit.Cm(0.76458358764648438D));
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox32.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.079999998211860657D);
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox32.StyleName = "";
            this.textBox32.Value = "CONTACT NAME";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5D), Telerik.Reporting.Drawing.Unit.Cm(0.92333316802978516D));
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox34.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.079999998211860657D);
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox34.StyleName = "";
            this.textBox34.Value = "PHONE NUMBER";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5D), Telerik.Reporting.Drawing.Unit.Cm(0.84395772218704224D));
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox36.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.079999998211860657D);
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox36.StyleName = "";
            this.textBox36.Value = "EMAIL ADDRESS";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5D), Telerik.Reporting.Drawing.Unit.Cm(0.81750017404556274D));
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "=Fields.ProjectNumber";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5D), Telerik.Reporting.Drawing.Unit.Cm(0.76458364725112915D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.StyleName = "";
            this.textBox2.Value = "=Fields.ContFirst +\" \" + Fields.ContLast";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5D), Telerik.Reporting.Drawing.Unit.Cm(0.79104173183441162D));
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.StyleName = "";
            this.textBox7.Value = "=Fields.ContPhone";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.5D), Telerik.Reporting.Drawing.Unit.Cm(0.764583170413971D));
            this.textBox8.Style.Font.Bold = true;
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.StyleName = "";
            this.textBox8.Value = "=Fields.ContEmail";
            // 
            // tblsales
            // 
            this.tblsales.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.8527083396911621D)));
            this.tblsales.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.2143745422363281D)));
            this.tblsales.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.55291754007339478D)));
            this.tblsales.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.91248321533203125D)));
            this.tblsales.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.73812419176101685D)));
            this.tblsales.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.81408315896987915D)));
            this.tblsales.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.84395802021026611D)));
            this.tblsales.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.86093127727508545D)));
            this.tblsales.Body.SetCellContent(1, 0, this.textBox37);
            this.tblsales.Body.SetCellContent(0, 0, this.textBox1);
            this.tblsales.Body.SetCellContent(3, 0, this.textBox3);
            this.tblsales.Body.SetCellContent(5, 0, this.textBox4);
            this.tblsales.Body.SetCellContent(2, 0, this.textBox9);
            this.tblsales.Body.SetCellContent(4, 0, this.textBox10);
            this.tblsales.Body.SetCellContent(6, 0, this.textBox11);
            tableGroup11.Name = "tableGroup";
            this.tblsales.ColumnGroups.Add(tableGroup11);
            this.tblsales.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.textBox37,
            this.textBox9,
            this.textBox3,
            this.textBox10,
            this.textBox4,
            this.textBox11});
            this.tblsales.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.948540687561035D), Telerik.Reporting.Drawing.Unit.Cm(20.200000762939453D));
            this.tblsales.Name = "tblsales";
            tableGroup13.Name = "group23";
            tableGroup14.Name = "group22";
            tableGroup15.Name = "group21";
            tableGroup16.Name = "group24";
            tableGroup12.ChildGroups.Add(tableGroup13);
            tableGroup12.ChildGroups.Add(tableGroup14);
            tableGroup12.ChildGroups.Add(tableGroup15);
            tableGroup12.ChildGroups.Add(tableGroup16);
            tableGroup12.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup12.Name = "detailTableGroup";
            tableGroup17.Name = "group";
            tableGroup18.Name = "group25";
            tableGroup19.Name = "group1";
            this.tblsales.RowGroups.Add(tableGroup12);
            this.tblsales.RowGroups.Add(tableGroup17);
            this.tblsales.RowGroups.Add(tableGroup18);
            this.tblsales.RowGroups.Add(tableGroup19);
            this.tblsales.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8527083396911621D), Telerik.Reporting.Drawing.Unit.Cm(5.9368720054626465D));
            this.tblsales.Style.Color = System.Drawing.Color.White;
            this.tblsales.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8527083396911621D), Telerik.Reporting.Drawing.Unit.Cm(0.55291736125946045D));
            this.textBox37.Style.Color = System.Drawing.Color.White;
            this.textBox37.Style.Font.Bold = false;
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox37.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.079999998211860657D);
            this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox37.StyleName = "";
            this.textBox37.Value = "NAME";
            // 
            // textBox1
            // 
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8527083396911621D), Telerik.Reporting.Drawing.Unit.Cm(1.2143750190734863D));
            this.textBox1.Style.Color = System.Drawing.Color.White;
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(22D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.StyleName = "";
            this.textBox1.Value = "SALES PERSON";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8527083396911621D), Telerik.Reporting.Drawing.Unit.Cm(0.73812419176101685D));
            this.textBox3.Style.Color = System.Drawing.Color.White;
            this.textBox3.Style.Font.Bold = false;
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox3.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.079999998211860657D);
            this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox3.StyleName = "";
            this.textBox3.Value = "EMAIL";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8527083396911621D), Telerik.Reporting.Drawing.Unit.Cm(0.84395813941955566D));
            this.textBox4.Style.Font.Bold = false;
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox4.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.079999998211860657D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox4.StyleName = "";
            this.textBox4.Value = "PHONE";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8527083396911621D), Telerik.Reporting.Drawing.Unit.Cm(0.91248339414596558D));
            this.textBox9.Style.Color = System.Drawing.Color.White;
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "=Fields.SalesRepName";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8527083396911621D), Telerik.Reporting.Drawing.Unit.Cm(0.81408309936523438D));
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.StyleName = "";
            this.textBox10.Value = "=Fields.SalesRepEmail";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8527083396911621D), Telerik.Reporting.Drawing.Unit.Cm(0.86093127727508545D));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.StyleName = "";
            this.textBox11.Value = "=Fields.SalesRepNo";
            // 
            // htmlTextBox7
            // 
            this.htmlTextBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.1338586807250977D), Telerik.Reporting.Drawing.Unit.Inch(17.401575088500977D));
            this.htmlTextBox7.Name = "htmlTextBox7";
            this.htmlTextBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.7625000476837158D), Telerik.Reporting.Drawing.Unit.Inch(4.055117130279541D));
            this.htmlTextBox7.Style.Font.Name = "Calibri";
            this.htmlTextBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.htmlTextBox7.Value = resources.GetString("htmlTextBox7.Value");
            // 
            // pictureBox7
            // 
            this.pictureBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.80000114440918D), Telerik.Reporting.Drawing.Unit.Cm(44.300003051757812D));
            this.pictureBox7.MimeType = "image/jpeg";
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D), Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D));
            this.pictureBox7.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox7.Value = ((object)(resources.GetObject("pictureBox7.Value")));
            // 
            // pictureBox8
            // 
            this.pictureBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.80000114440918D), Telerik.Reporting.Drawing.Unit.Cm(46.100002288818359D));
            this.pictureBox8.MimeType = "image/jpeg";
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D), Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D));
            this.pictureBox8.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox8.Value = ((object)(resources.GetObject("pictureBox8.Value")));
            // 
            // pictureBox9
            // 
            this.pictureBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.80000114440918D), Telerik.Reporting.Drawing.Unit.Cm(47.700000762939453D));
            this.pictureBox9.MimeType = "image/jpeg";
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D), Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D));
            this.pictureBox9.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox9.Value = ((object)(resources.GetObject("pictureBox9.Value")));
            // 
            // pictureBox10
            // 
            this.pictureBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.9000015258789062D), Telerik.Reporting.Drawing.Unit.Cm(51.299999237060547D));
            this.pictureBox10.MimeType = "image/jpeg";
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D), Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D));
            this.pictureBox10.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox10.Value = ((object)(resources.GetObject("pictureBox10.Value")));
            // 
            // pictureBox11
            // 
            this.pictureBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.9000015258789062D), Telerik.Reporting.Drawing.Unit.Cm(49.700000762939453D));
            this.pictureBox11.MimeType = "image/jpeg";
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D), Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D));
            this.pictureBox11.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox11.Value = ((object)(resources.GetObject("pictureBox11.Value")));
            // 
            // pictureBox12
            // 
            this.pictureBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.9000015258789062D), Telerik.Reporting.Drawing.Unit.Cm(53.5D));
            this.pictureBox12.MimeType = "image/jpeg";
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D), Telerik.Reporting.Drawing.Unit.Cm(0.30000001192092896D));
            this.pictureBox12.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox12.Value = ((object)(resources.GetObject("pictureBox12.Value")));
            // 
            // tblsitedetail
            // 
            this.tblsitedetail.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.6000003814697266D)));
            this.tblsitedetail.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60001063346862793D)));
            this.tblsitedetail.Body.SetCellContent(0, 0, this.textBox12);
            tableGroup20.Name = "tableGroup12";
            this.tblsitedetail.ColumnGroups.Add(tableGroup20);
            this.tblsitedetail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox12});
            this.tblsitedetail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(64.5D));
            this.tblsitedetail.Name = "tblsitedetail";
            tableGroup22.Name = "group26";
            tableGroup21.ChildGroups.Add(tableGroup22);
            tableGroup21.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup21.Name = "detailTableGroup5";
            this.tblsitedetail.RowGroups.Add(tableGroup21);
            this.tblsitedetail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.6000003814697266D), Telerik.Reporting.Drawing.Unit.Cm(0.60001063346862793D));
            this.tblsitedetail.Style.Font.Name = "Calibri";
            this.tblsitedetail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.tblsitedetail.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.6000003814697266D), Telerik.Reporting.Drawing.Unit.Cm(0.60001063346862793D));
            this.textBox12.Style.Color = System.Drawing.Color.White;
            this.textBox12.Style.Font.Name = "Calibri";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.textBox12.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox12.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.Value = "=TrimStart(Fields.PostalAddress) +\",\"+TrimStart(Fields.PostalCity) +\", \"+TrimStar" +
    "t(Fields.PostalState)+\", \"+TrimStart(Fields.PostalPostCode)";
            // 
            // txtProjectNumber
            // 
            this.txtProjectNumber.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.5D), Telerik.Reporting.Drawing.Unit.Cm(64.5D));
            this.txtProjectNumber.Name = "txtProjectNumber";
            this.txtProjectNumber.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9000015258789062D), Telerik.Reporting.Drawing.Unit.Cm(0.600002110004425D));
            this.txtProjectNumber.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(54)))), ((int)(((byte)(93)))));
            this.txtProjectNumber.Style.Font.Bold = true;
            this.txtProjectNumber.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(15D);
            this.txtProjectNumber.Value = "=Fields.ProjectNumber";
            // 
            // tblsitedetails1
            // 
            this.tblsitedetails1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.756528377532959D)));
            this.tblsitedetails1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.67264759540557861D)));
            this.tblsitedetails1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.57858401536941528D)));
            this.tblsitedetails1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.52849036455154419D)));
            this.tblsitedetails1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.62027996778488159D)));
            this.tblsitedetails1.Body.SetCellContent(0, 0, this.textBox13);
            this.tblsitedetails1.Body.SetCellContent(1, 0, this.textBox14);
            this.tblsitedetails1.Body.SetCellContent(2, 0, this.txtmeter);
            this.tblsitedetails1.Body.SetCellContent(3, 0, this.txtswitch);
            tableGroup23.Name = "tableGroup";
            this.tblsitedetails1.ColumnGroups.Add(tableGroup23);
            this.tblsitedetails1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox13,
            this.textBox14,
            this.txtmeter,
            this.txtswitch});
            this.tblsitedetails1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.7999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(65.5999984741211D));
            this.tblsitedetails1.Name = "tblsitedetails1";
            tableGroup24.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup24.Name = "detailTableGroup";
            tableGroup25.Name = "group";
            tableGroup26.Name = "group1";
            tableGroup27.Name = "group82";
            this.tblsitedetails1.RowGroups.Add(tableGroup24);
            this.tblsitedetails1.RowGroups.Add(tableGroup25);
            this.tblsitedetails1.RowGroups.Add(tableGroup26);
            this.tblsitedetails1.RowGroups.Add(tableGroup27);
            this.tblsitedetails1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.756528377532959D), Telerik.Reporting.Drawing.Unit.Cm(2.4000020027160645D));
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.756528377532959D), Telerik.Reporting.Drawing.Unit.Cm(0.67264765501022339D));
            this.textBox13.Style.Color = System.Drawing.Color.Black;
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox13.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.Value = "=Fields.ElecRetailer";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.756528377532959D), Telerik.Reporting.Drawing.Unit.Cm(0.57858407497406006D));
            this.textBox14.Style.Color = System.Drawing.Color.Black;
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox14.StyleName = "";
            this.textBox14.Value = "=Fields.ElecDistributor";
            // 
            // txtmeter
            // 
            this.txtmeter.Name = "txtmeter";
            this.txtmeter.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.756528377532959D), Telerik.Reporting.Drawing.Unit.Cm(0.528490424156189D));
            this.txtmeter.Style.Color = System.Drawing.Color.Black;
            this.txtmeter.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.txtmeter.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtmeter.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtmeter.StyleName = "";
            this.txtmeter.Value = "";
            // 
            // txtswitch
            // 
            this.txtswitch.Name = "txtswitch";
            this.txtswitch.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.756528377532959D), Telerik.Reporting.Drawing.Unit.Cm(0.62027996778488159D));
            this.txtswitch.Style.Color = System.Drawing.Color.Black;
            this.txtswitch.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.txtswitch.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtswitch.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtswitch.StyleName = "";
            // 
            // tblsitedetails2
            // 
            this.tblsitedetails2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.8000006675720215D)));
            this.tblsitedetails2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.73201286792755127D)));
            this.tblsitedetails2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.55291581153869629D)));
            this.tblsitedetails2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.65875035524368286D)));
            this.tblsitedetails2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.57937586307525635D)));
            this.tblsitedetails2.Body.SetCellContent(0, 0, this.txtmeterphase);
            this.tblsitedetails2.Body.SetCellContent(1, 0, this.textBox15);
            this.tblsitedetails2.Body.SetCellContent(2, 0, this.textBox16);
            this.tblsitedetails2.Body.SetCellContent(3, 0, this.textBox17);
            tableGroup28.Name = "tableGroup";
            this.tblsitedetails2.ColumnGroups.Add(tableGroup28);
            this.tblsitedetails2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtmeterphase,
            this.textBox15,
            this.textBox16,
            this.textBox17});
            this.tblsitedetails2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.59999942779541D), Telerik.Reporting.Drawing.Unit.Cm(65.5999984741211D));
            this.tblsitedetails2.Name = "tblsitedetails2";
            tableGroup29.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup29.Name = "detailTableGroup";
            tableGroup30.Name = "group";
            tableGroup31.Name = "group1";
            tableGroup32.Name = "group2";
            this.tblsitedetails2.RowGroups.Add(tableGroup29);
            this.tblsitedetails2.RowGroups.Add(tableGroup30);
            this.tblsitedetails2.RowGroups.Add(tableGroup31);
            this.tblsitedetails2.RowGroups.Add(tableGroup32);
            this.tblsitedetails2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8000006675720215D), Telerik.Reporting.Drawing.Unit.Cm(2.523054838180542D));
            this.tblsitedetails2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // txtmeterphase
            // 
            this.txtmeterphase.Name = "txtmeterphase";
            this.txtmeterphase.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8000006675720215D), Telerik.Reporting.Drawing.Unit.Cm(0.732012927532196D));
            this.txtmeterphase.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.txtmeterphase.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtmeterphase.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.txtmeterphase.Value = "";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8000006675720215D), Telerik.Reporting.Drawing.Unit.Cm(0.55291581153869629D));
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox15.StyleName = "";
            this.textBox15.Value = "=Fields.RoofType";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8000006675720215D), Telerik.Reporting.Drawing.Unit.Cm(0.65875035524368286D));
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox16.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox16.StyleName = "";
            this.textBox16.Value = "=Fields.RoofAngle";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8000006675720215D), Telerik.Reporting.Drawing.Unit.Cm(0.57937586307525635D));
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox17.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox17.StyleName = "";
            this.textBox17.Value = "=Fields.HouseType";
            // 
            // tblsysinsdetail
            // 
            this.tblsysinsdetail.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(9.72925853729248D)));
            this.tblsysinsdetail.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.268748939037323D)));
            this.tblsysinsdetail.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24097083508968353D)));
            this.tblsysinsdetail.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.407637357711792D)));
            this.tblsysinsdetail.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.29999846220016479D)));
            this.tblsysinsdetail.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.4904887676239014D)));
            this.tblsysinsdetail.Body.SetCellContent(4, 0, this.textBox18);
            this.tblsysinsdetail.Body.SetCellContent(0, 0, this.textBox19);
            this.tblsysinsdetail.Body.SetCellContent(2, 0, this.txtInverterdetail1);
            this.tblsysinsdetail.Body.SetCellContent(1, 0, this.textBox21);
            this.tblsysinsdetail.Body.SetCellContent(3, 0, this.textBox23);
            tableGroup33.Name = "tableGroup1";
            this.tblsysinsdetail.ColumnGroups.Add(tableGroup33);
            this.tblsysinsdetail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox19,
            this.textBox21,
            this.txtInverterdetail1,
            this.textBox23,
            this.textBox18});
            this.tblsysinsdetail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(70.699996948242188D));
            this.tblsysinsdetail.Name = "tblsysinsdetail";
            tableGroup34.Name = "group6";
            tableGroup35.Name = "group10";
            tableGroup36.Name = "group8";
            tableGroup37.Name = "group12";
            tableGroup38.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup38.Name = "detailTableGroup1";
            this.tblsysinsdetail.RowGroups.Add(tableGroup34);
            this.tblsysinsdetail.RowGroups.Add(tableGroup35);
            this.tblsysinsdetail.RowGroups.Add(tableGroup36);
            this.tblsysinsdetail.RowGroups.Add(tableGroup37);
            this.tblsysinsdetail.RowGroups.Add(tableGroup38);
            this.tblsysinsdetail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.72925853729248D), Telerik.Reporting.Drawing.Unit.Cm(4.5825719833374023D));
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.72925853729248D), Telerik.Reporting.Drawing.Unit.Cm(1.4904887676239014D));
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox18.Value = "- Installation by CEC Accredited Solar Installer\r\n- CEC Approved, Mounts, & Brack" +
    "ets\r\n- Standard Admin. & Grid-Connection";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.72925853729248D), Telerik.Reporting.Drawing.Unit.Inch(0.268748939037323D));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox19.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox19.StyleName = "";
            this.textBox19.Value = "=Fields.SystemCapKW +\" KW\" ";
            // 
            // txtInverterdetail1
            // 
            this.txtInverterdetail1.Name = "txtInverterdetail1";
            this.txtInverterdetail1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.72925853729248D), Telerik.Reporting.Drawing.Unit.Inch(0.407637357711792D));
            this.txtInverterdetail1.Style.Font.Bold = false;
            this.txtInverterdetail1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.txtInverterdetail1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtInverterdetail1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtInverterdetail1.StyleName = "";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.72925853729248D), Telerik.Reporting.Drawing.Unit.Inch(0.24097083508968353D));
            this.textBox21.Style.Font.Bold = false;
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox21.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox21.StyleName = "";
            this.textBox21.Value = "=Fields.PanelDetails";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.72925853729248D), Telerik.Reporting.Drawing.Unit.Inch(0.29999846220016479D));
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.textBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.StyleName = "";
            this.textBox23.Value = "..... Through SolarMiner";
            // 
            // txtotherdetail
            // 
            this.txtotherdetail.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.529458045959473D), Telerik.Reporting.Drawing.Unit.Cm(70.60009765625D));
            this.txtotherdetail.Name = "txtotherdetail";
            this.txtotherdetail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8705434799194336D), Telerik.Reporting.Drawing.Unit.Cm(3.3999974727630615D));
            this.txtotherdetail.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(70)))), ((int)(((byte)(110)))));
            this.txtotherdetail.Style.Font.Bold = false;
            this.txtotherdetail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(14D);
            this.txtotherdetail.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.txtotherdetail.StyleName = "";
            // 
            // table11
            // 
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.1536655426025391D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.82554370164871216D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.69147229194641113D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.63784360885620117D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.95961529016494751D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5870630145072937D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.79623764753341675D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.7719152569770813D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.798729419708252D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.7719150185585022D)));
            this.table11.Body.SetCellContent(0, 0, this.txtreq);
            this.table11.Body.SetCellContent(1, 0, this.txtcompl);
            this.table11.Body.SetCellContent(2, 0, this.txtextra);
            this.table11.Body.SetCellContent(3, 0, this.txtfullretail);
            this.table11.Body.SetCellContent(4, 0, this.txtdiscount2);
            this.table11.Body.SetCellContent(5, 0, this.txtstc);
            this.table11.Body.SetCellContent(6, 0, this.txtgst);
            this.table11.Body.SetCellContent(7, 0, this.txtdeposit);
            this.table11.Body.SetCellContent(8, 0, this.txtlessdeposit);
            tableGroup39.Name = "tableGroup9";
            tableGroup39.ReportItem = this.txtbasiccost;
            this.table11.ColumnGroups.Add(tableGroup39);
            this.table11.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.txtreq,
            this.txtcompl,
            this.txtextra,
            this.txtfullretail,
            this.txtdiscount2,
            this.txtstc,
            this.txtgst,
            this.txtdeposit,
            this.txtlessdeposit,
            this.txtbasiccost});
            this.table11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.4000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(80.5D));
            this.table11.Name = "table11";
            tableGroup41.Name = "group31";
            tableGroup42.Name = "group37";
            tableGroup43.Name = "group38";
            tableGroup44.Name = "group32";
            tableGroup45.Name = "group33";
            tableGroup46.Name = "group36";
            tableGroup47.Name = "group35";
            tableGroup48.Name = "group34";
            tableGroup49.Name = "group39";
            tableGroup40.ChildGroups.Add(tableGroup41);
            tableGroup40.ChildGroups.Add(tableGroup42);
            tableGroup40.ChildGroups.Add(tableGroup43);
            tableGroup40.ChildGroups.Add(tableGroup44);
            tableGroup40.ChildGroups.Add(tableGroup45);
            tableGroup40.ChildGroups.Add(tableGroup46);
            tableGroup40.ChildGroups.Add(tableGroup47);
            tableGroup40.ChildGroups.Add(tableGroup48);
            tableGroup40.ChildGroups.Add(tableGroup49);
            tableGroup40.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup40.Name = "detailTableGroup7";
            this.table11.RowGroups.Add(tableGroup40);
            this.table11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1536655426025391D), Telerik.Reporting.Drawing.Unit.Cm(7.5049934387207031D));
            // 
            // txtreq
            // 
            this.txtreq.Name = "txtreq";
            this.txtreq.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1536650657653809D), Telerik.Reporting.Drawing.Unit.Cm(0.82554388046264648D));
            this.txtreq.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtreq.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.05000000074505806D);
            this.txtreq.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtreq.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtreq.Value = "=Fields.lblreq";
            // 
            // txtcompl
            // 
            this.txtcompl.Name = "txtcompl";
            this.txtcompl.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1536650657653809D), Telerik.Reporting.Drawing.Unit.Cm(0.69147217273712158D));
            this.txtcompl.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtcompl.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtcompl.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtcompl.Value = "=Fields.lblcompl";
            // 
            // txtextra
            // 
            this.txtextra.Name = "txtextra";
            this.txtextra.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1536650657653809D), Telerik.Reporting.Drawing.Unit.Cm(0.637843668460846D));
            this.txtextra.Style.BorderColor.Bottom = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(55)))), ((int)(((byte)(94)))));
            this.txtextra.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.txtextra.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(2D);
            this.txtextra.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtextra.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtextra.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtextra.Value = "=Fields.lblextra";
            // 
            // txtfullretail
            // 
            this.txtfullretail.Name = "txtfullretail";
            this.txtfullretail.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1536650657653809D), Telerik.Reporting.Drawing.Unit.Cm(0.959615170955658D));
            this.txtfullretail.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtfullretail.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D);
            this.txtfullretail.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtfullretail.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtfullretail.Value = "=Fields.lblfullretail";
            // 
            // txtdiscount2
            // 
            this.txtdiscount2.Name = "txtdiscount2";
            this.txtdiscount2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1536655426025391D), Telerik.Reporting.Drawing.Unit.Cm(0.5870630145072937D));
            this.txtdiscount2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtdiscount2.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.txtdiscount2.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(-0.10000000149011612D);
            this.txtdiscount2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtdiscount2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtdiscount2.Value = "=Fields.lbldiscount2";
            // 
            // txtstc
            // 
            this.txtstc.Name = "txtstc";
            this.txtstc.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1536655426025391D), Telerik.Reporting.Drawing.Unit.Cm(0.79623764753341675D));
            this.txtstc.Style.BorderColor.Default = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(55)))), ((int)(((byte)(94)))));
            this.txtstc.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.txtstc.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.txtstc.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.txtstc.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.txtstc.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.txtstc.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtstc.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtstc.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtstc.Value = "=Fields.lblstc";
            // 
            // txtgst
            // 
            this.txtgst.Name = "txtgst";
            this.txtgst.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1536650657653809D), Telerik.Reporting.Drawing.Unit.Cm(0.77191537618637085D));
            this.txtgst.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtgst.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtgst.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtgst.Value = "=Fields.lblgst";
            // 
            // txtdeposit
            // 
            this.txtdeposit.Name = "txtdeposit";
            this.txtdeposit.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1536650657653809D), Telerik.Reporting.Drawing.Unit.Cm(0.79872971773147583D));
            this.txtdeposit.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtdeposit.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtdeposit.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtdeposit.Value = "=Fields.lbldeposit";
            // 
            // txtlessdeposit
            // 
            this.txtlessdeposit.Name = "txtlessdeposit";
            this.txtlessdeposit.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1536650657653809D), Telerik.Reporting.Drawing.Unit.Cm(0.77191519737243652D));
            this.txtlessdeposit.Style.BorderColor.Bottom = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(55)))), ((int)(((byte)(94)))));
            this.txtlessdeposit.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.txtlessdeposit.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(2D);
            this.txtlessdeposit.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(13D);
            this.txtlessdeposit.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.txtlessdeposit.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.txtlessdeposit.Value = "=Fields.lbllessdeposit";
            // 
            // table9
            // 
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.8948695659637451D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.74686682224273682D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.67285853624343872D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.91724485158920288D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.6011584997177124D)));
            this.table9.Body.SetCellContent(0, 0, this.textBox59);
            this.table9.Body.SetCellContent(3, 0, this.textBox60);
            this.table9.Body.SetCellContent(1, 0, this.textBox62);
            this.table9.Body.SetCellContent(2, 0, this.textBox64);
            tableGroup50.Name = "tableGroup6";
            this.table9.ColumnGroups.Add(tableGroup50);
            this.table9.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox59,
            this.textBox62,
            this.textBox64,
            this.textBox60});
            this.table9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15D), Telerik.Reporting.Drawing.Unit.Cm(81.699996948242188D));
            this.table9.Name = "table9";
            tableGroup52.Name = "group21";
            tableGroup53.Name = "group23";
            tableGroup54.Name = "group24";
            tableGroup55.Name = "group22";
            tableGroup51.ChildGroups.Add(tableGroup52);
            tableGroup51.ChildGroups.Add(tableGroup53);
            tableGroup51.ChildGroups.Add(tableGroup54);
            tableGroup51.ChildGroups.Add(tableGroup55);
            tableGroup51.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup51.Name = "detailTableGroup4";
            this.table9.RowGroups.Add(tableGroup51);
            this.table9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8948695659637451D), Telerik.Reporting.Drawing.Unit.Cm(2.9381287097930908D));
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8948695659637451D), Telerik.Reporting.Drawing.Unit.Cm(0.74686682224273682D));
            this.textBox59.Style.Font.Bold = true;
            this.textBox59.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox59.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox59.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox59.Value = "Westpac";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8948695659637451D), Telerik.Reporting.Drawing.Unit.Cm(0.6011584997177124D));
            this.textBox60.Style.Font.Bold = true;
            this.textBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox60.StyleName = "";
            this.textBox60.Value = "SOLAR MINER";
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8948695659637451D), Telerik.Reporting.Drawing.Unit.Cm(0.67285853624343872D));
            this.textBox62.Style.Font.Bold = true;
            this.textBox62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox62.StyleName = "";
            this.textBox62.Value = "034 115";
            // 
            // textBox64
            // 
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8948695659637451D), Telerik.Reporting.Drawing.Unit.Cm(0.91724485158920288D));
            this.textBox64.Style.Font.Bold = true;
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox64.StyleName = "";
            this.textBox64.Value = "19634";
            // 
            // table10
            // 
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.22937536239624D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.06916618347168D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.4050033092498779D)));
            this.table10.Body.SetCellContent(0, 0, this.textBox66);
            this.table10.Body.SetCellContent(0, 1, this.textBox20);
            tableGroup56.Name = "tableGroup7";
            tableGroup57.Name = "group3";
            this.table10.ColumnGroups.Add(tableGroup56);
            this.table10.ColumnGroups.Add(tableGroup57);
            this.table10.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox66,
            this.textBox20});
            this.table10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.300000190734863D), Telerik.Reporting.Drawing.Unit.Cm(86.200004577636719D));
            this.table10.Name = "table10";
            tableGroup59.Name = "group25";
            tableGroup58.ChildGroups.Add(tableGroup59);
            tableGroup58.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup58.Name = "detailTableGroup6";
            this.table10.RowGroups.Add(tableGroup58);
            this.table10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.2985410690307617D), Telerik.Reporting.Drawing.Unit.Cm(1.4050033092498779D));
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.22937536239624D), Telerik.Reporting.Drawing.Unit.Cm(1.4050033092498779D));
            this.textBox66.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox66.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox66.Value = "(Master / Visa cards)\r\nwith Applicable\r\nSurcharge";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.06916618347168D), Telerik.Reporting.Drawing.Unit.Cm(1.4050033092498779D));
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox20.StyleName = "";
            this.textBox20.Value = "SOLAR MINER\r\nPO Box 1011\r\nALTONA GATE VIC 3025";
            // 
            // textBox83
            // 
            this.textBox83.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.2047247886657715D), Telerik.Reporting.Drawing.Unit.Inch(66.74639892578125D));
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8188974857330322D), Telerik.Reporting.Drawing.Unit.Inch(0.37163713574409485D));
            this.textBox83.Style.Color = System.Drawing.Color.DodgerBlue;
            this.textBox83.Style.Font.Bold = true;
            this.textBox83.Style.Font.Name = "Calibri";
            this.textBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(25D);
            this.textBox83.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox83.Value = "1300 285 885";
            // 
            // textBox81
            // 
            this.textBox81.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.2047247886657715D), Telerik.Reporting.Drawing.Unit.Inch(67.204727172851562D));
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.8188974857330322D), Telerik.Reporting.Drawing.Unit.Inch(0.26747047901153564D));
            this.textBox81.Style.Font.Name = "Calibri";
            this.textBox81.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox81.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox81.Value = "info@solarminer.com.au";
            // 
            // txtprojectnote
            // 
            this.txtprojectnote.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6D), Telerik.Reporting.Drawing.Unit.Cm(76.5999984741211D));
            this.txtprojectnote.Name = "txtprojectnote";
            this.txtprojectnote.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.400002479553223D), Telerik.Reporting.Drawing.Unit.Cm(1.6999995708465576D));
            this.txtprojectnote.Style.Color = System.Drawing.Color.White;
            this.txtprojectnote.Value = "";
            // 
            // Welcomeletternew
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "Welcomeletternew";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.PictureBox pictureBox2;
        private Telerik.Reporting.PictureBox pictureBox3;
        private Telerik.Reporting.PictureBox pictureBox4;
        private Telerik.Reporting.PictureBox pictureBox5;
        private Telerik.Reporting.PictureBox pictureBox6;
        private Telerik.Reporting.TextBox textBox191;
        private Telerik.Reporting.Table tblcustomer;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.Table tblsales;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.HtmlTextBox htmlTextBox7;
        private Telerik.Reporting.PictureBox pictureBox7;
        private Telerik.Reporting.PictureBox pictureBox8;
        private Telerik.Reporting.PictureBox pictureBox9;
        private Telerik.Reporting.PictureBox pictureBox10;
        private Telerik.Reporting.PictureBox pictureBox11;
        private Telerik.Reporting.PictureBox pictureBox12;
        private Telerik.Reporting.Table tblsitedetail;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox txtProjectNumber;
        private Telerik.Reporting.Table tblsitedetails1;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox txtmeter;
        private Telerik.Reporting.TextBox txtswitch;
        private Telerik.Reporting.Table tblsitedetails2;
        private Telerik.Reporting.TextBox txtmeterphase;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.Table tblsysinsdetail;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox txtInverterdetail1;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox txtotherdetail;
        private Telerik.Reporting.Table table11;
        private Telerik.Reporting.TextBox txtreq;
        private Telerik.Reporting.TextBox txtcompl;
        private Telerik.Reporting.TextBox txtextra;
        private Telerik.Reporting.TextBox txtfullretail;
        private Telerik.Reporting.TextBox txtdiscount2;
        private Telerik.Reporting.TextBox txtstc;
        private Telerik.Reporting.TextBox txtgst;
        private Telerik.Reporting.TextBox txtdeposit;
        private Telerik.Reporting.TextBox txtlessdeposit;
        private Telerik.Reporting.TextBox txtbasiccost;
        private Telerik.Reporting.Table table9;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.Table table10;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.TextBox txtprojectnote;
    }
}