namespace EurosolarReporting
{
    partial class SolarMinerCustomerAc1
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SolarMinerCustomerAc1));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.pictureBox5 = new Telerik.Reporting.PictureBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.pictureBox12 = new Telerik.Reporting.PictureBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textB = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.pictureBox3 = new Telerik.Reporting.PictureBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.pictureBox4 = new Telerik.Reporting.PictureBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.pictureBox6 = new Telerik.Reporting.PictureBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.pictureBox7 = new Telerik.Reporting.PictureBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.pictureBox8 = new Telerik.Reporting.PictureBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.pictureBox9 = new Telerik.Reporting.PictureBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.pictureBox10 = new Telerik.Reporting.PictureBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.pictureBox11 = new Telerik.Reporting.PictureBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.pictureBox13 = new Telerik.Reporting.PictureBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.pictureBox14 = new Telerik.Reporting.PictureBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.table7 = new Telerik.Reporting.Table();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.table8 = new Telerik.Reporting.Table();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox = new Telerik.Reporting.TextBox();
            this.table9 = new Telerik.Reporting.Table();
            this.htmlTextBox1 = new Telerik.Reporting.HtmlTextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pictureBox5
            // 
            this.pictureBox5.MimeType = "image/jpeg";
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.34395879507064819D), Telerik.Reporting.Drawing.Unit.Cm(0.66145819425582886D));
            this.pictureBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox5.StyleName = "";
            this.pictureBox5.Value = ((object)(resources.GetObject("pictureBox5.Value")));
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.330209732055664D), Telerik.Reporting.Drawing.Unit.Cm(0.66145825386047363D));
            this.textBox10.Style.Font.Name = "Calibri";
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox10.Value = "I am completely satisfied with the Solar Power system I have been provided with, " +
    "including (but not limited to) the installation, positioning and location of the" +
    " solar\r\npanels and inverters";
            // 
            // pictureBox12
            // 
            this.pictureBox12.MimeType = "image/jpeg";
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.37041729688644409D), Telerik.Reporting.Drawing.Unit.Cm(0.42333334684371948D));
            this.pictureBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox12.StyleName = "";
            this.pictureBox12.Value = ((object)(resources.GetObject("pictureBox12.Value")));
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.478126525878906D), Telerik.Reporting.Drawing.Unit.Cm(0.42333331704139709D));
            this.textBox16.Style.Font.Name = "Calibri";
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox16.Value = "I am the owner of the property or dwelling to which the goods are being installed" +
    "";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.47354164719581604D));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Name = "Calibri";
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.StyleName = "";
            this.textBox19.Value = "3";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.473541796207428D));
            this.textBox20.Style.Font.Name = "Calibri";
            this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.StyleName = "";
            this.textBox20.Value = "2";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.4497915506362915D));
            this.textBox22.Style.Font.Name = "Calibri";
            this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox22.Value = "1";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.47354164719581604D));
            this.textBox29.Style.Font.Bold = true;
            this.textBox29.Style.Font.Name = "Calibri";
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.StyleName = "";
            this.textBox29.Value = "3";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.473541796207428D));
            this.textBox30.Style.Font.Name = "Calibri";
            this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox30.StyleName = "";
            this.textBox30.Value = "2";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.4497915506362915D));
            this.textBox31.Style.Font.Name = "Calibri";
            this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox31.Value = "1";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.47354164719581604D));
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.Font.Name = "Calibri";
            this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox34.StyleName = "";
            this.textBox34.Value = "3";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.473541796207428D));
            this.textBox35.Style.Font.Name = "Calibri";
            this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox35.StyleName = "";
            this.textBox35.Value = "2";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.4497915506362915D));
            this.textBox36.Style.Font.Name = "Calibri";
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox36.Value = "1";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.47354164719581604D));
            this.textBox38.Style.Font.Bold = true;
            this.textBox38.Style.Font.Name = "Calibri";
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.StyleName = "";
            this.textBox38.Value = "3";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.473541796207428D));
            this.textBox39.Style.Font.Name = "Calibri";
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.StyleName = "";
            this.textBox39.Value = "2";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.4497915506362915D));
            this.textBox40.Style.Font.Name = "Calibri";
            this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox40.Value = "1";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.47354164719581604D));
            this.textBox42.Style.Font.Bold = true;
            this.textBox42.Style.Font.Name = "Calibri";
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox42.StyleName = "";
            this.textBox42.Value = "3";
            // 
            // textB
            // 
            this.textB.Name = "textB";
            this.textB.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.473541796207428D));
            this.textB.Style.Font.Name = "Calibri";
            this.textB.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textB.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textB.StyleName = "";
            this.textB.Value = "2";
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.4497915506362915D));
            this.textBox46.Style.Font.Name = "Calibri";
            this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox46.Value = "1";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(297D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.textBox1,
            this.textBox2,
            this.table1,
            this.textBox32,
            this.table2,
            this.textBox5,
            this.table3,
            this.textBox43,
            this.textBox45,
            this.textBox3,
            this.textBox28,
            this.textBox25,
            this.textBox24,
            this.textBox23,
            this.textBox21,
            this.textBox27,
            this.table4,
            this.table5,
            this.table6,
            this.table7,
            this.table8,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50,
            this.textBox,
            this.table9});
            this.detail.Name = "detail";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(296D));
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.8000001907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.99989998340606689D));
            this.textBox1.Style.Color = System.Drawing.Color.White;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(25D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "Customer";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.30000004172325134D), Telerik.Reporting.Drawing.Unit.Cm(1.0001997947692871D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.699999809265137D), Telerik.Reporting.Drawing.Unit.Cm(0.89980030059814453D));
            this.textBox2.Style.Color = System.Drawing.Color.White;
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(25D);
            this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox2.Value = "Acknowledgement Form";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.39687570929527283D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(17.19792366027832D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.34395846724510193D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.34125006198883057D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.34125009179115295D)));
            this.table1.Body.SetCellContent(0, 1, this.textBox6);
            this.table1.Body.SetCellContent(0, 0, this.pictureBox2);
            this.table1.Body.SetCellContent(1, 1, this.textBox4);
            this.table1.Body.SetCellContent(1, 0, this.pictureBox3);
            this.table1.Body.SetCellContent(2, 1, this.textBox8);
            this.table1.Body.SetCellContent(2, 0, this.pictureBox4);
            tableGroup1.Name = "group";
            tableGroup2.Name = "tableGroup1";
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox2,
            this.textBox6,
            this.pictureBox3,
            this.textBox4,
            this.pictureBox4,
            this.textBox8});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.2000000476837158D), Telerik.Reporting.Drawing.Unit.Cm(4.0999999046325684D));
            this.table1.Name = "table1";
            tableGroup3.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup3.Name = "detailTableGroup";
            tableGroup4.Name = "group1";
            tableGroup5.Name = "group2";
            this.table1.RowGroups.Add(tableGroup3);
            this.table1.RowGroups.Add(tableGroup4);
            this.table1.RowGroups.Add(tableGroup5);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.594799041748047D), Telerik.Reporting.Drawing.Unit.Cm(1.0264586210250855D));
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.197921752929688D), Telerik.Reporting.Drawing.Unit.Cm(0.34395837783813477D));
            this.textBox6.Style.Font.Name = "Calibri";
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox6.Value = "Received a copy of my invoice and User Manuals.";
            // 
            // pictureBox2
            // 
            this.pictureBox2.MimeType = "image/jpeg";
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.39687567949295044D), Telerik.Reporting.Drawing.Unit.Cm(0.34395837783813477D));
            this.pictureBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox2.StyleName = "";
            this.pictureBox2.Value = ((object)(resources.GetObject("pictureBox2.Value")));
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.197917938232422D), Telerik.Reporting.Drawing.Unit.Cm(0.34125006198883057D));
            this.textBox4.Style.Font.Name = "Calibri";
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox4.StyleName = "";
            this.textBox4.Value = "Been made aware by the installer of the best location for my solar panels and inv" +
    "erters to be installed and have agreed with the location of the installation.";
            // 
            // pictureBox3
            // 
            this.pictureBox3.MimeType = "image/jpeg";
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.39687567949295044D), Telerik.Reporting.Drawing.Unit.Cm(0.34125009179115295D));
            this.pictureBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox3.StyleName = "";
            this.pictureBox3.Value = ((object)(resources.GetObject("pictureBox3.Value")));
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.19792366027832D), Telerik.Reporting.Drawing.Unit.Cm(0.34125009179115295D));
            this.textBox8.Style.Font.Name = "Calibri";
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox8.StyleName = "";
            this.textBox8.Value = "Satisfied with the condition that installer has left the premises after completio" +
    "n of the work.";
            // 
            // pictureBox4
            // 
            this.pictureBox4.MimeType = "image/jpeg";
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.39687570929527283D), Telerik.Reporting.Drawing.Unit.Cm(0.34125009179115295D));
            this.pictureBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox4.StyleName = "";
            this.pictureBox4.Value = ((object)(resources.GetObject("pictureBox4.Value")));
            // 
            // textBox32
            // 
            this.textBox32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045D), Telerik.Reporting.Drawing.Unit.Cm(5.5D));
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.674304962158203D), Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269D));
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.Font.Name = "Calibri";
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox32.Value = "As the \"Customer\" I do hereby acknowledge that I have: ";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.34395879507064819D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(17.330209732055664D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.97895830869674683D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table2.Body.SetCellContent(0, 1, this.textBox11);
            this.table2.Body.SetCellContent(0, 0, this.pictureBox6);
            this.table2.Body.SetCellContent(1, 1, this.textBox7);
            this.table2.Body.SetCellContent(1, 0, this.pictureBox7);
            this.table2.Body.SetCellContent(2, 1, this.textBox9);
            this.table2.Body.SetCellContent(2, 0, this.pictureBox8);
            this.table2.Body.SetCellContent(3, 1, this.textBox12);
            this.table2.Body.SetCellContent(3, 0, this.pictureBox9);
            this.table2.Body.SetCellContent(4, 1, this.textBox13);
            this.table2.Body.SetCellContent(4, 0, this.pictureBox10);
            this.table2.Body.SetCellContent(5, 1, this.textBox14);
            this.table2.Body.SetCellContent(5, 0, this.pictureBox11);
            tableGroup6.Name = "tableGroup2";
            tableGroup6.ReportItem = this.pictureBox5;
            tableGroup7.Name = "tableGroup3";
            tableGroup7.ReportItem = this.textBox10;
            this.table2.ColumnGroups.Add(tableGroup6);
            this.table2.ColumnGroups.Add(tableGroup7);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox6,
            this.textBox11,
            this.pictureBox7,
            this.textBox7,
            this.pictureBox8,
            this.textBox9,
            this.pictureBox9,
            this.textBox12,
            this.pictureBox10,
            this.textBox13,
            this.pictureBox11,
            this.textBox14,
            this.pictureBox5,
            this.textBox10});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.2000000476837158D), Telerik.Reporting.Drawing.Unit.Cm(6.2999997138977051D));
            this.table2.Name = "table2";
            tableGroup9.Name = "group3";
            tableGroup10.Name = "group4";
            tableGroup8.ChildGroups.Add(tableGroup9);
            tableGroup8.ChildGroups.Add(tableGroup10);
            tableGroup8.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup8.Name = "detailTableGroup1";
            tableGroup12.Name = "group6";
            tableGroup11.ChildGroups.Add(tableGroup12);
            tableGroup11.Name = "group5";
            tableGroup14.Name = "group8";
            tableGroup13.ChildGroups.Add(tableGroup14);
            tableGroup13.Name = "group7";
            tableGroup16.Name = "group10";
            tableGroup15.ChildGroups.Add(tableGroup16);
            tableGroup15.Name = "group9";
            tableGroup18.Name = "group12";
            tableGroup17.ChildGroups.Add(tableGroup18);
            tableGroup17.Name = "group11";
            this.table2.RowGroups.Add(tableGroup8);
            this.table2.RowGroups.Add(tableGroup11);
            this.table2.RowGroups.Add(tableGroup13);
            this.table2.RowGroups.Add(tableGroup15);
            this.table2.RowGroups.Add(tableGroup17);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.674167633056641D), Telerik.Reporting.Drawing.Unit.Cm(4.1404170989990234D));
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.330209732055664D), Telerik.Reporting.Drawing.Unit.Cm(0.9789583683013916D));
            this.textBox11.Style.Font.Name = "Calibri";
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox11.Value = resources.GetString("textBox11.Value");
            // 
            // pictureBox6
            // 
            this.pictureBox6.MimeType = "image/jpeg";
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.34395879507064819D), Telerik.Reporting.Drawing.Unit.Cm(0.97895830869674683D));
            this.pictureBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox6.StyleName = "";
            this.pictureBox6.Value = ((object)(resources.GetObject("pictureBox6.Value")));
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.330209732055664D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox7.Style.Font.Name = "Calibri";
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox7.StyleName = "";
            this.textBox7.Value = "I am aware that any damage caused by turning the system on before the inspection " +
    "has been completed is not covered by the warranty";
            // 
            // pictureBox7
            // 
            this.pictureBox7.MimeType = "image/jpeg";
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.34395879507064819D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.pictureBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox7.StyleName = "";
            this.pictureBox7.Value = ((object)(resources.GetObject("pictureBox7.Value")));
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.330209732055664D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox9.Style.Font.Bold = true;
            this.textBox9.Style.Font.Name = "Calibri";
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox9.StyleName = "";
            this.textBox9.Value = "I am aware that any claims for alleged damages caused by the installation of my s" +
    "ystem are to be made within Five (5) business days from the date of installation" +
    "";
            // 
            // pictureBox8
            // 
            this.pictureBox8.MimeType = "image/jpeg";
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.34395879507064819D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.pictureBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox8.StyleName = "";
            this.pictureBox8.Value = ((object)(resources.GetObject("pictureBox8.Value")));
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.330209732055664D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Font.Name = "Calibri";
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox12.StyleName = "";
            this.textBox12.Value = "I received the Signage Board from the Installer and Happy to Display Outside my H" +
    "ouse.";
            // 
            // pictureBox9
            // 
            this.pictureBox9.MimeType = "image/jpeg";
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.34395879507064819D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.pictureBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox9.StyleName = "";
            this.pictureBox9.Value = ((object)(resources.GetObject("pictureBox9.Value")));
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.330209732055664D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Font.Name = "Calibri";
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox13.StyleName = "";
            this.textBox13.Value = "I agree to publish my feedback for review purpose.";
            // 
            // pictureBox10
            // 
            this.pictureBox10.MimeType = "image/jpeg";
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.34395879507064819D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.pictureBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox10.StyleName = "";
            this.pictureBox10.Value = ((object)(resources.GetObject("pictureBox10.Value")));
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.330209732055664D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Font.Name = "Calibri";
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox14.StyleName = "";
            this.textBox14.Value = "If I am a Certegy Ezi Pay customer I, the undersigned state that:";
            // 
            // pictureBox11
            // 
            this.pictureBox11.MimeType = "image/jpeg";
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.34395879507064819D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.pictureBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox11.StyleName = "";
            this.pictureBox11.Value = ((object)(resources.GetObject("pictureBox11.Value")));
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045D), Telerik.Reporting.Drawing.Unit.Cm(3.3997995853424072D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.674304962158203D), Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269D));
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Name = "Calibri";
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox5.Value = "As the \"Customer\" I do hereby acknowledge that I have: ";
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.37041729688644409D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(15.478129386901856D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.79375004768371582D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D)));
            this.table3.Body.SetCellContent(0, 1, this.textBox17);
            this.table3.Body.SetCellContent(0, 0, this.pictureBox13);
            this.table3.Body.SetCellContent(1, 1, this.textBox15);
            this.table3.Body.SetCellContent(1, 0, this.pictureBox14);
            tableGroup19.Name = "tableGroup";
            tableGroup19.ReportItem = this.pictureBox12;
            tableGroup20.Name = "tableGroup4";
            tableGroup20.ReportItem = this.textBox16;
            this.table3.ColumnGroups.Add(tableGroup19);
            this.table3.ColumnGroups.Add(tableGroup20);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox13,
            this.textBox17,
            this.pictureBox14,
            this.textBox15,
            this.pictureBox12,
            this.textBox16});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(10.699999809265137D));
            this.table3.Name = "table3";
            tableGroup22.Name = "group13";
            tableGroup23.Name = "group14";
            tableGroup21.ChildGroups.Add(tableGroup22);
            tableGroup21.ChildGroups.Add(tableGroup23);
            tableGroup21.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup21.Name = "detailTableGroup2";
            this.table3.RowGroups.Add(tableGroup21);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.848546028137207D), Telerik.Reporting.Drawing.Unit.Cm(1.7170833349227905D));
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.478127479553223D), Telerik.Reporting.Drawing.Unit.Cm(0.79374992847442627D));
            this.textBox17.Style.Font.Name = "Calibri";
            this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox17.Value = resources.GetString("textBox17.Value");
            // 
            // pictureBox13
            // 
            this.pictureBox13.MimeType = "image/jpeg";
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.37041729688644409D), Telerik.Reporting.Drawing.Unit.Cm(0.79374992847442627D));
            this.pictureBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox13.StyleName = "";
            this.pictureBox13.Value = ((object)(resources.GetObject("pictureBox13.Value")));
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.47812557220459D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox15.Style.Font.Name = "Calibri";
            this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox15.StyleName = "";
            this.textBox15.Value = "The construction of services provided to my property or dwelling from the above c" +
    "ompany merchant: has been completed to my satisfaction.\r\nI am happy to commence " +
    "my repayments to Certegy.";
            // 
            // pictureBox14
            // 
            this.pictureBox14.MimeType = "image/jpeg";
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.37041729688644409D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.pictureBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.pictureBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.pictureBox14.StyleName = "";
            this.pictureBox14.Value = ((object)(resources.GetObject("pictureBox14.Value")));
            // 
            // textBox43
            // 
            this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.9180550575256348D), Telerik.Reporting.Drawing.Unit.Cm(12.699999809265137D));
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.38194465637207D), Telerik.Reporting.Drawing.Unit.Cm(0.37430489063262939D));
            this.textBox43.Style.Font.Name = "Calibri";
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox43.Value = "Job satisfactorily completed and installed on (date)";
            // 
            // textBox45
            // 
            this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.1999998092651367D), Telerik.Reporting.Drawing.Unit.Cm(12.699999809265137D));
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6999996900558472D), Telerik.Reporting.Drawing.Unit.Cm(0.37430527806282043D));
            this.textBox45.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox45.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.20000000298023224D);
            this.textBox45.Style.Font.Bold = true;
            this.textBox45.Style.Font.Name = "Calibri";
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox45.Value = "textBox45";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.10000000149011612D), Telerik.Reporting.Drawing.Unit.Cm(14.899999618530273D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.27430534362793D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox3.Style.Color = System.Drawing.Color.White;
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Name = "Calibri";
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox3.Value = "QUALITY OF SERVICE (1=POOR, 2=AVERAGE, 3=GOOD, 4=VERY GOOD)";
            // 
            // textBox28
            // 
            this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.92060196399688721D), Telerik.Reporting.Drawing.Unit.Cm(16.600000381469727D));
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2999999523162842D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox28.Style.Color = System.Drawing.Color.White;
            this.textBox28.Style.Font.Bold = true;
            this.textBox28.Style.Font.Name = "Calibri";
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox28.Value = "Overall Impression";
            // 
            // textBox25
            // 
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(16.299999237060547D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.999998152256012D));
            this.textBox25.Style.Color = System.Drawing.Color.White;
            this.textBox25.Style.Font.Bold = true;
            this.textBox25.Style.Font.Name = "Calibri";
            this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.Value = "Service/Technical\r\nSupport";
            // 
            // textBox24
            // 
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.6000003814697266D), Telerik.Reporting.Drawing.Unit.Cm(16.399999618530273D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0983800888061523D), Telerik.Reporting.Drawing.Unit.Cm(0.94282734394073486D));
            this.textBox24.Style.Color = System.Drawing.Color.White;
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.Font.Name = "Calibri";
            this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.Value = "Installation";
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.40000057220459D), Telerik.Reporting.Drawing.Unit.Cm(16.299999237060547D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(0.9999997615814209D));
            this.textBox23.Style.Color = System.Drawing.Color.White;
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Font.Name = "Calibri";
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.Value = "Your Experience";
            // 
            // textBox21
            // 
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.200000762939453D), Telerik.Reporting.Drawing.Unit.Cm(16.399997711181641D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0999984741210938D), Telerik.Reporting.Drawing.Unit.Cm(0.94282734394073486D));
            this.textBox21.Style.Color = System.Drawing.Color.White;
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.Font.Name = "Calibri";
            this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.Value = "Personal View";
            // 
            // textBox27
            // 
            this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.92060190439224243D), Telerik.Reporting.Drawing.Unit.Cm(21.399999618530273D));
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.0793981552124023D), Telerik.Reporting.Drawing.Unit.Cm(0.70000088214874268D));
            this.textBox27.Style.Color = System.Drawing.Color.White;
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Font.Name = "Calibri";
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox27.Value = "Installation Sketch";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.53798621892929077D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox18);
            tableGroup26.Name = "group";
            tableGroup26.ReportItem = this.textBox19;
            tableGroup25.ChildGroups.Add(tableGroup26);
            tableGroup25.Name = "group1";
            tableGroup25.ReportItem = this.textBox20;
            tableGroup24.ChildGroups.Add(tableGroup25);
            tableGroup24.Name = "tableGroup";
            tableGroup24.ReportItem = this.textBox22;
            this.table4.ColumnGroups.Add(tableGroup24);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox18,
            this.textBox22,
            this.textBox20,
            this.textBox19});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.9999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(18.299999237060547D));
            this.table4.Name = "table4";
            tableGroup27.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup27.Name = "detailTableGroup";
            this.table4.RowGroups.Add(tableGroup27);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(1.9348613023757935D));
            this.table4.Style.Color = System.Drawing.Color.White;
            this.table4.Style.Font.Bold = true;
            this.table4.Style.Font.Name = "Arial";
            this.table4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.table4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.table4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.53798621892929077D));
            this.textBox18.Style.Font.Name = "Calibri";
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox18.Value = "4";
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.53798621892929077D)));
            this.table5.Body.SetCellContent(0, 0, this.textBox26);
            tableGroup30.Name = "group";
            tableGroup30.ReportItem = this.textBox29;
            tableGroup29.ChildGroups.Add(tableGroup30);
            tableGroup29.Name = "group1";
            tableGroup29.ReportItem = this.textBox30;
            tableGroup28.ChildGroups.Add(tableGroup29);
            tableGroup28.Name = "tableGroup";
            tableGroup28.ReportItem = this.textBox31;
            this.table5.ColumnGroups.Add(tableGroup28);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox26,
            this.textBox31,
            this.textBox30,
            this.textBox29});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.09999942779541D), Telerik.Reporting.Drawing.Unit.Cm(18.299999237060547D));
            this.table5.Name = "table5";
            tableGroup31.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup31.Name = "detailTableGroup";
            this.table5.RowGroups.Add(tableGroup31);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(1.9348613023757935D));
            this.table5.Style.Color = System.Drawing.Color.White;
            this.table5.Style.Font.Bold = true;
            this.table5.Style.Font.Name = "Arial";
            this.table5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.table5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.table5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.53798621892929077D));
            this.textBox26.Style.Font.Name = "Calibri";
            this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.Value = "4";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.53798621892929077D)));
            this.table6.Body.SetCellContent(0, 0, this.textBox33);
            tableGroup34.Name = "group";
            tableGroup34.ReportItem = this.textBox34;
            tableGroup33.ChildGroups.Add(tableGroup34);
            tableGroup33.Name = "group1";
            tableGroup33.ReportItem = this.textBox35;
            tableGroup32.ChildGroups.Add(tableGroup33);
            tableGroup32.Name = "tableGroup";
            tableGroup32.ReportItem = this.textBox36;
            this.table6.ColumnGroups.Add(tableGroup32);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox33,
            this.textBox36,
            this.textBox35,
            this.textBox34});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.8000001907348633D), Telerik.Reporting.Drawing.Unit.Cm(18.299999237060547D));
            this.table6.Name = "table6";
            tableGroup35.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup35.Name = "detailTableGroup";
            this.table6.RowGroups.Add(tableGroup35);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(1.9348613023757935D));
            this.table6.Style.Color = System.Drawing.Color.White;
            this.table6.Style.Font.Bold = true;
            this.table6.Style.Font.Name = "Arial";
            this.table6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.table6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.table6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.53798621892929077D));
            this.textBox33.Style.Font.Name = "Calibri";
            this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox33.Value = "4";
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.53798621892929077D)));
            this.table7.Body.SetCellContent(0, 0, this.textBox37);
            tableGroup38.Name = "group";
            tableGroup38.ReportItem = this.textBox38;
            tableGroup37.ChildGroups.Add(tableGroup38);
            tableGroup37.Name = "group1";
            tableGroup37.ReportItem = this.textBox39;
            tableGroup36.ChildGroups.Add(tableGroup37);
            tableGroup36.Name = "tableGroup";
            tableGroup36.ReportItem = this.textBox40;
            this.table7.ColumnGroups.Add(tableGroup36);
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox37,
            this.textBox40,
            this.textBox39,
            this.textBox38});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.5D), Telerik.Reporting.Drawing.Unit.Cm(18.299999237060547D));
            this.table7.Name = "table7";
            tableGroup39.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup39.Name = "detailTableGroup";
            this.table7.RowGroups.Add(tableGroup39);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(1.9348613023757935D));
            this.table7.Style.Color = System.Drawing.Color.White;
            this.table7.Style.Font.Bold = true;
            this.table7.Style.Font.Name = "Arial";
            this.table7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.table7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.table7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.53798621892929077D));
            this.textBox37.Style.Font.Name = "Calibri";
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.Value = "4";
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.53798621892929077D)));
            this.table8.Body.SetCellContent(0, 0, this.textBox41);
            tableGroup42.Name = "group";
            tableGroup42.ReportItem = this.textBox42;
            tableGroup41.ChildGroups.Add(tableGroup42);
            tableGroup41.Name = "group1";
            tableGroup41.ReportItem = this.textB;
            tableGroup40.ChildGroups.Add(tableGroup41);
            tableGroup40.Name = "tableGroup";
            tableGroup40.ReportItem = this.textBox46;
            this.table8.ColumnGroups.Add(tableGroup40);
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox41,
            this.textBox46,
            this.textB,
            this.textBox42});
            this.table8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.399999618530273D), Telerik.Reporting.Drawing.Unit.Cm(18.299999237060547D));
            this.table8.Name = "table8";
            tableGroup43.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup43.Name = "detailTableGroup";
            this.table8.RowGroups.Add(tableGroup43);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(1.9348613023757935D));
            this.table8.Style.Color = System.Drawing.Color.White;
            this.table8.Style.Font.Bold = true;
            this.table8.Style.Font.Name = "Arial";
            this.table8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.table8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.table8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.3999997079372406D), Telerik.Reporting.Drawing.Unit.Cm(0.53798621892929077D));
            this.textBox41.Style.Font.Name = "Calibri";
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox41.Value = "4";
            // 
            // textBox47
            // 
            this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(28.099998474121094D));
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.600002110004425D));
            this.textBox47.Style.Font.Bold = true;
            this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox47.Value = "Name";
            // 
            // textBox48
            // 
            this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.9999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(28.19999885559082D));
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3000001907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.600002110004425D));
            this.textBox48.Value = "textBox48";
            // 
            // textBox49
            // 
            this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.3999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(28.099998474121094D));
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9999990463256836D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.textBox49.Style.Font.Bold = true;
            this.textBox49.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox49.Value = "Signature";
            // 
            // textBox50
            // 
            this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.5D), Telerik.Reporting.Drawing.Unit.Cm(28.099998474121094D));
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0999990701675415D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.textBox50.Style.Font.Bold = true;
            this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox50.Value = "Date";
            // 
            // textBox
            // 
            this.textBox.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.2000001668930054D), Telerik.Reporting.Drawing.Unit.Cm(26.099998474121094D));
            this.textBox.Name = "textBox";
            this.textBox.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.100001335144043D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.textBox.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(12)))), ((int)(((byte)(83)))), ((int)(((byte)(149)))));
            this.textBox.Style.Font.Bold = true;
            this.textBox.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(14D);
            this.textBox.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox.Value = "Signed (\"The Customer\", or Authorize Person):";
            // 
            // table9
            // 
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(17.399999618530273D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60854166746139526D)));
            this.table9.Body.SetCellContent(0, 0, this.htmlTextBox1);
            tableGroup44.Name = "tableGroup5";
            this.table9.ColumnGroups.Add(tableGroup44);
            this.table9.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.htmlTextBox1});
            this.table9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D), Telerik.Reporting.Drawing.Unit.Cm(2.5D));
            this.table9.Name = "table9";
            tableGroup45.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup45.Name = "detailTableGroup3";
            this.table9.RowGroups.Add(tableGroup45);
            this.table9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.399999618530273D), Telerik.Reporting.Drawing.Unit.Cm(0.60854166746139526D));
            // 
            // htmlTextBox1
            // 
            this.htmlTextBox1.Name = "htmlTextBox1";
            this.htmlTextBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.399999618530273D), Telerik.Reporting.Drawing.Unit.Cm(0.60854166746139526D));
            this.htmlTextBox1.Style.Color = System.Drawing.Color.Red;
            this.htmlTextBox1.StyleName = "";
            this.htmlTextBox1.Value = resources.GetString("htmlTextBox1.Value");
            // 
            // SolarMinerCustomerAc1
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "SolarMinerCustomerAc1";
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.PictureBox pictureBox2;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.PictureBox pictureBox3;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.PictureBox pictureBox4;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.PictureBox pictureBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.PictureBox pictureBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.PictureBox pictureBox8;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.PictureBox pictureBox9;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.PictureBox pictureBox10;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.PictureBox pictureBox11;
        private Telerik.Reporting.PictureBox pictureBox5;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.PictureBox pictureBox13;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.PictureBox pictureBox14;
        private Telerik.Reporting.PictureBox pictureBox12;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.Table table8;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textB;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox;
        private Telerik.Reporting.Table table9;
        private Telerik.Reporting.HtmlTextBox htmlTextBox1;
    }
}