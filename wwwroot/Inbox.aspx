﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="Inbox.aspx.cs" Inherits="Inbox" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/start/jquery-ui.css"
        rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300" rel="stylesheet" type="text/css">
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Inbox</h5>
                <div id="tdExport" class="pull-right" runat="server">
                    <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">
                        <%--<asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                    CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>--%>
                    </ol>
                </div>
            </div>
            <!-- /Page Content -->
            <div class="page-body padtopzero">
                <div class="mail-container">
                    <div class="mail-header">

                        <ul class="header-buttons">
                            <li>
                                <a href="Compose.aspx" class="tooltip-primary" data-toggle="tooltip" data-original-title="Compose"><i class="glyphicon glyphicon-pencil"></i></a>
                            </li>
                            <li>
                                <a class="tooltip-primary" data-toggle="tooltip" data-original-title="Options"><i class="fa fa-angle-down"></i></a>
                            </li>
                            <li>
                                <a class="tooltip-primary" data-toggle="tooltip" data-original-title="Reply"><i class="glyphicon glyphicon-repeat"></i></a>
                            </li>
                            <li>
                                <a class="tooltip-primary" data-toggle="tooltip" data-original-title="Forward"><i class="glyphicon glyphicon-share-alt"></i></a>
                            </li>
                            <li>
                                <a class="tooltip-primary" data-toggle="tooltip" data-original-title="Remove"><i class="glyphicon glyphicon-remove"></i></a>
                            </li>
                            <li>
                                <a class="tooltip-primary" data-toggle="tooltip" data-original-title="Important"><i class="fa fa-exclamation"></i></a>
                            </li>
                        </ul>
                        <ul class="header-buttons pull-right">
                            <li>
                                <a><i class="fa fa-angle-left"></i></a>
                            </li>
                            <li>
                                <a><i class="fa fa-angle-right"></i></a>
                            </li>
                            <li class="search">
                                <span class="input-icon">
                                    <input type="text" class="form-control input-sm" id="fontawsome-search">
                                    <i class="glyphicon glyphicon-search lightgray"></i>
                                </span>
                            </li>
                        </ul>
                        <div class="pages">
                            1-100 of 608
                        </div>
                    </div>
                </div>
                <div class="mail-body">
                    <div class="mail-list">
					 <div class="list-item unread">
                        <asp:GridView ID="gvEmails" runat="server" OnRowDataBound="OnRowDataBound" ShowHeader="false" OnRowCommand="gvEmails_RowCommand" DataKeyNames="MessageNumber"
                            CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div class="item-check">
                                            <label>
                                                <input type="checkbox">
                                                <span class="text"></span>
                                            </label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div class="item-star">
                                            <a href="#" class="">
                                                <i class="fa fa-star-o"></i>
                                            </a>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <div class="item-sender">
                                            <asp:Label ID="lblfrom" class="body" runat="server" Text='<%# Eval("From") %>'> </asp:Label>
                                            <asp:HiddenField ID="hdnfrom" runat="server" />
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <div class="item-subject">
                                            <asp:LinkButton ID="lnkView" CommandName="lnkView" CommandArgument="<%# Container.DataItemIndex %>" OnClick="lnkView_Click" runat="server" Text='<%# Eval("Subject") %>' />
                                            <asp:HiddenField ID="hdnsubject" runat="server" />
                                            <asp:Label ID="lblbody" class="body" runat="server" Visible="false" Text='<%# Eval("Body") %>'> </asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div class="item-options">
                                            <a href="message-view.html"><i class="fa fa-paperclip"></i></a>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div class="item-time">
                                            '<%# Eval("DateSent") %>'
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-CssClass="Attachments">
                                    <ItemTemplate>
                                        <asp:Repeater ID="rptAttachments" runat="server">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkAttachment" runat="server" OnClick="Download" Text='<%# Eval("FileName") %>' />
                                            </ItemTemplate>
                                            <SeparatorTemplate>
                                                <br>
                                            </SeparatorTemplate>
                                        </asp:Repeater>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                         </div>
                    </div>
                </div>


            </div>
            <asp:Button ID="btnNULLData1" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                DropShadow="false" PopupControlID="divName" OkControlID="btnOKName" TargetControlID="btnNULLData1">
            </cc1:ModalPopupExtender>
            <div id="divName" runat="server" style="display: none" class="modal_popup">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header text-center">
                            <h4 class="modal-title" id="myModalLabel">Message</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
