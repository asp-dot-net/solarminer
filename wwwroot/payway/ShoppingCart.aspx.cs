﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class payway_ShoppingCart : System.Web.UI.Page
{
    protected IDictionary prices = null;
    protected IDictionary quantities = null;
    protected double totalAmount = 0;
    protected void Page_Load(object sender, System.EventArgs e)
    {
        if (Session["prices"] != null)
        {
            prices = (IDictionary)Session["prices"];
        }
        else
        {
            prices = new Hashtable();
            prices["Product 1"] = 10.00;
            prices["Product 2"] = 12.00;
            prices["Product 3"] = 14.00;
            Session["prices"] = prices;
        }

        if (Session["quantities"] != null)
        {
            quantities = (IDictionary)Session["quantities"];
        }
        else
        {
            quantities = new Hashtable();
            quantities["Product 1"] = 1;
            quantities["Product 2"] = 2;
            quantities["Product 3"] = 1;
            Session["quantities"] = quantities;
        }

        // Update the shopping cart
        if (Request.Form["Submit"] != null)
        {
            quantities["Product 1"] = Int32.Parse(Request.Form["Product 1"]);
            quantities["Product 2"] = Int32.Parse(Request.Form["Product 2"]);
            quantities["Product 3"] = Int32.Parse(Request.Form["Product 3"]);
        }

        totalAmount =
            (int)quantities["Product 1"] * (double)prices["Product 1"] +
            (int)quantities["Product 2"] * (double)prices["Product 2"] +
            (int)quantities["Product 3"] * (double)prices["Product 3"];

        Session["totalAmount"] = totalAmount;

        // Hand-off to the PayWay payment page
        if ("Check Out".Equals(Request.Form["Submit"]))
        {
            Response.Redirect("CheckOut.aspx", true);
        }
    }
}