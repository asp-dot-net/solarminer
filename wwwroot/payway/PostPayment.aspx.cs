﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Security.Cryptography;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class payway_PostPayment : System.Web.UI.Page
{
    protected IDictionary parameters;

    private void Page_Load(object sender, System.EventArgs e)
    {
        Response.Write(ConfigurationSettings.AppSettings["encryptionKey"]);
        Response.End();
        parameters = DecryptParameters(
            ConfigurationSettings.AppSettings["encryptionKey"],
            Request.QueryString["EncryptedParameters"],
            Request.QueryString["Signature"]);

        IEnumerator parameterNames = parameters.Keys.GetEnumerator();
        Debug.WriteLine("parameters: ");
        while (parameterNames.MoveNext())
        {
            string parameterName = (string)parameterNames.Current;
            Debug.WriteLine("  " + parameterName + " = " + parameters[parameterName]);
        }
    }

    private IDictionary DecryptParameters(string base64Key,
        string encryptedParametersText,
        string signatureText)
    {
        byte[] keyBytes = Convert.FromBase64String(base64Key); ;

        // Decrypt the cipher text
        byte[] encryptedParametersBytes = Convert.FromBase64String(encryptedParametersText);
        RijndaelManaged cipher = new RijndaelManaged();
        cipher.KeySize = 128;
        cipher.BlockSize = 128;
        cipher.Mode = CipherMode.CBC;
        cipher.Padding = PaddingMode.PKCS7;
        cipher.IV = new byte[16];
        cipher.Key = keyBytes;
        ICryptoTransform decryptor = cipher.CreateDecryptor();
        byte[] parametersBytes = decryptor.TransformFinalBlock(
            encryptedParametersBytes, 0, encryptedParametersBytes.Length);
        string parametersText = Encoding.ASCII.GetString(parametersBytes);

        // Compute the hash from the decrypted plain text
        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
        byte[] computedHash = md5.ComputeHash(parametersBytes);

        // Decrypt the signature to get the hash value
        byte[] signatureBytes = Convert.FromBase64String(signatureText);
        byte[] hash = decryptor.TransformFinalBlock(
            signatureBytes, 0, signatureBytes.Length);

        // Check the provided MD5 hash against the computed one
        if (hash.Length != computedHash.Length)
        {
            throw new CryptographicException("Invalid parameters signature");
        }
        for (int i = 0; i < hash.Length; i++)
        {
            if (computedHash[i] != hash[i])
            {
                throw new CryptographicException("Invalid parameters signature");
            }
        }

        // Split the parameters
        Hashtable parametersTable = new Hashtable();
        string[] parameterStrings = parametersText.Split('&');
        foreach (string parameterString in parameterStrings)
        {
            string[] nameValue = parameterString.Split('=');
            parametersTable.Add(
                HttpUtility.UrlDecode(nameValue[0]),
                HttpUtility.UrlDecode(nameValue[1]));
        }
        return parametersTable;
    }
}