﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Net;

public partial class Signature : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected static int custompageIndex;
    protected static int startindex;
    protected static int lastpageindex;
    protected void Page_Load(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        String Token = Request.QueryString["rt"];
        pagetitle.Text = "SolarMiner | Signature";

        int TokenExist = Clstbl_TokenData.tbl_TokenDataExistByToken_ProjectID(Token, ProjectID);


        SttblProjects st1 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblProjects2 st3 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
        String contactid = st1.ContactID;
        String InstallerID = st1.Installer;
        SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(contactid);
        Sttbl_TokenData stk = Clstbl_TokenData.tbl_TokenData_SelectByToken_ProjectID(Token, ProjectID);

        //Response.Write(Request.PhysicalApplicationPath);
        //Response.End();

        if (!IsPostBack)
        {
            if (TokenExist == 0)
            {
                panelNotComplete.Visible = false;
                PanelPageNotExist.Visible = true;

            }
            //int ProjectIDSigned = Clstbl_SignatureLog.tbl_SignatureLogExistByProjectID(ProjectID);

            int ProjectIDSigned = Clstbl_SignatureLog.tbl_SignatureLogExistByProjectID_QDocNo(ProjectID, stk.QDocNo);
            if (ProjectIDSigned == 1)
            {
                panelNotComplete.Visible = false;
                panelExist.Visible = true;
            }


            string IsExpired = stk.IsExpired;
            if (IsExpired == "True")
            {
                panelNotComplete.Visible = false;
                panelExist.Visible = true;
                lblCustName4.Text = st2.ContFirst + " " + st2.ContLast;
            }
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;


            custompageIndex = 1;
            // divnopage.Visible = false;
            //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            //SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);        
        }

        lblCustName.Text = st2.ContFirst + " " + st2.ContLast;
        lblInstallerName.Text = st1.SalesRepName;
        lblRetailer.Text = "SOLAR MINER PTY LTD";
        lblCustAdd.Text = st1.InstallAddress + "," + st1.InstallState + "-" + st1.InstallPostCode;
        //lblInstalledDate.Text = st1.InstallCompleted;

        lblPanelBrand.Text = st1.PanelBrand;
        lblPanelModel.Text = st1.PanelModel;
        lblInverterBrand.Text = st1.InverterBrand;
        lblInverterModel.Text = st1.InverterModel;
        lblSystemOutput.Text = st1.SystemCapKW;
        lblPanelNo.Text = st1.NumberPanels;
        lblSTC.Text = st1.STCNumber;
        lblHType.Text = st1.HouseType;
        lblRType.Text = st1.RoofType;
        lblRAngle.Text = st1.RoofAngle;
        lblNMI.Text = st1.NMINumber;
        lblDistributor.Text = st1.ElecDistributor;
        lblRetailer2.Text = st1.ElecRetailer;
        lblMeterUpgrade.Text = st1.meterupgrade;
        lblMeterPhase.Text = st1.MeterPhase;
        //lblPrice.Text = st1.TotalQuotePrice;
        //lblVicRebate.Text = st1.VicRebate1;
        //lblVicRebateLoan.Text =st1.VicLoanDisc;
        try
        {
            if (!string.IsNullOrEmpty(st1.VicLoanDisc))
            {
                decimal Price1 = decimal.Parse(st1.VicLoanDisc);
                lblVicRebateLoan.Text = Price1.ToString("0.00");
            }
            else
            {
                lblVicRebateLoan.Text = "0.00";
            }
        }
        catch
        {
            lblVicRebateLoan.Text = "0.00";
        }
        try
        {
            if (!string.IsNullOrEmpty(st1.TotalQuotePrice))
            {
                decimal Price1 = decimal.Parse(st1.TotalQuotePrice);
                lblPrice.Text = Price1.ToString("0.00");
            }
            else
            {
                lblPrice.Text = "0.00";
            }
        }
        catch
        {
            lblPrice.Text = "0.00";
        }
        try
        {
            if (!string.IsNullOrEmpty(st1.VicRebate1))
            {
                decimal Price1 = decimal.Parse(st1.VicRebate1);
                lblVicRebate.Text = Price1.ToString("0.00");
            }
            else
            {
                lblVicRebate.Text = "0.00";
            }
        }
        catch
        {
            lblVicRebate.Text = "0.00";
        }
        DataTable dt1 = ClstblInvoicePayments.tblInvoicePayments_PayAmount(ProjectID);
        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        if (dtCount.Rows.Count > 0)
        {
            try
            {
                if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != "")
                {

                    decimal DepositPaid1 = decimal.Parse(dtCount.Rows[0]["InvoicePayTotal"].ToString());
                    lblPrice.Text = DepositPaid1.ToString("0.00");

                    decimal BalanceToPay1 = Convert.ToDecimal(st1.TotalQuotePrice) - Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
                    lblPrice.Text = BalanceToPay1.ToString("0.00");
                }
                else
                {
                    lblDPaid.Text = "0.00";
                    lblBalToPay.Text = "0.00";
                }
                // lblDPaid.Text = dtCount.Rows[0]["InvoicePayTotal"].ToString();
                // Decimal BalanceToPay = Convert.ToDecimal(st1.TotalQuotePrice) - Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
                // lblBalToPay.Text = Convert.ToString(BalanceToPay);
            }
            catch { }
        }
        else
        {
            lblDPaid.Text = "";
            lblBalToPay.Text = "";
        }

        //if (dt1.Rows.Count > 0)
        //{
        //    lblDPaid.Text =Convert.ToString(dt1.Rows[0]["InvoicePayTotal"].ToString());
        //    Decimal BalanceToPay = Convert.ToDecimal(st1.TotalQuotePrice) - Convert.ToDecimal(dt1.Rows[0]["InvoicePayTotal"].ToString())-4450;
        //    lblBalToPay.Text = Convert.ToString(BalanceToPay);
        //}
        //if (dtCount.Rows.Count > 0)OLd Change on 24-09-2019
        //{
        //    try
        //    {
        //        lblDPaid.Text = dtCount.Rows[0]["InvoicePayTotal"].ToString();
        //        Decimal BalanceToPay = Convert.ToDecimal(st1.TotalQuotePrice) - Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
        //        lblBalToPay.Text = Convert.ToString(BalanceToPay);
        //    }
        //    catch { }
        //}
        //else
        //{
        //    lblDPaid.Text = "";
        //    lblBalToPay.Text = "";
        //}

        if (st1.FinanceWithID == "1")
        {
            lblPOption.Text = "Cash";
        }
        else
        {
            lblPOption.Text = "Finance With " + st1.FinanceWith + "with " + st3.FinanceWithDeposit + "% for" + st1.PaymentType;
        }

        lblCustName2.Text = st2.ContFirst + " " + st2.ContLast;
        lblCustName3.Text = st2.ContFirst + " " + st2.ContLast;
        lblCustName4.Text = st2.ContFirst + " " + st2.ContLast;
        lblTodayDate.Text = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Today));
        lblCustEmailID.Text = st2.ContEmail;
        // lblIPAdd2.Text = lblIPAdd.Text;
    }

    [WebMethod]
    //string username, string subj, string desc
    public static string InsertData(string value, String ProjectID, String Token, String PhysicalRootLocation, String Latitude, String Longitude, String IPAddress)
    {
        string msg = string.Empty;
        //msg = value;
        SttblProjects st1 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        String contactid = st1.ContactID;
        String InstallerID = st1.Installer;
        SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(contactid);
        SttblEmployees st3 = ClstblEmployees.tblEmployees_SelectByEmployeeID(st1.SalesRep);

        int TokenExist = Clstbl_TokenData.tbl_TokenDataExistByToken_ProjectID(Token, ProjectID);

        //Sttbl_TokenData st = Clstbl_TokenData.tbl_TokenData_SelectByToken_ProjectID(Token, ProjectID);
        //String QFileName = st.QDocNo+ "Quotation.pdf";
        //SiteConfiguration.DeletePDFFile("quotedoc", QFileName);

        if (TokenExist == 1)
        {
            Sttbl_TokenData st = Clstbl_TokenData.tbl_TokenData_SelectByToken_ProjectID(Token, ProjectID);
            if (st.IsExpired == "False")
            {

                try
                {
                    String FileName = ProjectID + "_" + Token + "_" + "Owner.jpg";
                    byte[] bytIn = null;
                    value = value.Replace("data:image/png;base64,", "");
                    bytIn = Convert.FromBase64String(value);
                    //var path = Request.PhysicalApplicationPath + "userfiles\\" + "Signature" + "\\" + FileName;

                    var path = PhysicalRootLocation + "userfiles\\" + "Signature" + "\\" + FileName;
                    FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write);
                    fs.Write(bytIn, 0, bytIn.Length);
                    fs.Close();

                    //SiteConfiguration.UploadPDFFile("CustSignature", FileName);
                    //SiteConfiguration.deleteimage(FileName, "CustSignature");

                    String CustName = st2.ContFirst + " " + st2.ContLast;
                    String SignedDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
                    String latitude = Latitude;
                    String longitude = Longitude;
                    String IP_Address = IPAddress;
                    String CustEmail = st2.ContEmail;

                    Sttbl_TokenData sttok = Clstbl_TokenData.tbl_TokenData_SelectByToken_ProjectID(Token, ProjectID);
                    string Qdocno = sttok.QDocNo;

                    int success = Clstbl_SignatureLog.tbl_tbl_SignatureLog_Insert(CustName, SignedDate, latitude, longitude, IP_Address, CustEmail, Token, ProjectID, path, Qdocno);
                    Clstbl_TokenData.tbl_TokenData_Update_IsExpired(Token, ProjectID, "True");

                    if (success > 0)
                    {
                        msg = "success";
                        //String QFileName = st.QDocNo + "Quotation.pdf";
                        //SiteConfiguration.DeletePDFFile("quotedoc", QFileName);
                        //Telerik_reports.generate_quatation(ProjectID, Qdocno);
                        //Telerik_reports.generate_quatationSolarMiner(ProjectID, Qdocno);
                       Telerik_reports.Generatequotenew(ProjectID, Qdocno);
                        SiteConfiguration.deleteimage(FileName, "Signature");//FileName=Signature image name.
                        #region SendMail
                        TextWriter txtWriter = new StringWriter() as TextWriter;
                        StUtilities stU = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
                        string from = stU.from;
                        String Subject = "Signature Done- " + ConfigurationManager.AppSettings["SiteName"].ToString();

                        string body = string.Empty;
                        using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/mailtemplate/SignatureCompletionMail.aspx")))
                        {
                            body = reader.ReadToEnd();
                        }
                        body = body.Replace("{SalesRep}", st1.SalesRepName); //replacing the required things  
                        body = body.Replace("{CustName}", st2.ContFirst + " " + st2.ContLast);
                        body = body.Replace("{ProjectNumber}", st1.ProjectNumber);
                        body = body.Replace("{year}", Convert.ToString(DateTime.Now.Year));
                        body = body.Replace("{sitename}", ConfigurationManager.AppSettings["SiteName"].ToString());

                        Utilities.SendMail(from, st3.EmpEmail, Subject, body);
                        // Utilities.SendMail(from, "sumit@arisesolar.com.au", Subject, body);


                        try
                        {
                        }
                        catch(Exception ex)
                        {
                            msg = ex.Message+"Error";
                        }

                        #endregion
                        #region UpdateSignedQuote

                        if (!string.IsNullOrEmpty(st1.SQ))
                        {
                            SiteConfiguration.DeletePDFFile("SQ", st1.SQ);
                        }

                        string SQ = ProjectID + "_" + Qdocno + "_" + Token + "SolarMinerQuote.pdf";

                        bool sucSQ = ClstblProjects.tblProjects_UpdateSQ_SignedQuoteByProjectID(ProjectID, SQ, "true", SignedDate);

                        #endregion
                    }
                    else
                    {
                        msg =  "Error";
                    }
                }
                catch(Exception ex1)
                {
                    msg = ex1.Message + "Error";
                }
            }
            else
            {
                msg = "Error(Token Expired).";
            }
        }
        else
        {
            msg = "Error(Token does not exist).";

        }

        return msg;
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

}