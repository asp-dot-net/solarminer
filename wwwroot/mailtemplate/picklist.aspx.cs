﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mailtemplate_picklist : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string ProjectID = Request.QueryString["id"]; 
            //string ProjectID = "-2146260946";  

            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);
            SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(stPro.ContactID);
            lblProject.Text = stPro.Project;
            try
            {
                lblInstallBookingDate.Text = string.Format("{0:dddd - dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
            }
            catch { }

            if (stPro.Installer != string.Empty)
            {
                SttblContacts stinst = ClstblContacts.tblContacts_SelectByContactID(stPro.Installer);
                lblInstallerMobile.Text = stinst.ContMobile;
            }

            lblInstaller.Text = stPro.InstallerName;
            lblStockAllocationStore.Text = stPro.StoreName;

            lblCustomer.Text = stCust.Customer;
            lblCustPhone.Text = stCust.CustPhone;
            lblCustMobile.Text = stCont.ContMobile;

            lblHouseType.Text = stPro.HouseType;
            lblRoofAngle.Text = stPro.RoofAngle;
            lblRoofType.Text = stPro.RoofType;
            lblProjectNumber1.Text = stPro.ProjectNumber;
            lblProjectNumber2.Text = stPro.ProjectNumber;
            lblManualQuoteNumber.Text = stPro.ManualQuoteNumber;
            lblInstallerNotes.Text = stPro.InstallerNotes;
            lblPrinted.Text = string.Format("{0:dddd - dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14)));

            lblNumberPanels.Text = stPro.NumberPanels;
            lblPanelBrandName.Text = stPro.PanelBrandName;
            lblPanelModel.Text = stPro.PanelModel;
            if (stPro.PanelBrandID != string.Empty)
            {
                SttblStockItems stPan = ClstblStockItems.tblStockItems_SelectByStockItemID(stPro.PanelBrandID);
                lblPanDesc.Text = stPan.StockDescription;
            }

            if (stPro.InverterDetailsID != string.Empty)
            {
                trInv1.Visible = true;
                SttblStockItems stInv1 = ClstblStockItems.tblStockItems_SelectByStockItemID(stPro.InverterDetailsID);
                lblInvDesc1.Text = stInv1.StockDescription;
                lblInvName1.Text = stPro.InverterDetailsName;
                lblInvModel1.Text = stPro.InverterModel;
            }
            else
            {
                trInv1.Visible = false;
            }

            if (stPro.SecondInverterDetailsID != string.Empty)
            {
                trInv2.Visible = true;
                lblInvName2.Text = stPro.SecondInverterDetails;
                SttblStockItems stInv2 = ClstblStockItems.tblStockItems_SelectByStockItemID(stPro.SecondInverterDetailsID);
                lblInvModel2.Text = stInv2.StockModel;
                lblInvDesc2.Text = stInv2.StockDescription;
            }
            else
            {
                trInv2.Visible = false;
            }

            /* ------------------------------ Pick List ------------------------------ */

            lblProject1.Text = stPro.Project;
            try
            {
                lblInstallBookingDatePL.Text = string.Format("{0:dddd - dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
            }
            catch { }

            if (stPro.Installer != string.Empty)
            {
                SttblContacts stinst = ClstblContacts.tblContacts_SelectByContactID(stPro.Installer);
                lblInstallerMobile1.Text = stinst.ContMobile;
            }

            lblInstaller1.Text = stPro.InstallerName;
            lblStockAllocationStore1.Text = stPro.StoreName;

            lblCustomer1.Text = stCust.Customer;
            lblCustPhone1.Text = stCont.ContPhone;
            lblCustMobile1.Text = stCont.ContMobile;

            lblHouseType1.Text = stPro.HouseType;
            lblRoofAngle1.Text = stPro.RoofAngle;
            lblRoofType1.Text = stPro.RoofType;
            lblProjectNumber1PL.Text = stPro.ProjectNumber;
            lblProjectNumber2PL.Text = stPro.ProjectNumber;
            lblManualQuoteNumber1.Text = stPro.ManualQuoteNumber;
            lblPanelBrandName1.Text = stPro.PanelBrandName;
            if (stPro.PanelBrandID != string.Empty)
            {
                SttblStockItems stPan = ClstblStockItems.tblStockItems_SelectByStockItemID(stPro.PanelBrandID);
                lblPanDesc1.Text = stPan.StockDescription;
            }
            if (stPro.InverterDetailsID != string.Empty)
            {
                trInv11.Visible = true;
                SttblStockItems stInv1 = ClstblStockItems.tblStockItems_SelectByStockItemID(stPro.InverterDetailsID);
                lblInvDesc11.Text = stInv1.StockDescription;
                lblInvName11.Text = stPro.InverterDetailsName;
                lblInvModel11.Text = stPro.InverterModel;
            }
            else
            {
                trInv11.Visible = false;
            }
            if (stPro.SecondInverterDetailsID != string.Empty)
            {
                trInv21.Visible = true;
                lblInvName21.Text = stPro.SecondInverterDetails;
                SttblStockItems stInv2 = ClstblStockItems.tblStockItems_SelectByStockItemID(stPro.SecondInverterDetailsID);
                lblInvModel21.Text = stInv2.StockModel;
                lblInvDesc21.Text = stInv2.StockDescription;
            }
            else
            {
                trInv21.Visible = false;
            }
            lblInstallerNotes1.Text = stPro.InstallerNotes;
            lblPrinted1.Text = string.Format("{0:dddd - dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14)));

            
            /* ----------------------------------------------------------------------- */
        }
    }
}