﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="invoiceold.aspx.cs" Inherits="mailtemplete_invoice" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
</head>
<body>
    <div style="width: 100%;">
        <div style="width: 960px; margin: 0px auto;">
            <div style="clear: both; font-family: 'OpenSansLight' , sans-serif; font-size: 18px;">
            </div>
            <div style="font-family: 'OpenSansLight' , sans-serif; font-size: 18px;">
                <%--<div style="text-align: center; margin-bottom: 10px; font-family: 'OpenSansLight' , sans-serif; font-size: 18px;" runat="server" id="divinstall1">
                    Install Booking Date
                </div>
                <div style="text-align: center; font-family: 'OpenSansLight' , sans-serif; font-size: 18px;" runat="server" id="divinstall2">
                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                </div>--%>
                <h3 style="text-align: center; font-family: 'OpenSansLight' , sans-serif; margin: 5px;">Tax Invoice No:
                    <asp:Label ID="lblInvoiceNumber" runat="server"></asp:Label></h3>
                <asp:Label ID="lblDate" runat="server" style="text-align: center; font-family: 'OpenSansLight' , sans-serif; margin: 5px; display: block;"></asp:Label>
                <div style="font-family: 'OpenSansLight' , sans-serif; font-size: 18px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: solid 1px #999;">
                        <tr>
                            <td width="50%" style="border-top: solid 1px #999; border-collapse: collapse; padding: 5px;">
                                <strong>Customer Details</strong>
                            </td>
                            <td width="50%" style="border-top: solid 1px #999; border-collapse: collapse; padding: 5px; border-right: solid 1px #999;">
                                <strong>Installation Detail</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-collapse: collapse; padding: 5px;">
                                <asp:Label ID="lblContact" runat="server"></asp:Label>
                            </td>
                            <td style="border-collapse: collapse; border-right: solid 1px #999; padding: 5px;">
                                <asp:Label ID="lblInstallAddress" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-collapse: collapse; padding: 5px;">
                                <asp:Label ID="lblPostalAddress" runat="server"></asp:Label></strong>
                            </td>
                            <td style="border-collapse: collapse; border-right: solid 1px #999; padding: 5px;">
                                <asp:Label ID="lblInstallAddress2" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">
                                <asp:Label ID="lblPostalAddress2" runat="server"></asp:Label>
                            </td>
                            <td style="border-bottom: solid 1px #999; border-right: solid 1px #999; border-collapse: collapse; padding: 5px;">&nbsp;
                                
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: solid 1px #999; border-top: 1px solid #999;">
                        <tr>
                            <td width="70%" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">
                                <strong>For the Supply and Installation of Complete Solar System</strong>
                            </td>
                            <td width="16%" align="right" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">
                                <strong>Total Cost:</strong>
                            </td>
                            <td width="14%" align="right" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">
                                <strong>
                                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="16" algn="absmiddle" /><asp:Label ID="lblTotalQuotePrice" runat="server"></asp:Label></strong>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="3" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">
                                <strong>Panel Details :</strong>
                                <asp:Label ID="lblPanelDetails" runat="server"></asp:Label><strong><br>
                                    <br>
                                    Inverter Details :</strong>
                                <asp:Label ID="lblInverterDetails" runat="server"></asp:Label>
                            </td>
                           <%-- <td align="right" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">Less Govt Rebate:
                            </td>
                            <td align="right" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">
                                <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="16" algn="absmiddle" /><asp:Label ID="lblRECRebate" runat="server"></asp:Label>
                            </td>--%>
                        </tr>
                        <tr>
                            <td align="right" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">Net Cost:
                            </td>
                            <td align="right" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">
                                <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="16" algn="absmiddle" /><asp:Label ID="lblNetCost" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">Deposite:
                            </td>
                            <td align="right" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">
                                <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="16" algn="absmiddle" /><asp:Label ID="lblDepositPaid" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom: solid 1px #999;">&nbsp;
                                
                            </td>
                            <td align="right" style="border-left: solid 1px #999; border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">Balance Due:
                            </td>
                            <td align="right" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">
                                <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="16" algn="absmiddle" /><asp:Label ID="lblBalanceDue" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" rowspan="0" align="center" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">Note: The Net Cost above includes GST of:
                                <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="8" height="16" algn="absmiddle" /><asp:Label ID="lblBalanceGST" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: solid 1px #999; border-top: 1px solid #999;">
                        <tr>
                            <td width="100%" align="left" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">
                                <strong>Payment Schedule :</strong> This invoice needs to be paid on the day of
                                installation.
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">
                                <strong>Note:</strong> Customer agrees that he did not receive any solar grant in
                                the past and also agrees to transfer STCs to Euro Solar or Euro Solar Agent. All
                                products and services are supplied subject to Euro Solar terms and conditions.
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">*Upgrade of Meter Box (if needed) will be negotiated and charged by our Electrician
                                directly. The cost of the replacement of your current electric meter has to be borne
                                by you, which can include legal charges imposed by the Electricity Distributor.
                                Customer agrees to pay the balance amount of the system plus any additional costs
                                towards the system installation.
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: solid 1px #999; border-top: 1px solid #999;">
                        <tr>
                            <td colspan="6" align="left" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">
                                <strong>Payment Options</strong>
                            </td>
                        </tr>
                        <tr>
                            <td width="15%" align="left" bgcolor="#CCCCCC" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">EFT / Bank Transfer
                            </td>
                            <td width="17%" align="left" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">&nbsp;
                                
                            </td>
                            <td width="13%" align="left" bgcolor="#CCCCCC" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">Credit Card
                            </td>
                            <td width="13%" align="left" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">&nbsp;
                                
                            </td>
                            <td width="11%" align="left" bgcolor="#CCCCCC" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">Cheque / MO
                            </td>
                            <td width="31%" align="left" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">A/c Name: Euro Solar
                            </td>
                            <td colspan="2" align="left" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">(Master / Visa cards)
                            </td>
                            <td colspan="2" align="left" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">Cheque payable to &quot;Euro Solar&quot; and mail it to
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">BSB: 034037 A/c No. 250324
                            </td>
                            <td colspan="2" align="left" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">with 1.5% surcharge
                            </td>
                            <td colspan="2" align="left" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">PO BOX 570, Archerfield QLD 4108.
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: solid 1px #999; border-top: 1px solid #999;">
                        <tr>
                            <td width="100%" align="left" style="border-right: solid 1px #999; border-bottom: solid 1px #999; border-collapse: collapse; padding: 5px;">
                                <strong>Customer must pay the Balance Due amount on the day of the installation. A $20
                                    late payment each day/ collection fee, plus interest, will be applicable if customer
                                    fails to pay the Balance Due on time.</strong>
                            </td>
                        </tr>
                    </table>
                    <br />
                </div>
            </div>
        </div>
    </div>
</body>
</html>
