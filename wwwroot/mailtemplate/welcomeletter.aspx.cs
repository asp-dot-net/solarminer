﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class mailtemplate_welcomeletter : System.Web.UI.Page
{
    protected string SiteURL;
    public static string MakeImageSrcData(string filename)
    {
        FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
        byte[] filebytes = new byte[fs.Length];
        fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
        return "data:image/png;base64," +
          Convert.ToBase64String(filebytes, Base64FormattingOptions.None);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        if (!IsPostBack)
        {
            string ProjectID = Request.QueryString["id"]; // "-732617984";
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(stPro.ContactID);
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);

            lblContact.Text = stCont.ContFirst + " " + stCont.ContLast;
            //lblPostalAddress1.Text = stCust.PostalAddress;
            //lblPostalAddress2.Text = stCust.PostalCity + ", " + stCust.PostalState + ", " + stCust.PostalPostCode;
            //lblContact2.Text = stCont.ContFirst + " " + stCont.ContLast;
            //lblSalesRep.Text = stPro.SalesRepName;
            //lblQuotationNumber.Text = stPro.ProjectNumber;
        }
    }
}