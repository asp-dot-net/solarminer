﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="installerdocs.aspx.cs" Inherits="mailtemplate_installerdocs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width='600' border="0" align='left' cellpadding='0' cellspacing='1' style='color: #666; font-size: 14px;'>
                <tr>
                    <td>
                        <asp:Image ID="emailtop" runat="server" />
                    </td>
                </tr>
                <tr bgcolor='#ffffff'>
                    <td style="border: 1px solid #c17709;">
                        <table id="mytable" width='600' border="0" align='left' cellpadding='5' cellspacing='1'
                            style='color: #666; font-size: 14px;'>
                            <tr>
                                <td style="background-color: #d4d4d4; padding: 5px; color: #000;">
                                    <b>Dear Installation Department,</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>
                                        <%= Request["body"] %>
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>Thanks,<br />
                                    EURO SOLAR<br />
                                    1300 38 76 76
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Image ID="emailbottom" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
