﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="quatationdesign.aspx.cs" Inherits="mailtemplate_quatationdesign" %>

<%@ Register Src="~/includes/mail/quotefirst.ascx" TagPrefix="uc1" TagName="quotefirst" %>
<%@ Register Src="~/includes/mail/quotelast.ascx" TagPrefix="uc1" TagName="quotelast" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
    <style>
        body
        {
            margin: 0px!important;
            padding: 0px!important;
            font-family: 'Calibri';
        }

        /*.top_img
        {
            background-image: url("~/images/pdf_img1.jpg");
            background-size: 100%;
        }*/

        .page-break
        {
            display: block;
            page-break-after: always;
        }

        .pdfimages ul li
        {
            width: 318px;
            text-align: center;
            display: block;
            float: left;
            margin: 0px 0px 20px;
        }

        .pdfimages ul
        {
            width: 100%;
            float: left;
            margin: 0px;
            padding: 0px;
        }

        .terms tr td
        {
            font-size: 11px;
            text-align: justify;
            font-family: 'Calibri';
        }

        @font-face
        {
            font-family: 'amaticbold';
            src: url('../fonts/amatic-bold.eot');
            src: url('../fonts/amatic-bold.eot?#iefix') format('embedded-opentype'), url('../fonts/amatic-bold.woff2') format('woff2'), url('../fonts/amatic-bold.woff') format('woff'), url('../fonts/amatic-bold.ttf') format('truetype'), url('../fonts/amatic-bold.svg#amaticbold') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face
        {
            font-family: 'bobregular';
            src: url('../fonts/bob.eot');
            src: url('../fonts/bob.eot?#iefix') format('embedded-opentype'), url('../fonts/bob.woff2') format('woff2'), url('../fonts/bob.woff') format('woff'), url('../fonts/bob.ttf') format('truetype'), url('../fonts/bob.svg#bobregular') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face
        {
            font-family: 'mission_scriptregular';
            src: url('../fonts/mission-script.eot');
            src: url('../fonts/mission-script.eot?#iefix') format('embedded-opentype'), url('../fonts/mission-script.woff2') format('woff2'), url('../fonts/mission-script.woff') format('woff'), url('../fonts/mission-script.ttf') format('truetype'), url('../fonts/mission-script.svg#mission_scriptregular') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face
        {
            font-family: 'museo300';
            src: url('../fonts/museo300-regular.eot');
            src: url('../fonts/museo300-regular.eot?#iefix') format('embedded-opentype'), url('../fonts/museo300-regular.woff2') format('woff2'), url('../fonts/museo300-regular.woff') format('woff'), url('../fonts/museo300-regular.ttf') format('truetype'), url('../fonts/museo300-regular.svg#museo300') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face
        {
            font-family: 'museo500';
            src: url('../fonts/museo500-regular.eot');
            src: url('../fonts/museo500-regular.eot?#iefix') format('embedded-opentype'), url('../fonts/museo500-regular.woff2') format('woff2'), url('../fonts/museo500-regular.woff') format('woff'), url('../fonts/museo500-regular.ttf') format('truetype'), url('../fonts/museo500-regular.svg#museo500') format('svg');
            font-weight: normal;
            font-style: normal;
        }

        @font-face
        {
            font-family: 'museo700';
            src: url('../fonts/museo700-regular.eot');
            src: url('../fonts/museo700-regular.eot?#iefix') format('embedded-opentype'), url('../fonts/museo700-regular.woff2') format('woff2'), url('../fonts/museo700-regular.woff') format('woff'), url('../fonts/museo700-regular.ttf') format('truetype'), url('../fonts/museo700-regular.svg#museo700') format('svg');
            font-weight: normal;
            font-style: normal;
        }
    </style>
</head>
<body style="margin: 0px; padding: 0px;">
    <!-- ========== Strat static html ========== -->
   <%-- <div style="margin: 0px; padding: 0px;">
        <div style="top: 0px; z-index: 9; margin: 0px; left: 0px; top: 0px;">
            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/pdf_img1.jpg") %>" style="width: 100%; height: 100%; display: block;" />
        </div>
        <div style="left: 40px; top: 26px; position: absolute; z-index: 999!important; font-family: 'Calibri'; color: #fff; font-size: 13px; font-weight: bold;">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="135px" valign="top">Customer Name  </td>
                    <td valign="top">:</td>
                    <td valign="top">
                        <asp:Label runat="server" ID="lblcustname"></asp:Label></td>
                </tr>
                <tr>
                    <td valign="top">Address  </td>
                    <td valign="top">:</td>
                    <td valign="top">
                        <asp:Label runat="server" ID="lblStreetAddress"></asp:Label><br />
                        <asp:Label runat="server" ID="lblStreetCity"></asp:Label><br />
                        <asp:Label runat="server" ID="lblStreetState"></asp:Label>
                        -
                        <asp:Label runat="server" ID="lblStreetPostCoade"></asp:Label></td>
                </tr>
                <tr>
                    <td valign="top">Dated  </td>
                    <td valign="top">:</td>
                    <td valign="top">
                        <asp:Label ID="lblDate1" runat="server"></asp:Label></td>
                </tr>
            </table>
        </div>
        <div id="prjno2" runat="server" visible="false" style="left: 860px; position: absolute; top: 26px; z-index: 999999!important; font-family: 'Calibri'; color: #fff; font-size: 14px; font-weight: bold;">
            <asp:Label ID="lblProjectNumber1" runat="server"></asp:Label>
        </div>
    </div>--%>
    <table class="page-break"></table>

    <div style="padding: 20px 20px;">
        <div style="position: relative;">
            <div class="toparea" style="font-family: Arial, Helvetica, sans-serif;">
                <div style="float: left; font-family: Arial, Helvetica, sans-serif; padding: 5px 0px 0px 5px; width: 30%;">
                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/logo.png") %>"
                        width="124" height="100" />
                </div>
                <div style="float: right; text-align: right; padding: 5px 10px 0px 0px; width: 60%;">
                    <p style="font-family: 'Calibri'; font-size: 14px; line-height: 16px; color: #00b0f0; font-weight: 400;">
                        Solar Sales Representative :
                            <asp:Label ID="lblSalesRep" runat="server"></asp:Label><br>
                        <span style="font-size: 12px;">
                            <asp:Label ID="lblContMobile1" runat="server"></asp:Label><asp:Label ID="lblContPhone" runat="server"></asp:Label></span><br>
                        <span style="font-size: 14px; font-weight: 700;">
                            <asp:Label ID="lblProjectNumber" runat="server"></asp:Label></span><br>
                        <span style="font-size: 12px;">
                            <asp:Label ID="lblDate" runat="server"></asp:Label></span>
                    </p>
                </div>
                <div style="float: none; clear: both"></div>
            </div>
        </div>
        <div style="position: relative;">
            <div style="position: relative; z-index: 1;">
                <div>
                    <h1 style="font-family: 'Calibri'; font-weight: 600; font-size: 21px; line-height: 25px; color: #00b0f0; margin: 0px 0px 5px 0px; padding: 0px 0px 5px 0px; border-bottom: #000000 solid 3px;">Contact Details</h1>
                    <p style="font-family: 'Calibri'; font-size: 11px; line-height: 17px; color: #000; font-weight: 400; margin: 0px; padding: 0px 0px 35px 0px;">
                        Name:
                            <asp:Label ID="lblContFirst" runat="server"></asp:Label>
                        <asp:Label ID="lblContLast" runat="server"></asp:Label><br>
                        Mobile:
                            <asp:Label ID="lblContMobile" runat="server"></asp:Label><br>
                        Phone:
                            <asp:Label ID="lblCustPhone" runat="server"></asp:Label><br>
                        Email :
                            <asp:Label ID="lblContEmail" runat="server"></asp:Label>
                    </p>
                </div>
                <div>
                    <h1 style="font-family: 'Calibri'; font-weight: 600; font-size: 21px; line-height: 25px; color: #00b0f0; margin: 0px 0px 15px 0px; padding: 0px 0px 10px 0px; border-bottom: #000000 solid 3px;">Site Details</h1>
                    <p style="font-family: 'Calibri'; font-size: 11px; line-height: 17px; color: #000; font-weight: 400; margin: 0px; padding: 0px 0px 10px 0px;">
                        <asp:Label ID="lblInstallAddress" runat="server"></asp:Label>,<br>
                        <asp:Label ID="lblInstallCity" runat="server"></asp:Label>,
                            <asp:Label ID="lblInstallState" runat="server"></asp:Label>
                        –
                            <asp:Label ID="lblInstallPostCode" runat="server"></asp:Label>
                    </p>
                    <div style="padding-bottom: 20px;">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border: #000 solid 1px; font-family: 'Calibri'; font-size: 11px; color: #000; font-weight: 400;">
                            <tr>
                                <td style="background: #dbe5f1; border-bottom: #dbe5f1 solid 1px; border-left: #dbe5f1 solid 1px; border-left: #dbe5f1 solid 1px; padding: 2px 5px; width: 90px;">House Type:</td>
                                <td style="border-bottom: #000 solid 1px;">
                                    <asp:Label ID="lblHouseType" runat="server" Text="HouseType"></asp:Label></td>
                                <td style="background: #dbe5f1; border-bottom: #dbe5f1 solid 1px; padding: 2px 5px; width: 130px; border-left: #dbe5f1 solid 1px; border-top: #dbe5f1 solid 1px;">Meter Space:</td>
                                <td style="border-bottom: #000 solid 1px; padding: 3px 5px;">
                                    <asp:Label ID="lblMeterSpace" runat="server"></asp:Label></td>
                                <td style="background: #dbe5f1; border-bottom: #dbe5f1 solid 1px; padding: 2px 5px; width: 130px; border-left: #dbe5f1 solid 1px; border-top: #dbe5f1 solid 1px;">Off Peak:</td>
                                <td style="border-bottom: #000 solid 1px; padding: 2px 5px;">
                                    <asp:Label ID="lblOffPeak" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="background: #dbe5f1; border-bottom: #dbe5f1 solid 1px; padding: 2px 5px;">Roof Type:</td>
                                <td style="border-bottom: #000 solid 1px; padding: 2px 5px;">
                                    <asp:Label ID="lblRoofType" runat="server"></asp:Label></td>
                                <td style="background: #dbe5f1; border-bottom: #dbe5f1 solid 1px; padding: 2px 5px;">Meter Install:</td>
                                <td style="border-bottom: #000 solid 1px; padding: 2px 5px;">
                                    <asp:Label ID="lblMeterIncluded" runat="server"></asp:Label></td>
                                <td style="background: #dbe5f1; border-bottom: #dbe5f1 solid 1px; padding: 2px 5px;">Cherry Picker:</td>
                                <td style="border-bottom: #000 solid 1px; padding: 2px 5px;">
                                    <asp:Label ID="lblCherryPicker" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="background: #dbe5f1; border-bottom: #dbe5f1 solid 1px; padding: 2px 5px;">Roof Angle:</td>
                                <td style="border-bottom: #000 solid 1px; padding: 2px 5px;">
                                    <asp:Label ID="lblRoofAngle" runat="server"></asp:Label></td>
                                <td style="background: #dbe5f1; border-bottom: #dbe5f1 solid 1px; padding: 2px 5px;">Meter Phase:</td>
                                <td style="border-bottom: #000 solid 1px; padding: 2px 5px;">
                                    <asp:Label ID="lblMeterPhase" runat="server"></asp:Label></td>
                                <td style="background: #dbe5f1; border-bottom: #dbe5f1 solid 1px; padding: 2px 5px;">NMI:</td>
                                <td style="border-bottom: #000 solid 1px; padding: 2px 5px;">
                                    <asp:Label ID="lblNMINumber" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="background: #dbe5f1; border-bottom: #dbe5f1 solid 1px; padding: 2px 5px;">Asbestos:</td>
                                <td style="border-bottom: #000 solid 1px; padding: 2px 5px;">
                                    <asp:Label ID="lblAsbestos" runat="server"></asp:Label></td>
                                <td style="background: #dbe5f1; border-bottom: #dbe5f1 solid 1px; padding: 2px 5px;">Meter Upgrade:</td>
                                <td style="border-bottom: #000 solid 1px; padding: 2px 5px;">
                                    <asp:Label ID="lblMeterUG" runat="server"></asp:Label></td>
                                <td style="background: #dbe5f1; border-bottom: #dbe5f1 solid 1px; padding: 2px 5px;">Elec. Distributor:</td>
                                <td style="border-bottom: #000 solid 1px; padding: 2px 5px;">
                                    <asp:Label ID="lblElecDistributor" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="background: #dbe5f1; border-bottom: #dbe5f1 solid 1px; padding: 2px 5px;">Travel (KM):</td>
                                <td style="border-bottom: #000 solid 1px; padding: 2px 5px;">
                                    <asp:Label ID="lblTravelTime" runat="server"></asp:Label></td>
                                <td style="background: #dbe5f1; border-bottom: #000 solid 1px; padding: 2px 5px;">Meter Number:</td>
                                <td style="border-bottom: #000 solid 1px; padding: 2px 5px;"><%--«MeterNumber»--%><asp:Label ID="lblMeterNumber" runat="server"></asp:Label></td>
                                <td style="background: #dbe5f1; border-bottom: #000 solid 1px; padding: 2px 5px;">Elec. Retailer:</td>
                                <td style="border-bottom: #000 solid 1px; padding: 2px 5px;">
                                    <asp:Label ID="lblElecRetailer" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="background: #dbe5f1; border-bottom: #dbe5f1 solid 0px; padding: 2px 5px;">Split System:</td>
                                <td colspan="5" style="border-top: #000 solid 0px; padding-left: 0px;" valign="top">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td style="width: 10%;">&nbsp;</td>
                                            <td style="background: #dbe5f1; border-bottom: #dbe5f1 solid 0px; padding: 2px 5px; width: 18%; border-top: #dbe5f1 solid 1px;">North/West:</td>
                                            <td style="padding: 2px 5px; width: 10%;">
                                                <asp:Label ID="lblPanelConfigNW" runat="server"></asp:Label></td>
                                            <td style="background: #dbe5f1; border-bottom: #dbe5f1 solid 0px; padding: 2px 5px; width: 10%; border-top: #dbe5f1 solid 1px;">Other:</td>
                                            <td style="padding: 2px 5px; width: 10%">
                                                <asp:Label ID="lblPanelConfigOTH" runat="server"></asp:Label></td>
                                            <td style="background: #dbe5f1; border-bottom: #dbe5f1 solid 0px; padding: 2px 5px; width: 14%; border-top: #dbe5f1 solid 1px;">Other Variation:</td>
                                            <td style="padding: 2px 5px;">
                                                <asp:Label ID="lblVariationOther" runat="server"></asp:Label></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div style="position: relative;">
                <div>
                    <h1 style="font-family: 'Calibri'; font-weight: 600; font-size: 21px; line-height: 24px; color: #00b0f0; margin: 0px 0px 5px 0px; padding: 0px 0px 10px 0px; border-bottom: #000000 solid 3px;">System Details</h1>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="60%" valign="top">
                                <p style="font-family: 'Calibri'; font-size: 11px; line-height: 17px; color: #000; font-weight: 400; margin: 0px; padding: 0px 0px 0px 0px;">
                                    Panels:
                                        <asp:Label ID="lblPanelDetails" runat="server"></asp:Label><br>
                                    Inverter:
                                        <asp:Label ID="lblInverterDetails" runat="server"></asp:Label>
                                </p>
                            </td>
                            <td valign="top" align="right">
                                <p style="font-family: 'Calibri'; font-size: 13px; line-height: 19px; color: #000; font-weight: 400; margin: 0px; padding: 0px 0px 0px 0px;">
                                    <strong>
                                        <asp:Literal ID="litServiceValue" runat="server"></asp:Literal></strong>
                                </p>
                            </td>
                        </tr>
                    </table>

                    <div>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: 'Calibri'; font-size: 11px; line-height: 17px; color: #000; font-weight: 400;">
                            <tr>
                                <td valign="top" style="line-height: 17px;">For the supply and installation of the Solar Power System
                                    <%--, including wiring and brackets and as follows: Additional Charges (if any)--%>
                                </td>
                                <td width="25%" align="right" style="padding: 1px 5px;">House Type:</td>
                                <td width="25%" align="right" style="padding: 1px 5px;">
                                    <asp:Label ID="lblVarHouseType" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td rowspan="7" valign="top" style="line-height: 17px;">
                                    <asp:Label ID="lblQuotationNotes" runat="server" Visible="false"></asp:Label><br>
                                    <asp:Label ID="lblPromoNotes" runat="server" Visible="false"></asp:Label></td>
                                <td align="right" style="padding: 1px 5px;">Roof Type:</td>
                                <td align="right" style="padding: 1px 5px;">
                                    <asp:Label ID="lblVarRoofType" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="right" style="padding: 1px 5px;">Roof Angle:</td>
                                <td align="right" style="padding: 1px 5px;">
                                    <asp:Label ID="lblVarRoofAngle" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="right" style="padding: 1px 5px;">Meter Upgrade:</td>
                                <td align="right" style="padding: 1px 5px;">
                                    <asp:Label ID="lblVarMeterUG" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="right" style="padding: 1px 5px;">Other Variation:</td>
                                <td align="right" style="padding: 1px 5px;">
                                    <asp:Label ID="lblVarOther" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="right" style="padding: 1px 5px;">Meter Install:</td>
                                <td align="right" style="padding: 1px 5px;">
                                    <asp:Label ID="lblVarMeterInstall" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="right" style="padding: 1px 5px;">Special Discounts:</td>
                                <td align="right" style="padding: 1px 5px;">
                                    <asp:Label ID="lblSpecialDiscount" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td align="right" style="color: #000; font-weight: 600; padding: 1px 5px;">Less Govt. Rebate:</td>
                                <td align="right" style="padding: 1px 5px;">
                                    <asp:Label ID="lblRECRebate" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'Calibri'; font-size: 11px; line-height: 17px; color: #000; font-weight: 400; margin: 0px; padding: 10px 0px 5px 0px;"><strong>
                                    <asp:Label ID="lblFinanceStatus" runat="server"></asp:Label>
                                </strong></td>
                                <%--  <td>&nbsp;</td>--%>
                                <td align="right"><span style="font-size: 11px; font-weight: 600; padding: 1px 5px 3px 5px;">Total Cost after STCs Rebate:</span><br>
                                    Deposit Required:</td>
                                <td align="right" style="border-top: #000 solid 1px; padding: 1px 5px 3px 5px;"><span style="font-size: 11px; font-weight: 600;">
                                    <%--<img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" />--%>
                                    $
                                    <asp:Label ID="lblTotalQuotePrice" runat="server"></asp:Label></span><br>
                                    <%--<img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" />--%>
                                    $
                                    <asp:Label ID="lblDepositRequired" runat="server"></asp:Label></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right" style="font-size: 14px; color: #fff; font-weight: 600; background: #00b0f0; padding: 7px 5px 7px 5px;">Balance Due:</td>
                                <td align="right" style="font-size: 14px; color: #fff; font-weight: 600; background: #00b0f0; padding: 7px 5px 7px 5px;">$
                                    <%--<img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="6" height="12" algn="absmiddle" />--%>
                                    <asp:Label ID="lblBalanceToPay" runat="server"></asp:Label></td>
                            </tr>
                        </table>
                        <p style="font-family: 'Calibri'; font-size: 12px; line-height: 17px; color: #000; font-weight: 400; margin: 0px; padding: 3px 0px 5px 0px;">This Quotation is valid for 7 days from issue date.</p>

                        <table width="100%" border="0" cellspacing="0" cellpadding="6" style="border: #000000 solid 1px; font-family: 'Calibri'; font-size: 12px; line-height: 17px; color: #000; font-weight: 400;">
                            <tr>
                                <td colspan="4" align="center" style="font-family: 'Calibri'; font-size: 14px; line-height: 20px; color: #00b0f0; font-weight: 600; border-bottom: #000 solid 1px;">Payment Methods</td>
                            </tr>
                            <tr>
                                <td rowspan="2" valign="middle" style="border-right: #000 solid 1px; font-weight: 600; font-size: 12px; line-height: 17px;">Payment Option :</td>
                                <td style="border-bottom: #000 solid 1px; font-family: 'Calibri'; font-size: 12px; line-height: 17px; color: #00b0f0; font-weight: 600;">EFT / Bank Transfer</td>
                                <td style="border-bottom: #000 solid 1px; border-left: #000 solid 1px; font-family: 'Calibri'; font-size: 12px; line-height: 17px; color: #00b0f0; font-weight: 600;">Credit Card</td>
                                <td style="border-bottom: #000 solid 1px; border-left: #000 solid 1px; font-family: 'Calibri'; font-size: 12px; line-height: 17px; color: #00b0f0; font-weight: 600;">Cheque / Money Order</td>
                            </tr>
                            <tr>
                                <td>A/c Name: Think Green Solar
                                        <br>
                                    BSB:034008 A/c No. 308638 </td>

                                <td style="border-left: #000 solid 1px;">(Master / Visa cards)<br>
                                    with 1.5% surcharge</td>

                                <td style="border-left: #000 solid 1px;">Payable to &quot;Think Green Solar&quot; and mail it to<br>
                                   409, Francis Street, Brooklyn, VIC – 3012</td>
                            </tr>
                        </table>
                        <p style="font-family: 'Calibri'; font-size: 12px; line-height: 17px; color: #000; font-weight: 400; margin: 0px; padding: 7px 0px 7px 0px;">I agree to proceed with this Quotation/Contract and by paying the Deposit I acknowledge that I am aware of and accept the Terms & Conditions of the Contract stated overleaf.</p>
                        <table width="100%" border="1" cellspacing="0" cellpadding="0" style="font-family: 'Calibri'; font-size: 12px; line-height: 17px; color: #000; font-weight: 600; border-collapse: collapse;" bordercolor="#000">
                            <tr>
                                <td width="120" style="padding: 3px 10px; background: #808080; color: #fff;">Customer Name</td>
                                <td>&nbsp;</td>
                                <td width="90" style="padding: 3px 10px; background: #808080; color: #fff;">Signature:</td>
                                <td>&nbsp;</td>
                                <td width="70" style="padding: 3px 10px; background: #808080; color: #fff;">Date:</td>
                                <td width="130">&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div style="position: absolute; z-index: 2; right: 0px; top: 7px; width: 220px; height: 220px; background: url('<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/price.png") %>') no-repeat left top; background-size: cover;">
                <div style="font-family: 'bobregular'; color: #000; font-size: 13px; font-weight: normal; text-align: center; margin-top: 60px; margin-bottom: 7px;">
                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/go_img1.png") %>"
                        width="158" height="17" />
                </div>
                <div style="font-family: 'Calibri'; font-size: 14px; line-height: 20px; color: #000; font-weight: 400; text-align: center;">
                    <span style="font-size: 24px; display: inline-block; padding-right: 4px;">$</span>
                    <asp:Label ID="lblTotalQuotePrice1" runat="server" Style="font-size: 21px;"></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <p style="margin-top: 35px; font-size:7px; padding: 0px 0px 0px 13px; list-style: lower-alpha; font-weight: 400; font-family: 'Calibri'; line-height: 12px; text-align:center;">Address : 409, Francis Street, Brooklyn, VIC – 3012 | 1300 484 784 | <a href="http://www.thinkgreensolar.com.au" target="_blank">www.thinkgreensolar.com.au</a> | <a href="htpp://info@thinkgreensolar.com.au" target="_blank">info@thinkgreensolar.com.au</a> </p>
    <p style="margin: 0px; font-size:7px; padding: 0px 0px 0px 13px; list-style: lower-alpha; font-weight: 400; font-family: 'Calibri'; line-height: 15px; text-align:center;">ABN : 16 611 514 197 | Lic No QLD : 74095 | VIC : 22163 | TAS : 1300870 | NT : C3220</p>
    <table class="page-break"></table>
    <div style="margin: 20px 20px; padding-top: 20px; position: relative;">
        <div>
            <div style="z-index: 1;">
                <h1 style="font-family: 'Calibri'; font-weight: 600; font-size: 21px; line-height: 26px; color: #00b0f0; margin: 0px 0px 5px 0px; padding: 0px 0px 10px 0px; border-bottom: #000000 solid 3px; width: 80%;">Terms &amp; Conditions</h1>
            </div>
            <div style="position: absolute; right: 20px; top: 0px; z-index: 2;">
                <img style="" align="right" src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/logo.png") %>"
                    width="124" height="100" />
            </div>
            <div style="clear: both; float: none;"></div>
        </div>
        <div style="text-align: justify!important;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: 'Calibri'; font-weight: 400; font-size: 7px; color: #333;">
                <tr>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;" width="16">1.</td>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;">Customer Declaration</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <ul style="margin: 0px; padding: 0px 0px 0px 13px; list-style: lower-alpha; font-weight: 400; font-family: 'Calibri'; line-height: 12px;">
                            <li>You are over the legal age of 18 Years</li>
                            <li>You are one of the registered owners of the property at the installation address and your name is on the title deed of the Installation Address</li>
                            <li>You have never received or have never been approved for any rebate, financial assistance solar credit or small scale technology certificate (STCs) for small generation solar power system at the Installation Address.</li>
                        </ul>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: 'Calibri'; font-weight: 400; font-size: 7px; color: #333;">
                <tr>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;" width="16">2.</td>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;">Payment &amp; ST C's:</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <ul style="margin: 0px; padding: 0px 0px 0px 13px; list-style: lower-alpha; font-weight: 400; font-family: 'Calibri'; line-height: 12px;">
                            <li>THINK GREEN SOLAR must have the final payment prior to OR on the day of the installation by Credit Card or Cheque. If you prefer Electronic Fund transfer (EFT), the transaction must be completed 48 Hours prior to installation and you must provide us with written proof of the transfer. The customer must provide Credit Card details/cheque/ EFT Receipt to Installer prior to the Installation. Installation will not be carried out unless payment has been received by us.</li>
                            <li>You are agreeing to pay THINK GREEN SOLAR the STC's as part payment for your system. The STC's will be paid directly to THINK  GREEN SOLAR or a THINK  GREEN SOLAR Agent. If the Office of Clean Energy Regulator (http://ret.cleanenergyregulator.gov.au/) determines you are not eligible to receive STC's, and therefore THINK GREEN SOLAR is unable to receive the STC's as part payment, you will be liable to pay THINK  GREEN SOLAR the value of the STC's, as determined by market rates.</li>
                            <li>If you are not eligible for STC's, or if you wish to claim the STC's incentive yourself, the complete payment (including trading costs of the STC's) of the system is due before installation.</li>
                            <li>You acknowledge that if you breach any conditions of the STC's incentive regulations, you may be financially liable to the Office of Clean Energy Regulator. (http://ret.cleanenergyregulator.gov.au/). If you commit any breach of the Incentive Regulation, you acknowledge that THINK  GREEN SOLAR will not be liable to you.</li>
                            <li>THINK GREEN SOLAR or a THINK GREEN SOLAR Agent will arrange for the complete documentation and processing of the sale of STC's. You acknowledge that the price of STCs is governed by market movements and the REC guidelines. If the market price falls below a certain price, THINK  GREEN SOLAR may decide to delay the installation until the price rises to a higher level. THINK GREEN SOLAR may also refuse to carry out installation and refund your complete deposit. If the market price follow below the certain price, as determined by THINK GREEN SOLAR at its absolute discretion.</li>
                            <li>If you fail to pay any amount that is due and payable under this agreement, THINK GREEN SOLAR will be entitled to charge interest on the unpaid amount at the current Reserve Bank target cash rate plus 2%. You will also have to pay THINK GREEN SOLAR any costs associated with the recovery of such unpaid amounts</li>
                            <li>Title and ownership of the solar system will vets in you upon THINK GREEN SOLAR receiving complete payment from you for the cost of the solar system.</li>
                            <li>Failure to pay the complete amount may result in THINK GREEN SOLAR taking legal action against you and it will void all warranties.</li>
                        </ul>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: 'Calibri'; font-weight: 400; font-size: 7px; color: #333;">
                <tr>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;" width="16">3.</td>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;">Authority of access to property:</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <ul style="margin: 0px; padding: 0px 0px 0px 13px; list-style: lower-alpha; font-weight: 400; font-family: 'Calibri'; line-height: 12px;">
                            <li>You authorise THINK GREEN SOLAR and its contractors, employees and installers. Full access to the property at all reasonable times to carry out all work associated with the installation of your solar system including site Inspections, the signing of required paperwork, the delivery and installation of the PV Solar system, and connection to the grid.</li>
                            <li>Your co-operation is required to enable site inspections/installation to occur at the earliest possible time which is convenient to THINK GREEN SOLAR.</li>
                        </ul>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: 'Calibri'; font-weight: 400; font-size: 7px; color: #333;">
                <tr>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;" width="16">4.</td>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;">Liabilities and Risk:</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <ul style="margin: 0px; padding: 0px 0px 0px 13px; list-style: lower-alpha; font-weight: 400; font-family: 'Calibri'; line-height: 12px;">
                            <li>The ownership and insurance risk of the PV solar system passes to you upon installation and THINK GREEN SOLAR receiving full payment for the solar system. It is your responsibility to ensure that your property insurance adequately covers the cost of your PV solar system.</li>
                            <%--<li>The ownership and insurance risk of the PV solar system passes to you upon installation and EURO SOLAR receiving full payment for the solar system. It is your responsibility to ensure that your property insurance adequately covers the cost of your PV solar system.</li>--%>
                            <li>You acknowledge THINK GREEN SOLAR accepts no liability or responsibility for your STC's Bonus, also known as the "feed in tariff incentive" as administered by the relevant State or Territory government.</li>
                            <li>THINK GREEN SOLAR accepts no responsibility for any damage or loss caused to your property by the installer which has not been caused by the installer's negligence. All THINK GREEN SOLAR installers are subcontractors who are required by THINK GREEN SOLAR to have appropriate third party damage insurance, under relevant state and territory laws. THINK GREEN SOLAR will work with you and the installer to rectify any damage caused to your property by the installer's negligence.</li>
                            <li>You acknowledge that THINK GREEN SOLAR will not be responsible for any damage caused to old and brittle roofing tiles that may be cracked or damaged during installation.</li>
                            <li>THINK GREEN SOLAR accepts no responsibility for any additional costs associated you're your need to obtain an upgrade of your existing meter box upgrade or the installation of a new gross/net. THINK GREEN SOLAR will not be liable for any unexpected costs which may arise in relation to the removal and handling of asbestos at your property in relation to the installation of the solar system</li>
                            <li>If customer is connected with any Electric Hot water/Slab Heating/Climate Saver/Off Peak Meter then customers need to arrange wiring work at their own cost, including installation of timer/contactors/any amps switches to control the system. THINK GREEN SOLAR is not responsible to any costing for that and any delay comes during your grid connection process</li>
                        </ul>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: 'Calibri'; font-weight: 400; font-size: 7px; color: #333;">
                <tr>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;" width="16">5.</td>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;">Meters:</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <ul style="margin: 0px; padding: 0px 0px 0px 13px; list-style: lower-alpha; font-weight: 400; font-family: 'Calibri'; line-height: 12px;">
                            <li>If customer is connected with any Electric Hot water/Slab Heating/Climate Saver/Off Peak Meter then customers need to arrange wiring work at their own cost, including installation of timer/contactors/any amps switches to control the system. THINK GREEN SOLAR is not responsible to any costing for that and any delay comes during your grid connection process. In the event that the meter box is located remotely from the inverter location and such costs are not provided for in the relevant THINK GREEN SOLAR quote, you will be required to arrange and pay for the performance of this electrical work separately from the installation of the solar system.</li>
                            <li>Addition minimum charges may be applicable such as $ 200 to split the system in more than one row, $400 for horizontally fixing array, and extra charges for meter box upgrade, if applicable.</li>
                            <li>THINK GREEN SOLAR quote does not include the costs of reprogramming or reconfiguration of meter from your power distributor or electricity retailer.</li>
                            <li>Also, THINK GREEN SOLAR is not responsible for any cost occurring to replace your meter or truck appointment during the grid connection process</li>
                        </ul>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: 'Calibri'; font-weight: 400; font-size: 7px; color: #333;">
                <tr>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;" width="16">6.</td>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;">Delivery and Installation</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <ul style="margin: 0px; padding: 0px 0px 0px 13px; list-style: lower-alpha; font-weight: 400; font-family: 'Calibri'; line-height: 12px;">
                            <li>THINK GREEN SOLAR will make every reasonable effort to install your system in a timely manner. However, we will not be bound to meeting estimated or proposed delivery, installation or system completion dates as we have no control over for example, worldwide materials availability, peaks in demand created by changes in government legislation, inclement weather and/or other forms of force majeure. Delayed installation or grid connection is not a valid reason for claiming a refund or compensation from THINK GREEN SOLAR. Please note that no responsibility for the delay in installation is acceptable if customer details (including installation site) are incorrect</li>
                            <li>Delays in installation are not grounds for cancellation and THINK GREEN SOLAR is not liable to you for any perceived loss as a result of these delays.
					            <li>If difficulties with site access are encountered that were not notified to THINK GREEN SOLAR at the time of quote and/or offered by THINK GREEN SOLAR to the Customer, additional costs incurred in ensuring the safety of our installers may be payable by the Customer.</li>
                                <li>Before the installation can commence, and modifications to your roof take place, you are required to arrange payment with the scheduling department for the balance payment of the system and that the goods remain the property of THINK GREEN SOLAR until your payment or finance payment, if applicable, is received</li>
                                <li>A Home owner must be present during installation to sign the mandatory declaration assigning the STCs to THINK GREEN SOLAR, as per the renewable Energy Act (2000). Should the installer arrive on the agreed date and the home owner is not present, a rescheduling fee of $200.00 will apply and the installation will be delayed</li>
                                <%--<li>A Home owner must be present during installation to sign the mandatory declaration assigning the STCs to EURO SOLAR, as per the renewable Energy Act (2000). Should the installer arrive on the agreed date and the home owner is not present, a rescheduling fee of $200.00 will apply and the installation will be delayed.</li>--%>
                                <li>Upon signing the agreement, any requests for modifications to the material/equipment will incur an admin charge of $200. A new quote will be generated for any changes required</li>
                        </ul>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: 'Calibri'; font-weight: 400; font-size: 7px; color: #333;">
                <tr>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;" width="16">7.</td>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;">Privacy Policy</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <ul style="margin: 0px; padding: 0px 0px 0px 13px; list-style: lower-alpha; font-weight: 400; font-family: 'Calibri'; line-height: 12px;">
                            <li>You agree to provide THINK GREEN SOLAR with whatever personal information is required for the efficient functioning of THINK GREEN SOLAR on your behalf, in particular for the accurate completion of the paperwork for the STC's incentive (selling the STCs) and network connection to the grid</li>
                            <li>THINK GREEN SOLAR will provide your information to its contractors, employees and installers only as required to effectively perform their duties</li>
                            <li>THINK GREEN SOLAR will provide your information, on your behalf to the relevant bodies for the processing the STC's incentive (selling the STCs), to the electricity distributor for connecting your PV Solar system to the grid and if required to your electricity retailer</li>
                            <li>Unless otherwise agreed with you, THINK GREEN SOLAR will not provide your personal information to any third parties other than those mentioned above</li>
                            <li>You must sign all necessary documents on the date of installation for the performance of all party's obligations under this agreement</li>
                            <li>THINK GREEN SOLAR will not sell your personal information under any circumstances</li>
                        </ul>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: 'Calibri'; font-weight: 400; font-size: 7px; color: #333;">
                <tr>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;" width="16">8.</td>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;">Product Warranties</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <ul style="margin: 0px; padding: 0px 0px 0px 13px; list-style: lower-alpha; font-weight: 400; font-family: 'Calibri'; line-height: 12px;">
                            <li>The system comes with up to 10 years installation warranty from THINK GREEN SOLAR in conjunction with the manufacturer's warranty. Warranty booklet will be presented to you at installation which outlines that the solar system requires servicing every two years by a licenced solar installer. THINK GREEN SOLAR does not provide any kind of warranties on the Monitoring system which comes with the inverter</li>
                        </ul>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: 'Calibri'; font-weight: 400; font-size: 7px; color: #333;">
                <tr>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;" width="16">9.</td>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;">Cooling Off Period</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <p style="font-family: 'Open Sans',sans-serif; font-weight: 400; line-height: 12px; margin: 0px; padding: 0px 0px 0px 0px;">You understand that under Australian Consumer Law and relevant State Building Acts you are entitled to a cooling-off period of ten (10) working days – where required. All notifications must be received by THINK GREEN SOLAR in writing via email, fax or post</p>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: 'Calibri'; font-weight: 400; font-size: 7px; color: #333;">
                <tr>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;" width="20">10.</td>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;">Finance</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <ul style="margin: 0px; padding: 0px 0px 0px 13px; list-style: lower-alpha; font-weight: 400; font-family: 'Calibri'; line-height: 12px;">
                            <li>Financing is not available on advertised specials. Terms and Condition of repayment of finance and interest will be provided by the finance company.</li>
                        </ul>
                    </td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: 'Calibri'; font-weight: 400; font-size: 7px; color: #333;">
                <tr>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;" width="20">11.</td>
                    <td valign="top" style="color: #00b0f0; font-weight: 600;">Termination</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <ul style="margin: 0px; padding: 0px 0px 0px 13px; list-style: lower-alpha; font-weight: 400; font-family: 'Calibri'; line-height: 12px;">
                            <li>THINK GREEN SOLAR may terminate this contract with you if either of the following occurs
                	<ul style="margin: 0px; padding: 0px 0px 0px 10px; list-style: lower-roman">
                        <li>You do not abide by the terms and conditions</li>
                        <li>There are delays in the THINK GREEN SOLAR process causing supplier prices to increase; in which case full deposit will be refunded</li>
                        <li>THINK GREEN SOLAR has full authority to cancel the installation if installation can't be possible. Customer will receive full refund of any deposit by cheque or other method</li>
                        <li>You may terminate this agreement or contract within the first 10 days of quote date. You may be eligible to get a refund of deposit after deducting administration charges. We normally procure the material or equipment required within two weeks of receiving the signed copy of the contract. Customers would have to pay material cost if the job is cancelled after two week of receiving the signed copy of contract</li>
                    </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <p style="margin-top: 90px; margin-bottom: 0px; font-size:7px; padding: 0px 0px 0px 13px; list-style: lower-alpha; font-weight: 400; font-family: 'Calibri'; line-height: 12px; text-align:center;">Address : 409, Francis Street, Brooklyn, VIC – 3012 | 1300 484 784 | <a href="http://www.thinkgreensolar.com.au" target="_blank">www.thinkgreensolar.com.au</a> | <a href="htpp://info@thinkgreensolar.com.au" target="_blank">info@thinkgreensolar.com.au</a> </p>
    <p style="margin: 0px; padding: 0px 0px 0px 13px; font-size:7px; list-style: lower-alpha; font-weight: 400; font-family: 'Calibri'; line-height: 15px; text-align:center;">ABN : 16 611 514 197 | Lic No QLD : 74095 | VIC : 22163 | TAS : 1300870 | NT : C3220</p>
   <%-- <table class="page-break"></table>--%>
    <%--<div>
        <div style="margin: 0px; padding: 0px; left: 0px; top: 0px;">
            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/pdf_img2.jpg") %>" style="width: 100%; height: 100%; display: block;" />
        </div>
    </div>--%>
</body>
</html>
