﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mailtemplate_refundprint : System.Web.UI.Page
{
    public static string MakeImageSrcData(string filename)
    {
        FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
        byte[] filebytes = new byte[fs.Length];
        fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
        return "data:image/png;base64," +
          Convert.ToBase64String(filebytes, Base64FormattingOptions.None);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string RefundID = Request.QueryString["id"];
        
            SttblProjectRefund stref = ClstblProjectRefund.tblProjectRefund_SelectByRefundID(RefundID);

            SttblProjects stpro = ClstblProjects.tblProjects_SelectByProjectID(stref.ProjectID);

            lblProjectNumber.Text = stpro.ProjectNumber;
            lblProjectNumber1.Text = stpro.ProjectNumber;

            SttblProjectStatus stprosta = ClstblProjectStatus.tblProjectStatus_SelectByProjectStatusID(stpro.ProjectStatusID);
            lblProjectStatus.Text = stprosta.ProjectStatus;

            lblSalesRep.Text = stpro.SalesRepName;
            try
            {
                lblentrydate.Text = string.Format("{0:dd MMM, yyyy}", Convert.ToDateTime(stref.CreateDate));
            }
            catch { }
            //lblmanagername.Text = stref;
            lblBSBNo.Text = stref.BSBNo;
            lblAccountantName.Text = stref.AccName;
            lblAmount.Text = stref.Amount;
            lblAccountNo.Text = stref.AccNo;
            lblCustomer.Text = stpro.Customer;
            lblCustomer1.Text = stpro.Customer;
            try
            {
                lblDepositDate.Text = string.Format("{0:dd MMM, yyyy}", Convert.ToDateTime(stpro.DepositReceived));
            }
            catch { }
            //lblDepositPaid.Text = stpro.FDA;
            try
            {
                lblRefundDate.Text = string.Format("{0:dd MMM, yyyy}", Convert.ToDateTime(stref.CreateDate));
            }
            catch { }


            lblNumberPanels.Text = stpro.NumberPanels;
            lblcancelreason.Text = stpro.notes;
        }
    }
}