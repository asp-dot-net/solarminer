﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="resetpassword.aspx.cs" Inherits="mailtemplate_resetpassword" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reset Password Mail</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width='600' border="0" align='left' cellpadding='0' cellspacing='1' style='color: #666;
            font-size: 14px;'>
            <tr>
                <td>
                    <asp:Image ID="emailtop" runat="server" />
                </td>
            </tr>
            <tr bgcolor='#ffffff'>
                <td style="border: 1px solid #c17709;">
                    <table id="mytable" width='600' border="0" align='left' cellpadding='5' cellspacing='1'
                        style='color: #666; font-size: 14px;'>
                        <tr>
                            <td>
                                <span style="font-weight: bold;">Dear&nbsp;<%= Request["username"]%>,</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                Your password has been changed. Your new password is:&nbsp;<b><%= Request["password"]%></b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Best Regards,<br>
                                <b>SolarMiner Team</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Image ID="emailbottom" runat="server" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
