﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="instcancel.aspx.cs" Inherits="mailtemplate_instcancel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width='600' border="0" align='left' cellpadding='0' cellspacing='1' style='color: #666; font-size: 14px;'>
                <tr>
                    <td>
                        <asp:Image ID="emailtop" runat="server" />
                    </td>
                </tr>
                <tr bgcolor='#ffffff'>
                    <td style="border: 1px solid #c17709;">
                        <table id="mytable" width='600' border="0" align='left' cellpadding='5' cellspacing='1'
                            style='color: #666; font-size: 14px;'>
                            <tr>
                                <td style="background-color: #d4d4d4; padding: 5px; color: #000;">
                                    <b>Dear <%= Request["InstallerName"] %></b>
                                </td>
                            </tr>
                            <tr>
                                <td>The following job is cancel which was booked on <%= Request["InstallBookingDate"] %>
                                    <p>
                                        Project No.:&nbsp;<%= Request["ProjectNumber"] %><br />
                                        Customer Name:&nbsp;<%= Request["Customer"] %><br />
                                        Address:&nbsp;<%= Request["Address"] %><br />
                                        Mobile No:&nbsp;<%= Request["MobileNo"] %><br />
                                        Email Address:&nbsp;<%= Request["EmailAddress"] %><br />
                                        System Details:&nbsp;<%= Request["SystemDetails"] %><br />
                                        Installation Note:&nbsp;<%= Request["InstallationNote"] %><br />
                                    </p>
                                    <p>Please give us a call if any concern and issue</p>
                                </td>
                            </tr>
                            <tr>
                                <td>Thanks,<br />
                                    EURO SOLAR<br />
                                    1300 38 76 76
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Image ID="emailbottom" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
