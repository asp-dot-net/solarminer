﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="refundprint.aspx.cs" Inherits="mailtemplate_refundprint" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        .pagewrap {
            width: 980px;
            margin: 0 auto;
            border: 1px solid #000;
            padding: 15px;
        }

        h1 {
            text-align: center;
            text-decoration: underline;
        }

        .label {
            font-size: 20px;
            color: #000;
            font-weight: normal;
            margin-bottom: 15px;
            display: block;
        }

        label span {
            font-weight: bold;
            min-width: 170px;
            display: inline-block;
        }

        .clear {
        }

        .main {
            width: 100%;
            display: inline-block;
        }

            .main .mainleft {
                float: left;
                width: 49%;
            }

            .main .mainright {
                float: right;
                width: 49%;
            }

        .bggray {
            background: #d9d9d9;
            padding: 10px 0;
            min-height: 90px;
            padding: 15px;
            margin: 0 -15px;
        }
    </style>
</head>
<body>
    <div class="pagewrap">
        <center>
        <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/logo.png") %>"
                width="200" /><%--<img src="../images/eurosolarlogo.png" alt="" width="200" />--%></center>
        <h1>Refund Form</h1>
        <div class="main">
            <div class="mainleft">
                <label class="label">
                    <span>Project Number</span> :&nbsp;
                    <asp:Label ID="lblProjectNumber" runat="server"></asp:Label></label>
                <label class="label">
                    <span>Status</span> :&nbsp
                    <asp:Label ID="lblProjectStatus" runat="server"></asp:Label></label>
                <label class="label">
                    <span>Sales Person</span> :&nbsp
                    <asp:Label ID="lblSalesRep" runat="server"></asp:Label></label>

                <label class="label"><span>Deposit Date</span> :&nbsp<asp:Label ID="lblDepositDate" runat="server"></asp:Label></label>
                <%--</label>--%>
                <label class="label"><span>Entry Date</span> :&nbsp<asp:Label ID="lblentrydate" runat="server"></asp:Label></label>
                <%--</label>--%>
                <label class="label"><span>Manager Name</span> :&nbsp<asp:Label ID="lblmanagername" runat="server"></asp:Label></label>
                <%--</label>--%>
                <label class="label">
                    <span>No Of Panel</span> :&nbsp
                        <asp:Label ID="lblNumberPanels" runat="server"></asp:Label></label>
            </div>
            <div class="mainright">
                <label class="label">
                    <span>Customer Name </span>:&nbsp
                    <asp:Label ID="lblCustomer1" runat="server"></asp:Label></label>
                <label class="label"><span>Application </span>: YES NO (SP Ausnet / Energex)</label>
                <%--<label class="label"><span>Deposit Paid </span> : <asp:Label ID="lblDepositPaid" runat="server"></asp:Label>--%> <%--$500.00--%><%--</label>--%>
                <label class="label"><span>Any Promotion Given</span> : YES NO</label>
                <label class="label"><span>(Gift Voucher / Holiday Voucher or other promo)</span></label>
            </div>
            <div class="clear"></div>
        </div>
        <label class="label">
            <span>CANCELL REASON</span> :&nbsp<asp:Label ID="lblcancelreason" runat="server"></asp:Label></label>
        <%--</label>--%>
        <%--<div class="bggray">Due to not following up or confirming that when our job was going to be done</div>--%>
        <h1>Office Use Only</h1>
        <label class="label">
            <span>Project Number</span> :&nbsp
                <asp:Label ID="lblProjectNumber1" runat="server"></asp:Label></label>
        <label class="label">
            <span>Customer</span> :&nbsp
                <asp:Label ID="lblCustomer" runat="server"></asp:Label></label>
        <label class="label"><span>BANK NAME</span> :&nbsp</label>
        <div class="main">
            <div class="mainleft">
                <label class="label">
                    <span>BSB NO</span> :&nbsp
                        <asp:Label ID="lblBSBNo" runat="server"></asp:Label></label>
                <label class="label">
                    <span>Accountant Name</span> :&nbsp
                        <asp:Label ID="lblAccountantName" runat="server"></asp:Label></label>
                <label class="label">
                    <span>Amount</span> :&nbsp
                        <asp:Label ID="lblAmount" runat="server"></asp:Label></label>
            </div>
            <div class="mainright">
                <label class="label">
                    <span>ACCOUNT NO</span> :&nbsp
                        <asp:Label ID="lblAccountNo" runat="server"></asp:Label></label>
                <label class="label"><span>Signature</span> : </label>
                <label class="label">
                    <span>Refund Date</span> :&nbsp
                        <asp:Label ID="lblRefundDate" runat="server"></asp:Label></label>
            </div>
            <div class="clear"></div>
        </div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <label class="label"><span>Reason for not refund</span> :&nbsp</label>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </div>
</body>
</html>
