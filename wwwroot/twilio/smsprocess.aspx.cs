﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class twilio_smsprocess : System.Web.UI.Page
{
    protected string fromnumber;
    protected void Page_Load(object sender, EventArgs e)
    {
        ltmsg.Text = "<?xml version='1.0' encoding='UTF-8'?><Response><Message>Thanks for the message!</Message></Response>";
        fromnumber = Request["From"];
        //fromnumber = fromnumber + Request["Body"];
        fromnumber = fromnumber.Replace("+61", "0");
        string message = Request["Body"];
        //ClstblProjectRefund.testtable_insert(from);

        ClstblPromo.tblPromoLog_Insert(fromnumber, message);
        if (fromnumber != string.Empty)
        {
            string status = "3";
            if (message.ToLower().ToString() == "yes")//1
            {
                status = "1";
            }
            else if (message.ToLower().ToString() == "stop")//2
            {
                status = "2";
                ltmsg.Text = "";
            }

            DataTable dtcheck = ClstblPromo.tblPromo_Checkpromo(fromnumber);
            if (dtcheck.Rows.Count > 0)
            {
                ClstblPromo.tblPromo_UpdateInterested(fromnumber, status);
                ClstblPromo.tblPromo_UpdateResponseMsg(fromnumber, message);
                ClstblPromo.tblPromo_UpdateResponseFlag(fromnumber, "1");
            }
            else
            {
                DataTable dt = ClstblPromo.tblPromo_GetContact(fromnumber);
                if (dt.Rows.Count > 0)
                {
                    ClstblPromo.tblPromo_Insert(dt.Rows[0]["ContactID"].ToString(), "0", "", "", "3", "", "");
                    ClstblPromo.tblPromo_UpdateInterested(fromnumber, status);
                    ClstblPromo.tblPromo_UpdateResponseMsg(fromnumber, message);
                }
            }
           
        }
    }
}