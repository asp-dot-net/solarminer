<%@ Page Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="adminsiteconfiguration.aspx.cs" Inherits="admin_adminfiles_utilities_adminsiteconfiguration"
     Theme="admin" Culture="en-us" UICulture="en-us"
    EnableEventValidation="true" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div id="breadCrumbs">
        <ul>
            <li class="breadCrumbLast" title="Site Configuration"><a href="#"><strong>Manage Site Configuration</strong></a></li>
        </ul>
    </div>
    <div id="main">
        <div id="globalTabContent">
            <ul class="tabs-nav">
                <li class="tabs-selected"><a href="#tab-1"><span><strong>
                    <asp:Literal ID="lblAddUpdate" runat="server" Text=""></asp:Literal>
                    Site Configuration</strong></span></a></li>
               
            </ul>
            <div id="tab-1" class="tabs-container">
                <div class="curveTop">
                </div>

                <%-- START  : Error Panels --%>
                <div style="padding: 10px!important;">
                    <asp:Panel ID="PanSuccess" runat="server" CssClass="tick">
                        <asp:Label ID="lblSuccess" runat="server" Text="Transaction Successful."></asp:Label>
                    </asp:Panel>
                    <asp:Panel ID="PanError" runat="server" CssClass="cross">
                        <asp:Label ID="lblError" runat="server" Text="Transaction Failed."></asp:Label>
                    </asp:Panel>
                    <asp:Panel ID="PanNoRecord" runat="server" CssClass="cross">
                        <asp:Label ID="lblNoRecord" runat="server" Text="There are no items to show in this view."></asp:Label>
                    </asp:Panel>
                </div>
                <%-- END    : Error Panels --%>

                <%-- START  : ADD / UPDATE Panels --%>
                <asp:Panel ID="PanAddUpdate" runat="server" Visible="false">
                    <div class="globalContainerCA">
                        <ul class="addNewPage" style="width: 100%;">
                          
                            <li>
                                <label style="float: left;">
                                    <strong>Level</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtlevel" runat="server" Width="200"></asp:TextBox>


                            </li>
                            <li>
                                <label style="float: left;">
                                    <strong>Currency Digit</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtCurrencyDigit" runat="server" Width="200"></asp:TextBox>


                            </li>

                            <li>

                                <label style="float: left;">
                                    <strong>Symbol</strong><span class="highlight"></span></label>
                                <asp:TextBox ID="txtsymbol" runat="server" Width="200" MaxLength="50"></asp:TextBox>


                            </li>


                            <li>
                                <label style="float: left;">
                                    <strong>Category Large</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtCatLarge" runat="server" Width="200"></asp:TextBox>


                            </li>


                            <li>
                                <label id="trusername" style="float: left;">
                                    <strong>Category Medium</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtCatMedium" runat="server" Width="200"></asp:TextBox>

                            </li>
                            <li>
                                <label id="trpassword" style="float: left;">
                                    <strong>Category Small</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtCatSmall" runat="server" Width="200"></asp:TextBox>


                            </li>


                            <li  >

                                <label style="float: left;">
                                    <strong>Product Large</strong><span class="highlight"></span></label>


                                <asp:TextBox ID="txtProductLarge" runat="server" Width="200"></asp:TextBox>

                            </li>

                            <li>

                                <label  style="float: left;" >
                                    <strong>Product Medium</strong><span class="highlight"></span></label>


                                <asp:TextBox ID="txtProductMedium" runat="server" Width="200"></asp:TextBox>

                            </li>

                            <li>
                                <label style="float: left;">
                                    <strong>Product Small</strong><span class="highlight"></span></label>
                                <asp:TextBox ID="txtProductSmall" runat="server" Width="200"></asp:TextBox>
                            </li>

                            <li>
                                <label style="float: left;">
                                    <strong>Banner Large</strong><span class="highlight"></span></label>
                                <asp:TextBox ID="txtBannerLarge" runat="server" Width="200"></asp:TextBox>
                            </li>

                            <li>
                                <label style="float: left;">
                                    <strong>Banner Medium</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtBannerMedium" runat="server" Width="200"></asp:TextBox>

                            </li>

                            <li>
                                <label style="float: left;">
                                    <strong>Banner Small</strong><span class="highlight"></span></label>

                                <asp:TextBox ID="txtBannerSmall" runat="server" Width="200"></asp:TextBox>

                            </li>


                           
                            <li>

                                <td>&nbsp;
                                </td>
                                <td>
                                    <asp:ImageButton ID="btnUpdate" runat="server" ImageUrl="~/admin/images/save.gif"
                                        OnClick="btnUpdate_Click" Visible="true" />
                                </td>

                            </li>





                        </ul>
                       <%-- <ul class="addNewPageEditor" runat="server" visible="false">
                            <li class="floatLeft">

                              
                                <p>
                                    <asp:ImageButton ID="btnUpdate1" runat="server" ImageUrl="~/admin/images/save.gif"
                                        OnClick="btnUpdate_Click" Visible="false" />
                                </p>
                              
                            </li>
                        </ul>--%>
                        <div class="clear">
                        </div>
                    </div>
                </asp:Panel>


                <div class="curveBottom">
                    <img height="8" width="8" src="../../images/bl.jpg" alt="" class="curveCorner" />
                </div>
            </div>
        </div>
    </div>


</asp:Content>
