﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/admin/templates/MasterPageAdmin.master" CodeFile="Profile.aspx.cs" Inherits="admin_Profile_Profile" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/includes/Profile/Contact.ascx" TagName="contact" TagPrefix="uc1" %>
<%@ Register Src="~/includes/Profile/Setting.ascx" TagName="setting" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <asp:UpdatePanel runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Update Profile
          <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
        </h5>

    </div>


    <div class="panel-body animate-panel padtopzero">

        <asp:Panel runat="server" ID="PanGridSearch">
            <div class="searchfinal">

                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-container">
                            <div class="profile-header row">
                                <div class="col-lg-2 col-md-4 col-sm-12 text-center" style="margin-top: 20px;">
                                    <asp:Image runat="server" class="avatar" ID="EmpImage" Style="width: 128px; height: 128px;" AlternateText="..." />
                                </div>
                                <div class="col-lg-5 col-md-8 col-sm-12 profile-info" style="border-right: none">

                                    <div class="header-information">
                                        <h3 class="font-bold m-b-none m-t-none">
                                            <asp:Literal runat="server" ID="lblfullname"></asp:Literal></h3>
                                        <p>
                                            <asp:Literal runat="server" ID="ltemail"></asp:Literal>
                                        </p>
                                    </div>
                                </div>



                                <div class="row">
                                    <div>
                                        <div class="hpanel">
                                            <asp:Literal ID="lblCount" runat="server"></asp:Literal>
                                            <div class="bodymianbg">
                                                <div>
                                                    <div class="col-md-12" id="divright" runat="server">
                                                        <section class="scrollable bg-white">

                                                            <ul class="nav nav-tabs m-b-n-xxs bg-light">
                                                                <%--<li class="active"> <a href="#activities" data-toggle="tab" class="m-l">Activities<span class="badge bg-primary badge-sm m-l-xs">10</span></a> </li>--%>
                                                                <li class="active"><a href="#bio" data-toggle="tab" class="m-l ">Contact</a> </li>
                                                                <li><a href="#edit" data-toggle="tab">Edit profile</a> </li>
                                                            </ul>
                                                            <div class="tab-content tab_white marginleftright15">

                                                                <div class="tab-pane wrapper-lg active" id="bio">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="profile-contacts">

                                                                                <div class="profile-badge orange"><i class="fa fa-phone orange"></i><span>Contacts</span></div>
                                                                                <div class="contact-info">
                                                                                    <p id="divcell" runat="server">
                                                                                        <asp:Label ID="lblphone" Visible="false" runat="server"></asp:Label><br />
                                                                                        <asp:Label ID="lblcell" Visible="false" runat="server"></asp:Label><br>
                                                                                        <asp:Label ID="lblEmail" Visible="false" runat="server"></asp:Label>

                                                                                    </p>
                                                                                    <p>
                                                                                    </p>

                                                                                </div>
                                                                                <div class="profile-badge azure">
                                                                                    <i class="fa fa-map-marker azure"></i><span>Location</span>
                                                                                </div>
                                                                                <div class="contact-info">

                                                                                    <p>
                                                                                        Office<br>
                                                                                        Level 6/140 Creek Street,
                                                                                        <br />
                                                                                        Brisbane QLD 4000
                                                                                      
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3540.097712722561!2d153.02522671465!3d-27.46621718289238!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b915a1d3ab8c271%3A0x66b67afdfa77bb3f!2s6%2F140+Creek+St%2C+Brisbane+City+QLD+4000%2C+Australia!5e0!3m2!1sen!2sus!4v1516085876454" width="100%;" max-width="550px;" height="250" frameborder="2" style="border: 0" allowfullscreen></iframe>
                                                                        </div>
                                                                    </div>
                                                                </div>





                                                                <div class="tab-pane wrapper-lg" id="edit">

                                                                    <div id="settings" class="tab-pane">
                                                                        <form role="form">
                                                                            <div class="form-title">
                                                                                Personal Information
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <span class="input-icon icon-right">
                                                                                            <asp:TextBox ID="txtEmpFirst" runat="server" MaxLength="200" placeholder="First Name" CssClass="form-control"></asp:TextBox>
                                                                                            <i class="fa fa-user blue" style="z-index: 99"></i>
                                                                                        </span>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                            ControlToValidate="txtEmpFirst" Display="Dynamic" ValidationGroup="profile"></asp:RequiredFieldValidator>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <span class="input-icon icon-right">
                                                                                            <asp:TextBox ID="txtEmpLast" runat="server" MaxLength="200" placeholde="Last Name" CssClass="form-control"></asp:TextBox>
                                                                                            <i class="fa fa-user purple" style="z-index: 99"></i>
                                                                                        </span>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                            ControlToValidate="txtEmpLast" Display="Dynamic" ValidationGroup="profile"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <span class="input-icon icon-right">
                                                                                            <asp:TextBox ID="txtEmpMobile" placeholder="Phone" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                            <i class="glyphicon glyphicon-earphone yellow" style="z-index: 99"></i>
                                                                                        </span>
                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEmpMobile" SetFocusOnError="true" Display="Dynamic"
                                                                                            ValidationGroup="profile" ErrorMessage="Please enter valid mobile number (eg. 04XXXXXXXX)" ForeColor="Red"
                                                                                            ValidationExpression="^04[\d]{8}$"></asp:RegularExpressionValidator>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                            ControlToValidate="txtEmpMobile" Display="Dynamic" ValidationGroup="profile"></asp:RequiredFieldValidator>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <span class="input-icon icon-right">
                                                                                            <asp:TextBox ID="txtEmpPhone" placeholder="Phone" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>

                                                                                            <i class="glyphicon glyphicon-phone palegreen" style="z-index: 99"></i>
                                                                                        </span>
                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtEmpPhone"
                                                                                            ValidationGroup="profile" Display="Dynamic" ErrorMessage="Please enter valid number (eg. 07/03/08XXXXXXXX)"
                                                                                            ValidationExpression="^(07|03|08)[\d]{8}$"></asp:RegularExpressionValidator>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <span class="input-icon icon-right">
                                                                                            <asp:TextBox ID="txtEmpTitle" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                            <i class="fa fa-user palegreen" style="z-index: 99"></i>
                                                                                        </span>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <span class="input-icon icon-right">
                                                                                            <asp:TextBox ID="txtEmpInitials" runat="server" MaxLength="5" CssClass="form-control"></asp:TextBox>
                                                                                            <i class="fa fa-user yellow" style="z-index: 99"></i>
                                                                                        </span>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                            ControlToValidate="txtEmpInitials" Display="Dynamic" ValidationGroup="profile"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <span class="input-icon icon-right">
                                                                                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="200" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                                            <i class="glyphicon glyphicon-envelope purple" style="z-index: 99"></i>
                                                                                        </span>

                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <span class="input-icon icon-right">
                                                                                            <asp:TextBox ID="txtEmpNicName" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                            <i class="fa fa-user orange" style="z-index: 99"></i>
                                                                                        </span>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                                                            ControlToValidate="txtEmpNicName" Display="Dynamic" ValidationGroup="profile"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                            <div class="form-title">
                                                                                Password
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <span class="input-icon icon-right">
                                                                                            <asp:TextBox ID="NewPassword" runat="server" placeholder="Password" TextMode="Password" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                                                            <i class="glyphicon glyphicon-asterisk palegreen" style="z-index: 99"></i>
                                                                                        </span>
                                                                                        <asp:RegularExpressionValidator Display="Dynamic" ID="regConfirmPassword" runat="server" ValidationGroup="profile"
                                                                                            ControlToValidate="NewPassword" ErrorMessage="Minimum password length is 5" ValidationExpression=".{5}.*" />
                                                                                        <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                                                                                            ErrorMessage="This value is required." CssClass="comperror" ToolTip="New Password is required." Display="Dynamic"
                                                                                            ValidationGroup="profile"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <span class="input-icon icon-right">
                                                                                            <asp:TextBox ID="ConfirmNewPassword" runat="server" placeholder="Password Again" TextMode="Password" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                                                            <i class="glyphicon glyphicon-asterisk yellow" style="z-index: 99"></i>
                                                                                        </span>
                                                                                        <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                                                                                            ControlToValidate="ConfirmNewPassword" Display="Dynamic" ErrorMessage="The Confirm New Password must match the New Password entry."
                                                                                            ValidationGroup="profile"></asp:CompareValidator>
                                                                                        <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                                                                                            ErrorMessage="This value is required." CssClass="comperror" ToolTip="Confirm New Password is required." Display="Dynamic"
                                                                                            ValidationGroup="profile"></asp:RequiredFieldValidator>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="form-title">
                                                                                Contact Information
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <span class="input-icon icon-right">
                                                                                            <asp:TextBox ID="txtcity" placeholder="City" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                            <i class="fa fa-map-marker orange" style="z-index: 99"></i>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <span class="input-icon icon-right">
                                                                                            <asp:TextBox ID="txtstate" placeholder="State" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                            <i class="fa fa-map-marker yellow" style="z-index: 99"></i>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <span class="input-icon icon-right">
                                                                                            <asp:TextBox ID="txtaddress" placeholder="Address" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                            <i class="fa fa-map-marker palegreen" style="z-index: 99"></i>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <div class="form-group">
                                                                                        <span class="input-icon icon-right">
                                                                                            <asp:TextBox ID="txtpostcode" placeholder="Post Code" runat="server" MaxLength="200" CssClass="form-control"></asp:TextBox>
                                                                                            <i class="fa fa-map-marker purple" style="z-index: 99"></i>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" CssClass="  btn btn-primary" Text="Update" ValidationGroup="profile" />
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </section>
                                                    </div>
                                                </div>
                                            </div>



                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>




    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%=btnUpdate.ClientID %>').click(function () {
                formValidate();
            });
        });
    </script>


</asp:Content>
