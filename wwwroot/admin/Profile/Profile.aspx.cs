﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_Profile_Profile : System.Web.UI.Page
{
    protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            bind();
        }
    }

    public void bind()
    {
        if (Request.IsAuthenticated)
        {
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            int i = 0;
            if (st.EmpFirst != string.Empty)
            {
                txtEmpFirst.Text = st.EmpFirst;
                i++;
            }

            if (st.EmpLast != string.Empty)
            {
                txtEmpLast.Text = st.EmpLast;
                i++;
            }
            if (st.EmpTitle != string.Empty)
            {
                txtEmpTitle.Text = st.EmpTitle;
                i++;
            }
            if (st.EmpInitials != string.Empty)
            {
                txtEmpInitials.Text = st.EmpInitials;
                i++;
            }
            if (st.EmpNicName != string.Empty)
            {
                txtEmpNicName.Text = st.EmpNicName;
                i++;
            }

            txtEmail.Text = st.EmpEmail;

            if (st.EmpMobile != string.Empty)
            {
                txtEmpMobile.Text = st.EmpMobile;
                i++;
            }

            if (st.EmpPhone != string.Empty)
            {
                txtEmpPhone.Text = st.EmpPhone;
                i++;
            }
            if (st.EmpPostCode != string.Empty)
            {
                txtpostcode.Text = st.EmpPostCode;
                i++;
            }

            if (st.EmpState != string.Empty)
            {
                txtstate.Text = st.EmpState;
                i++;
            }
            if (st.EmpCity != string.Empty)
            {
                txtcity.Text = st.EmpCity;
                i++;
            }
            if (st.EmpAddress != string.Empty)
            {
                txtaddress.Text = st.EmpAddress;
                i++;
            }


            if (st.EmpImage != string.Empty)
            {
                //EmpImage.ImageUrl = "~/userfiles/EmployeeProfile/" + st.EmpImage;
                EmpImage.ImageUrl = pdfURL + "EmployeeProfile/" + st.EmpImage;
                i++;
            }
            else
            {
                EmpImage.ImageUrl = "~/images/anynomous.png";
            }

            
            lblfullname.Text = st.EmpFirst + " " + st.EmpLast;
            ltemail.Text = st.EmpEmail;
            //ltname.Text = st.EmpFirst;

            if (!string.IsNullOrEmpty(st.EmpPhone))
            {
                lblphone.Text = "Phone : +" + st.EmpPhone;
                lblphone.Visible = true;
            }
            else
            {

                lblphone.Visible = false;
            }
            if (!string.IsNullOrEmpty(st.EmpMobile))
            {
                lblcell.Text = "Cell : +" + st.EmpMobile;
                lblcell.Visible = true;
            }
            else
            {
                lblcell.Visible = false;
            }

            if (!string.IsNullOrEmpty(st.EmpEmail))
            {
                lblEmail.Text = "Email : " + st.EmpEmail;
                lblEmail.Visible = true;
            }
            else
            {
                lblEmail.Visible = false;
            }

            //if (st.EmpBio != string.Empty)
            //{
            //    ltbio.Text = st.EmpBio;
            //    divbio.Visible = true;
            //}
            //else
            //{
            //    divbio.Visible = false;
            //}

            //if (st.EmpAddress != string.Empty)
            //{
            //    ltaddress.Text = st.EmpAddress;
            //    divaddress.Visible = true;
            //}
            //else
            //{
            //    divaddress.Visible = false;
            //}

            //if (st.EmpCity != string.Empty)
            //{
            //    ltcity.Text = st.EmpCity;
            //    divcity.Visible = true;
            //}
            //else
            //{
            //    divcity.Visible = false;
            //}

            //if (st.EmpState != string.Empty)
            //{
            //    ltstate.Text = st.EmpState;
            //    divstate.Visible = true;
            //}
            //else
            //{
            //    divstate.Visible = false;
            //}

            //if (st.EmpPostCode != string.Empty)
            //{
            //    ltpostcode.Text = st.EmpPostCode;
            //    divpostcode.Visible = true;
            //}
            //else
            //{
            //    divpostcode.Visible = false;
            //}

            //if (st.HireDate != string.Empty)
            //{
            //    ltdatehire.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(st.HireDate));
            //    divdatehire.Visible = true;
            //}
            //else
            //{
            //    divdatehire.Visible = false;
            //}

            //if (st.LocationName != string.Empty)
            //{
            //    ltlocation.Text = st.LocationName;
            //    divlocation.Visible = true;
            //}
            //else
            //{
            //    divlocation.Visible = false;
            //}

            //if (st.TeamName != string.Empty)
            //{
            //    ltteam.Text = st.TeamName;
            //    divteam.Visible = true;
            //}
            //else
            //{
            //    divteam.Visible = false;
            //}

            //if (st.EmpType != string.Empty)
            //{
            //    if (st.EmpType == "1")
            //    {
            //        ltemptype.Text = "Eurosolar";
            //    }
            //    if (st.EmpType == "2")
            //    {
            //        ltemptype.Text = "Door to Door";
            //    }
            //    divemptype.Visible = true;
            //}
            //else
            //{
            //    divemptype.Visible = false;
            //}

            //progress bar
            decimal count = (100 * i) / 12;
            //ltper.Text = count.ToString();
            //divper.Attributes.Add("style", "width: " + count.ToString() + "%");
            //divper.Attributes.Add("data-original-title", count.ToString() + "%");
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        ClstblEmployees.aspnetmembership_Update_Password(userid, NewPassword.Text);
        ClstblEmployees.tblEmployees_UpdateProfileDetails(userid, txtEmpFirst.Text, txtEmpLast.Text, txtEmpTitle.Text, txtEmpInitials.Text, txtEmpMobile.Text, NewPassword.Text, txtEmpNicName.Text);

        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserID(userid);
        string employeeid = st.EmployeeID;

        string image = st.EmpImage;
        bool success = ClstblEmployees.tblEmployees_UpdateEmpAddressDetails(employeeid, txtaddress.Text, txtcity.Text, txtstate.Text, txtpostcode.Text);

        if (success)
        {
            SetAdd1();
            bind();
        }
        else
        {
            SetError1();
        }

    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }
}