﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class admin_include_SideMenu : System.Web.UI.UserControl
{
    protected static string Siteurl;
    private static RepeaterItem parentItem = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            string[] rolename = Roles.GetRolesForUser();
            //lblRolename.Text = rolename[0].ToString();
         //   lblUsserName.Text = "";
            SiteMapNodeCollection modifiableCollection = new SiteMapNodeCollection(SiteMap.RootNode.ChildNodes);
            foreach (SiteMapNode node in SiteMap.RootNode.ChildNodes)
            {
                if (rolename.Length > 0)
                {
                    bool isrole = false;
                    for (int j = 0; j < rolename.Length; j++)
                    {
                        if (node.Roles.Contains(rolename[j].ToString()))
                        {
                            isrole = true;
                        }
                    }
                    if (isrole != true)
                    {
                        modifiableCollection.Remove(node);
                    }
                    //Response.Write(isrole);
                }
            }

            menu1.DataSource = modifiableCollection;
            menu1.DataBind();

            modifiableCollection = null;
        }
    }
    protected void LoginStatus1_LoggedOut(object sender, EventArgs e)
    {
        Session["userid"] = "";
        Session.Abandon();
        Response.Redirect("~/Default.aspx");
    }

    protected void menu1_onitemdatabound(object sender, RepeaterItemEventArgs e)
    {

        string pagename = Utilities.GetPageName(Request.Url.ToString()).ToLower();

        parentItem = (RepeaterItem)e.Item;

        //string foldername = Utilities.GetPageFolderName(Request.Url.PathAndQuery).ToLower();
        SiteMapNode currentRepeaterNode = (SiteMapNode)e.Item.DataItem;
        SiteMapNodeCollection subnodes = currentRepeaterNode.ChildNodes;
        LinkButton lnkmain = e.Item.FindControl("hdnHeaderMenu") as LinkButton;
        HtmlGenericControl UL1 = e.Item.FindControl("UL1") as HtmlGenericControl;
        Label lblarraow = e.Item.FindControl("lblarraow") as Label;
        Repeater repsubmenu = e.Item.FindControl("repsubmenu") as Repeater;
        LinkButton hdnheadermenu = e.Item.FindControl("hdnheadermenu") as LinkButton;
        HtmlGenericControl limain = e.Item.FindControl("limain") as HtmlGenericControl;
        string desc = currentRepeaterNode.Description;
        //if ((pagename == "dashboard"))
        //{
        //    pagename = "Dashboard";
        //}
        HiddenField hdndesc = e.Item.FindControl("hdndesc") as HiddenField;


        HiddenField hdndesc1 = e.Item.FindControl("hdndesc1") as HiddenField;
        string[] words1 = hdndesc1.Value.Split(',');
        foreach (string first in words1)
        {

            //Response.Write(first.ToLower() + "=>" + pagename);
            //Response.End();
            if (first.ToLower() == pagename)
            {
                limain.Attributes.Add("class", "active");
            }
        }
      
    





        string[] rolename = Roles.GetRolesForUser();
        SiteMapNodeCollection collectionnode = new SiteMapNodeCollection();

        foreach (SiteMapNode node in currentRepeaterNode.ChildNodes)
        {
            if (rolename.Length > 0)
            {
                for (int j1 = 0; j1 < rolename.Length; j1++)
                {
                    if (node.Roles.Contains(rolename[j1].ToString()) && !collectionnode.Contains(node))
                    {
                        collectionnode.Add(node);
                    }
                }
            }
        }
        repsubmenu.DataSource = collectionnode;
        repsubmenu.DataBind();

        if (collectionnode.Count > 0)
        {
            lblarraow.Visible = true;
        }
        if (currentRepeaterNode.ChildNodes.Count == 0)
        {
            UL1.Visible = false;
        }
        int j = 0;
        for (int i = 0; i <= subnodes.Count - 1; i++)
        {
            if (rolename.Length > 0)
            {
                for (int k = 0; k < rolename.Length; k++)
                {
                    if (subnodes[i].Roles.Contains(rolename[k]))
                    {
                        if (j == 0)
                        {
                            lnkmain.CommandArgument = subnodes[i].Url;
                            j = 1;
                        }
                    }
                }
            }
        }
    }
    protected void menu1_onitemcommand(object sender, RepeaterCommandEventArgs e)
    {
        Response.Redirect(e.CommandArgument.ToString());
    }

    protected void repsubmenu_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        string pagename = Utilities.GetPageName(Request.Url.ToString()).ToLower();
        HiddenField hdndesc = e.Item.FindControl("hdndesc") as HiddenField;
       
        
        HtmlGenericControl lisub = e.Item.FindControl("lisub") as HtmlGenericControl;
        //Response.Write(hdndesc.Value);
        //Response.End();
        string[] words = hdndesc.Value.Split(',');
        foreach (string word in words)
        {
           
           
            if (word.ToLower() == pagename)
            {
                lisub.Attributes.Add("class", "active open");
             //   hdnheadermenu.Attributes.Add("class", "active");

                HtmlGenericControl limain = e.Item.Parent.Parent.FindControl("limain") as HtmlGenericControl;
                limain.Attributes.Add("class", "open");
            }
        }
       
      

        //SiteMapNode currentRepeaterNode = (SiteMapNode)e.Item.DataItem;
        //SiteMapNodeCollection subnodes = currentRepeaterNode.ChildNodes;
        //HtmlGenericControl UL2 = e.Item.FindControl("UL2") as HtmlGenericControl;
        //Repeater repsubmenu = e.Item.FindControl("repsubmenu2") as Repeater;

        //string pagename = Utilities.GetPageName(Request.Url.PathAndQuery).ToLower();
        //HiddenField hdn_Pagename = e.Item.FindControl("hdnPagename") as HiddenField;
        //HiddenField linkname = e.Item.FindControl("linkname") as HiddenField;
        //string foldername = Utilities.GetPageFolderName(Request.Url.PathAndQuery).ToLower();
        //if (pagename == "type" && pagename == "nature" && pagename == "lc" && pagename == "lcdetails" && pagename == "lctodo" && pagename == "lctodo_details")
        //{
        //    pagename = "lc";
        //}
        //if (pagename == "lcdetails")
        //{
        //    pagename = "lc";
        //}
        //if (pagename == "lctodo_details")
        //{
        //    pagename = "lctodo";
        //}
        //if (pagename.ToLower() == hdn_Pagename.Value.ToLower())
        //{
        //    HtmlGenericControl hyp_submenu = e.Item.FindControl("lisub") as HtmlGenericControl;
        //    hyp_submenu.Attributes.Add("class", "active");
        //}
        //string[] rolename = Roles.GetRolesForUser();
        //SiteMapNodeCollection collectionnode = new SiteMapNodeCollection();
        //foreach (SiteMapNode node in currentRepeaterNode.ChildNodes)
        //{
        //    if (rolename.Length > 0)
        //    {
        //        for (int j1 = 0; j1 < rolename.Length; j1++)
        //        {
        //            if (node.Roles.Contains(rolename[j1].ToString()) && !collectionnode.Contains(node))
        //            {
        //                collectionnode.Add(node);
        //            }
        //        }
        //    }
        //}
        //if (currentRepeaterNode.ChildNodes.Count == 0)
        //{
        //    UL2.Visible = false;
        //}
        //repsubmenu.DataSource = collectionnode;
        //repsubmenu.DataBind();
    }
}