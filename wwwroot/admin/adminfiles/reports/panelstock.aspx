﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    Culture="en-GB" UICulture="en-GB" AutoEventWireup="true" CodeFile="panelstock.aspx.cs" Inherits="admin_adminfiles_reports_panelstock" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../../includes/reports.ascx" TagName="reports" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
            <div class="finalheader printorder">
    <div class="small-header transition animated fadeIn printorder">
        <div class="hpanel">
            <div class="panel-body">
                <div id="tdExport" class="pull-right" runat="server">
                </div>
                <h2 class="font-light m-b-xs">Panel Stock Report
                </h2>
            </div>
        </div>
    </div>
                </div>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                }
                function endrequesthandler(sender, args) {
                }
                function pageLoaded() {
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                }
            </script>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                    $('.loading-container').css('display', 'none');
                }

            </script>
            <asp:Panel runat="server" ID="PanGridSearch" CssClass="printorder">
                <div class="content animate-panel" style="padding-bottom: 0px!important;">
                    <div class="messesgarea">

                        <div class="alert alert-info" id="PanNoRecord" visible="false" runat="server">
                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                        </div>
                    </div>
                </div>
                   <div class="searchfinal">
                <div class="panel-body animate-panel padbtmzero padtopzero">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="hpanel marbtmzero">
                                <%--<div class="panel-heading">
                            <div class="panel-tools">
                                
                            </div>
                            <br />
                        </div>--%>
                                <div class="panel-body">
                                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                        <div class="row">
                                            <div>
                                                <div class="dataTables_filter col-sm-11 Responsive-search printorder">
                                                    <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <div class="input-group date datetimepicker1 col-sm-2">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                        ControlToValidate="txtStartDate" CssClass="errormessage" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <div class="input-group date datetimepicker1 col-sm-2">
                                                                    <span class="input-group-addon">
                                                                        <span class="fa fa-calendar"></span>
                                                                    </span>
                                                                    <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                        ControlToValidate="txtEndDate" CssClass="errormessage" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                                    <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                        ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                        Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                                </div>
                                                                <div class="input-group">
                                                                    <asp:Button ID="btnSearch" runat="server" ValidationGroup="search" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                </div>
                                                                 <div class="input-group pull-right">
                                                    <div id="Div2" class="pull-right" runat="server">
                                                        <a href="javascript:window.print();" class="printicon">
                                                            <i class="fa fa-print"></i>
                                                        </a>
                                                    </div>
                                                </div>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                               
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                       </div>
                <%--</div>--%>
            </asp:Panel>
              <div class="finalgrid panel-body">
            <div class="xscroll">
                <div class="">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer xscroll" id="PanAddUpdate" runat="server">
                        <div>

                            <div class="dataTables_filter  Responsive-search xscroll printArea" style="border: 0px solid #000; display: inline-block;">
                                <table border="0" cellspacing="0" style="text-align: right; margin-bottom: 10px;" cellpadding="0" class="table table-striped table-bordered tableprint">
                                    <tr>
                                        <td style="width: 200px;" class="left-text"><b>Stock Item</b>
                                        </td>
                                        <asp:Repeater ID="rptState" runat="server">
                                            <ItemTemplate>
                                                <td colspan="4" style="text-align: center;">
                                                    <b><%# Eval("State") %></b>
                                                </td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <asp:Repeater ID="rptState2" runat="server">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hndState2" runat="server" Value='<%#Eval("State") %>' />
                                                <td class="center-text"><b>Total Sold</b></td>
                                                <td class="center-text"><b>Real Stock</b></td>
                                                <td class="center-text"><b>Diff.</b></td>
                                                <td class="center-text"><b>Order</b></td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tr>
                                    <asp:Repeater ID="rptStockItem" runat="server" OnItemDataBound="rptStockItem_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="nowraptext">
                                                    <asp:HiddenField ID="hndStockItem" runat="server" Value='<%#Eval("StockItemID") %>' />
                                                    <b><%#Eval("StockItem") %></b>
                                                </td>
                                                <asp:Repeater ID="rptHidState" runat="server" OnItemDataBound="rptHidState_ItemDataBound">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hndState" runat="server" Value='<%#Eval("State")%>' />
                                                        <asp:Repeater ID="rptCount" runat="server">
                                                            <ItemTemplate>
                                                                <td class="center-text"><%#Eval("totalsold") %></td>
                                                                <td class="center-text"><%#Eval("realstock") %></td>
                                                                <td class="center-text"><%#Eval("diff") %></td>
                                                                <td class="center-text"><%#Eval("order") %></td>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                  </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
