﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_stockusage : System.Web.UI.Page
{
    static DataView dv;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            BindDropDown();
            BindGrid(0);
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                tdExport.Visible = true;
            }
            else
            {
                tdExport.Visible = false;
            }
        }
    }
    public void BindDropDown()
    {

        ddlLocation.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddlLocation.DataMember = "location";
        ddlLocation.DataTextField = "location";
        ddlLocation.DataValueField = "CompanyLocationID";
        ddlLocation.DataBind();

        ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlInstaller.DataTextField = "Contact";
        ddlInstaller.DataValueField = "ContactID";
        ddlInstaller.DataBind();

    }
    protected DataTable GetGridData()
    {
        string Location = "";
        try
        {
            if (ddlLocation.SelectedValue == "")
            {
                ddlLocation.SelectedValue = "5";
                Location = ddlLocation.SelectedValue;
            }
            else
            {
                Location = ddlLocation.SelectedValue;
            }
        }
        catch
        {
            Location = ddlLocation.SelectedValue;
        }
        //Response.Write(Location + "','" + ddlStockCategory.SelectedValue + "','" + ddlInstaller.SelectedValue + "','" + txtStartDate.Text.Trim() + "','" + txtEndDate.Text.Trim());
        //Response.End();
        DataTable dt = ClstblStockItems.tblStockItems_SelectUsage(Location, ddlStockCategory.SelectedValue, ddlInstaller.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim());

        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGridSearch.Visible = true;
        //PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanAddUpdate.Visible = false;
        }
        else
        {
            rptModules.DataSource = dt;
            rptModules.DataBind();
            if (ddlshowdata.SelectedValue == "1")
            {
                panel.Visible = true;
                Stockfrom.Visible = true;
                Instock.Visible = true;

                foreach (RepeaterItem item in rptModules.Items)
                {
                    System.Web.UI.HtmlControls.HtmlContainerControl rptpanel = (System.Web.UI.HtmlControls.HtmlContainerControl)item.FindControl("rptpanel");
                    System.Web.UI.HtmlControls.HtmlContainerControl rptStockFrom = (System.Web.UI.HtmlControls.HtmlContainerControl)item.FindControl("rptStockFrom");
                    System.Web.UI.HtmlControls.HtmlContainerControl rptInStock = (System.Web.UI.HtmlControls.HtmlContainerControl)item.FindControl("rptInStock");
                    Repeater rptdet = (Repeater)item.FindControl("rptdet");
                    rptpanel.Visible = true;
                    rptStockFrom.Visible = true;
                    rptInStock.Visible = true;
                    rptdet.Visible = true;
                }
            }
            if (ddlshowdata.SelectedValue == "2")
            {
                panel.Visible = false;
                Stockfrom.Visible = false;
                Instock.Visible = false;
                foreach (RepeaterItem item in rptModules.Items)
                {
                    System.Web.UI.HtmlControls.HtmlContainerControl rptpanel = (System.Web.UI.HtmlControls.HtmlContainerControl)item.FindControl("rptpanel");
                    System.Web.UI.HtmlControls.HtmlContainerControl rptStockFrom = (System.Web.UI.HtmlControls.HtmlContainerControl)item.FindControl("rptStockFrom");
                    System.Web.UI.HtmlControls.HtmlContainerControl rptInStock = (System.Web.UI.HtmlControls.HtmlContainerControl)item.FindControl("rptInStock");
                    Repeater rptdet = (Repeater)item.FindControl("rptdet");
                    rptpanel.Visible = false;
                    rptStockFrom.Visible = false;
                    rptInStock.Visible = false;
                    rptdet.Visible = false;
                }
            }
            //PanNoRecord.Visible = false;
            PanAddUpdate.Visible = true;

            //lblFrom.Text = ddlsearchtransferfrom.SelectedItem.Text;
            //lblTo.Text = ddlsearchtransferto.SelectedItem.Text;
            if (ddlInstaller.SelectedValue != string.Empty)
            {
                lblddlInstaller.Text = ddlInstaller.SelectedItem.Text;
            }
            if (ddlStockCategory.SelectedValue != string.Empty)
            {
                lblddlStockCategory.Text = ddlStockCategory.SelectedItem.Text;
            }
            if (ddlLocation.SelectedValue != string.Empty)
            {
                lblLocation.Text = ddlLocation.SelectedItem.Text;
            }
            if (txtStartDate.Text != string.Empty)
            {
                lblstartdate.Text = string.Format("{0:dd MMMM, yyyy}", Convert.ToDateTime(txtStartDate.Text));
            }
            if (txtEndDate.Text != string.Empty)
            {
                lblenddate.Text = string.Format("{0:dd MMMM, yyyy}", Convert.ToDateTime(txtEndDate.Text));
            }

        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }
    //protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    //{
    //    ddlLocation.SelectedValue = "";
    //    ddlStockCategory.SelectedValue = "";
    //    ddlInstaller.SelectedValue = "";
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;

    //    BindGrid(0);
    //}
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    private string Encryptdata(string date)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[date.Length];
        encode = Encoding.UTF8.GetBytes(date);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }
    protected void btnPrint_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/reports/usage.aspx");
    }
    protected void rptModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rptdet = (Repeater)e.Item.FindControl("rptdet");
        HiddenField hdnStockCode = (HiddenField)e.Item.FindControl("hdnStockCode");


        string Location = "";
        try
        {
            if (ddlLocation.SelectedValue == "")
            {

                ddlLocation.SelectedValue = "5";
                Location = ddlLocation.SelectedValue;
            }
            else
            {
                Location = ddlLocation.SelectedValue;
            }
        }
        catch
        {
            Location = ddlLocation.SelectedValue;
        }

        DataTable dt = ClstblStockItems.tblStockItems_SelectUsageDetails(hdnStockCode.Value, Location, ddlStockCategory.SelectedValue, ddlInstaller.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim());


        if (dt.Rows.Count > 0)
        {
            rptdet.DataSource = dt;
            rptdet.DataBind();
        }
    }
    protected void lbtnExport_Click(object sender, EventArgs e)
    {

        DataTable dt = new DataTable();
        string Location = "";
        if (ddlLocation.SelectedValue == "")
        {
            Location = "5";
            ddlLocation.SelectedValue = "5";
        }
        else
        {
            Location = ddlLocation.SelectedValue;
        }
        dt = ClstblStockItems.tblStockItems_SelectUsage(Location, ddlStockCategory.SelectedValue, ddlInstaller.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim());

        Response.Write(dt.Rows.Count);
        Response.End();
        try
        {
            Export oExport = new Export();
            string FileName = "StockUsage" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 5, 8 };
            string[] arrHeader = { "Stock Item", "Allocation" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlLocation.SelectedValue = "";
        ddlStockCategory.SelectedValue = "";
        ddlInstaller.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        BindGrid(0);
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

}