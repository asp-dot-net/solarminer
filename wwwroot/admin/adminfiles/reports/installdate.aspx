<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" Culture="en-GB" UICulture="en-GB"
    AutoEventWireup="true" CodeFile="installdate.aspx.cs" Inherits="admin_adminfiles_reports_installdate" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../../includes/reports.ascx" TagName="reports" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
       <div class="finalheader printorder">
    <div class="small-header transition animated fadeIn printorder">
        <div class="hpanel">
            <div class="panel-body">
              <%--  <div id="tdExport" class="pull-right" runat="server">
                   
                    <asp:LinkButton ID="lbtnExport" CssClass="excelicon" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                        CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>
                    <a href="javascript:window.print();" class="printicon">
                      
                        <i class="fa fa-print"></i>
                    </a>
                    
                </div>--%>
                <h2 class="font-light m-b-xs">Install Date Report
                </h2>
            </div>
        </div>
    </div>
           </div>
    <%--<asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>--%>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
        }
        function endrequesthandler(sender, args) {
        }
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }
    </script>
    <asp:Panel runat="server" ID="PanGridSearch" CssClass="printorder">
        <div class="content animate-panel" style="padding-bottom: 0px!important;">
            <div class="messesgarea">

                <div class="alert alert-info" id="PanNoRecord" runat="server">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>
            </div>
        </div>

            <div class="searchfinal">
        <div class="panel-body animate-panel padbtmzero padtopzero">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel marbtmzero">
                        <%--<div class="panel-heading">
                            <div class="panel-tools">
                                
                            </div>
                            <br />
                        </div>--%>
                        <div class="panel-body">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="row">
                                    <div>
                                        
                                        <div class="dataTables_filter col-sm-12 Responsive-search printorder">
                                            <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div class="input-group" id="divCustomer" runat="server">
                                                            <asp:DropDownList ID="ddlState" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                <asp:ListItem Value="" Text="State"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group" id="div1" runat="server">
                                                            <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                <asp:ListItem Value="" Text="Installer"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>


                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server"
                                                                TargetControlID="txtStartDate"
                                                                WatermarkText="Start Date" />
                                                        </div>
                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" CssClass="errormessage" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                        </div>
                                                        <div class="input-group">
                                                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary btnsearchicon" ValidationGroup="search" Text="Search" OnClick="btnSearch_Click" />
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:LinkButton ID="btnClearAll" runat="server"
                                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-info"><i class="fa-eraser fa"></i>Clear </asp:LinkButton>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <%--<tr>
                                                    <td colspan="1">
                                                        <br />
                                                    </td>
                                                </tr>--%>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row martop5">
                                    <div>
                                        <div class="dataTables_length showdata col-sm-3 printorder">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>Show
                                                            <asp:DropDownList ID="ddlSelectRecords" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                aria-controls="DataTables_Table_0" class="form-control input-sm myval">
                                                            </asp:DropDownList>
                                                        entries
                                                    </td>

                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-md-9">
                                        	<div id="tdExport" class="pull-right" runat="server">
                    <%--  <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                    <asp:LinkButton ID="lbtnExport" CssClass="excelicon" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                        CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>
                    <a href="javascript:window.print();" class="printicon">
                        <%--        <img src="../../../images/icon_print.png" class="tooltips" data-toggle="tooltip"
                                data-placement="top" title="Print" />--%>
                        <i class="fa fa-print"></i>
                    </a>
                    <%--  </ol>--%>
                </div>
                                        </div>
                                      </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                 </div>
    </asp:Panel>
        <asp:Panel ID="panel" runat="server" CssClass="hpanel panel-body marbtmzero printorder">
                                    <div>
                                        <div class="panel-body marbtmzero">
                                            <table cellpadding="5" cellspacing="0" border="0" align="left" width="100%" class="printpage col-sm-12">
                                                <tr>
                                                    <td>
                                                        <table cellpadding="0" cellspacing="0" border="0">
                                                            <tr>
                                                                <td class="paddingleftright10">
                                                                    <b>Total Panels:&nbsp;</b><asp:Literal ID="lblTotalPanel" runat="server"></asp:Literal>
                                                                </td>
                                                                <td>&nbsp;&nbsp;</td>
                                                                <td class="paddingleftright10">
                                                                    <b>Total Job:&nbsp;</b><asp:Literal ID="lblTotalJob" runat="server"></asp:Literal>
                                                                </td>
                                                                <td>&nbsp;&nbsp;</td>
                                                                <td class="paddingleftright10">
                                                                    <b>Total Price:&nbsp;</b><asp:Literal ID="lbltotalamount" runat="server"></asp:Literal>
                                                                </td>
                                                                <td>&nbsp;&nbsp;</td>
                                                                <td class="paddingleftright10">
                                                                    <b>Total Pay:&nbsp;</b><asp:Literal ID="lblpaid" runat="server"></asp:Literal>
                                                                </td>
                                                                <td>&nbsp;&nbsp;</td>
                                                                <td class="paddingleftright10">
                                                                    <b>Owing:&nbsp;</b><asp:Literal ID="lblOwing" runat="server"></asp:Literal>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </asp:Panel>
                                 <asp:Panel ID="panel1" runat="server" CssClass="hpanel panel-body padtopzero">
                                            <div class="finalgrid">
                                <div class="Row">
                                    <div id="PanGrid" runat="server">
                                        <div class="table-responsive printArea">
                                            <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                                OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging"
                                                OnDataBound="GridView1_DataBound" OnRowCommand="GridView1_OnRowCommand" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Project Number" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="130px">
                                                        <ItemTemplate>
                                                            <%#Eval("ProjectNumber")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Project" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <%#Eval("Project")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Panels" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" ItemStyle-Width="50px">
                                                        <ItemTemplate>
                                                            <%#Eval("NumberPanels")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Quote Price" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Right"
                                                        ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" HeaderStyle-CssClass="right-text">
                                                        <ItemTemplate>
                                                            <%#Eval("TotalQuotePrice","{0:0.00}")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Opened" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text" ItemStyle-Width="120px">
                                                        <ItemTemplate>
                                                            <%#Eval("ProjectOpened","{0:dd MMM yyyy}")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle />

                                                <PagerTemplate>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    <div class="pagination printorder">
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                    </div>
                                                </PagerTemplate>
                                                <PagerStyle CssClass="paginationGrid" />
                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                            </asp:GridView>
                                        </div>
                                          <div class="paginationnew1 printorder" runat="server" id="divnopage" >
                                            <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                                </div>
                                     </asp:Panel>
</asp:Content>

