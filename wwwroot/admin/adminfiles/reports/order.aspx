﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="order.aspx.cs" Inherits="admin_adminfiles_reports_order" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .orderheader {
            width: 100%;
            float: right;
            border-bottom: 1px solid #000;
            text-align: right;
        }

            .orderheader h1 {
                font-size: 25px;
                color: #000;
                margin-bottom: 20px;
                text-transform: uppercase;
            }

            .orderheader h2 {
                font-size: 20px;
                color: #000;
                margin-bottom: 20px;
            }

            .orderheader h3 {
                font-size: 15px;
                color: #000;
                margin-bottom: 20px;
            }

        .clear {
            clear: both;
        }

        table tr td {
            padding: 10px 0px;
            font-size: 12px;
            color: #000;
            font-weight: bold;
        }

            table tr td.location {
                font-size: 16px;
                color: #000;
                font-weight: bold;
            }

        .qty {
            border: 1px solid #000;
            padding: 10px;
        }

            .qty table tr th {
                font-size: 12px;
                color: #000;
                font-weight: bold;
                border-bottom: 2px solid #000;
                padding: 10px;
            }

            .qty table tr td {
                font-size: 12px;
                color: #000;
                font-weight: normal;
                padding: 10px;
            }
    </style>
    <section class="row m-b-md">
        <div class="col-sm-12 minhegihtarea">
            <div class="small-header transition animated fadeIn">
                <div class="hpanel">
                    <div class="panel-body">
                        <div id="hbreadcrumb" class="pull-right">
                            <ol class="hbreadcrumb breadcrumb fontsize16">
                                <%-- <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click"><i class="fa fa-plus"></i> Add</asp:LinkButton>--%>
                                <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false"><i class="fa fa-chevron-left"></i> Back</asp:LinkButton>

                            </ol>
                        </div>
                        <h2 class="font-light m-b-xs">Stock Order

                        </h2>

                    </div>
                </div>
            </div>
            <div class="content animate-panel">
                <div class="bodymianbg">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div align="left" class="printpage">
                                        <a href="javascript:window.print();">
                                            <i class="icon-print printpage fa fa-print"></i>
                                        </a>
                                    </div>
                                    <div class="clear">
                                    </div>
                                    <div class="orderheader">
                                        <h1>Stock Order</h1>
                                        <h2>Order Number:&nbsp;<asp:Label ID="lblOredrNumber" runat="server"></asp:Label></h2>
                                        <h3>
                                            <asp:Label ID="lblOrderDate" runat="server"></asp:Label></h3>
                                    </div>
                                    <div class="clear"></div>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="location">Order For Location:&nbsp;<asp:Label ID="lblLocation" runat="server"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td>Order By:&nbsp;<asp:Label ID="lblOrderBy" runat="server"></asp:Label></td>
                                        </tr>
                                    </table>
                                    <div class="qty">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <th width="10%" align="left">Qty</th>
                                                <th align="left">Stock Item</th>
                                                <th align="left">Stock Code</th>
                                            </tr>
                                            <asp:Repeater ID="rptItems" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="lblQty" runat="server"><%#Eval("OrderQuantity") %></asp:Label></td>
                                                        <td>
                                                            <asp:Label ID="lblItem" runat="server"><%#Eval("StockItem") %></asp:Label></td>
                                                        <td>
                                                            <asp:Label ID="lblCode" runat="server"><%#Eval("StockCode") %></asp:Label></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

