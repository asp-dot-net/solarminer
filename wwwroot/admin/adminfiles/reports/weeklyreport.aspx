<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    Culture="en-GB" UICulture="en-GB" AutoEventWireup="true" CodeFile="weeklyreport.aspx.cs" Inherits="admin_adminfiles_reports_weeklyreport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../../includes/reports.ascx" TagName="reports" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="finalheader">
    <div class="small-header transition animated fadeIn printorder">
        <div class="hpanel">
            <div class="panel-body">
                <h2 class="font-light m-b-xs">Weekly Report
                </h2>
            </div>
        </div>
    </div>
        </div>
    <%--<asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>--%>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
        }
        function endrequesthandler(sender, args) {
        }
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }
    </script>

       <script>
           var prm = Sys.WebForms.PageRequestManager.getInstance();
           prm.add_pageLoaded(pageLoaded);
           //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
           prm.add_beginRequest(beginrequesthandler);
           // raised after an asynchronous postback is finished and control has been returned to the browser.
           prm.add_endRequest(endrequesthandler);
           function beginrequesthandler(sender, args) {
               //shows the modal popup - the update progress
               $('.loading-container').css('display', 'block');
           }
           function endrequesthandler(sender, args) {
               //hide the modal popup - the update progress
               $('.loading-container').css('display', 'none');
           }

            </script>
    <asp:Panel runat="server" ID="PanGridSearch">
        <div class="content animate-panel" style="padding-bottom: 0px!important;">
            <div class="messesgarea">

                <div class="alert alert-info" id="PanNoRecord" visible="false" runat="server">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>
            </div>
        </div>

              <div class="finalgrid">
        <div class="panel-body animate-panel padbtmzero padtopzero">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel marbtmzero">
                        <%--<div class="panel-heading">
                            <div class="panel-tools">
                                
                            </div>
                            <br />
                        </div>--%>
                        <div class="panel-body brdnone">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer displayinlineblock">
                                <div class="row">
                                    <div >
                                       
                                        <div class="dataTables_filter col-sm-11  Responsive-search printorder">
                                            <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            Select Year
                                                        </div>
                                                        <div class="input-group" id="divCustomer" runat="server">
                                                            <asp:DropDownList ID="ddlYear" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                <asp:ListItem Value="">Year</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                ControlToValidate="ddlYear" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </div>

                                                        <div class="form-group">
                                                            Select Week
                                                        </div>
                                                        <div class="input-group" id="div1" runat="server">
                                                            <asp:DropDownList ID="ddlWeek" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                <asp:ListItem Value="">Week</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>


                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStartDate" CssClass="errormessage" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" CssClass="errormessage" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                        </div>
                                                        <div class="input-group">
                                                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                        </div>
                                                          <div class="input-group pull-right">
                                        	<div id="tdExport" class="pull-right" runat="server">
                    <a href="javascript:window.print();" class="printicon">
                        <i class="fa fa-print"></i>
                    </a>
                </div>
                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                      
                                    </div>
                                </div>

                                
                                  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                  </div>
    </asp:Panel>
<div class="">
                            <div class="panel-body">
                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="PanAddUpdate" runat="server">
                                    <div>
                                        <div class="panel panel-default brdnone printArea">
                                            <div class="brdnone">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group dateimgarea">
                                                            <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover table-striped">
                                                                <tr>
                                                                    <td width="250px" class="center-text"><b>Panel Installed</b>
                                                                    </td>
                                                                    <td class="center-text">
                                                                        <asp:Label ID="lblTotalPI" runat="server" class="center-text"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <asp:Repeater ID="rptStatePI" runat="server" OnItemDataBound="rptStatePI_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td class="center-text"><%#Eval("State") %>
                                                                                <asp:HiddenField ID="hndState" runat="server" Value='<%#Eval("State") %>' />
                                                                            </td>
                                                                            <asp:Repeater ID="rptTotal" runat="server">
                                                                                <ItemTemplate>
                                                                                    <td class="center-text"><%#Eval("total") %></td>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                <tr>
                                                                    <td width="150px" class="center-text"><b>No. of Jobs</b>
                                                                    </td>
                                                                    <td class="center-text">
                                                                        <asp:Label ID="lblJobCompleted" runat="server" class="center-text"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <asp:Repeater ID="rptJobCompleted" runat="server" OnItemDataBound="rptJobCompleted_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td class="center-text"><%#Eval("State") %>
                                                                                <asp:HiddenField ID="hndState" runat="server" Value='<%#Eval("State") %>' />
                                                                            </td>
                                                                            <asp:Repeater ID="rptTotal" runat="server">
                                                                                <ItemTemplate>
                                                                                    <td class="center-text"><%#Eval("total") %></td>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                <tr>
                                                                    <td width="150px" class="center-text"><b>Total Sales</b>
                                                                    </td>
                                                                    <td class="center-text">
                                                                        <asp:Label ID="lblTotalSales" runat="server" class="center-text"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <asp:Repeater ID="rptTotalSales" runat="server" OnItemDataBound="rptTotalSales_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td class="center-text"><%#Eval("State") %>
                                                                                <asp:HiddenField ID="hndState" runat="server" Value='<%#Eval("State") %>' />
                                                                            </td>
                                                                            <asp:Repeater ID="rptTotal" runat="server">
                                                                                <ItemTemplate>
                                                                                    <td class="center-text"><%#Eval("total","{0:0.00}") %></td>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                <tr>
                                                                    <td width="150px" class="center-text"><b>Week Outstanding (Inc Finanace)</b>
                                                                    </td>
                                                                    <td class="center-text">
                                                                        <asp:Label ID="lblWOIF" class="center-text" runat="server"></asp:Label>
                                                                    </td>
                                                                    <asp:Repeater ID="rptFinanceWO" runat="server">
                                                                        <ItemTemplate>
                                                                            <td class="center-text">
                                                                                <%# Eval("FinanceWith") %>
                                                                            </td>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </tr>
                                                                <asp:Repeater ID="rptWOIF" runat="server" OnItemDataBound="rptWOIF_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td class="center-text"><%#Eval("State") %>
                                                                                <asp:HiddenField ID="hndState" runat="server" Value='<%#Eval("State") %>' />
                                                                            </td>
                                                                            <asp:Repeater ID="rptTotal" runat="server">
                                                                                <ItemTemplate>
                                                                                    <td class="center-text"><%#Eval("total","{0:0.00}") %></td>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                            <asp:Repeater ID="rptFinanceHndWO" runat="server" OnItemDataBound="rptFinanceHndWO_ItemDataBound">
                                                                                <ItemTemplate>
                                                                                    <asp:HiddenField ID="hndFin" runat="server" Value='<%# Eval("FinanceWithID") %>' />
                                                                                    <asp:Repeater ID="rptTotalWO" runat="server" >
                                                                                        <ItemTemplate>
                                                                                            <td class="center-text">
                                                                                                <%#Eval("total","{0:0.00}") %>
                                                                                            </td>
                                                                                        </ItemTemplate>
                                                                                    </asp:Repeater>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                <tr>
                                                                    <td width="150px" class="center-text"><b>Week Outstanding (Ex Finance)</b>
                                                                    </td>
                                                                    <td class="center-text">
                                                                        <asp:Label ID="lblWOEF" runat="server" class="center-text"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <asp:Repeater ID="rptWOEF" runat="server" OnItemDataBound="rptWOEF_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td class="center-text"><%#Eval("State") %>
                                                                                <asp:HiddenField ID="hndState"   runat="server" Value='<%#Eval("State") %>' />
                                                                            </td>
                                                                            <asp:Repeater ID="rptTotal" runat="server" >
                                                                                <ItemTemplate>
                                                                                    <td class="center-text"><%#Eval("total","{0:0.00}") %></td>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                <tr>
                                                                    <td width="150px" class="center-text"><b>Total Outstanding (Inc Finanace)</b>
                                                                    </td>
                                                                    <td class="center-text">
                                                                        <asp:Label ID="lblTOIF" runat="server"></asp:Label>
                                                                    </td>
                                                                    <asp:Repeater ID="rptFinanceTO"  runat="server" >
                                                                        <ItemTemplate>
                                                                            <td class="center-text">
                                                                                <%# Eval("FinanceWith") %>
                                                                            </td>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </tr>
                                                                <asp:Repeater ID="rptTOIF" runat="server" OnItemDataBound="rptTOIF_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td class="center-text"><%#Eval("State") %>
                                                                                <asp:HiddenField ID="hndState" runat="server" Value='<%#Eval("State") %>' />
                                                                            </td>
                                                                            <asp:Repeater ID="rptTotal" runat="server">
                                                                                <ItemTemplate>
                                                                                    <td class="center-text"><%#Eval("total","{0:0.00}") %></td>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                            <asp:Repeater ID="rptFinanceHndTO" runat="server" OnItemDataBound="rptFinanceHndTO_ItemDataBound">
                                                                                <ItemTemplate>
                                                                                    <asp:HiddenField ID="hndFin" runat="server"  Value='<%# Eval("FinanceWithID") %>' />
                                                                                    <asp:Repeater ID="rptTotalTO" runat="server">
                                                                                        <ItemTemplate>
                                                                                            <td class="center-text">
                                                                                                <%#Eval("total","{0:0.00}") %>
                                                                                            </td>
                                                                                        </ItemTemplate>
                                                                                    </asp:Repeater>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                <tr>
                                                                    <td width="150px" class="center-text"><b>Total Outstanding (Ex Finanace)</b>
                                                                    </td>
                                                                    <td class="center-text">
                                                                        <asp:Label ID="lblTOEF" runat="server" class="center-text"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <asp:Repeater ID="rptTOEF" runat="server" OnItemDataBound="rptTOEF_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td class="center-text"><%#Eval("State") %>
                                                                                <asp:HiddenField ID="hndState" runat="server" Value='<%#Eval("State") %>' />
                                                                            </td>
                                                                            <asp:Repeater ID="rptTotal" runat="server" >
                                                                                <ItemTemplate >
                                                                                    <td class="center-text"><%#Eval("total","{0:0.00}") %></td>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                <tr>
                                                                    <td width="150px" class="center-text"><b>Panels Remaining To Install</b>
                                                                    </td>
                                                                    <td class="center-text">
                                                                        <asp:Label ID="lblPanelsRemainingToInstall" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <asp:Repeater ID="rptPanelsRemainingToInstall" runat="server" OnItemDataBound="rptPanelsRemainingToInstall_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td class="center-text"><%#Eval("State") %>
                                                                                <asp:HiddenField ID="hndState" runat="server" Value='<%#Eval("State") %>' />
                                                                            </td>
                                                                            <asp:Repeater ID="rptTotal" runat="server">
                                                                                <ItemTemplate>
                                                                                    <td class="center-text"><%#Eval("total") %></td>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                <tr>
                                                                    <td width="150px" class="center-text"><b>Remaining STC</b>
                                                                    </td>
                                                                    <td class="center-text">
                                                                        <asp:Label ID="lblRemainingSTC" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <asp:Repeater ID="rptRemainingSTC" runat="server" OnItemDataBound="rptRemainingSTC_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td class="center-text"><%#Eval("State") %>
                                                                                <asp:HiddenField ID="hndState" runat="server" Value='<%#Eval("State") %>' />
                                                                            </td>
                                                                            <asp:Repeater ID="rptTotal" runat="server">
                                                                                <ItemTemplate>
                                                                                    <td class="center-text"><%#Eval("total") %></td>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                                <tr>
                                                                    <td width="150px" class="center-text"><b>Remaining Paperwork</b>
                                                                    </td>
                                                                    <td class="center-text">
                                                                        <asp:Label ID="lblRemainingPaperwork" runat="server" class="center-text"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <asp:Repeater ID="rptRemainingPaperwork" runat="server" OnItemDataBound="rptRemainingPaperwork_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <tr>
                                                                            <td class="center-text"><%#Eval("State") %>
                                                                                <asp:HiddenField ID="hndState" runat="server" Value='<%#Eval("State") %>' />
                                                                            </td>
                                                                            <asp:Repeater ID="rptTotal" runat="server">
                                                                                <ItemTemplate>
                                                                                    <td class="center-text"><%#Eval("total") %></td>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </tr>
                                                                    </ItemTemplate>
                                                                </asp:Repeater>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                     </div>
    </div>
    <%--</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>

