﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_stockreport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddllocationsearch.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
            ddllocationsearch.DataTextField = "location";
            ddllocationsearch.DataValueField = "CompanyLocationID";
            ddllocationsearch.DataMember = "CompanyLocationID";
            ddllocationsearch.DataBind();
            
            //PanNoRecord.Visible = true;
            PanAddUpdate.Visible = false;
            BindData();
            if (txtStartDate.Text == string.Empty)
            {
                txtStartDate.Text = DateTime.Now.ToShortDateString();
            }
        }
    }
    public void BindData()
    {
        //Reports.tblStockItems_SelectByCategory("2", txtSearchStockItem.Text);
        if (ddlcategory.SelectedValue == "1")
        {
            PanAddUpdate.Visible = true;
            PanNoRecord.Visible = false;
            trPanels.Visible = true;
            rptPanels.DataSource = Reports.tblStockItems_StockReportCount("2", txtSearchStockItem.Text, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddllocationsearch.SelectedValue, ddldetails.SelectedValue);
           
            rptPanels.DataBind();
        }
        else if (ddlcategory.SelectedValue == "2")
        {
           
            PanNoRecord.Visible = false;
            trInverters.Visible = true;
            rptInverters.DataSource = Reports.tblStockItems_StockReportCount("3", txtSearchStockItem.Text, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddllocationsearch.SelectedValue, ddldetails.SelectedValue);
            //Response.Write("3" + "<br>" + txtSearchStockItem.Text + "<br>" + txtStartDate.Text.Trim() + "<br>" + txtEndDate.Text.Trim() + "<br>" + ddllocationsearch.SelectedValue + "<br>" + ddldetails.SelectedValue);
            //Response.End();
            rptInverters.DataBind();
        }
        else if (ddlcategory.SelectedValue == "4")
        {
            Response.Write("3");
            Response.End();
            PanNoRecord.Visible = false;
            trMountingKit.Visible = true;
            rptMounting.DataSource = Reports.tblStockItems_StockReportCount("4", txtSearchStockItem.Text, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddllocationsearch.SelectedValue, ddldetails.SelectedValue);
            rptMounting.DataBind();
        }
        else if (ddlcategory.SelectedValue == "3")
        {
          
            PanNoRecord.Visible = false;
            trElectricialKit.Visible = true;
            rptElectricial.DataSource = Reports.tblStockItems_StockReportCount("5", txtSearchStockItem.Text, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddllocationsearch.SelectedValue, ddldetails.SelectedValue);
            rptElectricial.DataBind();
        }
        else
        {
           
            PanNoRecord.Visible = false;
            trPanels.Visible = false;
            trInverters.Visible = false;
            trMountingKit.Visible = false;
            trElectricialKit.Visible = false;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        BindData();
    }
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        PrepareGridViewForExport(PanAddUpdate);
        string timestemp = DateTime.Now.ToString("dd-MMM-yyyy H:mm:ss").ToString().Replace(" ", "_");
        string attachment = "attachment; filename=Stock_Report_" + timestemp + ".xls";

        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        PanAddUpdate.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {

    }
    private void PrepareGridViewForExport(Control gv)
    {
        Repeater lb = new Repeater();
        Literal l = new Literal();
        string name = String.Empty;

        for (int i = 0; i < gv.Controls.Count; i++)
        {
            if (gv.Controls[i].GetType() == typeof(LinkButton))
            {
                l.Text = (gv.Controls[i] as LinkButton).Text;
                Response.Write(l.Text);
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }
            else if (gv.Controls[i].GetType() == typeof(Label))
            {
                l.Text = (gv.Controls[i] as Label).Text;
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }
            else if (gv.Controls[i].GetType() == typeof(HyperLink))
            {

                l.Text = (gv.Controls[i] as HyperLink).Text;
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }

            else if (gv.Controls[i].GetType() == typeof(DropDownList))
            {
                l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }

            else if (gv.Controls[i].GetType() == typeof(CheckBox))
            {
                l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }
            else if (gv.Controls[i].GetType() == typeof(HiddenField))
            {
                l.Text = "";
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }

            if (gv.Controls[i].HasControls())
            {
                PrepareGridViewForExport(gv.Controls[i]);
            }
        }
        //Response.End();
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

}