﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_capkwcount : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            rptState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
            rptState.DataBind();

            rptCapKW.DataSource = Reports.tblProjects_Select_SystemCapKW();
            rptCapKW.DataBind();
        }
    }
    protected void rptCapKW_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptHidState = (Repeater)e.Item.FindControl("rptHidState");
            rptHidState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
            rptHidState.DataBind();

            HiddenField hndKW = (HiddenField)e.Item.FindControl("hndKW");
            Literal lblTotal = (Literal)e.Item.FindControl("lblTotal");
            DataTable dt = Reports.tblProjects_Total_SystemCapKW(hndKW.Value, txtStartDate.Text.Trim(), txtEndDate.Text.Trim());
            lblTotal.Text = dt.Rows[0]["total"].ToString();
        }
    }
    protected void rptHidState_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndKW = ((HiddenField)((Repeater)sender).Parent.FindControl("hndKW"));
            HiddenField hndState = (HiddenField)e.Item.FindControl("hndState");
            Repeater rptCount = (Repeater)e.Item.FindControl("rptCount");

            rptCount.DataSource = Reports.tblProjects_Count_SystemCapKW(hndKW.Value, hndState.Value, txtStartDate.Text.Trim(), txtEndDate.Text.Trim());
            rptCount.DataBind();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        rptCapKW.DataSource = Reports.tblProjects_Select_SystemCapKW();
        rptCapKW.DataBind();
    }
    //protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    //{
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;

    //    rptCapKW.DataSource = Reports.tblProjects_Select_SystemCapKW();
    //    rptCapKW.DataBind();
    //}
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        rptCapKW.DataSource = Reports.tblProjects_Select_SystemCapKW();
        rptCapKW.DataBind();
    }
}