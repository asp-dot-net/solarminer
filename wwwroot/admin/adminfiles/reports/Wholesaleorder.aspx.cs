﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_Wholesaleorder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(Request.QueryString["id"]);
            lblOredrNumber.Text = st.OrderNumber;
            try
            {
                lblOrderDate.Text = string.Format("{0:dd MMMM yyyy}", Convert.ToDateTime(st.DateOrdered));
            }
            catch { }
            lblLocation.Text = st.CompanyLocation;
            lblOrderBy.Text = st.OrderedName;

            DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(Request.QueryString["id"]);
            if (dt.Rows.Count > 0)
            {
                rptItems.DataSource = dt;
                rptItems.DataBind();
            }
        }
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/stock/Wholesale.aspx");
    }
}