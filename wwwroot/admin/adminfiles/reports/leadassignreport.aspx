﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" Culture="en-GB" UICulture="en-GB"
    AutoEventWireup="true" CodeFile="leadassignreport.aspx.cs" Inherits="admin_adminfiles_reports_leadassignreport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../../includes/reports.ascx" TagName="reports" TagPrefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <div class="finalheader printorder">
    <div class="small-header transition animated fadeIn printorder">
        <div class="hpanel">
            <div class="panel-body">
                <div id="tdExport" class="pull-right" runat="server">
                    <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">
                     
                    
                    </ol>
                </div>
                <h2 class="font-light m-b-xs">Lead Assign Report
                </h2>
            </div>
        </div>
    </div>
         </div>
    <%--<asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>--%>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
        }
        function endrequesthandler(sender, args) {
        }
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }
    </script>
      <script>
          var prm = Sys.WebForms.PageRequestManager.getInstance();
          prm.add_pageLoaded(pageLoaded);
          //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
          prm.add_beginRequest(beginrequesthandler);
          // raised after an asynchronous postback is finished and control has been returned to the browser.
          prm.add_endRequest(endrequesthandler);
          function beginrequesthandler(sender, args) {
              //shows the modal popup - the update progress
              $('.loading-container').css('display', 'block');
          }
          function endrequesthandler(sender, args) {
              //hide the modal popup - the update progress
              $('.loading-container').css('display', 'none');
          }

            </script>
    <asp:Panel runat="server" ID="PanGridSearch" CssClass="printorder">
        <div class="content animate-panel" style="padding-bottom:0px!important;">
            <div class="messesgarea">

                <div class="alert alert-info" id="PanNoRecord" visible="false" runat="server">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>
            </div>
        </div>
         <div class="searchfinal">
        <div class="panel-body animate-panel padbtmzero padtopzero">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel marbtmzero">
                        <%--<div class="panel-heading">
                            <div class="panel-tools">
                                
                            </div>
                            <br />
                        </div>--%>
                        <div class="panel-body">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer displayinlineblock">
                                <div class="row">
                                    <div>
                                        
                                        <div class="dataTables_filter col-sm-11 Responsive-search printorder">
                                            <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div class="input-group" id="divCustomer" runat="server">
                                                            <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                <asp:ListItem Value="" Text="Team"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group" id="div1" runat="server">
                                                            <asp:DropDownList ID="ddlState" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                <asp:ListItem Value="" Text="State"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStartDate" CssClass="errormessage" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" CssClass="errormessage" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                        </div>
                                                        <div class="input-group">
                                                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="search" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                        </div>

                                                         <div class="input-group printorder pull-right">
                                        	<div id="Div2" class="pull-right" runat="server">
                     <a href="javascript:window.print();" style="font-size:20px;">
              
                        <i class="fa fa-print"></i>
                        </a>
                </div>
                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                         
                                    </div>
                                </div>

                             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
             </div>
    </asp:Panel>
     <div class="finalgrid">
        </div><div class="panel-body">
                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="PanAddUpdate" runat="server">
                                    <div>
                                <div class="row">
                                    <div>
                                        <%--<div class="panel panel-default">--%>
                                            <div class=" brdnone printArea">
                                                <div class="col-md-6">
                                                    <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover table-striped">
                                                        <tr>
                                                            <td width="150px"><b>SalesRep</b>
                                                            </td>
                                                            <td class="center-text">
                                                                <b>Online Leads</b>
                                                            </td>
                                                            <td class="center-text">
                                                                <b>Others</b>
                                                            </td>
                                                            <td class="center-text">
                                                                <b>Tele. Mktg</b>
                                                            </td>
                                                        </tr>
                                                        <asp:Repeater ID="rptSalesRep" runat="server" OnItemDataBound="rptSalesRep_ItemDataBound">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                                                        <%#Eval("fullname") %>
                                                                    </td>
                                                                    <asp:Repeater ID="rptOnline" runat="server">
                                                                        <ItemTemplate>
                                                                            <td class="center-text"><%#Eval("total") %></td>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                    <asp:Repeater ID="rptOthers" runat="server">
                                                                        <ItemTemplate>
                                                                            <td class="center-text"><%#Eval("total") %></td>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                    <asp:Repeater ID="rptTeleMktg" runat="server">
                                                                        <ItemTemplate>
                                                                            <td class="center-text"><%#Eval("total") %></td>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <tr>
                                                            <td class="center-text">
                                                                <b>Total</b>
                                                            </td>
                                                            <td class="center-text">
                                                                <b>
                                                                    <asp:Literal ID="lblOnline" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td class="center-text">
                                                                <b>
                                                                    <asp:Literal ID="lblOther" runat="server"></asp:Literal></b>
                                                            </td>
                                                            <td class="center-text">
                                                                <b>
                                                                    <asp:Literal ID="lblTeleMktg" runat="server"></asp:Literal></b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                       <%-- </div>--%>
                                    </div>
                                </div>
</div>
                                    </div>
                                     </div>
          
    <%--</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>
