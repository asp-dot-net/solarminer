<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    Culture="en-GB" UICulture="en-GB" AutoEventWireup="true" CodeFile="leadtrackreport.aspx.cs" Inherits="admin_adminfiles_reports_leadtrackreport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../../includes/reports.ascx" TagName="reports" TagPrefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <div class="finalheader">
    <div class="small-header transition animated fadeIn printorder">
        <div class="hpanel">
            <div class="panel-body">
                <h2 class="font-light m-b-xs">Lead Track Report
                </h2>
            </div>
        </div>
    </div>
         </div>
    <%--<asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>--%>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
        }
        function endrequesthandler(sender, args) {
        }
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }
    </script>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');
        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            $('.loading-container').css('display', 'none');
        }

    </script>
    <asp:Panel runat="server" ID="PanGridSearch">
        <div class="content animate-panel" style="padding-bottom: 0px!important;">
            <div class="messesgarea">

                <div class="alert alert-info" id="PanNoRecord" visible="false" runat="server">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>
            </div>
        </div>
              <div class="searchfinal">
        <div class="panel-body animate-panel padbtmzero padtopzero">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel marbtmzero">
                        <%--<div class="panel-heading">
                            <div class="panel-tools">
                                
                            </div>
                            <br />
                        </div>--%>
                        <div class="panel-body">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer ">
                                <div class="row">
                                    <div>
                                        <div class="dataTables_length showdata col-sm-2" id="show" runat="server" visible="false">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>Show
                                                            <asp:DropDownList ID="ddlSelectRecords" runat="server"
                                                                aria-controls="DataTables_Table_0" class="form-control input-sm">
                                                            </asp:DropDownList>
                                                        entries
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="dataTables_filter col-sm-11 Responsive-search printorder">
                                            <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div class="input-group" id="divCustomer" runat="server">
                                                            <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                <asp:ListItem Value="" Text="Team"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group" id="div1" runat="server">
                                                            <asp:DropDownList ID="ddlYear" runat="server" AppendDataBoundItems="true"
                                                                AutoPostBack="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" aria-controls="DataTables_Table_0" CssClass="myval">
                                                                <asp:ListItem Value="">Year</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                    ControlToValidate="ddlYear" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                        </div>

                                                        <div class="input-group" id="div2" runat="server">
                                                            <asp:DropDownList ID="ddlWeek" runat="server" AppendDataBoundItems="true"
                                                                AutoPostBack="true" OnSelectedIndexChanged="ddlWeek_SelectedIndexChanged" aria-controls="DataTables_Table_0" CssClass="myval">
                                                                <asp:ListItem Value="">Week</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStartDate" CssClass="errormessage" ErrorMessage="* Required" ValidationGroup="search"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" CssClass="errormessage" ErrorMessage="* Required" ValidationGroup="search"></asp:RequiredFieldValidator>
                                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                        </div>
                                                        <div class="input-group">
                                                            <asp:Button ID="btnSearch" CausesValidation="false" ValidationGroup="search" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                        </div>
                                                         <div class="input-group pull-right">
                                            <div id="tdExport" class="pull-right" runat="server">
                                                <a href="javascript:window.print();" class="printicon">
                                                    <i class="fa fa-print"></i>
                                                </a>
                                            </div>
                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                       
                                    </div>
                                </div>
                                <div class="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
                  </div>
    </asp:Panel>
       <div class="finalgrid">
    <div class="panel-body">
        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="PanAddUpdate" runat="server">
            <div>
                <table cellpadding="5" cellspacing="0" border="0" align="left" width="100%" class="table table-striped table-bordered printArea">


                    <tr class="printArea">
                        <td width="150px"><b>Rep / Lead source</b>
                        </td>
                        <td>
                            <b>Leads</b>
                        </td>
                        <td>
                            <b>@<asp:TextBox ID="txt45" runat="server" Width="50px" MaxLength="5" class="form-control"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txt45"
                                    Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                <asp:Button ID="btn45" runat="server" Text="+" OnClick="btn45_Click" class="printpage btnaddclass" />
                            </b>
                        </td>
                        <td>
                            <b>Google</b>
                        </td>
                        <td>
                            <b>@
                                                <asp:TextBox ID="txt150" runat="server" Width="50px" MaxLength="5" class="form-control"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txt150"
                                    Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                <asp:Button ID="btn150" runat="server" Text="+" OnClick="btn150_Click" class="printpage btnaddclass" />
                            </b>
                        </td>
                        <td><b>Salary</b></td>
                        <td><b>Phone Cost</b></td>
                        <td><b>Total Cost</b></td>
                        <td><b>Panel Sold</b></td>
                        <td><b>Cancelled Panel</b></td>
                        <td><b>Net Panel Sold</b></td>
                        <td><b>Panel/Cost</b></td>
                    </tr>

                    <asp:Repeater ID="rptSalesRep" runat="server" OnItemDataBound="rptSalesRep_ItemDataBound">
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                    <b><%#Eval("fullname") %></b>
                                </td>
                                <asp:Repeater ID="rptOthers" runat="server" OnItemDataBound="rptOthers_ItemDataBound">
                                    <ItemTemplate>
                                        <td align="center">
                                            <asp:HiddenField ID="hndOther" runat="server" Value='<%#Eval("total") %>'></asp:HiddenField>
                                            <%#Eval("total") %></td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <td>
                                    <asp:Literal ID="lblLead45" runat="server"></asp:Literal></td>
                                <asp:Repeater ID="rptOnline" runat="server" OnItemDataBound="rptOnline_ItemDataBound">
                                    <ItemTemplate>
                                        <td align="center">
                                            <asp:HiddenField ID="hndOnlines" runat="server" Value='<%#Eval("total") %>'></asp:HiddenField>
                                            <%#Eval("total") %></td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <td>
                                    <asp:Literal ID="lblGoogle150" runat="server"></asp:Literal></td>
                                <td align="center">
                                    <asp:TextBox ID="txtSalary" runat="server" Width="100px" MaxLength="8" class="form-control" OnTextChanged="txtSalary_TextChanged" AutoPostBack="true"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtSalary"
                                        Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                </td>
                                <td align="center">
                                    <asp:TextBox ID="txtPhoneCost" runat="server" Width="100px" MaxLength="8" class="form-control" OnTextChanged="txtPhoneCost_TextChanged" AutoPostBack="true"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtPhoneCost"
                                        Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator></td>
                                <td>
                                    <asp:Literal ID="lblTotalCost" runat="server"></asp:Literal></td>
                                <asp:Repeater ID="rptPanelSold" runat="server">
                                    <ItemTemplate>
                                        <td align="center">
                                            <asp:Label ID="lblPanelSold" runat="server"></asp:Label>
                                            <%#Eval("total") %></td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Repeater ID="rptCancelledPanel" runat="server">
                                    <ItemTemplate>
                                        <td align="center"><%#Eval("total") %></td>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <td align="center">
                                    <asp:Literal ID="lblNetPanelSold" runat="server"></asp:Literal></td>
                                <td>
                                    <asp:Literal ID="lblPanelCost" runat="server"></asp:Literal></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>

                    <td>
                        <b>Total</b>
                    </td>
                    <td align="center">
                        <b>
                            <asp:Literal ID="lblLeadsTotal" runat="server"></asp:Literal></b>
                    </td>
                    <td align="center">
                        <b>
                            <asp:Literal ID="lbl45Total" runat="server"></asp:Literal></b>
                    </td>
                    <td align="center">
                        <b>
                            <asp:Literal ID="lblGoogleTotal" runat="server"></asp:Literal></b>
                    </td>
                    <td align="center">
                        <b>
                            <asp:Literal ID="lbl150Total" runat="server"></asp:Literal></b>
                    </td>
                    <td align="center">
                        <b>
                            <asp:Literal ID="lblSalaryTotal" runat="server"></asp:Literal></b>
                    </td>
                    <td align="center">
                        <b>
                            <asp:Literal ID="lblPhoneCostTotal" runat="server"></asp:Literal></b>
                    </td>
                    <td>
                        <b>
                            <asp:Literal ID="lblCostTotal" runat="server"></asp:Literal></b>
                    </td>
                    <td align="center">
                        <b>
                            <asp:Literal ID="lblSoldTotal" runat="server"></asp:Literal></b>
                    </td>
                    <td align="center">
                        <b>
                            <asp:Literal ID="lblCancelledTotal" runat="server"></asp:Literal></b>
                    </td>
                    <td align="center">
                        <b>
                            <asp:Literal ID="lblNetPanelTotal" runat="server"></asp:Literal></b>
                    </td>
                    <td align="center">
                        <b>
                            <asp:Literal ID="lblPanelCostTotal" runat="server"></asp:Literal></b>
                    </td>




                </table>
            </div>
        </div>
    </div>
           </div>
    <%--</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>
