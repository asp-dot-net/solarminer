<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" Culture="en-GB" UICulture="en-GB"
    AutoEventWireup="true" CodeFile="invoicesalesreport.aspx.cs" Inherits="admin_adminfiles_reports_invoicesalesreport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Invoice Sales Report</h5>
    </div>
    <%--<asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>--%>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
        }
        function endrequesthandler(sender, args) {
        }
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });

            $('.redreq').click(function () {
                formValidate();
            });
        }
    </script>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');
        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            $('.loading-container').css('display', 'none');
        }

    </script>
    <asp:Panel runat="server" ID="PanGridSearch">
        <div class="content animate-panel" style="padding-bottom: 0px!important;">
            <div class="messesgarea">

                <div class="alert alert-info" id="PanNoRecord" runat="server">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>
            </div>
        </div>
        <div class="panel-body animate-panel padbtmzero padtopzero">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel marbtmzero">
                        <%--<div class="panel-heading">
                            <div class="panel-tools">
                                
                            </div>
                            <br />
                        </div>--%>
                        <div class=" animate-panel padbtmzero padtopzero">
                            <div>
                                <div class="hpanel marbtmzero">
                                    <div class="">
                                        <div class="widget-body shadownone brdrgray">
                                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                <div class="row">
                                                    <div>
                                                        <div class="dataTables_filter col-sm-12 Responsive-search printorder">
                                                            <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                                                <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                                    <tr>
                                                                        <td>
                                                                            <div class="input-group col-sm-2" id="divCustomer" runat="server">
                                                                                <asp:DropDownList ID="ddlFinance" runat="server" AppendDataBoundItems="true"
                                                                                    aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                                    <asp:ListItem Value="1">None</asp:ListItem>
                                                                                    <asp:ListItem Value="2">Finance</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="input-group col-sm-2" id="div1" runat="server">
                                                                                <asp:DropDownList ID="ddlPayment" runat="server" AppendDataBoundItems="true"
                                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                                    <asp:ListItem Value="7">Cash</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <div class="input-group date datetimepicker1 col-sm-2">
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar"></span>
                                                                                </span>
                                                                                <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                                    ControlToValidate="txtStartDate" CssClass="" ErrorMessage=""></asp:RequiredFieldValidator>
                                                                            </div>
                                                                            <div class="input-group date datetimepicker1 col-sm-2">
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar"></span>
                                                                                </span>
                                                                                <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                                    ControlToValidate="txtEndDate" CssClass="" ErrorMessage=""></asp:RequiredFieldValidator>
                                                                                <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                                    ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                                    Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                                            </div>
                                                                            <div class="input-group">
                                                                                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info redreq btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <asp:LinkButton ID="btnClearAll" runat="server"
                                                                                    CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-info"><i class="fa-eraser fa"></i>Clear </asp:LinkButton>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row martop5 printorder">
                                                    <div>
                                                        <div class="dataTables_length showdata col-sm-3 printorder">
                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                            aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>

                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="col-md-9 printorder">
                                                            <div id="tdExport" class="pull-right" runat="server">
                                                                <%--  <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                                                <asp:LinkButton ID="lbtnExport" CssClass="btn btn-success btn-xs Excel" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                                                    CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel</asp:LinkButton>
                                                                <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>
                                                                <%--  </ol>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="panel" runat="server" CssClass="hpanel panel-body marbtmzero">
        <div class="widget-body shadownone brdrgray">
            <div class="panel-body padallzero">
                <table cellpadding="5" cellspacing="0" border="0" align="left" width="100%" class="printpage martop15 printorder">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td class="paddingleftright10 printorder">
                                        <b>Total Amount:&nbsp;</b><asp:Literal ID="lblTotalQuotePrice" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>

    <asp:Panel ID="panel1" runat="server" CssClass="hpanel panel-body padtopzero">
        <div class="Row">
            <div id="PanGrid" runat="server">
                <div class="table-responsive printArea">
                    <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                        OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnDataBound="GridView1_DataBound" OnRowCreated="GridView1_RowCreated"
                        AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                        <Columns>
                            <asp:TemplateField HeaderText="Project Number" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" SortExpression="ProjectNumber" ItemStyle-Width="130px">
                                <ItemTemplate>
                                    <%#Eval("ProjectNumber")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left" SortExpression="Customer">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hypcompdetail" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                        runat="server" NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=comp&compid="+Eval("CustomerID") %>'><%#Eval("Customer")%></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                ItemStyle-HorizontalAlign="left" ItemStyle-Width="150px" SortExpression="InstallAddress">
                                <ItemTemplate>
                                    <%#Eval("InstallAddress")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                ItemStyle-HorizontalAlign="left" ItemStyle-Width="90px" SortExpression="InstallCity">
                                <ItemTemplate>
                                    <%#Eval("InstallCity")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="State" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                ItemStyle-HorizontalAlign="left" ItemStyle-Width="50px" SortExpression="InstallState">
                                <ItemTemplate>
                                    <%#Eval("InstallState")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PostCode" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                ItemStyle-HorizontalAlign="left" ItemStyle-Width="50px" SortExpression="InstallPostCode">
                                <ItemTemplate>
                                    <%#Eval("InstallPostCode")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="InstallDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="left" SortExpression="InstallBookingDate1">
                                <ItemTemplate>
                                    <asp:Label ID="Label13" runat="server" Width="80px">
                                                                        <%#Eval("InstallBookingDate1","{0:dd MMM yyyy}")%></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Left" SortExpression="SystemDetails">
                                <ItemTemplate>
                                    <asp:Label ID="Label15" runat="server" Width="180px">
                                                                        <%#Eval("SystemDetails").ToString() == "" ? "-" : Eval("SystemDetails")%></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="STC" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left"
                                ItemStyle-HorizontalAlign="left" ItemStyle-Width="100px" SortExpression="STCNumber">
                                <ItemTemplate>
                                    <%#Eval("STCNumber")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cash Amount" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Right" ItemStyle-Width="110px" HeaderStyle-CssClass="right-text" SortExpression="TotalCash">
                                <ItemTemplate>
                                    <%#Eval("TotalCash", "{0:0.00}")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Quote Price" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Right" ItemStyle-Width="150px" HeaderStyle-CssClass="right-text" SortExpression="TotalQuotePrice">
                                <ItemTemplate>
                                    <%#Eval("TotalQuotePrice", "{0:0.00}")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle />
                        <PagerTemplate>
                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                            <div class="pagination">
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                            </div>
                        </PagerTemplate>
                        <PagerStyle CssClass="paginationGrid" />
                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />

                    </asp:GridView>

                <%--   <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                    <asp:Literal ID="ltrPage" runat="server"></asp:Literal></b>--%>
                </div>
                <div class="paginationnew1" runat="server" id="divnopage">
                                                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                <%--<table cellpadding="5" cellspacing="0" border="0" align="left" width="100%" class="printpage printorder">
                                    <tr>
                                        <td>
                                            <table cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td align="right">
                                                        <b>Total:&nbsp;</b><asp:Literal ID="lblTotalLeads" runat="server"></asp:Literal>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>--%>
                <%-- <div class="paginationnew1 printorder" runat="server" id="divnopage">--%>


                <%-- </div>--%>
            </div>
        </div>
    </asp:Panel>
    <%--</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>--%>
    <script>
        $(".paginationGrid td:first").prepend('<%=paginationstr%>');
    </script>
</asp:Content>

