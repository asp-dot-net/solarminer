﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="stockreport.aspx.cs" Inherits="admin_adminfiles_reports_stockreport" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../../includes/reports.ascx" TagName="reports" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Stock Report
          <div id="tdExport" class="pull-right printorder" runat="server"></div>
        </h5>
    </div>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {

                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                }
            </script>
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch" CssClass="printorder">
                    <div class="animate-panel" style="padding-bottom: 0px!important;" id="recordmsg">
                        <div class="messesgarea">
                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>
                    </div>

                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter Responsive-search printorder">
                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                        <table border="0" class="printorder" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <div class="input-group col-sm-2">
                                                        <asp:TextBox ID="txtSearchStockItem" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchStockItem"
                                                            WatermarkText="StockItem" />
                                                    </div>

                                                    <div class="input-group col-sm-2">
                                                        <asp:DropDownList ID="ddllocationsearch" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="">Location</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <div class="input-group date datetimepicker1 col-sm-2">
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                            ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                    </div>
                                                    <div class="input-group date datetimepicker1 col-sm-2">p
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar"></span>
                                                        </span>
                                                        <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                        <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                            ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                            Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                    </div>

                                                    <div class="input-group">
                                                        <asp:DropDownList ID="ddldetails" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="1">Hide Details</asp:ListItem>
                                                            <asp:ListItem Value="2">Show Details</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="input-group">
                                                        <asp:DropDownList ID="ddlcategory" runat="server" AppendDataBoundItems="true"
                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                            <asp:ListItem Value="1">Panels</asp:ListItem>
                                                            <asp:ListItem Value="2">Inverters</asp:ListItem>
                                                            <asp:ListItem Value="3">Electricial Kit</asp:ListItem>
                                                            <asp:ListItem Value="4">Mounting Kit</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>


                                                    <div class="input-group">
                                                        <asp:Button ID="btnSearch" runat="server" ValidationGroup="search" CssClass="btn btn-info btnsearchicon"
                                                            CausesValidation="false" Text="Search" OnClick="btnSearch_Click" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="showdatabox">
                                <div class="row">
                                    <div class="col-md-12 printorder">
                                        <div align="right">
                                            <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>


                                            <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </asp:Panel>
                <asp:Panel ID="panel1" runat="server" CssClass="hpanel">
                    <div class="" id="PanAddUpdate" runat="server">
                        <div class="xscroll">
                            <div id="PanGrid" runat="server">
                                <div>
                                    <div class="table-responsive">
                                        <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover table-striped">
                                            <tr>
                                                <th colspan="12">
                                                    <h4>Daily Inventory Sheet</h4>
                                                </th>
                                            </tr>
                                            <tr>
                                                <td width="180px"></td>
                                                <th colspan="5">Physical Stock</th>
                                                <th colspan="6">Stock allocated to</th>
                                            </tr>
                                            <tr>
                                                <td>Items</td>
                                                <td>This WK Op.Stock</td>
                                                <td>Stock In</td>
                                                <td>Total Stock</td>
                                                <td>Stock Out</td>
                                                <td>Closing Stock</td>
                                                <td>Euro Solar</td>
                                                <td>Whole Sale</td>
                                                <td>Interstate Transfer</td>
                                                <td>Mtce</td>
                                                <td>Total</td>
                                            </tr>
                                            <tr id="trPanels" runat="server" visible="false">
                                                <th>Panels</th>
                                                <td colspan="11"></td>
                                            </tr>
                                            <asp:Repeater ID="rptPanels" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%#Eval("StockItem") %><asp:HiddenField ID="hndStockItemID" runat="server" Value='<%#Eval("StockItemID") %>' />
                                                        </td>
                                                        <td class="center-text"><%#Eval("WKStock") %></td>
                                                        <td>
                                                            <asp:Literal runat="server" ID="ltInstock" Text='<%#Eval("StockIn").ToString()!=string.Empty?Eval("StockIn"):"0" %>'></asp:Literal></td>
                                                        <td class="center-text"><%# Convert.ToInt32(Eval("WKStock"))+Convert.ToInt32(Eval("StockIn")) %></td>
                                                        <%--<%#Eval("TotalStock") %>--%>
                                                        <%-- <td><%#Eval("StockOut") %></td>--%>
                                                        <td class="center-text"><%# Convert.ToInt32(Eval("EuroSolar"))+Convert.ToInt32(Eval("WholeSale"))+Convert.ToInt32(Eval("Interstate"))+Convert.ToInt32(Eval("Mtce")) %></td>
                                                        <td class="center-text"><%# ((Convert.ToInt32(Eval("WKStock"))+Convert.ToInt32(Eval("StockIn")))-(Convert.ToInt32(Eval("EuroSolar"))+Convert.ToInt32(Eval("WholeSale"))+Convert.ToInt32(Eval("Interstate"))+Convert.ToInt32(Eval("Mtce")))) %></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltEurosolar" Text='<%#Eval("EuroSolar") %>'></asp:Literal></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltWholeSale" Text='<%#Eval("WholeSale") %>'></asp:Literal></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltInterState" Text='<%#Eval("Interstate") %>'></asp:Literal></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltMtce" Text='<%#Eval("Mtce") %>'></asp:Literal></td>
                                                        <%--<td><%#Eval("StockOut") %></td>--%>
                                                        <td class="center-text"><%# Convert.ToInt32(Eval("EuroSolar"))+Convert.ToInt32(Eval("WholeSale"))+Convert.ToInt32(Eval("Interstate"))+Convert.ToInt32(Eval("Mtce")) %></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <tr id="trInverters" runat="server" visible="false">
                                                <th>Inverters</th>
                                                <td colspan="11"></td>
                                            </tr>
                                            <asp:Repeater ID="rptInverters" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%#Eval("StockItem") %>
                                                            <asp:HiddenField ID="hndStockItemID" runat="server" Value='<%#Eval("StockItemID") %>' />
                                                        </td>
                                                        <td class="center-text"><%#Eval("WKStock") %></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltInstock" Text='<%#Eval("StockIn").ToString()!=string.Empty?Eval("StockIn"):"0" %>'></asp:Literal>
                                                        </td>
                                                        <td class="center-text"><%# Convert.ToInt32(Eval("WKStock"))+Convert.ToInt32(Eval("StockIn")) %></td>
                                                        <%--<%#Eval("TotalStock") %>--%>
                                                        <%-- <td><%#Eval("StockOut") %></td>--%>
                                                        <td class="center-text"><%# Convert.ToInt32(Eval("EuroSolar"))+Convert.ToInt32(Eval("WholeSale"))+Convert.ToInt32(Eval("Interstate"))+Convert.ToInt32(Eval("Mtce")) %></td>
                                                        <td class="center-text"><%# ((Convert.ToInt32(Eval("WKStock"))+Convert.ToInt32(Eval("StockIn")))-(Convert.ToInt32(Eval("EuroSolar"))+Convert.ToInt32(Eval("WholeSale"))+Convert.ToInt32(Eval("Interstate"))+Convert.ToInt32(Eval("Mtce")))) %></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltEurosolar" Text='<%#Eval("EuroSolar") %>'></asp:Literal></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltWholeSale" Text='<%#Eval("WholeSale") %>'></asp:Literal></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltInterState" Text='<%#Eval("Interstate") %>'></asp:Literal></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltMtce" Text='<%#Eval("Mtce") %>'></asp:Literal></td>
                                                        <td class="center-text"><%# Convert.ToInt32(Eval("EuroSolar"))+Convert.ToInt32(Eval("WholeSale"))+Convert.ToInt32(Eval("Interstate"))+Convert.ToInt32(Eval("Mtce")) %></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <tr id="trElectricialKit" runat="server" visible="false">
                                                <th>Electricial Kit</th>
                                                <td colspan="11"></td>
                                            </tr>
                                            <asp:Repeater ID="rptElectricial" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%#Eval("StockItem") %>
                                                            <asp:HiddenField ID="hndStockItemID" runat="server" Value='<%#Eval("StockItemID") %>' />
                                                        </td>
                                                        <td class="center-text"><%#Eval("WKStock") %></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltInstock" Text='<%#Eval("StockIn").ToString()!=string.Empty?Eval("StockIn"):"0" %>'></asp:Literal>
                                                        </td>
                                                        <td class="center-text"><%# Convert.ToInt32(Eval("WKStock"))+Convert.ToInt32(Eval("StockIn")) %></td>
                                                        <%--<%#Eval("TotalStock") %>--%>
                                                        <%-- <td><%#Eval("StockOut") %></td>--%>
                                                        <td class="center-text"><%# Convert.ToInt32(Eval("EuroSolar"))+Convert.ToInt32(Eval("WholeSale"))+Convert.ToInt32(Eval("Interstate"))+Convert.ToInt32(Eval("Mtce")) %></td>
                                                        <td class="center-text"><%# ((Convert.ToInt32(Eval("WKStock"))+Convert.ToInt32(Eval("StockIn")))-(Convert.ToInt32(Eval("EuroSolar"))+Convert.ToInt32(Eval("WholeSale"))+Convert.ToInt32(Eval("Interstate"))+Convert.ToInt32(Eval("Mtce")))) %></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltEurosolar" Text='<%#Eval("EuroSolar") %>'></asp:Literal></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltWholeSale" Text='<%#Eval("WholeSale") %>'></asp:Literal></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltInterState" Text='<%#Eval("Interstate") %>'></asp:Literal></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltMtce" Text='<%#Eval("Mtce") %>'></asp:Literal></td>
                                                        <td class="center-text"><%# Convert.ToInt32(Eval("EuroSolar"))+Convert.ToInt32(Eval("WholeSale"))+Convert.ToInt32(Eval("Interstate"))+Convert.ToInt32(Eval("Mtce")) %></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <tr id="trMountingKit" runat="server" visible="false">
                                                <th>Mounting Kit</th>
                                                <td colspan="11"></td>
                                            </tr>
                                            <asp:Repeater ID="rptMounting" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%#Eval("StockItem") %>
                                                            <asp:HiddenField ID="hndStockItemID" runat="server" Value='<%#Eval("StockItemID") %>' />
                                                        </td>
                                                        <td class="center-text"><%#Eval("WKStock") %></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltInstock" Text='<%#Eval("StockIn").ToString()!=string.Empty?Eval("StockIn"):"0" %>'></asp:Literal>
                                                        </td>
                                                        <td class="center-text"><%# Convert.ToInt32(Eval("WKStock"))+Convert.ToInt32(Eval("StockIn")) %></td>
                                                        <%--<%#Eval("TotalStock") %>--%>
                                                        <%-- <td><%#Eval("StockOut") %></td>--%>
                                                        <td class="center-text"><%# Convert.ToInt32(Eval("EuroSolar"))+Convert.ToInt32(Eval("WholeSale"))+Convert.ToInt32(Eval("Interstate"))+Convert.ToInt32(Eval("Mtce")) %></td>
                                                        <%--<td><%#Eval("ClosingStock") %></td>--%>
                                                        <td class="center-text"><%# ((Convert.ToInt32(Eval("WKStock"))+Convert.ToInt32(Eval("StockIn")))-(Convert.ToInt32(Eval("EuroSolar"))+Convert.ToInt32(Eval("WholeSale"))+Convert.ToInt32(Eval("Interstate"))+Convert.ToInt32(Eval("Mtce")))) %></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltEurosolar" Text='<%#Eval("EuroSolar") %>'></asp:Literal></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltWholeSale" Text='<%#Eval("WholeSale") %>'></asp:Literal></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltInterState" Text='<%#Eval("Interstate") %>'></asp:Literal></td>
                                                        <td class="center-text">
                                                            <asp:Literal runat="server" ID="ltMtce" Text='<%#Eval("Mtce") %>'></asp:Literal></td>
                                                        <td class="center-text"><%# Convert.ToInt32(Eval("EuroSolar"))+Convert.ToInt32(Eval("WholeSale"))+Convert.ToInt32(Eval("Interstate"))+Convert.ToInt32(Eval("Mtce")) %></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </asp:Panel>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>
