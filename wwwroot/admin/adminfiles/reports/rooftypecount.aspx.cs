﻿using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_rooftypecount : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            rptState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
            rptState.DataBind();

            rptRoofType.DataSource = ClstblRoofTypes.tblRoofTypes_SelectASC();
            rptRoofType.DataBind();
        }
    }
    protected void rptRoofType_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            Repeater rptHidState = (Repeater)e.Item.FindControl("rptHidState");
            rptHidState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinctProjectWise();
            rptHidState.DataBind();
        }
    }
    protected void rptHidState_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndRoofTypeID = ((HiddenField)((Repeater)sender).Parent.FindControl("hndRoofTypeID"));
            HiddenField hndState = (HiddenField)e.Item.FindControl("hndState");
            Repeater rptCount = (Repeater)e.Item.FindControl("rptCount");

            rptCount.DataSource = Reports.tblProjects_Count_RoofType(hndRoofTypeID.Value, hndState.Value, txtStartDate.Text.Trim(), txtEndDate.Text.Trim());
            rptCount.DataBind();
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        rptRoofType.DataSource = ClstblRoofTypes.tblRoofTypes_SelectASC();
        rptRoofType.DataBind();
    }
    //protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    //{
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;

    //    rptRoofType.DataSource = ClstblRoofTypes.tblRoofTypes_SelectASC();
    //    rptRoofType.DataBind();
    //}
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        rptRoofType.DataSource = ClstblRoofTypes.tblRoofTypes_SelectASC();
        rptRoofType.DataBind();
    }
}