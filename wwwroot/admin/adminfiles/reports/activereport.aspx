<%@ Page Title="SolarMiner | Active Report" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    Culture="en-GB" UICulture="en-GB" CodeFile="activereport.aspx.cs" Inherits="admin_adminfiles_reports_activereport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Active Report</h5>
    </div>


    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
           
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                }
                function endrequesthandler(sender, args) {
                }
                function pageLoaded() {

                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $('.redreq').click(function () {
                        formValidate();
                    });
                }
            </script>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {
                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                }

            </script>
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="content animate-panel" style="padding-bottom: 0px!important;">
                        <div class="messesgarea">

                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>
                    </div>
                    <div class=" animate-panel padbtmzero padtopzero">
                        <div>
                            <div class="hpanel marbtmzero">
                                <div class="searchfinal">
                                    <div class="widget-body shadownone brdrgray">
                                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                            <div class="row">
                                                <div>
                                                    <div class="dataTables_filter col-sm-12 Responsive-search printorder">
                                                        <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                                            <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                                <tr>
                                                                    <td>

                                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtStartDate" ErrorMessage=""></asp:RequiredFieldValidator>
                                                                        </div>
                                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtEndDate" CssClass="" ErrorMessage=""></asp:RequiredFieldValidator>

                                                                        </div>
                                                                        <div class="input-group col-sm-1" id="div1" runat="server">
                                                                           
                                                                            <asp:DropDownList ID="ddlTeam" runat="server" AppendDataBoundItems="true" CssClass="myval"
                                                                                aria-controls="DataTables_Table_0">
                                                                                <asp:ListItem Value="">Select Team</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </div>

                                                                        <div class=" input-group col-sm-2" id="div2" runat="server">
                                                                            <asp:DropDownList ID="ddlSalesRep" runat="server" AppendDataBoundItems="true"
                                                                                aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                <asp:ListItem Value="">Sales Rep</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="dynamic"
                                                                                ControlToValidate="ddlSalesRep" CssClass="" ErrorMessage="" ValidationGroup="detail"></asp:RequiredFieldValidator>--%>
                                                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This value is required."
                                                                ValidationGroup="detail" ControlToValidate="ddlSalesRep" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                                                        </div>
                                                                        <div class="input-group">
                                                                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="detail" CssClass="btn btn-info redreq btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <asp:LinkButton ID="btnClearAll" runat="server"
                                                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </asp:Panel>
                                                    </div>
                                                </div>
                                            </div>
                                            <%-- <div class="datashowbox">--%>
                                            <div class="row ">
                                                <div>
                                                    <div class="dataTables_length showdata col-sm-3 printorder">
                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>

                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div id="tdExport" class="pull-right" runat="server">
                                                            <%--  <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                                            <asp:LinkButton ID="lbtnExport" CssClass="btn btn-success btn-xs Excel" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                                                CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel</asp:LinkButton>
                                                            <!--<a href="javascript:window.print();" class="printicon">
                       
                        <i class="fa fa-print"></i>
                    </a>-->


                                                            <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>
                                                            <%--  </ol>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%--</div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panel" runat="server" CssClass="hpanel marbtm15">
                    <div class="widget-body shadownone brdrgray">
                        <div class="panel-body padallzero">
                            <table cellpadding="5" cellspacing="0" border="0" align="left" width="100%" class="printpage col-sm-12">
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td class="paddingleftright10" style="font-size: medium">
                                                    <b>Total Amount:&nbsp;</b><asp:Literal ID="lblTotalAmount" runat="server"></asp:Literal>
                                                </td>
                                                <td>&nbsp;&nbsp;</td>
                                                <td class="paddingleftright10" style="font-size: medium">
                                                    <b>Total Panels:&nbsp;</b><asp:Literal ID="lblTotalPanels" runat="server"></asp:Literal>
                                                </td>
                                                <td>&nbsp;&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="panel1" runat="server" CssClass=" padleftzero padrightzero finalgrid">
                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                        <div id="PanGrid" runat="server">
                            <div class="table-responsive  printArea">

                                <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging"
                                    OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Project #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="ProjectNumber"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="90px">
                                            <ItemTemplate>
                                                <%#Eval("ProjectNumber")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ActiveDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="ActiveDate"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <%#Eval("ActiveDate","{0:dd MMM yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TotalQuotePrice" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="120px" SortExpression="TotalQuotePrice">
                                            <ItemTemplate>
                                                <%#Eval("TotalQuotePrice","{0:0.00}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NumberPanels" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Right" HeaderStyle-CssClass="right-text" ItemStyle-HorizontalAlign="Right" SortExpression="NumberPanels">
                                            <ItemTemplate>
                                                <%#Eval("NumberPanels")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="Customer"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <%#Eval("Customer")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="InvoicePayDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="InvoicePayDate"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <%#Eval("InvoicePayDate","{0:dd MMM yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="InvoicePayTotal" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="InvoicePayTotal"
                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="110px">
                                            <ItemTemplate>
                                                <%#Eval("InvoicePayTotal","{0:0.00}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pay Method" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="PaymentMethod"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <%#Eval("PaymentMethod")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EmpName" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <%#Eval("EmpName")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle />

                                    <PagerTemplate>
                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left; padding-top: 8px"></asp:Label>
                                        <div class="pagination printorder">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                        </div>
                                    </PagerTemplate>
                                    <PagerStyle CssClass="paginationGrid printorder" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                </asp:GridView>
                            </div>
                            <div class="paginationnew1" runat="server" id="divnopage">
                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

            </div>
        
             </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

