using System;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_orderinstalledreport : System.Web.UI.Page
{
    protected string SiteURL;
    static DataView dv;

    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
       // PanNoRecord.Visible = true;
        HidePanels();
        if (!IsPostBack)
        {
            BindVendor();
            rptState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
            rptState.DataBind();
        }
    }
    public void BindVendor()
    {
        DataTable dt = ClstblContacts.tblCustType_SelectVender();

        ddlSearchVendor.DataSource = dt;
        ddlSearchVendor.DataTextField = "Customer";
        ddlSearchVendor.DataValueField = "CustomerID";
        ddlSearchVendor.DataBind();

        DataTable dt1 = ClstblStockItems.tblStockItems_SelectAll();
        ddlitem.DataSource = dt1;
        ddlitem.DataTextField = "StockItem";
        ddlitem.DataValueField = "StockCode";
        ddlitem.DataBind();
    }
    public void BindData()
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        dt = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        dv = new DataView(dt);

        rptlocations.DataSource = dt;
        rptlocations.DataBind();

        //main
        string location = "";
        foreach (RepeaterItem item in rptState.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                location += "," + hdnID.Value.ToString();
            }
        }
        if (location != "")
        {
            location = location.Substring(1);
        }

        dt1 = ClstblStockOrders.tblOrderInstalledReport_Search(ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlitem.SelectedValue, location);
        string table = "";
        if (dt1.Rows.Count > 0)
        {
            foreach (DataRow dr in dt1.Rows)
            {
                table += "<tr>";
                table += "<td  width='150px'>" + dr["StockItem"].ToString() + "</td>";
                DataTable dtqty = ClstblCompanyLocations.tblCompanyLocations_SelectbyStockItemID(dr["StockItemID"].ToString());
                if (dtqty.Rows.Count > 0)
                {
                    foreach (DataRow drqty in dtqty.Rows)
                    {
                        DataTable dttotal = ClstblStockOrders.tblOrderInstalledReport_ByInstalled(ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), drqty["StockItemID"].ToString(), drqty["state"].ToString(), ddltype.SelectedValue);
                        if (dttotal.Rows.Count > 0)
                        {
                            table += "<td width='50%'>" + dttotal.Rows[0]["installed"].ToString() + "</td><td width='50%'>" + dttotal.Rows[0]["received"].ToString() + "</td>";
                        }
                    }
                    
                }
                table += "</tr>";
            }
        }
        ltdata.Text = table;
        //if (dt1.Rows.Count > 0)
        //{
        //    rpt.DataSource = dt1;
        //    rpt.DataBind();
        //}
        if (dt1.Rows.Count > 0)
        {
            PanGrid.Visible = true;
        }
        else
        {
            PanGrid.Visible = false;
        }
    }
    //protected DataTable GetGridData()
    //{
    //    DataTable dt = new DataTable();
    //    dt = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
    //    return dt;
    //}
    //public void BindGrid(int deleteFlag)
    //{
    //    PanGrid.Visible = true;
    //    DataTable dt = new DataTable();
    //    DataTable dt1 = new DataTable();
    //    dt = GetGridData();
    //    dv = new DataView(dt);

    //    if (dt.Rows.Count == 0)
    //    {
    //        if (deleteFlag == 1)
    //        {
    //            SetDelete();
    //        }
    //        else
    //        {
    //            PanNoRecord.Visible = true;
    //        }
    //        PanGrid.Visible = false;
    //    }
    //    else
    //    {
    //        rptlocations.DataSource = dt;
    //        rptlocations.DataBind();

    //        //main
    //        string location = "";
    //        foreach (RepeaterItem item in rptState.Items)
    //        {
    //            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
    //            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

    //            if (chkselect.Checked == true)
    //            {
    //                location += "," + hdnID.Value.ToString();
    //            }
    //        }
    //        if (location != "")
    //        {
    //            location = location.Substring(1);
    //        }

    //        dt1 = ClstblStockOrders.tblOrderInstalledReport_Search(ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), ddlitem.SelectedValue, location);
    //        if (dt1.Rows.Count > 0)
    //        {
    //            rpt.DataSource = dt1;
    //            rpt.DataBind();
    //        }
    //        if (dt1.Rows.Count > 0)
    //        {
    //            PanGrid.Visible = true;
    //        }
    //        else
    //        {
    //            PanGrid.Visible = false;
    //        }

    //        PanNoRecord.Visible = false;
    //    }
    //}


    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    private void HidePanels()
    {
        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;

    }
    public void Reset()
    {
        PanGrid.Visible = true;
        PanSearch.Visible = true;
    }
    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*])", RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);
        return Convert.ToInt32(match.Value);
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
        BindScript();
    }
    //protected void btnClearAll_Click(object sender, EventArgs e)
    //{
    //    ddlSearchVendor.SelectedValue = "";
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    BindData();
    //}
    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);
    }
    //protected void rpt_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{
    //    RepeaterItem item = e.Item;
    //    Repeater rptqty = (Repeater)item.FindControl("rptqty");
    //    HiddenField hdnStockItemID = (HiddenField)item.FindControl("hdnStockItemID");
    //    HiddenField hdnState = (HiddenField)item.FindControl("hdnState");
    //    DataTable dt1 = ClstblCompanyLocations.tblCompanyLocations_SelectbyStockItemID(hdnStockItemID.Value);
    //    if (dt1.Rows.Count > 0)
    //    {
    //        rptqty.DataSource = dt1;
    //        rptqty.DataBind();
    //    }
    //}
    //protected void rptqty_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{
    //    RepeaterItem item = e.Item;
    //    HiddenField hdnStockItemID = (HiddenField)item.FindControl("hdnStockItemID");
    //    HiddenField hdnState = (HiddenField)item.FindControl("hdnState");
    //    Literal ltstate = (Literal)item.FindControl("ltstate");
    //    //DataTable dt1 = ClstblStockOrders.tblOrderInstalledReport_ByLocSearch(ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), hdnStockItemID.Value, hdnState.Value);
    //    DataTable dt1 = ClstblStockOrders.tblOrderInstalledReport_ByInstalled(ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), hdnStockItemID.Value, hdnState.Value, ddltype.SelectedValue);
    //    Literal ltinstalled = (Literal)item.FindControl("ltinstalled");
       
    //    if (dt1.Rows.Count > 0)
    //    {
    //        ltstate.Text = dt1.Rows[0]["received"].ToString();
    //        ltinstalled.Text = dt1.Rows[0]["installed"].ToString();
    //    }
    //    else
    //    {
    //        ltstate.Text = "0";
    //        ltinstalled.Text = "0";
    //    }
    //    //Response.Write("'" + ddlSearchVendor.SelectedValue + "','" + txtStartDate.Text.Trim() + "','" + txtEndDate.Text.Trim() + "','" + hdnStockItemID.Value + "','" + hdnState.Value + "','" + ddltype.SelectedValue+"'<br/>");
    //    //DataTable dt2 = ClstblStockOrders.tblOrderInstalledReport_ByInstalled(ddlSearchVendor.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), hdnStockItemID.Value, hdnState.Value, ddltype.SelectedValue);

    //    //if (dt2.Rows.Count > 0)
    //    //{
    //    //    ltinstalled.Text = dt2.Rows[0]["Qty"].ToString();
    //    //}
    //    //else
    //    //{
    //    //    ltinstalled.Text = "0";
    //    //}
        
    //}


    //EXCEL
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        PrepareGridViewForExport(PanGrid);
        string timestemp = DateTime.Now.ToString("dd-MMM-yyyy H:mm:ss").ToString().Replace(" ", "_");
        string attachment = "attachment; filename=OrderInstalledReport " + timestemp + ".xls";

        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";

        StringWriter sw = new StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        PanGrid.RenderControl(htw);
        Response.Write(sw.ToString());
        Response.End();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {

    }
    private void PrepareGridViewForExport(Control gv)
    {
        Repeater lb = new Repeater();
        Literal l = new Literal();
        string name = String.Empty;

        for (int i = 0; i < gv.Controls.Count; i++)
        {
            if (gv.Controls[i].GetType() == typeof(LinkButton))
            {
                l.Text = (gv.Controls[i] as LinkButton).Text;
                Response.Write(l.Text);
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }
            else if (gv.Controls[i].GetType() == typeof(Label))
            {
                l.Text = (gv.Controls[i] as Label).Text;
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }
            else if (gv.Controls[i].GetType() == typeof(HyperLink))
            {

                l.Text = (gv.Controls[i] as HyperLink).Text;
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }

            else if (gv.Controls[i].GetType() == typeof(DropDownList))
            {
                l.Text = (gv.Controls[i] as DropDownList).SelectedItem.Text;
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }

            else if (gv.Controls[i].GetType() == typeof(CheckBox))
            {
                l.Text = (gv.Controls[i] as CheckBox).Checked ? "True" : "False";
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }
            else if (gv.Controls[i].GetType() == typeof(HiddenField))
            {
                l.Text = "";
                gv.Controls.Remove(gv.Controls[i]);
                gv.Controls.AddAt(i, l);
            }

            if (gv.Controls[i].HasControls())
            {
                PrepareGridViewForExport(gv.Controls[i]);
            }
        }
        //Response.End();
    }


    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlSearchVendor.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        BindData();
    }
}