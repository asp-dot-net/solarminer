﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" Culture="en-GB" UICulture="en-GB"
    AutoEventWireup="true" CodeFile="rooftypecount.aspx.cs" Inherits="admin_adminfiles_reports_rooftypecount" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../../includes/reports.ascx" TagName="reports" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="small-header transition animated fadeIn printorder">
        <div class="hpanel">
            <div class="panel-body">
                <h2 class="font-light m-b-xs">Roof Type Report
                </h2>
            </div>
        </div>
    </div>
    <%--<asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>--%>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
        }
        function endrequesthandler(sender, args) {
        }
        function pageLoaded() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });

            $('.redreq').click(function () {
                formValidate();
            });
        }
    </script>

      <script>
          var prm = Sys.WebForms.PageRequestManager.getInstance();
          prm.add_pageLoaded(pageLoaded);
          //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
          prm.add_beginRequest(beginrequesthandler);
          // raised after an asynchronous postback is finished and control has been returned to the browser.
          prm.add_endRequest(endrequesthandler);
          function beginrequesthandler(sender, args) {
              //shows the modal popup - the update progress
              $('.loading-container').css('display', 'block');
          }
          function endrequesthandler(sender, args) {
              //hide the modal popup - the update progress
              $('.loading-container').css('display', 'none');
          }

            </script>

  
    <asp:Panel runat="server" ID="PanGridSearch">
         <div class="finalheader">
        <div class="panel-body animate-panel" style="padding-bottom: 0px!important;">
            <div class="messesgarea">

                <div class="alert alert-info" id="PanNoRecord" visible="false" runat="server">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>
            </div>
        </div>
               </div>

           <div class="searchfinal">
        <div class="panel-body animate-panel padbtmzero padtopzero">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel marbtmzero">
                        <%--<div class="panel-heading">
                            <div class="panel-tools">
                                
                            </div>
                            <br />
                        </div>--%>
                        <div class="panel-body brdnone">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer displayinlineblock ">
                                <div class="row">
                                    <div>

                                        <div class="dataTables_filter col-sm-11 Responsive-search printorder">
                                            <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                ControlToValidate="txtStartDate" CssClass="errormessage" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                        </div>
                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                ControlToValidate="txtEndDate" CssClass="errormessage" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                        </div>
                                                        <div class="input-group">
                                                            <asp:Button ID="btnSearch" runat="server" ValidationGroup="search" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:LinkButton ID="btnClearAll" runat="server"
                                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-info"><i class="fa-eraser fa"></i>Clear </asp:LinkButton>
                                                        </div>
                                                         <div class="form-group  pull-right printorder">
                                        	<div id="Div1" class="pull-right" runat="server">
                    <a href="javascript:window.print();" class="printicon">
                        <i class="fa fa-print"></i>
                    </a>
                </div>
                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                          
                                    </div>
                                </div>

                                

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
               </div>
    </asp:Panel>
             <div class="finalgrid">
   <div class="">
                            <div class="panel-body">
                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="PanAddUpdate" runat="server">
                                    <div>
        <div class="printArea">
            <table cellpadding="5" cellspacing="0" border="0" width="100%" class="table table-striped table-bordered  rooftype">
                <tr>
                    <td><b>Roof Type</b>
                    </td>
                    <asp:Repeater ID="rptState" runat="server">
                        <ItemTemplate>
                            <td align="center">
                                <b><%# Eval("State") %></b>
                            </td>
                        </ItemTemplate>
                    </asp:Repeater>
                </tr>
                <asp:Repeater ID="rptRoofType" runat="server" OnItemDataBound="rptRoofType_ItemDataBound">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:HiddenField ID="hndRoofTypeID" runat="server" Value='<%#Eval("RoofTypeID") %>' />
                                <%#Eval("RoofType") %>
                            </td>
                            <asp:Repeater ID="rptHidState" runat="server" OnItemDataBound="rptHidState_ItemDataBound">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hndState" runat="server" Value='<%#Eval("State") %>' />
                                    <asp:Repeater ID="rptCount" runat="server">
                                        <ItemTemplate>
                                            <td align="center"><%#Eval("total") %></td>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
  </div>
                                    </div>
                                </div>
       </div>
                 </div>
    <%--</ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>--%>
</asp:Content>

