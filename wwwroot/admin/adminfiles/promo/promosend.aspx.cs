﻿using System;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Twilio;

public partial class admin_adminfiles_promo_promosend : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected static int custompageIndex;
    protected static int startindex;
    protected static int lastpageindex;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            HidePanels();
            custompageIndex = 1;
           // divnopage.Visible = false;
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                tdExport.Visible = true;
            }
            else
            {
                tdExport.Visible = false;
            }
            
                ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
                ddlSelectRecords.DataBind();

                BindSearchDropdon();
                    
        }
    }

    public void BindSearchDropdon()
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        ddlsearchsalerap.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlsearchsalerap.DataMember = "fullname";
        ddlsearchsalerap.DataTextField = "fullname";
        ddlsearchsalerap.DataValueField = "EmployeeID";
        ddlsearchsalerap.DataBind();

        ddlSearchType.DataSource = ClstblCustType.tblCustType_SelectActive();
        ddlSearchType.DataMember = "CustType";
        ddlSearchType.DataTextField = "CustType";
        ddlSearchType.DataValueField = "CustTypeID";
        ddlSearchType.DataBind();

        ddlSearchSource.DataSource = ClstblCustSource.tblCustSource_SelectbyAsc();
        ddlSearchSource.DataMember = "CustSource";
        ddlSearchSource.DataTextField = "CustSource";
        ddlSearchSource.DataValueField = "CustSourceID";
        ddlSearchSource.DataBind();

        lstSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        lstSearchStatus.DataBind();

        rptState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        rptState.DataBind();

        rptTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
        rptTeam.DataBind();

        rptLeadStatus.DataSource = ClstblContacts.tblContLeadStatus_SelectASC();
        rptLeadStatus.DataBind();

        ddlpromooffer.DataSource = ClstblPromoOffer.tblPromoOffer_Select();
        ddlpromooffer.DataMember = "PromoOffer";
        ddlpromooffer.DataTextField = "PromoOffer";
        ddlpromooffer.DataValueField = "PromoOfferID";
        ddlpromooffer.DataBind();
    }

    protected DataTable GetGridData()
    {

        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        //state
        string state = "";
        foreach (RepeaterItem item in rptState.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                state += "," + hdnID.Value.ToString();
            }
        }
        if (state != "")
        {
            state = state.Substring(1);
        }



        //team
        string team = "";
        foreach (RepeaterItem item in rptTeam.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                team += "," + hdnID.Value.ToString();
            }
        }
        if (team != "")
        {
            team = team.Substring(1);
        }

        //leadstatus
        string leadstatus = "";
        foreach (RepeaterItem item in rptLeadStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                leadstatus += "," + hdnID.Value.ToString();
            }
        }
        if (leadstatus != "")
        {
            leadstatus = leadstatus.Substring(1);
        }

        DataTable dt = new DataTable();
        //Response.Write("'" + ddlSearchType.SelectedValue + "','" + txtSearchMobile.Text.Trim() + "','" + txtSearchPhone.Text.Trim() + "','" + ddlSearchRec.SelectedValue + "','" + ddlSearchSource.SelectedValue + "','" + txtSearchClient.Text.Trim() + "','" + txtSearchCompanyNo.Text + "','" + txtSearchFirst.Text + "','" + txtSearchLast.Text + "','" + txtSearchPostCode.Text + "','" + state + "','" + txtSerachCity.Text + "','" + txtSearchEmail.Text + "','" + ddlsearchsalerap.SelectedValue + "','" + team + "','" + txtStartDate.Text + "','" + txtEndDate.Text + "','" + selectedItem + "','" + txtSearchStreet.Text + "','" + ddlSearchSubSource.SelectedValue + "','" + leadstatus + "'");
        //Response.End();
        dt = ClstblPromo.tblPromo_PromoSend(ddlSearchType.SelectedValue, txtSearchMobile.Text.Trim(), txtSearchPhone.Text.Trim(), ddlSearchRec.SelectedValue, ddlSearchSource.SelectedValue, txtSearchClient.Text.Trim(), txtSearchCompanyNo.Text, txtSearchFirst.Text, txtSearchLast.Text, txtSearchPostCode.Text, state, txtSerachCity.Text, txtSearchEmail.Text, ddlsearchsalerap.SelectedValue, team, txtStartDate.Text, txtEndDate.Text, selectedItem, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue, leadstatus);
        //Response.Write(dt.Rows.Count);
        //Response.End();
        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            HidePanels();

            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;

            //divnopage.Visible = false;
            divpromosms.Visible = false;

        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            PanNoRecord.Visible = false;
            //divnopage.Visible = true;
            divpromosms.Visible = true;

            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                     divnopage.Visible = false;
                }
                else
                {
                     divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                     ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                     divnopage.Visible = true;
                     ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
            
           // ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";

        }
    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        bool sucess1 = ClstblContacts.tblContacts_Delete(id);
        //--- do not chage this code start------
        if (sucess1)
        {
            SetDelete();
        }
        else
        {
            SetError();
        }
        GridView1.EditIndex = -1;
        BindGrid(0);
        BindGrid(1);
        //--- do not chage this code end------
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        SttblContacts st = ClstblContacts.tblContacts_SelectByContactID(id);

        InitUpdate();
    }

    protected void chkViewAll_OnCheckedChanged(object sender, EventArgs e)
    {
        if (Roles.IsUserInRole("DSalesRep") || Roles.IsUserInRole("SalesRep"))
        {

        }
        else
        {
            BindGrid(0);
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("company.aspx");
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/company/leadtracker.aspx");
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        InitAdd();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
       // BindScript();
    }

    private string ConvertSortDirectionToSql(SortDirection sortDireciton)
    {
        string m_SortDirection = String.Empty;

        switch (sortDireciton)
        {
            case SortDirection.Ascending:
                m_SortDirection = "ASC";
                break;

            case SortDirection.Descending:
                m_SortDirection = "DESC";
                break;
        }
        return m_SortDirection;
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    public void SetAdd()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetUpdate()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
    }
    public void InitUpdate()
    {
        HidePanels();
    }
    private void HidePanels()
    {
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }
    public void Reset()
    {
    }


    protected void chksmsheadertag_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chksmsheadertag = (CheckBox)GridView1.HeaderRow.FindControl("chksmsheadertag");
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chktagsms = (CheckBox)row.FindControl("chktagsms");

                if (chksmsheadertag.Checked == true)
                {
                    chktagsms.Checked = true;

                }
                else
                {
                    chktagsms.Checked = false;

                }
            }
        }
    }

    //protected void GridView1_RowDataBound(Object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        HyperLink lnkFullname = (HyperLink)e.Row.FindControl("lnkFullname");
    //        HyperLink lnkCustomer = (HyperLink)e.Row.FindControl("lnkCustomer");
    //        HiddenField hndEmployeeID = (HiddenField)e.Row.FindControl("hndEmployeeID");
    //        if (hndEmployeeID.Value != string.Empty)
    //        {
    //            SttblEmployees stSalesRep = ClstblEmployees.tblEmployees_SelectByEmployeeID(hndEmployeeID.Value);
    //            string SalesTeamRep = "";
    //            DataTable dt_empsalerep = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(hndEmployeeID.Value);
    //            if (dt_empsalerep.Rows.Count > 0)
    //            {
    //                foreach (DataRow dr in dt_empsalerep.Rows)
    //                {
    //                    SalesTeamRep += dr["SalesTeamID"].ToString() + ",";
    //                }
    //                SalesTeamRep = SalesTeamRep.Substring(0, SalesTeamRep.Length - 1);
    //            }

    //            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
    //            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
    //            string EmployeeID = stEmp.EmployeeID;
    //            ImageButton gvbtnUpdate = (ImageButton)e.Row.FindControl("gvbtnUpdate");

    //            if (Roles.IsUserInRole("Sales Manager"))
    //            {
    //                //string SalesTeamID = stEmp.SalesTeamID;
    //                string SalesTeam = "";
    //                DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmp.EmployeeID);
    //                if (dt_empsale.Rows.Count > 0)
    //                {
    //                    foreach (DataRow dr in dt_empsale.Rows)
    //                    {
    //                        SalesTeam += dr["SalesTeamID"].ToString() + ",";
    //                    }
    //                    SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
    //                }
    //                string EmpType = stEmp.EmpType;
    //                //if (stSalesRep.EmpType == EmpType && stSalesRep.SalesTeamID == SalesTeamID.ToString())
    //                if (stSalesRep.EmpType == EmpType && SalesTeamRep == SalesTeam)
    //                {
    //                    lnkFullname.Enabled = true;
    //                    lnkCustomer.Enabled = true;
    //                    gvbtnUpdate.Visible = true;
    //                }
    //                else
    //                {
    //                    lnkFullname.Enabled = false;
    //                    lnkCustomer.Enabled = false;
    //                    gvbtnUpdate.Visible = false;
    //                }
    //            }
    //            if (Roles.IsUserInRole("SalesRep"))
    //            {
    //                if (EmployeeID == hndEmployeeID.Value)
    //                {
    //                    lnkFullname.Enabled = true;
    //                    lnkCustomer.Enabled = true;
    //                }
    //                else
    //                {
    //                    lnkFullname.Enabled = false;
    //                    lnkCustomer.Enabled = false;
    //                }

    //                HiddenField hndContactID = (HiddenField)e.Row.FindControl("hndContactID");
    //                SttblContacts stSRep = ClstblContacts.tblContacts_SelectByContactID(hndContactID.Value);

    //                if (stEmp.EmployeeID == stSRep.EmployeeID)
    //                {
    //                    gvbtnUpdate.Visible = true;
    //                }
    //                else
    //                {
    //                    gvbtnUpdate.Visible = false;
    //                }
    //            }
    //        }
    //    }
    //}
    //protected void btnClearAll_Click(object sender, EventArgs e)
    //{
    //    ddlSearchType.SelectedValue = "";
    //    txtSearchMobile.Text = string.Empty;
    //    txtSearchPhone.Text = string.Empty;
    //    ddlSearchRec.SelectedValue = "";
    //    ddlSearchSource.SelectedValue = "";
    //    txtSearchClient.Text = string.Empty;
    //    txtSearchCompanyNo.Text = string.Empty;
    //    txtSearchFirst.Text = string.Empty;
    //    txtSearchLast.Text = string.Empty;
    //    txtSearchPostCode.Text = string.Empty;
    //    //ddlSearchState.SelectedValue = "";
    //    txtSerachCity.Text = string.Empty;
    //    txtSearchEmail.Text = string.Empty;
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    txtSearchStreet.Text = string.Empty;
    //    ddlSearchSubSource.SelectedValue = "";

    //    string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
    //    SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
    //    if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
    //    {
    //        ddlsearchsalerap.SelectedValue = st.EmployeeID;
    //    }

    //    foreach (RepeaterItem item in lstSearchStatus.Items)
    //    {
    //        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
    //        chkselect.Checked = false;
    //    }
    //    foreach (RepeaterItem item in rptTeam.Items)
    //    {
    //        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
    //        chkselect.Checked = false;
    //    }

    //    //BindGrid(0);
    //    GridView1.DataSource = null;
    //    GridView1.DataBind();
    //    PanGrid.Visible = false;

    //   // divnopage.Visible = false;
    //    divpromosms.Visible = false;
    //}
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        //state
        string state = "";
        foreach (RepeaterItem item in rptState.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                state += "," + hdnID.Value.ToString();
            }
        }
        if (state != "")
        {
            state = state.Substring(1);
        }



        //team
        string team = "";
        foreach (RepeaterItem item in rptTeam.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                team += "," + hdnID.Value.ToString();
            }
        }
        if (team != "")
        {
            team = team.Substring(1);
        }

        //leadstatus
        string leadstatus = "";
        foreach (RepeaterItem item in rptLeadStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                leadstatus += "," + hdnID.Value.ToString();
            }
        }
        if (leadstatus != "")
        {
            leadstatus = leadstatus.Substring(1);
        }

        DataTable dt = new DataTable();
        dt = ClstblPromo.tblPromo_PromoSend(ddlSearchType.SelectedValue, txtSearchMobile.Text.Trim(), txtSearchPhone.Text.Trim(), ddlSearchRec.SelectedValue, ddlSearchSource.SelectedValue, txtSearchClient.Text.Trim(), txtSearchCompanyNo.Text, txtSearchFirst.Text, txtSearchLast.Text, txtSearchPostCode.Text, state, txtSerachCity.Text, txtSearchEmail.Text, ddlsearchsalerap.SelectedValue, team, txtStartDate.Text, txtEndDate.Text, selectedItem, txtSearchStreet.Text, ddlSearchSubSource.SelectedValue, leadstatus);
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "Contacts" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 0, 4, 12, 5, 6, 7, 8, 10, 9 };
            string[] arrHeader = { "Contact", "Company", "Employee", "Street", "Suburb", "P/Cd", "State", "Lead Type", "Project Status" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName.ToLower() == "editdetail")
        {

        }

    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    #region pagination
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    Response.Write(lnkpagebtn.);
            //    //lnkpagebtn.Style.Add("class", "pagebtndesign nonepointer");
            //}
        }
    }
    public void PageClick(String id)
    {
        custompageIndex = Convert.ToInt32(id);
        BindGrid(0);
    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        //lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;



        //if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //{
        //    lastpageindex = lastpageindex + 1;
        //}
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
       // ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    #endregion

    public void BindScript()
    {
       // ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    protected void btnpromooffer_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string CreatedBy = st.EmployeeID;

        int rowsCount = GridView1.Rows.Count;
        GridViewRow gridRow;
        int success = 0;
        for (int i = 0; i < rowsCount; i++)
        {
            string id;
            gridRow = GridView1.Rows[i];
            id = GridView1.DataKeys[i].Value.ToString();

            CheckBox chktagsms = (CheckBox)gridRow.FindControl("chktagsms");
            HiddenField hndid = (HiddenField)gridRow.FindControl("hndid");
            if (ddlpromooffer.SelectedValue != string.Empty)
            {
                if (chktagsms.Checked)
                {
                    if (hndid.Value != string.Empty)
                    {
                        SttblContacts stContact = ClstblContacts.tblContacts_SelectByContactID(hndid.Value);
                        SttblPromoOffer stPromooffer = ClstblPromoOffer.tblPromoOffer_SelectByPromoOfferID(ddlpromooffer.SelectedValue);
                        if (stContact.ContMobile != string.Empty && stPromooffer.Description != string.Empty)
                        {
                            string AccountSid = "AC991f3f103f6a5f608278c1e283f139a1";
                            string AuthToken = "be9c00d8443b360082ab02d3d78969b2";
                            var twilio = new TwilioRestClient(AccountSid, AuthToken);//919879111739
                           // var message = twilio.SendMessage("+61428258614", "+61451831980", "Hello!!");
                            var message = twilio.SendMessage("+61428258614", stContact.ContMobile, stPromooffer.Description);
                            Response.Write(message.Sid);
                            if (message.RestException != null)
                            {
                                var error = message.RestException.Message;
                                //Response.Write(error);
                                // handle the error ...
                            }
                            else
                            {
                                ClstblPromo.tblPromo_UpdateIsArchive(stContact.ContMobile, true.ToString());
                                success = ClstblPromo.tblPromo_Insert(hndid.Value, ddlpromooffer.SelectedItem.Text, "2", DateTime.Now.ToString(), "0", "", CreatedBy);
                                ClstblPromo.tblPromo_UpdatePromoOfferID(success.ToString(), ddlpromooffer.SelectedValue.ToString());
                            }
                        }
                    }
                }
            }
        }

    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlSearchType.SelectedValue = "";
        txtSearchMobile.Text = string.Empty;
        txtSearchPhone.Text = string.Empty;
        ddlSearchRec.SelectedValue = "";
        ddlSearchSource.SelectedValue = "";
        txtSearchClient.Text = string.Empty;
        txtSearchCompanyNo.Text = string.Empty;
        txtSearchFirst.Text = string.Empty;
        txtSearchLast.Text = string.Empty;
        txtSearchPostCode.Text = string.Empty;
        //ddlSearchState.SelectedValue = "";
        txtSerachCity.Text = string.Empty;
        txtSearchEmail.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtSearchStreet.Text = string.Empty;
        ddlSearchSubSource.SelectedValue = "";

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
        {
            ddlsearchsalerap.SelectedValue = st.EmployeeID;
        }

        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
        foreach (RepeaterItem item in rptTeam.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        //BindGrid(0);
        GridView1.DataSource = null;
        GridView1.DataBind();
        PanGrid.Visible = false;

        // divnopage.Visible = false;
        divpromosms.Visible = false;
    }
    protected void GridView1_RowDataBound1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink lnkFullname = (HyperLink)e.Row.FindControl("lnkFullname");
            HyperLink lnkCustomer = (HyperLink)e.Row.FindControl("lnkCustomer");
            HiddenField hndEmployeeID = (HiddenField)e.Row.FindControl("hndEmployeeID");
            if (hndEmployeeID.Value != string.Empty)
            {
                SttblEmployees stSalesRep = ClstblEmployees.tblEmployees_SelectByEmployeeID(hndEmployeeID.Value);
                string SalesTeamRep = "";
                DataTable dt_empsalerep = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(hndEmployeeID.Value);
                if (dt_empsalerep.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsalerep.Rows)
                    {
                        SalesTeamRep += dr["SalesTeamID"].ToString() + ",";
                    }
                    SalesTeamRep = SalesTeamRep.Substring(0, SalesTeamRep.Length - 1);
                }

                string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                string EmployeeID = stEmp.EmployeeID;
                ImageButton gvbtnUpdate = (ImageButton)e.Row.FindControl("gvbtnUpdate");

                if (Roles.IsUserInRole("Sales Manager"))
                {
                    //string SalesTeamID = stEmp.SalesTeamID;
                    string SalesTeam = "";
                    DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmp.EmployeeID);
                    if (dt_empsale.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_empsale.Rows)
                        {
                            SalesTeam += dr["SalesTeamID"].ToString() + ",";
                        }
                        SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                    }
                    string EmpType = stEmp.EmpType;
                    //if (stSalesRep.EmpType == EmpType && stSalesRep.SalesTeamID == SalesTeamID.ToString())
                    if (stSalesRep.EmpType == EmpType && SalesTeamRep == SalesTeam)
                    {
                        lnkFullname.Enabled = true;
                        lnkCustomer.Enabled = true;
                        gvbtnUpdate.Visible = true;
                    }
                    else
                    {
                        lnkFullname.Enabled = false;
                        lnkCustomer.Enabled = false;
                        gvbtnUpdate.Visible = false;
                    }
                }
                if (Roles.IsUserInRole("SalesRep"))
                {
                    if (EmployeeID == hndEmployeeID.Value)
                    {
                        lnkFullname.Enabled = true;
                        lnkCustomer.Enabled = true;
                    }
                    else
                    {
                        lnkFullname.Enabled = false;
                        lnkCustomer.Enabled = false;
                    }

                    HiddenField hndContactID = (HiddenField)e.Row.FindControl("hndContactID");
                    SttblContacts stSRep = ClstblContacts.tblContacts_SelectByContactID(hndContactID.Value);

                    if (stEmp.EmployeeID == stSRep.EmployeeID)
                    {
                        gvbtnUpdate.Visible = true;
                    }
                    else
                    {
                        gvbtnUpdate.Visible = false;
                    }
                }
            }
        }
    }
    protected void ddlSearchSource_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSearchSubSource.Items.Clear();
        ListItem lst = new ListItem();
        lst.Text = "Sub Source";
        lst.Value = "";
        ddlSearchSubSource.Items.Add(lst);

        if (ddlSearchSource.SelectedValue != "")
        {
            DataTable dt = ClstblCustSourceSub.tblCustSourceSub_SelectByCSId(ddlSearchSource.SelectedValue);
            if (dt.Rows.Count > 0)
            {
                ddlSearchSubSource.DataSource = dt;
                ddlSearchSubSource.DataMember = "CustSourceSub";
                ddlSearchSubSource.DataTextField = "CustSourceSub";
                ddlSearchSubSource.DataValueField = "CustSourceSubID";
                ddlSearchSubSource.DataBind();
            }
        }
        BindScript();
    }
}