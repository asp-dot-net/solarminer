<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="allcontacts.aspx.cs" Inherits="admin_adminfiles_contacts_allcontacts" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

        <div class="page-body headertopbox">
              <h5 class="row-title"><i class="typcn typcn-th-small"></i>Contacts</h5>
              <div class="pull-right">
                                 <asp:LinkButton runat="server" ID="lnkallcontacts" CssClass="list-group-item" OnClick="lnkallcontacts_Click">All Contacts</asp:LinkButton>
                  
                            </div>
               </div>
    
 <div class="page-body padtopzero">
        <div class="finalgrid">
       <asp:Repeater runat="server" ID="rptteam">
                        <ItemTemplate>
                            <asp:HiddenField runat="server" ID="hdnid" Value='<%# Eval("SalesTeamID") %>' />
                            <asp:LinkButton runat="server" ID="lnkteam" CssClass="list-group-item" OnClick="lnkteam_Click"><%# Eval("SalesTeam") %></asp:LinkButton>
                        </ItemTemplate>
                    </asp:Repeater>
     </div>
            
            <div class="searchfinal" style="margin-top:15px;">
            <div class="widget-body shadownone brdrgray"> 
                     <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                     	<div class="dataTables_filter row">
                            <div class="col-md-12">
                                  <div>
                    <asp:Panel runat="server" ID="PanSearch" DefaultButton="btnGo">
                        <div class="input-group m-t-sm">

                            <asp:TextBox runat="server" ID="txtSearch" placeholder="Search" CssClass="input-md form-control"></asp:TextBox>
                            <span class="input-group-btn">
                                <asp:Button runat="server" ID="btnGo" CssClass="btn btn-sm btn-primary btngocontact" OnClick="btnGo_Click" Text="Go!" />
                            </span>

                        </div>
                    </asp:Panel>
                </div>
                            </div>
                         </div>
                </div>
                </div>
            </div>
            
            <div style="margin-top:15px;">
             <asp:Repeater runat="server" ID="rptnames">
                        <ItemTemplate>
                            <asp:HiddenField runat="server" ID="hdnempid" Value='<%# Eval("EmployeeID") %>' />
                            <asp:LinkButton runat="server" ID="lnkname" CssClass="list-group-item" OnClick="lnkname_Click"><%# Eval("fullname") %></asp:LinkButton>
                        </ItemTemplate>
                    </asp:Repeater>
            </div>
            
            </div>

    <section>
        <header class="header bg-light lter clearfix b-b b-light" runat="server" visible="false" id="divprofile1">
            <%--<a class="btn btn-sm btn-primary"><i class="i i-pencil"></i>Edit</a>--%>
            <h3 class="font-bold m-b-none m-t-none" style="padding-top: 10px;">Profile</h3>
        </header>
        <section class="vbox bg-white">
            <section class="scrollable wrapper-lg" runat="server" visible="false" id="divprofile" style="background-color: white;">
                <div class="row">
                    <div class="col-md-6">
                        <div class="text-center">
                            <asp:Image runat="server" ID="EmpImage" AlternateText="..." CssClass="img-circle m-b" />
                            <div>Profile finished</div>
                            <div class="">
                                <div class="progress progress-xs inline m-b-none" style="width: 128px">
                                    <div class="progress-bar bg-info" data-toggle="tooltip" runat="server" id="divper"></div>
                                </div>
                            </div>
                            <p>
                                <asp:Literal runat="server" ID="ltper"></asp:Literal>%
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3 class="font-bold m-b-none m-t-none">
                            <asp:Literal runat="server" ID="lblfullname"></asp:Literal></h3>
                        <p>
                            <asp:Literal runat="server" ID="ltemail"></asp:Literal>
                        </p>
                        <%--<p><i class="fa fa-lg fa-circle-o text-primary m-r-sm"></i><strong>Art Director</strong></p>--%>
                        <ul class="nav nav-pills nav-stacked">
                            <li class="bg-light"><a href="#"><i class="i i-phone m-r-sm"></i>Call
                                        <asp:Literal runat="server" ID="ltname"></asp:Literal></a></li>
                            <li class="bg-light"><a href="#"><i class="i i-mail m-r-sm"></i>Send Email</a></li>
                            <li class="bg-light"><a href="#"><i class="i i-chat m-r-sm"></i>Send Message</a></li>
                        </ul>
                    </div>
                </div>
                <div class="line b-b m-t m-b"></div>
                <div class="wrapper m-b">
                    <div class="row m-b">
                        <div class="col-xs-6" runat="server" id="divcell" visible="false">
                            <small>Cell Phone</small>
                            <div class="text-lt font-bold">
                                <asp:Literal runat="server" ID="ltcellphone"></asp:Literal>
                            </div>
                        </div>
                        <%--<div class="col-xs-6">
                            <small>Family Phone</small>
                            <div class="text-lt font-bold">+32(0) 3003 234 543</div>
                        </div>--%>
                    </div>
                    <div class="row m-b">
                        <div class="col-xs-6" runat="server" id="divaddress" visible="false">
                            <small>Address</small>
                            <div class="text-lt font-bold">
                                <asp:Literal runat="server" ID="ltaddress"></asp:Literal>
                            </div>
                        </div>
                        <div class="col-xs-6" runat="server" id="divcity" visible="false">
                            <small>City</small>
                            <div class="text-lt font-bold">
                                <asp:Literal runat="server" ID="ltcity"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="row m-b">
                        <div class="col-xs-6" runat="server" id="divstate" visible="false">
                            <small>State</small>
                            <div class="text-lt font-bold">
                                <asp:Literal runat="server" ID="ltstate"></asp:Literal>
                            </div>
                        </div>
                        <div class="col-xs-6" runat="server" id="divpostcode" visible="false">
                            <small>Post&nbsp;Code</small>
                            <div class="text-lt font-bold">
                                <asp:Literal runat="server" ID="ltpostcode"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="row m-b">
                        <div class="col-xs-6" runat="server" id="divdatehire" visible="false">
                            <small>Date Hire</small>
                            <div class="text-lt font-bold">
                                <asp:Literal runat="server" ID="ltdatehire"></asp:Literal>
                            </div>
                        </div>
                        <div class="col-xs-6" runat="server" id="divlocation" visible="false">
                            <small>Location</small>
                            <div class="text-lt font-bold">
                                <asp:Literal runat="server" ID="ltlocation"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <div class="row m-b">
                        <div class="col-xs-6" runat="server" id="divteam" visible="false">
                            <small>Team&nbsp;Name</small>
                            <div class="text-lt font-bold">
                                <asp:Literal runat="server" ID="ltteam"></asp:Literal>
                            </div>
                        </div>
                        <div class="col-xs-6" runat="server" id="divemptype" visible="false">
                            <small>Employee&nbsp;Type</small>
                            <div class="text-lt font-bold">
                                <asp:Literal runat="server" ID="ltemptype"></asp:Literal>
                            </div>
                        </div>
                    </div>
                    <%--<div class="row m-b">
                    <div class="col-xs-6" runat="server" id="divmanager" visible="false">
                        <small>Manager</small>
                        <div class="text-lt font-bold"><asp:Literal runat="server" ID="ltmanager"></asp:Literal></div>
                    </div>
                </div>--%>
                    <div runat="server" id="divbio" visible="false">
                        <small>Bio</small>
                        <div class="text-lt">
                            <asp:Literal runat="server" ID="ltbio"></asp:Literal>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </section>

    <asp:HiddenField runat="server" ID="hdnsrchid" />
    <asp:HiddenField runat="server" ID="hdnindex" />
</asp:Content>

