using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_company_lteamtracker : System.Web.UI.Page
{
    protected string SiteURL;
    static DataView dv;
    protected static int custompageIndex;
    protected static int startindex;
    protected static int lastpageindex;
    protected static string address;
    protected static string openModal = "false";
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        HidePanels();
        if (openModal == "true")
        {
            ModalPopupExtenderEdit.Show();
        }
        if (!IsPostBack)
        {
            ModalPopupExtenderEdit.Hide();
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            custompageIndex = 1;
            GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
            tblassign.Visible = false;
            if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Lead Manager"))
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
                tblassign.Visible = true;
            }
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                tdExport.Visible = true;
            }
            else
            {
                tdExport.Visible = false;
            }
            //if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Installation Manager")))
            //{
            //    tdExport.Visible = true;
            //}
            //else
            //{
            //    tdExport.Visible = false;
            //}
            BindDropDown();
            BindGrid(0);
        }
    }

    public void BindDropDown()
    {
        ddlAppTime.DataSource = ClstblLTeamTime.tblLTeamTime_SelectASC();
        ddlAppTime.DataMember = "Time";
        ddlAppTime.DataTextField = "Time";
        ddlAppTime.DataValueField = "ID";
        ddlAppTime.DataBind();


        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        if (Roles.IsUserInRole("Administrator"))
        {
            userid = "";
        }
        else
        {
            userid = userid;
        }
        ddlcloser.DataSource = ClstblEmployees.tblEmployees_SelectCloser(userid);
        ddlcloser.DataMember = "fullname";
        ddlcloser.DataTextField = "fullname";
        ddlcloser.DataValueField = "EmployeeID";
        ddlcloser.DataBind();

        ddloutdoor.DataSource = ClstblEmployees.tblEmployees_SelectOutDoor(userid);
        ddloutdoor.DataMember = "fullname";
        ddloutdoor.DataTextField = "fullname";
        ddloutdoor.DataValueField = "EmployeeID";
        ddloutdoor.DataBind();

        ddlcustleadstatus.DataSource = ClstblCustomers.tblCustLeadStatus_SelectASc();
        ddlcustleadstatus.DataMember = "CustLeadStatus";
        ddlcustleadstatus.DataTextField = "CustLeadStatus";
        ddlcustleadstatus.DataValueField = "CustLeadStatusID";
        ddlcustleadstatus.DataBind();
    }
    protected DataTable GetGridData()
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = new DataTable();
        if (Roles.IsUserInRole("Administrator"))
        {
            //Response.Write("" + "','" + txtCustomerSearch.Text + "','" + txtsuburb.Text + "','" + txtSearchPostCode.Text + "','" + ddlAppTime.SelectedValue + "','" + txtStartDate.Text.Trim() + "','" + txtEndDate.Text.Trim() + "','" + userid + "','" + ddlSelectDate.SelectedValue + "','" + ddloutdoor.SelectedValue + "','" + ddlcloser.SelectedValue + "','" + ddlcustleadstatus.SelectedValue);
            dt = ClstblCustomers.tblCustomers_SelectLTeam_Eadmin("", txtCustomerSearch.Text, txtsuburb.Text, txtSearchPostCode.Text, ddlAppTime.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), userid, ddlSelectDate.SelectedValue, ddloutdoor.SelectedValue, ddlcloser.SelectedValue, ddlcustleadstatus.SelectedValue);
            GridView1.Columns[2].Visible = true;
        }
        else
        {
            string userid1 = userid;
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("leadgen") || Roles.IsUserInRole("Lead Manager") || Roles.IsUserInRole("Administrator"))
            {
                // userid = "";
            }

            dt = ClstblCustomers.tblCustomers_SelectLTeam(userid, txtCustomerSearch.Text, txtsuburb.Text.Trim(), txtSearchPostCode.Text.Trim(), ddlAppTime.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), userid1, ddlSelectDate.SelectedValue, ddloutdoor.SelectedValue, ddlcloser.SelectedValue, ddlcustleadstatus.SelectedValue);
            GridView1.Columns[2].Visible = false;
        }

        return dt;
    }
    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            HidePanels();

            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;

            }
            PanGrid.Visible = false;
            //divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;

            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    // divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    // divnopage.Visible = true;
                    //ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    private void HidePanels()
    {
        divnopage.Visible = true;
        PanNoRecord.Visible = false;
    }

    protected void btnAssidnLead_Click(object sender, EventArgs e)
    {
        int rowsCount = GridView1.Rows.Count;
        GridViewRow gridRow;
        DropDownList ddlSalesRep;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        for (int i = 0; i < rowsCount; i++)
        {
            string id;
            gridRow = GridView1.Rows[i];
            id = GridView1.DataKeys[i].Value.ToString();
            ddlSalesRep = (DropDownList)gridRow.FindControl("ddlSalesRep");


            if (ddlSalesRep.SelectedValue.ToString() != "")
            {
                //   Response.Write(ddlSalesRep.SelectedValue.ToString()+"</br>");
                bool test = ClstblCustomers.tblCustomers_Update_Assignemployee(id, stEmp.EmployeeID, ddlSalesRep.SelectedValue.ToString(), DateTime.Now.AddHours(14).ToString(), Convert.ToString(true));

                bool test1 = ClstblProjects.tblProjects_UpdateEmployee(id, ddlSalesRep.SelectedValue.ToString());
                bool test2 = ClstblContacts.tblContacts_UpdateEmployee(id, ddlSalesRep.SelectedValue.ToString());

            }
        }
        // Response.End();
        BindGrid(0);
    }

    protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    {
        txtCustomerSearch.Text = string.Empty;
        txtSearchPostCode.Text = string.Empty;
        txtsuburb.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddloutdoor.SelectedValue = "";
        ddlcloser.SelectedValue = "";
        ddlcustleadstatus.SelectedValue = "";
        ddlAppTime.SelectedValue = "";
        ddlSelectDate.SelectedValue = "";

        BindGrid(0);
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = new DataTable();
        if (Roles.IsUserInRole("Administrator"))
        {
            dt = ClstblCustomers.tblCustomers_SelectLTeam("", txtCustomerSearch.Text, txtsuburb.Text, txtSearchPostCode.Text, ddlAppTime.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), userid, ddlSelectDate.SelectedValue, ddloutdoor.SelectedValue, ddlcloser.SelectedValue, ddlcustleadstatus.SelectedValue);
            GridView1.Columns[2].Visible = true;
        }
        else
        {
            string userid1 = userid;
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("leadgen"))
            {
                // userid = "";
            }
            dt = ClstblCustomers.tblCustomers_SelectLTeam(userid, txtCustomerSearch.Text, txtsuburb.Text.Trim(), txtSearchPostCode.Text.Trim(), ddlAppTime.SelectedValue, txtStartDate.Text.Trim(), txtEndDate.Text.Trim(), userid1, ddlSelectDate.SelectedValue, ddloutdoor.SelectedValue, ddlcloser.SelectedValue, ddlcustleadstatus.SelectedValue);
            GridView1.Columns[2].Visible = false;
        }
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "TeamLeadTracker" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 2, 12, 9, 3, 4, 13, 7, 8 };
            string[] arrHeader = { "Company Name", "CreateDate", "Employee", "Suburb", "PCode", "App Date", "App Time", "Remarks" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DropDownList ddlSalesRep = (DropDownList)e.Row.FindControl("ddlSalesRep");
            HiddenField hdnemployeeid = (HiddenField)e.Row.FindControl("hdnemployeeid");
            HiddenField hndCustomerID = (HiddenField)e.Row.FindControl("hndCustomerID");

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
            {
                ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_SelectCloser(userid);
            }
            else
            {
                if (Roles.IsUserInRole("Administrator"))
                {
                    userid = "";
                }
                ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_SelectCloser(userid);
            }
            ddlSalesRep.DataMember = "fullname";
            ddlSalesRep.DataTextField = "fullname";
            ddlSalesRep.DataValueField = "EmployeeID";
            ddlSalesRep.DataBind();
            ddlSalesRep.SelectedValue = hdnemployeeid.Value.ToString();

            e.Row.Cells[0].CssClass = "locked";
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }

    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    #region pagination
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    Response.Write(lnkpagebtn.);
            //    //lnkpagebtn.Style.Add("class", "pagebtndesign nonepointer");
            //}
        }
    }
    public void PageClick(String id)
    {
        custompageIndex = Convert.ToInt32(id);
        BindGrid(0);
    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        //lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;



        //if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //{
        //    lastpageindex = lastpageindex + 1;
        //}
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    #endregion

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "editdetail")
        {
            ModalPopupExtenderEdit.Show();

            hndEditCustID.Value = e.CommandArgument.ToString();
            DataTable dtCont = ClstblContacts.tblContacts_SelectTop(hndEditCustID.Value);
            if (dtCont.Rows.Count > 0)
            {
                txtContMr.Text = dtCont.Rows[0]["ContMr"].ToString();
                txtContFirst.Text = dtCont.Rows[0]["ContFirst"].ToString();
                txtContLast.Text = dtCont.Rows[0]["ContLast"].ToString();
                txtContEmail.Text = dtCont.Rows[0]["ContEmail"].ToString();
                txtContMobile.Text = dtCont.Rows[0]["ContMobile"].ToString();

                SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(hndEditCustID.Value);
                txtCustPhone.Text = stCust.CustPhone;
                rblResCom1.Checked = false;
                rblResCom2.Checked = false;
                rblArea1.Checked = false;
                rblArea2.Checked = false;

                ddlformbayunittype.DataSource = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
                ddlformbayunittype.DataMember = "UnitType";
                ddlformbayunittype.DataTextField = "UnitType";
                ddlformbayunittype.DataValueField = "UnitType";
                ddlformbayunittype.DataBind();

                ddlformbaystreettype.DataSource = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
                ddlformbaystreettype.DataMember = "StreetType";
                ddlformbaystreettype.DataTextField = "StreetType";
                ddlformbaystreettype.DataValueField = "StreetCode";
                ddlformbaystreettype.DataBind();

                if (stCust.ResCom == "1")
                {
                    rblResCom1.Checked = true;
                }
                if (stCust.ResCom == "2")
                {
                    rblResCom2.Checked = true;
                }
                if (stCust.Area != string.Empty)
                {
                    if (stCust.Area == "1")
                    {
                        rblArea1.Checked = true;
                    }
                    if (stCust.Area == "2")
                    {
                        rblArea2.Checked = true;
                    }
                }

                try
                {
                    txtStreetAddressline.Text = stCust.StreetAddress + ", " + stCust.StreetCity + " " + stCust.StreetState + " " + stCust.StreetPostCode;
                }
                catch { }

                txtformbayUnitNo.Text = stCust.unit_number;

                ddlformbayunittype.SelectedValue = stCust.unit_type;

                txtformbayStreetNo.Text = stCust.street_number;
                txtformbaystreetname.Text = stCust.street_name;
                try
                {
                    ddlformbaystreettype.SelectedValue = stCust.street_type;
                }
                catch { }
                txtStreetAddress.Text = stCust.StreetAddress;
                ddlStreetCity.Text = stCust.StreetCity;
                txtStreetState.Text = stCust.StreetState;
                txtStreetPostCode.Text = stCust.StreetPostCode;

            }
            BindScript();
            BindGrid(0);
        }

    }
    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtCustomerSearch.Text = string.Empty;
        txtSearchPostCode.Text = string.Empty;
        txtsuburb.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddloutdoor.SelectedValue = "";
        ddlcloser.SelectedValue = "";
        ddlcustleadstatus.SelectedValue = "";
        ddlAppTime.SelectedValue = "";
        ddlSelectDate.SelectedValue = "";

        BindGrid(0);
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void ibtnEditDetail_Click(object sender, EventArgs e)
    {
        BindScript();
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        string ResCom = false.ToString();
        if (rblResCom1.Checked == true)
        {
            ResCom = "1";
        }
        if (rblResCom2.Checked == true)
        {
            ResCom = "2";
        }
        string Area = false.ToString();
        if (rblArea1.Checked == true)
        {
            Area = "1";
        }
        if (rblArea2.Checked == true)
        {
            Area = "2";
        }

        string formbayUnitNo = txtformbayUnitNo.Text.Trim();
        string formbayunittype = ddlformbayunittype.SelectedValue.Trim();
        string formbayStreetNo = txtformbayStreetNo.Text.Trim();
        string formbaystreetname = txtformbaystreetname.Text.Trim();
        string formbaystreettype = ddlformbaystreettype.SelectedValue.Trim();
        string StreetCity = ddlStreetCity.Text.Trim();
        string StreetState = txtStreetState.Text.Trim();
        string StreetPostCode = txtStreetPostCode.Text.Trim();
        string StreetAddress = formbayunittype + " " + formbayUnitNo + " " + formbayStreetNo + " " + formbaystreetname + " " + formbaystreettype;
        //txtStreetAddress.Text;

        DataTable dtCont = ClstblContacts.tblContacts_SelectTop(hndEditCustID.Value);
        string id = hndEditCustID.Value;
        if (dtCont.Rows.Count > 0)
        {
            // int existaddress = ClstblCustomers.tblCustomers_Exits_Address(formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, StreetCity, StreetState, StreetPostCode);
            DataTable dtaddress = ClstblCustomers.tblCustomers_Exits_Select_StreetAddressByID(id, StreetAddress, StreetCity.Trim(), StreetState.Trim(), StreetPostCode.Trim());
            if (dtaddress.Rows.Count == 0)
            {
                //Response.Write(existaddress);
                // Response.End();
                lbleror.Visible = false;
                string ContactID = dtCont.Rows[0]["ContactID"].ToString();
                bool succ = ClstblCustomers.tblCustomer_Update_Address(hndEditCustID.Value, ddlformbayunittype.SelectedValue.Trim(), txtformbayUnitNo.Text.Trim(), txtformbayStreetNo.Text.Trim(), txtformbaystreetname.Text.Trim(), ddlformbaystreettype.SelectedValue.Trim(), hndstreetsuffix.Value.Trim());
                bool suc = ClstblContacts.tblContacts_UpdateCustDetail(ContactID, txtContMr.Text, txtContFirst.Text, txtContLast.Text, txtContMobile.Text.Trim(), txtContEmail.Text);
                bool add = ClstblCustomers.tblCustomers_UpdateAddress(hndEditCustID.Value, StreetAddress, ddlStreetCity.Text.Trim(), txtStreetState.Text.Trim(), txtStreetPostCode.Text.Trim(), txtContFirst.Text + " " + txtContLast.Text);
                bool Suc1 = ClstblCustomers.tblCustomers_UpdateInContact(hndEditCustID.Value, ResCom, txtCustPhone.Text, Area);
                if (suc && succ && add && Suc1)
                {

                    ModalPopupExtenderEdit.Hide();
                    openModal = "false";
                    BindGrid(0);
                }
            }
            else
            {

                lbleror.Visible = true;
                ModalPopupExtenderEdit.Show();
                openModal = "true";
            }
        }
    }

    protected void txtformbayUnitNo_TextChanged(object sender, EventArgs e)
    {

        ModalPopupExtenderEdit.Show();
        openModal = "true";
        streetaddress();
        //checkExistsAddress();
        BindScript();
        ddlformbayunittype.Focus();
    }


    protected void ddlformbayunittype_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        streetaddress();
        //checkExistsAddress();
        BindScript();
        txtformbayStreetNo.Focus();

    }
    protected void txtformbayStreetNo_TextChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        streetaddress();
        //checkExistsAddress();
        BindScript();
        txtformbaystreetname.Focus();
    }
    protected void txtformbaystreetname_TextChanged(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "address();", true);
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        // Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>doMyAction();</script>");

        streetaddress();
        //checkExistsAddress();


        BindScript();
        ddlformbaystreettype.Focus();


        // ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction1", "funddlformbaystreettypefocus();", true);


    }
    protected void ddlformbaystreettype_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        streetaddress();
        //checkExistsAddress();
        BindScript();
        ddlStreetCity.Focus();
    }

    public void streetaddress()
    {
        address = ddlformbayunittype.SelectedValue + " " + txtformbayUnitNo.Text + " " + txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue;
        txtStreetAddress.Text = address;
        BindScript();

    }

    public void checkExistsAddress()
    {
        BindScript();
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        string StreetCity = ddlStreetCity.Text;
        string StreetState = txtStreetState.Text;
        string StreetPostCode = txtStreetPostCode.Text;
        string StreetAddress = txtStreetAddress.Text;
        int exist = ClstblCustomers.tblCustomers_Exits_Address(hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, StreetCity, StreetState, StreetPostCode);
        DataTable dt1 = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress(StreetAddress, StreetCity, StreetState, StreetPostCode);

        if (dt1.Rows.Count > 0)
        {
            lbleror.Visible = true;
        }
        else
        {
            lbleror.Visible = false;
        }
    }
    protected void btnCancelEdit_Click(object sender, EventArgs e)
    {
        lbleror.Visible = false;
        openModal = "false";
    }
    protected void ddlStreetCity_TextChanged(object sender, EventArgs e)
    {
        BindScript();
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        if (ddlStreetCity.Text != string.Empty)
        {
            DataTable dtStreet = ClstblCustomers.tblPostCodes_SelectBy_PostCodeID(ddlStreetCity.Text);
            if (dtStreet.Rows.Count > 0)
            {
                txtStreetState.Text = dtStreet.Rows[0]["State"].ToString();
                txtStreetPostCode.Text = dtStreet.Rows[0]["PostCode"].ToString();
            }
            streetaddress();
            string[] cityarr = ddlStreetCity.Text.Split('|');
            if (cityarr.Length > 1)
            {
                ddlStreetCity.Text = cityarr[0].Trim();
                txtStreetState.Text = cityarr[1].Trim();
                txtStreetPostCode.Text = cityarr[2].Trim();
            }
            //checkExistsAddress();
            txtContEmail.Focus();
        }

    }


}