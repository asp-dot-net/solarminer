﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_company_STC_Tracker : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected static int custompageIndex;
    protected static int countdata;
    protected static int startindex;
    protected static int lastpageindex;
    protected static int custompagesize = Convert.ToInt32(SiteConfiguration.GetPageSize());

    public class TokenData
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
    public class RootObject
    {
        public TokenData TokenData { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string access_token { get; set; }
    }
    public class TokenObject
    {
        public string access_token { get; set; }
    }
    public class RootObj
    {
        public string Message { get; set; }
        public string code { get; set; }
        //public string[] PVDNumber { get; set; }
        public List<STCData> STCDataList { get; set; }
        public string ManualQuoteNumber { get; set; }
        //public string[] guidvalue { get; set; }
    }
    public class STCData
    {
        public string guidvalue { get; set; }
        public string ProjectNo { get; set; }
        public string PVDNumber { get; set; }
        public string QuickFormID { get; set; }
        public Nullable<DateTime> STCApplied { get; set; }
        public string PVDStatus { get; set; }
    }

    #region CreateJob Model
    public partial class CreateJob
    {
        [JsonProperty("quickformGuids")]
        public string[] quickformGuids { get; set; }

        [JsonProperty("ProjectNumbers")]
        public int[] ProjectNumbers { get; set; }
    }
    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        HidePanels();
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            custompageIndex = 1;

            if (Roles.IsUserInRole("Administrator"))
            {
                fbtn.Visible = true;
            }
            else
            {
                fbtn.Visible = false;
            }

            //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            //SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            //if (Convert.ToBoolean(st_emp.showexcel) == true)
            //{
            //    tdExport.Visible = true;
            //}
            //else
            //{
            //    tdExport.Visible = false;
            //}
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            BindDropDown();
            BindGrid(0);
        }

        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    public void BindDropDown()
    {

        System.Web.UI.WebControls.ListItem item3 = new System.Web.UI.WebControls.ListItem();
        item3.Text = "Select";
        item3.Value = "";
        ddlPVDStatusID.Items.Clear();
        ddlPVDStatusID.Items.Add(item3);

        ddlPVDStatusID.DataSource = ClsProjectSale.tblPVDStatus_SelectActive();
        ddlPVDStatusID.DataValueField = "PVDStatusID";
        ddlPVDStatusID.DataMember = "PVDStatus";
        ddlPVDStatusID.DataTextField = "PVDStatus";
        ddlPVDStatusID.DataBind();

        lstSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        lstSearchStatus.DataBind();

        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();

        ddlSTCCheckedBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlSTCCheckedBy.DataMember = "fullname";
        ddlSTCCheckedBy.DataTextField = "fullname";
        ddlSTCCheckedBy.DataValueField = "EmployeeID";
        ddlSTCCheckedBy.DataBind();

        ddlSTCStatus.DataSource = ClsProjectSale.tblPVDStatus_SelectActive();
        ddlSTCStatus.DataMember = "PVDStatus";
        ddlSTCStatus.DataTextField = "PVDStatus";
        ddlSTCStatus.DataValueField = "PVDStatusID";
        ddlSTCStatus.DataBind();

        ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlInstaller.DataValueField = "ContactID";
        ddlInstaller.DataMember = "Contact";
        ddlInstaller.DataTextField = "Contact";
        ddlInstaller.DataBind();
    }

    protected DataTable GetGridData()
    {
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        //string quickform = "";
        //if (ddlisquickform.SelectedValue.ToString() == "1")
        //{
        //    quickform = "1";
        //}
        //if (ddlisquickform.SelectedValue.ToString() == "2")
        //{
        //    quickform = "0";
        //}

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = new DataTable();

        //Response.Write(txtContactSearch.Text + "--" + txtProjectNumber.Text + "--" + txtMQNsearch.Text + "--" + ddlSTCCheckedBy.SelectedValue + "--" + txtSerachCity.Text + "--" + ddlSearchState.SelectedValue + "--" + txtSearchPostCode.Text + "--" + txtSTCApprovalNo.Text + "--" + txtSTCUpload.Text + "--" + ddlSTCStatus.SelectedValue + "--" + ddlSearchDate.SelectedValue + "--" + txtStartDate.Text + "--" + txtEndDate.Text + "--" + Convert.ToString(chkNoPVD.Checked) + "--" + ddlisquickform.SelectedValue.ToString() + "--" + selectedItem);
        //Response.End();

        dt = ClstblProjects.tblProjects_SelectSTCTrackerSearchNew(txtContactSearch.Text, txtProjectNumber.Text, txtMQNsearch.Text, ddlSTCCheckedBy.SelectedValue, txtSerachCity.Text, ddlSearchState.SelectedValue, txtSearchPostCode.Text, txtSTCApprovalNo.Text, txtSTCUpload.Text, ddlSTCStatus.SelectedValue, ddlSearchDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlIsPVD.SelectedValue, ddlisquickform.SelectedValue.ToString(), selectedItem, ddlSerialNo.SelectedValue, ddlInstaller.SelectedValue);

        //ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        panelHeader.Visible = true;
        DataTable dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
            divnopage.Visible = false;
            panelHeader.Visible = false;
        }
        else
        {
            //Response.Write("dsfd");
            //Response.End();
            GridView1.DataSource = dt;
            GridView1.DataBind();
            BindTotal(dt);
            PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    public void BindTotal(DataTable dt)
    {
        if (dt.Rows.Count > 0)
        {
            int noofpanel = 0, stcvalue = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["NumberPanels"].ToString() != string.Empty)
                {
                    noofpanel += Convert.ToInt32(dr["NumberPanels"]);
                }
                if (dr["STCNumber"].ToString() != string.Empty)
                {
                    stcvalue += Convert.ToInt32(dr["STCNumber"]);
                }
            }
            lbltotalpanel.Text = noofpanel.ToString();
            lblstcvalue.Text = stcvalue.ToString();

            DataTable dtPVD = ClsProjectSale.tblPVDStatus_Select();
            dtPVD.Columns.Add("Total", typeof(int));
            dtPVD.Columns.Add("STC", typeof(int));
            foreach (DataRow dr in dtPVD.Rows)
            {
                string Total = dt.Select("PVDStatus = '" + dr["PVDStatus"] + "'").Length.ToString();
                dr["Total"] = string.IsNullOrEmpty(Total) ? 0 : Convert.ToInt32(Total);

                string STC = dt.AsEnumerable().Where(y => y.Field<string>("PVDStatus") == dr["PVDStatus"].ToString()).Sum(x => x.Field<int?>("STCNumber1")).ToString();
                dr["STC"] = string.IsNullOrEmpty(STC) ? 0 : Convert.ToInt32(STC);
            }

            if (dtPVD.Rows.Count > 0)
            {
                rptHeaderStatus.DataSource = dtPVD;
                rptHeaderStatus.DataBind();
            }
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction()", true);
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else if (Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = false;
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
        BindScript();
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    private void HidePanels()
    {
        PanNoRecord.Visible = false;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        // ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string ProjectID = e.CommandArgument.ToString();
        if (e.CommandName.ToLower() == "docscheck")
        {
            ClstblProjects.tblProjects2_UpdateDocsDone(ProjectID, "True");
        }
        if (e.CommandName.ToLower() == "docsuncheck")
        {
            ClstblProjects.tblProjects2_UpdateDocsDone(ProjectID, "False");
        }
        if (e.CommandName.ToLower() == "nocheck")
        {
            ClstblProjects.tblProjects2_UpdateNoDocs(ProjectID, "True");
        }
        if (e.CommandName.ToLower() == "nouncheck")
        {
            ClstblProjects.tblProjects2_UpdateNoDocs(ProjectID, "False");
        }
        BindGrid(0);
        // BindScript();
    }
    //protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    //{
    //    txtContactSearch.Text = string.Empty;
    //    txtProjectNumber.Text = string.Empty;
    //    txtMQNsearch.Text = string.Empty;
    //    ddlSTCCheckedBy.SelectedValue = "";
    //    txtSerachCity.Text = string.Empty;
    //    ddlSearchState.SelectedValue = "";
    //    txtSearchPostCode.Text = string.Empty;
    //    txtSTCApprovalNo.Text = string.Empty;
    //    txtSTCUpload.Text = string.Empty;
    //    ddlSTCStatus.SelectedValue = "";
    //    ddlSearchDate.SelectedValue = "";
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    foreach (RepeaterItem item in lstSearchStatus.Items)
    //    {
    //        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
    //        chkselect.Checked = false;
    //    }

    //    BindGrid(0);
    //    ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    //}
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }
        //string quickform = "";
        //if (ddlisquickform.SelectedValue.ToString() == "1")
        //{
        //    quickform = "1";
        //}
        //if (ddlisquickform.SelectedValue.ToString() == "2")
        //{
        //    quickform = "0";
        //}
        // DataTable dt = ClstblProjects.tblProjects_SelectSTCTrackerSearch(txtContactSearch.Text, txtProjectNumber.Text, txtMQNsearch.Text, ddlSTCCheckedBy.SelectedValue, txtSerachCity.Text, ddlSearchState.SelectedValue, txtSearchPostCode.Text, txtSTCApprovalNo.Text, txtSTCUpload.Text, ddlSTCStatus.SelectedValue, ddlSearchDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, Convert.ToString(chkNoPVD.Checked), ddlisquickform.SelectedValue.ToString(), selectedItem);
        DataTable dt = GetGridData();
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
            string FileName = "STC Tracker" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 5, 22, 31, 0, 9, 33, 32, 16, 29, 21, 17, 18, 27, 20, };
            string[] arrHeader = { "Project#", "ProjectType", "InstallDate", "Address", "PCode", "Inst Conf", "STC App Date", "PVD No", "PVD Status", "ProjectStatus", "KW", "STC", "InstallerName", "STC Notes" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //lblError.Text = Ex.Message;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {

            ltrPage.Text = "";
        }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtContactSearch.Text = string.Empty;
        txtProjectNumber.Text = string.Empty;
        txtMQNsearch.Text = string.Empty;
        ddlSTCCheckedBy.SelectedValue = "";
        txtSerachCity.Text = string.Empty;
        ddlSearchState.SelectedValue = "";
        txtSearchPostCode.Text = string.Empty;
        txtSTCApprovalNo.Text = string.Empty;
        txtSTCUpload.Text = string.Empty;
        ddlSTCStatus.SelectedValue = "";
        ddlSearchDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlisquickform.ClearSelection();
        ddlSerialNo.ClearSelection();
        ddlInstaller.ClearSelection();
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        BindGrid(0);
        //  ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void lnkjob_Click(object sender, EventArgs e)
    {
        LinkButton lnkReceived = (LinkButton)sender;
        GridViewRow item1 = lnkReceived.NamingContainer as GridViewRow;
        HiddenField hdnProjectID = (HiddenField)item1.FindControl("hdnProjectID");
        HiddenField hdncust = (HiddenField)item1.FindControl("hdncust");
        // hdn_custpopup.Value = hdncust.Value;
        //Response.Write(hdnProjectID.Value);
        //Response.End();
        if (hdnProjectID.Value != string.Empty)
        {

            SttblProjects stpro = ClstblProjects.tblProjects_SelectByProjectID(hdnProjectID.Value);

            ModalPopupExtender1.Show();
            div_popup.Visible = true;
            hdnid.Value = hdnProjectID.Value;
            GetSTCByProject(hdnProjectID.Value);


        }
        else
        {
            div_popup.Visible = false;
            ModalPopupExtender1.Hide();


        }
        BindGrid(0);

    }

    public void GetSTCByProject(string proid)
    {
        if (!string.IsNullOrEmpty(proid))
        {

            string ProjectID = proid;
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);


            try
            {
                txtSTCUploaded.Text = Convert.ToDateTime(st.STCUploaded).ToShortDateString();
            }
            catch { }

            try
            {
                txtSTCApplied.Text = Convert.ToDateTime(st.STCApplied).ToShortDateString();
            }
            catch { }

            txtSTCUploadNumber.Text = st.STCUploadNumber;
            txtPVDNumber.Text = st.PVDNumber;
            ddlPVDStatusID.SelectedValue = st.PVDStatusID;
            txtSTCNotes.Text = st.STCNotes;
            txtformbayid.Text = st.FormbayId;

        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void lstSearchStatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        HiddenField hdnID = (HiddenField)item.FindControl("hdnID");
        Literal ltprojstatus = (Literal)item.FindControl("ltprojstatus");
        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");

        if (Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("Purchase Manager") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Verification") || Roles.IsUserInRole("WarehouseManager") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("Maintenance"))
        {
            if (hdnID.Value == "2")
            {
                e.Item.Visible = false;
            }
        }

    }

    protected void btnUpdateSTC_Click(object sender, EventArgs e)
    {

        string ProjectID = hdnid.Value;
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        string STCUploaded = txtSTCUploaded.Text.Trim();
        string STCApplied = txtSTCApplied.Text.Trim();
        string STCUploadNumber = txtSTCUploadNumber.Text;
        string PVDNumber = txtPVDNumber.Text;
        string PVDStatusID = ddlPVDStatusID.SelectedValue;
        string formbayid = txtformbayid.Text;
        string STCNotes = txtSTCNotes.Text;

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;

        bool sucForms = ClsProjectSale.tblProjects_UpdateSTC(ProjectID, STCUploaded, STCApplied, STCUploadNumber, PVDNumber, PVDStatusID, UpdatedBy);
        bool sucstcnotes = ClsProjectSale.tblProjects_UpdateSTCNotes(ProjectID, STCNotes);
        bool sucForms1 = ClsProjectSale.tblProjects_UpdateSTC_formbayid(ProjectID, formbayid);

        /* -------------------- STC / Invoice Paid -------------------- */
        if (txtSTCApplied.Text.Trim() != string.Empty)
        {
            if (st.InvoiceFU != string.Empty)
            {
                DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "14");
                if (dtStatus.Rows.Count > 0)
                {
                }
                else
                {
                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("14", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                }
                ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "14");
            }
        }
        /* ------------------------------------------------------------ */
        /* ---------------------- pvdapproved ---------------------- */
        if (ddlPVDStatusID.SelectedValue == "7")
        {
            DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "19");
            if (dtStatus.Rows.Count > 0)
            {
            }
            else
            {
                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("19", ProjectID, stEmp.EmployeeID, st.NumberPanels);
            }
            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "19");
        }
        /* ------------------------------------------------------------ */
        /* ---------------------- Complete ---------------------- */
        if (ddlPVDStatusID.SelectedValue == "7" && st.SalesCommPaid != string.Empty && st.paydate != string.Empty)
        {
            DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "5");
            if (dtStatus.Rows.Count > 0)
            {
            }
            else
            {
                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("5", ProjectID, stEmp.EmployeeID, st.NumberPanels);
            }
            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "5");
        }
        /* ------------------------------------------------------ */
        /* ---------------------- paerworkrec back ---------------------- */
        if (txtSTCApplied.Text.Trim() == string.Empty && st.InvoiceFU == string.Empty)
        {
            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "13");
        }
        /* ------------------------------------------------------ */


        if (sucForms)
        {

            SetAdd1();

            //PanSuccess.Visible = true;
        }
        else
        {
            SetExist();
            //PanAlreadExists.Visible = true;

        }
        BindGrid(0);
    }

    protected void btnquickfrom_Click(object sender, EventArgs e)
    {
        try
        {
            String CurrentDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
            DataTable dt = ClstblProjects.tblProjects_SelectGuid_ApprovedSTCStatus(CurrentDate);
            if (dt.Rows.Count > 0)
            {
                int[] projectno = new int[dt.Rows.Count];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    projectno[i] = Convert.ToInt32(dt.Rows[i]["ProjectNumber"].ToString());
                }
                int[] p = projectno;
                litmessage.Text = string.Empty;
                for (int i = 0; i < p.Length; i++)
                {
                    litmessage.Text = litmessage.Text + p[i].ToString();
                }
                PanError.Visible = true;
                //litmessage.Visible = true;
                //try
                //{
                //  lbltesting.Text = "hii4";
                Dictionary<string, string> oJsonObject = new Dictionary<string, string>();
                /// JObject oJsonObject = new JObject();
                oJsonObject.Add("username", "arisesolar");
                oJsonObject.Add("password", "Arise@123");
                oJsonObject.Add("redirect_uri", "http://api.quickforms.com.au/");
                oJsonObject.Add("client_id", "5D95C4B5-EE01-4D87-B1BC-B4C159CBEFDF");
                //oJsonObject.Add("scope", "JobData");
                oJsonObject.Add("scope", "CreateJob");
                oJsonObject.Add("response_type", "code");

                HttpClient client = new HttpClient();

                var content1 = new FormUrlEncodedContent(oJsonObject);
                var result = client.PostAsync("http://api.quickforms.com.au/auth/logon", content1).Result;
                var tokenJson = result.Content.ReadAsStringAsync();
                bool IsSuccess = result.IsSuccessStatusCode;
                litmessage.Text = litmessage.Text + IsSuccess;
                if (IsSuccess)
                {
                    TokenObject RootObject = JsonConvert.DeserializeObject<TokenObject>(tokenJson.Result);
                    string AccessToken = RootObject.access_token;
                    //lbltesting.Text = AccessToken;
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);
                    var content = new CreateJob
                    {
                        quickformGuids = null,
                        ProjectNumbers = projectno
                    };

                    var temp = JsonConvert.SerializeObject(content, Formatting.Indented);
                    // lbltesting.Text = temp;               
                    using (var client1 = new HttpClient())
                    {
                        client1.BaseAddress = new Uri("http://api.quickforms.com.au/api/ApprovedDataFetch/jobData");

                        //HTTP POST
                        var postTask = client1.PostAsJsonAsync<CreateJob>("Job", content);

                        postTask.Wait();

                        var resultdata = postTask.Result;
                        // lbltesting.Text = resultdata.ToString();
                        if (resultdata.IsSuccessStatusCode)
                        {
                            var jsondata = resultdata.Content.ReadAsStringAsync().Result;

                            RootObj JObresult = JsonConvert.DeserializeObject<RootObj>(jsondata);

                            //   litmessage.Text = jsondata.ToString();
                            PanError.Visible = true;
                            litmessage.Text = JObresult.Message;
                            if (JObresult.Message == "success")
                            {
                                for (int i = 0; i < JObresult.STCDataList.Count; i++)
                                {
                                    string stcapplied = string.Empty;
                                    string guid = JObresult.STCDataList[i].guidvalue.ToString();
                                    string projno = JObresult.STCDataList[i].ProjectNo.ToString();
                                    string pvdno = JObresult.STCDataList[i].PVDNumber.ToString();
                                    if (JObresult.STCDataList[i].STCApplied != null)
                                    {
                                        stcapplied = Convert.ToDateTime(JObresult.STCDataList[i].STCApplied).ToString();
                                    }
                                    string quickformid = JObresult.STCDataList[i].QuickFormID.ToString();
                                    string pvdstatus = JObresult.STCDataList[i].PVDStatus.ToString();
                                    DataTable dt1 = ClstblProjects.tblPVDStatus_SelectByPVDStatus(pvdstatus);
                                    string pvdstatusid = dt1.Rows[0]["PVDStatusID"].ToString();
                                    // litmessage.Text = JObresult.Message + projno + pvdno;
                                    bool sucForms = ClsProjectSale.tblProjects_UpdateSTCByProjNo(projno, stcapplied, pvdno, pvdstatusid, quickformid);
                                }
                                litmessage.Text = "Success";
                                PanError.Visible = true;

                                //   lbltesting.Text = JObresult.Message;
                            }
                            else
                            {
                                //   lbltesting.Text = JObresult.Message.ToString();
                                PanError.Visible = true;
                                litmessage.Text = JObresult.Message;
                            }
                        }
                        else
                        {
                            PanError.Visible = true;
                            litmessage.Text = "Service call was not successful.";
                        }
                    }
                }
                else
                {
                    PanError.Visible = true;
                    litmessage.Text = "Login Failed";
                }
                //}
                //catch (Exception ex)
                //{
                //    lbltesting.Text = ex.Message;
                //    //Console.WriteLine(e.Message);
                //}
            }
            else
            {
                PanError.Visible = true;
                litmessage.Text = "No Projects Found";
            }
        }
        catch (Exception ex)
        {
            PanError.Visible = true;
            lbltesting.Text = ex.Message;
            //Console.WriteLine(e.Message);
        }


    }

    protected void lbtnFatch_Click(object sender, EventArgs e)
    {
        //if (ddlFatch.SelectedValue == "1")
        //{
        //    int UpdateCount = UpdateGreenBot();
        //    SetAdd1();
        //}
        //else if (ddlFatch.SelectedValue == "2")
        //{
        //    int UpdateCount = UpdateBridgeSelect();
        //    SetAdd1();
        //}
        //else
        //{
        //    MsgError("Please Select GreenBot/BridgeSelect");
        //}

        if (ddlFatch.SelectedValue == "1")
        {
            UpdateGreenBotFromAllData();
            SetAdd1();
        }
    }

    #region Greenbot
    //protected int UpdateGreenBot()
    //{
    //    int Return = 0;
    //    DataTable dt = ClsSTCTracker.tblProjects_GetProjectNumberByGBBSFlag("1");

    //    if (dt.Rows.Count > 0)
    //    {
    //        for (int i = 0; i < dt.Rows.Count; i++)
    //        {
    //            string ProjectNumber = dt.Rows[i]["ProjectNumber"].ToString();
    //            string PVDNumberDb = dt.Rows[i]["PVDNumber"].ToString();

    //            Job_Response.RootObject JobDetails = new Job_Response.RootObject();
    //            if (!string.IsNullOrEmpty(ProjectNumber))
    //            {
    //                JobDetails = GetGreenbot_JobDetails(ProjectNumber, "arisesolar", "arisesolar1");

    //                if (JobDetails.Status == true)
    //                {
    //                    int index = JobDetails.lstJobData.Count;
    //                    Job_Response.lstJobData lstJobData = JobDetails.lstJobData[index - 1]; // Root Object
    //                    //Job_Response.lstJobData lstJobData = JobDetails.lstJobData[0]; // Root Object
    //                    //Job_Response.JobSystemDetails JobSystemDetails = lstJobData.JobSystemDetails; // get Child object Details

    //                    string PVDnumber = lstJobData.JobSTCDetails.FailedAccreditationCode;
    //                    string PVDStatus = lstJobData.JobSTCStatusData.STCStatus;
    //                    string STCNumber = lstJobData.JobSTCStatusData.CalculatedSTC;
    //                    string STCApliedDate = lstJobData.JobSTCStatusData.STCSubmissionDate;
    //                    STCApliedDate = STCApliedDate == null ? "" : STCApliedDate;
    //                    PVDnumber = PVDnumber == null ? "" : PVDnumber;
    //                    STCNumber = STCNumber == null ? "" : STCNumber;

    //                    string PVDStatusID = "";

    //                    if (PVDStatus == "CER Failed")
    //                    {
    //                        PVDStatusID = "5";
    //                    }
    //                    else if (PVDStatus == "Pending Approval")
    //                    {
    //                        PVDStatusID = "6";
    //                    }
    //                    else if (PVDStatus == "CER Approved")
    //                    {
    //                        PVDStatusID = "7";
    //                    }

    //                    if (!string.IsNullOrEmpty(PVDNumberDb))
    //                    {
    //                        bool Update = ClsSTCTracker.tblProjects_Update_STC_FromGBBS(ProjectNumber, STCNumber, PVDnumber, STCApliedDate);

    //                        if (Update)
    //                        {
    //                            Return++;
    //                        }
    //                    }

    //                    if (PVDStatusID == "5" || PVDStatusID == "6" || PVDStatusID == "7") // PVD Status Failed, Pending or Approved then Update PVD Status
    //                    {
    //                        bool Update1 = ClsSTCTracker.tblProjects_Update_STC_PVDStatutsID(ProjectNumber, PVDStatusID);
    //                    }


    //                }
    //            }
    //        }
    //    }

    //    return Return;
    //}

    //protected Job_Response.RootObject GetGreenbot_JobDetails(string ReferenceNo, string Username, string Password)
    //{
    //    Job_Response.RootObject JobDetails = new Job_Response.RootObject();
    //    try
    //    {
    //        var client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/Login");
    //        var request = new RestRequest(Method.POST);
    //        //request.AddParameter("Username", "arisesolar");
    //        //request.AddParameter("Password", "arisesolar1");
    //        request.AddParameter("Username", Username);
    //        request.AddParameter("Password", Password);
    //        var response = client.Execute(request);
    //        if (response.StatusCode == System.Net.HttpStatusCode.OK)
    //        {
    //            RootObject RootObject = JsonConvert.DeserializeObject<RootObject>(response.Content);
    //            string AccessToken = RootObject.TokenData.access_token; // get Login Access Token

    //            client = new RestSharp.RestClient("https://api.greenbot.com.au/api/Account/GetJobs?RefNumber=" + ReferenceNo);
    //            request = new RestRequest(Method.GET);
    //            client.AddDefaultHeader("Authorization", string.Format("Bearer {0}", AccessToken));
    //            client.AddDefaultHeader("Content-Type", "application/json");
    //            client.AddDefaultHeader("Accept", "application/json");
    //            IRestResponse<Job_Response.RootObject> JsonData = client.Execute<Job_Response.RootObject>(request);
    //            if (JsonData.StatusCode == System.Net.HttpStatusCode.OK)
    //            {
    //                JobDetails = JsonConvert.DeserializeObject<Job_Response.RootObject>(JsonData.Content);
    //            }
    //        }

    //    }
    //    catch (Exception ex)
    //    {

    //    }

    //    return JobDetails;
    //}

    protected void UpdateGreenBotFromAllData()
    {
        //DataTable dt = ClsSTCTracker.tblProjects_GetProjectNumberByGBBSFlag("1");

        bool update = ClstblProjects.Update_STCDetails();
    }
    #endregion

    #region BridgeSelect
    //protected int UpdateBridgeSelect()
    //{
    //    int Return = 0;
    //    DataTable dt = ClsSTCTracker.tblProjects_GetProjectNumberByGBBSFlag("2");

    //    if (dt.Rows.Count > 0)
    //    {
    //        for (int i = 0; i < dt.Rows.Count; i++)
    //        {
    //            string ProjectNumber = dt.Rows[i]["ProjectNumber"].ToString();
    //            string PVDNumberDb = dt.Rows[i]["PVDNumber"].ToString();
    //            //string ProjectNumber = "887405";

    //            Job_Response.SuccessStatus JobDetails = new Job_Response.SuccessStatus();
    //            if (!string.IsNullOrEmpty(ProjectNumber))
    //            {
    //                try
    //                {
    //                    JobDetails = GetJobDetailsFromBS(ProjectNumber);

    //                    if (JobDetails.Description == "Job status found successfully")
    //                    {
    //                        Job_Response.DetailsStatus Details = JobDetails.Details;

    //                        string PVDnumber = Details.Pvd;
    //                        string PVDStatus = Details.pvdStatus;
    //                        string STCNumber = Details.Stc.ToString();
    //                        string STCApliedDate = Details.processedDate;

    //                        string PVDStatusID = "";
    //                        if (PVDStatus == "Failed")
    //                        {
    //                            PVDStatusID = "5";
    //                        }
    //                        else if (PVDStatus == "Pending")
    //                        {
    //                            PVDStatusID = "6";
    //                        }
    //                        else if (PVDStatus == "Approved")
    //                        {
    //                            PVDStatusID = "7";
    //                        }

    //                        if (!string.IsNullOrEmpty(PVDNumberDb))
    //                        {
    //                            bool Update = ClsSTCTracker.tblProjects_Update_STC_FromGBBS(ProjectNumber, STCNumber, PVDnumber, STCApliedDate);

    //                            if (Update)
    //                            {
    //                                Return++;
    //                            }
    //                        }

    //                        if (PVDStatusID == "5" || PVDStatusID == "6" || PVDStatusID == "7") // PVD Status Failed, Pending or Approved then Update PVD Status
    //                        {
    //                            bool Update1 = ClsSTCTracker.tblProjects_Update_STC_PVDStatutsID(ProjectNumber, PVDStatusID);
    //                        }
    //                    }
    //                }
    //                catch (Exception ex)
    //                {

    //                }
    //            }
    //        }
    //    }

    //    return Return;
    //}

    //public Job_Response.SuccessStatus GetJobDetailsFromBS(string JobNumber)
    //{
    //    Job_Response.SuccessStatus ReturnDetails = new Job_Response.SuccessStatus();
    //    try
    //    {
    //        var obj = new ClsBridgeSelect
    //        {
    //            //crmid = "872136"
    //            crmid = JobNumber
    //        };

    //        var json = new JavaScriptSerializer().Serialize(obj);
    //        json = json.Replace(@"\""", @"""");
    //        json = json.Replace(@"""{", "{");
    //        json = json.Replace(@"}""", "}");

    //        string encodeddata = EncodeTo64(json);

    //        string salt = "A6754833B0249A01EE587622869F9D0B0F3CBA67D658CA33458816AE409A0923";
    //        string URL = "https://e2rzenvycd.execute-api.ap-southeast-2.amazonaws.com/prodb/connector/7f2511e53d35a95e36a56e4543da90e2e8a84e3f145544812718fc7bacd3ad7eret/job/status";

    //        string csum = sha256(encodeddata + salt);
    //        string DATA = @"{""data"":" + @"""" + encodeddata.ToString() + @""",""csum"":" + @"""" + csum.ToString() + @"""}";

    //        var client = new RestSharp.RestClient(URL);
    //        var request = new RestRequest(Method.POST);
    //        client.AddDefaultHeader("Accept", "application/json");
    //        request.Parameters.Clear();
    //        request.AddParameter("application/json", DATA, ParameterType.RequestBody);

    //        IRestResponse<Job_Response.RootObjectBSStatus> JsonData = client.Execute<Job_Response.RootObjectBSStatus>(request);

    //        if (JsonData.StatusCode == System.Net.HttpStatusCode.OK)
    //        {
    //            Job_Response.RootObjectBSStatus jobData = JsonConvert.DeserializeObject<Job_Response.RootObjectBSStatus>(JsonData.Content);

    //            Job_Response.SuccessStatus Success = jobData.Success; // Root Object
    //            ReturnDetails = Success; // get Child object Details

    //            //ReturnDetails = Details;
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        //ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('" + ex.Message + "')", true);
    //    }

    //    return ReturnDetails;
    //}

    //static public string EncodeTo64(string toEncode)
    //{
    //    byte[] toEncodeAsBytes = ASCIIEncoding.ASCII.GetBytes(toEncode);
    //    string returnValue = Convert.ToBase64String(toEncodeAsBytes);
    //    return returnValue;
    //}

    //static string sha256(string randomString)
    //{
    //    var crypt = new SHA256Managed();
    //    var hash = new StringBuilder();
    //    byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(randomString));
    //    foreach (byte theByte in crypto)
    //    {
    //        hash.Append(theByte.ToString("x2"));
    //    }
    //    return hash.ToString();
    //}
    #endregion
}
