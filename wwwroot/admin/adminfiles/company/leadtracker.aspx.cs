using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_company_leadtracker : System.Web.UI.Page
{
    protected static string Siteurl;
    protected static string address;
    static DataView dv;
    protected static int custompageIndex;
    protected static int countdata;
    protected static int startindex;
    protected static int lastpageindex;
    protected static string openModal = "false";
    protected static int custompagesize = Convert.ToInt32(SiteConfiguration.GetPageSize());

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (openModal == "true")
        //{
        //    ModalPopupExtenderEdit.Show();
        //}

        // BindGrid(0);
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        Siteurl = st.siteurl;
        HidePanels();
        if (!IsPostBack)
        {
            ModalPopupExtenderEdit.Hide();
            // PanGrid.Visible = true;
            //  cmpNextDate.ValueToCompare = DateTime.Now.AddHours(14).ToShortDateString();
            BindDropDown();
            custompageIndex = 1;
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            GridView1.Columns[GridView1.Columns.Count - 2].Visible = false;
            tblassign.Visible = false;
            if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
            {
                GridView1.Columns[GridView1.Columns.Count - 2].Visible = true;
                tblassign.Visible = true;
            }
            if (Roles.IsUserInRole("SalesRep"))
            {
                ddlTeam.Enabled = false;
                ddlSalesRepSearch.Enabled = false;

                // GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
            }
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                tdExport.Visible = true;
            }
            else
            {
                tdExport.Visible = false;
            }
          
            //if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Installation Manager")))
            //{
            //    tdExport.Visible = true;
            //}
            //else
            //{
            //    tdExport.Visible = false;
            //}
            //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            //SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);

            ddlSelectDate.SelectedIndex = 1;
            //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            if (Roles.IsUserInRole("Administrator"))
            {
                txtStartDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14).ToShortDateString()));
                txtEndDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14).ToShortDateString()));
                ddlTeam.SelectedValue = "1";
                //GridView1.Columns[18].Visible = true;
                //GridView1.Columns[19].Visible = true;
                //GridView1.Columns[17].Visible = true;
            }
            else if (stEmp.SalesTeamID == "7")
            {
                txtStartDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14).ToShortDateString()));
                txtEndDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14).ToShortDateString()));
                ddlTeam.SelectedValue = "7";
                //GridView1.Columns[18].Visible = false;
                //GridView1.Columns[19].Visible = false;
                //GridView1.Columns[17].Visible = false;
            }

            //if (Roles.IsUserInRole("SalesRep"))
            //{
            //    ddlTeam.Visible = false;
            //    ddlSalesRepSearch.Visible = false;
            //    //GridView1.Columns[11].Visible = false;
            //    //thtype.Visible = false;
            //    //tblGrid.Columns[2].Visible = false;
            //    GridView1.Columns[15].Visible = false;
            //}
            BindGrid(0);
        }

    }

    public void BindDropDown()
    {
        lstSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        lstSearchStatus.DataBind();

        ddlSearchType.DataSource = ClstblCustType.tblCustType_SelectActive();
        ddlSearchType.DataMember = "CustType";
        ddlSearchType.DataTextField = "CustType";
        ddlSearchType.DataValueField = "CustTypeID";
        ddlSearchType.DataBind();
        try
        {
            ddlSearchType.SelectedValue = "1";
        }
        catch
        {
        }

        ddlformbayunittype.DataSource = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
        ddlformbayunittype.DataMember = "UnitType";
        ddlformbayunittype.DataTextField = "UnitType";
        ddlformbayunittype.DataValueField = "UnitType";
        ddlformbayunittype.DataBind();


        ddlSearchSource.DataSource = ClstblCustSource.tblCustSource_SelectbyAsc();
        ddlSearchSource.DataMember = "CustSource";
        ddlSearchSource.DataTextField = "CustSource";
        ddlSearchSource.DataValueField = "CustSourceID";
        ddlSearchSource.DataBind();

        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();


        ddlformbaystreettype.DataSource = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
        ddlformbaystreettype.DataMember = "StreetType";
        ddlformbaystreettype.DataTextField = "StreetType";
        ddlformbaystreettype.DataValueField = "StreetCode";
        ddlformbaystreettype.DataBind();


        ddlSearchStatus.DataSource = ClstblContacts.tblContLeadStatus_SelectByAsc();
        ddlSearchStatus.DataMember = "ContLeadStatus";
        ddlSearchStatus.DataTextField = "ContLeadStatus";
        ddlSearchStatus.DataValueField = "ContLeadStatusID";
        ddlSearchStatus.DataBind();
        try
        {
            ddlSearchStatus.SelectedValue = "2";
        }
        catch
        {
        }
        ddlTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
        ddlTeam.DataMember = "SalesTeam";
        ddlTeam.DataTextField = "SalesTeam";
        ddlTeam.DataValueField = "SalesTeamID";
        ddlTeam.DataBind();

        ListItem item5 = new ListItem();
        item5.Text = "Select";
        item5.Value = "";
        ddlSalesRepSearch.Items.Clear();
        ddlSalesRepSearch.Items.Add(item5);

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string SalesTeam = "";
        DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
        if (dt_empsale.Rows.Count > 0)
        {
            foreach (DataRow dr in dt_empsale.Rows)
            {
                SalesTeam += dr["SalesTeamID"].ToString() + ",";
            }
            SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            ddlTeam.SelectedValue = dt_empsale.Rows[0]["SalesTeamID"].ToString();
        }

        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
        {
            if (SalesTeam != string.Empty)
            {
                ddlSalesRepSearch.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlSalesRepSearch.DataSource = ClstblEmployees.tblEmployees_SelectASC_Include();
        }
        ddlSalesRepSearch.DataMember = "fullname";
        ddlSalesRepSearch.DataTextField = "fullname";
        ddlSalesRepSearch.DataValueField = "EmployeeID";
        ddlSalesRepSearch.DataBind();

        if (Roles.IsUserInRole("DSales Rep") || Roles.IsUserInRole("Sales Rep"))
        {
            tdsalerep.Visible = false;
            tdTeam.Visible = false;
        }
        if (Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("CompanyManager"))
        {
            //tdTeam.Visible = false;
            ddlTeam.Enabled = false;
           
            ddlSalesRepSearch.SelectedValue = st.EmployeeID;
        }
        else
        {
            ddlSalesRepSearch.SelectedValue = st.EmployeeID;
        }
        //ddlSearchSubSource.DataSource = ClstblCustSourceSub.tblCustSourceSub_SelectActive();
        //ddlSearchSubSource.DataMember = "CustSourceSub";
        //ddlSearchSubSource.DataTextField = "CustSourceSub";
        //ddlSearchSubSource.DataValueField = "CustSourceSubID";
        //ddlSearchSubSource.DataBind();
    }
    public void BindDataCount()
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string Employeeid = st.EmployeeID;

        if (Roles.IsUserInRole("Administrator"))
        {
            userid = "";
        }
        else
        {
            string userid1 = userid;
            if (Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("CompanyManager"))
            {
                // userid = "";
            }
        }
        string Count = "", SalesTeamID = "", SalesTeamID1 = "", EmpType = "", EmpType1 = "", data1 = "";
        Count = "select COUNT(userid)as cnt from aspnet_Users where  (convert(varchar(100),UserId) in (select userid from tblEmployees where EmployeeID='" + Employeeid + "')) and UserId in (select UserId from aspnet_UsersInRoles where RoleId in (select RoleId from aspnet_Roles where RoleName='DSales Manager' or RoleName='Sales Manager'))";

        DataTable dtcount = ClstblCustomers.query_execute(Count);

        if (dtcount.Rows.Count > 0)
        {

            if (Convert.ToInt32(dtcount.Rows[0]["cnt"].ToString()) > 0)
            {
                EmpType = "(select EmpType from tblEmployees where EmployeeID=" + Employeeid + ")";
                DataTable dtEmpType = ClstblCustomers.query_execute(EmpType);
                if (dtEmpType.Rows.Count > 0)
                {
                    EmpType1 = dtEmpType.Rows[0]["EmpType"].ToString();
                }
                SalesTeamID = "(select STUFF(( SELECT ',' +  Convert(varchar(100),SalesTeamID) FROM tblEmployeeTeam WHERE EmployeeID=tblEmployees.EmployeeID FOR XML PATH ('')) ,1,1,'') AS SalesTeamID  from tblEmployees where EmployeeID=" + Employeeid + ")";
                DataTable dtSalesTeamID = ClstblCustomers.query_execute(SalesTeamID);
                if (dtSalesTeamID.Rows.Count > 0)
                {
                    SalesTeamID1 = dtSalesTeamID.Rows[0]["SalesTeamID"].ToString();
                }
                data1 = " (TE.EmpType=" + EmpType1 + ") and (Convert(nvarchar(200),ST.SalesTeamID) in (select  * from Split(" + SalesTeamID1 + ",','))) and ";

            }
        }
        string user = "";
        if (!Roles.IsUserInRole("DSales Manager") && !Roles.IsUserInRole("Sales Manager"))
        {
            user = "((TE.userid='" + userid + "') or '" + userid + "'='') and";
        }
        string data = "select count(C.CustomerID) as countdata from tblCustomers as C join tblEmployees as TE on C.EmployeeID=TE.EmployeeID join tblEmployeeTeam as ETeam on C.EmployeeID=ETeam.EmployeeID join tblCustType as CT on C.CustTypeID=CT.CustTypeID join tblCustSource as CS on C.CustSourceID=CS.CustSourceID  left outer join(select ContMobile,ContNotes,CustomerID,ContactID,NewDate from tblContacts where ContactID in(select MAX(ContactID) from tblContacts group by CustomerID))as CM on C.CustomerID=CM.CustomerID left outer join(select FollowupDate,NextFollowupDate,CustomerID,[Description] from tblCustInfo where CustInfoID in(select MAX(CustInfoID) from tblCustInfo group by CustomerID))as CI on C.CustomerID=CI.CustomerID left outer join tblSalesTeams as ST on ST.SalesTeamID=ETeam.SalesTeamID left outer join tblEmployees as TE2 on TE2.userid=C.AppFixBy where " + data1 + " " + user + "   TE.ActiveEmp=1 and (isnull(ETeam.SalesTeamID,0)= case when '" + ddlTeam.SelectedValue + "' =0 then isnull(ETeam.SalesTeamID,0) else '" + ddlTeam.SelectedValue + "' end) and (C.CustTypeID='" + ddlSearchType.SelectedValue + "' or  '" + ddlSearchType.SelectedValue + "'=0) and (C.CustSourceID='" + ddlSearchSource.SelectedValue + "' or  '" + ddlSearchSource.SelectedValue + "'=0) and (C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "' or  '" + ddlSearchSubSource.SelectedValue + "'=0) and ((upper(C.StreetAddress) like '%'+'" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'='')  and ((upper(C.StreetCity) like '%'+'" + System.Security.SecurityElement.Escape(txtsuburb.Text) + "'+'%')  or  '" + System.Security.SecurityElement.Escape(txtsuburb.Text) + "'='') and ((upper(C.StreetState) like '%'+'" + ddlSearchState.SelectedValue + "'+'%')  or  '" + ddlSearchState.SelectedValue + "'='') and ((upper(C.StreetPostCode) like '%'+'" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "' +'%') or  '" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'='') and (C.EmployeeID = '" + ddlSalesRepSearch.SelectedValue + "' or  '" + ddlSalesRepSearch.SelectedValue + "'=0) and isnull('" + System.Security.SecurityElement.Escape(txtStartDate.Text) + "',0) <= CASE WHEN '" + ddlSelectDate.SelectedValue + "'=1 THEN convert(datetime,CONVERT(VARCHAR(10),C.CustEntered, 101)) else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=2 then convert(datetime,CONVERT(VARCHAR(10),CI.NextFollowupDate, 101)) else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=3 then convert(datetime,CONVERT(VARCHAR(10),CM.NewDate, 101)) else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=4 then convert(datetime,CONVERT(VARCHAR(10),(SELECT DISTINCT NewDate FROM tblProjects where C.CustomerID=tblProjects.CustomerID), 101)) else convert(datetime,CONVERT(VARCHAR(10),C.CustEntered, 101)) end end end END and (('" + System.Security.SecurityElement.Escape(txtEndDate.Text) + "' >= CASE WHEN '" + ddlSelectDate.SelectedValue + "'=1 THEN convert(datetime,CONVERT(VARCHAR(10),C.CustEntered, 101)) else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=2 then convert(datetime,CONVERT(VARCHAR(10),CI.NextFollowupDate, 101)) else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=3 then convert(datetime,CONVERT(VARCHAR(10),CM.NewDate, 101)) else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=4 then convert(datetime,CONVERT(VARCHAR(10),(SELECT DISTINCT NewDate FROM tblProjects where C.CustomerID=tblProjects.CustomerID), 101)) else isnull('" + System.Security.SecurityElement.Escape(txtEndDate.Text) + "',0) end end end END) or '" + System.Security.SecurityElement.Escape(txtEndDate.Text) + "'='') and C.CustomerID in(select CustomerID from tblContacts where  convert(varchar,ContLeadStatusID)!='' and(((upper(ContLeadStatusID) = '" + ddlSearchStatus.SelectedValue + "' ) or (ISNULL('" + ddlSearchStatus.SelectedValue + "',0)=0)))  and ((ContFirst+ ' '+ContLast like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'='')) ";
        DataTable dt = ClstblCustomers.query_execute(data);
        //Response.Write(dt.Rows.Count);
        //Response.End();
        if (dt.Rows.Count > 0)
        {

            countdata = Convert.ToInt32(dt.Rows[0]["countdata"]);
            hdncountdata.Value = countdata.ToString();
        }
        #region MyRegionForPagination

        //if (custompagesize != 0)
        //{

        //    lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;

        //    if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //    {
        //        lastpageindex = lastpageindex + 1;
        //    }

        //    int pageindexcustom1 = Convert.ToInt32(hdncountdata.Value) / custompagesize;

        //    if (pageindexcustom1 > 0)
        //    {

        //        if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //        {
        //            pageindexcustom1 = pageindexcustom1 + 1;

        //        }

        //        if (pageindexcustom1 >= custompageIndex)
        //        {

        //        }
        //        else
        //        {

        //            custompageIndex = pageindexcustom1;

        //        }

        //        int PageIndex = 3;
        //        //Response.Write(custompageIndex);
        //        //Response.End();
        //        int StartPageIndex = custompageIndex - PageIndex;//10-3=7
        //        int EndPageIndex = custompageIndex + PageIndex;//10+3=13

        //        if (StartPageIndex < 1)
        //        {
        //            EndPageIndex = EndPageIndex - StartPageIndex;
        //        }
        //        if (EndPageIndex > lastpageindex)
        //        {
        //            StartPageIndex = (StartPageIndex - (EndPageIndex - lastpageindex)) + 1;
        //        }
        //        DataTable dt_page = new DataTable();
        //        dt_page.Columns.Add("ID");

        //        for (int i = 1; i <= lastpageindex; i++)
        //        {
        //            if ((StartPageIndex < i && EndPageIndex > i))
        //            {
        //                DataRow dr = dt_page.NewRow();
        //                dr["ID"] = i;
        //                dt_page.Rows.Add(dr);

        //            }
        //        }

        //        rptpage.DataSource = dt_page;
        //        rptpage.DataBind();

        //        if (custompageIndex == 1)
        //        {
        //            lnkfirst.Visible = false;
        //            lnkprevious.Visible = false;
        //            lnknext.Visible = true;
        //            lnklast.Visible = true;
        //        }
        //        if (custompageIndex > 1)
        //        {
        //            lnkfirst.Visible = true;
        //            lnkprevious.Visible = true;
        //        }
        //        if (custompageIndex == EndPageIndex)
        //        {
        //            lnknext.Visible = false;
        //            lnklast.Visible = false;
        //        }
        //        if (lastpageindex == custompageIndex)
        //        {
        //            lnknext.Visible = false;
        //            lnklast.Visible = false;
        //        }
        //        if (custompageIndex > 1 && custompageIndex < lastpageindex)
        //        {
        //            lnkfirst.Visible = true;
        //            lnkprevious.Visible = true;
        //            lnknext.Visible = true;
        //            lnklast.Visible = true;
        //        }
        //    }
        //    else
        //    {
        //        rptpage.DataSource = null;
        //        rptpage.DataBind();
        //        lnkfirst.Visible = false;
        //        lnkprevious.Visible = false;
        //        lnknext.Visible = false;
        //        lnklast.Visible = false;
        //    }
        //}
        //else
        //{
        //    rptpage.DataSource = null;
        //    rptpage.DataBind();
        //    lnkfirst.Visible = false;
        //    lnkprevious.Visible = false;
        //    lnknext.Visible = false;
        //    lnklast.Visible = false;
        //}
        #endregion
    }
    protected DataTable GetGridData()
    {
        //BindDataCount();
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string Employeeid = st.EmployeeID;
        DataTable dt = new DataTable();

        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        if (Roles.IsUserInRole("Administrator"))
        {
            userid = "";

            //Response.Write("hii");

            //Response.Write("" + "=" + ddlSearchType.SelectedValue + "=" + ddlSearchSource.SelectedValue + "=" + ddlSearchStatus.SelectedValue + "=" + ddlSearchSubSource.SelectedValue + "=" + ddlSearchState.SelectedValue + "=" + ddlTeam.SelectedValue + "=" + txtSearchPostCode.Text + "=" + txtsuburb.Text + "=" + txtContactSearch.Text + "=" + txtStartDate.Text + "=" + txtEndDate.Text + "=" + ddlSalesRepSearch.SelectedValue + "=" + userid + "=" + txtSearchStreet.Text + "=" + ddlSelectDate.SelectedValue);
            //Response.End();
            dt = ClstblCustomers.tblCustomers_SelectByUserIdSearchData_admin("", ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchStatus.SelectedValue, ddlSearchSubSource.SelectedValue, ddlSearchState.SelectedValue, ddlTeam.SelectedValue, txtSearchPostCode.Text, txtsuburb.Text, txtContactSearch.Text, txtStartDate.Text, txtEndDate.Text, ddlSalesRepSearch.SelectedValue, userid, txtSearchStreet.Text, ddlSelectDate.SelectedValue, selectedItem,ddlRejectOrNot.SelectedValue);
            // Response.Write(dt.Rows.Count);

            GridView1.Columns[16].Visible = true;
            GridView1.Columns[13].Visible = false;
            GridView1.Columns[2].Visible = true;
        }
        else
        {
            string userid1 = userid;
            if (Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("CompanyManager"))
            {
                userid = "";
                GridView1.Columns[13].Visible = false;
                GridView1.Columns[16].Visible = true;
            }
            //Response.Write("hii23");
           // Response.Write(userid + "=" + ddlSearchType.SelectedValue + "=" + ddlSearchSource.SelectedValue + "=" + ddlSearchStatus.SelectedValue + "=" + ddlSearchSubSource.SelectedValue + "=" + ddlSearchState.SelectedValue + "=" + ddlTeam.SelectedValue + "=" + txtSearchPostCode.Text + "=" + txtsuburb.Text + "=" + txtContactSearch.Text + "=" + txtStartDate.Text + "=" + txtEndDate.Text + "=" + ddlSalesRepSearch.SelectedValue + "=" + userid + "=" + txtSearchStreet.Text + "=" + ddlSelectDate.SelectedValue);
            //Response.End();
            dt = ClstblCustomers.tblCustomers_SelectByUserIdSearchData(userid, ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchStatus.SelectedValue, ddlSearchSubSource.SelectedValue, ddlSearchState.SelectedValue, ddlTeam.SelectedValue, txtSearchPostCode.Text, txtsuburb.Text, txtContactSearch.Text, txtStartDate.Text, txtEndDate.Text, ddlSalesRepSearch.SelectedValue, userid1, txtSearchStreet.Text, ddlSelectDate.SelectedValue, selectedItem, ddlRejectOrNot.SelectedValue);
            GridView1.Columns[2].Visible = true;
            if (Roles.IsUserInRole("SalesRep"))
            {
                GridView1.Columns[13].Visible = false;
                GridView1.Columns[17].Visible = true;
            }
        }
        //Response.End();
        string Count = "", SalesTeamID = "", SalesTeamID1 = "", EmpType = "", EmpType1 = "", data1 = "";
         Count = "select COUNT(userid)as cnt from aspnet_Users where  (convert(varchar(100),UserId) in (select userid from tblEmployees where EmployeeID='" + Employeeid + "')) and UserId in (select UserId from aspnet_UsersInRoles where RoleId in (select RoleId from aspnet_Roles where RoleName='DSales Manager' or RoleName='Sales Manager'))";
        //Count = "0";
        DataTable dtcount = ClstblCustomers.query_execute(Count);

        if (dtcount.Rows.Count > 0)
        {

            if (Convert.ToInt32(dtcount.Rows[0]["cnt"].ToString()) > 0)
            {
                EmpType = "(select EmpType from tblEmployees where EmployeeID=" + Employeeid + ")";
                DataTable dtEmpType = ClstblCustomers.query_execute(EmpType);
                if (dtEmpType.Rows.Count > 0)
                {
                    EmpType1 = dtEmpType.Rows[0]["EmpType"].ToString();
                }
                SalesTeamID = "(select STUFF(( SELECT ',' +  Convert(varchar(100),SalesTeamID) FROM tblEmployeeTeam WHERE EmployeeID=tblEmployees.EmployeeID FOR XML PATH ('')) ,1,1,'') AS SalesTeamID  from tblEmployees where EmployeeID=" + Employeeid + ")";
                DataTable dtSalesTeamID = ClstblCustomers.query_execute(SalesTeamID);
                if (dtSalesTeamID.Rows.Count > 0)
                {
                    SalesTeamID1 = dtSalesTeamID.Rows[0]["SalesTeamID"].ToString();
                }
                data1 = " (TE.EmpType=" + EmpType1 + ") and (Convert(nvarchar(200),ST.SalesTeamID) in (select  * from Split(" + SalesTeamID1 + ",','))) and ";

            }
        }
        string user = "";
        if (!Roles.IsUserInRole("DSales Manager") && !Roles.IsUserInRole("Sales Manager"))
        {
            user = "((TE.userid='" + userid + "') or '" + userid + "'='') and";
        }

        startindex = (custompageIndex - 1) * custompagesize + 1;
        //string data = "select C.CustomerID,C.EmployeeID,C.Customer,C.CustPhone,C.StreetAddress,C.StreetCity,C.StreetState,C.StreetPostCode,C.CustEntered,C.StreetState+ ' - '+C.StreetCity as Location,CT.CustType,CS.CustSource, (select CustSourceSub from tblCustSourceSub where CustSourceSubID=C.CustSourceSubID) as CustSourceSub,(select case when TE.EmpLast is null then TE.EmpFirst else TE.EmpFirst+' '+TE.EmpLast end) as AssignedTo, CM.ContMobile,CM.ContactID,CM.ContNotes,CI.FollowupDate,CI.NextFollowupDate,CI.[Description],C.CustNotes,convert(varchar,C.CustEntered,106)as CEDate,convert(varchar,CI.FollowupDate,106)as FD,convert(varchar,CI.NextFollowupDate,106)as NFD,(select case when TE2.EmpLast is null then TE2.EmpFirst else TE2.EmpFirst+' '+TE2.EmpLast end) as AppFixBy,C.AppFixDate,CM.NewDate as NewDateC,  (SELECT DISTINCT P.NewDate FROM tblProjects as P where C.CustomerID=P.CustomerID)as NewDateP   from tblCustomers as C join tblEmployees as TE on C.EmployeeID=TE.EmployeeID join tblEmployeeTeam as ETeam on C.EmployeeID=ETeam.EmployeeID join tblCustType as CT on C.CustTypeID=CT.CustTypeID join tblCustSource as CS on C.CustSourceID=CS.CustSourceID  left outer join(select ContMobile,ContNotes,CustomerID,ContactID,NewDate from tblContacts where ContactID in(select MAX(ContactID) from tblContacts group by CustomerID))as CM on C.CustomerID=CM.CustomerID left outer join(select FollowupDate,NextFollowupDate,CustomerID,[Description] from tblCustInfo where CustInfoID in(select MAX(CustInfoID) from tblCustInfo group by CustomerID))as CI on C.CustomerID=CI.CustomerID left outer join tblSalesTeams as ST on ST.SalesTeamID=ETeam.SalesTeamID left outer join tblEmployees as TE2 on TE2.userid=C.AppFixBy where ((TE.userid='" + userid + "') or '" + userid + "'='') and  TE.ActiveEmp=1 and (isnull(ETeam.SalesTeamID,0)= case when '" + ddlTeam.SelectedValue + "' =0 then isnull(ETeam.SalesTeamID,0) else '" + ddlTeam.SelectedValue + "' end) and (C.CustTypeID='" + ddlSearchType.SelectedValue + "' or  '" + ddlSearchType.SelectedValue + "'=0) and (C.CustSourceID='" + ddlSearchSource.SelectedValue + "' or  '" + ddlSearchSource.SelectedValue + "'=0) and (C.CustSourceSubID='" + ddlSearchSubSource.SelectedValue + "' or  '" + ddlSearchSubSource.SelectedValue + "'=0) and ((upper(C.StreetAddress) like '%'+'" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtSearchStreet.Text) + "'='') and ((upper(C.StreetCity) like '%'+'" + System.Security.SecurityElement.Escape(txtsuburb.Text) + "'+'%')  or  '" + System.Security.SecurityElement.Escape(txtsuburb.Text) + "'='') and ((upper(C.StreetState) like '%'+'" + ddlSearchState.SelectedValue + "'+'%')  or  '" + ddlSearchState.SelectedValue + "'='') and ((upper(C.StreetPostCode) like '%'+'" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "' +'%') or  '" + System.Security.SecurityElement.Escape(txtSearchPostCode.Text) + "'='') and (C.EmployeeID = '" + ddlSalesRepSearch.SelectedValue + "' or  '" + ddlSalesRepSearch.SelectedValue + "'=0) and isnull('" + System.Security.SecurityElement.Escape(txtStartDate.Text) + "',0) <= CASE WHEN '" + ddlSelectDate.SelectedValue + "'=1 THEN convert(datetime,CONVERT(VARCHAR(10),C.CustEntered, 101)) else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=2 then convert(datetime,CONVERT(VARCHAR(10),CI.NextFollowupDate, 101)) else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=3 then convert(datetime,CONVERT(VARCHAR(10),CM.NewDate, 101)) else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=4 then convert(datetime,CONVERT(VARCHAR(10),(SELECT DISTINCT NewDate FROM tblProjects  where C.CustomerID=tblProjects.CustomerID), 101)) else convert(datetime,CONVERT(VARCHAR(10),C.CustEntered, 101)) end end end END and (('" + System.Security.SecurityElement.Escape(txtEndDate.Text) + "' >= CASE WHEN '" + ddlSelectDate.SelectedValue + "'=1 THEN convert(datetime,CONVERT(VARCHAR(10),C.CustEntered, 101)) else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=2 then convert(datetime,CONVERT(VARCHAR(10),CI.NextFollowupDate, 101)) else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=3 then convert(datetime,CONVERT(VARCHAR(10),CM.NewDate, 101)) else CASE WHEN '" + ddlSelectDate.SelectedValue + "'=4 then convert(datetime,CONVERT(VARCHAR(10),(SELECT DISTINCT NewDate FROM tblProjects  where C.CustomerID=tblProjects.CustomerID), 101)) else isnull('" + System.Security.SecurityElement.Escape(txtEndDate.Text) + "',0) end end end END) or '" + System.Security.SecurityElement.Escape(txtEndDate.Text) + "'='') and C.CustomerID in(select CustomerID from tblContacts where  convert(varchar,ContLeadStatusID)!='' and(((upper(ContLeadStatusID) = '" + ddlSearchStatus.SelectedValue + "' ) or (ISNULL('" + ddlSearchStatus.SelectedValue + "',0)=0)))  and ((ContFirst+ ' '+ContLast like '%'+'" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'+'%') or '" + System.Security.SecurityElement.Escape(txtContactSearch.Text) + "'='')) order by C.CustEntered desc  OFFSET (" + startindex.ToString() + "-1) ROWS FETCH NEXT  (case when " + custompagesize.ToString() + ">0 then(" + custompagesize.ToString() + " ) else (select count(CustomerID) from tblCustomers ) end) ROWS ONLY ;";
        //Response.Write(data);
        //Response.End();
        return dt;
    }
    public void BindGrid(int deleteFlag)
    {

        PanGrid.Visible = true;
        DataTable dt = GetGridData();

        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            //Response.Write(dt.Rows.Count);
            //Response.End();
            HidePanels();

            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;

            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
      //       PanNoRecord.Visible = false;
            divnopage.Visible = true;


            //PanNoRecord.Visible = false;


            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {

                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {

                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                  
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    //Response.Write(iTotalRecords);
                    //Response.End();
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
        //Response.Write(dt.Rows.Count);
        //Response.End();
    }

    protected void ddlSearchSource_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSearchSubSource.Items.Clear();
        ListItem lst = new ListItem();
        lst.Text = "Sub Source";
        lst.Value = "";
        ddlSearchSubSource.Items.Add(lst);

        if (ddlSearchSource.SelectedValue != "")
        {
            DataTable dt = ClstblCustSourceSub.tblCustSourceSub_SelectByCSId(ddlSearchSource.SelectedValue);
            if (dt.Rows.Count > 0)
            {
                ddlSearchSubSource.DataSource = dt;
                ddlSearchSubSource.DataMember = "CustSourceSub";
                ddlSearchSubSource.DataTextField = "CustSourceSub";
                ddlSearchSubSource.DataValueField = "CustSourceSubID";
                ddlSearchSubSource.DataBind();
            }
        }
        // BindScript();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);

    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else if (Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = false;
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
        //   BindScript();
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {

        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        //BindScript();
    }
    private void HidePanels()
    {
        //PanSuccess.Visible = false;
        //PanError.Visible = false;
        //PanNoRecord.Visible = false;

        //PanSearch.Visible = true;
    }

    protected void btnAssidnLead_Click(object sender, EventArgs e)
    {
        int rowsCount = GridView1.Rows.Count;
        GridViewRow gridRow;
        DropDownList ddlSalesRep;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        for (int i = 0; i < rowsCount; i++)
        {
            string id;
            gridRow = GridView1.Rows[i];
            id = GridView1.DataKeys[i].Value.ToString();

            //if(id== "217440")
            //{
            //    string x = "217440";
            //}

            ddlSalesRep = (DropDownList)gridRow.FindControl("ddlSalesRep");

            if (ddlSalesRep.SelectedValue.ToString() != "")
            {
                bool suc = ClstblCustomers.tblCustomers_Update_Assignemployee(id, ddlSalesRep.SelectedValue.ToString(), stEmp.EmployeeID, DateTime.Now.AddHours(14).ToString(), Convert.ToString(true));
                //  Response.Write(suc + "</br>");
                ClstblProjects.tblProjects_UpdateEmployee(id, ddlSalesRep.SelectedValue.ToString());
                ClstblContacts.tblContacts_UpdateEmployee(id, ddlSalesRep.SelectedValue.ToString());
            }
        }
        // Response.End();
        BindGrid(0);
    }

    //protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    //{
    //    ddlSalesRepSearch.SelectedValue = "";
    //    ddlSearchType.SelectedValue = "3";
    //    ddlSearchSource.SelectedValue = "";
    //    ddlSearchStatus.SelectedValue = "2";
    //    ddlSearchSubSource.SelectedValue = "";
    //    ddlSearchState.SelectedValue = "";
    //    ddlTeam.SelectedValue = "";
    //    ddlSelectDate.SelectedValue = "";
    //    txtSearchPostCode.Text = string.Empty;
    //    txtsuburb.Text = string.Empty;
    //    txtContactSearch.Text = string.Empty;
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    txtSearchStreet.Text = string.Empty;

    //    BindGrid(0);
    //}

    protected void ibtnCancel_Click(object sender, EventArgs e)
    {
        ModalPopupExtender2.Hide();
        ddlContact.SelectedValue = "";
        //txtFollowupDate.Text = string.Empty;
        txtNextFollowupDate.Text = string.Empty;
        txtDescription.Text = string.Empty;
        ddlManager.ClearSelection();
    }
    protected void ibtnAdd_Click(object sender, EventArgs e)
    {
        string CustomerID = hndCustomerID.Value;
        string Description = txtDescription.Text;
        //string FollowupDate = txtFollowupDate.Text;
        string NextFollowupDate = txtNextFollowupDate.Text;
        string Reject = ddlreject.SelectedValue;

        if (ddlManager.SelectedValue == "1")
        {
            NextFollowupDate = txtNextFollowupDate.Text;
        }
        if (ddlManager.SelectedValue == "2")
        {
            NextFollowupDate = DateTime.Now.AddHours(14).ToShortDateString();
        }

        string ContactID = ddlContact.SelectedValue.ToString();
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string CustInfoEnteredBy = st.EmployeeID;

        int success = ClstblCustInfo.tblCustInfo_Insert(CustomerID, Description, NextFollowupDate, ContactID, CustInfoEnteredBy, ddlManager.SelectedValue);
        bool update = ClstblCustInfo.tblCustInfo_UpdateReject(success.ToString(),Reject);
        if (Convert.ToString(success) != string.Empty)
        {
            ModalPopupExtender2.Hide();
            ddlContact.SelectedValue = "";

            //txtFollowupDate.Text = string.Empty;
            txtNextFollowupDate.Text = string.Empty;
            txtDescription.Text = string.Empty;
            SetAdd1();
            grdFollowUpBind(ContactID);
        }
        else
        {
            ModalPopupExtender2.Show();
            grdFollowUpBind(ContactID);
            SetError1();
        }
        BindGrid(0);
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        DataTable dt = new DataTable();
        if (Roles.IsUserInRole("Administrator"))
        {
            dt = ClstblCustomers.tblCustomers_SelectByUserIdSearchData_admin("", ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchStatus.SelectedValue, ddlSearchSubSource.SelectedValue, ddlSearchState.SelectedValue, ddlTeam.SelectedValue, txtSearchPostCode.Text, txtsuburb.Text, txtContactSearch.Text, txtStartDate.Text, txtEndDate.Text, ddlSalesRepSearch.SelectedValue, userid, txtSearchStreet.Text, ddlSelectDate.SelectedValue, selectedItem, ddlRejectOrNot.SelectedValue);
            GridView1.Columns[2].Visible = true;
        }
        else
        {
            string userid1 = userid;
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
            {
                // userid = "";
            }
            dt = ClstblCustomers.tblCustomers_SelectByUserIdSearchData(userid, ddlSearchType.SelectedValue, ddlSearchSource.SelectedValue, ddlSearchStatus.SelectedValue, ddlSearchSubSource.SelectedValue, ddlSearchState.SelectedValue, ddlTeam.SelectedValue, txtSearchPostCode.Text, txtsuburb.Text, txtContactSearch.Text, txtStartDate.Text, txtEndDate.Text, ddlSalesRepSearch.SelectedValue, userid1, txtSearchStreet.Text, ddlSelectDate.SelectedValue, selectedItem, ddlRejectOrNot.SelectedValue);
            GridView1.Columns[2].Visible = false;
        }
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "LeadTracker" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            //int[] ColList = { 2, 14, 3, 4, 5, 6, 7, 22, 23, 19, 10, 11, 12, 20, 21, 13 };
            int[] ColList = { 2, 15, 3, 4, 5, 6, 7, 23, 24, 25, 10, 11, 12, 17, 27, 14, 13, 21 };
            string[] arrHeader = { "Company Name", "Mobile", "Phone", "Address", "Suburb", "State", "PCode", "Follow-Up Date", "Next Follow-Up Date", "Description", "Type", "Source", "Sub Source", "Notes", "Lead Date", "Sales Rep", "Team", "Status" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DropDownList ddlSalesRep = (DropDownList)e.Row.FindControl("ddlSalesRep");
            System.Web.UI.HtmlControls.HtmlContainerControl trCancelReason = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Row.FindControl("trCancelReason");
            System.Web.UI.HtmlControls.HtmlContainerControl trCancelDescription = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Row.FindControl("trCancelDescription");
            LinkButton lbtnCancelLead = (LinkButton)e.Row.FindControl("lbtnCancelLead");
            LinkButton lbtnCancelProject = (LinkButton)e.Row.FindControl("lbtnCancelProject");
            HiddenField hdnemployeeid = (HiddenField)e.Row.FindControl("hdnemployeeid");
            HiddenField hndCustomerID = (HiddenField)e.Row.FindControl("hndCustomerID");

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            ListItem item1 = new ListItem();
            item1.Text = "Select";
            item1.Value = "";
            ddlSalesRep.Items.Clear();
            ddlSalesRep.Items.Add(item1);

            if (ddlSearchStatus.SelectedValue == "4")
            {
                trCancelReason.Visible = true;
                trCancelDescription.Visible = true;
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
            {
                string SalesTeam = "";
                DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
                if (dt_empsale.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale.Rows)
                    {
                        SalesTeam += dr["SalesTeamID"].ToString() + ",";
                    }
                    SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                }

                if (SalesTeam != string.Empty)
                {
                    ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
                }
            }
            else
            {
                ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_SelectASC();
            }
           // ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_SelectASC();
            ddlSalesRep.DataMember = "fullname";
            ddlSalesRep.DataTextField = "fullname";
            ddlSalesRep.DataValueField = "EmployeeID";
            ddlSalesRep.DataBind();
            try
            {
                ddlSalesRep.SelectedValue = hdnemployeeid.Value.ToString();

            }
            catch(Exception es)
            {

            }
            

            //if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager") || st.SalesTeamID == "8")
            if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
            {
                //DataTable dt = ClstblContacts.tblContacts_SelectByLeadStatus(hndCustomerID.Value, ddlSearchStatus.SelectedValue);
                if (ddlSearchType.SelectedValue == "1" && ddlSearchStatus.SelectedValue == "2")
                {
                    lbtnCancelLead.Visible = true;
                    // GridView1.Columns[9].Visible = true;
                }
                else
                {
                    lbtnCancelLead.Visible = false;
                    //  GridView1.Columns[9].Visible = false;
                }
                //Response.Write(ddlSearchType.SelectedValue+"==="+ ddlSearchStatus.SelectedValue);
                //Response.End();
                if (ddlSearchType.SelectedValue == "2" && ddlSearchStatus.SelectedValue == "5")
                {
                    lbtnCancelProject.Visible = true;
                    GridView1.Columns[11].Visible = true;
                }
                else
                {
                    lbtnCancelProject.Visible = false;
                    GridView1.Columns[11].Visible = false;
                }
            }
            else
            {
                lbtnCancelLead.Visible = false;
                lbtnCancelProject.Visible = false;
                GridView1.Columns[9].Visible = false;
                GridView1.Columns[10].Visible = false;
            }

            e.Row.Cells[0].CssClass = "locked tdspecialclass";



            System.Web.UI.HtmlControls.HtmlContainerControl thtype = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Row.FindControl("thtype");
            System.Web.UI.HtmlControls.HtmlContainerControl tdtype = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Row.FindControl("tdtype");
            System.Web.UI.HtmlControls.HtmlContainerControl tdsource = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Row.FindControl("tdsource");
            System.Web.UI.HtmlControls.HtmlContainerControl tdsource1 = (System.Web.UI.HtmlControls.HtmlContainerControl)e.Row.FindControl("tdsource1");
            if (Roles.IsUserInRole("SalesRep"))
            {
                ddlTeam.Enabled = false;
                ddlSalesRepSearch.Enabled = false;
                thtype.Visible = false;
                tdtype.Visible = false;
                GridView1.Columns[11].Visible = false;
            }
            if (ddlSelectDate.SelectedValue == "3")
            {
                //GridView1.Columns[11].Visible = true;
                thtype.Visible = true;
                tdtype.Visible = true;
            }
            else
            {
                //GridView1.Columns[11].Visible = false;
                thtype.Visible = false;
                tdtype.Visible = false;
            }
            if (ddlSelectDate.SelectedValue == "4")
            {
                GridView1.Columns[12].Visible = true;
                //tdsource.Visible = true;
                //tdsource1.Visible = true;
            }
            else
            {
                GridView1.Columns[12].Visible = false;
                //tdsource.Visible = false;
                //tdsource1.Visible = false;
            }
        }
    }
    public void grdFollowUpBind(string customerid)
    {
        ddlContact.SelectedValue = customerid;
        DataTable dt = ClstblCustInfo.tblCustInfo_SelectByCustomerID(hndCustomerID.Value);
        if (dt.Rows.Count > 0)
        {
            divgrdFollowUp.Visible = true;
            grdFollowUp.DataSource = dt;
            grdFollowUp.DataBind();
        }
        else
        {
            divgrdFollowUp.Visible = false;
        }
    }
    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlContact.Items.Clear();
        ddlContact.Items.Add(item1);
        lbleror.Visible = false;
        lbleror1.Visible = false;
        if (e.CommandName.ToLower() == "addfollowupnote")
        {
            hndCustomerID.Value = e.CommandArgument.ToString();
            ModalPopupExtender2.Show();
            ModalPopupExtenderEdit.Hide();
            ddlContact.DataSource = ClstblContacts.tblContacts_SelectByCustId(hndCustomerID.Value);
            ddlContact.DataMember = "Contact";
            ddlContact.DataTextField = "Contact";
            ddlContact.DataValueField = "ContactID";
            ddlContact.DataBind();

            DataTable dt_c = ClstblContacts.tblContacts_SelectByCustId(hndCustomerID.Value);
            if (dt_c.Rows.Count > 0)
            {
                ddlContact.SelectedValue = dt_c.Rows[0]["ContactID"].ToString();
                grdFollowUpBind(dt_c.Rows[0]["ContactID"].ToString());
            }



            BindScript();
        }
        if (e.CommandName.ToLower() == "leadcancel")
        {
            ListItem item5 = new ListItem();
            item5.Text = "Select";
            item5.Value = "";
            ddlContLeadCancelReason.Items.Clear();
            ddlContLeadCancelReason.Items.Add(item5);

            ddlContLeadCancelReason.DataSource = ClstblContLeadCancelReason.tblContLeadCancelReason_SelectASC();
            ddlContLeadCancelReason.DataMember = "ContLeadCancelReason";
            ddlContLeadCancelReason.DataTextField = "ContLeadCancelReason";
            ddlContLeadCancelReason.DataValueField = "ContLeadCancelReasonID";
            ddlContLeadCancelReason.DataBind();

            ModalPopupCancelLead.Show();
            ModalPopupExtenderEdit.Hide();
            hndLeadCustID.Value = e.CommandArgument.ToString();
        }
        if (e.CommandName.ToLower() == "projectcancel")
        {
            hndStatusCustomerID.Value = e.CommandArgument.ToString();
            ModalPopupExtenderStatus.Show();
            trCancel.Visible = true;

            ListItem item8 = new ListItem();
            item8.Text = "Select";
            item8.Value = "";
            ddlProjectCancelID.Items.Clear();
            ddlProjectCancelID.Items.Add(item8);

            ddlProjectCancelID.DataSource = ClstblProjectCancel.tblProjectCancel_SelectASC();
            ddlProjectCancelID.DataValueField = "ProjectCancelID";
            ddlProjectCancelID.DataTextField = "ProjectCancel";
            ddlProjectCancelID.DataMember = "ProjectCancel";
            ddlProjectCancelID.DataBind();

            BindGrid(0);
        }
        if (e.CommandName.ToLower() == "editdetail")
        {
            ModalPopupExtenderEdit.Show();
            openModal = "true";
            hndEditCustID.Value = e.CommandArgument.ToString();
            DataTable dtCont = ClstblContacts.tblContacts_SelectTop(hndEditCustID.Value);
            if (dtCont.Rows.Count > 0)
            {
                txtContMr.Text = dtCont.Rows[0]["ContMr"].ToString();
                txtContFirst.Text = dtCont.Rows[0]["ContFirst"].ToString();
                txtContLast.Text = dtCont.Rows[0]["ContLast"].ToString();
                txtContEmail.Text = dtCont.Rows[0]["ContEmail"].ToString().Trim();
                txtContMobile.Text = dtCont.Rows[0]["ContMobile"].ToString();

                SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(hndEditCustID.Value);
                txtCustPhone.Text = stCust.CustPhone;
                rblResCom1.Checked = false;
                rblResCom2.Checked = false;
                rblArea1.Checked = false;
                rblArea2.Checked = false;

                ddlformbayunittype.DataSource = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
                ddlformbayunittype.DataMember = "UnitType";
                ddlformbayunittype.DataTextField = "UnitType";
                ddlformbayunittype.DataValueField = "UnitType";
                ddlformbayunittype.DataBind();

                ddlformbaystreettype.DataSource = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
                ddlformbaystreettype.DataMember = "StreetType";
                ddlformbaystreettype.DataTextField = "StreetType";
                ddlformbaystreettype.DataValueField = "StreetCode";
                ddlformbaystreettype.DataBind();

                if (stCust.ResCom == "1")
                {
                    rblResCom1.Checked = true;
                }
                if (stCust.ResCom == "2")
                {
                    rblResCom2.Checked = true;
                }
                if (stCust.Area != string.Empty)
                {
                    if (stCust.Area == "1")
                    {
                        rblArea1.Checked = true;
                    }
                    if (stCust.Area == "2")
                    {
                        rblArea2.Checked = true;
                    }
                }

                try
                {
                    txtStreetAddressline.Text = stCust.StreetAddress + ", " + stCust.StreetCity + " " + stCust.StreetState + " " + stCust.StreetPostCode;
                }
                catch { }

                txtformbayUnitNo.Text = stCust.unit_number;

                ddlformbayunittype.SelectedValue = stCust.unit_type;

                txtformbayStreetNo.Text = stCust.street_number;
                txtformbaystreetname.Text = stCust.street_name;
                try
                {
                    ddlformbaystreettype.SelectedValue = stCust.street_type;
                }
                catch { }
                txtStreetAddress.Text = stCust.StreetAddress;
                ddlStreetCity.Text = stCust.StreetCity;
                txtStreetState.Text = stCust.StreetState;
                txtStreetPostCode.Text = stCust.StreetPostCode;

            }
            BindScript();
            BindGrid(0);
        }
    }

    protected void ibtnUpdateStatus_Onclick(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        string from = st.from;

        if (ddlProjectCancelID.SelectedValue != string.Empty)
        {
            bool suc = ClstblProjects.tblProjects_UpdatePStatusByCustomerID(hndStatusCustomerID.Value);
            bool suc1 = ClstblContacts.tblcontacts_UpdatereasonByCustomerID(txtprojectcanceldesc.Text, hndStatusCustomerID.Value);
            //TextWriter txtWriter = new StringWriter() as TextWriter;
            //DataTable dtEmail = ClstblContacts.tblContacts_SelectTop(hndStatusCustomerID.Value);
            //string mailtoC = dtEmail.Rows[0]["ContEmail"].ToString();
            //string subjectC = "Project Cancel mail from EURO SOLAR";

            //Server.Execute("~/mailtemplate/projectcancelmail.aspx", txtWriter);
            //try
            //{
            //    Utilities.SendMail(from, mailtoC, subjectC, txtWriter.ToString());
            //}
            //catch
            //{
            //}
            if (suc)
            {
                ModalPopupExtenderStatus.Hide();
                ddlProjectCancelID.SelectedValue = "";
                txtprojectcanceldesc.Text = string.Empty;
                trCancel.Visible = false;
            }
        }
        BindGrid(0);
    }
    protected void ibtnCancelStatus_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderStatus.Hide();
    }
    protected void ibtnCancelLead_Click(object sender, EventArgs e)
    {
        if (ddlContLeadCancelReason.SelectedValue != string.Empty)
        {
            string CustomerID = hndLeadCustID.Value;
            ClstblContacts.tblContacts_UpdateContLeadStatusByCustID("4", CustomerID);
            ClsProjectSale.tblProjects_UpdateProjectStatusIDByCustomerID(CustomerID, "6");
            //Response.Write(ddlContLeadCancelReason.SelectedValue);
            //Response.End();
            bool suc = ClstblContacts.tblContacts_UpdateContLeadCancelReason(ddlContLeadCancelReason.SelectedValue, CustomerID);
            bool suc1 = ClstblContacts.tblContacts_UpdateContLeadCancelReason_desc(txtcanceldesc.Text, CustomerID);
            if (suc)
            {
                ModalPopupCancelLead.Hide();
                txtcanceldesc.Text = "";
                BindGrid(0);
            }
        }
    }
    protected void ibtnEditDetail_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderEdit.Show();
        string ResCom = "0";
        //int a = Convert.ToInt32(ResCom.ToString());
        //Response.Write(a);
        //Response.End();
        if (rblResCom1.Checked == true)
        {
            ResCom = "1";
        }
        if (rblResCom2.Checked == true)
        {
            ResCom = "2";
        }
        string Area = "0";
        if (rblArea1.Checked == true)
        {
            Area = "1";
        }
        if (rblArea2.Checked == true)
        {
            Area = "2";
        }

        string formbayUnitNo = txtformbayUnitNo.Text;
        string formbayunittype = ddlformbayunittype.SelectedValue;
        string formbayStreetNo = txtformbayStreetNo.Text;
        string formbaystreetname = txtformbaystreetname.Text;
        string formbaystreettype = ddlformbaystreettype.SelectedValue;
        string StreetCity = ddlStreetCity.Text;
        string StreetState = txtStreetState.Text;
        string StreetPostCode = txtStreetPostCode.Text;
        string StreetAddress = formbayunittype + " " + formbayUnitNo + " " + formbayStreetNo + " " + formbaystreetname + " " + formbaystreettype;
        //txtStreetAddress.Text;

        DataTable dtCont = ClstblContacts.tblContacts_SelectTop(hndEditCustID.Value);
        string id = hndEditCustID.Value;
        if (dtCont.Rows.Count > 0)
        {
            // int existaddress = ClstblCustomers.tblCustomers_Exits_Address(formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, StreetCity, StreetState, StreetPostCode);
            DataTable dtaddress = ClstblCustomers.tblCustomers_Exits_Select_StreetAddressByID(id, StreetAddress, StreetCity, StreetState, StreetPostCode);
            //DataTable dtMobileEmailExist = ClstblCustomers.tblContacts_Exits_Select_MobileEmailByID(id, txtContMobile.Text.Trim(), txtContEmail.Text);
            //DataTable dtMobileEmailExist = ClstblCustomers.tblContacts_Exits_Select_MobileEmailByID2(id, txtContMobile.Text.Trim(), txtContEmail.Text);
            string mobiletxt = txtContMobile.Text.Trim();
            string emailtxt = txtContEmail.Text.Trim();
            if (dtaddress.Rows.Count == 0)
            {
                int dtMobileEmailExist = 0;
                int flag1 = 0;
                int flag2 = 0;
                string email = dtCont.Rows[0]["ContEmail"].ToString().Trim();
                string mobile = dtCont.Rows[0]["ContMobile"].ToString();
                if (mobiletxt != mobile)
                {
                    if(mobiletxt != "0400000000") 
                    flag1 = 1;
                }
                if(emailtxt != email)
                {
                    if (emailtxt != "no-reply@arisesolar.com.au")
                    flag2 = 1;
                }
                if(flag1 == 1 || flag2==1)
                {
                    if (flag1 == 0)
                    { mobiletxt = "0400000000"; }
                    if (flag2 == 0)
                    { emailtxt = "no-reply@arisesolar.com.au"; }

                    dtMobileEmailExist = ClstblCustomers.tblContacts_Exits_Select_MobileEmailByID2(id, mobiletxt, emailtxt);               
                }
                if (dtMobileEmailExist== 0)
                {
                    //Response.Write(existaddress);
                    // Response.End();
                    lbleror.Visible = false;
                    lbleror1.Visible = false;
                    string ContactID = dtCont.Rows[0]["ContactID"].ToString();
                    bool succ = ClstblCustomers.tblCustomer_Update_Address(hndEditCustID.Value, ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value);
                    bool suc = ClstblContacts.tblContacts_UpdateCustDetail(ContactID, txtContMr.Text, txtContFirst.Text, txtContLast.Text, txtContMobile.Text.Trim(), txtContEmail.Text);
                    bool add = ClstblCustomers.tblCustomers_UpdateAddress(hndEditCustID.Value, StreetAddress, ddlStreetCity.Text, txtStreetState.Text, txtStreetPostCode.Text, txtContFirst.Text + " " + txtContLast.Text);
                    bool Suc1 = ClstblCustomers.tblCustomers_UpdateInContact(hndEditCustID.Value, ResCom, txtCustPhone.Text, Area);
                    if (suc && succ && add && Suc1)
                    {
                        ModalPopupExtenderEdit.Hide();
                        openModal = "false";
                        BindGrid(0);
                    }
                }
                else
                {
                    lbleror1.Visible = true;
                    openModal = "true";
                    ModalPopupExtenderEdit.Show();
                }
            }
            else
            {
                lbleror.Visible = true;
                openModal = "true";
                ModalPopupExtenderEdit.Show();
            }
        }
    }
    protected void ddlStreetCity_TextChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        if (ddlStreetCity.Text != string.Empty)
        {
            DataTable dtStreet = ClstblCustomers.tblPostCodes_SelectBy_PostCodeID(ddlStreetCity.Text);
            if (dtStreet.Rows.Count > 0)
            {
                txtStreetState.Text = dtStreet.Rows[0]["State"].ToString();
                txtStreetPostCode.Text = dtStreet.Rows[0]["PostCode"].ToString();
            }
            streetaddress();
            string[] cityarr = ddlStreetCity.Text.Split('|');
            if (cityarr.Length > 1)
            {
                ddlStreetCity.Text = cityarr[0].Trim();
                txtStreetState.Text = cityarr[1].Trim();
                txtStreetPostCode.Text = cityarr[2].Trim();
            }
            //checkExistsAddress();
        }
        BindScript();
    }



    protected void GridView1_DataBound(object sender, EventArgs e)
    {

        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }
    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    #region pagination
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    Response.Write(lnkpagebtn.);
            //    //lnkpagebtn.Style.Add("class", "pagebtndesign nonepointer");
            //}
        }
    }
    public void PageClick(String id)
    {

        custompageIndex = Convert.ToInt32(id);
        BindGrid(0);

    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());

    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        //lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;



        //if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //{
        //    lastpageindex = lastpageindex + 1;
        //}
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    #endregion

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlSalesRepSearch.SelectedValue = "";
        ddlSearchType.SelectedValue = "1";
        ddlSearchSource.SelectedValue = "";
        ddlSearchStatus.SelectedValue = "2";
        ddlSearchSubSource.SelectedValue = "";
        ddlSearchState.SelectedValue = "";
        ddlTeam.SelectedValue = "";
        ddlSelectDate.SelectedValue = "";
        txtSearchPostCode.Text = string.Empty;
        txtsuburb.Text = string.Empty;
        txtContactSearch.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlRejectOrNot.SelectedValue = "";
        txtSearchStreet.Text = string.Empty;

        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        BindGrid(0);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }



    protected void txtformbayUnitNo_TextChanged(object sender, EventArgs e)
    {

        ModalPopupExtenderEdit.Show();
        openModal = "true";
        streetaddress();
        //checkExistsAddress();
        BindScript();
        txtformbayUnitNo.Focus();
    }


    protected void ddlformbayunittype_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        streetaddress();
        //checkExistsAddress();
        BindScript();
        ddlformbayunittype.Focus();

    }
    protected void txtformbayStreetNo_TextChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        streetaddress();
        //checkExistsAddress();
        BindScript();
        txtformbayStreetNo.Focus();
    }
    protected void txtformbaystreetname_TextChanged(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "address();", true);
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        // Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>doMyAction();</script>");

        streetaddress();
        //checkExistsAddress();

        BindScript();

        txtformbaystreetname.Focus();


        // ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction1", "funddlformbaystreettypefocus();", true);


    }
    protected void ddlformbaystreettype_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        streetaddress();
        //checkExistsAddress();
        BindScript();
        ddlformbaystreettype.Focus();
    }

    public void streetaddress()
    {
        address = ddlformbayunittype.SelectedValue + " " + txtformbayUnitNo.Text + " " + txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue;
        txtStreetAddress.Text = address;
        openModal = "true";
        BindScript();

    }

    public void checkExistsAddress()
    {
        ModalPopupExtenderEdit.Show();
        openModal = "true";
        string StreetCity = ddlStreetCity.Text;
        string StreetState = txtStreetState.Text;
        string StreetPostCode = txtStreetPostCode.Text;
        string StreetAddress = txtStreetAddress.Text;
        int exist = ClstblCustomers.tblCustomers_Exits_Address(hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, StreetCity, StreetState, StreetPostCode);
        DataTable dt1 = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress(StreetAddress, StreetCity, StreetState, StreetPostCode);

        if (dt1.Rows.Count > 0)
        {
            lbleror.Visible = true;
        }
        else
        {
            lbleror.Visible = false;
        }
    }
    protected void btnCancelEdit_Click(object sender, EventArgs e)
    {
        lbleror.Visible = false;
        lbleror1.Visible = false;
    }
    protected void ddlManager_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalPopupExtender2.Show();
        if (ddlManager.SelectedValue == "1")
        {
            divNextDate.Visible = true;
            //divDescription.Attributes["class"] = "form-group col-md-8";
        }
        else
        {
            divNextDate.Visible = false;
           // divDescription.Attributes["class"] = "form-group col-md-12";
        }
        BindScript();
    }

    protected void ddlSearchSource_SelectedIndexChanged1(object sender, EventArgs e)
    {

    }

    protected void lstSearchStatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //RepeaterItem item = e.Item;
        //HiddenField hdnID = (HiddenField)item.FindControl("hdnID");
        //Literal ltprojstatus = (Literal)item.FindControl("ltprojstatus");
        //CheckBox chkselect = (CheckBox)item.FindControl("chkselect");

        //if (Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("Purchase Manager") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Verification") || Roles.IsUserInRole("WarehouseManager") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("Maintenance"))
        //{
        //    if (hdnID.Value == "2")
        //    {
        //        e.Item.Visible = false;
        //    }
        //}

    }
}