<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    Culture="en-GB" UICulture="en-GB" AutoEventWireup="true" CodeFile="installbookingtracker.aspx.cs"
    Inherits="admin_adminfiles_company_installbookingtracker" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/includes/controls/projectpreinst.ascx" TagPrefix="uc1" TagName="projectpreinst" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <script language="javascript" type="text/javascript">
        // defult enter
        function DefaultEnter(evnt) {

            var btn = $get('#<%=btnSearch.ClientID %>');

            alert(btn.text);
            var x1 = btn.id;
            if (evnt.keyCode == Sys.UI.Key.enter) {
                //__doPostBack('" + btn.id + "', '');
                location = document.getElementById('<%=btnSearch.ClientID %>').href;
                return false;
            }
            return true;
        }
    </script>

    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }
    </script>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>

    <asp:Panel runat="server" ID="PanelCreateEdit" DefaultButton="btnSearch">
        <asp:UpdatePanel ID="updatepanelgrid" runat="server">
            <ContentTemplate>


                <div class="page-body headertopbox">
                    <h5 class="row-title"><i class="typcn typcn-th-small"></i>Install Booking Tracker</h5>
                    <div id="tdExport" class="pull-right" runat="server">
                        <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">
                            <%-- <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                            CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>--%>
                        </ol>
                    </div>
                </div>





                <div class="page-body padtopzero">
                    <asp:Panel runat="server" ID="PanGridSearch">
                        <div class="animate-panel" style="padding-bottom: 0px!important;">
                            <div class="messesgarea">

                                <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                </div>
                            </div>
                        </div>


                        <div class="searchfinal" onkeypress="DefaultEnter(event);">
                            <div class="widget-body shadownone brdrgray">
                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                    <div class="dataTables_filter">

                                        <div class="dataTables_filter  Responsive-search">
                                            <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <div class="inlineblock">

                                                            <div class="input-group col-sm-1 martop5">
                                                                <asp:TextBox ID="txtClient" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender11" runat="server" TargetControlID="txtClient"
                                                                    WatermarkText="Client" />
                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender7" MinimumPrefixLength="2" runat="server"
                                                                    UseContextKey="true" TargetControlID="txtClient" ServicePath="~/Search.asmx"
                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCompanyList"
                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                            </div>
                                                            <div class="input-group col-sm-1 martop5">
                                                                <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtProjectNumber"
                                                                    WatermarkText="Project Num" />
                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                                    UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                    ControlToValidate="txtProjectNumber" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                            </div>
                                                            <div class="input-group col-sm-1 martop5">
                                                                <asp:TextBox ID="txtMQNsearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender8" runat="server" TargetControlID="txtMQNsearch"
                                                                    WatermarkText="Manual Num" />
                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                                    UseContextKey="true" TargetControlID="txtMQNsearch" ServicePath="~/Search.asmx"
                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetManualQuoteNumber"
                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                            </div>
                                                            <div class="input-group col-sm-1 martop5">
                                                                <asp:TextBox ID="txtSearchStreet" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearchStreet"
                                                                    WatermarkText="Street" />
                                                            </div>
                                                            <div class="input-group col-sm-1 martop5">
                                                                <asp:TextBox ID="txtSerachCity" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender10" runat="server" TargetControlID="txtSerachCity"
                                                                    WatermarkText="City" />
                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                    UseContextKey="true" TargetControlID="txtSerachCity" ServicePath="~/Search.asmx"
                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                </cc1:AutoCompleteExtender>
                                                            </div>
                                                            <div class="input-group col-sm-1 martop5" id="divCustomer" runat="server">
                                                                <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="">State</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="input-group groupalign col-sm-1 martop5">
                                                                <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" Text="Post Code :" Enabled="false"></asp:TextBox>
                                                            </div>
                                                            <div class="input-group col-md-1 martop5" style="width: 148px">

                                                                <asp:TextBox ID="txtSearchPostCodeFrom" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender13" runat="server" TargetControlID="txtSearchPostCodeFrom"
                                                                    WatermarkText="From" />
                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                    UseContextKey="true" TargetControlID="txtSearchPostCodeFrom" ServicePath="~/Search.asmx"
                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                </cc1:AutoCompleteExtender>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="search"
                                                                    ControlToValidate="txtSearchPostCodeFrom" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                            </div>
                                                            <div class="input-group col-md-1 martop5">
                                                                <asp:TextBox ID="txtSearchPostCodeTo" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender14" runat="server" TargetControlID="txtSearchPostCodeTo"
                                                                    WatermarkText="To" />
                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                                    UseContextKey="true" TargetControlID="txtSearchPostCodeTo" ServicePath="~/Search.asmx"
                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                </cc1:AutoCompleteExtender>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationGroup="search"
                                                                    ControlToValidate="txtSearchPostCodeTo" Display="Dynamic" ErrorMessage="Enter valid value"
                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                            </div>
                                                            <div class="input-group col-sm-2 martop5" style="width: 215px" id="div1" runat="server">
                                                                <asp:DropDownList ID="ddlSearchStatus" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="">Project Status</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="input-group col-sm-1 martop5" style="width: 120px" id="div2" runat="server">
                                                                <asp:DropDownList ID="ddlProjectTypeID" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="" Text="Project Type"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="input-group col-sm-1 martop5" style="width: 140px;">
                                                                <asp:TextBox ID="txtPModel" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtPModel"
                                                                    WatermarkText="Panel Model" />
                                                            </div>
                                                            <div class="input-group col-sm-1 martop5" style="width: 145px;">
                                                                <asp:TextBox ID="txtIModel" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtIModel"
                                                                    WatermarkText="Inverter Model" />
                                                            </div>
                                                            <div class="input-group groupalign col-md-1 martop5">
                                                                <asp:TextBox ID="TextBox2" runat="server" CssClass="form-control" Text="System Capacity:" Enabled="false"></asp:TextBox>
                                                            </div>
                                                            <div class="input-group col-md-1 martop5" style="width: 148px">
                                                                <asp:TextBox ID="txtSysCapacityFrom" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender12" runat="server" TargetControlID="txtSysCapacityFrom"
                                                                    WatermarkText="From" />
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationGroup="search"
                                                                    ControlToValidate="txtSysCapacityFrom" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                    ValidationExpression="^\d*\.?\d+$"> </asp:RegularExpressionValidator>
                                                            </div>
                                                            <div class="input-group col-md-2 martop5" style="width: 148px">
                                                                <asp:TextBox ID="txtSysCapacityTo" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender15" runat="server" TargetControlID="txtSysCapacityTo"
                                                                    WatermarkText="To" />
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ValidationGroup="search"
                                                                    ControlToValidate="txtSysCapacityTo" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                    ValidationExpression="^\d*\.?\d+$"> </asp:RegularExpressionValidator>
                                                            </div>
                                                            <div class="input-group col-sm-1 martop5" style="width: 172px" id="div3" runat="server">
                                                                <asp:DropDownList ID="ddlFinanceOption" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="">FinanceOption</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="input-group col-sm-1 martop5" style="width:150px">
                                                                <asp:TextBox ID="txtpurchaseNo" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtpurchaseNo"
                                                                    WatermarkText="Purchase Num" />
                                                            </div>

                                                            <div class="input-group col-sm-1 martop5" runat="server">
                                                                <asp:DropDownList ID="ddlRebateStatus" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="">Rebate Status</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>

                                                            <div class="input-group col-sm-1 martop5" style="width:166px">
                                                                <asp:TextBox ID="txtelecdist" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtelecdist"
                                                                    WatermarkText="ElecDist Approved No." />
                                                            </div>
                                                            <div class="input-group col-sm-1 martop5" id="div4" runat="server">
                                                                <asp:DropDownList ID="ddlElecDistAppDate" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="">Dist Approved Date</asp:ListItem>
                                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="2">No</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="input-group col-sm-1 martop5" style="width: 120px" id="div6" runat="server">
                                                                <asp:DropDownList ID="ddlhousetype" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="">Select House Type</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="input-group col-sm-1 martop5" style="width: 120px" id="div8" runat="server">
                                                                <asp:DropDownList ID="ddlrooftype" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                    <asp:ListItem Value="">Select Roof Type</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="input-group col-sm-1 martop5" id="div9" runat="server">
                                                                <asp:DropDownList ID="ddlhistoric" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                   <asp:ListItem Value="3">All</asp:ListItem>
                                                                    <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                    <asp:ListItem Value="2">No</asp:ListItem>
                                                                    <%-- <asp:ListItem Value="3">InstallBooking Date</asp:ListItem>--%>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="input-group col-sm-1 martop5" id="div5" runat="server">
                                                                <asp:DropDownList ID="ddldatetype" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                   <asp:ListItem Value="">Select Date</asp:ListItem>
                                                                    <asp:ListItem Value="1">Dep Date</asp:ListItem>
                                                                    <asp:ListItem Value="2">Active Date</asp:ListItem>
                                                                     <asp:ListItem Value="3">InstallBooking Date</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div class="input-group date datetimepicker1 col-sm-1 martop5">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                    ControlToValidate="txtStartDate" ValidationGroup="search" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                            </div>
                                                            <div class="input-group date datetimepicker1 col-sm-1 martop5">
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar"></span>
                                                                </span>
                                                                <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic" ValidationGroup="search"
                                                                    ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                                <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                    ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                    Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                            </div>
                                                            <div class="form-group checkbox-info paddtop3td alignchkbox btnviewallorange martop5" style="display:none">

                                                                <label for="<%=chkishistoric.ClientID %>" class="btn btn-magenta">
                                                                    <asp:CheckBox ID="chkishistoric" runat="server" />
                                                                    <span class="text">Historic</span>
                                                                </label>

                                                            </div>
                                                            <div class="input-group martop5">
                                                                <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                            </div>
                                                            <div class="form-group martop5">
                                                                <asp:LinkButton ID="btnClearAll" runat="server"
                                                                    CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn-primary btn"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                            </div>
                                                        </div>

                                                    </td>
                                                </tr>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                                <div class="datashowbox">
                                    <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                        <div class="row">
                                            <div class="dataTables_length showdata col-sm-6">
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="padtopzero">
                                                            <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                aria-controls="DataTables_Table_0" class="myval">
                                                                <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-md-6">
                                                <div id="Div7" class="pull-right" runat="server">
                                                    <asp:LinkButton ID="lbtnExport" runat="server" CausesValidation="false" class="btn btn-success btn-xs Excel" data-original-title="Excel Export" data-placement="left" data-toggle="tooltip" OnClick="lbtnExport_Click" title=""><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                    <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                                 <%--   <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                        CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>--%>
                                                    <%-- </ol>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <asp:HiddenField runat="server" ID="hdnInvoiceProjectid" />
                        <asp:Button ID="btnNULLData1" Style="display: none;" runat="server" />
                        <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                            CancelControlID="ibtnCancelStatus" DropShadow="false" PopupControlID="div_popup" TargetControlID="btnNULLData1">
                        </cc1:ModalPopupExtender>
                        <div runat="server" id="div_popup" class="modal_popup" style="display: none; width: auto">
                            <div class="modal-dialog modal-lg" style="width: 1100px">
                                <div class="modal-content">
                                    <div class="color-line"></div>

                                    <div class="modal-header text-center">
                                        <button id="ibtnCancelStatus" runat="server" type="button" class="close width40" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <h4 class="modal-title" id="H1">
                                            <asp:Label runat="server" ID="lbltitle" Text="Pre Inst"></asp:Label>
                                        </h4>
                                    </div>
                                    <div class="modal-body paddnone">
                                        <div class="panel-body" style="height: 500px; overflow-y: auto">
                                            <div class="tablescrolldiv">
                                                <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                                    <uc1:projectpreinst runat="server" ID="projectpreinst" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <asp:HiddenField runat="server" ID="hdn_custpopup" />
                    </asp:Panel>

                    <asp:Panel ID="panel" runat="server">
                        <div class="searchfinal">
                            <div class="widget-body shadownone brdrgray">
                                <div class="printorder">
                                    <div>
                                        <b>Total Number of panels:&nbsp;</b>
                                        <asp:Literal ID="lblTotalPanels" runat="server"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>

                    <div class="content padtopzero marbtm50 finalgrid" id="divgrid" runat="server">
                        <div id="leftgrid" runat="server">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div id="PanGrid" runat="server" visible="false">
                                    <div class="table-responsive xscroll  noPagination">
                                        <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                            OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound"
                                            OnRowCreated="GridView1_RowCreated1" OnRowCommand="GridView1_RowCommand" AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                            <Columns>
                                                <asp:TemplateField ItemStyle-Width="20px">
                                                    <ItemTemplate>
                                                        <asp:HiddenField ID="hdnempid" runat="server" Value='<%# Eval("ProjectID") %>' />
                                                        <a href="JavaScript:divexpandcollapse('div<%# Eval("ProjectID") %>','tr<%# Eval("ProjectID") %>');">
                                                            <img id='imgdiv<%# Eval("ProjectID") %>' src="../../../images/icon_plus.png" />
                                                        </a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Project#" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center" SortExpression="InstallPostCode">
                                                    <ItemTemplate>

                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                            NavigateUrl='<%# "~/admin/adminfiles/company/SalesInfo.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink>
                                                        <%-- <asp:Label ID="lblProjectNumber" runat="server" Width="70px"><%#Eval("ProjectNumber")%></asp:Label>--%>
                                                        <asp:HiddenField Value='<%#Eval("ProjectID") %>' runat="server" ID="hdnProjectID" />
                                                        <asp:HiddenField Value='<%#Eval("CustomerID") %>' runat="server" ID="hdncust" />
                                                        <asp:LinkButton ID="lnkjob" Visible="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Job Book" OnClick="lnkjob_Click" CssClass="btn btn-info btn-xs"><i class="fa fa-edit"></i></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="P/CD" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="Center" SortExpression="InstallPostCode">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProject15" runat="server" Width="40px"><%#Eval("InstallPostCode")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="Customername">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProject3" runat="server" Width="150px"><%#Eval("Customer")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="ProjectStatus"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPhone" runat="server" Width="80px"><%#Eval("ProjectStatus")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Mobile" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="ContMobile"
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProject4" runat="server" Width="80px"><%#Eval("ContMobile")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="System Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" SortExpression="SystemDetails"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSystemDetails" runat="server" data-toggle="tooltip" Width="80px" data-placement="top" data-original-title='<%#Eval("SystemDetails")%>'><%#Eval("SystemDetails")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Project Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="ProjectNotes"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProject7" runat="server" data-toggle="tooltip" Width="80px" data-placement="top" data-original-title='<%#Eval("ProjectNotes")%>'><%#Eval("ProjectNotes")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Install Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="InstallerNotes"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInstallNotes" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("InstallerNotes")%>'
                                                            Width="80px"><%#Eval("InstallerNotes")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="House Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="HouseType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProject13" runat="server" Width="100px"><%#Eval("HouseType")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Roof Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="RoofType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblProjectRoofType" runat="server" Width="80px"><%#Eval("RoofType")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%-- <asp:TemplateField HeaderText="Next Followup Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="FollowUp"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblnxtfollowupdate" runat="server" Width="80px" Text=<%#Eval("FollowUp","{0:dd-MMM-yyyy}")%>> </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Description" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="OtherDescription"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbldescription" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("OtherDescription")%>'
                                                            Width="80px"><%#Eval("OtherDescription")%></asp:Label>
                                                    </ItemTemplate
                                                </asp:TemplateField>>--%>


                                                <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                    <ItemTemplate>
                                                        <tr id='tr<%# Eval("ProjectID") %>' style="display: none;" class="dataTable left-text">
                                                            <td colspan="98%" class="details">
                                                                <div id='div<%# Eval("ProjectID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                    <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                        <tr>
                                                                            <td>
                                                                                <b>Project</b>
                                                                            </td>
                                                                            <td colspan="5"><%#Eval("Project")%></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td width="180px"><b>Employee Name</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblProject18" runat="server" Width="150px"><%#Eval("Employee")%></asp:Label>
                                                                            </td>
                                                                            <td width="180px"><b>State</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblInstallState" runat="server" Width="60px"><%#Eval("InstallState")%></asp:Label>
                                                                            </td>

                                                                            <td width="180px"><b>Phone</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblProjectStatus" runat="server" Width="100px"><%#Eval("CustPhone")%></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                      
                                                                        <tr>
                                                                            <td><b>Man#</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblProject10" runat="server" Width="40px"><%#Eval("ManualQuoteNumber")%></asp:Label>
                                                                            </td>
                                                                            <td runat="server" id="thtype"><b>Sys</b>
                                                                            </td>
                                                                            <td runat="server" id="tdtype">
                                                                                <asp:Label ID="lblProject11" runat="server" Width="25px"><%#Eval("SystemCapKW")%></asp:Label>
                                                                            </td>

                                                                            <td><b>Price</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblTotalQuotePrice" runat="server" Width="70px"><%# Eval("TotalQuotePrice").ToString() == "" ? "" : ""+Math.Round(Convert.ToDecimal(Eval("TotalQuotePrice")), 2) %></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                           
                                                                            <td><b>Install Book Date</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblProject14" runat="server" Width="96px"><%# DataBinder.Eval(Container.DataItem, "InstallBookingDate", "{0:dd MMM yyyy}")%></asp:Label>
                                                                            </td>
                                                                            <td><b>Install Complete Date</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblComplete" runat="server" Width="96px"><%# DataBinder.Eval(Container.DataItem, "InstallCompleted", "{0:dd MMM yyyy}")%></asp:Label>
                                                                            </td>
                                                                            <td><b>Dep Rec</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblProject22" runat="server" Width="80px"><%# DataBinder.Eval(Container.DataItem, "DepositReceived", "{0:dd MMM yyyy}")%></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b>E-Mail</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="LblEmail" runat="server" Width="40px"><%#Eval("ContEmail")%></asp:Label>
                                                                            </td>
                                                                            <td><b>FinanceWith</b>
                                                                            </td>
                                                                            <td >
                                                                                <asp:Label ID="Label2" runat="server" Width="25px"><%#Eval("FinanceWith")%></asp:Label>
                                                                            </td>
                                                                            <td><b>Installer Name</b>
                                                                            </td>
                                                                            <td >
                                                                                <asp:Label ID="lblInstallerName" runat="server" Width="25px"><%#Eval("InstallerName")%></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                          
                                            </Columns>
                                            <AlternatingRowStyle />

                                            <PagerTemplate>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                <div class="pagination">
                                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                </div>
                                            </PagerTemplate>
                                            <PagerStyle CssClass="paginationGrid" />
                                            <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                        </asp:GridView>
                                    </div>
                                    <div class="paginationnew1" runat="server" id="divnopage">
                                        <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnSearch" />
                    <asp:PostBackTrigger ControlID="lbtnExport" />
                <%-- <asp:PostBackTrigger ControlID="GridView1" />--%>
                <%--   <asp:AsyncPostBackTrigger ControlID="GridView1" />--%>
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
        }

        function pageLoadedpro() {
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.loading-container').css('display', 'none');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

            $("[data-toggle=tooltip]").tooltip();

        }
    </script>
</asp:Content>

