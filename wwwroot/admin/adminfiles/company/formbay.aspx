<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" 
    CodeFile="formbay.aspx.cs" Inherits="admin_adminfiles_company_formbay" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<style type="text/css">
        .selected_row {
            background-color: #A1DCF2!important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>
     <div class="page-body headertopbox">
              <h5 class="row-title"><i class="typcn typcn-th-small"></i>FormBay</h5>
              <div id="hbreadcrumb" class="pull-right">
                               
                            </div>
               </div>
    <div class="page-body padtopzero">
      <div class="messesgarea">
                    <div class="alert alert-info" id="PanNoRecord" runat="server">
                        <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                    </div>
                </div>
        
        <div class="searchfinal">
                     <div class="widget-body shadownone brdrgray"> 
                     <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                     	<div class="dataTables_filter row">
                            <div class="col-md-6">
                            <table border="0" cellspacing="0" width="100%" style="text-align: right;" cellpadding="0">
                                                        <tr>
                                                            <td class="left-text dataTables_length showdata padtopzero"> <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                aria-controls="DataTables_Table_0" class="myval">
                                           <asp:ListItem Value="25">Show entries</asp:ListItem>
                                  </asp:DropDownList>
                                                            </td>
                                </tr>
                            </table>
                                </div>
                            <div class="col-md-6">
                            <div class="pull-right">
                                <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td id="tdExport" runat="server" width="20px" style="padding-right: 7px; vertical-align: bottom;">
                                                    <asp:LinkButton ID="lbtnExport" ForeColor="brown" Font-Underline="false" runat="server" CausesValidation="false"
                                                        OnClick="lbtnExport_Click" class="btn btn-success btn-xs Excel" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                </td>
                                            </tr>
                                        </table>
                                
                                </div>
                            </div>
                         </div>
                         </div>
            </div>
        </div>
          <div class="finalgrid">
    <div class="table-responsive">
                    <div class="marginnone">
                        <div class="tableblack">
                            <div class="table-responsive" id="PanGrid" runat="server">
                                <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" ShowFooter="false"
                                    OnPageIndexChanging="GridView1_PageIndexChanging" AllowSorting="true" AllowPaging="true"
                                    OnDataBound="GridView1_DataBound" OnRowCreated="GridView1_RowCreated" CssClass="table table-bordered table-striped"
                                     OnRowCommand="GridView1_RowCommand">
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center">
                                            <HeaderTemplate>
                                                &nbsp;<asp:CheckBox ID="chkAssignAll"  runat="server" OnCheckedChanged="chkAssignAll_CheckedChanged" AutoPostBack="true" />
                                                <label for='<%# ((GridViewRow)Container).FindControl("chkAssignAll").ClientID %>' runat="server" id="lblchk">
                                                    <span></span>
                                                </label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkAssign" runat="server"  />
                                                <label for='<%# ((GridViewRow)Container).FindControl("chkAssign").ClientID %>'>
                                                    <span></span>
                                                </label>
                                            </ItemTemplate>
                                            <ItemStyle Width="40px" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Project #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="90px">
                                            <ItemTemplate>
                                                <%#Eval("ProjectNumber")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Project" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hndProjectID" runat="server" Value='<%# Eval("ProjectID") %>' />
                                                <%#Eval("Project")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Install Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px">
                                            <ItemTemplate>
                                                <%#Eval("InstallBookingDate","{0:dd MMM yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>                                   
                                  <PagerTemplate>
                                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                            <div class="pagination">
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                            </div>
                                                        </PagerTemplate>
                                                        <PagerStyle CssClass="paginationGrid" />
                                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                    </asp:GridView>
                                                </div>
                                                <div class="paginationnew1" runat="server" id="divnopage">
                                                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
   
    
   
</asp:Content>

