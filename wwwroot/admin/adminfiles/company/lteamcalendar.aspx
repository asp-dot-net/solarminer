<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" Theme="admin" CodeFile="lteamcalendar.aspx.cs" Culture="en-GB"
    Inherits="admin_adminfiles_company_lteamcalendar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

            doMyAction();
        });
        function doMyAction() {
            HighlightControlToValidate();
            $('#<%=ibtnAdd.ClientID %>').click(function () {
                formValidate();
            });
            $('#<%=btnAttend.ClientID %>').click(function () {
                formValidate();
            });
        }
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>

    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
        }

        function pageLoadedpro() {

            $('.loading-container').css('display', 'none');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();

        }
    </script>
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Team Calendar</h5>
    </div>
    <div class="page-body padtopzero">
        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
            <ContentTemplate>

                <section>
                    <div class=" animate-panel padbtmzero ">
                        <%--<div>
                        <div >--%>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="hpanel marbtmzero">

                                    <div class="searchfinal">
                                        <div class="widget-body shadownone brdrgray">
                                            <div class="form-horizontal">
                                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                    <div class="dataTables_filter Responsive-search printorder">
                                                        <div class="row">
                                                            <div class="col-md-3">
                                                                <div style="text-align: left; margin-top: 0px;">
                                                                    <div class="btnmarzero">
                                                                        <span class="arrowbutton">
                                                                            <asp:LinkButton ID="btnpreviousweek" runat="server" Text="Previous Week" OnClick="btnpreviousweek_Click" CssClass="fc-prev-button fc-button fc-state-default fc-corner-left btnarrowbox"><i class="fa fa-angle-left"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="btnnextweek" runat="server" Text="Next Week" OnClick="btnnextweek_Click" CssClass="fc-next-button fc-button fc-state-default fc-corner-right btnarrowbox">   <i class="fa fa-angle-right"></i></asp:LinkButton>

                                                                        </span>

                                                                        <asp:Button ID="btntoday" runat="server" CssClass="btn btn-dark" Text="Today" OnClick="btntoday_Click" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div style="text-align: center;">
                                                                    <div class="form-group">
                                                                        <h5>
                                                                            <b>
                                                                                <asp:Literal ID="ltdate" runat="server"></asp:Literal></b></h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <%-- </div>
                    </div>--%>
                    </div>

                </section>

                <div class=" padleftzero padrightzero">
                    <div class="panel panel-default teamcalendar">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#BDC3C8" class="table">
                            <tbody>
                                <tr>
                                    <asp:Repeater ID="RptDays" runat="server" OnItemDataBound="RptDays_ItemDataBound">
                                        <ItemTemplate>
                                            <td valign="top">
                                                <table width="100%" cellpadding="0" cellspacing="0" class="table table-striped table-bordered teamcaltable">
                                                    <thead>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:HiddenField ID="hdnDate" runat="server" Value='<%# Eval("CDate")%>' />
                                                                <%# Eval("CDate")%>
                                                                <%# Eval("CDay") %>
                                                            </td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr class="title">
                                                            <td width="50%"><b>Customer</b>
                                                            </td>
                                                            <td width="25%" align="center"><b>Time</b>
                                                            </td>
                                                        </tr>
                                                        <asp:Repeater ID="rptinstaller1" runat="server" OnItemCommand="rptinstaller1_ItemCommand" OnItemDataBound="rptinstaller1_ItemDataBound">
                                                            <ItemTemplate>
                                                                <tr class="brdergray">
                                                                    <td valign="top" style="width: 50px">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <asp:HiddenField ID="hndCustID" runat="server" Value='<%#Eval("CustomerID")%>' />
                                                                                    <b><%#Eval("Customer")%></b>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <%# Eval("StreetAddress")%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <%# Eval("StreetCity")%> , <%# Eval("StreetState")%> - <%# Eval("StreetPostCode")%>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <asp:HiddenField ID="hndd2daapdate" runat="server" Value='<%# Eval("D2DAppDate")%>' />
                                                                                    <asp:Literal ID="l1" runat="server" Text="Confirmed :" Visible='<%#Eval("appointment_status").ToString()=="0"?true:false %>'></asp:Literal>
                                                                                    <asp:LinkButton ID="lbtnConfirmed" runat="server" CommandArgument='<%# Eval("CustomerID") %>'
                                                                                        CommandName="Confirmed" Visible='<%#Eval("appointment_status").ToString()=="0"?true:false %>'
                                                                                        OnClientClick="return confirm('Are you sure want to Fix Appointment?');">Yes /</asp:LinkButton>
                                                                                    <asp:LinkButton ID="lbtnNotConfirmed" runat="server" CommandName="NotConfirmed" CommandArgument='<%# Eval("CustomerID") %>'
                                                                                        Visible='<%#Eval("appointment_status").ToString()=="0"?true:false %>'>No</asp:LinkButton>
                                                                                    <b>
                                                                                        <asp:LinkButton ID="lbtnStatus" runat="server" CommandArgument='<%# Eval("CustomerID") %>' CommandName="chngstatus"
                                                                                            Visible='<%#Eval("appointment_status").ToString()=="1" && Eval("customer_lead_status").ToString()=="0"? true:false %>'>
                                                                                                    Confirmed</asp:LinkButton></b>
                                                                                    <asp:LinkButton ID="lbtnAssignSalesRep" runat="server" CommandArgument='<%# Eval("CustomerID") %>' CommandName="salesrep"
                                                                                        Visible='<%#Eval("appointment_status").ToString()=="1" && Eval("customer_lead_status").ToString()=="0"? true:false %>'>
                                                                                                    Assign Sales Rep</asp:LinkButton>
                                                                                    <b>
                                                                                        <asp:LinkButton ID="lbtnAttended" runat="server" CommandArgument='<%# Eval("CustomerID") %>' CommandName="attend"
                                                                                            Visible='<%#Eval("appointment_status").ToString()=="1" && Eval("customer_lead_status").ToString()=="0"? true:false %>'>
                                                                                                    Attended</asp:LinkButton></b>
                                                                                    <b>
                                                                                        <asp:LinkButton ID="lbtnQuatation" runat="server" CommandArgument='<%# Eval("CustomerID") %>' CommandName="quatation"
                                                                                            Visible='<%#Eval("appointment_status").ToString()=="1" && Eval("customer_lead_status").ToString()=="1"? true:false %>'>
                                                                                                Quatation</asp:LinkButton>
                                                                                    </b>
                                                                                    <b>
                                                                                        <asp:LinkButton ID="lbtnFollowUps" runat="server" CommandArgument='<%# Eval("CustomerID") %>' CommandName="followups"
                                                                                            Visible='<%#Eval("appointment_status").ToString()=="1" && Eval("customer_lead_status").ToString()=="4"? true:false %>'>
                                                                                                Follow Ups</asp:LinkButton>
                                                                                    </b>
                                                                                    <asp:Literal ID="Literal1" runat="server" Text="Lead Status:" Visible='<%#Eval("customer_lead_status").ToString()=="0"?false:true%>'></asp:Literal>
                                                                                    <asp:Literal ID="leadstatus" runat="server" Text='<%# Eval("customerleadstatus") %>' Visible='<%#Eval("customer_lead_status").ToString()=="0"?false:true%>'></asp:Literal>
                                                                                    <asp:LinkButton ID="lbtnReschYes" runat="server" CommandArgument='<%# Eval("CustomerID") %>'
                                                                                        CommandName="RescheduleYes" Visible='<%#Eval("customer_lead_status").ToString()=="3"?true:false %>'
                                                                                        OnClientClick="return confirm('Are you sure want to Reschedule?');">Yes /</asp:LinkButton>
                                                                                    <asp:LinkButton ID="lbtnReschNo" runat="server" CommandArgument='<%# Eval("CustomerID") %>' CommandName="RescheduleNo"
                                                                                        Visible='<%#Eval("customer_lead_status").ToString()=="3"?true:false %>'>No</asp:LinkButton>
                                                                                    <asp:LinkButton ID="lbtnReschNotes" runat="server" CommandArgument='<%# Eval("CustomerID") %>' CommandName="ReschNotes"
                                                                                        Visible='<%#Eval("customer_lead_status").ToString()=="3"?true:false %>'>Notes</asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td class="valignbtm">
                                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            <tr>
                                                                                <td><b><%# Eval("D2DAppDate","{0:dd/MM/yyyy}")%></b></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><b><%# Eval("D2DAppTime")%></b></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><b><%# Eval("CustPhone")%></b></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><b><%# Eval("CustAltPhone")%></b></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><b><%# Eval("D2DEmpName")%></b></td>
                                                                            </tr>
                                                                        </table>

                                                                    </td>
                                                                </tr>

                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                    CancelControlID="ibtnCancelNotes" DropShadow="true" PopupControlID="divAddNotes" TargetControlID="btnNULLNotes">
                </cc1:ModalPopupExtender>
                <div id="divAddNotes" runat="server" style="display: none;" class="modal_popup">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="color-line"></div>
                            <div class="modal-header">
                                <div style="float: right">
                                    <asp:LinkButton ID="ibtnCancelNotes" runat="server" OnClick="ibtnCancelNotes_Onclick" CssClass="btn btn-danger btncancelicon" data-dismiss="modal"
                                        CausesValidation="false">Close
                                    </asp:LinkButton>
                                </div>
                                <h4 class="modal-title" id="myModalLabel">Change customer Lead Status
                                </h4>
                            </div>
                            <asp:UpdateProgress ID="UpdateProg1" DisplayAfter="0" runat="server" AssociatedUpdatePanelID="UpdatePanel11">
                                <ProgressTemplate>
                                    <div class="overlay">
                                        <asp:Image ID="imgloading" ImageUrl="~/admin/images/loading.gif" AlternateText="Processing"
                                            runat="server" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <div class="modal-body paddnone">
                                <div class="panel-body">
                                    <div id="divInstallgrid" runat="server">
                                        <div class="table-responsive">
                                            <div class="alert alert-success" id="PanSuccess" runat="server" style="text-align: left" visible="false">
                                                <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful.</strong>
                                            </div>
                                            <div class="alert alert-danger" id="PanError" runat="server" visible="false"><i class="icon-remove-sign"></i><strong>&nbsp;Please Enter Customer Address.</strong> </div>
                                            <asp:RadioButtonList ID="rblcustleadstatus" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblcustleadstatus_SelectedIndexChanged" AutoPostBack="true"
                                                CssClass="radio">
                                            </asp:RadioButtonList>
                                            <div style="height: auto; width: 100%">
                                                <div class="panel-body haftdiv">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div id="divfollowsup" runat="server" visible="false">
                                                                <div class="form-group" id="divcontact" runat="server">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Contact
                                                                        </label>
                                                                    </span><span>
                                                                        <asp:DropDownList ID="ddlContact" runat="server" Width="200px" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" CssClass="myval">
                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                            ControlToValidate="ddlContact" Display="Dynamic" ValidationGroup="info"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                                <%--  <div class="form-group dateimgarea" id="divNextDate" runat="server" visible="true">
                                                                <span class="name">
                                                                    <label class="control-label">
                                                                        Next Follow Up Date<span class="symbol required">*</span>
                                                                    </label>
                                                                </span><span class="dateimg">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:TextBox ID="txtNextFollowupDate" runat="server" Width="100" CssClass="form-control"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:ImageButton ID="ImageButton1" CausesValidation="false" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" PopupButtonID="ImageButton1"
                                                                                    TargetControlID="txtNextFollowupDate" Format="dd/MM/yyyy">
                                                                                </cc1:CalendarExtender>
                                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Mask="99/99/9999"
                                                                                    MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                                    MaskType="date" TargetControlID="txtNextFollowupDate" CultureName="en-GB">
                                                                                </cc1:MaskedEditExtender>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtNextFollowupDate"
                                                                                    ValidationGroup="info" ErrorMessage="This value is required." CssClass="reqerror" Display="Dynamic"> </asp:RequiredFieldValidator>
                                                                                <asp:CompareValidator ID="cmpNextDate" runat="server" ControlToValidate="txtNextFollowupDate"
                                                                                    ErrorMessage="Next follow up date cannot be less than today's date" Operator="GreaterThanEqual"
                                                                                    ValidationGroup="info" Type="Date" Display="Dynamic"></asp:CompareValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>--%>

                                                                <div class="form-group">
                                                                    <asp:Label ID="Label23" runat="server" class="col-sm-4  control-label">
                                                <strong> Next Follow Up Date</strong></asp:Label>
                                                                    <div class="input-group date datetimepicker1 col-sm-4">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtNextFollowupDate" runat="server" class="form-control" placeholder="NextFollowupDate">
                                                                        </asp:TextBox>
                                                                        <asp:CompareValidator ID="cmpNextDate" runat="server" ControlToValidate="txtNextFollowupDate"
                                                                            ErrorMessage="Next follow up date cannot be less than today's date" Operator="GreaterThanEqual"
                                                                            ValidationGroup="info" Type="Date" Display="Dynamic"></asp:CompareValidator>

                                                                    </div>
                                                                </div>


                                                                <div class="form-group" id="description" runat="server">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Description<span class="symbol required">*</span>
                                                                        </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Height="10%" Width="200px"
                                                                            CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                            ValidationGroup="info" ControlToValidate="txtDescription" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="divappointment" runat="server" visible="false">
                                                                <div class="form-group">
                                                                    <label class="control-label">
                                                                        Appointment Time<span class="symbol required">*</span>
                                                                    </label>
                                                                    <asp:DropDownList ID="ddlAppTime" runat="server" Width="100px" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                                                        <asp:ListItem Value="">Select</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                        ControlToValidate="ddlAppTime" Display="Dynamic" ValidationGroup="info"></asp:RequiredFieldValidator>
                                                                </div>
                                                                <%--      <div class="form-group dateimgarea">
                                                                <span class="name">
                                                                    <label class="control-label">
                                                                        Appointment Date<span class="symbol required">*</span>
                                                                    </label>
                                                                </span><span class="dateimg">
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:TextBox ID="txtD2DAppDate" runat="server" Width="100" CssClass="form-control"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <asp:ImageButton ID="ImageButton2" CausesValidation="false" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="ImageButton2"
                                                                                    TargetControlID="txtD2DAppDate" Format="dd/MM/yyyy">
                                                                                </cc1:CalendarExtender>
                                                                                <cc1:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Mask="99/99/9999"
                                                                                    MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                                                    MaskType="date" TargetControlID="txtNextFollowupDate" CultureName="en-GB">
                                                                                </cc1:MaskedEditExtender>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorEDA" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                                    ValidationGroup="info" ControlToValidate="txtD2DAppDate"
                                                                                    Display="Dynamic"></asp:RequiredFieldValidator>
                                                                                <asp:CompareValidator ID="cmpNextDate1" runat="server" ControlToValidate="txtD2DAppDate"
                                                                                    ErrorMessage="Appointment date cannot be less than today's date" Operator="GreaterThanEqual"
                                                                                    ValidationGroup="info" Type="Date" Display="Dynamic"></asp:CompareValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </span>
                                                                <div class="clear">
                                                                </div>
                                                            </div>--%>

                                                                <div class="form-group">
                                                                    <asp:Label ID="Label1" runat="server" class="col-sm-4  control-label">
                                                <strong>Appointment Date</strong></asp:Label>
                                                                    <div class="input-group date datetimepicker1 col-sm-4">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtD2DAppDate" runat="server" class="form-control" placeholder="Start Date">
                                                                        </asp:TextBox>
                                                                        <asp:CompareValidator ID="cmpNextDate1" runat="server" ControlToValidate="txtD2DAppDate"
                                                                            ErrorMessage="Appointment date cannot be less than today's date" Operator="GreaterThanEqual"
                                                                            ValidationGroup="info" Type="Date" Display="Dynamic"></asp:CompareValidator>

                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Note<span class="symbol required">*</span>
                                                                        </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtnote" runat="server" TextMode="MultiLine" Height="10%" Width="200px"
                                                                            CssClass="form-control"></asp:TextBox>
                                                                    </span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="divCancel" runat="server" visible="false">
                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Notes<span class="symbol required">*</span>
                                                                        </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtCancelNotes" runat="server" TextMode="MultiLine" Height="10%" Width="200px"
                                                                            CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                            ValidationGroup="info" ControlToValidate="txtCancelNotes" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <span class="name">
                                                                    <label class="control-label">&nbsp;</label>
                                                                </span>
                                                                <span>
                                                                    <asp:Button ID="ibtnAdd" runat="server" Text="Add" OnClick="btnSave_Click"
                                                                        ValidationGroup="info" class="btn btn-primary addwhiteicon" />
                                                                    <asp:Button ID="btnOK" Style="display: none; visible: false;" runat="server"
                                                                        CssClass="btn" Text=" OK " />
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:Button ID="btnNULLNotes" Style="display: none;" runat="server" />
                <asp:HiddenField ID="hndD2Ddate" runat="server" />

                <cc1:ModalPopupExtender ID="ModalPopupExtenderNotes" runat="server" BackgroundCssClass="modalbackground"
                    CancelControlID="ibtnCancelResNotes" DropShadow="true" PopupControlID="divReschNotes" TargetControlID="btnNULLResNotes">
                </cc1:ModalPopupExtender>
                <div id="divReschNotes" runat="server" style="display: none">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div style="float: right">
                                    <button id="ibtnCancelResNotes" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">
                                        Close
                                    </button>
                                </div>
                                <h4 class="modal-title" id="H1">Previous Appointment Detail</h4>
                            </div>
                            <div class="modal-body paddnone">
                                <div class="panel-body">
                                    <div class="formainline">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="tblDisplay widthPc tablebrd marginbtmsp20">
                                                    <tr id="trD2DEmployee" runat="server">
                                                        <td width="150px" valign="top"><b>Appointment Time</b>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblAppTime" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trD2DAppDate" runat="server">
                                                        <td valign="top"><b>Appointment Date</b>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblAppDate" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr id="trD2DAppTime" runat="server">
                                                        <td valign="top"><b>Notes</b>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblAppNotes" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:Button ID="btnNULLResNotes" Style="display: none;" runat="server" />

                <cc1:ModalPopupExtender ID="ModalPopupExtenderASR" runat="server" BackgroundCssClass="modalbackground"
                    CancelControlID="Button1" DropShadow="true" PopupControlID="divAssignSalesRep" TargetControlID="btnNullASR">
                </cc1:ModalPopupExtender>
                <div id="divAssignSalesRep" runat="server" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                 <div style="float: right">
                                <button id="Button1" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">
                                    Close
                                </button>
                                     </div>
                                <h4 class="modal-title" id="H2">Assign Sales Rep</h4>
                            </div>
                            <div class="modal-body paddnone">
                                <div class="panel-body">
                                    <div class="formainline">
                                        <div class="form-group checkareanew">
                                            <label>Select</label>
                                            <asp:DropDownList ID="ddlAssignSalesRep" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" CssClass="myval">
                                                <asp:ListItem Value="">Sales Rep</asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="form-group marginleft">
                                            <asp:Button ID="btnAssign" runat="server" CssClass="btn btn-primary savewhiteicon" Text="Assign" OnClick="btnAssign_Click" CausesValidation="true" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:Button ID="btnNullASR" Style="display: none;" runat="server" />

                <cc1:ModalPopupExtender ID="ModalPopupExtenderAttnd" runat="server" BackgroundCssClass="modalbackground"
                    CancelControlID="Button2" DropShadow="true" PopupControlID="divAttended" TargetControlID="btnNullAttnd">
                </cc1:ModalPopupExtender>
                <div id="divAttended" runat="server" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                 <div style="float: right">
                                <button id="Button2" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">
                                    Close
                                </button>
                                     </div>
                                <h4 class="modal-title" id="H3">Attend Detail</h4>
                            </div>
                            <div class="modal-body paddnone">
                                <div class="panel-body">
                                    <div class="formainline">
                                        <div class="form-group checkareanew">
                                            <label></label>
                                            <asp:RadioButtonList ID="rblAttend" runat="server" AppendDataBoundItems="true" AutoPostBack="true" RepeatDirection="Horizontal"
                                                OnSelectedIndexChanged="rblAttend_SelectedIndexChanged">
                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                <asp:ListItem Value="2">No</asp:ListItem>
                                            </asp:RadioButtonList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                ControlToValidate="rblAttend" Display="Dynamic" ValidationGroup="attend"></asp:RequiredFieldValidator>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="form-group checkareanew" id="divAttndNo" runat="server" visible="false">
                                            <label>Notes</label>
                                            <asp:TextBox ID="txtAttendReason" runat="server" MaxLength="500" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                ControlToValidate="txtAttendReason" Display="Dynamic" ValidationGroup="attend"></asp:RequiredFieldValidator>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="form-group marginleft">
                                            <asp:Button ID="btnAttend" runat="server" CssClass="btn btn-primary savewhiteicon" Text="Save" ValidationGroup="attend"
                                                OnClick="btnAttend_Click" CausesValidation="true" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:Button ID="btnNullAttnd" Style="display: none;" runat="server" />

                <asp:HiddenField ID="hndcustomerid" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>

