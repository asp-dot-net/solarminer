using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_company_mtcecalldetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            TextWriter txtWriter = new StringWriter() as TextWriter;
            Server.Execute("~/mailtemplate/printmtce.aspx", txtWriter);

            liDetail.Text = txtWriter.ToString();
        }
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/company/mtcecall.aspx");
    }
}