﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_reports_PickListTracker : System.Web.UI.Page
{
    static DataView dv;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            //SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            //if (Convert.ToBoolean(st_emp.showexcel) == true)
            //{
            //    tdExport.Visible = true;
            //}
            //else
            //{
            //    tdExport.Visible = false;
            //}
            txtStartDate.Text = DateTime.Now.AddHours(14).ToShortDateString();
            txtEndDate.Text = DateTime.Now.AddHours(14).ToShortDateString();
            ListItem item2 = new ListItem();
            item2.Text = "Installer";
            item2.Value = "";
            ddlInstaller.Items.Clear();
            ddlInstaller.Items.Add(item2);

            ddlInstaller.DataSource = ClstblContacts.tblContacts_SelectInverter();
            ddlInstaller.DataValueField = "ContactID";
            ddlInstaller.DataTextField = "Contact";
            ddlInstaller.DataMember = "Contact";
            ddlInstaller.DataBind();

            ListItem item = new ListItem();
            item.Value = "";
            item.Text = "Select";
            ddlStockAllocationStore.Items.Clear();
            ddlStockAllocationStore.Items.Add(item);

            ddlStockAllocationStore.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectActive();
            ddlStockAllocationStore.DataValueField = "CompanyLocationID";
            ddlStockAllocationStore.DataTextField = "CompanyLocation";
            ddlStockAllocationStore.DataMember = "CompanyLocation";
            ddlStockAllocationStore.DataBind();

            if(Roles.IsUserInRole("Administrator"))
            {
                lnkdeleteNew.Visible = true;
            }
            else
            {
                lnkdeleteNew.Visible = false;
            }
            BindGrid(0);
        }
        
    }
    protected DataTable GetGridData()
    {
        //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = new DataTable();
        
        dt = Reports.PicklistTrackerGetData("2", txtProjectNumber.Text, ddlInstaller.SelectedValue, ddlStockAllocationStore.SelectedValue, txtStockItem.Text, ddlSearchDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlPicklist.SelectedValue, ddlDeduct.SelectedValue, ddlpcktype.SelectedValue);
        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;

        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {

            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;

            divnopage.Visible = true;

            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else if (Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = false;
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }



    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = new DataTable();
        dt = GetGridData();
        Response.Clear();
        string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
        //try
        //{
        Export oExport = new Export();
        string FileName = "PicklistTracker" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
        if (ddlPicklist.SelectedValue == "1")
        {
            int[] ColList = { 0,5,12,16,14,2,9,8,7,11,3,10 };
            string[] arrHeader = { "ID", "ProjectNumber","ProjectStatus", "PostCode", "Location","InstallerName", "CreatedDate", "InstallDate","Deduct Date", "SystemDetails", "Reason", "CreatedBy" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        else if (ddlPicklist.SelectedValue == "2")
        {
            int[] ColList = { 0,2,12,9,8,7,11,3,10 };
            string[] arrHeader = { "ID", "ProjectNumber", "ProjectStatus", "PostCode", "Location", "InstallerName", "CreatedDate", "InstallDate", "Deduct Date", "SystemDetails", "Reason", "CreatedBy" };
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        else
        {
            int[] ColList = { 5,1,6,15,9,3,4,11,14,8,12 };
            string[] arrHeader = { "ID", "ProjectNumber", "ProjectStatus", "PostCode", "Location", "InstallerName", "CreatedDate", "InstallDate", "Deduct Date", "SystemDetails", "Reason", "CreatedBy" };
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
       
        //}
        //catch (Exception Ex)
        //{
        //    //   lblError.Text = Ex.Message;
        //}
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }


    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = dv.ToTable();

        GridViewSortExpression = e.SortExpression;
        GridView1.DataSource = SortDataTable(dt, false);
        GridView1.DataBind();
    }
    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlStockAllocationStore.SelectedValue = "";
        ddlSearchDate.SelectedValue = "1";
        ddlInstaller.SelectedValue = "";
        txtProjectNumber.Text = "";
        txtStockItem.Text = "";
        txtStartDate.Text = DateTime.Now.AddHours(14).ToShortDateString();
        txtEndDate.Text = DateTime.Now.AddHours(14).ToShortDateString();
        BindGrid(0);
    }
    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToString() == "Select")
        {
            GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);
            int RowIndex = row.RowIndex;
            string ProjectID = (row.FindControl("hdnPrjId") as HiddenField).Value;
            //string ProjectID =e.CommandArgument.ToString();
            Telerik_reports.generate_PickList(ProjectID);
        }
        //if (e.CommandName == "delete")
        //{
        //    string PickID = e.CommandArgument.ToString();
        //    hndpicklistid.Value = PickID;
        //    ModalPopupExtenderDelete.Show();
        //}

        BindGrid(0);
    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        //DateTime currentDateWithCurrentTime = System.DateTime.Now.AddHours(14);
        //DateTime dateWeekBeforeWithTime = currentDateWithCurrentTime.AddDays(-7);
        //string time = currentDateWithCurrentTime.ToShortDateString();
        ////bool d1 = ClstblProjects.deletepicklistData(time);
        ////if(d1)
        ////{
        ////    SetAdd1();
        ////}
        //ModalPopupExtenderDelete.Hide();
        //BindGrid(0);
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hndpickid = (HiddenField)e.Row.FindControl("HiddenField1");
            LinkButton btnduplicate = (LinkButton)e.Row.FindControl("btnduplicate");
           // LinkButton btnpicklistDelete = (LinkButton)e.Row.FindControl("btnpicklistDelete");
            if (hndpickid.Value != null && hndpickid.Value != "")
            {
                if (hndpickid.Value=="" || hndpickid.Value == "0")
                {
                    btnduplicate.Visible = false;
                }
                else
                {
                    btnduplicate.Visible = true;
                }
            }
            //if(Roles.IsUserInRole("Administrator"))
            //{
            //    btnpicklistDelete.Visible = true;
            //}
            //else
            //{
            //    btnpicklistDelete.Visible = false;
            //}
            
        }
    }

    protected void lnkdel_Click(object sender, EventArgs e)
    {
        try
        {
            int rowsCount = GridView1.Rows.Count;
            GridViewRow gridRow;
            for (int i = 0; i < rowsCount; i++)
            {
                string id;
                gridRow = GridView1.Rows[i];
                id = GridView1.DataKeys[i].Value.ToString();
                //Response.Write(id+"</br>");

                CheckBox chkdelete = (CheckBox)gridRow.FindControl("cbxDelete");

                if (chkdelete.Checked)
                {
                    //if (ddlpcktype.SelectedValue != null && ddlpcktype.SelectedValue != "")
                    //{
                    if (id != null && id != "")
                    {
                       bool sucess1 = ClstblProjects.tbl_PickListLog_DeleteByID(id);
                    }

                    //}

                }
            }
            SetAdd1();
            BindGrid(0);
            ModalPopupExtenderDelete.Hide();
        }
        catch (Exception ex)
        {

        }
    }

    protected void chkdeletewhole_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkdeletewhole = (CheckBox)GridView1.HeaderRow.FindControl("chkdeletewhole");
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkdelete = (CheckBox)row.FindControl("cbxDelete");

                if (chkdeletewhole.Checked == true)
                {
                    chkdelete.Checked = true;
                }
                else
                {
                    chkdelete.Checked = false;
                }
            }
        }
    }

    protected void lnkupdateType_Click(object sender, EventArgs e)
    {
        try
        {
            int rowsCount = GridView1.Rows.Count;
            GridViewRow gridRow;
            for (int i = 0; i < rowsCount; i++)
            {
                string id;
                gridRow = GridView1.Rows[i];
                id = GridView1.DataKeys[i].Value.ToString();
                //Response.Write(id+"</br>");

                CheckBox chkdelete = (CheckBox)gridRow.FindControl("cbxDelete");

                if (chkdelete.Checked)
                {
                    if (ddlpcktype.SelectedValue != null && ddlpcktype.SelectedValue != "")
                    {
                       // bool sucess1 = ClstblProjects.tbl_picklistlog_updatePicklistType(id,ddlpcktype.SelectedValue);

                    }

                }
            }
            SetAdd1();
            BindGrid(0);
           // ModalPopupExtenderDelete.Hide();
        }
        catch(Exception ex)
        {

        }
    }

    //protected void lnkDel_Click(object sender, EventArgs e)
    //{
    //    //ModalPopupExtenderDelete.Show();
    //}

    protected void lnkdeleteNew_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderDelete.Show();
    }
}