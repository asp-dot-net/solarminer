using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_company_stctracker : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;
    protected static int custompageIndex;
    protected static int countdata;
    protected static int startindex;
    protected static int lastpageindex;
    protected static int custompagesize = Convert.ToInt32(SiteConfiguration.GetPageSize());

    public class TokenData
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
    }
    public class RootObject
    {
        public TokenData TokenData { get; set; }
        public bool Status { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string access_token { get; set; }
    }
    public class TokenObject
    {
        public string access_token { get; set; }
    }
    public class RootObj
    {
        public string Message { get; set; }
        public string code { get; set; }
        //public string[] PVDNumber { get; set; }
        public List<STCData> STCDataList { get; set; }
        public string ManualQuoteNumber { get; set; }
        //public string[] guidvalue { get; set; }
    }
    public class STCData
    {
        public string guidvalue { get; set; }
        public string ProjectNo { get; set; }
        public string PVDNumber { get; set; }
        public string QuickFormID { get; set; }
        public Nullable<DateTime> STCApplied { get; set; }
        public string PVDStatus { get; set; }
    }

    #region CreateJob Model
    public partial class CreateJob
    {
        [JsonProperty("quickformGuids")]
        public string[] quickformGuids { get; set; }

        [JsonProperty("ProjectNumbers")]
        public int[] ProjectNumbers { get; set; }
    }
    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        HidePanels();
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            custompageIndex = 1;



            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                tdExport.Visible = true;
            }
            else
            {
                tdExport.Visible = false;
            }
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            BindDropDown();
            BindGrid(0);
        }

        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    public void BindDropDown()
    {

        System.Web.UI.WebControls.ListItem item3 = new System.Web.UI.WebControls.ListItem();
        item3.Text = "Select";
        item3.Value = "";
        ddlPVDStatusID.Items.Clear();
        ddlPVDStatusID.Items.Add(item3);

        ddlPVDStatusID.DataSource = ClsProjectSale.tblPVDStatus_SelectActive();
        ddlPVDStatusID.DataValueField = "PVDStatusID";
        ddlPVDStatusID.DataMember = "PVDStatus";
        ddlPVDStatusID.DataTextField = "PVDStatus";
        ddlPVDStatusID.DataBind();

        lstSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        lstSearchStatus.DataBind();

        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();

        ddlSTCCheckedBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlSTCCheckedBy.DataMember = "fullname";
        ddlSTCCheckedBy.DataTextField = "fullname";
        ddlSTCCheckedBy.DataValueField = "EmployeeID";
        ddlSTCCheckedBy.DataBind();

        ddlSTCStatus.DataSource = ClsProjectSale.tblPVDStatus_SelectActive();
        ddlSTCStatus.DataMember = "PVDStatus";
        ddlSTCStatus.DataTextField = "PVDStatus";
        ddlSTCStatus.DataValueField = "PVDStatusID";
        ddlSTCStatus.DataBind();

        ListItem item = new ListItem();
        item.Value = "";
        item.Text = "Blank";
        ddlSTCStatus.Items.Add(item);
    }
    protected DataTable GetGridData()
    {
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }

        //string quickform = "";
        //if (ddlisquickform.SelectedValue.ToString() == "1")
        //{
        //    quickform = "1";
        //}
        //if (ddlisquickform.SelectedValue.ToString() == "2")
        //{
        //    quickform = "0";
        //}

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = new DataTable();

        //Response.Write(txtContactSearch.Text + "--" + txtProjectNumber.Text + "--" + txtMQNsearch.Text + "--" + ddlSTCCheckedBy.SelectedValue + "--" + txtSerachCity.Text + "--" + ddlSearchState.SelectedValue + "--" + txtSearchPostCode.Text + "--" + txtSTCApprovalNo.Text + "--" + txtSTCUpload.Text + "--" + ddlSTCStatus.SelectedValue + "--" + ddlSearchDate.SelectedValue + "--" + txtStartDate.Text + "--" + txtEndDate.Text + "--" + Convert.ToString(chkNoPVD.Checked) + "--" + ddlisquickform.SelectedValue.ToString() + "--" + selectedItem);
        //Response.End();

        dt = ClstblProjects.tblProjects_SelectSTCTrackerSearch(txtContactSearch.Text, txtProjectNumber.Text, txtMQNsearch.Text, ddlSTCCheckedBy.SelectedValue, txtSerachCity.Text, ddlSearchState.SelectedValue, txtSearchPostCode.Text, txtSTCApprovalNo.Text, txtSTCUpload.Text, ddlSTCStatus.SelectedValue, ddlSearchDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlIsPVD.SelectedValue, ddlisquickform.SelectedValue.ToString(), selectedItem);
        if (dt.Rows.Count > 0)
        {
            int noofpanel = 0, stcvalue = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["NumberPanels"].ToString() != string.Empty)
                {
                    noofpanel += Convert.ToInt32(dr["NumberPanels"]);
                }
                if (dr["STCNumber"].ToString() != string.Empty)
                {
                    stcvalue += Convert.ToInt32(dr["STCNumber"]);
                }
            }
            lbltotalpanel.Text = noofpanel.ToString();
            lblstcvalue.Text = stcvalue.ToString();
        }


        //ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            //Response.Write("dsfd");
            //Response.End();
            GridView1.DataSource = dt;
            GridView1.DataBind();

            PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction()", true);
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else if (Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = false;
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
        BindScript();
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    private void HidePanels()
    {
        PanNoRecord.Visible = false;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        // ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        string ProjectID = e.CommandArgument.ToString();
        if (e.CommandName.ToLower() == "docscheck")
        {
            ClstblProjects.tblProjects2_UpdateDocsDone(ProjectID, "True");
        }
        if (e.CommandName.ToLower() == "docsuncheck")
        {
            ClstblProjects.tblProjects2_UpdateDocsDone(ProjectID, "False");
        }
        if (e.CommandName.ToLower() == "nocheck")
        {
            ClstblProjects.tblProjects2_UpdateNoDocs(ProjectID, "True");
        }
        if (e.CommandName.ToLower() == "nouncheck")
        {
            ClstblProjects.tblProjects2_UpdateNoDocs(ProjectID, "False");
        }
        BindGrid(0);
        // BindScript();
    }
    //protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    //{
    //    txtContactSearch.Text = string.Empty;
    //    txtProjectNumber.Text = string.Empty;
    //    txtMQNsearch.Text = string.Empty;
    //    ddlSTCCheckedBy.SelectedValue = "";
    //    txtSerachCity.Text = string.Empty;
    //    ddlSearchState.SelectedValue = "";
    //    txtSearchPostCode.Text = string.Empty;
    //    txtSTCApprovalNo.Text = string.Empty;
    //    txtSTCUpload.Text = string.Empty;
    //    ddlSTCStatus.SelectedValue = "";
    //    ddlSearchDate.SelectedValue = "";
    //    txtStartDate.Text = string.Empty;
    //    txtEndDate.Text = string.Empty;
    //    foreach (RepeaterItem item in lstSearchStatus.Items)
    //    {
    //        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
    //        chkselect.Checked = false;
    //    }

    //    BindGrid(0);
    //    ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    //}
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }
        //string quickform = "";
        //if (ddlisquickform.SelectedValue.ToString() == "1")
        //{
        //    quickform = "1";
        //}
        //if (ddlisquickform.SelectedValue.ToString() == "2")
        //{
        //    quickform = "0";
        //}
        // DataTable dt = ClstblProjects.tblProjects_SelectSTCTrackerSearch(txtContactSearch.Text, txtProjectNumber.Text, txtMQNsearch.Text, ddlSTCCheckedBy.SelectedValue, txtSerachCity.Text, ddlSearchState.SelectedValue, txtSearchPostCode.Text, txtSTCApprovalNo.Text, txtSTCUpload.Text, ddlSTCStatus.SelectedValue, ddlSearchDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, Convert.ToString(chkNoPVD.Checked), ddlisquickform.SelectedValue.ToString(), selectedItem);
        DataTable dt = GetGridData();
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
            string FileName = "STC Tracker" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
                                                                int[] ColList = {5,22,31,0,9,33,32,16,29,21,17,18,27,20,};
            string[] arrHeader = {"Project#", "ProjectType","InstallDate","Address","PCode","Inst Conf","STC App Date","PVD No","PVD Status","ProjectStatus","KW","STC","InstallerName","STC Notes"};
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //lblError.Text = Ex.Message;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {

            ltrPage.Text = "";
        }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    #region pagination
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    Response.Write(lnkpagebtn.);
            //    //lnkpagebtn.Style.Add("class", "pagebtndesign nonepointer");
            //}
        }
    }
    public void PageClick(String id)
    {
        custompageIndex = Convert.ToInt32(id);
        BindGrid(0);
    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        //lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;



        //if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //{
        //    lastpageindex = lastpageindex + 1;
        //}
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    #endregion


    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtContactSearch.Text = string.Empty;
        txtProjectNumber.Text = string.Empty;
        txtMQNsearch.Text = string.Empty;
        ddlSTCCheckedBy.SelectedValue = "";
        txtSerachCity.Text = string.Empty;
        ddlSearchState.SelectedValue = "";
        txtSearchPostCode.Text = string.Empty;
        txtSTCApprovalNo.Text = string.Empty;
        txtSTCUpload.Text = string.Empty;
        ddlSTCStatus.SelectedValue = "";
        ddlSearchDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlisquickform.ClearSelection();
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }

        BindGrid(0);
        //  ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    protected void lnkjob_Click(object sender, EventArgs e)
    {
        LinkButton lnkReceived = (LinkButton)sender;
        GridViewRow item1 = lnkReceived.NamingContainer as GridViewRow;
        HiddenField hdnProjectID = (HiddenField)item1.FindControl("hdnProjectID");
        HiddenField hdncust = (HiddenField)item1.FindControl("hdncust");
        // hdn_custpopup.Value = hdncust.Value;
        //Response.Write(hdnProjectID.Value);
        //Response.End();
        if (hdnProjectID.Value != string.Empty)
        {

            SttblProjects stpro = ClstblProjects.tblProjects_SelectByProjectID(hdnProjectID.Value);

            ModalPopupExtender1.Show();
            div_popup.Visible = true;
            hdnid.Value = hdnProjectID.Value;
            GetSTCByProject(hdnProjectID.Value);


        }
        else
        {
            div_popup.Visible = false;
            ModalPopupExtender1.Hide();


        }
        BindGrid(0);

    }
    public void GetSTCByProject(string proid)
    {
        if (!string.IsNullOrEmpty(proid))
        {

            string ProjectID = proid;
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);


            try
            {
                txtSTCUploaded.Text = Convert.ToDateTime(st.STCUploaded).ToShortDateString();
            }
            catch { }

            try
            {
                txtSTCApplied.Text = Convert.ToDateTime(st.STCApplied).ToShortDateString();
            }
            catch { }

            txtSTCUploadNumber.Text = st.STCUploadNumber;
            txtPVDNumber.Text = st.PVDNumber;
            ddlPVDStatusID.SelectedValue = st.PVDStatusID;
            txtSTCNotes.Text = st.STCNotes;
            txtformbayid.Text = st.FormbayId;

        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void lstSearchStatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        HiddenField hdnID = (HiddenField)item.FindControl("hdnID");
        Literal ltprojstatus = (Literal)item.FindControl("ltprojstatus");
        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");

        if (Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("Purchase Manager") || Roles.IsUserInRole("STC") || Roles.IsUserInRole("Verification") || Roles.IsUserInRole("WarehouseManager") || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("Maintenance"))
        {
            if (hdnID.Value == "2")
            {
                e.Item.Visible = false;
            }
        }

    }


    protected void btnUpdateSTC_Click(object sender, EventArgs e)
    {

        string ProjectID = hdnid.Value;
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        string STCUploaded = txtSTCUploaded.Text.Trim();
        string STCApplied = txtSTCApplied.Text.Trim();
        string STCUploadNumber = txtSTCUploadNumber.Text;
        string PVDNumber = txtPVDNumber.Text;
        string PVDStatusID = ddlPVDStatusID.SelectedValue;
        string formbayid = txtformbayid.Text;
        string STCNotes = txtSTCNotes.Text;

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;

        bool sucForms = ClsProjectSale.tblProjects_UpdateSTC(ProjectID, STCUploaded, STCApplied, STCUploadNumber, PVDNumber, PVDStatusID, UpdatedBy);
        bool sucstcnotes = ClsProjectSale.tblProjects_UpdateSTCNotes(ProjectID, STCNotes);
        bool sucForms1 = ClsProjectSale.tblProjects_UpdateSTC_formbayid(ProjectID, formbayid);

        /* -------------------- STC / Invoice Paid -------------------- */
        if (txtSTCApplied.Text.Trim() != string.Empty)
        {
            if (st.InvoiceFU != string.Empty)
            {
                DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "14");
                if (dtStatus.Rows.Count > 0)
                {
                }
                else
                {
                    int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("14", ProjectID, stEmp.EmployeeID, st.NumberPanels);
                }
                ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "14");
            }
        }
        /* ------------------------------------------------------------ */
        /* ---------------------- pvdapproved ---------------------- */
        if (ddlPVDStatusID.SelectedValue == "7")
        {
            DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "19");
            if (dtStatus.Rows.Count > 0)
            {
            }
            else
            {
                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("19", ProjectID, stEmp.EmployeeID, st.NumberPanels);
            }
            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "19");
        }
        /* ------------------------------------------------------------ */
        /* ---------------------- Complete ---------------------- */
        if (ddlPVDStatusID.SelectedValue == "7" && st.SalesCommPaid != string.Empty && st.paydate != string.Empty)
        {
            DataTable dtStatus = ClsProjectSale.tblProjects_SelectStatus(ProjectID, "5");
            if (dtStatus.Rows.Count > 0)
            {
            }
            else
            {
                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("5", ProjectID, stEmp.EmployeeID, st.NumberPanels);
            }
            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "5");
        }
        /* ------------------------------------------------------ */
        /* ---------------------- paerworkrec back ---------------------- */
        if (txtSTCApplied.Text.Trim() == string.Empty && st.InvoiceFU == string.Empty)
        {
            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "13");
        }
        /* ------------------------------------------------------ */


        if (sucForms)
        {

            SetAdd1();

            //PanSuccess.Visible = true;
        }
        else
        {
            SetExist();
            //PanAlreadExists.Visible = true;

        }
        BindGrid(0);
    }
    protected void btnquickfrom_Click(object sender, EventArgs e)
    {
        try
        {
            String CurrentDate = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));
        DataTable dt = ClstblProjects.tblProjects_SelectGuid_ApprovedSTCStatus(CurrentDate);
        if (dt.Rows.Count > 0)
        {
            int[] projectno = new int[dt.Rows.Count];

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                projectno[i] = Convert.ToInt32(dt.Rows[i]["ProjectNumber"].ToString());
            }
            int[] p = projectno;
            litmessage.Text = string.Empty;
            for (int i = 0; i < p.Length; i++)
            {
                litmessage.Text = litmessage.Text + p[i].ToString();
            }
            PanError.Visible = true;
            //litmessage.Visible = true;
            //try
            //{
            //  lbltesting.Text = "hii4";
            Dictionary<string, string> oJsonObject = new Dictionary<string, string>();
            /// JObject oJsonObject = new JObject();
            oJsonObject.Add("username", "arisesolar");
            oJsonObject.Add("password", "Arise@123");
            oJsonObject.Add("redirect_uri", "http://api.quickforms.com.au/");
            oJsonObject.Add("client_id", "5D95C4B5-EE01-4D87-B1BC-B4C159CBEFDF");
            //oJsonObject.Add("scope", "JobData");
            oJsonObject.Add("scope", "CreateJob");
            oJsonObject.Add("response_type", "code");

            HttpClient client = new HttpClient();

            var content1 = new FormUrlEncodedContent(oJsonObject);
            var result = client.PostAsync("http://api.quickforms.com.au/auth/logon", content1).Result;
            var tokenJson = result.Content.ReadAsStringAsync();
            bool IsSuccess = result.IsSuccessStatusCode;
            litmessage.Text = litmessage.Text + IsSuccess;
            if (IsSuccess)
            {
                TokenObject RootObject = JsonConvert.DeserializeObject<TokenObject>(tokenJson.Result);
                string AccessToken = RootObject.access_token;
                //lbltesting.Text = AccessToken;
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", AccessToken);
                var content = new CreateJob
                {
                    quickformGuids = null,
                    ProjectNumbers = projectno                    
                };

                var temp = JsonConvert.SerializeObject(content, Formatting.Indented);
                // lbltesting.Text = temp;               
                using (var client1 = new HttpClient())
                {
                    client1.BaseAddress = new Uri("http://api.quickforms.com.au/api/ApprovedDataFetch/jobData");

                    //HTTP POST
                    var postTask = client1.PostAsJsonAsync<CreateJob>("Job", content);

                    postTask.Wait();

                    var resultdata = postTask.Result;
                    // lbltesting.Text = resultdata.ToString();
                    if (resultdata.IsSuccessStatusCode)
                    {
                        var jsondata = resultdata.Content.ReadAsStringAsync().Result;

                        RootObj JObresult = JsonConvert.DeserializeObject<RootObj>(jsondata);

                        //   litmessage.Text = jsondata.ToString();
                        PanError.Visible = true;
                        litmessage.Text = JObresult.Message;
                        if (JObresult.Message == "success")
                        {
                            for (int i = 0; i < JObresult.STCDataList.Count; i++)
                            {
                                string stcapplied = string.Empty;
                                string guid = JObresult.STCDataList[i].guidvalue.ToString();
                                string projno = JObresult.STCDataList[i].ProjectNo.ToString();
                                string pvdno = JObresult.STCDataList[i].PVDNumber.ToString();
                                if(JObresult.STCDataList[i].STCApplied!=null)
                                {
                                  stcapplied = Convert.ToDateTime(JObresult.STCDataList[i].STCApplied).ToString();
                                }                                
                                string quickformid = JObresult.STCDataList[i].QuickFormID.ToString();
                                string pvdstatus = JObresult.STCDataList[i].PVDStatus.ToString();
                                DataTable dt1 = ClstblProjects.tblPVDStatus_SelectByPVDStatus(pvdstatus);
                                string pvdstatusid = dt1.Rows[0]["PVDStatusID"].ToString();
                                // litmessage.Text = JObresult.Message + projno + pvdno;
                                bool sucForms = ClsProjectSale.tblProjects_UpdateSTCByProjNo(projno, stcapplied, pvdno, pvdstatusid, quickformid);
                            }
                            litmessage.Text = "Success";
                            PanError.Visible = true;

                            //   lbltesting.Text = JObresult.Message;
                        }
                        else
                        {
                            //   lbltesting.Text = JObresult.Message.ToString();
                            PanError.Visible = true;
                            litmessage.Text = JObresult.Message;
                        }
                    }
                    else
                    {
                        PanError.Visible = true;
                        litmessage.Text = "Service call was not successful.";
                    }
                }               
            }
            else
            {
                PanError.Visible = true;
                litmessage.Text = "Login Failed";
            }
            //}
            //catch (Exception ex)
            //{
            //    lbltesting.Text = ex.Message;
            //    //Console.WriteLine(e.Message);
            //}
        }
        else
        {
            PanError.Visible = true;
            litmessage.Text = "No Projects Found";
        }
        }
        catch (Exception ex)
        {
            PanError.Visible = true;
            lbltesting.Text = ex.Message;
            //Console.WriteLine(e.Message);
        }


    }
}
