using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_company_promotracker : System.Web.UI.Page
{
    static DataView dv;
    static DataView dv2;
    protected static string Siteurl;
    protected DataTable rpttable;
    static int MaxAttribute=0;
    static int AddButtonDisable = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        HidePanels();
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        Siteurl = st.siteurl;
        if (!IsPostBack)
        {
            
            txtstart2.Text = DateTime.Now.AddHours(14).ToShortDateString();
            txtend2.Text = DateTime.Now.AddHours(14).ToShortDateString();
            
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            ddlSelectRecords2.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords2.DataBind();
            MaxAttribute = 0;
            BindGrid(0);
            // BindDropDown();
            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                //    tdExport.Visible = true;
            }
            else
            {
                //   tdExport.Visible = false;
            }
            if (Roles.IsUserInRole("Administrator"))
            {
                lnkjob.Visible = true;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
                GridView1.Columns[9].Visible = true;
                //GridView1.Columns[GridView1.Columns.Count - 2].Visible = true;
                tdEmployee.Visible = true;
                tdteam.Visible = true;

                tdEmployee2.Visible = true;
                tdteam2.Visible = true;
                //dvprojectstatus.Visible = true;
               // ddproject.Visible = true;
            }
            else
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
                GridView1.Columns[9].Visible = false;
                //GridView1.Columns[GridView1.Columns.Count - 2].Visible = false;
                tdEmployee.Visible = false;
                tdteam.Visible = false;

                tdEmployee2.Visible = false;
                tdteam2.Visible = false;
                if (Roles.IsUserInRole("Sales Manager"))
                {
                    tdEmployee.Visible = true;
                    tdEmployee2.Visible = true;
                }
                if (Roles.IsUserInRole("Administrator"))
                {
                    tdEmployee.Visible = false;
                    tdEmployee2.Visible = false;
                }
            }
            BindSearchDropdon();
        }

    }

    public void BindSearchDropdon()
    {
        //lstSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        //lstSearchStatus.DataBind();

        lstSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        lstSearchStatus.DataBind();

        ddlTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
        ddlTeam.DataMember = "SalesTeam";
        ddlTeam.DataTextField = "SalesTeam";
        ddlTeam.DataValueField = "SalesTeamID";
        ddlTeam.DataBind();

        ddlTeam2.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
        ddlTeam2.DataMember = "SalesTeam";
        ddlTeam2.DataTextField = "SalesTeam";
        ddlTeam2.DataValueField = "SalesTeamID";
        ddlTeam2.DataBind();

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
        {
            string SalesTeam = "";
            DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }


            if (SalesTeam != string.Empty)
            {
                ddlSearchEmployee.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlSearchEmployee.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }

        ddlSearchEmployee.DataMember = "fullname";
        ddlSearchEmployee.DataTextField = "fullname";
        ddlSearchEmployee.DataValueField = "EmployeeID";
        ddlSearchEmployee.DataBind();


        string userid2 = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st2 = ClstblEmployees.tblEmployees_SelectByUserId(userid2);
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("CompanyManager"))
        {
            string SalesTeam2 = "";
            DataTable dt_empsale2 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st2.EmployeeID);
            if (dt_empsale2.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale2.Rows)
                {
                    SalesTeam2 += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeam2 = SalesTeam2.Substring(0, SalesTeam2.Length - 1);
            }


            if (SalesTeam2 != string.Empty)
            {
                ddlSearchEmployee2.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam2);
            }
        }
        else
        {
            ddlSearchEmployee2.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }

        ddlSearchEmployee2.DataMember = "fullname";
        ddlSearchEmployee2.DataTextField = "fullname";
        ddlSearchEmployee2.DataValueField = "EmployeeID";
        ddlSearchEmployee2.DataBind();
    }
    protected void bindrepeater()
    {
        if (rpttable == null)
        {
            rpttable = new DataTable();
            rpttable.Columns.Add("MobileNo", Type.GetType("System.String"));
            rpttable.Columns.Add("Message", Type.GetType("System.String"));

        }

        for (int i = rpttable.Rows.Count; i < MaxAttribute; i++)
        {
            DataRow dr = rpttable.NewRow();

            dr["MobileNo"] = "";
             dr["Message"] = "";
           
            rpttable.Rows.Add(dr);
        }
        rptattribute.DataSource = rpttable;
        rptattribute.DataBind();

        //============Do not Delete Start
        int y = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
           //HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            ////if (hdntype1.Value == "1")
            ////{
            ////    item1.Visible = false;
            ////    y++;
            ////}
            int count = rptattribute.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
        //============Do not Delete End
    }
    protected void BindAddedAttribute()
    {
        rpttable = new DataTable();
       
        rpttable.Columns.Add("MobileNo", Type.GetType("System.String"));
        rpttable.Columns.Add("Message", Type.GetType("System.String"));

        foreach (RepeaterItem item in rptattribute.Items)
        {
            TextBox txtmodelno = (TextBox)item.FindControl("txtmodelno");
            TextBox TextBox2 = (TextBox)item.FindControl("TextBox2");
            DataRow dr = rpttable.NewRow();
            dr["MobileNo"] = txtmodelno.Text;
            dr["Message"] = TextBox2.Text;

            rpttable.Rows.Add(dr);
        }
    }

    protected void AddmoreAttribute()
    {
        MaxAttribute = MaxAttribute + 1;
        BindAddedAttribute();
        bindrepeater();
        ModalPopupExtender2.Show();
    }
    //protected DataTable GetGridData()
    //{

    //    string team1 = "";
    //    foreach (RepeaterItem item in lstSearchStatus.Items)
    //    {
    //        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
    //        HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

    //        if (chkselect.Checked == true)
    //        {
    //            team1 += "," + hdnID.Value.ToString();
    //        }
    //    }
    //    if (team1 != "")
    //    {
    //        team1 = team1.Substring(1);
    //    }

    //    DataTable dt = new DataTable();
    //    string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
    //    string curuserid = "", employee = "", team = "";
    //    if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("Administrator"))
    //    {
    //        curuserid = "";
    //        employee = ddlSearchEmployee.SelectedValue;
    //        if (Roles.IsUserInRole("Administrator"))
    //        {
    //            team = ddlTeam.SelectedValue;
    //        }
    //    }
    //    else
    //    {
    //        curuserid = userid;
    //        employee = "";
    //    }

    //    string historic = "1";
    //    if (chkhist.Checked.ToString() == "True")
    //    {
    //        historic = "0";
    //    }


    //    //Response.Write("'" + txtStartDate.Text + "','" + txtEndDate.Text + "','" + ddlinterested.SelectedValue + "','" + txtcontactname.Text + "','" + userid + "','" + curuserid + "','" + team + "','" + employee + "','" + historic + "','" + team1 + "'");
    //    //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>alert('" + selectedItem + "');</script>");
    //    dt = ClstblPromo.tblPromo_tracker(txtStartDate.Text, txtEndDate.Text, ddlinterested.SelectedValue, txtcontactname.Text, userid, curuserid, team, employee, historic, team1);

    //    if (dt.Rows.Count > 0)
    //    {
    //        PanGrid2.Visible = true;
    //     //   divnopage.Visible = true;
    //        PanNoRecord.Visible = false;
    // //       div1.Visible = true;
    //    }
    //    else
    //    {
    //        PanGrid2.Visible = false;
    // //       divnopage.Visible = false;
    //        SetNoRecords();
    //        //PanNoRecord.Visible = true;
    //   //     div1.Visible = false;
    //    }

    //    int iTotalRecords = dt.Rows.Count;
    //    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
    //    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
    //    if (iEndRecord > iTotalRecords)
    //    {
    //        iEndRecord = iTotalRecords;
    //    }
    //    if (iStartsRecods == 0)
    //    {
    //        iStartsRecods = 1;
    //    }
    //    if (iEndRecord == 0)
    //    {
    //        iEndRecord = iTotalRecords;
    //    }
    //    //ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";


    //    return dt;
    //}
    protected DataTable GetGridData2()
    {

        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }




        //Response.Write(team1);
        DataTable dt2 = new DataTable();
     
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        string curuserid = "", employee = "", team = "";
        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("Administrator"))
        {
            curuserid = "";
            employee = ddlSearchEmployee2.SelectedValue;
            if (Roles.IsUserInRole("Administrator"))
            {
                team = ddlTeam2.SelectedValue;
            }
        }
        else
        {
            curuserid = userid;
            employee = "";
        }
        //Response.Write("'" + userid + "','" + curuserid + "','" + ddlinterested2.SelectedValue + "','" + txtcontactname2.Text + "','" + team + "','" + employee + "'");
        dt2 = ClstblPromo.tblPromo_othertracker1(userid, curuserid, txtstart2.Text, txtend2.Text, ddlinterested2.SelectedValue, txtcontactname2.Text, team, employee, ddlHistoric.SelectedValue, selectedItem,TextBox1.Text,ddlselectdate.SelectedValue, ddlRead.SelectedValue,ddlPromotype.SelectedValue);
        // dt2 = ClstblPromo.tblPromo_othertracker(userid, curuserid, "", "", ddlinterested2.SelectedValue, txtcontactname2.Text, team, employee);
        //Response.Write(dt2.Rows.Count);
        //Response.End();
        if (dt2.Rows.Count > 0)
        {
            CountLeadData(dt2);
            PanGrid.Visible = true;
            //divnopage2.Visible = true;
            //PanNoRecord.Visible = true;
            divpromosms.Visible = true;
        }
        else
        {
            PanGrid.Visible = false;
            divpromosms.Visible = false;
            SetNoRecords();
            //divnopage2.Visible = false;
        }

        //Response.Write("etweathw");
        //Response.End();
        int iTotalRecords2 = dt2.Rows.Count;
        if (ddlSelectRecords2.SelectedItem.Text != "All")
        {
            int iEndRecord2 = GridView2.PageSize * (GridView2.PageIndex + 1);
            int iStartsRecods2 = (iEndRecord2 + 1) - GridView2.PageSize;
            if (iEndRecord2 > iTotalRecords2)
            {
                iEndRecord2 = iTotalRecords2;
            }
            if (iStartsRecods2 == 0)
            {
                iStartsRecods2 = 1;
            }
            if (iEndRecord2 == 0)
            {
                iEndRecord2 = iTotalRecords2;
            }
            //  ltrPage2.Text = "Showing " + iStartsRecods2 + " to " + iEndRecord2 + " of " + iTotalRecords2 + " entries";
        }

        return dt2;
    }
    public void CountLeadData(DataTable dt)
    {
        if (dt != null && dt.Rows.Count > 0)
        {
            int yes = 0;
            int no = 0;
            int Others = 0;
            panel.Visible = true;
            decimal nooflead = 0;
            decimal unhandeled = 0;
            decimal Sales = 0;
            decimal project = 0;
            decimal cancel = 0;
            decimal avg = 100;
            nooflead = Convert.ToDecimal(dt.Rows.Count.ToString());
            lblnooflead.Text = dt.Rows.Count.ToString();
            DataRow[] dr = dt.Select("leadstatusanme= 'Unhandled'");
            if (dr.Length > 0)
            {
                unhandeled = Convert.ToDecimal(dr.Length.ToString());
                decimal ans = (unhandeled / nooflead) * avg;
                decimal ans1 = Math.Round(ans, 2);
                litUnHandlCount.Text = dr.Length.ToString() + "/" + Convert.ToString(ans1);

            }
            else
            {
                litUnHandlCount.Text = "0";
            }
            DataRow[] drprjCount = dt.Select("ProjectStatusID=" + 2);
            if (drprjCount.Length > 0)
            {
                project = Convert.ToDecimal(drprjCount.Length.ToString());
                decimal ans = (project / nooflead) * avg;
                decimal ans1 = Math.Round(ans, 2);
                lbltotalpanel.Text = drprjCount.Length.ToString() + "/" + Convert.ToString(ans1);
            }
            else
            {
                lbltotalpanel.Text = "0";
            }
            //string qr = "3, 5, 8, 10, 19, 11, 12, 13, 14";

            // string qr = "3, 5, 8, 10, 19, 11, 12, 13, 14";
            //DataRow[] drprjCount1 = dt.Select("ProjectStatusID IN '" + qr + "'");
            DataRow[] drprjCount1 = dt.Select("ProjectStatusID  IN ('3','5','8','10','19','11','12','13','14')");
            if (drprjCount1.Length > 0)
            {
                Sales = Convert.ToDecimal(drprjCount1.Length.ToString());
                decimal ans = (Sales / nooflead) * avg;
                decimal ans1 = Math.Round(ans, 2);
                lblSale.Text = drprjCount1.Length.ToString() + "/" + Convert.ToString(ans1);
            }
            else
            {
                lblSale.Text = "0";
            }
            DataRow[] drCancelLead = dt.Select("Interested=1");
            if (drCancelLead.Length > 0)
            {
                //cancel = Convert.ToDecimal(drCancelLead.Length.ToString());

                lblyes.Text = drCancelLead.Length.ToString();

            }
            else
            {
                lblyes.Text = "0";
            }
            DataRow[] drCancelLead1 = dt.Select("Interested=2");
            if (drCancelLead.Length > 0)
            {
                //cancel = Convert.ToDecimal(drCancelLead1.Length.ToString());

                lblno.Text = drCancelLead1.Length.ToString();

            }
            else
            {
                lblno.Text = "0";
            }
            DataRow[] drCancelLead2 = dt.Select("Interested=3");
            if (drCancelLead2.Length > 0)
            {
                //cancel = Convert.ToDecimal(drCancelLead1.Length.ToString());

                lblothers.Text = drCancelLead2.Length.ToString();

            }
            else
            {
                lblothers.Text = "0";
            }
        }

        else
        {

            litUnHandlCount.Text = "0";
            lbltotalpanel.Text = "0";
            lblSale.Text = "0";
            //litCancel.Text = "0";
            //litUnHandlCount.Text = "0";

            //panel.Visible = false;
        }
    }
    public void BindGrid(int deleteFlag)
    {
        //PanGrid.Visible = true;
        //DataTable dt = GetGridData2();
        //dv = new DataView(dt);
        //if (dt.Rows.Count == 0)
        //{
        //    HidePanels();

        //    if (deleteFlag == 1)
        //    {
        //        //SetDelete();
        //    }
        //    else
        //    {
        //        SetNoRecords();
        //        //PanNoRecord.Visible = true;
        //    }
        //    //Response.Write("wetrrtgryy");
        //    //Response.End();
        //    PanGrid.Visible = false;
        //   // divnopage.Visible = false;
        //}
        //else
        //{
        //    GridView1.DataSource = dt;
        //    GridView1.DataBind();

        //    PanNoRecord.Visible = false;
        //    if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
        //    {
        //        if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
        //        {
        //            //========label Hide
        //            //divnopage.Visible = false;
        //        }
        //        else
        //        {
        //           // divnopage.Visible = true;
        //            int iTotalRecords = dv.ToTable().Rows.Count;
        //            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
        //            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
        //            if (iEndRecord > iTotalRecords)
        //            {
        //                iEndRecord = iTotalRecords;
        //            }
        //            if (iStartsRecods == 0)
        //            {
        //                iStartsRecods = 1;
        //            }
        //            if (iEndRecord == 0)
        //            {
        //                iEndRecord = iTotalRecords;
        //            }
        //          //  ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        //        }
        //    }
        //    else
        //    {
        //        if (ddlSelectRecords.SelectedValue == "All")
        //        {
        //            //divnopage.Visible = true;
        //            //ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
        //        }
        //    }
        //}

        PanGrid2.Visible = true;
        DataTable dt2 = GetGridData2();
        dv2 = new DataView(dt2);
        if (dt2.Rows.Count == 0)
        {
            //HidePanels();

            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                //PanNoRecord.Visible = true;
            }
            SetNoRecords();
            PanGrid2.Visible = false;
            divnopage2.Visible = false;
        }
        else
        {

            GridView2.DataSource = dt2;
            GridView2.DataBind();
           

            //PanNoRecord.Visible = false;
            
            if (dt2.Rows.Count > 0 && ddlSelectRecords2.SelectedValue != string.Empty && ddlSelectRecords2.SelectedValue != "All")
            {
                //Response.Write("iuityiut");
                //Response.End();
                if (Convert.ToInt32(ddlSelectRecords2.SelectedValue) < (dt2.Rows.Count))
                {
                    //========label Hide
                     divnopage2.Visible = false;
                }
                else
                {
                       divnopage2.Visible = true;
                    int iTotalRecords = dv2.ToTable().Rows.Count;
                    int iEndRecord = GridView2.PageSize * (GridView2.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView2.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage2.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                    
                }
            }
            else
            {
                if (ddlSelectRecords2.SelectedValue == "All")
                {
                  divnopage2.Visible = false;
                    ltrPage2.Text = "Showing " + dt2.Rows.Count + " entries";
                }
            }
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView2.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView2.AllowPaging = true;
            GridView2.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
        BindScript();
    }
    //protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    DataTable dt = new DataTable();
    //    dt = GetGridData();

    //    ////////////// Don't Change Start
    //    string SortDir = string.Empty;
    //    if (dir == SortDirection.Ascending)
    //    {
    //        dir = SortDirection.Descending;
    //        SortDir = "Desc";
    //    }
    //    else
    //    {
    //        dir = SortDirection.Ascending;
    //        SortDir = "Asc";
    //    }
    //    DataView sortedView = new DataView(dt);
    //    sortedView.Sort = e.SortExpression + " " + SortDir;
    //    //////////////////////End

    //    GridView1.DataSource = sortedView;
    //    GridView1.DataBind();
    //}
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    private void HidePanels()
    {
        PanNoRecord.Visible = false;
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        BindScript();
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    protected void btnClearAll_Click(object sender, EventArgs e)
    {

        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlSearchEmployee.SelectedValue = "";
        ddlTeam.SelectedValue = "";
        ddlinterested.SelectedValue = "0";
        txtcontactname.Text = string.Empty;
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
        BindGrid(0);
        BindScript();
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void btnClearAll2_Click(object sender, EventArgs e)
    {
        ddlinterested2.SelectedValue = "0";
        txtcontactname2.Text = string.Empty;
        ddlSearchEmployee2.SelectedValue = "";
        ddlTeam2.SelectedValue = "";
        txtstart2.Text = string.Empty;
        txtend2.Text = string.Empty;
        //ddlSearchEmployee.SelectedValue = "";


        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            chkselect.Checked = false;
        }
        BindGrid(0);
        BindScript();
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        //DataTable dt = ClstblProjects.tblProjects_SelectSTCTrackerSearch(txtContactSearch.Text, txtProjectNumber.Text, txtMQNsearch.Text, ddlSTCCheckedBy.SelectedValue, txtSerachCity.Text, ddlSearchState.SelectedValue, txtSearchPostCode.Text, txtSTCApprovalNo.Text, txtSTCUpload.Text, ddlSTCStatus.SelectedValue, ddlSearchDate.SelectedValue, txtStartDate.Text, txtEndDate.Text, Convert.ToString(chkNoPVD.Checked));
        //string team1 = "";
        //foreach (RepeaterItem item in lstSearchStatus.Items)
        //{
        //    CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
        //    HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

        //    if (chkselect.Checked == true)
        //    {
        //        team1 += "," + hdnID.Value.ToString();
        //    }
        //}
        //if (team1 != "")
        //{
        //    team1 = team1.Substring(1);
        //}

        //DataTable dt = new DataTable();
        //string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        //string curuserid = "", employee = "", team = "";
        //if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("Administrator"))
        //{
        //    curuserid = "";
        //    employee = ddlSearchEmployee.SelectedValue;
        //    if (Roles.IsUserInRole("Administrator"))
        //    {
        //        team = ddlTeam.SelectedValue;
        //    }
        //}
        //else
        //{
        //    curuserid = userid;
        //    employee = "";
        //}

        //string historic = "1";
        //if (chkhist.Checked.ToString() == "True")
        //{
        //    historic = "0";
        //}
        //dt = ClstblPromo.tblPromo_tracker(txtStartDate.Text, txtEndDate.Text, ddlinterested.SelectedValue, txtcontactname.Text, userid, curuserid, team, employee, historic, team1);
        DataTable dt = GetGridData2();
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "Promo_Tracker" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList ={612,604,269,600,611,615,257,4,12,37,112,377,619,620};
            string[] arrHeader = { "Sales Rep","SalesTeam", "ProjectNumber", "ProjectStatus", "LeadStatus", "Contact", "Promo", "Promo-Sent", "Interested", "Mobile","Phone","Deposit Rec","PromoType","Interested" };

            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                    .Select(x => x.ColumnName)
                                    .ToArray();
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        //GridView2.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
        // BindScript();
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView2.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt2 = new DataTable();
        dt2 = GetGridData2();

        ////////////// Don't Change Start
        string SortDir2 = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir2 = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir2 = "Asc";
        }
        DataView sortedView2 = new DataView(dt2);
        sortedView2.Sort = e.SortExpression + " " + SortDir2;
        //////////////////////End

        GridView2.DataSource = sortedView2;
        GridView2.DataBind();
    }

    protected void  GridView2_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow2 = GridView2.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow2.Cells[0].FindControl("CurrentPage");
        if (lblcurrentpage != null)
        {
            lblcurrentpage.Text = Convert.ToString(GridView2.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView2.PageIndex - 2;
            page[1] = GridView2.PageIndex - 1;
            page[2] = GridView2.PageIndex;
            page[3] = GridView2.PageIndex + 1;
            page[4] = GridView2.PageIndex + 2;
            page[5] = GridView2.PageIndex + 3;
            page[6] = GridView2.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView2.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow2.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow2.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }
            if (GridView2.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow2.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow2.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView2.PageIndex == GridView2.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow2.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow2.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }
            Label ltrPage2 = (Label)gvrow2.Cells[0].FindControl("ltrPage2");
            if (dv2.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv2.ToTable().Rows.Count;
                int iEndRecord = GridView2.PageSize * (GridView2.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView2.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                ltrPage2.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage2.Text = "";
                //Response.Write("twretwrgrwywy0");
                //Response.End()
            }
        }
    }
    void lb_Command2(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }
    //protected void GridView2_RowCreated(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.Pager)
    //    {
    //        GridViewRow gvr2 = e.Row;
    //        LinkButton lb = (LinkButton)gvr2.Cells[0].FindControl("p0");
    //        lb.Command += new CommandEventHandler(lb_Command2);
    //        lb = (LinkButton)gvr2.Cells[0].FindControl("p1");
    //        lb.Command += new CommandEventHandler(lb_Command2);
    //        lb = (LinkButton)gvr2.Cells[0].FindControl("p2");
    //        lb.Command += new CommandEventHandler(lb_Command2);
    //        lb = (LinkButton)gvr2.Cells[0].FindControl("p4");
    //        lb.Command += new CommandEventHandler(lb_Command2);
    //        lb = (LinkButton)gvr2.Cells[0].FindControl("p5");
    //        lb.Command += new CommandEventHandler(lb_Command2);
    //        lb = (LinkButton)gvr2.Cells[0].FindControl("p6");
    //        lb.Command += new CommandEventHandler(lb_Command2);
    //    }
    //}

    protected void chkpromo_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkpromo = (CheckBox)GridView2.HeaderRow.FindControl("chkpromo");
        foreach (GridViewRow row in GridView2.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chktagpromo = (CheckBox)row.FindControl("chktagpromo");

                if (chkpromo.Checked == true)
                {
                    chktagpromo.Checked = true;
                }
                else
                {
                    chktagpromo.Checked = false;
                }
            }
        }
    }
    protected void btnpromooffer_Click(object sender, EventArgs e)
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        int rowsCount = GridView2.Rows.Count;
        GridViewRow gridRow;
        int success = 0;
        for (int i = 0; i < rowsCount; i++)
        {
            string id;
            gridRow = GridView2.Rows[i];
            id = GridView2.DataKeys[i].Value.ToString();

            CheckBox chktagpromo = (CheckBox)gridRow.FindControl("chktagpromo");
            HiddenField hndid = (HiddenField)gridRow.FindControl("hndid");
            if (ddlpromooffer.SelectedValue != string.Empty)
            {
                if (chktagpromo.Checked)
                {
                    if (hndid.Value != string.Empty)
                    {

                        SttblPromo stpromo = ClstblPromo.tblPromo_SelectByPromoID(hndid.Value);
                        SttblContacts stcon = ClstblContacts.tblContacts_SelectByContactID(stpromo.ContactID);
                        ClstblPromo.tblPromo_UpdateInterested(stcon.ContMobile, ddlpromooffer.SelectedValue.ToString());
                        ClstblPromo.tblPromo_UpdatePromoUser(hndid.Value, userid);
                        ClstblPromo.tblPromo_UpdateReadFlag(hndid.Value, "2");
                        ClstblPromo.tblPromo_UpdateIsArchivebyPromoID(hndid.Value, true.ToString());
                    }
                }
            }
        }
        BindGrid(0);
        BindScript();
    }

    protected void chkreadhead_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkreadhead = (CheckBox)GridView2.HeaderRow.FindControl("chkreadhead");
        foreach (GridViewRow row in GridView2.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkread = (CheckBox)row.FindControl("chkread");

                if (chkreadhead.Checked == true)
                {
                    chkread.Checked = true;
                }
                else
                {
                    chkread.Checked = false;
                }
            }
        }
    }
    protected void btnread_Click(object sender, EventArgs e)
    {

        int rowsCount = GridView2.Rows.Count;
        GridViewRow gridRow;
        for (int i = 0; i < rowsCount; i++)
        {
            string id;
            gridRow = GridView2.Rows[i];
            id = GridView2.DataKeys[i].Value.ToString();

            CheckBox chkread = (CheckBox)gridRow.FindControl("chkread");
            HiddenField hndid = (HiddenField)gridRow.FindControl("hndid");

            if (chkread.Checked)
            {
                if (hndid.Value != string.Empty)
                {
                    SttblPromo stpromo = ClstblPromo.tblPromo_SelectByPromoID(hndid.Value);
                    ClstblPromo.tblPromo_UpdateReadFlag(hndid.Value, "2");
                    ClstblPromo.tblPromo_UpdateIsArchivebyPromoID(hndid.Value, true.ToString());
                }
            }
        }
        BindGrid(0);
        BindScript();
    }

    protected void chksmshead_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chksmshead = (CheckBox)GridView1.HeaderRow.FindControl("chksmshead");
        foreach (GridViewRow row in GridView1.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chksms = (CheckBox)row.FindControl("chksms");

                if (chksmshead.Checked == true)
                {
                    chksms.Checked = true;
                }
                else
                {
                    chksms.Checked = false;
                }
            }
        }
    }
    protected void btnstopsms_Click(object sender, EventArgs e)
    {

        int rowsCount = GridView1.Rows.Count;
        GridViewRow gridRow;
        int success = 0;
        for (int i = 0; i < rowsCount; i++)
        {
            string id;
            gridRow = GridView1.Rows[i];
            id = GridView1.DataKeys[i].Value.ToString();

            CheckBox chksms = (CheckBox)gridRow.FindControl("chksms");
            HiddenField hndid = (HiddenField)gridRow.FindControl("hndid");

            if (chksms.Checked)
            {
                if (hndid.Value != string.Empty)
                {
                    SttblPromo stpromo = ClstblPromo.tblPromo_SelectByPromoID(hndid.Value);
                    string contactid = stpromo.ContactID;
                    ClstblContacts.tblContacts_UpdateSendSMS(contactid, false.ToString());
                }
            }
        }
        BindGrid(0);
        BindScript();
    }
    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }
    protected void ddlSelectRecords2_SelectedIndexChanged(object sender, EventArgs e)
    {
       if (Convert.ToString(ddlSelectRecords2.SelectedValue) == "All")
        {
            GridView2.AllowPaging = false;
            // custompagesize = 0;
        }
        else if (Convert.ToString(ddlSelectRecords2.SelectedValue) == "")
        {
            GridView2.AllowPaging = false;
        }
        else
        {
            GridView2.AllowPaging = true;
            GridView2.PageSize = Convert.ToInt32(ddlSelectRecords2.SelectedValue);
            BindGrid(0);
        }
        BindGrid(0);
        BindScript();
    }
    protected void btnSearch2_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        BindScript();
    }
    protected void gridclearbtn_Click(object sender, ImageClickEventArgs e)
    {
        ddlinterested2.SelectedValue = "0";
        txtcontactname2.Text = string.Empty;
        ddlSearchEmployee2.SelectedValue = "";
        ddlTeam2.SelectedValue = "";
        BindGrid(0);
        BindScript();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hndCustomerID = (HiddenField)e.Row.FindControl("hndCustomerID");
            Label lblFollowUpDate = (Label)e.Row.FindControl("lblFollowUpDate");
            DataTable dtFollwUp = ClstblCustInfo.tblCustInfo_SelectTop1ByCustID(hndCustomerID.Value);
            if (dtFollwUp.Rows.Count > 0)
            {
                try
                {
                    lblFollowUpDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(dtFollwUp.Rows[0]["NextFollowupDate"].ToString()));
                }
                catch { }
            }
        }
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        ddlSearchEmployee.SelectedValue = "";
        ddlTeam.SelectedValue = "";
        ddlinterested.SelectedValue = "0";
        txtcontactname.Text = string.Empty;

        BindGrid(0);
        BindScript();
    }
    protected void GridView2_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr2 = e.Row;
            LinkButton lb = (LinkButton)gvr2.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command2);
            lb = (LinkButton)gvr2.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command2);
            lb = (LinkButton)gvr2.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command2);
            lb = (LinkButton)gvr2.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command2);
            lb = (LinkButton)gvr2.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command2);
            lb = (LinkButton)gvr2.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command2);
        }
    }


    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }
    public void SetNoData()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunNoNull();", true);
    }
    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    protected void lstSearchStatus_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        HiddenField hdnID = (HiddenField)item.FindControl("hdnID");
        Literal ltprojstatus = (Literal)item.FindControl("ltprojstatus");
        CheckBox chkselect = (CheckBox)item.FindControl("chkselect");


        if (hdnID.Value == "2")
        {
            e.Item.Visible = false;
        }

    }
    protected void lnkjob_Click(object sender, EventArgs e)
    {
        //LinkButton lnkReceived = (LinkButton)sender;
        //GridViewRow item1 = lnkReceived.NamingContainer as GridViewRow;
        //HiddenField hdnProjectID = (HiddenField)item1.FindControl("hdnProjectID");
        //HiddenField hdncust = (HiddenField)item1.FindControl("hdncust");
        // hdn_custpopup.Value = hdncust.Value;
        //Response.Write(hdnProjectID.Value);
        //Response.End();
        //if (hdnProjectID.Value != string.Empty)
        //{

        //    SttblProjects stpro = ClstblProjects.tblProjects_SelectByProjectID(hdnProjectID.Value);

        txtpromo.Text = string.Empty;
        txtpromosent.Text = string.Empty;
        ModalPopupExtenderEdit.Show();
        divEdit.Visible = true;
        //hdnid.Value = hdnProjectID.Value;
        //GetSTCByProject(hdnProjectID.Value);


        //}
        //else
        //{
        //div_popup.Visible = false;
        //ModalPopupExtender1.Hide();


        //}
        //BindGrid(0);

    }

    protected void btnupdate_Click(object sender, EventArgs e)
    {

        int suc = ClstblPromo.tblPromo_UpdateData(txtpromo.Text, txtpromosent.Text, DropDownList1.SelectedValue);
        //Response.Write(suc);
        //Response.End();
        if (suc > 0)
        {
            SetAdd1();
        }
        else
        {
            if (suc == 0)
            {
                SetNoData();
            }
            else
            {
                SetError1();
            }
        }
        BindGrid(0);
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        
        ModalPopupExtenderEdit.Hide();
        divEdit.Visible = true;

    }



    protected void lnkUpdateQuery_Click(object sender, EventArgs e)
    {
        int suc = ClstblPromo.tblPromo_UpdateQuery();
        //Response.Write(suc);
        //Response.End();
        if (suc > 0)
        {
            SetAdd1();
        }
        else
        {
            if (suc == 0)
            {
                SetNoData();
            }
            else
            {
                SetError1();
            }
        }

    }

    protected void btnGetData_Click(object sender, EventArgs e)
    {
        // string StartDate = Convert.ToString();
        try
        {
            DateTime dtStart = Convert.ToDateTime(txtstartdate1.Text);
            DateTime dtend = Convert.ToDateTime(txtenddate1.Text);
            string StartDate = dtStart.ToString("MM/dd/yyyy");
            string EndDate = dtend.ToString("MM/dd/yyyy");
            ///  string EndDate = Convert.ToString(txtenddate1.Text);
            string StateId = "";
            string SalesTeamID = "";
            if (ddlSearchState.Items.Count > 0)//for save selected item Value
            {
                for (int i = 0; i < ddlSearchState.Items.Count; i++)
                {
                    if (ddlSearchState.Items[i].Selected)
                    {
                        StateId += ddlSearchState.Items[i].Text + ",";
                    }
                }
            }

            if (ddlSalesTeamID.Items.Count > 0)//for save selected item Value
            {
                for (int i = 0; i < ddlSalesTeamID.Items.Count; i++)
                {
                    if (ddlSalesTeamID.Items[i].Selected)
                    {
                        SalesTeamID += ddlSalesTeamID.Items[i].Value + ",";

                    }
                }
            }
            if (SalesTeamID != "")
            {
                SalesTeamID = SalesTeamID.TrimEnd(',');
            }
            string StateList = "";
            if (StateId != "")
            {
                string[] St = StateId.Split(',');

                if (St.Length > 0)
                {

                    // string s1 = "ABC";
                    //string test = "('" + s1 + "')";
                    for (int s = 0; s < St.Length - 1; s++)
                    {
                      
                        StateList += "'" + St[s] + "'" + ",";
                    }
                }
                StateList = StateList.TrimEnd(',');
            }

            string sql = "SELECT tblContacts.ContMobile FROM tblContacts INNER JOIN tblCustomers ON tblContacts.CustomerID = tblCustomers.CustomerID " +
                " WHERE(((tblContacts.ContMobile)Is Not Null) AND((tblContacts.SendSMS) = 1) AND((tblContacts.SendEmail) = 1) " +
                " AND((tblCustomers.CustTypeID) = 1) and((tblContacts.ContLeadStatusID)  " +
                "   in (2)) and((tblCustomers.StreetState)In(" + StateList + ")) " +
                " AND((tblCustomers.CustEntered)Between '" + StartDate + "' and '" + EndDate + "')) " +
                "and tblCustomers.employeeid in (select employeeid from tblemployees where " +
                " salesteamid in (" + SalesTeamID + ") " +
                "union all " +
                "SELECT tblContacts.ContMobile FROM " +
                " tblContacts INNER JOIN tblProjects " +
                "ON tblContacts.ContactID = tblProjects.ContactID WHERE(((tblContacts.SendSMS) = 1) " +
                " AND((tblContacts.SendEmail) = 1) AND((tblProjects.ProjectStatusID) in (2, 6)) AND((tblProjects.InstallState) " +
                "In(" + StateList + ")) AND((tblContacts.ContactEntered) " +
                "Between '" + StartDate + "' and '" + EndDate + "')) " +
                "and tblProjects.employeeid in (select employeeid from tblemployees where salesteamid in (" + SalesTeamID + "))) ";

            DataTable dt = ClstblCustomers.query_execute(sql);
            Response.Clear();
            int count = dt.Rows.Count;
            //DataTable dt = ClstblPromo.tblPromo_GetUpdtaedData(StartDate, EndDate, StateId, SalesTeamID);
            if (dt != null && dt.Rows.Count > 0)
            {
                try
                {
                    Export oExport = new Export();
                    string FileName = "PromoData" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";

                    int[] ColList = { 0 };
                    string[] arrHeader = { "ContactMobile" };
                    oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
                }
                catch (Exception ex)
                {
                }
            }
            else
            {
                SetNoRecords();
                //SetNoData();
            }
        }
        catch (Exception ex)
        {

        }
    }

    protected void LinkButton6_Click(object sender, EventArgs e)
    {

    }
    
    protected void lnkExportData_Click(object sender, EventArgs e)
    {
        ModalPopupExtender1.Show();
        div3.Visible = true;
        txtstartdate1.Text = string.Empty;
        txtenddate1.Text = string.Empty;
        DataTable dtTeam = ClstblSalesTeams.tblSalesTeams_SelectASC();
        ddlSalesTeamID.DataSource = dtTeam;
        ddlSalesTeamID.DataValueField = "SalesTeamID";
        ddlSalesTeamID.DataTextField = "SalesTeam";
        ddlSalesTeamID.DataMember = "SalesTeam";
        ddlSalesTeamID.DataBind();

        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();
    }

    protected void lnkEmailReply_Click(object sender, EventArgs e)
    {
       // MaxAttribute = 0;
        AddmoreAttribute();
        ModalPopupExtender2.Show();
       
    }
    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*])", RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);
        return Convert.ToInt32(match.Value);
    }
    

public bool IsNumeric(string str)
    {
        return str.All(c => "0123456789".Contains(c));
    }
    protected void btnEmailReply_Click(object sender, EventArgs e)
    {
       // string msgText = TextBox2.Text;s
        int suc = 0;
        foreach (RepeaterItem item in rptattribute.Items)
        {
            TextBox txtmodelno = (TextBox)item.FindControl("txtmodelno");
            TextBox TextBox2 = (TextBox)item.FindControl("TextBox2");
            if(txtmodelno.Text !=null && txtmodelno.Text !="")
            {
                string message = TextBox2.Text;
                string status = "3";
                if (message.ToLower().ToString().Contains("yes") || message.ToString().Contains("yes"))//1
                {
                    status = "1";
                }
                else if (message.ToLower().ToString().Contains("stop") || message.ToString().Contains("Stop") || message.ToString().Contains("STOP"))//2
                {
                    status = "2";
                   // ltmsg.Text = "";
                }
                bool s1 = IsNumeric(txtmodelno.Text);
                if(s1)
                {
                    string MobileNo = "0" + txtmodelno.Text;
                    DataTable dt = ClstblPromo.GetCOntactID(MobileNo);
                    if(dt.Rows.Count > 0)
                    {
                        suc=ClstblPromo.tblPromo_Insert(dt.Rows[0]["ContactID"].ToString(),"", "1","", status, "", "");
                        if(suc > 0)
                        {
                            bool d1 = ClstblPromo.tblPromo_UpdateArcResponseMsg(suc.ToString(), message);
                        }
                    }
                }
                else
                {
                    DataTable dt = ClstblPromo.GetCOntactID(txtmodelno.Text);
                    if (dt.Rows.Count > 0)
                    {
                        suc = ClstblPromo.tblPromo_Insert(dt.Rows[0]["ContactID"].ToString(), "0", "1", "", status, "", "");
                        if (suc > 0)
                        {
                            bool d1 = ClstblPromo.tblPromo_UpdateArcResponseMsg(suc.ToString(), message);
                        }
                    }
                }
                   

            }
           // suc = ClstblPromo.tblPromoLog_InsertWithPromoType(txtmodelno.Text, msgText);
        }
        if(suc> 0)
        {
            MaxAttribute = 0;
            //TextBox2.Text = "";
            SetAdd1();
        }
        else
        {
            SetError1();
        }
    }

    protected void litremove_Click(object sender, EventArgs e)
    {
        int index = GetControlIndex(((Button)sender).ClientID);

        RepeaterItem item = rptattribute.Items[index];

        HiddenField hdntype = (HiddenField)item.FindControl("hdntype");
       

        hdntype.Value = "1";
        int y = 0;
        foreach (RepeaterItem item1 in rptattribute.Items)
        {
            HiddenField hdntype1 = (HiddenField)item1.FindControl("hdntype");
            Button lbremove1 = (Button)item1.FindControl("litremove");
            if (hdntype1.Value == "1")
            {
                item1.Visible = false;
                y++;
            }
            int count = rptattribute.Items.Count - y;
            lbremove1.Visible = true;
            if (count < 2)
            {
                foreach (RepeaterItem item2 in rptattribute.Items)
                {
                    Button lbremove = (Button)item2.FindControl("litremove");
                    lbremove.Visible = false;
                }
            }
        }
        ModalPopupExtender2.Show();
    }


    protected void rptattribute_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem item = e.Item;
        ImageButton btnDelete = (ImageButton)item.FindControl("btnDelete");
        CheckBox chkdelete = (CheckBox)item.FindControl("chkdelete");
        Button litremove = (Button)item.FindControl("litremove");
    }

    protected void btnaddnew_Click(object sender, EventArgs e)
    {
        AddmoreAttribute();
        //ModeAddUpdate();
        //lnkAdd.Visible = false;
    }

    protected void lnkFetchReceivedSMS_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtstart2.Text) && !string.IsNullOrEmpty(txtend2.Text))
        {
            //DateTime dtStart = Convert.ToDateTime(txtstart2.Text);
            //DateTime dtend = Convert.ToDateTime(txtend2.Text);

            int FetchCount = ClsSMS.GetReceivedSMS(txtstart2.Text, txtend2.Text);

            //DataTable dt = ClstblPromo.tblPromo_GetContact(fromnumber);
            //if (dt.Rows.Count > 0)
            //{
            //    ClstblPromo.tblPromo_Insert(dt.Rows[0]["ContactID"].ToString(), "0", "", "", "3", "", "");
            //    ClstblPromo.tblPromo_UpdateInterested(fromnumber, status);
            //    ClstblPromo.tblPromo_UpdateResponseMsg(fromnumber, message);
            //}

            if (FetchCount > 0)
            {
                SetAdd1();
            }
        }
        else
        {
            SetError1();
            //txtstartdate1.Focus();
            //txtend2.Focus();
        }
    }

    protected void lnkSendSMS_Click(object sender, EventArgs e)
    {
        txtMobileNo.Text = string.Empty;
        txtMessage.Text = string.Empty;
        ModalPopupExtenderSendSMS.Show();
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        string Number = txtMobileNo.Text;

        string [] MobileNo = Number.Split(',');

        string To = "";
        string MsgText = txtMessage.Text;
        int MsgCount = 0;

        for (int i = 0; i < MobileNo.Length; i++)
        {
            To = ClsSMS.GetNumberWithCountyCode(MobileNo[i].ToString(), "91");
        }
    }
}