﻿using System;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Text.RegularExpressions;

public partial class admin_adminfiles_company_Customer : System.Web.UI.Page
{
    protected static string Siteurl;
    public DataView dv;
    protected static string SortDir;
    protected static string SortFlag = "unsorted";
    protected static string address;
    protected static bool streetname;
    string foldername = "customerimage";
    protected static string postaladdress;
    private Image objPrintImage;
    private FrameDimension objDimension;
    protected static int custompagesize = Convert.ToInt32(SiteConfiguration.GetPageSize());

    protected void Page_Load(object sender, EventArgs e)
    {
        HidePanels();
        if (!IsPostBack)
        {
            BindDropDown();
            BindGrid(0);

            ddlCustSourceID.SelectedValue = "1";

            if(Request.QueryString["Mode"] == "AddCustomerFormLeadTracker")
            {
                DirectAddFromLeadTracker();
            }

            if(Roles.IsUserInRole("Administrator"))
            {
                lbtnExport.Visible = true;
            }
        }
    }

    

    public void BindDropDown()
    {
        DataTable dtType = ClstblCustType.tblCustType_SelectActive_COmpany();
        ddlSearchType.DataSource = dtType;
        ddlSearchType.DataTextField = "CustType";
        ddlSearchType.DataValueField = "CustTypeID";
        ddlSearchType.DataBind();

        ddlCustTypeID.DataSource = dtType;
        ddlCustTypeID.DataTextField = "CustType";
        ddlCustTypeID.DataValueField = "CustTypeID";
        ddlCustTypeID.DataBind();

        ddlCustSourceSubID.DataSource = ClstblCustSourceSub.tblCustSourceSub_Select();
        ddlCustSourceSubID.DataMember = "CustSourceSub";
        ddlCustSourceSubID.DataTextField = "CustSourceSub";
        ddlCustSourceSubID.DataValueField = "CustSourceSubID";
        ddlCustSourceSubID.DataBind();

        ddlformbayunittype.DataSource = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
        ddlformbayunittype.DataMember = "UnitType";
        ddlformbayunittype.DataTextField = "UnitType";
        ddlformbayunittype.DataValueField = "UnitType";
        ddlformbayunittype.DataBind();

        ddlCustSourceID.DataSource = ClstblCustSource.tblCustSource_SelectbyAsc();
        ddlCustSourceID.DataMember = "CustSource";
        ddlCustSourceID.DataTextField = "CustSource";
        ddlCustSourceID.DataValueField = "CustSourceID";
        ddlCustSourceID.DataBind();

        ddlformbaystreettype.DataSource = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
        ddlformbaystreettype.DataMember = "StreetType";
        ddlformbaystreettype.DataTextField = "StreetType";
        ddlformbaystreettype.DataValueField = "StreetCode";
        ddlformbaystreettype.DataBind();

        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();
    }

    protected DataTable GetGridData()
    {
        string UserID = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(UserID);
        string EmployeeID = st_emp.EmployeeID != "" ? st_emp.EmployeeID : "0";
        EmployeeID = EmployeeID == null ? "" : EmployeeID;

        string RoleName = "";
        string[] Role = Roles.GetRolesForUser();

        for (int i = 0; i < Role.Length; i++)
        {
            if (Role[i] != "crm_Customer")
            {
                RoleName = Role[i].ToString();
            }
        }

        DataTable dt = ClstblSalesinfo.tblCustomer_GetData(ddlSearchType.SelectedValue, ddlSearchRec.SelectedValue, ddlSearchArea.SelectedValue, txtSearch.Text, txtSearchStreet.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtSearchPostCode.Text, txtStartDate.Text, txtEndDate.Text, RoleName, EmployeeID);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        PanGrid.Visible = true;
        PanSearch.Visible = true;
        DataTable dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            HidePanels();
            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                //     PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
            ltrPage.Text = "";
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();

            //   divnopage.Visible = true;
            //if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            //{
            //    if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
            //    {
            //        //========label Hide
            //        divnopage.Visible = false;
            //    }
            //    else
            //    {
            //        divnopage.Visible = true;
            //        int iTotalRecords = dv.ToTable().Rows.Count;
            //        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            //        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            //        if (iEndRecord > iTotalRecords)
            //        {
            //            iEndRecord = iTotalRecords;
            //        }
            //        if (iStartsRecods == 0)
            //        {
            //            iStartsRecods = 1;
            //        }
            //        if (iEndRecord == 0)
            //        {
            //            iEndRecord = iTotalRecords;
            //        }
            //        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            //    }
            //}
            //else
            //{
            //    if (ddlSelectRecords.SelectedValue == "All")
            //    {
            //        divnopage.Visible = true;
            //        ltrPage.Text = "Showing " + (dt.Rows.Count) + " entries";
            //    }
            //}
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        streetaddress();
        string StreetCity = ddlStreetCity.Text;
        string StreetState = txtStreetState.Text;
        string StreetPostCode = txtStreetPostCode.Text;

        string PostalAddress = txtStreetAddress.Text;
        string PostalCity = ddlStreetCity.Text;
        string PostalState = txtStreetState.Text;
        string PostalPostCode = txtStreetPostCode.Text;

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        //Response.Write(userid);
        //Response.End();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = st.EmployeeID;

        string CustTypeID = ddlCustTypeID.SelectedValue;
        string CustSourceID = ddlCustSourceID.SelectedValue;
        string CustSourceSubID = ddlCustSourceSubID.SelectedValue;
        string ResCom = false.ToString();
        if (rblResCom1.Checked == true)
        {
            ResCom = "1";
        }
        if (rblResCom2.Checked == true)
        {
            ResCom = "2";
        }
        string CustEnteredBy = st.EmployeeID;
        string Customer = txtCompany.Text;
        string StreetAddress = txtStreetAddress.Text;
        //string StreetCity = ddlStreetCity.Text;
        //string StreetState = txtStreetState.Text;
        //string StreetPostCode = txtStreetPostCode.Text;

        string Country = "Australia";
        string CustPhone = txtCustPhone.Text;
        string CustAltPhone = txtCustAltPhone.Text;
        string CustFax = txtCustFax.Text;
        string CustNotes = txtCustNotes.Text;
        string CustWebSite = txtCustWebSiteLink.Text;
        string CustWebSiteLink = txtCustWebSiteLink.Text;
        string BranchLocation = txtStreetState.Text + " - " + ddlStreetCity.Text;
        string Area = false.ToString();
        int success = 0;
        if (rblArea.SelectedItem.Value == "1")
        {
            Area = "1";
        }
        if (rblArea.SelectedItem.Value == "2")
        {
            Area = "2";
        }
        //int exist = ClstblCustomers.tblCustomers_Exits_Address(hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, StreetCity, StreetState, StreetPostCode);
        //if (txtstreetaddressline.Text.Trim() != string.Empty)//chkformbayid.Checked.ToString() == true.ToString()
        //{
        //Response.Write(txtstreetaddressline.Text + "=" + StreetCity + "=" + StreetState + "=" + StreetPostCode);
        //Response.End();

        txtStreetAddress.Text = ddlformbayunittype.SelectedValue.Trim() + " " + txtformbayUnitNo.Text.Trim() + " " + txtformbayStreetNo.Text.Trim() + " " + txtformbaystreetname.Text.Trim() + " " + ddlformbaystreettype.SelectedValue.Trim();

        DataTable dt1 = ClstblCustomers.tblCustomers_Exits_SelectCustomer2(txtStreetAddress.Text, StreetCity.Trim(), StreetState.Trim(), StreetPostCode.Trim(), txtContMobile.Text.Trim(), txtContEmail.Text.Trim());
        //DataTable dt1 = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);

        if (dt1.Rows.Count > 0)
        {
            PanSearch.Visible = false;
            ModalPopupExtenderAddress.Show();
            PanAddUpdate.Visible = true;
            DataTable dt = ClstblCustomers.tblCustomers_Exits_SelectCustomer2(txtStreetAddress.Text, StreetCity.Trim(), StreetState.Trim(), StreetPostCode.Trim(), txtContMobile.Text.Trim(), txtContEmail.Text.Trim());
            //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
            rptaddress.DataSource = dt;
            rptaddress.DataBind();
            txtstreetaddressline.Focus();
        }
        else
        {

            //------------Existence of Street Address----------//

            if (chkformbayid.Checked.ToString() == false.ToString())
            {

                //int existaddress = ClstblCustomers.tblCustomers_ExitsStreet_address(txtstreetaddressline.Text);
                int existaddress = ClstblCustomers.tblCustomers_Exits_Address(hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, StreetCity, StreetState, StreetPostCode);
                if (existaddress != 1)
                {
                    success = ClstblCustomers.tblCustomers_Insert("", EmployeeID, "False", CustTypeID, CustSourceID, CustSourceSubID, "", "", ResCom, "", "", "", "", "", "", Customer, BranchLocation, "", StreetAddress, StreetCity, StreetState, StreetPostCode, PostalAddress, PostalCity, PostalState, PostalPostCode, Country, CustPhone, "", "", CustAltPhone, CustFax, CustNotes, "", "", CustWebSiteLink, "", "False", "", "", "", "", "", "", "", Area);
                    int sucContacts = ClstblCustomers.tblCustomers_InsertContacts(Convert.ToString(success), txtContMr.Text.Trim(), txtContFirst.Text.Trim(), txtContLast.Text.Trim(), txtContMobile.Text, txtContEmail.Text, EmployeeID, CustEnteredBy);
                    // New Log in Lead History
                    DataTable dt = ClstblSalesinfo.tblCustomer_GetDataByIDforAssignLead(Convert.ToString(success));
                    string EmpID = dt.Rows[0]["EmployeeID"].ToString();
                    string CustSource = dt.Rows[0]["CustSource"].ToString();
                    string CustSourceSub = dt.Rows[0]["CustSourceSub"].ToString();
                    string CustEntered = dt.Rows[0]["CustEntered"].ToString();
                    string RoleName = dt.Rows[0]["RoleName"].ToString();
                    string Msg = "Assign New Lead";
                    int Uns1 = ClsLead.tblLeadHistory_Insert(Convert.ToString(success), "New", CustSource, CustSourceSub, Msg, EmpID, CustEntered, userid);
                    //Response.Write("Convert.ToString(success)" + Convert.ToString(success) + "Customer Entered on" + Customer Entered on + "DateTime.Now.AddHours(14).ToShortDateString()" + DateTime.Now.AddHours(14).ToShortDateString() + "DateTime.Now.AddHours(14).ToString()" + DateTime.Now.AddHours(14).ToString() + "Convert.ToString(sucContacts)" + Convert.ToString(sucContacts) + "CustEnteredBy" + CustEnteredBy + "1"  + 1);
                    //Response.End();

                    //Response.Write(Convert.ToString(success) + "<br>" + "Customer Entered on " + DateTime.Now.AddHours(14).ToShortDateString() + "<br>" + DateTime.Now.AddHours(14).ToString() + "<br>" + Convert.ToString(sucContacts) + "<br>" + CustEnteredBy + "<br>" + "1");
                    //Response.End();
                    int sucCustInfo = ClstblCustInfo.tblCustInfo_Insert(Convert.ToString(success), "Customer Entered on " + DateTime.Now.AddHours(14).ToShortDateString(), DateTime.Now.AddHours(14).ToString(), Convert.ToString(sucContacts), CustEnteredBy, "1");

                    //ClstblCustomers.tblCustomers_UpdateD2DEmp(Convert.ToString(success), ddlD2DEmployee.SelectedValue, userid, txtD2DAppDate.Text.Trim(), ddlAppTime.SelectedValue);

                    bool succ = ClstblCustomers.tblCustomer_Update_Address(Convert.ToString(success), ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value);
                    bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(Convert.ToString(success), txtstreetaddressline.Text);


                    //bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(Convert.ToString(success), hndunittype1.Value, hndunitno1.Value, hndstreetno1.Value, hndstreetname1.Value, hndstreettype1.Value, hndstreetsuffix1.Value);

                    //bool succ_postal = ClstblCustomers.tblCustomer_Update_Postal_line(Convert.ToString(success), txtpostaladdressline.Text);

                    if (txtformbaystreetname.Text != string.Empty)
                    {
                        bool succ_formbay = ClstblCustomers.tblCustomer_Update_isformbayadd(Convert.ToString(success), true.ToString());
                    }

                    //----tblcontact contphone update - jigar chokshi
                    bool suuc_contact = ClstblContacts.tblContacts_Update_ContPhone(Convert.ToString(success), CustPhone);

                    //if (txtD2DAppDate.Text != string.Empty)
                    //{
                    //    bool succ1 = ClstblCustomers.tblCustomers_Updateappointment_status("0", Convert.ToString(success));
                    //    bool succ2 = ClstblCustomers.tblCustomers_Updatecustomer_lead_status("0", Convert.ToString(success));
                    //}

                    int suc = ClstblCustomers.tblCustLogReport_Insert(Convert.ToString(success));
                    string LogReport = "";
                    string a = "";

                    if (Convert.ToString(suc) != "")
                    {
                        int exist = ClstblCustomers.tblContacts_ExistName(txtContFirst.Text + ' ' + txtContLast.Text);
                        if (exist == 1)
                        {
                            //    Response.Write("");
                            //    Response.End();
                            //a = "Update tblContacts set ContFirst='" + txtContFirst.Text + ' ' + txtContLast.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                            LogReport = "Update tblCustLogReport set Name='" + txtContFirst.Text + ' ' + txtContLast.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                        }
                        int exist2 = ClstblCustomers.tblContacts_ExistMobile(txtContMobile.Text);
                        if (exist2 == 1)
                        {
                            LogReport = "Update tblCustLogReport set Mobile='" + txtContMobile.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                        }
                        int exist3 = ClstblCustomers.tblContacts_ExistEmail(txtContEmail.Text);
                        if (exist3 == 1)
                        {
                            LogReport = "Update tblCustLogReport set Email='" + txtContEmail.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                        }
                        int exist4 = ClstblCustomers.tblCustomers_ExistPhone(txtCustPhone.Text);
                        if (exist4 == 1)
                        {
                            LogReport = "Update tblCustLogReport set LocalNo='" + txtCustPhone.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                        }
                        ClsProjectSale.ap_form_element_execute(LogReport);


                        if (Convert.ToString(success) != "")
                        {
                            //PanSuccess.Visible = true;
                            SetAdd();
                            lnkAdd.Visible = true;
                            lnkBack.Visible = false;
                            lnkclose.Visible = false;
                        }
                        else
                        {
                            SetError();
                        }

                        BindGrid(0);
                    }
                }
                else
                {
                    PAnAddress.Visible = true;
                    BindGrid(0);
                    //lblexistame.Visible = true;
                    //lblexistame.Text = "Record with this Address already exists ";
                }

            }
            else
            {
                int existaddress = ClstblCustomers.tblCustomers_ExitsStreet_address(txtStreetAddress.Text);
                //int existaddress = ClstblCustomers.tblCustomers_ExitsStreet_address(txtstreetaddressline.Text);

                if (existaddress != 1)
                {
                    success = ClstblCustomers.tblCustomers_Insert("", EmployeeID, "False", CustTypeID, CustSourceID, CustSourceSubID, "", "", ResCom, "", "", "", "", "", "", Customer, BranchLocation, "", StreetAddress, StreetCity, StreetState, StreetPostCode, PostalAddress, PostalCity, PostalState, PostalPostCode, Country, CustPhone, "", "", CustAltPhone, CustFax, CustNotes, "", "", CustWebSiteLink, "", "False", "", "", "", "", "", "", "", Area);
                    // New Log in Lead History
                    DataTable dt = ClstblSalesinfo.tblCustomer_GetDataByIDforAssignLead(Convert.ToString(success));
                    string EmpID = dt.Rows[0]["EmployeeID"].ToString();
                    string CustSource = dt.Rows[0]["CustSource"].ToString();
                    string CustSourceSub = dt.Rows[0]["CustSourceSub"].ToString();
                    string CustEntered = dt.Rows[0]["CustEntered"].ToString();
                    string RoleName = dt.Rows[0]["RoleName"].ToString();
                    string Msg = "Assign New Lead";
                    int Uns1 = ClsLead.tblLeadHistory_Insert(Convert.ToString(success), "New", CustSource, CustSourceSub, Msg, EmpID, CustEntered, userid);

                    //Response.Write("YEs");
                    //Response.End();
                    int sucContacts = ClstblCustomers.tblCustomers_InsertContacts(Convert.ToString(success), txtContMr.Text.Trim(), txtContFirst.Text.Trim(), txtContLast.Text.Trim(), txtContMobile.Text, txtContEmail.Text, EmployeeID, CustEnteredBy);
                    //Response.Write(Convert.ToString(success) + "<br>" + "Customer Entered on " + DateTime.Now.AddHours(14).ToShortDateString() + "<br>" + DateTime.Now.AddHours(14).ToString() + "<br>" + Convert.ToString(sucContacts) + "<br>" + CustEnteredBy + "<br>" + "1");
                    //Response.End();
                    int sucCustInfo = ClstblCustInfo.tblCustInfo_Insert(Convert.ToString(success), "Customer Entered on " + DateTime.Now.AddHours(14).ToShortDateString(), DateTime.Now.AddHours(14).ToString(), Convert.ToString(sucContacts), CustEnteredBy, "1");
                    //ClstblCustomers.tblCustomers_UpdateD2DEmp(Convert.ToString(success), ddlD2DEmployee.SelectedValue, userid, txtD2DAppDate.Text.Trim(), ddlAppTime.SelectedValue);
                    //bool succ = ClstblCustomers.tblCustomer_Update_Address(Convert.ToString(success), hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, hndstreetsuffix.Value);

                    bool succ = ClstblCustomers.tblCustomer_Update_Address(Convert.ToString(success), ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value);

                    bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(Convert.ToString(success), txtStreetAddress.Text);
                    //bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(Convert.ToString(success), txtstreetaddressline.Text);


                    //bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(Convert.ToString(success), hndunittype1.Value, hndunitno1.Value, hndstreetno1.Value, hndstreetname1.Value, hndstreettype1.Value, hndstreetsuffix1.Value);
                    //bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(Convert.ToString(success), ddlPostalformbayunittype.SelectedValue, txtPostalformbayUnitNo.Text, txtPostalformbayStreetNo.Text, txtPostalformbaystreetname.Text, ddlPostalformbaystreettype.SelectedValue, hndstreetsuffix1.Value);

                    //bool succ_postal = ClstblCustomers.tblCustomer_Update_Postal_line(Convert.ToString(success), txtpostaladdressline.Text);


                    if (txtformbaystreetname.Text != string.Empty)
                    {
                        bool succ_formbay = ClstblCustomers.tblCustomer_Update_isformbayadd(Convert.ToString(success), true.ToString());
                    }

                    //----tblcontact contphone update - jigar chokshi
                    bool suuc_contact = ClstblContacts.tblContacts_Update_ContPhone(Convert.ToString(success), CustPhone);

                    //if (txtD2DAppDate.Text != string.Empty)
                    //{
                    //    bool succ1 = ClstblCustomers.tblCustomers_Updateappointment_status("0", Convert.ToString(success));
                    //    bool succ2 = ClstblCustomers.tblCustomers_Updatecustomer_lead_status("0", Convert.ToString(success));
                    //}

                    int suc = ClstblCustomers.tblCustLogReport_Insert(Convert.ToString(success));
                    string LogReport = "";

                    if (Convert.ToString(suc) != "")
                    {
                        int exist = ClstblCustomers.tblContacts_ExistName(txtContFirst.Text + ' ' + txtContLast.Text);
                        if (exist == 1)
                        {
                            LogReport = "Update tblCustLogReport set Name='" + txtContFirst.Text + ' ' + txtContLast.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                        }
                        int exist2 = ClstblCustomers.tblContacts_ExistMobile(txtContMobile.Text);
                        if (exist2 == 1)
                        {
                            LogReport = "Update tblCustLogReport set Mobile='" + txtContMobile.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                        }
                        int exist3 = ClstblCustomers.tblContacts_ExistEmail(txtContEmail.Text);
                        if (exist3 == 1)
                        {
                            LogReport = "Update tblCustLogReport set Email='" + txtContEmail.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                        }
                        int exist4 = ClstblCustomers.tblCustomers_ExistPhone(txtCustPhone.Text);
                        if (exist4 == 1)
                        {
                            LogReport = "Update tblCustLogReport set LocalNo='" + txtCustPhone.Text + "' WHERE CustomerID='" + Convert.ToString(success) + "'";
                        }
                        ClsProjectSale.ap_form_element_execute(LogReport);
                        if (Convert.ToString(success) != "")
                        {
                            SetAdd();
                            lnkAdd.Visible = true;
                            lnkBack.Visible = false;
                            lnkclose.Visible = false;
                        }
                        else
                        {
                            SetError();
                        }
                        BindGrid(0);
                    }
                }
                else
                {
                    PAnAddress.Visible = true;
                    BindGrid(0);
                    //lblexistame.Visible = true;
                    //lblexistame.Text = "Record with this Address already exists ";
                }

            }
        }
    }

    public string GetErrorMessage(MembershipCreateStatus status)
    {
        switch (status)
        {
            case MembershipCreateStatus.DuplicateUserName:
                return "User with this name already exists. Please enter a different User Name.";

            case MembershipCreateStatus.DuplicateEmail:
                return "A E-mail with address already exists. Please enter a different e-mail address.";

            case MembershipCreateStatus.InvalidPassword:
                return "The password provided is invalid. Please enter a valid password value.";

            case MembershipCreateStatus.InvalidEmail:
                return "The e-mail address provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidAnswer:
                return "The password retrieval answer provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidQuestion:
                return "The password retrieval question provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.InvalidUserName:
                return "The user name provided is invalid. Please check the value and try again.";

            case MembershipCreateStatus.ProviderError:
                return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

            case MembershipCreateStatus.UserRejected:
                return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

            default:
                return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        streetaddress();
        //BindScript();
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        ;
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string id1 = "";
        if (!string.IsNullOrEmpty(Request.QueryString["companyid"]))
        {
            id1 = (Request.QueryString["companyid"]);
        }
        else
        {
            id1 = GridView1.SelectedDataKey.Value.ToString();
        }


        string EmployeeID = st.EmployeeID;
        string CustTypeID = ddlCustTypeID.SelectedValue;
        string CustSourceID = ddlCustSourceID.SelectedValue;
        string CustSourceSubID = ddlCustSourceSubID.SelectedValue;
        string ResCom = false.ToString();
        if (rblResCom1.Checked == true)
        {
            ResCom = "1";
        }
        if (rblResCom2.Checked == true)
        {
            ResCom = "2";
        }
        //bool suc_mobile1 = ClstblContacts.tblContacts_UpdateContMobile(id1, txtContMobile.Text);
        string CustEnteredBy = st.EmployeeID;
        string StreetAddress = txtStreetAddress.Text = ddlformbayunittype.SelectedValue.Trim() + " " + txtformbayUnitNo.Text.Trim() + " " + txtformbayStreetNo.Text.Trim() + " " + txtformbaystreetname.Text.Trim() + " " + ddlformbaystreettype.SelectedValue.Trim();
        //string StreetAddress = txtStreetAddress.Text;
        string StreetCity = ddlStreetCity.Text;
        string StreetState = txtStreetState.Text;
        string StreetPostCode = txtStreetPostCode.Text;
        string PostalAddress = txtStreetAddress.Text;
        string PostalCity = ddlStreetCity.Text;
        string PostalState = txtStreetState.Text;
        string PostalPostCode = txtStreetPostCode.Text;
        string Country = "Australia";
        string CustPhone = txtCustPhone.Text;
        string CustAltPhone = txtCustAltPhone.Text;
        string CustFax = txtCustFax.Text;
        string CustNotes = txtCustNotes.Text;
        string CustWebSiteLink = txtCustWebSiteLink.Text;
        string Area = false.ToString();
        bool success = false;
        if (rblArea.SelectedItem.Value == "1")
        {
            Area = "1";
        }
        if (rblArea.SelectedItem.Value == "2")
        {
            Area = "2";
        }
        DataTable dt1 = ClstblCustomers.tblCustomers_Exits_SelectCustomerbyID2(id1, txtStreetAddress.Text, StreetCity.Trim(), StreetState.Trim(), StreetPostCode.Trim(), txtContMobile.Text.Trim(), txtContEmail.Text.Trim());
        //DataTable dt1 = ClstblCustomers.tblCustomers_Exits_SelectCustomerbyID(id1, txtStreetAddress.Text, StreetCity.Trim(), StreetState.Trim(), StreetPostCode.Trim(), txtContMobile.Text.Trim(), txtContEmail.Text.Trim());
        //DataTable dt1 = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
        //Response.Write(id1 + "  " + txtStreetAddress.Text + "  " + StreetCity.Trim() + "  " + StreetState.Trim() + "  " + StreetPostCode.Trim() + "  " + txtContMobile.Text.Trim() + "  " + txtContEmail.Text.Trim());
        //Response.End();
        if (dt1.Rows.Count > 0)
        {

            PanSearch.Visible = false;
            ModalPopupExtenderAddress.Show();
            PanAddUpdate.Visible = true;
            DataTable dt = ClstblCustomers.tblCustomers_Exits_SelectCustomerbyID2(id1, txtStreetAddress.Text, StreetCity.Trim(), StreetState.Trim(), StreetPostCode.Trim(), txtContMobile.Text.Trim(), txtContEmail.Text.Trim());
            //DataTable dt = ClstblCustomers.tblCustomers_Exits_SelectCustomerbyID(id1, txtStreetAddress.Text, StreetCity.Trim(), StreetState.Trim(), StreetPostCode.Trim(), txtContMobile.Text.Trim(), txtContEmail.Text.Trim());
            //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
            rptaddress.DataSource = dt;
            rptaddress.DataBind();
            txtstreetaddressline.Focus();
        }
        else
        {
            if (chkformbayid.Checked.ToString() == "False")
            {
                //int existaddress = ClstblCustomers.tblCustomers_ExitsByID_Street_address(id1, txtstreetaddressline.Text);
                int existaddress = ClstblCustomers.tblCustomers_ExitsByID_Address(id1, hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, StreetCity, StreetState, StreetPostCode);
                if (existaddress != 1)
                {
                    // Response.End();
                    bool succ = ClstblCustomers.tblCustomer_Update_Address(id1, ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value);
                    //bool succ = ClstblCustomers.tblCustomer_Update_Address(id1, hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, hndstreetsuffix.Value);
                    bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(id1, txtstreetaddressline.Text);
                    success = ClstblCustomers.tblCustomers_Update(id1, EmployeeID, "False", CustTypeID, CustSourceID, CustSourceSubID, "", "", ResCom, CustEnteredBy, "", "", "", "", "", "", "", StreetAddress, StreetCity, StreetState, StreetPostCode, PostalAddress, PostalCity, PostalState, PostalPostCode, Country, CustPhone, "", "", CustAltPhone, CustFax, CustNotes, "", "", CustWebSiteLink, "", "False", "", "", "", "", "", "", "", Area, txtCompany.Text);

                    //bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(id1, ddlPostalformbayunittype.SelectedValue, txtPostalformbayUnitNo.Text, txtPostalformbayStreetNo.Text, txtPostalformbaystreetname.Text, ddlPostalformbaystreettype.SelectedValue, hndstreetsuffix1.Value);
                    //bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(id1, hndunittype1.Value, hndunitno1.Value, hndstreetno1.Value, hndstreetname1.Value, hndstreettype1.Value, hndstreetsuffix1.Value);
                    //bool succ_postal = ClstblCustomers.tblCustomer_Update_Postal_line(id1, txtpostaladdressline.Text);
                    if (txtformbaystreetname.Text != string.Empty)
                    {
                        bool succ_formbay = ClstblCustomers.tblCustomer_Update_isformbayadd(id1, true.ToString());
                    }

                    //----tblcontact contphone update - jigar chokshi
                    bool suuc_contact = ClstblContacts.tblContacts_Update_ContPhone(id1, CustPhone);
                    bool suc_mobile = ClstblContacts.tblContacts_UpdateContMobile(id1, txtContMobile.Text);

                    string LogReport = "";
                    string LogReportName = "";
                    string LogReportEmail = "";
                    int exist = ClstblCustomers.tblContacts_ExistName(txtContFirst.Text + ' ' + txtContLast.Text);
                    if (exist == 1)
                    {
                        LogReport = "Update tblCustLogReport set Name='" + txtContFirst.Text + ' ' + txtContLast.Text + "' WHERE CustomerID='" + id1 + "'";
                        LogReportName = "Update tblContacts set ContFirst='" + txtContFirst.Text + "' , ContLast='" + txtContLast.Text + "' WHERE CustomerID='" + id1 + "'";
                    }
                    else
                    {
                        LogReportName = "Update tblContacts set ContFirst='" + txtContFirst.Text + "' , ContLast='" + txtContLast.Text + "' WHERE CustomerID='" + id1 + "'";
                        //Response.Write(LogReport1);
                        //Response.End();
                    }
                    int exist2 = ClstblCustomers.tblContacts_ExistMobile(txtContMobile.Text);
                    if (exist2 == 1)
                    {
                        LogReport = "Update tblCustLogReport set Mobile='" + txtContMobile.Text + "' WHERE CustomerID='" + id1 + "'";
                    }
                    int exist3 = ClstblCustomers.tblContacts_ExistEmail(txtContEmail.Text);
                    if (exist3 == 1)
                    {
                        LogReport = "Update tblCustLogReport set Email='" + txtContEmail.Text + "' WHERE CustomerID='" + id1 + "'";
                    }
                    else
                    {
                        LogReportEmail = "Update tblContacts set ContEmail='" + txtContEmail.Text + "' WHERE CustomerID='" + id1 + "'";
                    }
                    int exist4 = ClstblCustomers.tblCustomers_ExistPhone(txtCustPhone.Text);
                    if (exist4 == 1)
                    {
                        LogReport = "Update tblCustLogReport set LocalNo='" + txtCustPhone.Text + "' WHERE CustomerID='" + id1 + "'";
                    }
                    ClsProjectSale.ap_form_element_execute(LogReport);

                    ClsProjectSale.ap_form_element_execute(LogReportName);

                    ClsProjectSale.ap_form_element_execute(LogReportEmail);

                    //ClstblCustomers.tblCustomers_UpdateD2DEmp(id1, ddlD2DEmployee.SelectedValue, userid, txtD2DAppDate.Text.Trim(), ddlAppTime.SelectedValue);

                    //--- do not chage this code Start------
                    if (success)
                    {
                        SetUpdate();
                    }
                    else
                    {
                        SetError();
                    }
                    BindGrid(0);

                }
                else
                {
                    PAnAddress.Visible = true;
                    BindGrid(0);
                }
            }
            else
            {
                //Response.Write("else");
                //Response.End();
                int existaddress = ClstblCustomers.tblCustomers_ExitsByID_Streetaddress(id1, StreetAddress);
                if (existaddress != 1)
                {
                    bool succ = ClstblCustomers.tblCustomer_Update_Address(id1, ddlformbayunittype.SelectedValue, txtformbayUnitNo.Text, txtformbayStreetNo.Text, txtformbaystreetname.Text, ddlformbaystreettype.SelectedValue, hndstreetsuffix.Value);
                    //bool succ = ClstblCustomers.tblCustomer_Update_Address(id1, hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, hndstreetsuffix.Value);
                    bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(id1, txtstreetaddressline.Text);
                    success = ClstblCustomers.tblCustomers_Update(id1, EmployeeID, "False", CustTypeID, CustSourceID, CustSourceSubID, "", "", ResCom, CustEnteredBy, "", "", "", "", "", "", "", StreetAddress, StreetCity, StreetState, StreetPostCode, PostalAddress, PostalCity, PostalState, PostalPostCode, Country, CustPhone, "", "", CustAltPhone, CustFax, CustNotes, "", "", CustWebSiteLink, "", "False", "", "", "", "", "", "", "", Area, txtCompany.Text);

                    //bool succ_updatepostalAddress = ClstblCustomers.tblCustomer_Update_PostalAddress(id1, hndunittype1.Value, hndunitno1.Value, hndstreetno1.Value, hndstreetname1.Value, hndstreettype1.Value, hndstreetsuffix1.Value);
                    //bool succ_postal = ClstblCustomers.tblCustomer_Update_Postal_line(id1, txtpostaladdressline.Text);
                    if (txtformbaystreetname.Text != string.Empty)
                    {
                        bool succ_formbay = ClstblCustomers.tblCustomer_Update_isformbayadd(id1, true.ToString());
                    }
                    //Response.Write(chkformbayid.Checked.ToString());
                    //Response.End();
                    //----tblcontact contphone update - jigar chokshi
                    bool suuc_contact = ClstblContacts.tblContacts_Update_ContPhone(id1, CustPhone);
                    bool suc_mobile = ClstblContacts.tblContacts_UpdateContMobile(id1, txtContMobile.Text);

                    string LogReport = "";
                    int exist = ClstblCustomers.tblContacts_ExistName(txtContFirst.Text + ' ' + txtContLast.Text);
                    if (exist == 1)
                    {
                        LogReport = "Update tblCustLogReport set Name='" + txtContFirst.Text + ' ' + txtContLast.Text + "' WHERE CustomerID='" + id1 + "'";
                    }
                    int exist2 = ClstblCustomers.tblContacts_ExistMobile(txtContMobile.Text);
                    if (exist2 == 1)
                    {
                        LogReport = "Update tblCustLogReport set Mobile='" + txtContMobile.Text + "' WHERE CustomerID='" + id1 + "'";
                    }
                    int exist3 = ClstblCustomers.tblContacts_ExistEmail(txtContEmail.Text);
                    if (exist3 == 1)
                    {
                        LogReport = "Update tblCustLogReport set Email='" + txtContEmail.Text + "' WHERE CustomerID='" + id1 + "'";
                    }
                    int exist4 = ClstblCustomers.tblCustomers_ExistPhone(txtCustPhone.Text);
                    if (exist4 == 1)
                    {
                        LogReport = "Update tblCustLogReport set LocalNo='" + txtCustPhone.Text + "' WHERE CustomerID='" + id1 + "'";
                    }
                    ClsProjectSale.ap_form_element_execute(LogReport);

                    //ClstblCustomers.tblCustomers_UpdateD2DEmp(id1, ddlD2DEmployee.SelectedValue, userid, txtD2DAppDate.Text.Trim(), ddlAppTime.SelectedValue);

                    //--- do not chage this code Start------
                    if (success)
                    {
                        SetUpdate();
                    }
                    else
                    {
                        SetError();
                    }
                    BindGrid(0);
                }
                else
                {
                    PanAddUpdate.Visible = true;
                    PanSearch.Visible = false;
                    ModalPopupExtenderAddress.Show();
                    DataTable dt = ClstblCustomers.tblCustomers_Exits_SelectCustomerbyID(id1, txtStreetAddress.Text, StreetCity.Trim(), StreetState.Trim(), StreetPostCode.Trim(), txtContMobile.Text.Trim(), txtContEmail.Text.Trim());
                    //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
                    rptaddress.DataSource = dt;
                    rptaddress.DataBind();
                    txtstreetaddressline.Focus();
                    //BindGrid(0);
                }
            }
        }
    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "lnkphoto")
        {
            ModalPopupExtender1.Show();
            div_popup.Visible = true;
            string custid = e.CommandArgument.ToString();
            //Response.Write(custid);
            //Response.End();
            DataTable dt = ClstblCustomers.tbl_CustImage_SelectByCustomerID(custid);
            RepeaterImages.DataSource = dt;
            RepeaterImages.DataBind();
            //Profile.eurosolar.companyid = custid;
        }
        BindGrid(0);
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        lblcompanyaddress.Text = "Edit Company Address";
        lbladdcompany.Text = "Edit New Company";
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        chkformbayid_CheckedChanged(sender, e);
        CompanyEditForm(id);

        //--- do not chage this code end------
    }

    public void CompanyEditForm(string id)
    {
        txtContMr.Focus();
        lblAddUpdate.Visible = false;
        Haddcompadd.Visible = true;
        Haddcomp.Visible = true;

        hdnupdateaddress.Value = id;
        SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(id);

        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.EmployeeID);
        DataTable dtCont = ClstblContacts.tblContacts_SelectTop(id);

        DataTable dtdatetime = ClstblCustomers.getUserRoles(id);

        if (dtCont.Rows.Count > 0)
        {
            txtContMr.Text = dtCont.Rows[0]["ContMr"].ToString();
            txtContFirst.Text = dtCont.Rows[0]["ContFirst"].ToString();
            txtContLast.Text = dtCont.Rows[0]["ContLast"].ToString();
            txtContEmail.Text = dtCont.Rows[0]["ContEmail"].ToString();
            txtContMobile.Text = dtCont.Rows[0]["ContMobile"].ToString();

            if (dtdatetime.Rows.Count > 0)
            {

                foreach (DataRow role in dtdatetime.Rows)
                {

                    if (role["rolename"].ToString() == "Lead Manager" || role["rolename"].ToString() == "leadgen")
                    {

                        //divD2DEmployee.Visible = true;
                        //divD2DAppDate.Visible = true;
                        //divD2DAppTime.Visible = true;

                        Response.Redirect("~/admin/adminfiles/company/ECompany.aspx?compid=" + id);
                    }
                }

            }

        }

        if (st.isformbayadd == "1")
        {
            chkformbayid.Checked = true;
        }
        else
        {
            chkformbayid.Checked = false;
        }

        txtformbayUnitNo.Text = st.unit_number;
        txtformbayStreetNo.Text = st.street_number;
        try
        {
            txtformbaystreetname.Text = st.street_name;
        }
        catch { }



        try
        {
            ddlformbaystreettype.SelectedValue = st.street_type;
        }
        catch { }
        try
        {
            ddlformbayunittype.SelectedValue = st.unit_type;
        }
        catch { }

        try
        {
            ddlCustTypeID.SelectedValue = st.CustTypeID;
        }
        catch
        {
        }
        try
        {
            ddlCustSourceID.SelectedValue = st.CustSourceID;
        }
        catch
        {
        }
        try
        {

            if (st.CustSourceSubID != string.Empty)
            {

                ddlCustSourceSubID.SelectedValue = st.CustSourceSubID;
            }
        }
        catch
        {
        }

        if (st.ResCom != string.Empty)
        {
            if (st.ResCom == "1")
            {
                rblResCom1.Checked = true;
            }
            if (st.ResCom == "2")
            {
                rblResCom2.Checked = true;
            }
        }
        else
        {
            rblResCom1.Checked = true;
        }
        //}
        //catch
        //{
        //}
        txtstreetaddressline.Text = st.street_address;
        txtCompany.Text = st.Customer;
        txtCustPhone.Text = st.CustPhone;

        //txtStreetAddress.Text = st.StreetAddress;
        if (!string.IsNullOrEmpty(st.StreetAddress))
        {
            txtStreetAddress.Text = st.StreetAddress;
        }
        else
        {
            txtStreetAddress.Text = st.unit_type + " " + st.unit_number + " " + st.street_number + " " + st.street_name + " " + st.street_type;
        }

        ddlStreetCity.Text = st.StreetCity;
        txtStreetState.Text = st.StreetState;
        txtStreetPostCode.Text = st.StreetPostCode;
        //txtPostalAddress.Text = st.PostalAddress;

        txtCustAltPhone.Text = st.CustAltPhone;
        txtCustFax.Text = st.CustFax;
        txtCustNotes.Text = st.CustNotes;
        txtCustWebSiteLink.Text = st.CustWebSiteLink;

        if (st.CustSourceID == "2")
        {
            //ddlCustSourceSubID.Visible = true;
            divSubSource.Visible = true;
        }
        else
        {
            //ddlCustSourceSubID.Visible = false;
            divSubSource.Visible = false;
        }
        if (st.Area != string.Empty)
        {
            //rblArea.SelectedValue = st.Area;
            if (st.Area == "1")
            {
                rblArea.SelectedIndex = 0;
            }
            if (st.Area == "2")
            {
                rblArea.SelectedIndex = 1;
            }
        }
        else
        {
            rblArea.ClearSelection();
        }


        InitUpdate();
        PanGrid.Visible = false;
        PanSearch.Visible = false;
        BindScript();
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            // custompagesize = 0;
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            // custompagesize = Convert.ToInt32(ddlSelectRecords.SelectedValue.ToString());
        }
        BindGrid(0);
        BindScript();
    }

    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        BindGrid(0);
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {

            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Descending;
            }

            else
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
        PanGrid.Visible = true;

        PanSearch.Visible = true;
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
        //  PanGrid.Visible = false;
        PanSearch.Visible = false;
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        //Response.Redirect("~/admin/adminfiles/company/Company.aspx");
        SetCancel();
        BindGrid(0);
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        txtContMr.Focus();
        PanSearch.Visible = false;
        InitAdd();
        PAnAddress.Visible = false;
        Reset();
        PanGrid.Visible = false;
        PanSearch.Visible = false;
        //divnopage.Visible = false;
       
        if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager"))
        {
            ddlCustSourceID.Items.Remove(ddlCustSourceID.Items.FindByText("Lead Source"));
            
            //ddlCustSourceID.Enabled = false;
            ddlCustSourceID.SelectedValue = "1";
        }

        chkformbayid_CheckedChanged(sender, e);

        rblArea.SelectedIndex = 0;
    }

    private string ConvertSortDirectionToSql(SortDirection sortDireciton)
    {
        string m_SortDirection = String.Empty;

        switch (sortDireciton)
        {
            case SortDirection.Ascending:
                m_SortDirection = "ASC";
                break;

            case SortDirection.Descending:
                m_SortDirection = "DESC";
                break;
        }
        return m_SortDirection;
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetAdd()
    {

        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAlreadExists.Visible = false;
    }

    public void SetUpdate()
    {
        PanAddUpdate.Visible = false;
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }

    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }

    public void SetCancel()
    {
        // PanNoRecord.Visible = false;
        PanSearch.Visible = true;
        Reset();
        // HidePanels();
        BindGrid(0);
        PanAddUpdate.Visible = false;
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        // ModalPopupExtender2.Hide();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = false;
    }

    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }

    public void InitAdd()
    {
        HidePanels();

        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;


        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = true;

        lblAddUpdate.Text = " ";

        rblResCom1.Checked = true;
    }

    public void InitUpdate()
    {
        PanGridSearch.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        //  HidePanels();
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
        btnReset.Visible = false;
        lblAddUpdate.Text = "Update ";

    }

    private void HidePanels()
    {
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        PanAlreadExists.Visible = false;
        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        //  PanNoRecord.Visible = false;
        PAnAddress.Visible = false;
        divTeamTime.Visible = false;
        //PanAlreadExists.Visible = false;
        //PanSuccess.Visible = false;
        //PanError.Visible = false;
        //PanNoRecord.Visible = false;
        //PanAddUpdate.Visible = false;
        //lnkBack.Visible = false;
        //lnkAdd.Visible = true;
        //PanSearch.Visible = true;


        //tdupdateaddress.Visible = false;
    }

    public void Reset()
    {
        //tdupdateaddress.Visible = false;
        PanAddUpdate.Visible = true;

        ddlformbayunittype.SelectedValue = "";
        txtformbayUnitNo.Text = string.Empty;
        txtformbayStreetNo.Text = string.Empty;
        txtformbaystreetname.Text = string.Empty;
        ddlformbaystreettype.ClearSelection();

        //ddlPostalformbayunittype.SelectedValue = "";
        //txtPostalformbayUnitNo.Text = string.Empty;
        //txtPostalformbayStreetNo.Text = string.Empty;
        //txtPostalformbaystreetname.Text = string.Empty;
        //ddlPostalformbaystreettype.SelectedValue = "";

        txtContMr.Text = string.Empty;
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        txtContMobile.Text = string.Empty;
        txtContEmail.Text = string.Empty;
        txtCustFax.Text = string.Empty;
        txtStreetAddress.Text = string.Empty;
        txtStreetState.Text = string.Empty;
        txtStreetPostCode.Text = string.Empty;
        //txtPostalAddress.Text = string.Empty;
        //txtPostalState.Text = string.Empty;
        //txtPostalPostCode.Text = string.Empty;
        txtCustPhone.Text = string.Empty;
        txtCustAltPhone.Text = string.Empty;
        txtCustFax.Text = string.Empty;
        txtCustNotes.Text = string.Empty;
        txtCustWebSiteLink.Text = string.Empty;
        txtCompany.Text = string.Empty;
        txtstreetaddressline.Text = string.Empty;
        //txtpostaladdressline.Text = string.Empty;
        txtCountry.Text = string.Empty;
        //txtuname.Text = string.Empty;
        //txtpassword.Text = string.Empty;
        //txtcpassword.Text = string.Empty;

        ddlCustSourceID_SelectedIndexChanged(ddlCustTypeID, new EventArgs());
        ddlStreetCity.Text = "";
        //ddlPostalCity.Text = "";
        //rblResCom.ClearSelection();
        rblResCom1.Checked = true;
        rblResCom2.Checked = false;
        //rblArea.ClearSelection();
        rblArea.ClearSelection();
        //divCustomer.Visible = false;
        try
        {
            ddlCustTypeID.SelectedValue = "1";
        }
        catch { }

        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        //SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);


        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        DataTable dtempteam = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
        if (dtempteam.Rows.Count > 0)
        {
            foreach (DataRow dr in dtempteam.Rows)
            {
                if (dr["SalesTeamID"].ToString() == "10" || dr["SalesTeamID"].ToString() == "12")
                {
                    SttblCustSource st1 = ClstblCustSource.tblCustSource_SelectByCustSourceIDCustSource("E2E");
                    ddlCustSourceID.SelectedValue = st1.CustSourceID;
                    ddlCustSourceID.Enabled = false;
                }
                else
                {
                    ddlCustSourceID.Enabled = true;
                }
            }
        }
        else
        {
            ddlCustSourceID.Enabled = true;
        }
        txtContMr.Focus();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //hdncgharid.Value = string.Empty;
        //ModalPopupExtenderAddress.Hide();
        //divAddressCheck.Visible = false;
        //startindex = 1;
        BindGrid(0);
        BindScript();
        //PanGrid.Attributes.Add("class", "table-responsive datahidden");
    }

    protected void ddlCustSourceID_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        PanSearch.Visible = false;
        ddlCustSourceSubID.Items.Clear();
        ListItem lst = new ListItem();
        lst.Text = "Select";
        lst.Value = "";
        ddlCustSourceSubID.Items.Add(lst);

        if (ddlCustSourceID.SelectedValue != "")
        {
            DataTable dt = ClstblCustSourceSub.tblCustSourceSub_SelectByCSId(ddlCustSourceID.SelectedValue);
            if (dt.Rows.Count > 0)
            {
                divSubSource.Visible = false;
                ddlCustSourceSubID.Visible = true;
                ddlCustSourceSubID.DataSource = dt;
                ddlCustSourceSubID.DataMember = "CustSourceSub";
                ddlCustSourceSubID.DataTextField = "CustSourceSub";
                ddlCustSourceSubID.DataValueField = "CustSourceSubID";
                ddlCustSourceSubID.DataBind();
            }
            else
            {
                ddlCustSourceSubID.Visible = false;
                divSubSource.Visible = false;
            }
        }
        BindScript();
    }

    protected void ddlStreetCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        if (ddlStreetCity.Text != string.Empty)
        {
            DataTable dtStreet = ClstblCustomers.tblPostCodes_SelectBy_PostCodeID(ddlStreetCity.Text);
            if (dtStreet.Rows.Count > 0)
            {
                txtStreetState.Text = dtStreet.Rows[0]["State"].ToString();
                txtStreetPostCode.Text = dtStreet.Rows[0]["PostCode"].ToString();
            }
        }
        BindScript();
    }

    public void DupButton()
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        PanSearch.Visible = false;
        PanGrid.Visible = false;
    }

    protected void txtContLast_TextChanged(object sender, EventArgs e)
    {
        lnkBack.Visible = true;
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        txtCompany.Text = txtContFirst.Text + " " + txtContLast.Text;


        if (txtContFirst.Text.Trim() != string.Empty || txtContLast.Text.Trim() != string.Empty)
        {
            DataTable dt = ClstblCustomers.tblContacts_ExistSelect(txtContFirst.Text + ' ' + txtContLast.Text);
            rptName.DataSource = dt;
            rptName.DataBind();

            checkExistsName();
        }
        PanSearch.Visible = false;
        PanGrid.Visible = false;
        txtCompany.Focus();
        BindScript();
    }

    public void checkExistsName()
    {
        int exist = ClstblCustomers.tblContacts_ExistName(txtContFirst.Text + ' ' + txtContLast.Text);
        if (exist == 1)
        {
            ModalPopupExtenderName.Show();
            txtContLast.Focus();
        }
        DataTable dt = ClstblCustomers.tblContacts_ExistSelect(txtContFirst.Text + ' ' + txtContLast.Text);
        rptName.DataSource = dt;
        rptName.DataBind();



    }

    protected void btnDupeName_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderName.Hide();
        HidePanels();
        rptName.DataSource = null;
        rptName.DataBind();
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        txtCompany.Text = string.Empty;
        lnkAdd.Visible = true;
        PanGrid.Visible = true;
        BindScript();
    }

    protected void btnNotDupeName_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderName.Hide();
        DupButton();
        txtContMobile.Focus();
        BindScript();
    }

    protected void txtCustPhone_TextChanged(object sender, EventArgs e)
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;

        if (txtCustPhone.Text.Trim() != string.Empty)
        {
            DataTable dt = ClstblCustomers.tblCustomers_ExistSelectPhone(txtCustPhone.Text);
            rptPhone.DataSource = dt;
            rptPhone.DataBind();

            checkExistsPhone();
        }
        txtContEmail.Focus();
        //Regex Phonepattern = new Regex(@"^(07|03|08)[\d]{8}$");
        //if (Phonepattern.IsMatch(txtCustPhone.Text))
        //{
        //    //txtContEmail.Focus();
        //    txtCustPhone.Attributes.Add("class", "form-control");
        //    txtCustPhone.Attributes.Remove("errormassage");
        //    ddlCustTypeID.Focus();
        //    //txtContMobile.CssClass.Replace("errormassage", "form-control"); 
        //}
        //else
        //{
        //    txtCustPhone.Focus();
        //    txtCustPhone.Attributes.Add("class", "form-control errormassage");
        //}
        PanSearch.Visible = false;
        PanGrid.Visible = false;
        BindScript();
    }

    public void checkExistsPhone()
    {
        int exist = ClstblCustomers.tblCustomers_ExistPhone(txtCustPhone.Text);
        if (exist == 1)
        {
            ModalPopupExtenderPhone.Show();
        }
        DataTable dt = ClstblCustomers.tblCustomers_ExistSelectPhone(txtCustPhone.Text);
        rptPhone.DataSource = dt;
        rptPhone.DataBind();
    }

    protected void btnDupePhone_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderPhone.Hide();
        HidePanels();
        rptPhone.DataSource = null;
        rptPhone.DataBind();
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        txtCompany.Text = string.Empty;
        txtCustPhone.Text = string.Empty;
        txtContEmail.Text = string.Empty;
        txtContMobile.Text = string.Empty;
        lnkAdd.Visible = true;
        BindScript();
    }

    protected void btnNotDupePhone_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderPhone.Hide();
        DupButton();
        txtCustAltPhone.Focus();
        BindScript();
    }

    protected void txtContMobile_TextChanged(object sender, EventArgs e)
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        txtCustAltPhone.Text = txtContMobile.Text;

        if (txtContMobile.Text.Trim() != string.Empty)
        {
            DataTable dt = ClstblCustomers.tblContacts_ExistSelectMobile(txtContMobile.Text);
            rptMobile.DataSource = dt;
            rptMobile.DataBind();

            checkExistsMobile();
        }
        PanSearch.Visible = false;
        Regex phoneNumpattern = new Regex(@"^04[\d]{8}$");
        if (phoneNumpattern.IsMatch(txtContMobile.Text))
        {
            txtContEmail.Focus();
            txtContMobile.Attributes.Add("class", "form-control");
            txtContMobile.Attributes.Remove("errormassage");
            //txtContMobile.CssClass.Replace("errormassage", "form-control"); 
        }
        else
        {
            txtContMobile.Focus();
            txtContMobile.Attributes.Add("class", "form-control errormassage");
        }

        BindScript();
    }

    public void checkExistsMobile()
    {
        int exist = ClstblCustomers.tblContacts_ExistMobile(txtContMobile.Text);
        if (exist == 1)
        {
            ModalPopupExtenderMobile.Show();
        }
        DataTable dt = ClstblCustomers.tblContacts_ExistSelectMobile(txtContMobile.Text);
        rptMobile.DataSource = dt;
        rptMobile.DataBind();
    }

    protected void btnDupeMobile_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderMobile.Hide();
        HidePanels();
        rptName.DataSource = null;
        rptName.DataBind();
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        txtCompany.Text = string.Empty;
        txtCustPhone.Text = string.Empty;
        txtContMobile.Text = string.Empty;
        txtContEmail.Text = string.Empty;
        lnkAdd.Visible = true;
    }

    protected void btnNotDupeMobile_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderMobile.Hide();
        DupButton();
        txtContEmail.Focus();
        BindScript();
    }

    protected void txtContEmail_TextChanged(object sender, EventArgs e)
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        txtCustPhone.Focus();
        if (txtContEmail.Text.Trim() != string.Empty)
        {
            DataTable dt = ClstblCustomers.tblContacts_ExistSelectEmail(txtContEmail.Text);
            rptEmail.DataSource = dt;
            rptEmail.DataBind();

            checkExistsEmail();
        }
        PanSearch.Visible = false;

        Regex Emailpattern = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\+)*\.\w+([-.]\w+)*");
        if (Emailpattern.IsMatch(txtContEmail.Text))
        {
            //txtContEmail.Focus();
            txtContEmail.Attributes.Add("class", "form-control");
            txtContEmail.Attributes.Remove("errormassage");
            //txtContMobile.CssClass.Replace("errormassage", "form-control"); 
        }
        else
        {
            txtContEmail.Focus();
            txtContEmail.Attributes.Add("class", "form-control errormassage");
        }
        BindScript();
    }

    public void checkExistsEmail()
    {
        int exist = ClstblCustomers.tblContacts_ExistEmail(txtContEmail.Text);
        if (exist == 1)
        {
            ModalPopupExtenderEmail.Show();
        }
        DataTable dt = ClstblCustomers.tblContacts_ExistSelectEmail(txtContEmail.Text);
        rptEmail.DataSource = dt;
        rptEmail.DataBind();
    }

    protected void btnDupeEmail_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderEmail.Hide();
        HidePanels();
        rptEmail.DataSource = null;
        rptEmail.DataBind();
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        txtCompany.Text = string.Empty;
        txtContEmail.Text = string.Empty;
        txtContMobile.Text = string.Empty;
        txtCustPhone.Text = string.Empty;
        lnkAdd.Visible = true;
    }

    protected void btnNotDupeEmail_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtenderEmail.Hide();
        DupButton();
        txtCustPhone.Focus();
        BindScript();
    }

    protected void ddlStreetCity_OnTextChanged(object sender, EventArgs e)
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        string[] cityarr = ddlStreetCity.Text.Split('|');
        if (cityarr.Length > 1)
        {
            ddlStreetCity.Text = cityarr[0].Trim();
            txtStreetState.Text = cityarr[1].Trim();
            txtStreetPostCode.Text = cityarr[2].Trim();
        }
        PanSearch.Visible = false;
        BindScript();
    }

    protected void chkViewAll_OnCheckedChanged(object sender, EventArgs e)
    {
        // PanGrid.Visible = true;
        // Response.End();
        //  {
        //DataTable dt = GetGridData();
        //dv = new DataView(dt);
        // Response.End();
        BindGrid(0);

        //   }
        // BindScript();
    }

    protected void GridView1_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton gvbtnDelete = (LinkButton)e.Row.FindControl("gvbtnDelete");

            if (Roles.IsUserInRole("Administrator"))
            {
                gvbtnDelete.Visible = true;
            }
            else
            {
                gvbtnDelete.Visible = false;
            }
        }

        if (e.Row.RowType == DataControlRowType.Pager)
        {
            //GridViewRow gvrow = GridView1.BottomPagerRow;
            //Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");

            GridViewRow gvrow = e.Row;
            Label lblcurrentpage = (Label)gvrow.FindControl("CurrentPage");

            lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
            int[] page = new int[7];
            page[0] = GridView1.PageIndex - 2;
            page[1] = GridView1.PageIndex - 1;
            page[2] = GridView1.PageIndex;
            page[3] = GridView1.PageIndex + 1;
            page[4] = GridView1.PageIndex + 2;
            page[5] = GridView1.PageIndex + 3;
            page[6] = GridView1.PageIndex + 4;
            for (int i = 0; i < 7; i++)
            {
                if (i != 3)
                {
                    if (page[i] < 1 || page[i] > GridView1.PageCount)
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Visible = false;
                    }
                    else
                    {
                        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                        lnkbtn.Text = Convert.ToString(page[i]);
                        lnkbtn.CommandName = "PageNo";
                        lnkbtn.CommandArgument = lnkbtn.Text;

                    }
                }
            }

            if (GridView1.PageIndex == 0)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
                lnkbtn.Visible = false;

            }
            if (GridView1.PageIndex == GridView1.PageCount - 1)
            {
                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
                lnkbtn.Visible = false;
                lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
                lnkbtn.Visible = false;

            }

            Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
            if (dv.ToTable().Rows.Count > 0)
            {
                int iTotalRecords = dv.ToTable().Rows.Count;

                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }

                ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
            else
            {
                ltrPage.Text = "";
            }
        }
    }

    protected void lnkclose_Click(object sender, EventArgs e)
    {
        if (Roles.IsUserInRole("Installer"))
        {
            Response.Redirect("~/admin/adminfiles/installation/installations.aspx");
        }
        else
        {
            Response.Redirect("~/admin/adminfiles/company/testcompany.aspx");
        }
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {

    }

    protected void rptEmail_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ModalPopupExtenderEmail.Show();
        rptEmail.PageIndex = e.NewPageIndex;
        DataTable dt = ClstblCustomers.tblContacts_ExistSelectEmail(txtContEmail.Text);
        rptEmail.DataSource = dt;
        rptEmail.DataBind();
    }

    protected void rptPhone_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ModalPopupExtenderPhone.Show();
        rptPhone.PageIndex = e.NewPageIndex;
        DataTable dt = ClstblCustomers.tblCustomers_ExistSelectPhone(txtCustPhone.Text);
        rptPhone.DataSource = dt;
        rptPhone.DataBind();
    }

    protected void rptMobile_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ModalPopupExtenderMobile.Show();
        rptMobile.PageIndex = e.NewPageIndex;
        DataTable dt = ClstblCustomers.tblContacts_ExistSelectMobile(txtContMobile.Text);
        rptMobile.DataSource = dt;
        rptMobile.DataBind();
    }

    protected void rptName_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ModalPopupExtenderName.Show();
        rptName.PageIndex = e.NewPageIndex;
        DataTable dt = ClstblCustomers.tblContacts_ExistSelect(txtContFirst.Text + ' ' + txtContLast.Text);
        rptName.DataSource = dt;
        rptName.DataBind();
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        //BindGrid(0);
    }

    protected void chkformbayid_CheckedChanged(object sender, EventArgs e)
    {

        PanAddUpdate.Visible = true;
        PanSearch.Visible = false;
        if (chkformbayid.Checked == true)
        {
            txtstreetaddressline.Enabled = true;
            txtStreetAddress.Enabled = false;
            ddlStreetCity.Enabled = false;
            AutoCompletestreet.Enabled = false;

            txtstreetaddressline.Attributes.Add("onblur", "getParsedAddress();");
        }
        else
        {
            divsttype.Visible = true;
            divstname.Visible = true;
            divunittype.Visible = true;
            divUnitno.Visible = true;
            divStreetno.Visible = true;

            txtstreetaddressline.Enabled = false;

            //txtStreetAddress.Enabled = true;
            ddlStreetCity.Enabled = true;
            AutoCompletestreet.Enabled = true;
            rfvstreetaddressline.Visible = false;
            //Customstreetadd.Visible = false;
            //Custompostaladdress.Visible = false;

            txtstreetaddressline.Attributes.Add("onblur", "");
        }
        BindScript();
    }

    protected void btnUpdateAddress_Click(object sender, EventArgs e)
    {
        if (hdnupdateaddress.Value != string.Empty)
        {

            //   ModalPopupExtender1.Show();
            //  div_popup.Visible = true;
            //ModalPopupExtenderName.Hide();
            divName.Visible = false;
            PanAddUpdate.Visible = true;
            BindScript();

            //ddlUnitType.DataSource = ClstblUnitType.tblUnitType_SelectAsc();
            //ddlUnitType.DataMember = "UnitTypeName";
            //ddlUnitType.DataTextField = "UnitTypeName";
            //ddlUnitType.DataValueField = "UnitTypeCode";
            //ddlUnitType.DataBind();

            //ddlstreetType.DataSource = ClstblStreetType.tblStreetType_SelectAsc();
            //ddlstreetType.DataMember = "StreetTypeName";
            //ddlstreetType.DataTextField = "StreetTypeName";
            //ddlstreetType.DataValueField = "StreetTypeCode";
            //ddlstreetType.DataBind();

            //SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(hdnupdateaddress.Value);
            //ddlUnitType.SelectedValue = st.unit_type.ToUpper().ToString();
            //txtUnitNo.Text = st.unit_number;
            //txtstreetno.Text = st.street_number;
            //txtStreetName.Text = st.street_name;
            //ddlstreetType.SelectedValue = st.street_type.ToUpper().ToString();

            //txtCity.Text = st.StreetCity;
            //txtState.Text = st.StreetState;
            //txtpostcode.Text = st.StreetPostCode;
        }
    }

    protected void btnUpdateAddressData_Click(object sender, EventArgs e)
    {
        if (hdnupdateaddress.Value != string.Empty)
        {
            SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(hdnupdateaddress.Value);
            //bool succ = ClstblCustomers.tblCustomer_Update_Address(hdnupdateaddress.Value, ddlUnitType.SelectedValue, txtUnitNo.Text, txtstreetno.Text, txtStreetName.Text, ddlstreetType.SelectedValue, st.street_suffix);


            //bool succ_street = ClstblCustomers.tblCustomer_Update_Street_line(hdnupdateaddress.Value, ddlUnitType.SelectedValue + " " + txtUnitNo.Text + " " + txtstreetno.Text + " " + txtStreetName.Text + " " + ddlstreetType.SelectedValue + " ," + txtCity.Text + " " + txtState.Text + " " + txtpostcode.Text);
            //   string addressline = ddlUnitType.SelectedValue + " " + txtUnitNo.Text + " " + txtstreetno.Text + " " + txtStreetName.Text + " " + ddlstreetType.SelectedValue + " ," + txtCity.Text + " " + txtState.Text + " " + txtpostcode.Text;

            // ClstblCustomers.tblCustomer_Update_Address(hdnupdateaddress.Value, ddlUnitType.SelectedValue, txtUnitNo.Text, txtstreetno.Text, txtStreetName.Text, ddlstreetType.SelectedValue, st.street_suffix);
            // ClstblCustomers.tblCustomer_Update_Street_line(hdnupdateaddress.Value, addressline);

            // ClstblCustomers.tblCustomer_Update_PostalAddress(hdnupdateaddress.Value, ddlUnitType.SelectedValue, txtUnitNo.Text, txtstreetno.Text, txtStreetName.Text, ddlstreetType.SelectedValue, st.street_suffix);

            //  ClstblCustomers.tblCustomer_Update_Postal_line(hdnupdateaddress.Value, addressline);
            //   string streetaddress = txtUnitNo.Text + txtStreetName.Text + " " + ddlstreetType.SelectedValue;
            //  ClstblCustomers.tblCustomers_UpdateAddressDetails(hdnupdateaddress.Value, streetaddress, txtCity.Text, txtState.Text, txtpostcode.Text, streetaddress, txtCity.Text, txtState.Text, txtpostcode.Text, st.CustPhone);
            PanAddUpdate.Visible = true;
        }
    }

    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "dodropdown();", true);
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);

    }

    protected void ibtnCancel_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderAddress.Hide();
    }

    protected void ibtnCancel1_Click(object sender, EventArgs e)
    {
        ModalPopupExtender1.Hide();

    }

    protected void RepeaterImages_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        //Response.Write("yes");

        if (e.CommandName == "Delete")
        {
            string id = e.CommandArgument.ToString();
            hdndelete1.Value = id;
            ModalPopupExtenderDelete.Show();
            BindGrid(0);
        }

        //Response.Write("delete" + id);
        //Response.End();

    }

    protected void txtstreetaddressline_TextChanged(object sender, EventArgs e)
    {
        //txtstreetaddressline.Attributes.Add("onblur", "getParsedAddress();");
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        txtstreetaddressline.Focus();
        string StreetCity = ddlStreetCity.Text;
        string StreetState = txtStreetState.Text;
        string StreetPostCode = txtStreetPostCode.Text;
        txtStreetAddress.Text = txtformbayUnitNo.Text + " " + ddlformbayunittype.SelectedValue + " " + txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue;

        if (txtstreetaddressline.Text.Trim() != string.Empty)
        {
            DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress(txtStreetAddress.Text, StreetCity, StreetState, StreetPostCode);
            //DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);

            rptaddress.DataSource = dt;
            rptaddress.DataBind();

            //checkExistsAddress();
        }
        PanSearch.Visible = false;
        BindScript();
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "getParsedAddress();", true);
    }

    public void checkExistsAddress()
    {

        string StreetCity = ddlStreetCity.Text;
        string StreetState = txtStreetState.Text;
        string StreetPostCode = txtStreetPostCode.Text;
        string StreetAddress = txtStreetAddress.Text;
        int exist = ClstblCustomers.tblCustomers_Exits_Address(hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, StreetCity, StreetState, StreetPostCode);
        DataTable dt1 = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress(StreetAddress, StreetCity, StreetState, StreetPostCode);

        if (dt1.Rows.Count > 0)
        {
            ModalPopupExtenderAddress.Show();
        }
        DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress(StreetAddress, StreetCity, StreetState, StreetPostCode);
        rptaddress.DataSource = dt;
        rptaddress.DataBind();
    }

    [WebMethod]
    public static bool Getstreetname(string streetname)
    {
        string data = "select StreetName from tbl_formbaystreetname where StreetName='" + streetname + "'";
        DataTable dt = ClstblCustomers.query_execute(data);
        string desc = string.Empty;
        if (dt.Rows.Count > 0)
        {
            desc = dt.Rows[0]["StreetName"].ToString();
            return true;
        }
        else
        {
            return false;
        }
    }

    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        ddlSearchType.SelectedValue = "";
        ddlSearchRec.SelectedValue = "";
        ddlSearchArea.SelectedValue = "";
        ddlSearchState.SelectedValue = "";
        txtSearch.Text = string.Empty;
        txtSearchPostCode.Text = string.Empty;
        txtSerachCity.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;
        txtSearchStreet.Text = string.Empty;

        BindScript();

        BindGrid(0);
    }

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }

    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }

    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }

    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }

    protected void ddlStreetCity_TextChanged(object sender, EventArgs e)
    {
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        //streetaddress();
        string[] cityarr = ddlStreetCity.Text.Split('|');
        if (cityarr.Length > 1)
        {
            ddlStreetCity.Text = cityarr[0].Trim();
            txtStreetState.Text = cityarr[1].Trim();
            txtStreetPostCode.Text = cityarr[2].Trim();
        }

        PanSearch.Visible = false;
        BindScript();
    }

    public void streetaddress()
    {
        address = ddlformbayunittype.SelectedValue + " " + txtformbayUnitNo.Text + " " + txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue;
        txtStreetAddress.Text = address;
        BindScript();
    }

    protected void lblprint_Click(object sender, EventArgs e)
    {

    }

    protected void btnDupeAddress_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderEmail.Hide();
        HidePanels();
        rptaddress.DataSource = null;
        rptaddress.DataBind();
        txtformbayUnitNo.Text = string.Empty;
        ddlformbayunittype.Text = string.Empty;
        txtformbayStreetNo.Text = string.Empty;
        ddlformbaystreettype.Text = string.Empty;
        txtformbaystreetname.Text = string.Empty;
        ddlStreetCity.Text = string.Empty;
        txtStreetState.Text = string.Empty;
        txtStreetPostCode.Text = string.Empty;
        txtStreetAddress.Text = string.Empty;
        lnkAdd.Visible = true;
    }

    protected void btnNotDupeAddress_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderEmail.Hide();
        DupButton();
        txtformbayUnitNo.Focus();
        ddlformbayunittype.Focus();
        txtformbayStreetNo.Focus();
        ddlformbaystreettype.Focus();
        txtformbaystreetname.Focus();
        ddlStreetCity.Focus();
        BindScript();
    }
    protected void txtContFirst_TextChanged(object sender, EventArgs e)
    {
        PanGrid.Visible = false;
        PanSearch.Visible = false;
        lnkBack.Visible = true;
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        txtCompany.Text = txtContFirst.Text.Trim() + " " + txtContLast.Text.Trim();
        txtContLast.Focus();
        DataTable dt = ClstblCustomers.tblContacts_ExistSelect(txtContFirst.Text.Trim() + ' ' + txtContLast.Text.Trim());
        rptName.DataSource = dt;
        rptName.DataBind();
        checkExistsName();
        BindScript();
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {

        string id = string.Empty;
        bool sucess1 = false;
        bool sucess2 = false;
        if (hdndelete.Value.ToString() != string.Empty)
        {
            id = hdndelete.Value.ToString();

            DataTable dt = ClstblContacts.tblContacts_SelectByCustId(id);

            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    SiteConfiguration.deleteimage(dt.Rows[i]["AL"].ToString(), "AL");
                    SiteConfiguration.DeletePDFFile("AL", dt.Rows[i]["AL"].ToString());
                }
            }


            DataTable dtP = ClstblProjects.tblProjects_SelectByUCustomerID(id);
            if (dtP.Rows.Count > 0)
            {
                for (int i = 0; i < dtP.Rows.Count; i++)
                {
                    SiteConfiguration.deleteimage(dtP.Rows[i]["SQ"].ToString(), "SQ");
                    SiteConfiguration.deleteimage(dtP.Rows[i]["MP"].ToString(), "MP");
                    SiteConfiguration.deleteimage(dtP.Rows[i]["EB"].ToString(), "EB");
                    SiteConfiguration.deleteimage(dtP.Rows[i]["PD"].ToString(), "PD");
                    SiteConfiguration.deleteimage(dtP.Rows[i]["ST"].ToString(), "ST");
                    SiteConfiguration.deleteimage(dtP.Rows[i]["CE"].ToString(), "CE");
                    SiteConfiguration.deleteimage(dtP.Rows[i]["PR"].ToString(), "PR");
                    SiteConfiguration.deleteimage(dtP.Rows[i]["InvDoc"].ToString(), "InvDoc");
                    SiteConfiguration.deleteimage(dtP.Rows[i]["InvDocDoor"].ToString(), "InvDocDoor");

                    SiteConfiguration.DeletePDFFile("SQ", dtP.Rows[i]["SQ"].ToString());
                    SiteConfiguration.DeletePDFFile("MP", dtP.Rows[i]["MP"].ToString());
                    SiteConfiguration.DeletePDFFile("EB", dtP.Rows[i]["EB"].ToString());
                    SiteConfiguration.DeletePDFFile("PD", dtP.Rows[i]["PD"].ToString());
                    SiteConfiguration.DeletePDFFile("ST", dtP.Rows[i]["ST"].ToString());
                    SiteConfiguration.DeletePDFFile("CE", dtP.Rows[i]["CE"].ToString());
                    SiteConfiguration.DeletePDFFile("PR", dtP.Rows[i]["PR"].ToString());
                    SiteConfiguration.DeletePDFFile("InvDoc", dtP.Rows[i]["InvDoc"].ToString());
                    SiteConfiguration.DeletePDFFile("InvDocDoor", dtP.Rows[i]["InvDocDoor"].ToString());
                }
            }

            DataTable dtIC = ClstblInvoiceCommon.tblInvoiceCommon_SelectByCustomerID(id);
            if (dtIC.Rows.Count > 0)
            {
                for (int i = 0; i < dtIC.Rows.Count; i++)
                {
                    SiteConfiguration.deleteimage(dtIC.Rows[i]["InvDoc"].ToString(), "commoninvdoc");
                    SiteConfiguration.DeletePDFFile("commoninvdoc", dtIC.Rows[i]["InvDoc"].ToString());
                }
            }

            //Response.Write(id);
            //Response.End();  

            sucess1 = ClstblCustomers.tblCustomers_Delete(id);
        }
        if (hdndelete1.Value.ToString() != string.Empty)
        {
            id = hdndelete1.Value.ToString();
            DataTable dt = ClstblCustomers.tbl_CustImage_SelectByCustImageID(id);

            sucess2 = ClstblCustomers.tbl_CustImage_DeleteByCustImageID(id);
            if (sucess2)
            {
                if (dt.Rows.Count > 0)
                {
                    SiteConfiguration.deleteimage(dt.Rows[0]["Imagename"].ToString(), foldername);
                }
            }
        }


        //Response.Write(sucess1);
        //Response.End();
        //--- do not chage this code start------
        if (sucess1 || sucess2)
        {
            SetDelete();
        }
        else
        {
            SetError();
        }
        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "modal_danger", "$('#modal_danger').modal('hide');", true);
        GridView1.EditIndex = -1;
        BindGrid(0);

    }
    //protected override PageStatePersister PageStatePersister
    //{
    //    get
    //    {
    //        return new FileViewStateOptimizer(Page);
    //    }
    //}

    protected void txtformbaystreetname_TextChanged(object sender, EventArgs e)
    {

    }

    public void DirectAddFromLeadTracker()
    {
        txtContMr.Focus();
        PanSearch.Visible = false;
        InitAdd();
        PAnAddress.Visible = false;
        Reset();
        PanGrid.Visible = false;
        PanSearch.Visible = false;
        //divnopage.Visible = false;

        if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager"))
        {
            ddlCustSourceID.Items.Remove(ddlCustSourceID.Items.FindByText("Lead Source"));

            //ddlCustSourceID.Enabled = false;
            ddlCustSourceID.SelectedValue = "1";
        }

        rblArea.SelectedIndex = 0;

        PanAddUpdate.Visible = true;
        PanSearch.Visible = false;
        if (chkformbayid.Checked == true)
        {
            txtstreetaddressline.Enabled = true;
            txtStreetAddress.Enabled = false;
            ddlStreetCity.Enabled = false;
            AutoCompletestreet.Enabled = false;

            txtstreetaddressline.Attributes.Add("onblur", "getParsedAddress();");
        }
        else
        {
            divsttype.Visible = true;
            divstname.Visible = true;
            divunittype.Visible = true;
            divUnitno.Visible = true;
            divStreetno.Visible = true;

            txtstreetaddressline.Enabled = false;

            //txtStreetAddress.Enabled = true;
            ddlStreetCity.Enabled = true;
            AutoCompletestreet.Enabled = true;
            rfvstreetaddressline.Visible = false;
            //Customstreetadd.Visible = false;
            //Custompostaladdress.Visible = false;

            txtstreetaddressline.Attributes.Add("onblur", "");
        }
        BindScript();
    }
}