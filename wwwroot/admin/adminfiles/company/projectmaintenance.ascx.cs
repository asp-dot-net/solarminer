﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;


public partial class includes_controls_projectmaintenance : System.Web.UI.UserControl
{
  

    decimal capacity;
    decimal rebate;
    decimal SystemCapacity;
    decimal stcno;
    decimal output;  
    protected static Boolean openModalPanel = false;
    protected static Boolean openModalInverter = false;
     
    protected void Page_Load(object sender, EventArgs e)
    {
        txtOpenDate.Text = DateTime.Now.AddHours(14).ToShortDateString();

        string ProjectID =  Request.QueryString["proid"];
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if ((Roles.IsUserInRole("Sales Manager")))
            {
                if (st2.ProjectStatusID == "3")
                {
                    ibtnUpdateDetail.Visible = false;
                }
            }
        }

        if (openModalPanel == true)
        {
            ModalPopupExtenderAddPanel.Show();
        }
        else
        {
            ModalPopupExtenderAddPanel.Hide();
        }
        if (openModalInverter == true)
        {
            ModalPopupExtenderAddInverter.Show();
        }
        else
        {
            ModalPopupExtenderAddInverter.Hide();
        }
      
        if(!IsPostBack)
        {
           ModalPopupExtenderAddPanel.Hide();
            //ModalPopupExtenderAddInverter.Hide();
            BindProjectSale();
        }
    }


    public void BindProjectSale()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);

            if (Roles.IsUserInRole("Accountant"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("Finance"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("BookInstallation"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Customer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Maintenance"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("STC"))
            {
                PanAddUpdate.Enabled = false;
            }
            //if (Roles.IsUserInRole("PreInstaller"))
            //{
            //    PanAddUpdate.Enabled = true;
            //}
            //if (Roles.IsUserInRole("PostInstaller"))
            //{
            //    PanAddUpdate.Enabled = false;
            //}
            if (Roles.IsUserInRole("Installer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("DSalesRep") || Roles.IsUserInRole("SalesRep"))
            {
                ddlEmployee.Enabled = false;
              
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
            {
                if (st.ProjectStatusID == "5")
                {
                    PanAddUpdate.Enabled = false;
                }
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                string CEmpType = stEmpC.EmpType;
                //string CSalesTeamID = stEmpC.SalesTeamID;
                string CSalesTeamID = "";
                DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmpC.EmployeeID);
                if (dt_empsale.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale.Rows)
                    {
                        CSalesTeamID += dr["SalesTeamID"].ToString() + ",";
                    }
                    CSalesTeamID = CSalesTeamID.Substring(0, CSalesTeamID.Length - 1);
                }

                if (Request.QueryString["proid"] != string.Empty)
                {
                    SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.EmployeeID);
                    string EmpType = stEmp.EmpType;
                    //string SalesTeamID = stEmp.SalesTeamID;
                    string SalesTeam = "";
                    DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
                    if (dt_empsale1.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_empsale1.Rows)
                        {
                            SalesTeam += dr["SalesTeamID"].ToString() + ",";
                        }
                        SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                    }

                    if (CEmpType == EmpType && CSalesTeamID == SalesTeam)
                    {
                        PanAddUpdate.Enabled = true;
                    }
                    else
                    {
                        PanAddUpdate.Enabled = false;
                    }
                }
            }
            BindSaleDropDown();
            if (Roles.IsUserInRole("SalesRep") && (st.ProjectStatusID == "3" || st.ProjectStatusID == "5"))
            {
                PanAddUpdate.Enabled = false;
            }

          
            try
            {
                //Response.Write(st.PanelBrandID);
                //Response.End();
                ddlPanel.SelectedValue = st.PanelBrandID;
            }
            catch { }

            txtPanelBrand.Text = st.PanelBrand;
            //Response.Write(st.PanelBrand);
            //Response.End();
            txtWatts.Text = st.PanelOutput;
            try
            {
                txtRebate.Text = SiteConfiguration.ChangeCurrencyVal((st.RECRebate).ToString());
            }
            catch { }
            txtSTCMult.Text = st.STCMultiplier;
            txtPanelModel.Text = st.PanelModel;
            
            txtNoOfPanels.Text = st.NumberPanels;
            txtSystemCapacity.Text = st.SystemCapKW;
            //try
            //{
            ddlInverter.SelectedValue = st.InverterDetailsID;
            //}
            //catch
            //{
            //}
            txtInverterBrand.Text = st.InverterBrand;
            txtSeries.Text = st.InverterSeries;
            //try
            //{
            ddlInverter2.SelectedValue = st.SecondInverterDetailsID;

            if (st.ThirdInverterDetailsID != "")
            {
                ddlInverter3.SelectedValue = st.ThirdInverterDetailsID;
            }

            //}
            //catch
            //{
            //}
            txtSTCNo.Text = st.STCNumber;

            DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
            if (dtSTCValue.Rows.Count > 0)
            {
                //try
                //{
                txtSTCValue.Text = SiteConfiguration.ChangeCurrencyVal(dtSTCValue.Rows[0]["STCValue"].ToString());
                //}
                //catch { }
            }

            txtInverterModel.Text = st.InverterModel;
            txtqty.Text = st.inverterqty;
            txtqty2.Text = st.inverterqty2;
            txtqty3.Text = st.inverterqty3;


            txtInverterOutput.Text = st.InverterOutput;
            

            if (st.InstallPostCode != "")
            {
                DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(st.InstallPostCode);
                if (dtZoneCode.Rows.Count > 0)
                {

                    string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
                    DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                    if (dtZoneRt.Rows.Count > 0)
                    {
                        //Response.Write(dtZoneRt.Rows[0]["STCRating"].ToString());
                        //Response.End();
                        txtZoneRt.Text = dtZoneRt.Rows[0]["STCRating"].ToString();
                    }
                }
            }




            if (Roles.IsUserInRole("Installation Manager"))
            {

                if (st.ProjectTypeID == "8")
                {

                    divPanelDetail.Visible = true;
                    divInverterDetail.Visible = true;

                }
            }
          
            if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("Sales Manager"))
            {
                //Panel1.Enabled = false;
                Panel1.Attributes.Add("style", "pointer-events: none;");
            }
            else
            {
                //Panel1.Enabled = true;
                //Panel1.Attributes.Add("", "");
            }
        }

        // remove code till the end when shift page


        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {

            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

            string CustomerID = stPro.CustomerID;
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);
            lblCustomer.Text = stCust.Customer;
            lblProject.Text = stPro.Project;
            DataTable dtContact = ClstblContacts.tblContacts_SelectTop1ByCustId(CustomerID);
            if (dtContact.Rows.Count > 0)
            {
                lblContact.Text = dtContact.Rows[0]["ContFirst"].ToString() + " " + dtContact.Rows[0]["ContLast"].ToString();
                lblMobile.Text = dtContact.Rows[0]["ContMobile"].ToString();
            }
            try
            {
                lblInstallDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
            }
            catch { }

            lblSalesRep.Text = stPro.SalesRepName;
            if (stPro.Installer != string.Empty)
            {
                lblInstaller.Text = stPro.InstallerName;
            }
            else
            {
                lblInstaller.Text = "N/A";
            }
            if (stPro.Electrician != string.Empty)
            {
                lblMeterElec.Text = stPro.ElectricianName;
            }
            else
            {
                lblMeterElec.Text = "N/A";
            }
            if (stPro.PanelBrandID != string.Empty && stPro.InverterDetailsID != string.Empty)
            {
                lblSystem.Text = stPro.SystemDetails;
            }


            string id = Request.QueryString["proid"];

            SttblProjectMaintenance st = ClstblProjectMaintenance.tblProjectMaintenance_SelectByProjectMaintainanceByProjectID(id);
            try
            {
                ddlProjectMtceReasonID.SelectedValue = st.ProjectMtceReasonID;
            }
            catch
            {
            }
            ddlProjectMtceReasonSubID.SelectedValue = st.ProjectMtceReasonSubID;
           
            ddlInstallerAssigned.SelectedValue = st.InstallerID;

            try
            {
                txtOpenDate.Text = Convert.ToDateTime(st.OpenDate).ToShortDateString();
            }
            catch { }

            try
            {
                txtPropResolutionDate.Text = Convert.ToDateTime(st.PropResolutionDate).ToShortDateString();
            }
            catch { }
            try
            {
                txtCompletionDate.Text = Convert.ToDateTime(st.CompletionDate).ToShortDateString();
            }
            catch { }

        
            txtCurrentPhoneContact.Text = st.CurrentPhoneContact;
            chkWarranty.Checked = Convert.ToBoolean(st.Warranty);
            try
            {
                txtMtceCost.Text = SiteConfiguration.ChangeCurrencyVal(st.MtceCost);
            }
            catch { }
            try
            {
                txtServiceCost.Text = SiteConfiguration.ChangeCurrencyVal(st.ServiceCost);
            }
            catch { }

            try
            {
                txtMtceDiscount.Text = SiteConfiguration.ChangeCurrencyVal(st.MtceDiscount);
            }
            catch { }
            try
            {
                txtMtceBalance.Text = SiteConfiguration.ChangeCurrencyVal(st.MtceBalance);
            }
            catch { }

           
        }
    }


    public void BindSaleDropDown()
    {
        SttblProjects stProj = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stProj.CustomerID);

        DataTable dtState = ClstblCustomers.tblCompanyLocations_SelectID(stProj.InstallState);
        string State = "0";
        if (dtState.Rows.Count > 0)
        {
            State = dtState.Rows[0]["CompanyLocationID"].ToString();
        }
        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlPanel.Items.Clear();
        ddlPanel.Items.Add(item1);

        DataTable dtPanel = null;
        if (Roles.IsUserInRole("SalesRep"))
        {
            dtPanel = ClsProjectSale.tblStockItems_SelectPanel_SalesRep(State);
        }
        else
        {

            dtPanel = ClsProjectSale.tblStockItems_SelectPanel(State);
        }

        ddlPanel.DataSource = dtPanel;
        ddlPanel.DataValueField = "StockItemID";
        ddlPanel.DataMember = "StockItem";
        ddlPanel.DataTextField = "StockItem";
        ddlPanel.DataBind();



        ListItem item2 = new ListItem();
        item2.Text = "Select";
        item2.Value = "";
        ddlInverter.Items.Clear();
        ddlInverter.Items.Add(item2);

        DataTable dtInverter = null;
        if (Roles.IsUserInRole("SalesRep"))
        {
            dtInverter = ClsProjectSale.tblStockItems_SelectInverter_SalesRep(State);
        }
        else
        {
            dtInverter = ClsProjectSale.tblStockItems_SelectInverter(State);
        }

        ddlInverter.DataSource = dtInverter;
        ddlInverter.DataValueField = "StockItemID";
        ddlInverter.DataMember = "StockItem";
        ddlInverter.DataTextField = "StockItem";
        ddlInverter.DataBind();

        ListItem item3 = new ListItem();
        item3.Text = "Select";
        item3.Value = "";
        ddlInverter2.Items.Clear();
        ddlInverter2.Items.Add(item3);

        ddlInverter2.DataSource = dtInverter;
        ddlInverter2.DataValueField = "StockItemID";
        ddlInverter2.DataMember = "StockItem";
        ddlInverter2.DataTextField = "StockItem";
        ddlInverter2.DataBind();


        ListItem item9 = new ListItem();
        item9.Text = "Select";
        item2.Value = "";
        ddlInverter3.Items.Clear();
        ddlInverter3.Items.Add(item9);

        ddlInverter3.DataSource = dtInverter;
        ddlInverter3.DataValueField = "StockItemID";
        ddlInverter3.DataMember = "StockItem";
        ddlInverter3.DataTextField = "StockItem";
        ddlInverter3.DataBind();

        ddlInstallerAssigned.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlInstallerAssigned.DataValueField = "ContactID";
        ddlInstallerAssigned.DataMember = "Contact";
        ddlInstallerAssigned.DataTextField = "Contact";
        ddlInstallerAssigned.DataBind();
      

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);


        string SalesTeam = "";
        DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmp.EmployeeID);
        if (dt_empsale.Rows.Count > 0)
        {
            foreach (DataRow dr in dt_empsale.Rows)
            {
                if (dr["SalesTeamID"].ToString() != string.Empty)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
            }
            if (SalesTeam != string.Empty)
            {
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }
        }
        

        System.Web.UI.WebControls.ListItem item12 = new System.Web.UI.WebControls.ListItem();
        item12.Text = "Select";
        item12.Value = "";
        ddlEmployee.Items.Clear();
        ddlEmployee.Items.Add(item12);

        if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        {
            //string SalesTeam = "";
            //DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
            if (dt_empsale.Rows.Count > 0)
            {
                foreach (DataRow dr in dt_empsale.Rows)
                {
                    SalesTeam += dr["SalesTeamID"].ToString() + ",";
                }
                SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
            }
            if (SalesTeam != string.Empty)
            {
                ddlEmployee.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlEmployee.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        }
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataMember = "fullname";
        ddlEmployee.DataTextField = "fullname";
        ddlEmployee.DataBind();

        ddlEmployee.SelectedValue = stEmp.EmployeeID;

      

      

      
        ListItem item21 = new ListItem();
        item21.Text = "Select";
        item21.Value = "";
        ddlProjectMtceReasonID.Items.Clear();
        ddlProjectMtceReasonID.Items.Add(item21);

        ddlProjectMtceReasonID.DataSource = ClstblProjectMtceReason.tblProjectMtceReason_SelectASC();
        ddlProjectMtceReasonID.DataValueField = "ProjectMtceReasonID";
        ddlProjectMtceReasonID.DataMember = "ProjectMtceReason";
        ddlProjectMtceReasonID.DataTextField = "ProjectMtceReason";
        ddlProjectMtceReasonID.DataBind();

        ddlProjectMtceReasonID.SelectedValue = "2";

        ListItem item22 = new ListItem();
        item22.Text = "Select";
        item22.Value = "";
        ddlProjectMtceReasonSubID.Items.Clear();
        ddlProjectMtceReasonSubID.Items.Add(item22);

        ddlProjectMtceReasonSubID.DataSource = ClstblProjectMtceReasonSub.tblProjectMtceReasonSub_SelectASC();
        ddlProjectMtceReasonSubID.DataValueField = "ProjectMtceReasonSubID";
        ddlProjectMtceReasonSubID.DataMember = "ProjectMtceReasonSub";
        ddlProjectMtceReasonSubID.DataTextField = "ProjectMtceReasonSub";
        ddlProjectMtceReasonSubID.DataBind();

        ddlProjectMtceReasonSubID.SelectedValue = "2";


      
    }

    public void Bindmaintainance()
    {
        BindSaleDropDown();


        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {

            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

            string CustomerID = stPro.CustomerID;
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);
            lblCustomer.Text = stCust.Customer;
            lblProject.Text = stPro.Project;
            DataTable dtContact = ClstblContacts.tblContacts_SelectTop1ByCustId(CustomerID);
            if (dtContact.Rows.Count > 0)
            {
                lblContact.Text = dtContact.Rows[0]["ContFirst"].ToString() + " " + dtContact.Rows[0]["ContLast"].ToString();
                lblMobile.Text = dtContact.Rows[0]["ContMobile"].ToString();
            }
            try
            {
                lblInstallDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
            }
            catch { }

            lblSalesRep.Text = stPro.SalesRepName;
            if (stPro.Installer != string.Empty)
            {
                lblInstaller.Text = stPro.InstallerName;
            }
            else
            {
                lblInstaller.Text = "N/A";
            }
            if (stPro.Electrician != string.Empty)
            {
                lblMeterElec.Text = stPro.ElectricianName;
            }
            else
            {
                lblMeterElec.Text = "N/A";
            }
            if (stPro.PanelBrandID != string.Empty && stPro.InverterDetailsID != string.Empty)
            {
                lblSystem.Text = stPro.SystemDetails;
            }
        }


        
        
            
    }
    protected void btnAddPanel_Click(object sender, EventArgs e)
    {
        openModalPanel = true;
        ModalPopupExtenderAddPanel.Show();

        string salestype = "1";

        string ProjectID = Request.QueryString["proid"];
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        string PanelBrandID = ddlPanel.SelectedValue; //ID
        string PanelBrand = txtPanelBrand.Text;
        string PanelOutput = txtWatts.Text;
        string RECRebate = txtRebate.Text;
        string STCMultiplier = txtSTCMult.Text;
        string STCZoneRating = txtZoneRt.Text;
        string PanelModel = txtPanelModel.Text;
        string NumberPanels = txtNoOfPanels.Text;
        string SystemCapKW = txtSystemCapacity.Text;

        string PanelDetails = "";
        if (ddlPanel.SelectedValue != string.Empty)
        {
            SttblStockItems stPanel = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlPanel.SelectedValue);
            if (ddlPanel.SelectedValue != "")
            {
                PanelDetails = txtNoOfPanels.Text + " X " + txtWatts.Text + " Watt " + stPanel.StockItem + " Panels. (" + txtPanelModel.Text + ")";
            }
        }

        string InverterDetailsID = ddlInverter.SelectedValue; //Id
        string InverterBrand = txtInverterBrand.Text;
        string InverterSeries = txtSeries.Text;
        string SecondInverterDetailsID = ddlInverter2.SelectedValue;
        string ThirdInverterDetailsID = ddlInverter3.SelectedValue;
        string STCNumber = txtSTCNo.Text;

        DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
        string STCValue = "0";
        if (dtSTCValue.Rows.Count > 0)
        {
            STCValue = dtSTCValue.Rows[0]["STCValue"].ToString();
        }
        string InverterModel = txtInverterModel.Text;
        string inverterqty = txtqty.Text;
        string inverterqty2 = txtqty2.Text;
        string inverterqty3 = txtqty3.Text;
        string InverterOutput = txtInverterOutput.Text;

        string InverterDetails = "";
        if (ddlInverter.SelectedValue != "")
        {
            SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
            InverterDetails = "One " + stInverter.StockItem + " KW Inverter ";
        }
        if (ddlInverter2.SelectedValue != "")
        {
            SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter2.SelectedValue);
            InverterDetails = "One " + stInverter2.StockItem + " KW Inverter";
        }
        if (ddlInverter.SelectedValue != "" && ddlInverter2.SelectedValue != "")
        {
            SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
            SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter2.SelectedValue);
            InverterDetails = "One " + stInverter.StockItem + " KW Inverter. Plus " + stInverter2.StockItem + " KW Inverter.";
        }

        string SystemDetails = "";
        if (ddlPanel.SelectedValue != "")
        {
            SttblStockItems stPanel = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlPanel.SelectedValue);
            SystemDetails = txtNoOfPanels.Text + " X " + stPanel.StockItem;
        }
        if (ddlInverter.SelectedValue != "")
        {
            SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
            SystemDetails += " + " + stInverter.StockItem;
        }
        if (ddlInverter2.SelectedValue != "")
        {
            SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter2.SelectedValue);
            SystemDetails += " + " + stInverter2.StockItem;
        }

       
        DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(stPro.InstallPostCode);
        if (dtZoneCode.Rows.Count > 0)
        {
            string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
            DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
            txtZoneRt.Text = dtZoneRt.Rows[0]["STCRating"].ToString();
        }
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;

        if (txtNoOfPanels.Text != stPro.NumberPanels)
        {
            //bool sucSalePrice = ClsProjectSale.tblProjects_UpdateSalePrice(ProjectID);

            //add by jigar chokshi
            //bool sucSaleTotalQuote = ClsProjectSale.tblProjects_UpdateTotalQuotePrice(ProjectID, "0");
            //int sucpanelslog = ClsProjectSale.tblPanelsUser_log_insert(ProjectID, txtNoOfPanels.Text, userid);
        }
        //bool successSale = ClsProjectSale.tblProjects_UpdateSale(ProjectID, PanelBrandID, PanelBrand, PanelOutput, RECRebate, STCMultiplier, STCZoneRating, PanelModel, NumberPanels, SystemCapKW, PanelDetails, InverterDetailsID, InverterBrand, InverterSeries, SecondInverterDetailsID, STCNumber, STCValue, InverterModel, InverterOutput, InverterDetails, SystemDetails, ElecDistributorID, ElecDistApprovelRef, ElecRetailerID, RegPlanNo, HouseTypeID, RoofTypeID, RoofAngleID, LotNumber, NMINumber, MeterNumber1, MeterNumber2, MeterPhase, MeterEnoughSpace, OffPeak, UpdatedBy, ElecDistApplied, ElecDistApplyMethod, ElecDistApplySentFrom, FlatPanels, PitchedPanels, SurveyCerti, ElecDistAppDate, ElecDistAppBy, CertiApprove, ThirdInverterDetailsID);
        // add by roshni

        //add by jigar chokshi
        bool successSale_inverterqty = ClsProjectSale.tblProjects_UpdateSale_inverterqty(ProjectID, inverterqty, inverterqty2, inverterqty3);


        ClsProjectSale.tblProjects_UpdateSalesType(ProjectID, salestype);

        int Tracksuccess = ClstblTrackPanelDetails.tblTrackPanelDetails_Insert(ProjectID, DateTime.Now.ToString(), userid, PanelBrandID, PanelBrand, RECRebate, NumberPanels, PanelOutput, STCMultiplier, SystemCapKW, InverterDetailsID, InverterBrand, InverterSeries, SecondInverterDetailsID, STCNumber, InverterModel, InverterOutput);


        if (stPro.ProjectTypeID == "8")
        {
           // ClsProjectSale.tblProjectMaintenance_UpdateDetail(ProjectID, txtOpenDate.Text.Trim(), ddlProjectMtceReasonID.SelectedValue, ddlProjectMtceReasonSubID.SelectedValue, ddlProjectMtceCallID.SelectedValue, ddlProjectMtceStatusID.SelectedValue, Convert.ToString(chkWarranty.Checked), txtCustomerInput.Text, txtFaultIdentified.Text, txtActionRequired.Text, txtWorkDone.Text);
        }

        activemethod();
       
    }

    public void activemethod()//put by roshni
    {
        string ProjectID = Request.QueryString["proid"];

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (stPro.ElecDistributorID == "13")
        {

            if ((stPro.FinanceWithID == "4" || stPro.FinanceWithID == string.Empty))
            {
                //Response.Write(stPro.DocumentVerified);
                //Response.End();
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.RegPlanNo != string.Empty && stPro.LotNumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.meterupgrade != string.Empty)
                {
                    //Response.Write(stPro.DocumentVerified );
                    //Response.End();
                    if (stPro.InstallState == "VIC")
                    {
                        if (!string.IsNullOrEmpty(stPro.VicAppReference))
                        {
                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    else
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
            }
            else
            {
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.RegPlanNo != string.Empty && stPro.LotNumber != string.Empty && stPro.DocumentVerified != false.ToString())
                {
                    if (stPro.InstallState == "VIC")
                    {
                        if (!string.IsNullOrEmpty(stPro.VicAppReference))
                        {
                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    else
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
            }

                if ((stPro.ElecDistApprovelRef == string.Empty || stPro.QuoteAccepted == string.Empty || stPro.SignedQuote == string.Empty || stPro.ElecDistributorID == string.Empty || stPro.NMINumber == string.Empty || stPro.RegPlanNo == string.Empty || stPro.LotNumber == string.Empty || stPro.meterupgrade == string.Empty) && stPro.DepositReceived != string.Empty)
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
                }

        }
        else if (stPro.ElecDistributorID == "12")
        {
            if ((stPro.FinanceWithID == "4" || stPro.FinanceWithID == string.Empty))
            {
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.meterupgrade != string.Empty)
                {
                    if (stPro.InstallState == "VIC")
                    {
                        if (!string.IsNullOrEmpty(stPro.VicAppReference))
                        {
                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    else
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
            }
            else
            {
                if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.DocumentVerified != false.ToString() && stPro.meterupgrade != string.Empty)
                {
                    if (stPro.InstallState == "VIC")
                    {
                        if (!string.IsNullOrEmpty(stPro.VicAppReference))
                        {
                            bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                        }
                    }
                    else
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
            }
                if ((stPro.ElecDistApprovelRef == string.Empty || stPro.QuoteAccepted == string.Empty || stPro.SignedQuote == string.Empty || stPro.ElecDistributorID == string.Empty || stPro.NMINumber == string.Empty || stPro.meterupgrade == string.Empty) && stPro.DepositReceived != string.Empty)
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "8");
                }
        }
        else
        {
            if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty && stPro.DocumentVerified != false.ToString())
            {
                if (stPro.InstallState == "VIC")
                {
                    if (!string.IsNullOrEmpty(stPro.VicAppReference))
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
                else
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                }
            }
            else if (stPro.SignedQuote != string.Empty && stPro.DepositReceived != string.Empty && stPro.QuoteAccepted != string.Empty && stPro.ElecDistApprovelRef != string.Empty && stPro.NMINumber != string.Empty)
            {
                if (stPro.InstallState == "VIC")
                {
                    if (!string.IsNullOrEmpty(stPro.VicAppReference))
                    {
                        bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                    }
                }
                else
                {
                    bool suc = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
                }
            }
        }



    }
    protected void btnAddInverter_Click(object sender, EventArgs e)
    {
        openModalInverter = true;
        ModalPopupExtenderAddInverter.Show();

        string ProjectID = Request.QueryString["proid"];
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

    }
    protected void ddlPanel_SelectedIndexChanged(object sender, EventArgs e)
    {
           PanAddUpdate.Visible = true;

           SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);

        DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(st2.InstallPostCode);
        if (dtZoneCode.Rows.Count > 0)
        {
            string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
            DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
            txtZoneRt.Text = dtZoneRt.Rows[0]["STCRating"].ToString();
        }
        if (ddlPanel.SelectedValue != string.Empty)
        {
            SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlPanel.SelectedValue);
            txtPanelBrand.Text = st.StockManufacturer;
            txtPanelModel.Text = st.StockModel;
            txtWatts.Text = st.StockSize;

            if (!string.IsNullOrEmpty(st.StockSize) && !string.IsNullOrEmpty(txtNoOfPanels.Text))
            {
                SystemCapacity = (Convert.ToDecimal(st.StockSize) * Convert.ToDecimal(txtNoOfPanels.Text)) / 1000;
                txtSystemCapacity.Text = Convert.ToString(SystemCapacity);
            }

            txtSTCMult.Text = "1";
            DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
            if (dtSTCValue.Rows.Count > 0)
            {
                try
                {
                    txtSTCValue.Text = SiteConfiguration.ChangeCurrencyVal(dtSTCValue.Rows[0]["STCValue"].ToString());
                }
                catch { }
            }

            //-----------------------------------------------------------------------------------------------------11/12/18

            string installbooking = st2.InstallBookingDate;
            if (installbooking == null || installbooking == "")
            {
                DateTime currentdate = ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now);
                String year = currentdate.Year.ToString();
                DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                stcno = ((SystemCapacity) * Convert.ToDecimal(txtZoneRt.Text) * stcrate);
            }
            else
            {
                string date = Convert.ToDateTime(installbooking).ToShortDateString();
                DateTime dt1 = Convert.ToDateTime(date);
                String year = dt1.Year.ToString();
                DataTable dt = ClstblProjects.tblSTCYearWiseRate_SelectByYear(year);
                Decimal stcrate = Convert.ToDecimal(dt.Rows[0]["STCRate"]);
                stcno = ((SystemCapacity) * Convert.ToDecimal(txtZoneRt.Text) * stcrate);
            }

            ////--------------------------------------------------------------11/12/18------------------------------------------


            //-----------------------------------------------------------------------------------------------------
            //stcno = (Convert.ToDecimal(SystemCapacity) * Convert.ToDecimal(txtZoneRt.Text) * 15);
            //----------------------------------------------------------------------------------------------------
            int myInt = (int)Math.Round(stcno);
            txtSTCNo.Text = Convert.ToString(myInt);
            rebate = myInt * Convert.ToDecimal(txtSTCValue.Text);
            txtRebate.Text = Convert.ToString(rebate);
        }
        else
        {
            txtPanelBrand.Text = string.Empty;
            txtPanelModel.Text = string.Empty;
            txtWatts.Text = string.Empty;
        }
        //ScriptManager.RegisterStartupScript(panprojectsale,
        //                                   this.GetType(),
        //                                   "MyAction",
        //                                   "doMyAction();",
        //                                   true);
        // PanAddUpdate.Visible = true;
        //if (ddlPanel.SelectedValue != string.Empty)
        //{
        //    SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlPanel.SelectedValue);
        //    txtPanelBrand.Text = st.StockManufacturer;
        //    txtPanelModel.Text = st.StockModel;
        //    txtWatts.Text = st.StockSize;

        //    if (!string.IsNullOrEmpty(st.StockSize) && !string.IsNullOrEmpty(txtNoOfPanels.Text))
        //    {
        //        SystemCapacity = (Convert.ToDecimal(st.StockSize) * Convert.ToDecimal(txtNoOfPanels.Text)) / 1000;
        //        txtSystemCapacity.Text = Convert.ToString(SystemCapacity);
        //    }

        //    txtSTCMult.Text = "1";
        //    DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
        //    if (dtSTCValue.Rows.Count > 0)
        //    {
        //        try
        //        {
        //            txtSTCValue.Text = SiteConfiguration.ChangeCurrencyVal(dtSTCValue.Rows[0]["STCValue"].ToString());
        //        }
        //        catch { }
        //    }

        //    if (SystemCapacity != 0 && txtZoneRt.Text != string.Empty)
        //    {
        //        stcno = (Convert.ToDecimal(SystemCapacity) * Convert.ToDecimal(txtZoneRt.Text) * 15);
        //        int myInt = (int)Math.Round(stcno);
        //        txtSTCNo.Text = Convert.ToString(myInt);
        //        rebate = myInt * Convert.ToDecimal(txtSTCValue.Text);
        //        txtRebate.Text = Convert.ToString(rebate);
        //    }
        //}
        //else
        //{
        //    txtPanelBrand.Text = string.Empty;
        //    txtPanelModel.Text = string.Empty;
        //    txtWatts.Text = string.Empty;
        //}
    }
    protected void txtNoOfPanels_TextChanged(object sender, EventArgs e)
    {

    }
    protected void ddlInverter_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        if (ddlInverter.SelectedValue != string.Empty)
        {
            SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
            txtInverterBrand.Text = st.StockManufacturer;
            txtInverterModel.Text = st.StockModel;
            txtSeries.Text = st.StockSeries;

            if (ddlInverter.SelectedValue != "" && ddlInverter2.SelectedValue != "")
            {
                SttblStockItems st2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter2.SelectedValue);
                output = Convert.ToDecimal(st.StockSize) + Convert.ToDecimal(st2.StockSize);
                txtInverterOutput.Text = Convert.ToString(output);
            }
            if (ddlInverter.SelectedValue == "" && ddlInverter2.SelectedValue != "")
            {
                SttblStockItems st2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter2.SelectedValue);
                txtInverterOutput.Text = Convert.ToString(st2.StockSize);
            }
            if (ddlInverter.SelectedValue != "" && ddlInverter2.SelectedValue == "")
            {
                SttblStockItems st2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
                txtInverterOutput.Text = Convert.ToString(st.StockSize);
            }
            if (ddlInverter.SelectedValue == "" && ddlInverter2.SelectedValue == "")
            {
                txtInverterOutput.Text = string.Empty;
            }
        }
        else
        {
            txtInverterBrand.Text = string.Empty;
            txtInverterModel.Text = string.Empty;
            txtSeries.Text = string.Empty;
            if (ddlInverter2.SelectedValue == string.Empty)
            {
                txtInverterOutput.Text = string.Empty;
            }
        }
    }
    protected void ddlInverter2_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        if (ddlInverter2.SelectedValue != string.Empty)
        {
            SttblStockItems st2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter2.SelectedValue);

            if (ddlInverter2.SelectedValue != "" && ddlInverter.SelectedValue != "")
            {
                SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
                output = Convert.ToDecimal(st2.StockSize) + Convert.ToDecimal(st.StockSize);
                txtInverterOutput.Text = Convert.ToString(output);
            }
            if (ddlInverter2.SelectedValue == "" && ddlInverter.SelectedValue != "")
            {
                SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
                txtInverterOutput.Text = Convert.ToString(st.StockSize);
            }

            if (ddlInverter.SelectedValue != "")
            {
                SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
                output = Convert.ToDecimal(st.StockSize) + Convert.ToDecimal(st2.StockSize);
                txtInverterOutput.Text = Convert.ToString(output);
            }
            else
            {
                txtInverterOutput.Text = st2.StockSize;
            }
        }

    }
    protected void txtMtceDiscount_TextChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        if (txtMtceDiscount.Text != string.Empty)
        {
            decimal balance = Convert.ToDecimal(txtMtceCost.Text) - Convert.ToDecimal(txtMtceDiscount.Text);
            txtMtceBalance.Text = Convert.ToString(balance);
        }
    }
    protected void ibtnEditInverter_Click(object sender, EventArgs e)
    {
        ibtnEditPanel_Click(sender, e);
    }
    protected void ibtnEditPanel_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        string salestype = "1";
        
        string PanelBrandID = ddlPanel.SelectedValue; //ID
        string PanelBrand = txtPanelBrand.Text;
        string PanelOutput = txtWatts.Text;
        string RECRebate = txtRebate.Text;
        string STCMultiplier = txtSTCMult.Text;
        string STCZoneRating = txtZoneRt.Text;
        string PanelModel = txtPanelModel.Text;
        string NumberPanels = txtNoOfPanels.Text;
        string SystemCapKW = txtSystemCapacity.Text;

        string PanelDetails = "";
        if (ddlPanel.SelectedValue != string.Empty)
        {
            SttblStockItems stPanel = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlPanel.SelectedValue);
            if (ddlPanel.SelectedValue != "")
            {
                PanelDetails = txtNoOfPanels.Text + " X " + txtWatts.Text + " Watt " + stPanel.StockItem + " Panels. (" + txtPanelModel.Text + ")";
            }
        }

        string InverterDetailsID = ddlInverter.SelectedValue; //Id
        string InverterBrand = txtInverterBrand.Text;
        string InverterSeries = txtSeries.Text;
        string SecondInverterDetailsID = ddlInverter2.SelectedValue;
        string ThirdInverterDetailsID = ddlInverter3.SelectedValue;
        string STCNumber = txtSTCNo.Text;

        DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
        string STCValue = "0";
        if (dtSTCValue.Rows.Count > 0)
        {
            STCValue = dtSTCValue.Rows[0]["STCValue"].ToString();
        }
        string InverterModel = txtInverterModel.Text;
        string inverterqty = txtqty.Text;
        string inverterqty2 = txtqty2.Text;
        string inverterqty3 = txtqty3.Text;
        string InverterOutput = txtInverterOutput.Text;

        string InverterDetails = "";
        if (ddlInverter.SelectedValue != "")
        {
            SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
            InverterDetails = "One " + stInverter.StockItem + " KW Inverter ";
        }
        if (ddlInverter2.SelectedValue != "")
        {
            SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter2.SelectedValue);
            InverterDetails = "One " + stInverter2.StockItem + " KW Inverter";
        }
        if (ddlInverter.SelectedValue != "" && ddlInverter2.SelectedValue != "")
        {
            SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
            SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter2.SelectedValue);
            InverterDetails = "One " + stInverter.StockItem + " KW Inverter. Plus " + stInverter2.StockItem + " KW Inverter.";
        }

        string SystemDetails = "";
        if (ddlPanel.SelectedValue != "")
        {
            SttblStockItems stPanel = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlPanel.SelectedValue);
            SystemDetails = txtNoOfPanels.Text + " X " + stPanel.StockItem;
        }
        if (ddlInverter.SelectedValue != "")
        {
            SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter.SelectedValue);
            SystemDetails += " + " + stInverter.StockItem;
        }
        if (ddlInverter2.SelectedValue != "")
        {
            SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(ddlInverter2.SelectedValue);
            SystemDetails += " + " + stInverter2.StockItem;
        }

    
        DataTable dtZoneCode = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(stPro.InstallPostCode);
        if (dtZoneCode.Rows.Count > 0)
        {
            string STCZoneID = dtZoneCode.Rows[0]["STCZoneID"].ToString();
            DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
            txtZoneRt.Text = dtZoneRt.Rows[0]["STCRating"].ToString();
        }
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;

        if (txtNoOfPanels.Text != stPro.NumberPanels)
        {
            //bool sucSalePrice = ClsProjectSale.tblProjects_UpdateSalePrice(ProjectID);

            //add by jigar chokshi
            //bool sucSaleTotalQuote = ClsProjectSale.tblProjects_UpdateTotalQuotePrice(ProjectID, "0");
            int sucpanelslog = ClsProjectSale.tblPanelsUser_log_insert(ProjectID, txtNoOfPanels.Text, userid);
        }
        bool successSale = ClsProjectSale.tblProjects_UpdateSale(ProjectID, PanelBrandID, PanelBrand, PanelOutput, RECRebate, STCMultiplier, STCZoneRating, PanelModel, NumberPanels, SystemCapKW, PanelDetails, InverterDetailsID, InverterBrand, InverterSeries, SecondInverterDetailsID, STCNumber, STCValue, InverterModel, InverterOutput, InverterDetails, SystemDetails, "", "", "", "", "", "", "", "", "", "", "", "", "", "", UpdatedBy, "", "", "", "", "", "", "", "", "", ThirdInverterDetailsID,"","");
        // add by roshni
       // bool successSale_meterupgrade = ClsProjectSale.tblProjects_UpdateSale_meterupgrade(ProjectID, ddlmeterupgrade.SelectedValue);//
        //add by jigar chokshi
        bool successSale_inverterqty = ClsProjectSale.tblProjects_UpdateSale_inverterqty(ProjectID, inverterqty, inverterqty2, inverterqty3);

       // bool sucQuote2 = ClsProjectSale.tblProjects2_UpdateQuote(ProjectID, SPAInvoiceNumber, SPAPaid, SPAPaidBy, SPAPaidAmount, SPAPaidMethod);

        ClsProjectSale.tblProjects_UpdateSalesType(ProjectID, salestype);

        int Tracksuccess = ClstblTrackPanelDetails.tblTrackPanelDetails_Insert(ProjectID, DateTime.Now.ToString(), userid, PanelBrandID, PanelBrand, RECRebate, NumberPanels, PanelOutput, STCMultiplier, SystemCapKW, InverterDetailsID, InverterBrand, InverterSeries, SecondInverterDetailsID, STCNumber, InverterModel, InverterOutput);

      


        if (stPro.ProjectTypeID == "8")
        {
            ClsProjectSale.tblProjectMaintenance_UpdateDetail(ProjectID, txtOpenDate.Text.Trim(), ddlProjectMtceReasonID.SelectedValue, ddlProjectMtceReasonSubID.SelectedValue, "", "", Convert.ToString(chkWarranty.Checked), "", "", "", "");
        }

        if (successSale && successSale_inverterqty)
        {
            if(Tracksuccess>0)
            {
                SetAdd1();
            }
            else
            {
                SetError1();
            }
        }
        else
        {
            SetError1();
        }
        activemethod();

    }
    protected void ibtnUpdateDetail_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = stEmp.EmployeeID;

        string ProjectMtceReasonID = ddlProjectMtceReasonID.SelectedValue;
        string ProjectMtceReasonSubID = ddlProjectMtceReasonSubID.SelectedValue;

        string OpenDate = SiteConfiguration.ConveqrtToSqlDate_View1(txtOpenDate.Text);


        string PropResolutionDate = SiteConfiguration.ConveqrtToSqlDate_View1(txtPropResolutionDate.Text);
        string CompletionDate = SiteConfiguration.ConveqrtToSqlDate_View1(txtCompletionDate.Text);

        string CurrentPhoneContact = txtCurrentPhoneContact.Text;
        string Warranty = chkWarranty.Checked.ToString();
        string MtceCost = txtMtceCost.Text;
        string ServiceCost = txtServiceCost.Text;
        string MtceDiscount = txtMtceDiscount.Text;
        string MtceBalance = txtMtceBalance.Text;
      
        string InstallerID = ddlInstallerAssigned.SelectedValue;
        //Response.Write(SiteConfiguration.ConveqrtToSqlDate_View1(txtOpenDate.Text) + "===" + DateTime.Now.ToString() + "===" + SiteConfiguration.ConveqrtToSqlDate_View1(txtCompletionDate.Text));
        //Response.End();
        int success =  ClstblProjectMaintenance.tblProjectMaintenance_Insert(ProjectID, EmployeeID, ProjectMtceReasonID, ProjectMtceReasonSubID, "", "", OpenDate, "", "", "", "", "", PropResolutionDate, CompletionDate , "", CurrentPhoneContact, Warranty, "False", "", "", "", "", "", "", "", "", "", "", "", "", MtceCost, MtceDiscount, MtceBalance, "", "", ServiceCost);
        //--- do not chage this code start------
      
      
        if (Convert.ToString(success) != "")
        {
            SetAdd1();
        }
        else
        {
            SetError1();
        }
    }

   
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
}