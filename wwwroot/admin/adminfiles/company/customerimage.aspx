﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" CodeFile="customerimage.aspx.cs" Inherits="admin_adminfiles_company_customerimage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .lightBoxGallery {
            text-align: center;
        }

            .lightBoxGallery a {
                margin: 5px;
                display: inline-block;
            }
    </style>
    <!-- Vendor styles -->
    <%--<link rel="stylesheet" href="../../../vendor/fontawesome/css/font-awesome.css" />--%>
    <%--  <link href="../../../vendor/metisMenu/dist/metisMenu.css" rel="stylesheet" />--%>
    <%--<link rel="stylesheet" href="../../../vendor/animate.css/animate.css" />
    <link rel="stylesheet" href="../../../vendor/bootstrap/dist/css/bootstrap.css" />--%>
    <%--  <link rel="stylesheet" href="../../../vendor/blueimp-gallery/css/blueimp-gallery.min.css" />--%>
    <!-- App styles -->
    <%-- <link href="../../../fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <link rel="stylesheet" href="../../../fonts/pe-icon-7-stroke/css/helper.css" />--%>
    <%--<link rel="stylesheet" href="styles/style.css">--%>
    <%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="cc1" %>
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');
        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
            $('.loading-container').css('display', 'none');
        }
    </script>
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {
                    $(".myvalcusttype").select2({
                        placeholder: "select",
                        allowclear: true
                    });

                    //$('.i-checks').iCheck({
                    //    checkboxClass: 'icheckbox_square-green',
                    //    radioClass: 'iradio_square-green'
                    //});
                    $('.redreq').click(function () {
                        formValidate();
                    });
                }




            </script>
            <script>
                function ComfirmDelete(event, ctl) {
                    event.preventDefault();
                    var defaultAction = $(ctl).prop("href");

                    swal({
                        title: "Are you sure you want to delete this Image?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, delete it!",
                        cancelButtonText: "No, cancel!",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                      function (isConfirm) {
                          if (isConfirm) {
                              // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                              eval(defaultAction);

                              //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                              return true;
                          } else {
                              // swal("Cancelled", "Your imaginary file is safe :)", "error");
                              return false;
                          }
                      });
                }
            </script>
                    </ContentTemplate>
    </asp:UpdatePanel>
            <div class="finalheader">
                <div class="small-header transition animated fadeIn">
                    <div class="hpanel">
                        <div class="panel-body">
                            <div id="hbreadcrumb" class="pull-right">
                                <ol class="hbreadcrumb breadcrumb fontsize16">
                                    <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-info"><i class="fa fa-plus"></i> Add</asp:LinkButton>
                                    <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-info"><i class="fa fa-chevron-left"></i> Back</asp:LinkButton>
                                </ol>
                            </div>
                            <h2 class="font-light m-b-xs">Manage Customer Image
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="finaladdupdate">
                <div id="PanAddUpdate" runat="server" visible="false">
                    <div class="content animate-panel">
                        <div>
                            <div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="hpanel">
                                            <div class="panel-heading">
                                                <%--<div class="panel-tools">
                                        <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                                        <a class="closebox"><i class="fa fa-times"></i></a>
                                    </div>--%>
                                                <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                                Manage Customer Image
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-horizontal">
                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" class="col-sm-2 control-label">
                                                <strong>Upload Image</strong></asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:FileUpload ID="fu1" runat="server"  />
                                                            &nbsp;&nbsp;<small><i>[.jpg,.jpeg,.png,.gif Only ] </i></small>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  ErrorMessage="Please upload image"
                                                                ControlToValidate="fu1"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="fu1" 
                                                                Display="Dynamic" ErrorMessage="Upload a valid file" ValidationExpression="^.+(.jpg|.JPG|.jpeg|.JPEG|.gif|.GIF|.png|.PNG)$"></asp:RegularExpressionValidator>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-8 col-sm-offset-2">
                                                         <asp:Button CssClass="btn btn-primary redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                                                Text="Add" />
                                                            <%--<asp:Button CssClass="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                                                Text="Save" Visible="false" />--%>
                                                            <%-- <asp:Button CssClass="btn btn-default btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                                CausesValidation="false" Text="Reset" />--%>
                                                            <asp:Button CssClass="btn btn-default btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                                CausesValidation="false" Text="Cancel" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Panel runat="server" ID="PanGridSearch">
                <div class="">
                    <div class="panel-body animate-panelmessesgarea padbtmzero">
                        <div class="alert alert-success" id="PanSuccess" runat="server">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server">
                            <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                Text="Transaction Failed."></asp:Label></strong>
                        </div>
                        <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                        </div>
                        <div class="alert alert-info" id="PanNoRecord" runat="server">
                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                        </div>
                    </div>
                    <div class="searchfinal">
                        <div class="panel-body padbtmzero">
                            <div class="">
                                <div class="hpanel marbtmzero">
                                    <%--<div class="panel-heading">
                                <div class="panel-tools">
                                    <a class="showhide"><i class="fa fa-chevron-up"></i></a>
                                    <a class="closebox"><i class="fa fa-times"></i></a>
                                </div>
                                <br />
                            </div>--%>
                                    <%--<div class="panel-body marbtmzero">
                                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                            <div class="">
                                                <div class="dataTables_filter ">
                                                    <div class="dataTables_filter">
                                                        <table border="0" cellspacing="0" width="100%" style="text-align: right; margin-bottom: 10px;" cellpadding="0">
                                                            <tr>
                                                                <td class="left-text dataTables_length showdata">Show
                                                            <asp:DropDownList ID="ddlSelectRecords" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                aria-controls="DataTables_Table_0" class="myvalcusttype">
                                                            </asp:DropDownList>
                                                                    entries
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="finalgrid">
                        <div class="panel-body">
                            <div class="">
                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                    <div class="Row">
                                        <div id="PanGrid" runat="server">
                                            <div class="table-responsive">
                                                <div class="hpanel">
                                                    <%-- <div class="panel-body">--%>

                                                    <div class="lightBoxGallery">
                                                        <asp:Repeater ID="RepeaterImages" runat="server" OnItemCommand="RepeaterImages_ItemCommand">
                                                            <ItemTemplate>
                                                                <div class="galleryimage">
                                                                    <asp:HyperLink ID="hyp1" runat="server" NavigateUrl='<%# "~/userfiles/customerimage/" +Eval("Imagename") %>' title="Image from Unsplash" data-gallery="">
                                                                        <asp:Image ID="imgcustomer" runat="server" ImageUrl='<%# "~/userfiles/customerimage/" +Eval("Imagename") %>' CssClass="wdth100" />
                                                                    </asp:HyperLink>
                                                                    <asp:LinkButton ID="gvbtnDelete" runat="server" CommandName="Delete" CausesValidation="false"  class="btndeleted" CommandArgument='<%# Eval("CustImageID") %>'>
                                                                               <i class="fa fa-times-circle-o"></i>
                                                                    </asp:LinkButton>
                                                                    <%--   <a href="#" class="btndeleted"><i class="fa fa-times-circle-o"></i></a>--%>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <%--    <a href="../../images/gallery/1.jpg" title="Image from Unsplash" data-gallery=""><img src="../../images/gallery/1s.jpg"></a>
                            <a href="../../images/gallery/2.jpg" title="Image from Unsplash" data-gallery=""><img src="../../images/gallery/2s.jpg"></a>
                            <a href="../../images/gallery/3.jpg" title="Image from Unsplash" data-gallery=""><img src="../../images/gallery/3s.jpg"></a>
                            <a href="../../images/gallery/4.jpg" title="Image from Unsplash" data-gallery=""><img src="../../images/gallery/4s.jpg"></a>
                            <a href="../../images/gallery/5.jpg" title="Image from Unsplash" data-gallery=""><img src="../../images/gallery/5s.jpg"></a>
                            <a href="../../images/gallery/6.jpg" title="Image from Unsplash" data-gallery=""><img src="../../images/gallery/6s.jpg"></a>
                            <a href="../../images/gallery/7.jpg" title="Image from Unsplash" data-gallery=""><img src="../../images/gallery/7s.jpg"></a>
                            <a href="../../images/gallery/8.jpg" title="Image from Unsplash" data-gallery=""><img src="../../images/gallery/8s.jpg"></a>
                            <a href="../../images/gallery/9.jpg" title="Image from Unsplash" data-gallery=""><img src="../../images/gallery/9s.jpg"></a>
                            <a href="../../images/gallery/10.jpg" title="Image from Unsplash" data-gallery=""><img src="../../images/gallery/10s.jpg"></a>--%>
                                                    </div>
                                                    <%--    </div>--%>
                                                    <%--  <div class="panel-footer">
                        <i class="fa fa-picture-o"> </i> 20 pimages
                    </div>--%>
                                                </div>
                                            </div>
                                            <div id="blueimp-gallery" class="blueimp-gallery">
                                                <div class="slides"></div>
                                                <h3 class="title"></h3>
                                                <a class="prev">‹</a>
                                                <a class="next">›</a>
                                                <a class="close">×</a>
                                                <a class="play-pause"></a>
                                                <ol class="indicator"></ol>
                                            </div>

                                            <%-- <asp:GridView ID="GridView1" DataKeyNames="CustImageID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" 
                                                    OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Photo" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Image ID="imgcustomer" runat="server" ImageUrl='<%# "~/userfiles/customerimage/" +Eval("Imagename") %>' Height="30px" Width="30px" Visible='<%# Eval("Imagename").ToString()!=""?true:false %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="35px" HeaderStyle-Width="35px" HeaderText="Delete" HeaderStyle-CssClass="brnone spcalmarandpaddnone">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="gvbtnDelete" runat="server" CommandName="Delete" CausesValidation="false" OnClientClick="return ComfirmDelete(event,this)"
                                                                    data-toggle="tooltip" data-placement="top" title="" data-original-title="delete">
                                                                                <i class="fa fa-trash"></i>
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <AlternatingRowStyle />

                                                    <PagerTemplate>
                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                        <div class="pagination">
                                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                        </div>
                                                    </PagerTemplate>
                                                    <PagerStyle CssClass="paginationGrid" />
                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                </asp:GridView>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </asp:Panel>
    
           <!--Danger Modal Templates-->
                    <asp:Button ID="btndelete" Style="display:block;" Text="hello" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
         PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete" >
    </cc1:ModalPopupExtender>
    <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">
     
       <div class="modal-dialog " style="margin-top:-300px">
            <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                <div class="modal-header text-center">
                    <i class="glyphicon glyphicon-fire"></i>
                </div>
                          
                                  
                <div class="modal-title">Delete</div>
                <label id="ghh" runat="server" ></label>
                <div class="modal-body ">Are You Sure Delete This Entry?</div>
                <div class="modal-footer " style="text-align:center">
                    <asp:LinkButton ID="lnkdelete"  runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel"  runat="server"  class="btn"  data-dismiss="modal"  >Cancel</asp:LinkButton>
                </div>
            </div>
        </div> 
         
    </div>

           <asp:HiddenField ID="hdndelete" runat="server" />  
  
<!--End Danger Modal Templates-->
</asp:Content>




