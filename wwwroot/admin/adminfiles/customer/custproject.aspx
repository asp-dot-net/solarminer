<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" Theme="admin"
    CodeFile="custproject.aspx.cs" Inherits="admin_adminfiles_customer_custproject" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<style type="text/css">
        .selected_row {
            background-color: #A1DCF2!important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>
         <div class="page-body headertopbox">
              <h5 class="row-title"><i class="typcn typcn-th-small"></i>Customer System Detail</h5>
              <div class="pull-right">
                               
                  
                            </div>
               </div>
    <div class="page-body">
    <div class="messesgarea">
        <div class="alert alert-info">
                    <asp:Panel ID="PanNoRecord" runat="server" CssClass="failure">
                        <i class="icon-remove-sign"></i>
                        <asp:Label ID="lblNoRecord" runat="server" Text="There are no items to show in this view."></asp:Label>
                    </asp:Panel>
        </div>
                </div>
    </div>
    
    <section class="row m-b-md">
        <div class="col-sm-12 minhegihtarea">
            <h3 class="m-b-xs text-black"></h3>
            <div class="contactsarea">
                
                <div class="contactbottomarea">
                    <div class="tableblack">
                        <div class="table-responsive" id="PanGrid" runat="server">
                            <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server"
                                OnPageIndexChanging="GridView1_PageIndexChanging" AllowSorting="true">
                                <Columns>
                                    <asp:TemplateField HeaderText="ProjectNumber" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%#Eval("ProjectNumber")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ProjectOpened" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%#Eval("ProjectOpened","{0:dd MMM yyyy}")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Project" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <%#Eval("Project")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Location" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                        <ItemTemplate>
                                            <%#Eval("Location")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ProjectStatus" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="120px">
                                        <ItemTemplate>
                                            <%#Eval("ProjectStatus")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-Width="40px">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hypDetail" runat="server" data-original-title="Detail View" class="btn btn-blue tooltips"
                                                NavigateUrl='<%# "~/admin/adminfiles/customer/customerdetail.aspx?id=" + Eval("ProjectID")%>'> <i class="icon-circle-arrow-right"></i></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

