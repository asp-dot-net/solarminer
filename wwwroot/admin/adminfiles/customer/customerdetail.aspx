<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true" Theme="admin"
    CodeFile="customerdetail.aspx.cs" Inherits="admin_adminfiles_customer_customerdetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<style type="text/css">
        .selected_row {
            background-color: #A1DCF2!important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            HighlightControlToValidate();
            $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
            });
          
        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <section class="row m-b-md">
        <div class="col-sm-12 minhegihtarea">
            <h3 class="m-b-xs text-black">Customer System Detail</h3>
            <div class="contactsarea">
                <div class="addcontent">
                    <asp:LinkButton ID="lnkBack" runat="server" CausesValidation="false" OnClick="lnkBack_Click"><img src="../../../images/btn_back.png" /></asp:LinkButton>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body ">
                                <div class="row printpage">
                                    <div class="col-md-12">
                                        <div class="graybgarea">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4>System Details</h4>
                                                </div>
                                                <div class="row ">
                                                    <div class="clear">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover">
                                                            <tr>
                                                                <td width="200px">System Details</td>
                                                                <td>
                                                                    <asp:Label ID="lblSystemDetails" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Install Date</td>
                                                                <td>
                                                                    <asp:Label ID="lblInstallBookingDate" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Installer</td>
                                                                <td>
                                                                    <asp:Label ID="lblInstaller" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td>House Type</td>
                                                                <td>
                                                                    <asp:Label ID="lblHouseType" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Roof Type</td>
                                                                <td>
                                                                    <asp:Label ID="lblRoofType" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Roof Angle</td>
                                                                <td>
                                                                    <asp:Label ID="lblRoofAngle" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Elec Dist</td>
                                                                <td>
                                                                    <asp:Label ID="lblElecDist" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Elec Retailer</td>
                                                                <td>
                                                                    <asp:Label ID="lblElecRetailer" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Approval Ref</td>
                                                                <td>
                                                                    <asp:Label ID="lblApprovalRef" runat="server"></asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td>NMI Number</td>
                                                                <td>
                                                                    <asp:Label ID="lblNMINumber" runat="server"></asp:Label></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <span></span>
                                    </div>
                                </div>
                                <div class="row printpage">
                                    <div class="col-md-12">
                                        <div class="graybgarea">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4>View Quotation</h4>
                                                </div>
                                                <div class="row ">
                                                    <div class="clear">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                                <div class="col-md-12" id="divQuote" runat="server">
                                                    <div class="form-group">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Quotation Doc
                                                            </label>
                                                        </span>
                                                        <span style="margin-top: 5px;">
                                                            <asp:HyperLink ID="hypDoc" runat="server" Target="_blank">
                                                                <asp:Image ID="imgDoc" runat="server" ImageUrl="~/images/new/icon_document_downalod.png" />
                                                            </asp:HyperLink>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <span></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="graybgarea">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4>Invoice Detail</h4>
                                                    <div align="right" class="printpage" id="divPrint" runat="server">
                                                        <a href="javascript:window.print();">
                                                            <i class="icon-print printpage"></i>Print
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="row ">
                                                    <div class="clear">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <asp:Panel ID="PanNoRecord" runat="server" CssClass="failure">
                                                        <i class="icon-remove-sign"></i>
                                                        <asp:Label ID="lblNoRecord" runat="server" Text="No invoice payment detail."></asp:Label>
                                                    </asp:Panel>
                                                    <div class="panel panel-default" id="PanGrid" runat="server">
                                                        <div class="table-responsive">
                                                            <asp:GridView ID="GridView1" DataKeyNames="InvoicePaymentID" runat="server" ShowFooter="false"
                                                                OnPageIndexChanging="GridView1_PageIndexChanging" AllowSorting="true">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Date Paid" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="90px">
                                                                        <ItemTemplate>
                                                                            <%#Eval("InvoicePayDate","{0:dd-MMM-yyyy}")%>
                                                                        </ItemTemplate>
                                                                        <ItemStyle />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Inv Total" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="110px">
                                                                        <ItemTemplate>
                                                                            <asp:HiddenField ID="hndPrice" runat="server" Value='<%#Eval("TotalQuotePrice")%>' />
                                                                            <%#Eval("TotalQuotePrice","{0:0.00}")%>
                                                                        </ItemTemplate>
                                                                        <ItemStyle />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Amnt Paid" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="110px">
                                                                        <ItemTemplate>
                                                                            <asp:HiddenField ID="hndPayTotal" runat="server" Value='<%#Eval("InvoicePayTotal")%>' />
                                                                            <%#Eval("InvoicePayTotal","{0:0.00}")%>
                                                                        </ItemTemplate>
                                                                        <ItemStyle />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Paid By" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="80px">
                                                                        <ItemTemplate>
                                                                            <%#Eval("InvoicePayMethod")%>
                                                                        </ItemTemplate>
                                                                        <ItemStyle />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 printpage">
                                        <div class="graybgarea">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4>Customer Feedback</h4>
                                                </div>
                                                <div class="row ">
                                                    <div class="clear">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                Feedback
                                                            </label>
                                                        </span>
                                                        <span>
                                                            <asp:TextBox ID="txtFeedback" runat="server" TextMode="MultiLine" CssClass="form-control" Width="500px"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                ControlToValidate="txtFeedback" Display="Dynamic"></asp:RequiredFieldValidator>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="row ">
                                                    <div class="clear">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                                <div class="col-md-12" id="divAddFeedback" runat="server">
                                                    <div class="form-group">
                                                        <span class="name">
                                                            <label class="control-label">
                                                                &nbsp;
                                                            </label>
                                                        </span>
                                                        <span>
                                                            <asp:Button class="btn btn-primary" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                                                Text="Save" />
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <span></span>
                                    </div>
                                </div>
                                <div class="row printpage">

                                    <div>
                                        <span></span>
                                    </div>
                                </div>
                                <div class="row printpage">
                                    <div class="col-md-12">
                                        <div class="graybgarea">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4>Project Stage</h4>
                                                </div>
                                                <div class="row ">
                                                    <div class="clear">
                                                        &nbsp;
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover">
                                                            <tr>
                                                                <td width="150px"><b>Status</b>
                                                                </td>
                                                                <td>
                                                                    <b>Date</b>
                                                                </td>
                                                                <td>
                                                                    <b>Days</b>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>


