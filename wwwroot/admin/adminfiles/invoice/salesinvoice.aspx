<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="salesinvoice.aspx.cs" Inherits="admin_adminfiles_invoice_salesinvoice" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../assets/js/jquery.min.js"></script>
    <script type="text/javascript">

        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.jpg";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.jpg";
            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });

    </script>
    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
            
            <script>
                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();

                function beginrequesthandler(sender, args) {
                    $('.loading-container').css('display', 'block');
                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                }
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function pageLoaded() {
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $("[data-toggle=tooltip]").tooltip();
                    $(".myvalsalesinvoice").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $(".myval").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox();
                    });


                    $(".myval1").select2({
                        minimumResultsForSearch: -1
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }



                    $('.tooltipwidth').tooltip();
                    $('.tooltips').tooltip();
                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox();
                    });

                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });

                    $('.loading-container').css('display', 'none');
                }
            </script>

            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Sales Invoice Tracker</h5>
                <div class="pull-right">
                </div>
            </div>

            <div class="page-body padtopzero padbtmzero">
                <div class="messesgarea">
                    <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                        <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                    </div>
                </div>

                <div class="searchfinal">
                    <div class="widget-body shadownone brdrgray">
                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                            <div class="dataTables_filter">
                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                    <table border="0" cellspacing="0" width="100%" style="text-align: left;" cellpadding="0">
                                        <tr>
                                            <td>

                                                <div class="inlineblock martop5">
                                                    <div>
                                                        <div class="input-group col-sm-2">
                                                            <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtProjectNumber"
                                                                WatermarkText="Project Num" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                ControlToValidate="txtProjectNumber" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                        </div>

                                                        <div class="input-group col-sm-2">
                                                            <asp:TextBox ID="txtMQNsearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender8" runat="server" TargetControlID="txtMQNsearch"
                                                                WatermarkText="Manual Num" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtMQNsearch" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetManualQuoteNumber"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                        </div>

                                                        <div class="input-group col-sm-1">
                                                            <asp:TextBox ID="txtContactSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender16" runat="server" TargetControlID="txtContactSearch"
                                                                WatermarkText="Contact" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender9" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtContactSearch" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetContactList"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                            </cc1:AutoCompleteExtender>
                                                        </div>

                                                        <div class="input-group col-sm-1">
                                                            <asp:TextBox ID="txtSerachCity" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender10" runat="server" TargetControlID="txtSerachCity"
                                                                WatermarkText="Suburb" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtSerachCity" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                            </cc1:AutoCompleteExtender>
                                                        </div>

                                                        <div class="input-group col-sm-1" id="divCustomer" runat="server">
                                                            <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                                CssClass="myvalsalesinvoice" Width="150px">
                                                                <asp:ListItem Value="">State</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group col-sm-1">
                                                            <asp:TextBox ID="txtSearchPostCode" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender13" runat="server" TargetControlID="txtSearchPostCode"
                                                                WatermarkText="PCode" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtSearchPostCode" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                            </cc1:AutoCompleteExtender>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="search"
                                                                ControlToValidate="txtSearchPostCode" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                        </div>

                                                        <div class="input-group col-sm-1" id="div1" runat="server" style="width: 235px;">
                                                            <asp:DropDownList ID="ddlSalesRep" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalsalesinvoice">
                                                                <asp:ListItem Value="">Sales Rep</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="inlineblock martop5">
                                                    <div>
                                                        <div class="input-group spical multiselect col-sm-2">
                                                            <dl class="dropdown">
                                                                <dt>
                                                                    <a href="#">
                                                                        <span class="hida" id="spanselect">Select</span>
                                                                        <p class="multiSel"></p>
                                                                    </a>
                                                                </dt>
                                                                <dd id="ddproject" runat="server">
                                                                    <div class="mutliSelect">
                                                                        <ul>
                                                                            <asp:Repeater ID="lstSearchStatus" runat="server">
                                                                                <ItemTemplate>
                                                                                    <li>
                                                                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />

                                                                                        <asp:CheckBox ID="chkselect" runat="server" />
                                                                                        <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                            <span></span>
                                                                                        </label>

                                                                                        <label class="chkval">
                                                                                            <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                                        </label>

                                                                                    </li>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </ul>
                                                                    </div>
                                                                </dd>
                                                            </dl>
                                                        </div>
                                                        <div class="input-group col-md-2" id="div2" runat="server">
                                                            <asp:DropDownList ID="ddlSearchDate" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalsalesinvoice">
                                                                <asp:ListItem Value="">Select Date</asp:ListItem>
                                                                <asp:ListItem Value="1">Invoice Date</asp:ListItem>
                                                                <asp:ListItem Value="2">Quote Date</asp:ListItem>
                                                                <asp:ListItem Value="3">Sale Date</asp:ListItem>
                                                                <asp:ListItem Value="4">Paid Date</asp:ListItem>
                                                                <asp:ListItem Value="5">Install Date</asp:ListItem>
                                                                <asp:ListItem Value="6">Advance Date</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                        </div>
                                                        <div class="input-group date datetimepicker1 col-sm-2">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                            <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                                ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                        </div>
                                                        <div class="input-group">
                                                            <table>
                                                                <tr>
                                                                    <td class="checkbox-info paddtop3td alignchkbox btnviewallorange">
                                                                        <label for="<%=chkHistoric.ClientID %>" class="btn btn-magenta">
                                                                            <asp:CheckBox ID="chkHistoric" runat="server" />
                                                                            <span class="text">Historic</span>
                                                                        </label>
                                                                    </td>
                                                                    <td></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="input-group">
                                                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:LinkButton ID="btnClearAll" runat="server"
                                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                        </div>
                        <div class="datashowbox martop5">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="dataTables_length showdata">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="padtopzero">
                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-9">
                                    <div id="Div3" class="pull-right" runat="server" style="padding-top: 8px">
                                        <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                        <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                            CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                        <%-- </ol>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="page-body padtopzero">
                <div class="contactbottomarea finalgrid">
                    <div class="marginnone">
                        <div class="tableblack">
                            <div class="table-responsive noPagination" id="PanGrid" runat="server">
                                <asp:GridView ID="GridView1" DataKeyNames="CustomerID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging"
                                    OnDataBound="GridView1_DataBound" OnRowCommand="GridView1_RowCommand" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="20px" HeaderStyle-CssClass="printorder" ItemStyle-CssClass="printorder">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnempid" runat="server" Value='<%# Eval("CustomerID") %>' />
                                                <a href="JavaScript:divexpandcollapse('div<%# Eval("CustomerID") %>','tr<%# Eval("CustomerID") %>');">
                                                    <img id='imgdiv<%# Eval("CustomerID") %>' src="../../../images/icon_plus.jpg" />
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Project#" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="left" SortExpression="ProjectNumber">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProjectNumber" runat="server" Width="80px">
                                                    <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                        NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=proelec&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>
                                                                <%#Eval("ProjectNumber")%></asp:HyperLink>
                                                </asp:Label>
                                                <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Manual#" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="ManualQuoteNumber">
                                            <ItemTemplate>
                                                <asp:Label ID="lblManualQuoteNumber" runat="server" Width="80px"><%#Eval("ManualQuoteNumber")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Contact" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" SortExpression="Contact">
                                            <ItemTemplate>
                                                <asp:Label ID="lblContact" runat="server" Width="120px"><%#Eval("Contact")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField HeaderText="SystemDetails" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSystemDetails" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("SystemDetails")%>'
                                                            Width="250px"><%#Eval("SystemDetails")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Install Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="left" SortExpression="InstallBookingDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInstallBookingDate" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "InstallBookingDate", "{0:dd MMM yyyy}")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--  <asp:TemplateField HeaderText="Rep Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRepName" runat="server" Width="150px"><%#Eval("RepName")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Project Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="ProjectStatus"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProjectStatus" runat="server" Width="90px"><%#Eval("ProjectStatus")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Base Price" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="ServiceValue"
                                            ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBasePrice" runat="server" Width="100px"><%#Eval("ServiceValue","{0:0.00}")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Landing Price" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                    ItemStyle-HorizontalAlign="Right" SortExpression="SalesLandingPrice">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSalesLandingPrice" runat="server" Width="100px"><%#Eval("SalesLandingPrice","{0:0.00}")%></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Market Price" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                    ItemStyle-HorizontalAlign="Right" SortExpression="SalesMarketPrice">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSalesMarketPrice" runat="server" Width="100px"><%#Eval("SalesMarketPrice","{0:0.00}")%></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Finance With" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="FinanceWith"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFinanceWith" runat="server" Width="100px"><%#Eval("FinanceWith")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Fin Dep" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="FinanceWithDeposit"
                                            ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFinanceWithDeposit" runat="server" Width="50px"><%#Eval("FinanceWithDeposit","{0:0.00}")%></asp:Label>
                                                <asp:Label ID="Label1" runat="server" Text="%" Visible='<%#Eval("FinanceWithDeposit").ToString() !=""?true:false %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Commission" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="InvAmnt"
                                            ItemStyle-HorizontalAlign="left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSalesInvAmnt" runat="server" Width="90px"><%#Eval("InvAmnt","{0:0.00}")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Inv #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="SalesInvNo">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSalesInvNo" runat="server" Width="100px"><%#Eval("SalesInvNo")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Issued" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="left" SortExpression="InvDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSalesInvDate" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "InvDate", "{0:dd MMM yyyy}")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--  <asp:TemplateField HeaderText="Paid Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center" SortExpression="SalesPayDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSalesPayDate" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "SalesPayDate", "{0:dd MMM yyyy}")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Advance date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" SortExpression="AdvanceDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAdvanceDate" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "AdvanceDate", "{0:dd MMM yyyy}")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Advance Amount" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="left" SortExpression="AdvanceAmount">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIdvAmnt" runat="server" Width="150px"><%#Eval("AdvanceAmount","{0:0.00}")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Follow-Up" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text"
                                            ItemStyle-HorizontalAlign="Center" SortExpression="FollowUp">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFollUp" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "FollowUp", "{0:dd MMM yyyy}")%></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Paid Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="left" SortExpression="InstallerPayDate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInstallerPayDate" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "paydate", "{0:dd/MMM/yyyy}")%></asp:Label>
                                                <asp:LinkButton ID="lbtnPaid" CommandName="Paid" CssClass="btn btn-sky btn-xs"
                                                    CommandArgument='<%#Eval("InvoiceID") %>' CausesValidation="false" runat="server" Visible='<%#Convert.ToString( Eval("paydate"))==""?true:false%>' data-toggle="tooltip" data-placement="top" data-original-title="Paid Date">
                                                          <i class="fa fa-retweet"></i>  </asp:LinkButton>

                                                <asp:LinkButton ID="lbtnRevert" CommandName="PaidRevert" CssClass="btn btn-maroon btn-xs"
                                                    CommandArgument='<%#Eval("InvoiceID") %>' Visible='<%#Convert.ToString( Eval("paydate"))!=""?true:false%>' CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Revert Paid Date">
                                                          <i class="fa fa-retweet"></i>  </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                            <ItemTemplate>
                                                <tr id='tr<%# Eval("CustomerID") %>' style="display: none;" class="dataTable left-text">
                                                    <td colspan="98%" class="details">
                                                        <div id='div<%# Eval("CustomerID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                            <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                <tr>
                                                                    <td width="150px"><b>SystemDetails</b>
                                                                    </td>
                                                                    <td>

                                                                        <asp:Label ID="Label2" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("SystemDetails")%>'
                                                                            Width="250px"><%#Eval("SystemDetails")%></asp:Label>

                                                                    </td>
                                                                    <td width="150px"><b>Rep Name</b>
                                                                    </td>
                                                                    <td>

                                                                        <asp:Label ID="lblRepName" runat="server" Width="150px"><%#Eval("RepName")%></asp:Label>
                                                                    </td>

                                                                    <td width="150px"><b>Inv #</b>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblSalesInvNo" runat="server" Width="100px"><%#Eval("SalesInvNo")%></asp:Label>
                                                                    </td>
                                                                </tr>

                                                                <tr>

                                                                    <td><b>Paid Date</b>
                                                                    </td>
                                                                    <td runat="server" id="tdtype">
                                                                        <asp:Label ID="lblSalesPayDate" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "PayDate", "{0:dd MMM yyyy}")%></asp:Label>
                                                                    </td>
                                                                    <td><b>Sales Pay Notes</b>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblSalesPayNotes" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("SalesPayNotes")%>'
                                                                            Width="250px"><%#Eval("SalesPayNotes")%></asp:Label>
                                                                    </td>

                                                                </tr>

                                                            </table>

                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <AlternatingRowStyle />

                                    <PagerTemplate>
                                        <div class="printorder">
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            <div class="pagination">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                            </div>
                                        </div>
                                    </PagerTemplate>
                                    <PagerStyle CssClass="paginationGrid" />
                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />

                                </asp:GridView>
                            </div>
                            <div class="paginationnew1" runat="server" id="divnopage">
                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


            <asp:Button ID="btnNULLPaid" Style="display: none;" runat="server" />

            <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="divPaiddate" TargetControlID="btnNULLPaid" CancelControlID="ibtnCancel">
            </cc1:ModalPopupExtender>

            <div id="divPaiddate" runat="server" style="display: none;" class="modal_popup">
                <div class="modal-dialog" style="width: 400px;">
                    <div class="modal-content">
                        <div class="color-line"></div>
                        <div class="modal-header">
                            <div style="float: right">
                                <asp:LinkButton ID="ibtnCancel" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal"
                                    OnClick="ibtnCancel_Click">Close
                                </asp:LinkButton>
                            </div>
                            <h4 class="modal-title" id="myModalLabel1">Change Paid Date</h4>
                        </div>
                        <div class="modal-body paddnone">
                            <div class="panel-body">
                                <div class="formainline">

                                    <div class="form-group marginbtm10 row">
                                        <asp:Label ID="Label1" runat="server" class="col-sm-4 control-label" Style="padding: -right:0px;">
                                                <strong>Paid Date</strong></asp:Label>

                                        <%-- <div class="input-group date datetimepicker1 col-sm-3">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                    <asp:TextBox ID="txtActualPayDate" runat="server" class="form-control" placeholder="Actual Pay Date">
                                    </asp:TextBox>
                                  
                                </div>--%>
                                        <div class="col-sm-8">

                                            <div class="input-group date datetimepicker1 col-sm-12">
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar"></span>
                                                </span>
                                                <asp:TextBox ID="txtpaiddate" placeholder="Paid Date" runat="server" class="form-control"></asp:TextBox>
                                                <%--  <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtActualPayDate"
                                            WatermarkText="Actual Date" />--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group marginbtm10 row">
                                        <asp:Label ID="Label11" runat="server" class="col-sm-4 control-label" Style="padding: -right:0px;">
                                                <strong>Comment</strong></asp:Label>
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtpaidcomment" TextMode="MultiLine" Rows="3" Columns="5" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpaidcomment"
                                                ErrorMessage="This value is required." CssClass="reqerror" ValidationGroup="AddNotesPaid" Display="Dynamic"></asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group col-md-12 center-text">
                                        <asp:Button ID="ibtnAddPAidDate" runat="server" Text="Update" OnClick="ibtnAddPAidDate_Click"
                                            ValidationGroup="AddNotesPaid" CssClass="btn btn-primary savewhiteicon btnsaveicon" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <asp:Button ID="btndelete" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
            </cc1:ModalPopupExtender>
            <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

                <div class="modal-dialog " style="margin-top: -300px">
                    <div class=" modal-content ">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header text-center">
                            <i class="glyphicon glyphicon-fire"></i>
                        </div>


                        <div class="modal-title">Revert</div>
                        <label id="ghh" runat="server"></label>
                        <div class="modal-body ">Are You Sure Revert the Paid Date?</div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                            <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>

            <asp:HiddenField ID="hdndelete" runat="server" />

            <asp:HiddenField ID="hndinvoicepaymentid" runat="server" />

            <asp:HiddenField runat="server" ID="hdncountdata" />

        
    <script type="text/javascript">
                $(".dropdown dt a").on('click', function () {
                    $(".dropdown dd ul").slideToggle('fast');
                });

                $(".dropdown dd ul li a").on('click', function () {
                    $(".dropdown dd ul").hide();
                });


                $(document).bind('click', function (e) {
                    var $clicked = $(e.target);
                    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
                });
                $(document).ready(function () {
                    doMyAction();
                });
                function doMyAction() {



                    //gridviewScroll();
                    //$(".myvalsalesinvoice").select2({
                    //    placeholder: "select",
                    //    allowclear: true
                    //});
                    //$(".myvalsalesinvoice1").select2({
                    //    minimumResultsForSearch: -1
                    //});

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox();
                    });
                }
                //$("#nav").on("click", "a", function () {
                //    $('#content').animate({ opacity: 0 }, 500, function () {
                //        gridviewScroll();
                //        $('#content').delay(250).animate({ opacity: 1 }, 500);
                //    });
                //});

                function callMultiCheckbox() {

                    var title = "";
                    $("#<%=ddproject.ClientID%> :checkbox").each(function () {
                        if (this.checked) {
                            title += ", " + $(this).next("label").next("label.chkval").html();
                            //alert(title);
                        }
                    });

                    if (title != "") {
                        var html = title.substr(1);
                        $('.multiSel').show();
                        $('.multiSel').html(html);
                        $(".hida").hide();
                    }
                    else {
                        $('#spanselect').show();
                        $('.multiSel').hide();
                    }
                }

    </script>
            </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>


