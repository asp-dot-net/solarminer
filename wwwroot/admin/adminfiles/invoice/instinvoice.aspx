<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="instinvoice.aspx.cs" Inherits="admin_adminfiles_invoice_instinvoice" Culture="en-GB" UICulture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
               </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lbtnExport" />
            <asp:PostBackTrigger ControlID="GridView1" />
        </Triggers>
    </asp:UpdatePanel>
            <script type="text/javascript">

                $(document).ready(function () {
                    doMyAction();
                });



                function doMyAction() {

                    $('#<%=ibtnAddPAidDate.ClientID %>').click(function () {
                formValidate();
            });

            $('.tooltipwidth').tooltip();
            $('.tooltips').tooltip();


        }


            </script>
            <script type="text/javascript">
                function divexpandcollapse(divname, trname) {
                    var div = document.getElementById(divname);
                    var img = document.getElementById('img' + divname);
                    var tr = document.getElementById(trname);

                    if (div.style.display == "none") {
                        div.style.display = "inline";
                        tr.style.display = "";
                        img.src = "../../../images/icon_minus.png";
                    } else {
                        div.style.display = "none";
                        tr.style.display = "none";
                        img.src = "../../../images/icon_plus.png";
                    }
                }
            </script>

            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Inst Invoice Tracker</h5>
                <div id="tdExport" class="pull-right" runat="server">
                    <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">
                        <%--<asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export"
                                    CausesValidation="false" OnClick="lbtnExport_Click"><img src="../../../images/icon_excel.gif" /> </asp:LinkButton>--%>
                    </ol>
                </div>
            </div>
            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);
                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');
                }
                function endrequesthandler(sender, args) {
                   
                }
                function pageLoaded() {

                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    //alert($(".search-select").attr("class"));
                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $("[data-toggle=tooltip]").tooltip();

                    $(".myvalinstinvoice").select2({
                        //placeholder: "select",
                        allowclear: true
                    });
                    $(".myval").select2({
                        minimumResultsForSearch: -1
                    });
                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }
                }
            </script>
            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="content animate-panel " style="padding-bottom: 0px!important;">
                        <div class="messesgarea">
                            <div class="alert alert-info" id="PanNoRecord" runat="server">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>
                    </div>

                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter">
                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch">
                                        <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <div class="inlineblock ">

                                                        <div class="input-group col-sm-1 martop5" id="divCustomer" runat="server" style="width: 177px;">
                                                            <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalinstinvoice" Width="150px">
                                                                <asp:ListItem Value="">Installer</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group col-sm-1 martop5">
                                                            <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtProjectNumber"
                                                                WatermarkText="Project Num" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                ControlToValidate="txtProjectNumber" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                        </div>

                                                        <div class="input-group col-sm-1 martop5">
                                                            <asp:TextBox ID="txtMQNsearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender8" runat="server" TargetControlID="txtMQNsearch"
                                                                WatermarkText="Manual Num" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtMQNsearch" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetManualQuoteNumber"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                        </div>

                                                        <div class="input-group col-sm-1 martop5">
                                                            <asp:TextBox ID="txtContactSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender16" runat="server" TargetControlID="txtContactSearch"
                                                                WatermarkText="Contact" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender9" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtContactSearch" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetContactList"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                            </cc1:AutoCompleteExtender>
                                                        </div>

                                                        <div class="input-group col-sm-1 martop5">
                                                            <asp:TextBox ID="txtSerachCity" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender10" runat="server" TargetControlID="txtSerachCity"
                                                                WatermarkText="Suburb" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtSerachCity" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCities"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                            </cc1:AutoCompleteExtender>
                                                        </div>


                                                        <div class="input-group col-sm-1 martop5" id="div1" runat="server">
                                                            <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalinstinvoice">
                                                                <asp:ListItem Value="">State</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group col-sm-1 martop5" style="width: 148px">
                                                            <asp:TextBox ID="txtSearchPostCode" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender13" runat="server" TargetControlID="txtSearchPostCode"
                                                                WatermarkText="PCode" />
                                                            <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                                UseContextKey="true" TargetControlID="txtSearchPostCode" ServicePath="~/Search.asmx"
                                                                CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetPostCodeList"
                                                                EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                            </cc1:AutoCompleteExtender>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="search"
                                                                ControlToValidate="txtSearchPostCode" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                        </div>
                                                        </br>
                                                        <div class="input-group col-sm-1 martop5" style="width: 101px" id="div2" runat="server">
                                                            <asp:DropDownList ID="ddlmeterapplyref" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalinstinvoice">
                                                                <asp:ListItem Value="">Meter Apply Ref</asp:ListItem>
                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group col-sm-1 martop5" style="width: 101px" id="div3" runat="server">
                                                            <asp:DropDownList ID="ddlSearchDate" runat="server" AppendDataBoundItems="true"
                                                                aria-controls="DataTables_Table_0" CssClass="myvalinstinvoice">
                                                                <asp:ListItem Value="">Select Date</asp:ListItem>
                                                                <asp:ListItem Value="1">Invoice Date</asp:ListItem>
                                                                <asp:ListItem Value="3">Install Date</asp:ListItem>
                                                                <asp:ListItem Value="4">Paid Date</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                        <div class="input-group date datetimepicker1 col-sm-2 martop5">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                    ControlToValidate="txtStartDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                        </div>
                                                        <div class="input-group date datetimepicker1 col-sm-2 martop5">
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar"></span>
                                                            </span>
                                                            <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                            <%--   <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic"
                                                                    ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                            <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                        </div>

                                                        <div class="form-group martop5 ">
                                                            <table>
                                                                <tr>
                                                                    <td class="checkbox-info paddtop3td alignchkbox btnviewallorange">
                                                                        <label for="<%=chkHistoric.ClientID %>" class="btn btn-magenta">
                                                                            <asp:CheckBox ID="chkHistoric" runat="server" />
                                                                            <span class="text">Historic</span>
                                                                        </label>
                                                                    </td>
                                                                    <td></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="input-group martop5">
                                                            <asp:Button ID="btnSearch" runat="server" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                        </div>
                                                        <div class="form-group martop5">
                                                            <asp:LinkButton ID="btnClearAll" runat="server"
                                                                CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa fa-refresh"></i>Clear </asp:LinkButton>
                                                        </div>

                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="datashowbox martop5">
                                <div class="row">
                                    <div class="dataTables_length showdata col-sm-6">
                                        <table border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="padtopzero">
                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                        aria-controls="DataTables_Table_0" class="myval">
                                                        <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <div id="Div4" class="pull-right" runat="server" style="padding-top:8px">
                                            <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>
                                            <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                CausesValidation="false" OnClick="lbtnExport_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                            <%-- </ol>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </asp:Panel>
                <asp:Panel ID="panel1" runat="server" CssClass="hpanel panel-body padtopzero finalgrid">
                    <div class="row">
                        <div id="PanGrid" runat="server">
                            <div>
                                <div style="width: 100%; overflow-x: auto;">
                                    <asp:GridView ID="GridView1" DataKeyNames="CustomerID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                        OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging"
                                        OnDataBound="GridView1_DataBound" OnRowCommand="GridView1_RowCommand" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                        <Columns>
                                            <asp:TemplateField ItemStyle-Width="20px">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hdnempid" runat="server" Value='<%# Eval("CustomerID") %>' />
                                                    <a href="JavaScript:divexpandcollapse('div<%# Eval("CustomerID") %>','tr<%# Eval("CustomerID") %>');">
                                                        <img id='imgdiv<%# Eval("CustomerID") %>' src="../../../images/icon_plus.png" />

                                                        <%--<i id='imgdiv<%# Eval("CustomerID") %>' class="fa fa-plus"></i>--%>
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Project#" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="center-text"
                                                ItemStyle-HorizontalAlign="Center" SortExpression="ProjectNumber">
                                                <ItemTemplate>
                                                    <asp:HiddenField runat="server" ID="hndformbay" Value='<%#Eval("FormbayID")%>' />
                                                    <asp:HiddenField runat="server" ID="hdnProjectID" Value='<%#Eval("ProjectID")%>' />

                                                    <asp:Label ID="lblProjectNumber" runat="server" Width="70px">
                                                        <asp:HiddenField ID="HiddenField1" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=proelec&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'>
                                                                <%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>
                                                    <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("EmployeeID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Left" SortExpression="customer">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCustomer" runat="server" Width="100px">
                                                <%#Eval("customer")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="Manual#" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="ManualQuoteNumber">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblManualQuoteNumber" runat="server" Width="80px"><%#Eval("ManualQuoteNumber")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            <%--<asp:TemplateField HeaderText="Quote" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    HeaderStyle-CssClass="gridheadertext" ItemStyle-HorizontalAlign="Center"
                                                    SortExpression="ProjectOpened">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblQuote" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "ProjectOpened", "{0:dd MMM yyyy}")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Address" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="Address"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAddress" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("Address")%>'
                                                        Width="170px"><%#Eval("Address")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PCode" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="left" SortExpression="InstallPostCode"
                                                ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="center-text">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPCode" runat="server" Width="50px"><%#Eval("InstallPostCode")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="SystemDetails" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSystemDetails" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("SystemDetails")%>'
                                                            Width="250px"><%#Eval("SystemDetails")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Install Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="left" SortExpression="InstallBookingDate">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInstallBookingDate" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "InstallBookingDate", "{0:dd MMM yyyy}")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--  <asp:TemplateField HeaderText="House Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="HouseType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblHouseType" runat="server" Width="100px"><%#Eval("HouseType")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            <%--  <asp:TemplateField HeaderText="Roof Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="RoofType">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRoofType" runat="server" Width="100px"><%#Eval("RoofType")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            <%-- <asp:TemplateField HeaderText="Angle" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="RoofAngle">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRoofAngle" runat="server" Width="70px"><%#Eval("RoofAngle")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            <%-- <asp:TemplateField HeaderText="Split" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSplitSystemYes" Width="40px" runat="server" Text="Yes" Visible='<%#Eval("SplitSystem").ToString()=="True"?true:false %>'></asp:Label>
                                                        <asp:Label ID="lblSplitSystemNo" Width="40px" runat="server" Text="No" Visible='<%#Eval("SplitSystem").ToString()=="False"?true:false %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Trav" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left" SortExpression="TravelTime">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTrav" runat="server" Width="50px"><%#Eval("TravelTime")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Install Complete" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="left" SortExpression="InstallCompleted">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblConfirmed" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "InstallCompleted", "{0:dd/MMM/yyyy}")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--  <asp:TemplateField HeaderText="Inv #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInvoiceNumber" runat="server" Width="100px"><%#Eval("InstallerInvNo")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Amount" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" SortExpression="invamount"
                                                ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAmount" runat="server" Width="90px"><%#Eval("invamount","{0:0.00}")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Issued" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="left" SortExpression="InstallerInvDate">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInstallerInvDate" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "invdate", "{0:dd/MMM/yyyy}")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Payment Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="left" SortExpression="InstallerInvDate">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPaymentstatus" runat="server" Width="110px"> <%# Eval("paymentstatus") %></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="Document Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center" SortExpression="InstallerInvDate" HeaderStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDocumentstatus" runat="server" Width="120px" Text='<%# Eval("STCFormSaved").ToString()=="False"?"Pending":"Uploded" %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Paid Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="left" SortExpression="InstallerPayDate">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblInstallerPayDate" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "paydate", "{0:dd/MMM/yyyy}")%></asp:Label>
                                                    <asp:LinkButton ID="lbtnPaid" CommandName="Paid" CssClass="btn btn-sky btn-xs"
                                                        CommandArgument='<%#Eval("InvoiceID") %>' CausesValidation="false" runat="server" Visible='<%#Convert.ToString( Eval("paydate"))==""?true:false%>' data-toggle="tooltip" data-placement="top" data-original-title="Paid Date">
                                                          <i class="fa fa-retweet"></i>  </asp:LinkButton>

                                                    <asp:LinkButton ID="lbtnRevert" CommandName="PaidRevert" CssClass="btn btn-maroon btn-xs"
                                                        CommandArgument='<%#Eval("InvoiceID") %>' Visible='<%#Convert.ToString( Eval("paydate"))!=""?true:false%>' CausesValidation="false" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Revert Paid Date">
                                                          <i class="fa fa-retweet"></i>  </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField HeaderText="Meter Time" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center" SortExpression="MeterAppliedTime">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMeterAppliedTime" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "MeterAppliedTime", "{0:dd/MMM/yyyy}")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            <%-- <asp:TemplateField HeaderText="Meter Apply Ref" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center" SortExpression="MeterAppliedRef" HeaderStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMeterAppliedRef" runat="server" Width="120px" Text='<%# Eval("MeterAppliedRef") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            <%--<asp:TemplateField HeaderText="Inspector Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center" SortExpression="InspectorName" HeaderStyle-Width="100px">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInspectorName" runat="server" Width="120px" Text='<%# Eval("InspectorName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            <%-- <asp:TemplateField HeaderText="Inspection Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center" SortExpression="InspectionDate">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblInspectionDate" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "InspectionDate", "{0:dd/MMM/yyyy}")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            <%-- <asp:TemplateField HeaderText="Foll-Up" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                    ItemStyle-HorizontalAlign="Center" SortExpression="FollowUp">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFollUp" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "FollowUp", "{0:dd/MMM/yyyy}")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Meter Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                    ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMeterNotes" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("MeterInstallerNotes")%>'
                                                            Width="250px"><%#Eval("MeterInstallerNotes")%></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            <%--   <asp:TemplateField HeaderText="rec" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25px">
                                            <ItemTemplate>

                                                <asp:LinkButton runat="server" ID="lnkrec" CommandName="lnkrec" CommandArgument='<%#Eval("FormbayID")%>' CausesValidation="false" Visible="false">
                                            View
                                                </asp:LinkButton>

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ccp" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25px">
                                            <ItemTemplate>

                                                <asp:LinkButton runat="server" ID="lnkccp" CommandName="lnkccp" CommandArgument='<%#Eval("FormbayID")%>' CausesValidation="false" Visible="false">
                                            View
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>



                                            <asp:TemplateField HeaderStyle-CssClass="disnone" ItemStyle-CssClass="disnone">
                                                <ItemTemplate>
                                                    <tr id='tr<%# Eval("CustomerID") %>' style="display: none;" class="dataTable left-text">
                                                        <td colspan="98%" class="details">
                                                            <div id='div<%# Eval("CustomerID") %>' style="display: none; position: relative; left: 0px; overflow: auto">
                                                                <table id="tblGrid" runat="server" width="100%" class="table table-bordered table-hover">
                                                                    <tr>
                                                                        <td width="150px"><b>Manual#</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblManualQuoteNumber" runat="server" Width="80px"><%#Eval("ManualQuoteNumber")%></asp:Label>
                                                                        </td>
                                                                        <td width="150px"><b>SystemDetails</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblSystemDetails" runat="server" data-toggle="tooltip" data-placement="top" data-original-title='<%#Eval("SystemDetails")%>'
                                                                                Width="80px"><%#Eval("SystemDetails")%></asp:Label>
                                                                        </td>

                                                                        <td width="150px"><b>House Type</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblHouseType" runat="server" Width="80px"><%#Eval("HouseType")%></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><b>Roof Type</b>
                                                                        </td>
                                                                        <td runat="server" id="tdtype">
                                                                            <asp:Label ID="lblRoofType" runat="server" Width="100px"><%#Eval("RoofType")%></asp:Label>
                                                                        </td>
                                                                        <td><b>Angle</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblRoofAngle" runat="server" Width="70px"><%#Eval("RoofAngle")%></asp:Label>
                                                                        </td>
                                                                        <td><b>Inv #</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblInvoiceNumber" runat="server" Width="100px"><%#Eval("InstallerInvNo")%></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><b>Document Status</b>
                                                                        </td>

                                                                        <td>
                                                                            <asp:Label ID="lblDocumentstatus" runat="server" Width="120px" Text='<%# Eval("STCFormSaved").ToString()=="False"?"Pending":"Uploded" %>'></asp:Label>
                                                                        </td>
                                                                        <td><b>Meter Time</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblMeterAppliedTime" runat="server" Width="100px"><%# Eval("MeterAppliedTime")%></asp:Label>
                                                                        </td>

                                                                        <td><b>Meter Apply Ref</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblMeterAppliedRef" runat="server" Width="120px" Text='<%# Eval("MeterAppliedRef") %>'></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><b>Inspector Name</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblInspectorName" runat="server" Width="120px" Text='<%# Eval("InspectorName") %>'></asp:Label>
                                                                        </td>

                                                                        <td><b>Inspection Date</b>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Label ID="lblInspectionDate" runat="server" Width="100px"><%# DataBinder.Eval(Container.DataItem, "InspectionDate", "{0:dd/MMM/yyyy}")%></asp:Label>
                                                                        </td>

                                                                    </tr>
                                                                </table>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerTemplate>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            <div class="pagination">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                            </div>
                                        </PagerTemplate>
                                        <PagerStyle CssClass="paginationGrid" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />

                                    </asp:GridView>

                                    <%--    <div class="paginationnew1" runat="server" id="divnopage" visible="false">
                                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                    <tr>
                                                        <td>
                                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                        </td>
                                                        <td style="float: right;">
                                                            <asp:LinkButton ID="lnkfirst" runat="server" CausesValidation="false" CssClass="pagebtndesign firstpage nextbtn" OnClick="lnkfirst_Click">First</asp:LinkButton>
                                                            <asp:LinkButton ID="lnkprevious" runat="server" CausesValidation="false" CssClass="pagebtndesign nextbtn" OnClick="lnkprevious_Click">Previous</asp:LinkButton>
                                                            <asp:Repeater runat="server" ID="rptpage" OnItemCommand="rptpage_ItemCommand">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton runat="server" ID="lnkpagebtn" CssClass="pagebtndesign" CausesValidation="false" CommandArgument='<%#Eval("ID") %>' CommandName="Pagebtn">
                                                                        <%#Eval("ID") %>

                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                            <asp:LinkButton ID="lnknext" runat="server" CausesValidation="false" CssClass="pagebtndesign nextbtn" OnClick="lnknext_Click">Next</asp:LinkButton>
                                                            <asp:LinkButton ID="lnklast" runat="server" CausesValidation="false" CssClass="pagebtndesign nextbtn" OnClick="lnklast_Click">Last</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>--%>
                                </div>
                                <div class="paginationnew1" runat="server" id="divnopage">
                                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>


                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <asp:Button ID="btnNULLData1" Style="display: none;" runat="server" />
                <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
                    CancelControlID="ibtnCancelStatus" DropShadow="true" PopupControlID="div_popup" TargetControlID="btnNULLData1">
                </cc1:ModalPopupExtender>
                <div runat="server" id="div_popup" class="modalpopup" align="center" style="width: 900px; overflow-x: scroll;">
                    <div class="popupdiv">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading pad15">
                                        <asp:Label runat="server" ID="lbltitle"></asp:Label>
                                        <div class="panel-tools">
                                            <div class="closbtn">
                                                <asp:LinkButton ID="ibtnCancelStatus" CausesValidation="false" runat="server">
                                                    <asp:Image ID="imgClose" runat="server" ImageUrl="~/admin/images/icon_close.png" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel-body formareapop" style="background: none!important; min-height: 50px!important; max-height: 550px!important; overflow: auto!important;">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group dateimgarea">
                                                    <asp:Repeater runat="server" ID="rpt">
                                                        <ItemTemplate>
                                                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("URL") %>' />
                                                        </ItemTemplate>
                                                        <SeparatorTemplate>
                                                            <br />
                                                            <br />
                                                        </SeparatorTemplate>
                                                    </asp:Repeater>
                                                    <div class="clear">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bodymianbg">
                    <div class="row">
                        <div class="col-md-12">
                        </div>
                    </div>
                </div>



                <asp:Button ID="btnNULLPaid" Style="display: none;" runat="server" />

                <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
                    PopupControlID="divPaiddate" TargetControlID="btnNULLPaid" CancelControlID="ibtnCancel">
                </cc1:ModalPopupExtender>

                <div id="divPaiddate" runat="server" style="display: none;" class="modal_popup">
                    <div class="modal-dialog" style="width: 400px;">
                        <div class="modal-content">
                            <div class="color-line"></div>
                            <div class="modal-header">
                                <div style="float: right">
                                    <asp:LinkButton ID="ibtnCancel" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal"
                                        OnClick="ibtnCancel_Click">Close
                                    </asp:LinkButton>
                                </div>
                                <h4 class="modal-title" id="myModalLabel1">Change Paid Date</h4>
                            </div>
                            <div class="modal-body paddnone">
                                <div class="panel-body">
                                    <div class="formainline">

                                        <div class="form-group marginbtm10 row">
                                            <asp:Label ID="Label1" runat="server" class="col-sm-4 control-label" Style="padding: -right:0px;">
                                                <strong>Paid Date</strong></asp:Label>

                                            <%-- <div class="input-group date datetimepicker1 col-sm-3">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                    <asp:TextBox ID="txtActualPayDate" runat="server" class="form-control" placeholder="Actual Pay Date">
                                    </asp:TextBox>
                                  
                                </div>--%>
                                            <div class="col-sm-8">

                                                <div class="input-group date datetimepicker1 col-sm-12">
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar"></span>
                                                    </span>
                                                    <asp:TextBox ID="txtpaiddate" placeholder="Paid Date" runat="server" class="form-control"></asp:TextBox>
                                                    <%--  <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtActualPayDate"
                                            WatermarkText="Actual Date" />--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group marginbtm10 row">
                                            <asp:Label ID="Label11" runat="server" class="col-sm-4 control-label" Style="padding: -right:0px;">
                                                <strong>Comment</strong></asp:Label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txtpaidcomment" TextMode="MultiLine" Rows="3" Columns="5" runat="server" MaxLength="200" class="form-control modaltextbox"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpaidcomment"
                                                    ErrorMessage="This value is required." CssClass="reqerror" ValidationGroup="AddNotesPaid" Display="Dynamic"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group col-md-12 center-text">
                                            <asp:Button ID="ibtnAddPAidDate" runat="server" Text="Update" OnClick="ibtnAddPAidDate_Click"
                                                ValidationGroup="AddNotesPaid" CssClass="btn btn-primary savewhiteicon btnsaveicon" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <asp:Button ID="btndelete" Style="display: none;" runat="server" />
                <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                    PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
                </cc1:ModalPopupExtender>
                <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

                    <div class="modal-dialog " style="margin-top: -300px">
                        <div class=" modal-content ">
                            <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                            <div class="modal-header text-center">
                                <i class="glyphicon glyphicon-fire"></i>
                            </div>


                            <div class="modal-title">Revert</div>
                            <label id="ghh" runat="server"></label>
                            <div class="modal-body ">Are You Sure Revert the Paid Date?</div>
                            <div class="modal-footer " style="text-align: center">
                                <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                                <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                            </div>
                        </div>
                    </div>

                </div>

                <asp:HiddenField ID="hdndelete" runat="server" />

                <asp:HiddenField ID="hndinvoicepaymentid" runat="server" />

                <asp:HiddenField runat="server" ID="hdncountdata" />
            </div>


     


</asp:Content>

