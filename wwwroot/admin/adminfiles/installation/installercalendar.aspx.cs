﻿using System;
using System.Data;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Globalization;
using System.Text;

public partial class admin_adminfiles_installation_installercalendar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int offset = DateTime.Now.AddHours(14).DayOfWeek - DayOfWeek.Monday;
            DateTime lastMonday = DateTime.Now.AddHours(14).AddDays(-offset);
            BindWeekDates(lastMonday);
        }
    }
    public void BindWeekDates(DateTime startdate)
    {
        ArrayList values = new ArrayList();
        for (int i = 0; i < 7; i++)
        {
            values.Add(new PositionData(startdate.AddDays(i).ToShortDateString(), CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(startdate.AddDays(i).DayOfWeek)));
        }

        RptDays.DataSource = values;
        RptDays.DataBind();

        RepeaterItem item0 = RptDays.Items[0];
        string hdnDate0 = ((HiddenField)item0.FindControl("hdnDate")).Value;
        RepeaterItem item6 = RptDays.Items[6];
        string hdnDate6 = ((HiddenField)item6.FindControl("hdnDate")).Value;

        ltdate.Text = string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(hdnDate0)) + " - " + string.Format("{0:MMM dd yyyy}", Convert.ToDateTime(hdnDate6));
    }
    public class PositionData
    {
        private string CDate;
        private string CDay;
        public PositionData(string CDate, string CDay)
        {
            this.CDate = CDate;
            this.CDay = CDay;
        }
        public string CDATE
        {
            get
            {
                return CDate;
            }
        }
        public string CDAY
        {
            get
            {
                return CDay;
            }
        }
    }

    protected void btntoday_Click(object sender, EventArgs e)
    {
        BindWeekDates(DateTime.Now.AddHours(14));
    }

    protected void btnpreviousweek_Click(object sender, EventArgs e)
    {
        if (RptDays.Items.Count > 6)
        {
            RepeaterItem item = RptDays.Items[0];
            string startdate = ((HiddenField)item.FindControl("hdnDate")).Value;
            BindWeekDates(Convert.ToDateTime(startdate).AddDays(-7));
        }
    }
    protected void btnnextweek_Click(object sender, EventArgs e)
    {
        if (RptDays.Items.Count > 6)
        {
            RepeaterItem item = RptDays.Items[6];
            string enddate = ((HiddenField)item.FindControl("hdnDate")).Value;
            BindWeekDates(Convert.ToDateTime(enddate).AddDays(1));
        }
    }

    protected void RptDays_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        string userid = "";
        if (Roles.IsUserInRole("Installer"))
        {
            userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        }
        else
        {
            userid = "";
        }

        HiddenField hdnDate = (HiddenField)e.Item.FindControl("hdnDate");
        DataTable dt = ClstInstallations.tblProjects_AllInstaller_ByBookingDate(hdnDate.Value, userid);
        Repeater rptinstaller1 = (Repeater)e.Item.FindControl("rptinstaller1");
        rptinstaller1.DataSource = dt;
        rptinstaller1.DataBind();
    }

    protected void rptinstaller1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "assign")
        {
            ListItem item5 = new ListItem();
            item5.Text = "Select";
            item5.Value = "";
            ddlProject.Items.Clear();
            ddlProject.Items.Add(item5);

            ModalPopupProject.Show();
            string Installer = e.CommandArgument.ToString();

            DataTable dt = ClstInstallations.tblProjects_SelectByInstaller();
            if (dt.Rows.Count > 0)
            {
                ddlProject.DataSource = ClstInstallations.tblProjects_SelectByInstaller();
                ddlProject.DataMember = "Project";
                ddlProject.DataTextField = "Project";
                ddlProject.DataValueField = "ProjectID";
                ddlProject.DataBind();
            }
        }
    }
    protected void ibtnAssign_Click(object sender, EventArgs e)
    {
        if (ddlProject.SelectedValue != string.Empty)
        {
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ddlProject.SelectedValue);
            /* -------------------- Job Booked -------------------- */
            if (txtInstallBookingDate.Text.Trim() != string.Empty)
            {
                string EmployeeID = "";
                string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

                if (Roles.IsUserInRole("Installer") || Roles.IsUserInRole("Customer"))
                {

                }
                else
                {
                    if (userid != string.Empty)
                    {
                        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                        EmployeeID = stEmp.EmployeeID;
                    }
                }
                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("11", ddlProject.SelectedValue, EmployeeID, stPro.NumberPanels);
                ClsProjectSale.tblProjects_UpdateProjectStatusID(ddlProject.SelectedValue, "11");
            }
            /* ---------------------------------------------------- */
        }
    }
}