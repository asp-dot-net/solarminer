﻿using System;
using System.Data;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Text.RegularExpressions;
using System.Data.Common;

public partial class admin_adminfiles_upload_pvdstatus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            HidePanel();
        }
    }
    public void HidePanel()
    {
        PanSuccess.Visible = false;
        PanError.Visible = false;
    }
     
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("pvdstatus.aspx");
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        int success= 0;
        bool success2 = false;
        //Response.Expires = 400;
        Response.Expires = 10000000;
        //Server.ScriptTimeout = 1200;
        Server.ScriptTimeout = 12000000;
        string insstmt = "";
        bool stmt = false;
        if (FileUpload1.HasFile)
        {
            FileUpload1.SaveAs(Request.PhysicalApplicationPath+"\\userfiles" + "\\PVDStatus\\" + FileUpload1.FileName);
            string connectionString = "";
            //if (FileUpload1.FileName.EndsWith(".csv"))
            //{
            //    connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\CouponCSV\\" + ";Extended Properties=\"Text;HDR=YES;\"";
            //}

            //connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("\\userfiles") + "\\subscriber\\" + FileUpload1.FileName + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";

            //(Comment on 10-01-2019) connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Request.PhysicalApplicationPath + "\\userfiles" + "\\PVDStatus\\" + FileUpload1.FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;'";
            if (FileUpload1.FileName.EndsWith(".xls"))
            {
                connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\PVDStatus\\" + FileUpload1.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
            }
            else if (FileUpload1.FileName.EndsWith(".xlsx"))
            {
                connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\PVDStatus\\" + FileUpload1.FileName) +
                    ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";

            }
            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                using (DbCommand command = connection.CreateCommand())
                {
                    // Cities$ comes from the name of the worksheet

                    command.CommandText = "SELECT * FROM [Sheet1$]";
                    command.CommandType = CommandType.Text;
                    // command.CommandTimeout = 120;
                    command.CommandTimeout = 12000000;

                    //Response.End();
                    string employeeid = string.Empty;
                    using (DbDataReader dr = command.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                int count = 0;
                                
                                string PVDNumber = "";
                                string ProjectNumber = "";
                                string ManualQuoteNumber = "";
                                string ProjManual = "";
                                string PVDStatus = "";
                                string STCApplied = "";

                                PVDNumber = dr["PVDNumber"].ToString();
                                ProjectNumber = dr["ProjectNumber"].ToString();
                                ManualQuoteNumber = dr["ManualQuoteNumber"].ToString();
                                ProjManual = dr["ProjManual"].ToString();
                                PVDStatus = dr["PVDStatus"].ToString();
                                STCApplied = dr["STCApplied"].ToString();
                                if (PVDNumber==""&& ProjectNumber == "" && PVDNumber == "" && PVDNumber == "" && PVDNumber == "" && PVDNumber == "" )
                                {
                                    count=1;
                                }
                                //Response.Write(PVDNumber + "<br />" + ProjectNumber + "<br />" + ManualQuoteNumber + "<br />" + ProjManual + "<br />" + PVDStatus + "<br />" + STCApplied);
                                //Response.End();
                                if (count != 1)
                                {
                                    try
                                    {
                                        // insstmt += "INSERT INTO tblPVDStatusImport ( PVDNumber,  ProjectNumber, ManualQuoteNumber, ProjManual, PVDStatusID, STCApplied) values ( '" + PVDNumber + "',  '" + ProjectNumber + "', '" + ManualQuoteNumber + "', '" + ProjManual + "', '" + PVDStatus + "', '" + string.Format("{0:yyyy-MM-dd hh:mm:ss:mmm}", Convert.ToDateTime(STCApplied)) + "')";
                                        success = ClstblPVDStatusImport.tblPVDStatusImport_Insert2(PVDNumber, ProjectNumber, ManualQuoteNumber, ProjManual, PVDStatus, STCApplied);
                                        //Response.Write(success);
                                        //Response.End();
                                        //SttblPVDStatusImport st = ClstblPVDStatusImport.tblPVDStatusImport_SelectByPVDStatusImportID(Convert.ToString(success));
                                        success2 = ClstblProjects.tblProjects_Update_STC(ProjectNumber, PVDNumber, PVDStatus, ManualQuoteNumber, STCApplied);
                                        //insstmt += "Update tblProjects set  PVDNumber='" + PVDNumber + "',PVDStatusID='" + PVDStatus + "',ManualQuoteNumber='" + ManualQuoteNumber + "',STCApplied='" + string.Format("{0:yyyy-MM-dd hh:mm:ss:mmm}", Convert.ToDateTime(STCApplied)) + "' WHERE ProjectNumber= '" + ProjectNumber + "'";

                                    }
                                    catch { }
                                    if (success > 0 && success2 == true)
                                    //if (stmt.ToString() == true.ToString())
                                    {
                                        SetAdd1();
                                        //PanSuccess.Visible = true;
                                        PanError.Visible = false;
                                    }
                                    else
                                    {

                                        SetError1();
                                        //PanError.Visible = true;
                                        PanSuccess.Visible = false;
                                    }
                                }
                              
                            }
                            //Response.Write(insstmt);
                            //Response.End();
                           
                        }
                    }
                }
            }
            
        }
        // stmt = ClstblPVDStatusImport.tblPVDStatusImport_Statement(insstmt);
        //if (success > 0)
        ////if (stmt.ToString() == true.ToString())
        //{
        //    SetAdd1();
        //    //PanSuccess.Visible = true;
        //    PanError.Visible = false;
        //}
        //else
        //{

        //    SetError1();
        //    //PanError.Visible = true;
        //    PanSuccess.Visible = false;
        //}

        //excel is deleted once data is fetched and process is completed.
        SiteConfiguration.deleteimage(FileUpload1.FileName, "PVDStatus");
       
    }


    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
}