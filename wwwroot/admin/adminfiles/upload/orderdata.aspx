<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    Theme="admin" CodeFile="orderdata.aspx.cs" Inherits="admin_adminfiles_upload_orderdata" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            HighlightControlToValidate();
            $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();
            });
        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
    
            <section class="">
                <div class="small-header transition animated fadeIn">
                    <div class="finalheader">
                        <div class="hpanel">
                            <div class="panel-body">
                                <h2 class="font-light m-b-xs">Upload Order No and Serial No</h2>
                                <div class="contactsarea small-header transition animated fadeIn">
                                    <div class="addcontent pull-right">
                                        <asp:HyperLink ID="hypNavi" runat="server" CssClass="btn btn-info" NavigateUrl="~/admin/adminfiles/stock/stockorder.aspx">
                       <i class="fa fa-chevron-left"></i>Back
                                        </asp:HyperLink>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="messesgarea">
                    <div class="alert alert-success" id="PanSuccess" runat="server">
                        <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful.</strong>
                    </div>
                    <div class="alert alert-danger" id="PanError" runat="server">
                        <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed. </strong>
                    </div>
                </div>
                <div class="">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span class="name">
                                                <label class="control-label">
                                                    Upload Excel File 
                                                </label>
                                            </span><span>
                                                <asp:FileUpload ID="FileUpload1" runat="server" />

                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="FileUpload1"
                                                    ValidationGroup="success" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                                    Display="Dynamic" ErrorMessage=".xls only"></asp:RegularExpressionValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                    ControlToValidate="FileUpload1" Display="Dynamic" ValidationGroup="success"></asp:RequiredFieldValidator>&nbsp;
                                    &nbsp; </span>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <span class="name">
                                            <label class="control-label">
                                            </label>
                                        </span><span>
                                            <asp:HyperLink ID="hypsubscriber" runat="server" NavigateUrl="~/userfiles/OrderData/format/OrderData.xls"
                                                Target="_blank">Click here</asp:HyperLink>
                                            to download Excel Sheet format. </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span class="name">&nbsp; </span><span>
                                                <asp:Button CssClass="btn btn-primary addwhiteicon btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                                    Text="Add" ValidationGroup="success" />
                                                <asp:Button CssClass="btn btn-dark-grey calcelwhiteicon btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                    CausesValidation="false" Text="Cancel" />
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
