using System;
using System.Web.UI;
using System.Data;
using System.Linq;

public partial class admin_adminfiles_upload_LeadReport : System.Web.UI.Page
{
    static DataView dv;
    protected static string Siteurl;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            BindDropDown();
        }
    }

    protected void BindDropDown()
    {
        ddlTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
        ddlTeam.DataMember = "SalesTeam";
        ddlTeam.DataTextField = "SalesTeam";
        ddlTeam.DataValueField = "SalesTeamID";
        ddlTeam.DataBind();

        ddlEmployee.DataSource = ClstblEmployees.tblEmployees_SelectASC_Include();
        ddlEmployee.DataMember = "EmployeeID";
        ddlEmployee.DataTextField = "fullname";
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataBind();
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void MsgError(string msg)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", string.Format("MyRedfun('{0}');", msg), true);
    }

    protected DataTable GetGridData()
    {
        DataTable dt = ClsLead.SP_GetLead_ByEmployee(txtStartDate.Text, txtEndDate.Text, ddlTeam.SelectedValue, ddlEmployee.SelectedValue);

        return dt;
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {
        DataTable dt = GetGridData();
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string[] columnNames = dt.Columns.Cast<DataColumn>()
                                .Select(x => x.ColumnName)
                                .ToArray();

            string FileName = "LeadReport_" + "" + ".xls";

            int[] ColList = { 1, 17, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };

            string[] arrHeader = { "Employee Name", "Actual Lead", "Current Lead", "Sold", "Ratio", "TV", "TV Sold", "TV Ratio",  "Online", "Online Sold", "Online Ratio", "Facebook", "Facebook Sold", "Facebook Ratio", "Others", "Others Sold", "Others Ratio" };

            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
            MsgError("Error while downloading Excel..");
        }
    }
}