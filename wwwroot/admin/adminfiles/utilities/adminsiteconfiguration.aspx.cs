using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class admin_adminfiles_utilities_adminsiteconfiguration : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if (!IsPostBack)
		{
			InitUpdate();
			StSiteConfiguration st = ClsAdminSiteConfiguration.AdminSiteConfigurationGetDataStructById("1");
			txtlevel.Text = st.level;
			txtCurrencyDigit.Text = st.Currencydigit;
			txtsymbol.Text = st.Symbol;
			txtCatLarge.Text = st.CategoryLarge;
			txtCatMedium.Text = st.CategoryMedium;
			txtCatSmall.Text = st.CategorySmall;
			txtProductLarge.Text = st.ProductLarge;
			txtProductMedium.Text = st.ProductMedium;
			txtProductSmall.Text = st.ProductSmall;
			txtBannerLarge.Text = st.BannerLarge;
			txtBannerMedium.Text = st.BannerMedium;
			txtBannerSmall.Text = st.BannerSmall;

		}
	}
	protected void btnUpdate_Click(object sender, EventArgs e)
	{
		int level = 0, currencydigit=0, catlarge = 0, catmedium = 0, catsmall = 0, prodlarge = 0, prodmedium = 0, prodsmall = 0, bannerlarge = 0,
			bannermedium = 0, bannersmall=0;

		if (txtlevel.Text != string.Empty)
		{
			level = Convert.ToInt32(txtlevel.Text);
		}
		if (txtCurrencyDigit.Text != string.Empty)
		{
			currencydigit = Convert.ToInt32(txtCurrencyDigit.Text);
		}
		string symbol = txtsymbol.Text;
		if (txtCatLarge.Text != string.Empty)
		{
			catlarge = Convert.ToInt32(txtCatLarge.Text);
		}
		if (txtCatMedium.Text != string.Empty)
		{
			catmedium = Convert.ToInt32(txtCatMedium.Text);
		}
		if (txtCatSmall.Text != string.Empty)
		{
			catsmall = Convert.ToInt32(txtCatSmall.Text);
		}
		if (txtProductLarge.Text != string.Empty)
		{
			prodlarge = Convert.ToInt32(txtProductLarge.Text); 
		}
		if (txtProductMedium.Text != string.Empty)
		{ 
			prodmedium = Convert.ToInt32(txtProductMedium.Text);
		}
		if (txtProductSmall.Text != string.Empty)
		{
			prodsmall = Convert.ToInt32(txtProductSmall.Text);
		}
		if (txtBannerLarge.Text != string.Empty)
		{
			bannerlarge = Convert.ToInt32(txtBannerLarge.Text);
		}
		if (txtBannerMedium.Text != string.Empty)
		{
			bannermedium = Convert.ToInt32(txtBannerMedium.Text);
		}
		if (txtBannerSmall.Text != string.Empty)
		{
			bannersmall = Convert.ToInt32(txtBannerSmall.Text);
		}

		bool success1 = ClsAdminSiteConfiguration.AdminSiteConfigurationUpdateData(1, level, currencydigit, symbol, catlarge,
			catmedium, catsmall, prodlarge, prodmedium, prodsmall, bannerlarge, bannermedium, bannersmall);
		if (success1)
		{
			SetUpdate();
		}
		else
		{
			SetError();
		}

	}
	//================================== Start Common Code ==================================


	private string ConvertSortDirectionToSql(SortDirection sortDireciton)
	{
		string m_SortDirection = String.Empty;

		switch (sortDireciton)
		{
			case SortDirection.Ascending:
				m_SortDirection = "ASC";
				break;

			case SortDirection.Descending:
				m_SortDirection = "DESC";
				break;
		}

		return m_SortDirection;
	}

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

	public void SetUpdate()
	{
		Reset();
		HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;

	}
	public void SetDelete()
	{
		Reset();
		HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
	}

	public void SetError()
	{
		Reset();
		HidePanels();
        SetError1();
        //PanError.Visible = true;
	}

	public void InitUpdate()
	{
		HidePanels();
		PanAddUpdate.Visible = true;
		btnUpdate.Visible = true;
		//   lblAddUpdate.Text = "Update ";
	}
	private void HidePanels()
	{
		PanSuccess.Visible = false;
		PanError.Visible = false;
		PanNoRecord.Visible = false;
		PanAddUpdate.Visible = false;
	}
	public void Reset()
	{
		PanAddUpdate.Visible = true;
		//	tbsitename.Text = string.Empty;

	}
	//================================== End Common Code =====================================



}
