<%@ Page Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="adminutilities.aspx.cs" Inherits="admin_adminfiles_utilities_adminutilities"
    Theme="admin" Culture="en-us" UICulture="en-us" EnableEventValidation="true"
    ValidateRequest="false" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header paddright20">
                <table width="100%">
                    <tr>
                        <td width="50%">
                            <h1>Manage Utilities</h1>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="clear">
            </div>

        </div>
    </div>
    <div class="row" id="PanAddUpdate" runat="server">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        Site Name</label>
                                </span><span>
                                    <asp:TextBox ID="tbsitename" runat="server" Width="200" MaxLength="100"></asp:TextBox>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        Site URL</label>
                                </span><span>
                                    <asp:TextBox ID="tbsiteurl" runat="server" MaxLength="100" Width="200"></asp:TextBox>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        MailServer</label>
                                </span><span>
                                    <asp:TextBox ID="txtMailServer" runat="server" MaxLength="100" Width="200"></asp:TextBox>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        From</label>
                                </span><span>
                                    <asp:TextBox ID="txtfrom" runat="server" MaxLength="100" Width="200"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter Valid Email Address"
                                        ControlToValidate="txtfrom" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        CC</label>
                                </span><span>
                                    <asp:TextBox ID="txtcc" runat="server" MaxLength="500" Width="200"></asp:TextBox>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        Authenticate</label>
                                </span><span>
                                    <asp:DropDownList ID="ddlauthenticate" runat="server" AppendDataBoundItems="true"
                                        OnSelectedIndexChanged="ddlauthenticate_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Value="0">No Othenticate</asp:ListItem>
                                        <asp:ListItem Value="1">Simple Othenticate</asp:ListItem>
                                        <asp:ListItem Value="2">G-Mail Othenticate</asp:ListItem>
                                    </asp:DropDownList>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group " id="trusername" runat="server" visible="false">
                                <span class="name">
                                    <label class="control-label">
                                        username</label>
                                </span><span>
                                    <asp:TextBox ID="txtusername" runat="server" MaxLength="500" Width="200"></asp:TextBox>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group " id="trpassword" runat="server" visible="false">
                                <span class="name">
                                    <label class="control-label">
                                        Password</label>
                                </span><span>
                                    <asp:TextBox ID="txtpassword" runat="server" MaxLength="500" Width="200"></asp:TextBox>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group " id="trsslportno" runat="server" visible="false">
                                <span class="name">
                                    <label class="control-label">
                                        EmailSSLPortNo</label>
                                </span><span>
                                    <asp:TextBox ID="txtsslportno" runat="server" MaxLength="500" Width="200"></asp:TextBox>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group " id="trsslportvalue" runat="server" visible="false">
                                <span class="name">
                                    <label class="control-label">
                                        EmailSSLValue</label>
                                </span><span>
                                    <asp:TextBox ID="txtsslvalue" runat="server" MaxLength="50" Width="200"></asp:TextBox>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        Order Subject</label>
                                </span><span>
                                    <asp:TextBox ID="txtordersubject" runat="server" MaxLength="500" Width="300"></asp:TextBox>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        Order Thanks Message</label>
                                </span><span>
                                    <asp:TextBox ID="txtorderthanksmessage" runat="server" MaxLength="500" Width="300"
                                        TextMode="MultiLine" Height="100"></asp:TextBox>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        Contact us subject</label>
                                </span><span>
                                    <asp:TextBox ID="txtcontactussubject" runat="server" MaxLength="500" Width="300"></asp:TextBox>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        Contact Us Thanks message</label>
                                </span><span>
                                    <asp:TextBox ID="txtcontactusthenksmsg" runat="server" MaxLength="500" Width="300"
                                        TextMode="MultiLine" Height="100"></asp:TextBox>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        Regards Name</label>
                                </span><span>
                                    <asp:TextBox ID="txtregardsname" runat="server" MaxLength="500" Width="300" TextMode="MultiLine"
                                        Height="100"></asp:TextBox>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        Email Top</label>
                                </span><span>
                                    <asp:FileUpload ID="fulemailtop" runat="server" />
                                    <asp:Image ID="imageemailtop" runat="server" Height="50px" Width="50px" />
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        Email Bottom</label>
                                </span><span>
                                    <asp:FileUpload ID="fulemailbottom" runat="server" />
                                    <asp:Image ID="imageemailbottom" runat="server" Height="50px" Width="50px" />
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        Email Body Color</label>
                                </span><span>
                                    <asp:TextBox ID="txtemailbodycolor" runat="server" MaxLength="500" Width="200"></asp:TextBox>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        Site Logo Upload</label>
                                </span><span>
                                    <asp:FileUpload ID="fulsitelogo" runat="server" />
                                    <asp:Image ID="imagesitelogo" runat="server" Height="50px" Width="50px" />
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        Admin Footer</label>
                                </span><span>
                                    <CKEditor:CKEditorControl ID="txtadminfooter" runat="server" BasePath="~/ckeditor">
                                    </CKEditor:CKEditorControl>
                                    <asp:CustomValidator runat="server" ID="CustomValidator1" ControlToValidate="txtadminfooter"
                                        SetFocusOnError="true" Display="dynamic" ErrorMessage="*" ClientValidationFunction="ValidateContentText"
                                        ValidateEmptyText="true"></asp:CustomValidator>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        Paypal Account Name</label>
                                </span><span>
                                    <asp:TextBox ID="txtpaypalaccountname" runat="server" MaxLength="500" Width="200"></asp:TextBox>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        Paypal mode</label>
                                </span><span>
                                    <asp:TextBox ID="txtpaypalmode" runat="server" MaxLength="500" Width="200"></asp:TextBox>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                        Paypal Currency Symbol</label>
                                </span><span>
                                    <asp:TextBox ID="txtcurrencysymbol" runat="server" MaxLength="500" Width="200"></asp:TextBox>
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="form-group ">
                                <span class="name">
                                    <label class="control-label">
                                    </label>
                                </span><span>
                                    <asp:Button class="btn btn-dark-grey" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                        Text="Save" Visible="false" />
                                </span>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                        <asp:Panel ID="PanSuccess" runat="server" CssClass="tick">
                            <asp:Label ID="lblSuccess" runat="server" Text="Transaction Successful."></asp:Label>
                        </asp:Panel>
                        <asp:Panel ID="PanError" runat="server" CssClass="cross">
                            <asp:Label ID="lblError" runat="server" Text="Transaction Failed."></asp:Label>
                        </asp:Panel>
                        <asp:Panel ID="PanNoRecord" runat="server" CssClass="cross">
                            <asp:Label ID="lblNoRecord" runat="server" Text="There are no items to show in this view."></asp:Label>
                        </asp:Panel>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
