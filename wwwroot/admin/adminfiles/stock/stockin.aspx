<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="stockin.aspx.cs" Inherits="admin_adminfiles_stock_stockin" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }
    </style>
    <%--<script src="~/admin/vendor/jquery/dist/jquery.min.js"></script>--%>

    <script type="text/javascript">
        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }
    </script>


    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="btnSearch" />--%>
        </Triggers>
    </asp:UpdatePanel>



    <script>

        $(document).ready(function () {
        });

        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    // alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }

   <%--     function formValidate2() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {

                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                        $('#div3').css('display', 'block');
                        $('#<%=divMatch.ClientID %>').hide();
                    }
                }
            }
        }--%>

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');
            callMultiCheckbox();

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress

            $('.loading-container').css('display', 'none');

            $(".dropdown dt a").on('click', function () {
                $(".dropdown dd ul").slideToggle('fast');
            });

            $(".dropdown dd ul li a").on('click', function () {
                $(".dropdown dd ul").hide();
            });
            callMultiCheckbox();

            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });

        }
        function pageLoaded() {

        <%--    $('#div3').css("display", "none");
            $('#<%=btnRevert.ClientID %>').click(function () {
                formValidate2();
            });--%>

            $('.loading-container').css('display', 'none');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            //alert($(".search-select").attr("class"));

            $(".myval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".myvalinvoiceissued").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
            //gridviewScroll();

            callMultiCheckbox();

            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            //  callMultiCheckbox();

        }
    </script>
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Stock In
          <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
        </h5>

    </div>



    <asp:Panel runat="server" ID="PanGridSearch">
        <div class="content animate-panel" style="padding-bottom: 0px!important;">
            <div class="messesgarea">

                <%-- <div class="alert alert-info" id="PanNoRecord" runat="server">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>--%>
            </div>
        </div>

        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <%--<div class="panel-heading">
                            <div class="panel-tools">
                                
                            </div>
                            <br />
                        </div>--%>
                        <div>
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="Row">
                                    <div class="">




                                        <div class="col-md-12" id="divright" runat="server">
                                            <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" AutoPostBack="true">
                                                <cc1:TabPanel ID="TabDeduct" runat="server" HeaderText="Stock In">

                                                    <ContentTemplate>
                                                        <%--<asp:UpdatePanel runat="server" ID="UpdateDeduct">
                                                                    <ContentTemplate>--%>

                                                        <div align="right">
                                                            <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>


                                                            <asp:LinkButton ID="lbtnExport" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                                CausesValidation="false" OnClick="lbtnExport1_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                        </div>
                                                        <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>


                                                        <%-- </ol>--%>

                                                        <div class="messesgarea">
                                                            <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="searchfinal">
                                                                    <div class="widget-body shadownone brdrgray">
                                                                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                            <div class="dataTables_filter">
                                                                                <asp:Panel ID="Panel1" runat="server" DefaultButton="btnSearch">
                                                                                    <div class="dataTables_filter Responsive-search printpage searchfinal">
                                                                                        <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <div class="inlineblock martop5">
                                                                                                        <div>
                                                                                                            <div class="input-group col-sm-1 martop5">
                                                                                                                <asp:TextBox ID="txtProjectNumber" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="txtProjectNumber"
                                                                                                                    WatermarkText="ProjectNumber" />
                                                                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender1" MinimumPrefixLength="2" runat="server"
                                                                                                                    UseContextKey="true" TargetControlID="txtProjectNumber" ServicePath="~/Search.asmx"
                                                                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search"
                                                                                                                    ControlToValidate="txtProjectNumber" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                                                            </div>
                                                                                                            <div class="input-group col-sm-1 martop5" id="divCustomer" runat="server">
                                                                                                                <asp:DropDownList ID="ddllocationsearch" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                                                                    <asp:ListItem Value="">Location</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>

                                                                                                            <div class="input-group col-sm-1 martop5">
                                                                                                                <asp:TextBox ID="txtSerachCity" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender10" runat="server" TargetControlID="txtSerachCity"
                                                                                                                    WatermarkText="City" />
                                                                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender5" MinimumPrefixLength="2" runat="server"
                                                                                                                    UseContextKey="true" TargetControlID="txtSerachCity" ServicePath="~/Search.asmx"
                                                                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCitiesList"
                                                                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                                                                </cc1:AutoCompleteExtender>
                                                                                                            </div>

                                                                                                            <div class="input-group col-sm-1 martop5" id="div1" runat="server">
                                                                                                                <asp:DropDownList ID="ddlSearchState" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                                                    <asp:ListItem Value="">State</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>

                                                                                                            <div class="input-group col-sm-1 martop5">
                                                                                                                <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch"
                                                                                                                    WatermarkText="Project Name" />
                                                                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender3" MinimumPrefixLength="2" runat="server"
                                                                                                                    UseContextKey="true" TargetControlID="txtSearch" ServicePath="~/Search.asmx"
                                                                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectList"
                                                                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                                                            </div>



                                                                                                            <div class="input-group col-sm-1 martop5" id="div2" runat="server">
                                                                                                                <asp:DropDownList ID="ddlSearchDate" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                    <asp:ListItem Value="1" Selected="True">InstallBookingDate</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>

                                                                                                            <div class="input-group date datetimepicker1 col-sm-1 martop5" style="width: 150px;">
                                                                                                                <span class="input-group-addon">
                                                                                                                    <span class="fa fa-calendar"></span>
                                                                                                                </span>
                                                                                                                <asp:TextBox ID="txtStartDate" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                                                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                                                                                                    ControlToValidate="txtStartDate" ErrorMessage="* Required" CssClass="errormessage"></asp:RequiredFieldValidator>--%>
                                                                                                            </div>
                                                                                                            <div class="input-group date datetimepicker1 col-sm-1 martop5" style="width: 150px;">
                                                                                                                <span class="input-group-addon">
                                                                                                                    <span class="fa fa-calendar"></span>
                                                                                                                </span>
                                                                                                                <asp:TextBox ID="txtEndDate" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                                                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic" CssClass="errormessage"
                                                                                                                    ControlToValidate="txtEndDate" ErrorMessage="* Required"></asp:RequiredFieldValidator>--%>
                                                                                                                <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                                                                    ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" Operator="GreaterThanEqual"
                                                                                                                    Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                                                                            </div>
                                                                                                            <div class="form-group spical multiselect martop5" style="width: 142px">
                                                                                                                <dl class="dropdown">
                                                                                                                    <dt>
                                                                                                                        <a href="#">
                                                                                                                            <span class="hida" id="spanselect">Select</span>
                                                                                                                            <p class="multiSel"></p>
                                                                                                                        </a>
                                                                                                                    </dt>
                                                                                                                    <dd id="ddproject" runat="server">
                                                                                                                        <div class="mutliSelect" id="mutliSelect">
                                                                                                                            <ul>
                                                                                                                                <asp:Repeater ID="lstSearchStatus" runat="server">
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <li>
                                                                                                                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />
                                                                                                                                            <%--  <span class="checkbox-info checkbox">--%>
                                                                                                                                            <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                                                            <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                                                                                <span></span>
                                                                                                                                            </label>
                                                                                                                                            <%-- </span>--%>
                                                                                                                                            <label class="chkval">
                                                                                                                                                <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                                                                                            </label>
                                                                                                                                        </li>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:Repeater>
                                                                                                                            </ul>
                                                                                                                        </div>
                                                                                                                    </dd>
                                                                                                                </dl>
                                                                                                            </div>
                                                                                                            <div class="input-group martop5">
                                                                                                                <asp:LinkButton ID="btnSearch" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon"
                                                                                                                    CausesValidation="false" OnClick="btnSearch_Click"></asp:LinkButton>
                                                                                                                <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                                                                                            </div>
                                                                                                            <div class="form-group martop5">
                                                                                                                <asp:LinkButton ID="btnClearAll" runat="server"
                                                                                                                    CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                                                            </div>
                                                                                                            <div class="form-group" style="display: none;">
                                                                                                                <div>
                                                                                                                    <div class="checkbox-info paddtop3td alignchkbox btnviewallorange martop5">
                                                                                                                        <label for="<%=chkHistoric.ClientID %>" class="btn btn-magenta ">
                                                                                                                            <asp:CheckBox ID="chkHistoric" runat="server" />
                                                                                                                            <span class="text">
                                                                                                                                <asp:Label ID="Label2" runat="server" class="control-label">
                                                                                                                                   Historic</asp:Label></span>
                                                                                                                        </label>


                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>

                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="inlineblock" style="margin-top: 3px;">
                                                                                                        <div>
                                                                                                            <div class="datashowbox">
                                                                                                                <div class="leftarea1">
                                                                                                                    <div class="showenteries showdata">
                                                                                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                                                                                        <asp:ListItem Value="10">Show entries</asp:ListItem>
                                                                                                                                    </asp:DropDownList>

                                                                                                                                </td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </asp:Panel>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="finalgrid">
                                                                    <div class="table-responsive printArea">
                                                                        <div id="PanGrid" runat="server">
                                                                            <div class="table-responsive xscroll ">
                                                                                <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                                                                    OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand"
                                                                                    OnDataBound="GridView1_DataBound" AllowSorting="true" OnRowCreated="GridView1_RowCreated" AutoGenerateColumns="false" AllowPaging="true">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="DeductDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                            ItemStyle-HorizontalAlign="Center">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="Label21" runat="server" Width="100px">
                                                                    <%#Eval("StockDeductDate","{0: dd MMM yyyy}")%></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="P.No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                            ItemStyle-HorizontalAlign="Center" SortExpression="ProjectNumber">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="Label22" runat="server" Width="60px">
                                                                                                    <asp:HiddenField ID="hndProjectID" runat="server" Value='<%#Eval("ProjectID")%>' />
                                                                                                    <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                                        NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--<asp:TemplateField HeaderText="Project" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                            ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label12" runat="server" Width="250px">
                                                                                    <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                        NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("Project")%></asp:HyperLink></asp:Label>--%>
                                                                                        <%--<asp:Label ID="Label23" runat="server" Width="300px">
                                                                                    <asp:LinkButton ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail"
                                                                                        CausesValidation="false" CommandName="viewproject" CommandArgument='<%#Eval("ProjectID")+"|"+Eval("CustomerID") +"|"+Eval("ContactID")%>'><%#Eval("Project")%></asp:LinkButton></asp:Label>--%>
                                                                                        <%--</ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                                        <asp:TemplateField HeaderText="P.Status" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                            ItemStyle-HorizontalAlign="Center" SortExpression="ProjectStatus">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="Label266" runat="server" Width="80px">
                                                                    <%#Eval("ProjectStatus")%></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="InstallDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                            ItemStyle-HorizontalAlign="Center" SortExpression="InstallBookingDate">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="Label24" runat="server" Width="80px">
                                                                    <%#Eval("InstallBookingDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Installer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="InstallerName">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="Label25" runat="server" Width="150px">
                                                                    <%#Eval("InstallerName").ToString() == "" ? "-" : Eval("InstallerName")%></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="N.Panels" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                            <ItemTemplate>
                                                                                                <asp:Literal runat="server" ID="ltallocatedpanels" Text='<%# Eval("NumberPanels")%>'></asp:Literal>
                                                                                                <asp:HiddenField runat="server" ID="hdnStockItem" Value='<%#Eval("StockItemID")%>' />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Deducted" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                            <ItemTemplate>
                                                                                                <asp:Literal runat="server" ID="ltdeductpanels" Text='<%# Eval("NumberPanels")%>'></asp:Literal>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Revert" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Center" ControlStyle-CssClass="printpage">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblrevert" runat="server" Width="30px" Text='<%#(Convert.ToInt32(Eval("NumberPanels"))+Convert.ToInt32(Eval("StockSum")) != 0) ? Convert.ToInt32(Eval("StockSum"))*(-1): Convert.ToInt32(0)%>'>                                                                                                
                                                                                                </asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Remaining" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label runat="server" ID="ltremainingpanels" Text='<%#Convert.ToInt32(Eval("NumberPanels"))+Convert.ToInt32(Eval("StockSum"))%>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField HeaderText="Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                            ItemStyle-HorizontalAlign="Left">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="Label26" runat="server" Width="300px">
                                                                    <%#Eval("SystemDetails").ToString() == "" ? "-" : Eval("SystemDetails")%></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Store Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="StockAllocationStoreName">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="Label27" runat="server" Width="100px">
                                                                                                    <asp:HiddenField ID="hndStockAllocationStore" runat="server" Value='<%#Eval("StockAllocationStore") %>' />
                                                                                                    <%#Eval("StockAllocationStoreName")%></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridheadertext brdrgrayright" ItemStyle-HorizontalAlign="Center">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="Label17" runat="server" Width="80px">
                                                                                                    <asp:LinkButton ID="lbtnDeduct" CommandName="revert" CommandArgument='<%#Eval("ProjectID")%>' CausesValidation="false" runat="server">Revert</asp:LinkButton>
                                                                                                </asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>

                                                                                    </Columns>
                                                                                    <PagerTemplate>
                                                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                                        <div class="pagination">
                                                                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                                                        </div>
                                                                                    </PagerTemplate>
                                                                                    <PagerStyle CssClass="paginationGrid" />
                                                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                                                </asp:GridView>
                                                                            </div>
                                                                            <div class="paginationnew1" runat="server" id="divnopage" visible="false">
                                                                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%-- </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnSearch" />
                                                                         <asp:PostBackTrigger ControlID="GridView1" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>--%>
                                                    </ContentTemplate>
                                                </cc1:TabPanel>

                                                <cc1:TabPanel ID="TabRevert" runat="server" HeaderText="Revert">
                                                    <ContentTemplate>
                                                        <%-- <asp:UpdatePanel runat="server" ID="updaterevert">
                                                                    <ContentTemplate>--%>
                                                        <div align="right">

                                                            <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>


                                                            <asp:LinkButton ID="LinkButton5" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                                CausesValidation="false"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>


                                                        </div>
                                                        <div class="messesgarea">
                                                            <div class="alert alert-info" id="PanNoRecord4" runat="server" visible="false">
                                                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                            </div>
                                                        </div>
                                                        <div class="searchfinal">
                                                            <div class="widget-body shadownone brdrgray">
                                                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                    <div class="dataTables_filter">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <asp:Panel ID="Panel2" runat="server" DefaultButton="btnsearch2">
                                                                                    <div class="dataTables_filter Responsive-search ">
                                                                                        <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <div class="inlineblock">
                                                                                                        <div>
                                                                                                            <div class="input-group col-sm-1 martop5">
                                                                                                                <asp:TextBox ID="txtProjectNumber2" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtProjectNumber2"
                                                                                                                    WatermarkText="ProjectNumber" />
                                                                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender2" MinimumPrefixLength="2" runat="server"
                                                                                                                    UseContextKey="true" TargetControlID="txtProjectNumber2" ServicePath="~/Search.asmx"
                                                                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ValidationGroup="search"
                                                                                                                    ControlToValidate="txtProjectNumber2" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                                                            </div>
                                                                                                            <div class="input-group col-sm-1 martop5" id="div7" runat="server">
                                                                                                                <asp:DropDownList ID="ddllocationsearch2" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                                                                    <asp:ListItem Value="">Location</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>

                                                                                                            <div class="input-group col-sm-1 martop5">
                                                                                                                <asp:TextBox ID="txtSerachCity2" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender6" runat="server" TargetControlID="txtSerachCity2"
                                                                                                                    WatermarkText="City" />
                                                                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender4" MinimumPrefixLength="2" runat="server"
                                                                                                                    UseContextKey="true" TargetControlID="txtSerachCity2" ServicePath="~/Search.asmx"
                                                                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetCitiesList"
                                                                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20">
                                                                                                                </cc1:AutoCompleteExtender>
                                                                                                            </div>

                                                                                                            <div class="input-group col-sm-1 martop5" id="div8" runat="server">
                                                                                                                <asp:DropDownList ID="ddlSearchState2" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                                                    <asp:ListItem Value="">State</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>


                                                                                                            <div class="input-group col-sm-1 martop5">
                                                                                                                <asp:TextBox ID="txtSearch2" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender7" runat="server" TargetControlID="txtSearch2"
                                                                                                                    WatermarkText="Project Name" />
                                                                                                                <cc1:AutoCompleteExtender ID="AutoCompleteExtender6" MinimumPrefixLength="2" runat="server"
                                                                                                                    UseContextKey="true" TargetControlID="txtSearch2" ServicePath="~/Search.asmx"
                                                                                                                    CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectList"
                                                                                                                    EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />
                                                                                                            </div>
                                                                                                            <div class="input-group col-sm-1 martop5" id="div11" runat="server">
                                                                                                                <asp:DropDownList ID="ddlSearchDate2" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                    <asp:ListItem Value="3" Selected="True">Revert Date</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>
                                                                                                            <div class="input-group date datetimepicker1 col-sm-1 martop5" style="width: 150px;">
                                                                                                                <span class="input-group-addon">
                                                                                                                    <span class="fa fa-calendar"></span>
                                                                                                                </span>
                                                                                                                <asp:TextBox ID="txtStartDate2" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="dynamic" CssClass="errormessage"
                                                                                                                    ControlToValidate="txtStartDate2" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                                                                            </div>
                                                                                                            <div class="input-group date datetimepicker1 col-sm-1 martop5" style="width: 150px;">
                                                                                                                <span class="input-group-addon">
                                                                                                                    <span class="fa fa-calendar"></span>
                                                                                                                </span>
                                                                                                                <asp:TextBox ID="txtEndDate2" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="dynamic" CssClass="errormessage"
                                                                                                                    ControlToValidate="txtEndDate2" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                                                                                <asp:CompareValidator ID="CompareValidator3" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                                                                    ControlToCompare="txtStartDate2" ControlToValidate="txtEndDate2" Operator="GreaterThanEqual"
                                                                                                                    Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                                                                            </div>

                                                                                                            <div class="input-group col-sm-1 martop5" id="div9" runat="server">
                                                                                                                <asp:DropDownList ID="ddlRevert" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                    <asp:ListItem Value="1">Revert</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2">Revert All</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>
                                                                                                            <div class="input-group martop5">
                                                                                                                <asp:LinkButton ID="btnsearch2" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon" OnClick="btnSearch2_Click"
                                                                                                                    CausesValidation="false"></asp:LinkButton>
                                                                                                                <%--  <asp:Button ID="btnsearch2" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnsearch2_Click" />--%>
                                                                                                            </div>
                                                                                                            <div class="form-group martop5">
                                                                                                                <asp:LinkButton ID="btnClearAll2" runat="server"
                                                                                                                    CausesValidation="false" OnClick="btnClearAll2_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <div class="inlineblock" style="margin-top: 3px;">
                                                                                                        <div>

                                                                                                            <div class="datashowbox">
                                                                                                                <div class="leftarea1">
                                                                                                                    <div class="showenteries showdata">
                                                                                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:DropDownList ID="ddlSelectRecords2" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords2_SelectedIndexChanged"
                                                                                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                                                                                        <asp:ListItem Value="10">Show entries</asp:ListItem>
                                                                                                                                    </asp:DropDownList></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>




                                                                                                        </div>
                                                                                                    </div>

                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </asp:Panel>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="finalgrid">
                                                                <div class="table-responsive printArea">
                                                                    <div id="Div10" runat="server">
                                                                        <div class="table-responsive xscroll" id="PanGrid2" runat="server">
                                                                            <asp:GridView ID="GridView2" DataKeyNames="ProjectID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover printarea"
                                                                                OnSorting="GridView2_Sorting" OnPageIndexChanging="GridView2_PageIndexChanging" OnRowCreated="GridView2_RowCreated"
                                                                                OnDataBound="GridView2_DataBound"
                                                                                AllowSorting="true" AutoGenerateColumns="false" AllowPaging="true" PageSize="10">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Project No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Center" SortExpression="ProjectNumber" HeaderStyle-CssClass="brdrgrayleft">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label11" runat="server" Width="100px">
                                                                                                <asp:HiddenField ID="hndProjectID" runat="server" Value='<%#Eval("ProjectID")%>' />
                                                                                                <%#Eval("ProjectNumber")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Project" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                                        ItemStyle-HorizontalAlign="Left" SortExpression="Project">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label12" runat="server" Width="250px">
                                                                                                <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                                    NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("Project")%></asp:HyperLink></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Revert Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Center" SortExpression="RevertDate">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label19" runat="server" Width="100px">
                                                                                         <%#Eval("RevertDate","{0:dd MMM yyyy}")%>
                                                                                            </asp:Label>

                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="InstallBookingDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Center" SortExpression="ProjectNumber">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label13" runat="server" Width="100px">
                                                                        <%#Eval("InstallBookingDate","{0:dd MMM yyyy}")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Revert Panel" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Center" SortExpression="Stock">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label119" runat="server" Width="100px">
                                                                                         <%#Eval("Stock")%>
                                                                                            </asp:Label>

                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <%--<asp:TemplateField HeaderText="Installer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                ItemStyle-HorizontalAlign="Left" SortExpression="InstallerName">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="Label14" runat="server" Width="120px">
                                                                        <%#Eval("InstallerName").ToString() == "" ? "-" : Eval("InstallerName")%></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>--%>
                                                                                    <asp:TemplateField HeaderText="Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Left" SortExpression="SystemDetails">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label15" runat="server" Width="290px">
                                                                        <%#Eval("SystemDetails").ToString() == "" ? "-" : Eval("SystemDetails")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <%--<asp:TemplateField HeaderText="Store Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                                ItemStyle-HorizontalAlign="Left" SortExpression="StockAllocationStoreName">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="Label16" runat="server" Width="100px">
                                                                        <%#Eval("StockAllocationStoreName")%></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>--%>
                                                                                    <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="printorder"
                                                                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px">
                                                                                        <ItemTemplate>
                                                                                            <asp:HiddenField Value='<%#Eval("ProjectID") %>' runat="server" ID="hdnstockid" />
                                                                                            <asp:LinkButton CssClass="printorder" ID="lnkReceived" CausesValidation="false" runat="server" OnClick="lnkReceived_Click" data-toggle="tooltip" data-placement="top" data-original-title="Detail">
                                                                                              
                                                                                                <a target="_blank" href="#" data-original-title="Detail" data-placement="top" data-toggle="tooltip" class="btn btn-primary btn-xs"> <i class="fa fa-link"></i> Detail</a>
                                                                                            </asp:LinkButton>

                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <PagerTemplate>
                                                                                    <asp:Label ID="ltrPage2" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                                    <div class="pagination">
                                                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                                                    </div>
                                                                                </PagerTemplate>
                                                                                <PagerStyle CssClass="paginationGrid" />
                                                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <div class="paginationnew1" runat="server" id="divnopage2" visible="false">
                                                                            <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage2" style="width: 100%; border-collapse: collapse;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="ltrPage2" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%-- </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnsearch2" />
                                                                        <asp:PostBackTrigger ControlID="GridView2" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>--%>
                                                    </ContentTemplate>
                                                </cc1:TabPanel>

                                            </cc1:TabContainer>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>


    <asp:Button ID="btnNULL" Style="display: none;" runat="server" />

    <cc1:ModalPopupExtender ID="ModalPopupExtenderDR" runat="server" BackgroundCssClass="modalbackground"
        CancelControlID="ibtnCancel2" DropShadow="false" PopupControlID="divStockItems2"
        OkControlID="btnOK2" TargetControlID="btnNULL2">
    </cc1:ModalPopupExtender>
    <div runat="server" id="divStockItems2" style="display: none;">

        <div class="modal-dialog" style="width: 900px;">
            <div class="modal-content">
                <div class="modal-header">
                    <div style="float: right">
                        <button id="ibtnCancel2" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">
                            Close</button>
                    </div>
                    <h4 class="modal-title" id="myModalLabel">Project Stock Revert</h4>
                </div>
                <div class="modal-body paddnone" style="overflow-y: scroll; height: 350px;">
                    <div class="panel-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <table cellpadding="5" cellspacing="5" border="0" class="table">
                                    <tr>
                                        <td width="150px"><b>Customer</b></td>
                                        <td>
                                            <asp:Label ID="lblCustomer2" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Project</b></td>
                                        <td>
                                            <asp:Label ID="lblProject2" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Panel Details</b></td>
                                        <td class="stockdudect">
                                            <asp:Label ID="lblPanelDetails2" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Inverter Details</b></td>
                                        <td>
                                            <asp:Label ID="lblInverterDetails2" runat="server" Style="width: 100%!important;"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Install City</b></td>
                                        <td>
                                            <asp:Label ID="lblInstallCity2" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Install State</b></td>
                                        <td>
                                            <asp:Label ID="lblInstallState2" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Roof Type</b></td>
                                        <td>
                                            <asp:Label ID="lblRoofType2" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px" colspan="2">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <%--<div class="clear"></div>
                        <br />--%>
                        <div class="col-md-12">
                            <div class="form-group">
                                <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover">
                                    <tr>
                                        <td width="50%"><b>Stock Item</b></td>
                                        <td width="20%"><b>Deduct</b></td>
                                        <td width="30%"><b>Qty</b></td>
                                    </tr>
                                    <asp:Repeater ID="rptDeductRev" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td width="50%">
                                                    <asp:HiddenField ID="hndStockItemID" runat="server" Value='<%#Eval("StockItemID") %>' />
                                                    <%#Eval("StockItem") %>                                                     
                                                </td>
                                                <td width="20%">
                                                    <asp:Label ID="lblStockDeduct" runat="server" Text='<%#Eval("Deduct","{0:0}") %>'></asp:Label>
                                                </td>
                                                <td width="30%">
                                                    <table>
                                                        <tr>
                                                            <td style="padding-right: 5px;">
                                                                <asp:TextBox ID="txtDeductRevert" runat="server" MaxLength="4" Width="50px" CssClass="form-control" Style="margin: 0;"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtDeductRevert" FilterType="Numbers" />
                                                            </td>
                                                            <td>
                                                                <asp:CompareValidator ID="CompareValidatorDR" runat="server" ControlToValidate="txtDeductRevert" ValueToCompare='<%#Eval("Deduct","{0:0}") %>'
                                                                    Type="Integer" Display="Dynamic" ErrorMessage="Enter valid stock" Operator="LessThanEqual" Style="color: red;" ValidationGroup="abc"></asp:CompareValidator>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server" Style="color: red;" ValidationGroup="abc"
                                                                    ControlToValidate="txtDeductRevert" Display="Dynamic" ErrorMessage="Qty can't be Zero." ValidationExpression="^[1-9]\d*$"></asp:RegularExpressionValidator>                                                                
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <br />
                        <div class="col-md-12">
                            <div class="form-group">
                                <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover">
                                    <tr>
                                        <td width="10%"><b>Sr No.</b></td>
                                        <td width="40%"><b>Pallet Number</b></td>
                                        <td width="40%"><b>Serial Number</b></td>
                                        <td width="10%"><b></b></td>
                                    </tr>
                                    <asp:Repeater ID="rptDeductRevSerial" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td width="10%">
                                                    <asp:HiddenField ID="hdnInventoryHistId" runat="server" Value='<%# Eval("InventoryHistoryId") %>' />
                                                    <asp:Label ID="lblSrNo" runat="server" Text='<%# Container.ItemIndex + 1 %>'></asp:Label>
                                                </td>
                                                <td width="40%">
                                                    <asp:Label ID="lblPallet" runat="server" Text='<%# Eval("Pallet") %>'></asp:Label>
                                                </td>
                                                <td width="40%">
                                                    <asp:Label ID="lblSerialNo" runat="server" Text='<%# Eval("SerialNo") %>'></asp:Label>
                                                </td>
                                                <td width="10%" style="text-align: center;">
                                                    <label for='<%# Container.FindControl("chkSerialNo").ClientID  %>' runat="server" id="lblchk123" cssclass="form-control">
                                                        <asp:CheckBox ID="chkSerialNo" runat="server" />
                                                        <span class="text">&nbsp;</span>
                                                    </label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <br />
                        <div class="col-md-12" id="divMatch" runat="server">
                            <div class="form-group" style="text-align: center">
                                <div class="alert alert-danger">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Selected Check boxes and Panel Qty didn't matched.</strong>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <br />
                        </div>
                        <div class="col-md-12" id="div3" style="display:none;">
                            <div class="form-group" style="text-align: center">
                                <div class="alert alert-danger">
                                    <i class="icon-remove-sign"></i><strong>&nbsp;Qty cannot be greater than deduct quantity.</strong>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <br />
                        </div>
                        <div class="col-md-12" align="center">
                            <div class="form-group">
                                <asp:Button ID="btnRevert" runat="server" Text="Revert" OnClick="btnRevert_Click" CssClass="btn btn-purple resetbutton"
                                    CausesValidation="true" ValidationGroup="abc" />
                                <button id="btnRevertAll" type="submit" runat="server" causesvalidation="false" class="btn btn-purple resetbutton" onserverclick="btnRevertAll_Click">
                                    Revert All</button><%--                             
                                <%--     <asp:Button runat="server"  OnClick="btnRevertAll_Click" class="btn btn-purple resetbutton"  Text="Revert All" />--%>
                                <asp:Button ID="btnOK2" Style="display: none;" runat="server"
                                    CssClass="btn" Text=" OK" Visible="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnNULL2" Style="display: none;" runat="server" />

    <asp:Button ID="btnNULLMove" Style="display: none;" runat="server" />
    <asp:HiddenField ID="hndProjectID" runat="server" />

    <asp:Button ID="Button2" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
        DropShadow="false" PopupControlID="divstockdetail" TargetControlID="Button2">
    </cc1:ModalPopupExtender>
    <div id="divstockdetail" runat="server" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div style="float: right">
                        <asp:LinkButton ID="Button3" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" OnClick="Button3_Click">
                        Close
                        </asp:LinkButton>
                    </div>
                    <h4 class="modal-title" id="H3">
                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                        Stock Revert Details</h4>
                </div>
                <div class="modal-body paddnone" runat="server" id="divdetail">
                    <div class="panel-body" style="overflow: scroll;">
                        <div class="formainline">
                            <div class="panel panel-default">
                                <div class="panel-body formareapop heghtauto" style="background: none!important;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <span class="name disblock">
                                                    <label class="control-label">
                                                    </label>
                                                </span><span>
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                        <thead>
                                                            <tr>
                                                                <td style="width: 70%">Stock Item
                                                                </td>
                                                                <td style="width: 20%">Stock
                                                                </td>
                                                            </tr>
                                                        </thead>
                                                        <asp:Repeater ID="rptstockdetail" runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td style="width: 70%;">
                                                                        <%#Eval("StockItem")%>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <%#Eval("Stock")%>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body paddnone" runat="server" id="divdetailmsg" visible="false">
                    <div class="panel-body" style="overflow: scroll;">
                        <div class="formainline">
                            <div class="panel panel-default">
                                <div class="panel-body formareapop heghtauto" style="background: none!important;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <span class="name disblock">
                                                    <label class="control-label">
                                                    </label>
                                                </span><span>

                                                    <div class="messesgarea">
                                                        <div class="alert alert-info" id="Div12" runat="server">
                                                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                        </div>
                                                    </div>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnstocktransferid" />


    <asp:Button ID="Button8" Style="display: none;" runat="server" />


    <script>

        function printContent() {
            var PageHTML = document.getElementById('<%= (PanGrid.ClientID) %>').innerHTML;
            var html = '<html><head>' +
                '<link href="../../assets/styles/print.css" rel="stylesheet" type="text/css" media="print"/>' +
                '</head><body style="background:#ffffff;">' +
                PageHTML +
                '</body></html>';
            var WindowObject = window.open("", "PrintWindow",
                "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
            WindowObject.document.writeln(html);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
            document.getElementById('print_link').style.display = 'block';
        }
    </script>


    <script type="text/javascript">

        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');

        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });


        $(document).ready(function () {
            HighlightControlToValidate();

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
        });

        function callMultiCheckbox() {
            var title = "";

            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }

        }


        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
        }
    </script>

</asp:Content>
