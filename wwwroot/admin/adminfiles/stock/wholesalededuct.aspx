<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master"
    AutoEventWireup="true" CodeFile="wholesalededuct.aspx.cs" Inherits="admin_adminfiles_stock_wholesalededuct" Culture="en-GB" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .modal-dialog1 {
            margin-left: -300px;
            margin-right: -300px;
            width: 985px;
        }
    </style>
    <%--<script src="<%=Siteurl %>admin/vendor/jquery/dist/jquery.min.js"></script>--%>

    <script type="text/javascript">

        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
        }
        document.onkeypress = stopRKey;

        function divexpandcollapse(divname, trname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var tr = document.getElementById(trname);

            if (div.style.display == "none") {
                div.style.display = "inline";
                tr.style.display = "";
                img.src = "../../../images/icon_minus.png";
            } else {
                div.style.display = "none";
                tr.style.display = "none";
                img.src = "../../../images/icon_plus.png";
            }
        }
    </script>


    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>
        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="btnSearch" />--%>
        </Triggers>
    </asp:UpdatePanel>



    <script>

        $(document).ready(function () {
            $('#<%=lnkSearchPallet.ClientID %>').click(function (e) {
                formValidate();

            });

            $('#<%=btnSearchInvoiceNo2.ClientID %>').click(function (e) {
                formValidate();

            });

        });
        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    alert("2");
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoaded);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);
        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');
            callMultiCheckbox();
        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress

            $('.loading-container').css('display', 'none');

            $(".dropdown dt a").on('click', function () {
                $(".dropdown dd ul").slideToggle('fast');
            });

            $(".dropdown dd ul li a").on('click', function () {
                $(".dropdown dd ul").hide();
            });
            callMultiCheckbox();

            $(document).bind('click', function (e) {
                var $clicked = $(e.target);
                if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
            });

        }
        function pageLoaded() {

            $('#<%=lnkSearchPallet.ClientID %>').click(function (e) {
                formValidate();

            });

            $('#<%=btnSearchInvoiceNo2.ClientID %>').click(function (e) {
                formValidate();

            });

            //alert($(".search-select").attr("class"));

            $('.loading-container').css('display', 'none');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            //alert($(".search-select").attr("class"));

            $(".myval").select2({
                // placeholder: "select",
                allowclear: true
            });
            $(".myvalinvoiceissued").select2({
                minimumResultsForSearch: -1
            });
            if ($(".tooltips").length) {
                $('.tooltips').tooltip();
            }
            //gridviewScroll();

            callMultiCheckbox();

            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });

            //  callMultiCheckbox();


        }
    </script>
    <div class="page-body headertopbox">
        <h5 class="row-title"><i class="typcn typcn-th-small"></i>Wholesale Stock Deduct
          <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
        </h5>

    </div>



    <asp:Panel runat="server" ID="PanGridSearch">
        <div class="content animate-panel" style="padding-bottom: 0px!important;">
            <div class="messesgarea">

                <%-- <div class="alert alert-info" id="PanNoRecord" runat="server">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>--%>
            </div>
        </div>

        <div class="content animate-panel">
            <div class="row">
                <div class="col-lg-12">
                    <div class="hpanel">
                        <%--<div class="panel-heading">
                            <div class="panel-tools">
                                
                            </div>
                            <br />
                        </div>--%>
                        <div>
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="Row">
                                    <div class="">
                                        <div class="dataTables_length showdata col-sm-2" id="show" runat="server" visible="false">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>Show
                                                            <asp:DropDownList ID="ddlSelectRecords" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                                aria-controls="DataTables_Table_0" class="form-control input-sm">
                                                            </asp:DropDownList>
                                                        entries
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>



                                        <div class="col-md-12" id="divright" runat="server">
                                            <cc1:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" AutoPostBack="true">
                                                <cc1:TabPanel ID="TabWholesale" runat="server" HeaderText="Wholesale">

                                                    <ContentTemplate>
                                                        <%--<asp:UpdatePanel runat="server" ID="UpdateDeduct">
                                                                    <ContentTemplate>--%>

                                                        <div align="right">
                                                            <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>


                                                            <asp:LinkButton ID="LinkButton6" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                                CausesValidation="false" OnClick="lbtnExport1_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                        </div>
                                                        <%--   <ol class="hbreadcrumb breadcrumb tooltip-demo fontsize16">--%>


                                                        <%-- </ol>--%>

                                                        <div class="messesgarea">
                                                            <div class="alert alert-info" id="PanNoRecord5" runat="server" visible="false">
                                                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                            </div>
                                                        </div>

                                                        <div class="row">

                                                            <div class="col-md-12">
                                                                <div class="searchfinal">
                                                                    <div class="widget-body shadownone brdrgray">
                                                                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                            <div class="dataTables_filter">
                                                                                <div class="form-group" id="filediv">
                                                                                    <div>
                                                                                        <span class="name">
                                                                                            <label class="control-label">
                                                                                                Serial No: Upload Excel File <span class="symbol required"></span>
                                                                                            </label>
                                                                                            <span class="">
                                                                                                <asp:FileUpload ID="PanelFileUpload" runat="server" Style="display: inline-block;" class="fileupdate" />
                                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="PanelFileUpload"
                                                                                                    ValidationGroup="success1" ValidationExpression="^.+(.xls|.XLS|.xlsx|.XLSX)$"
                                                                                                    Display="Dynamic" ErrorMessage=".xls only"></asp:RegularExpressionValidator>
                                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                                                                                    ControlToValidate="PanelFileUpload" Display="Dynamic" ValidationGroup="success1"></asp:RequiredFieldValidator>
                                                                                            </span>

                                                                                            <asp:Button class="btn btn-primary addwhiteicon btnaddicon" ID="btnaddpanel" runat="server" OnClick="btnaddpanel_Click"
                                                                                                Text="Add" ValidationGroup="success1" Style="padding-left: 24px;" />
                                                                                            <%--  <asp:Button class="btn btn-dark-grey calcelwhiteicon btncancelicon" ID="Button2" runat="server" OnClick="btnCancel_Click"
                                                            CausesValidation="false" Text="Cancel" Style="padding-left: 24px;" />--%>

                                                        
                                    
                                                                                        </span>
                                                                                        <div class="clear">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12">
                                                                <div class="searchfinal">
                                                                    <div class="widget-body shadownone brdrgray">
                                                                        <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                            <div class="dataTables_filter">
                                                                                <asp:Panel ID="Panel4" runat="server" DefaultButton="btnSearch5">
                                                                                    <div class="dataTables_filter Responsive-search printpage searchfinal">
                                                                                        <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <div class="inlineblock martop5">
                                                                                                        <div>
                                                                                                            <div class="input-group col-sm-1" style="width: 114px">
                                                                                                                <asp:TextBox ID="txtinvoiceno" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="txtinvoiceno"
                                                                                                                    WatermarkText="InvoiceNumber" />
                                                                                                                <%--    <cc1:AutoCompleteExtender ID="AutoCompleteExtender10" MinimumPrefixLength="2" runat="server"
                                                                                                        UseContextKey="true" TargetControlID="txtinvoiceno" ServicePath="~/Search.asmx"
                                                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />--%>
                                                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="search"
                                                                                                                    ControlToValidate="txtinvoiceno" Display="Dynamic" ErrorMessage="Please enter a number" Style="color: red;"
                                                                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                                                            </div>
                                                                                                            <div class="input-group col-sm-1" id="div17" runat="server">
                                                                                                                <asp:DropDownList ID="ddlloc" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                                                                    <asp:ListItem Value="">Location</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>

                                                                                                            <div class="input-group" id="div18" runat="server" style="width: 114px">
                                                                                                                <asp:DropDownList ID="ddlSearchVendor" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                                                                    <asp:ListItem Value="0">Customer Name</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>





                                                                                                            <div class="input-group" id="div19" runat="server">
                                                                                                                <asp:DropDownList ID="ddldate2" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                                                                                                    <asp:ListItem Value="1" Selected="True">Expected Date</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>

                                                                                                            <div class="input-group date datetimepicker1 col-sm-1" style="width: 150px;">
                                                                                                                <span class="input-group-addon">
                                                                                                                    <span class="fa fa-calendar"></span>
                                                                                                                </span>
                                                                                                                <asp:TextBox ID="txtStartDate2" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" Display="dynamic"
                                                                                                                    ControlToValidate="txtStartDate2" ErrorMessage="* Required" CssClass="errormessage"></asp:RequiredFieldValidator>
                                                                                                            </div>
                                                                                                            <div class="input-group " style="width: 150px;">
                                                                                                                <div class="input-group date datetimepicker1 col-sm-1" style="width: 150px;">
                                                                                                                    <span class="input-group-addon">
                                                                                                                        <span class="fa fa-calendar"></span>
                                                                                                                    </span>
                                                                                                                    <asp:TextBox ID="txtEndDate2" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" Display="dynamic" CssClass="errormessage"
                                                                                                                        ControlToValidate="txtEndDate2" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                                                                                </div>
                                                                                                                <asp:CompareValidator ID="CompareValidator4" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                                                                    ControlToCompare="txtStartDate2" ControlToValidate="txtEndDate2" Operator="GreaterThanEqual" Style="color: red;"
                                                                                                                    Display="Dynamic" ValidationGroup="search"></asp:CompareValidator>
                                                                                                            </div>



                                                                                                            <div class="form-group">
                                                                                                                <div>
                                                                                                                    <div class="checkbox-info paddtop3td alignchkbox btnviewallorange">
                                                                                                                        <label for="<%=histchkbox.ClientID %>" class="btn btn-magenta ">
                                                                                                                            <asp:CheckBox ID="histchkbox" runat="server" />
                                                                                                                            <span class="text">
                                                                                                                                <asp:Label ID="Label3" runat="server" class="control-label">
                                                                                                                                    Historic</asp:Label></span>
                                                                                                                        </label>


                                                                                                                    </div>
                                                                                                                </div>
                                                                                                                <div class="clear"></div>
                                                                                                            </div>

                                                                                                            <div class="input-group col-sm-0">
                                                                                                                <asp:LinkButton ID="btnSearch5" runat="server" Text="Search" ValidationGroup="search" CssClass="btn btn-info btnsearchicon"
                                                                                                                    CausesValidation="true" OnClick="btnSearch_Click5"></asp:LinkButton>
                                                                                                                <%--<asp:Button ID="btnSearch" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />--%>
                                                                                                            </div>
                                                                                                            <div class="input-group col-sm-0">
                                                                                                                <asp:LinkButton ID="btnClearAll" runat="server"
                                                                                                                    CausesValidation="false" OnClick="btnClearAll_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                                                            </div>


                                                                                                        </div>
                                                                                                    </div>
                                                                                                    <div class="inlineblock martop5">
                                                                                                        <div>



                                                                                                            <%-- <div class="form-group">
                                                                                        <asp:CheckBox ID="chkHistoric" runat="server" Class="i-checks" />
                                                                                        <label for="<%=chkHistoric.ClientID %>">
                                                                                            <span></span>
                                                                                        </label>
                                                                                       
                                                                                    </div>--%>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </asp:Panel>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="finalgrid">
                                                                    <div class="table-responsive printArea">
                                                                        <div id="PanGrid5" runat="server">
                                                                            <div class="table-responsive xscroll ">
                                                                                <asp:HiddenField ID="hdnWholesaleOrderID2" runat="server" Value='<%#Eval("WholesaleOrderID")%>' />
                                                                                <asp:GridView ID="GridView5" DataKeyNames="WholesaleOrderID" runat="server" CssClass="tooltip-demo text-center table table-striped GridviewScrollItem table-bordered table-hover Gridview printorder"
                                                                                    OnSorting="GridView5_Sorting" OnRowCommand="GridView5_RowCommand" OnPageIndexChanging="GridView5_PageIndexChanging"
                                                                                    AllowSorting="true" AutoGenerateColumns="false" OnDataBound="GridView5_DataBound" OnRowCreated="GridView5_RowCreated" AllowPaging="true">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Invoice No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="OrderNumber" HeaderStyle-CssClass="brdrgrayleft">
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField ID="hndWholesaleID" runat="server" Value='<%#Eval("WholesaleOrderID")%>' />
                                                                                                <%--<asp:Label ID="Label12" runat="server" Width="100px">
                                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>--%>
                                                                                                <asp:Label ID="Label11" runat="server" Width="100px">
                                                                                        <%#Eval("InvoiceNo")%></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="Vendor">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="Label14" runat="server" Width="120px">
                                                                                             <%#Eval("Vendor")%></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="ExpectedDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="ExpectedDelivery">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="Label13" runat="server" Width="100px">
                                                                        <%#Eval("ExpectedDelivery","{0:dd MMM yyyy}")%></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="500px" SortExpression="WholesaleOrderItem">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lbldetails" runat="server" data-placement="top" data-original-title='<%#Eval("WholesaleOrderItem")%>' data-toggle="tooltip"
                                                                                                    Width="260px"><%#Eval("WholesaleOrderItem")%></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="StoreName" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                                            ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="Label4" runat="server" Width="80px">
                                                                                                 <%#Eval("CompanyLocation")%></asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>

                                                                                        <%--<asp:TemplateField HeaderText="Allocated Panels" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                                <ItemTemplate>
                                                                                    <asp:HiddenField ID="hdnProjectID" runat="server" Value='<%#Eval("ProjectID")%>' />
                                                                                    <asp:LinkButton ID="lbtnAllo" CommandName="allocatepanel" CommandArgument='<%#Eval("ProjectID")%>' CausesValidation="false" runat="server">
                                                                                        <asp:Label ID="lblAllocatePanels" runat="server" Width="80px"> </asp:Label></asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>--%>
                                                                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridheadertext brdrgrayright" ItemStyle-HorizontalAlign="Center">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="Label17" runat="server" Width="100px">
                                                                                                    <asp:LinkButton ID="lbtnDeduct5" CommandName="deduct" CommandArgument='<%#Eval("WholesaleOrderID")%>' CausesValidation="false" runat="server">Deduct</asp:LinkButton>
                                                                                                    <asp:Label ID="lbldiv5" runat="server" Visible='<%# Eval("IsDeduct").ToString()=="1"?true:false %>' Text=" / "></asp:Label>
                                                                                                    <asp:LinkButton ID="lbtnMove5" CommandName="move" CommandArgument='<%#Eval("WholesaleOrderID")%>' Visible='<%# Eval("IsDeduct").ToString()=="1"?true:false %>' CausesValidation="false" runat="server">Move</asp:LinkButton>
                                                                                                </asp:Label>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                    <PagerTemplate>
                                                                                        <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                                        <div class="pagination">
                                                                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                                                            <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                                                            <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                                            <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                            <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                                                            <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                                                        </div>
                                                                                    </PagerTemplate>
                                                                                    <PagerStyle CssClass="paginationGrid" />
                                                                                    <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                                                </asp:GridView>
                                                                            </div>
                                                                            <div class="paginationnew1 printorder" runat="server" id="divnopage5">
                                                                                <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage1" style="width: 100%; border-collapse: collapse;">
                                                                                    <tr>
                                                                                        <td>
                                                                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>




                                                        <%-- </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnSearch" />
                                                                         <asp:PostBackTrigger ControlID="GridView1" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>--%>
                                                    </ContentTemplate>
                                                </cc1:TabPanel>

                                                <cc1:TabPanel ID="TabAllocated" runat="server" HeaderText="Allocated">
                                                    <ContentTemplate>
                                                        <div align="right">
                                                            <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>


                                                            <asp:LinkButton ID="lblexport7" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                                CausesValidation="false" OnClick="lbtnExport2_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>
                                                        </div>
                                                        <%--  <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                                                                    <ContentTemplate>--%>
                                                        <div class="messesgarea">
                                                            <div class="alert alert-info" id="Div15" runat="server" visible="false">
                                                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                            </div>
                                                        </div>
                                                        <%-- <div class="messesgarea">
                                                                            <asp:Panel ID="PanNoRecord3" runat="server" CssClass="failure" Visible="false">
                                                                                <i class="icon-remove-sign"></i>
                                                                                <asp:Label ID="lblNoRecord3" runat="server" Text="There are no items to show in this view."></asp:Label>
                                                                            </asp:Panel>
                                                                        </div>--%>

                                                        <div class="searchfinal">
                                                            <div class="widget-body shadownone brdrgray">
                                                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                    <div class="dataTables_filter">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <asp:Panel ID="Panel3" runat="server" DefaultButton="btnSearch3">
                                                                                    <div class="dataTables_filter  Responsive-search">
                                                                                        <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <div class="inlineblock martop5">
                                                                                                        <div>
                                                                                                            <div class="input-group" style="width: 115px;">
                                                                                                                <asp:TextBox ID="txtinvoiceno3" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender13" runat="server" TargetControlID="txtinvoiceno3"
                                                                                                                    WatermarkText="InvoiceNumber" />
                                                                                                                <%--    <cc1:AutoCompleteExtender ID="AutoCompleteExtender10" MinimumPrefixLength="2" runat="server"
                                                                                                        UseContextKey="true" TargetControlID="txtinvoiceno" ServicePath="~/Search.asmx"
                                                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20" />--%>
                                                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationGroup="search3"
                                                                                                                    ControlToValidate="txtinvoiceno3" Display="Dynamic" ErrorMessage="Please enter a number"
                                                                                                                    ValidationExpression="^\d+$" Style="color: red;"></asp:RegularExpressionValidator>
                                                                                                            </div>
                                                                                                            <div class="input-group col-sm-1" id="div3" runat="server">
                                                                                                                <asp:DropDownList ID="ddllocationsearch3" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                                                                    <asp:ListItem Value="">Location</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>

                                                                                                            <div class="input-group" id="div2" runat="server" style="width: 114px">
                                                                                                                <asp:DropDownList ID="ddlvendor3" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                                                                    <asp:ListItem Value="0">Customer Name</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>

                                                                                                            <div class="input-group" id="div5" runat="server">
                                                                                                                <asp:DropDownList ID="ddlSearchDate3" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                    <asp:ListItem Value="1">ExpectedDate</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2" Selected="True">StockDeductDate</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>
                                                                                                            <div class="input-group date datetimepicker1 col-sm-1" style="width: 150px;">
                                                                                                                <span class="input-group-addon">
                                                                                                                    <span class="fa fa-calendar"></span>
                                                                                                                </span>
                                                                                                                <asp:TextBox ID="txtStartDate3" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="dynamic" CssClass="errormessage"
                                                                                                                    ControlToValidate="txtStartDate3" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                                                                            </div>
                                                                                                            <div class="input-group " style="width: 150px;">
                                                                                                                <div class="input-group date datetimepicker1 col-sm-1" style="width: 150px;">
                                                                                                                    <span class="input-group-addon">
                                                                                                                        <span class="fa fa-calendar"></span>
                                                                                                                    </span>
                                                                                                                    <asp:TextBox ID="txtEndDate3" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                                                                </div>
                                                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="dynamic" CssClass="errormessage"
                                                                                                                    ControlToValidate="txtEndDate3" ErrorMessage="* Required"></asp:RequiredFieldValidator>

                                                                                                                <asp:CompareValidator ID="CompareValidator2" Type="Date" runat="server" ErrorMessage="Invalid End Date"
                                                                                                                    ControlToCompare="txtStartDate3" ControlToValidate="txtEndDate3" Operator="GreaterThanEqual"
                                                                                                                    Display="Dynamic" ValidationGroup="search3" Style="color: red;"></asp:CompareValidator>
                                                                                                            </div>
                                                                                                            <div class="form-group spical multiselect" style="width: 142px; display: none">
                                                                                                                <dl class="dropdown">
                                                                                                                    <dt>
                                                                                                                        <a href="#">
                                                                                                                            <span class="hida" id="spanselect">Select</span>
                                                                                                                            <p class="multiSel"></p>
                                                                                                                        </a>
                                                                                                                    </dt>
                                                                                                                    <dd id="ddproject" runat="server" visible="false">
                                                                                                                        <div class="mutliSelect" id="mutliSelect">
                                                                                                                            <ul>
                                                                                                                                <asp:Repeater ID="lstSearchStatus" runat="server">
                                                                                                                                    <ItemTemplate>
                                                                                                                                        <li>
                                                                                                                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ProjectStatusID") %>' />
                                                                                                                                            <%--  <span class="checkbox-info checkbox">--%>
                                                                                                                                            <asp:CheckBox ID="chkselect" runat="server" />
                                                                                                                                            <label for="<%# Container.FindControl("chkselect").ClientID %>">
                                                                                                                                                <span></span>
                                                                                                                                            </label>
                                                                                                                                            <%-- </span>--%>
                                                                                                                                            <label class="chkval">
                                                                                                                                                <asp:Literal runat="server" ID="ltprojstatus" Text='<%# Eval("ProjectStatus")%>'></asp:Literal>
                                                                                                                                            </label>
                                                                                                                                        </li>
                                                                                                                                    </ItemTemplate>
                                                                                                                                </asp:Repeater>
                                                                                                                            </ul>
                                                                                                                        </div>
                                                                                                                    </dd>
                                                                                                                </dl>
                                                                                                            </div>
                                                                                                            <div class="input-group">
                                                                                                                <asp:LinkButton ID="btnSearch3" runat="server" Text="Search"
                                                                                                                    ValidationGroup="search3" CssClass="btn btn-info btnsearchicon"
                                                                                                                    CausesValidation="true" OnClick="btnSearch3_Click"></asp:LinkButton>
                                                                                                                <%--<asp:LinkButton ID="btnSearch3" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch3_Click" />--%>
                                                                                                            </div>
                                                                                                            <div class="input-group col-sm-0">
                                                                                                                <asp:LinkButton runat="server" ID="btnClear3"
                                                                                                                    CausesValidation="false" OnClick="btnClear3_Click1" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </asp:Panel>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="finalgrid">
                                                            <div class="table-responsive printArea">
                                                                <div>
                                                                    <div id="Div6" runat="server">
                                                                        <div class="table-responsive xscroll" id="PanGrid3" runat="server">
                                                                            <asp:GridView ID="GridView3" DataKeyNames="WholesaleOrderID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                                                                OnSorting="GridView3_Sorting" OnPageIndexChanging="GridView3_PageIndexChanging" OnRowDataBound="GridView3_RowDataBound" OnRowCommand="GridView3_RowCommand"
                                                                                OnDataBound="GridView3_DataBound" AllowSorting="true" OnRowCreated="GridView3_RowCreated" AutoGenerateColumns="false" AllowPaging="true">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="DeductDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Left" SortExpression="StockDeductDate">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label21" runat="server" Width="100px">
                                                                    <%#Eval("StockDeductDate","{0: dd MMM yyyy}")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Invoice No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Left" SortExpression="OrderNumber" HeaderStyle-CssClass="brdrgrayleft">
                                                                                        <ItemTemplate>
                                                                                            <asp:HiddenField ID="hndWholesaleID" runat="server" Value='<%#Eval("WholesaleOrderID")%>' />
                                                                                            <%--<asp:Label ID="Label12" runat="server" Width="100px">
                                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>--%>
                                                                                            <asp:Label ID="Label11" runat="server" Width="100px">
                                                                                        <%#Eval("InvoiceNo")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Left" SortExpression="Vendor">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label14" runat="server" Width="120px">
                                                                                             <%#Eval("Vendor")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="ExpectedDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Left" SortExpression="ExpectedDelivery">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label13" runat="server" Width="100px">
                                                                        <%#Eval("ExpectedDelivery","{0:dd MMM yyyy}")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="N.Panels" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                        <ItemTemplate>
                                                                                            <asp:Literal runat="server" ID="ltallocatedpanels"></asp:Literal>
                                                                                            <%# Eval("Npanel")%>
                                                                                            <asp:HiddenField runat="server" ID="hdnStockItem" Value='<%#Eval("StockItemID")%>' />
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Deducted" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                        <ItemTemplate>
                                                                                            <asp:Literal runat="server" ID="ltdeductpanels"></asp:Literal>
                                                                                            <%# Eval("Npanel")%>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Revert" ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Center" ControlStyle-CssClass="printpage">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lblrevert" runat="server" Width="30px">
                                                                                                <asp:Button ID="btnRevertStock" runat="server" Text="Revert" CssClass="btnrevert" data-toggle="tooltip" data-placement="top" title="Revert" CommandName="RevertStock" CommandArgument='<%#Eval("WholesaleOrderID")%>' OnClientClick="return confirm('Are you sure you want to Revert this Record?');" Visible="false" />
                                                                                            </asp:Label>
                                                                                            <asp:LinkButton ID="lbtnDeduct" CommandName="deduct" CommandArgument='<%#Eval("WholesaleOrderID")%>' CausesValidation="false" runat="server" Visible="false">
                                                                                                <%--<asp:LinkButton ID="LinkButton7" CommandName="deduct" CommandArgument='<%#Eval("WholesaleOrderID")%>' CausesValidation="false" runat="server" Visible='<%#Eval("ProjectStatusID").ToString()=="3" ||Eval("ProjectStatusID").ToString()=="6" ||Eval("ProjectStatusID").ToString()=="11"?true:false%>'>--%>
                                                                                                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/revert.png" />
                                                                                            </asp:LinkButton>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Remaining" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label runat="server" ID="ltremainingpanels"></asp:Label>
                                                                                            <%--<%#Eval("stock")%>--%>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="500px" SortExpression="WholesaleOrderItem">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lbldetails" runat="server" data-placement="top" data-original-title='<%#Eval("WholesaleOrderItem")%>' data-toggle="tooltip"
                                                                                                Width="260px"><%#Eval("WholesaleOrderItem")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="StoreName" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                                        ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label4" runat="server" Width="80px">
                                                                                                 <%#Eval("CompanyLocation")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <PagerTemplate>
                                                                                    <asp:Label ID="ltrPage3" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                                    <div class="pagination">
                                                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                                                    </div>
                                                                                </PagerTemplate>
                                                                                <PagerStyle CssClass="paginationGrid" />
                                                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <div class="paginationnew1 printorder" runat="server" id="divnopage3">
                                                                            <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage2" style="width: 100%; border-collapse: collapse;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="ltrPage3" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%--</ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnSearch3" />
                                                                         <asp:PostBackTrigger ControlID="GridView3" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>--%>
                                                    </ContentTemplate>
                                                </cc1:TabPanel>

                                                <cc1:TabPanel ID="TabRevert" runat="server" HeaderText="Revert">
                                                    <%-- <ContentTemplate>
                                                         <asp:UpdatePanel runat="server" ID="updaterevert">--%>
                                                    <ContentTemplate>
                                                        <div align="right">

                                                            <a href="javascript:window.print();" class="btn btn-primary btn-xs Print"><i class="fa fa-print"></i>Print</a>


                                                            <asp:LinkButton ID="LinkButton5" runat="server" data-toggle="tooltip" data-placement="left" title="" data-original-title="Excel Export" class="btn btn-success btn-xs Excel"
                                                                CausesValidation="false" OnClick="lbtnExport3_Click"><i class="fa fa-file-excel-o"></i> Excel </asp:LinkButton>


                                                        </div>
                                                        <div class="messesgarea">
                                                            <div class="alert alert-info" id="PanNoRecord4" runat="server" visible="false">
                                                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                            </div>
                                                        </div>
                                                        <div class="searchfinal">
                                                            <div class="widget-body shadownone brdrgray">
                                                                <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                                                    <div class="dataTables_filter">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <asp:Panel ID="Panel2" runat="server" DefaultButton="btnSearch4">
                                                                                    <div class="dataTables_filter Responsive-search ">
                                                                                        <table border="0" cellspacing="0" width="100%" style="text-align: left; margin-bottom: 10px;" cellpadding="0">
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <div class="inlineblock">
                                                                                                        <div>
                                                                                                            <div class="input-group" style="width: 115px;">
                                                                                                                <asp:TextBox ID="txtinvoiceno4" runat="server" CssClass="form-control m-b"></asp:TextBox>
                                                                                                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtinvoiceno4"
                                                                                                                    WatermarkText="InvoiceNumber" />
                                                                                                                <%--<cc1:AutoCompleteExtender ID="AutoCompleteExtender10" MinimumPrefixLength="2" runat="server"
                                                                                                        UseContextKey="true" TargetControlID="txtinvoiceno" ServicePath="~/Search.asmx"
                                                                                                        CompletionListCssClass="autocomplete_completionListElement" ServiceMethod="GetProjectNumber"
                                                                                                        EnableCaching="false" CompletionInterval="10" CompletionSetCount="20"/>--%>
                                                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="search4"
                                                                                                                    ControlToValidate="txtinvoiceno4" Display="Dynamic" ErrorMessage="Please enter numberic value" Style="color: red"
                                                                                                                    ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                                                                            </div>

                                                                                                            <div class="input-group col-sm-1" id="div1" runat="server">
                                                                                                                <asp:DropDownList ID="ddllocationsearch4" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval" Width="150px">
                                                                                                                    <asp:ListItem Value="">Location</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>

                                                                                                            <div class="input-group" id="div4" runat="server" style="width: 114px">
                                                                                                                <asp:DropDownList ID="ddlvendor4" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                                                                    <asp:ListItem Value="0">Customer Name</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>

                                                                                                            <div class="input-group" id="div7" runat="server" style="width: 120px">
                                                                                                                <asp:DropDownList ID="ddlSearchDate4" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                    <asp:ListItem Value="3">RevertDate</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>
                                                                                                            <div class="input-group date datetimepicker1 col-sm-1" style="width: 150px;">
                                                                                                                <span class="input-group-addon">
                                                                                                                    <span class="fa fa-calendar"></span>
                                                                                                                </span>
                                                                                                                <asp:TextBox ID="txtStartDate4" placeholder="Start Date" runat="server" class="form-control"></asp:TextBox>
                                                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="dynamic" CssClass="errormessage"
                                                                                                                    ControlToValidate="txtStartDate4" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                                                                            </div>
                                                                                                            <div class="input-group" id="div35" runat="server" style="width: 150px">
                                                                                                                <div class="input-group date datetimepicker1" style="width: 150px;">
                                                                                                                    <span class="input-group-addon">
                                                                                                                        <span class="fa fa-calendar"></span>
                                                                                                                    </span>
                                                                                                                    <asp:TextBox ID="txtEndDate4" placeholder="End Date" runat="server" class="form-control"></asp:TextBox>
                                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic" CssClass="errormessage"
                                                                                                                        ControlToValidate="txtEndDate4" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                                                                                                                </div>
                                                                                                                <asp:CompareValidator ID="CompareValidator1" Type="Date" runat="server" ErrorMessage="Invalid End Date" Style="color: red"
                                                                                                                    ControlToCompare="txtStartDate4" ControlToValidate="txtEndDate4" Operator="GreaterThanEqual"
                                                                                                                    Display="Dynamic" ValidationGroup="search4"></asp:CompareValidator>
                                                                                                            </div>
                                                                                                            <div class="input-group col-sm-1" id="div9" runat="server">
                                                                                                                <asp:DropDownList ID="ddlRevert" runat="server" AppendDataBoundItems="true"
                                                                                                                    aria-controls="DataTables_Table_0" CssClass="myval">
                                                                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                                                                    <asp:ListItem Value="1">Revert</asp:ListItem>
                                                                                                                    <asp:ListItem Value="2">Revert All</asp:ListItem>
                                                                                                                </asp:DropDownList>
                                                                                                            </div>
                                                                                                            <div class="input-group ">
                                                                                                                <asp:Button ID="btnSearch4" runat="server" Text="Search" ValidationGroup="search4" CssClass="btn btn-info btnsearchicon"
                                                                                                                    CausesValidation="true" OnClick="btnSearch4_Click"></asp:Button>
                                                                                                                <%--  <asp:Button ID="btnSearch4" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch4_Click" />--%>
                                                                                                            </div>
                                                                                                            <div class="input-group ">
                                                                                                                <asp:LinkButton runat="server" ID="btnClear4"
                                                                                                                    CausesValidation="false" OnClick="btnClearAll4_Click" CssClass="btn btn-primary"><i class="fa-refresh fa"></i>Clear </asp:LinkButton>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                    <%--   <div class="inlineblock" style="margin-top: 3px;">
                                                                                                        <div>

                                                                                                            <div class="datashowbox">
                                                                                                                <div class="leftarea1">
                                                                                                                    <div class="showenteries showdata">
                                                                                                                        <table border="0" cellspacing="0" cellpadding="0">
                                                                                                                            <tr>
                                                                                                                                <td>
                                                                                                                                    <asp:DropDownList ID="ddlSelectRecords4" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords4_SelectedIndexChanged"
                                                                                                                                        aria-controls="DataTables_Table_0" class="myval">
                                                                                                                                        <asp:ListItem Value="">Show entries</asp:ListItem>
                                                                                                                                    </asp:DropDownList></td>
                                                                                                                            </tr>
                                                                                                                        </table>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>--%>

                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                </asp:Panel>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <div class="finalgrid">
                                                                <div class="table-responsive printArea">
                                                                    <div id="Div10" runat="server">
                                                                        <div class="table-responsive xscroll" id="PanGrid4" runat="server">
                                                                            <asp:GridView ID="GridView2" DataKeyNames="WholesaleOrderID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover printarea"
                                                                                OnSorting="GridView2_Sorting" OnPageIndexChanging="GridView2_PageIndexChanging"
                                                                                OnDataBound="GridView2_DataBound" AllowSorting="true" OnRowCreated="GridView2_RowCreated" AutoGenerateColumns="false" AllowPaging="true">
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Invoice No." ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Left" SortExpression="OrderNumber" HeaderStyle-CssClass="brdrgrayleft">
                                                                                        <ItemTemplate>
                                                                                            <asp:HiddenField ID="hndWholesaleID" runat="server" Value='<%#Eval("WholesaleOrderID")%>' />
                                                                                            <%--<asp:Label ID="Label12" runat="server" Width="100px">
                                                                                        <asp:HyperLink ID="hypDetail" runat="server" data-toggle="tooltip" data-placement="top" data-original-title="Detail" Target="_blank"
                                                                                            NavigateUrl='<%# "~/admin/adminfiles/company/company.aspx?m=pro&compid="+Eval("CustomerID")+"&proid="+Eval("ProjectID") %>'><%#Eval("ProjectNumber")%></asp:HyperLink></asp:Label>--%>
                                                                                            <asp:Label ID="Label11" runat="server" Width="100px">
                                                                                        <%#Eval("InvoiceNo")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Left" SortExpression="Vendor">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label14" runat="server" Width="120px">
                                                                                             <%#Eval("Vendor")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="ExpectedDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Left" SortExpression="ExpectedDelivery">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label13" runat="server" Width="100px">
                                                                        <%#Eval("ExpectedDelivery","{0:dd MMM yyyy}")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="500px" SortExpression="WholesaleOrderItem">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="lbldetails" runat="server" data-placement="top" data-original-title='<%#Eval("WholesaleOrderItem")%>' data-toggle="tooltip"
                                                                                                Width="260px"><%#Eval("WholesaleOrderItem")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="StoreName" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                                                        ItemStyle-HorizontalAlign="Left" SortExpression="CompanyLocation">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label4" runat="server" Width="80px">
                                                                                                 <%#Eval("CompanyLocation")%></asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Revert Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Center" SortExpression="RevertDate">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label19" runat="server" Width="100px">
                                                                                         <%#Eval("RevertDate","{0:dd MMM yyyy}")%>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Revert Panel" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                                                        ItemStyle-HorizontalAlign="Center" SortExpression="Stock">
                                                                                        <ItemTemplate>
                                                                                            <asp:Label ID="Label119" runat="server" Width="100px">
                                                                                         <%#Eval("Stock")%>
                                                                                            </asp:Label>
                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                    <asp:TemplateField HeaderText="Details" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="printorder"
                                                                                        ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px">
                                                                                        <ItemTemplate>
                                                                                            <asp:HiddenField Value='<%#Eval("WholesaleOrderID") %>' runat="server" ID="hdnWholesaleOrderID" />
                                                                                            <asp:LinkButton CssClass="printorder" ID="lnkReceived" CausesValidation="false" runat="server" OnClick="lnkReceived_Click" data-toggle="tooltip" data-placement="top" data-original-title="Detail">
                                                                                              
                                                                                                <a target="_blank" href="#" data-original-title="Detail" data-placement="top" data-toggle="tooltip" class="btn btn-primary btn-xs"> <i class="fa fa-link"></i> Detail</a>
                                                                                            </asp:LinkButton>

                                                                                        </ItemTemplate>
                                                                                    </asp:TemplateField>
                                                                                </Columns>
                                                                                <PagerTemplate>
                                                                                    <asp:Label ID="ltrPage2" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                                    <div class="pagination">
                                                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                                                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                                        <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                                                        <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                                                    </div>
                                                                                </PagerTemplate>
                                                                                <PagerStyle CssClass="paginationGrid" />
                                                                                <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                                                            </asp:GridView>
                                                                        </div>
                                                                        <div class="paginationnew1 printorder" runat="server" id="divnopage2" visible="true">
                                                                            <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage3" style="width: 100%; border-collapse: collapse;">
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label ID="ltrPage2" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <%--     </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:PostBackTrigger ControlID="btnSearch4" />
                                                                      <%--  <asp:PostBackTrigger ControlID="GridView2" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>--%>
                                                    </ContentTemplate>
                                                </cc1:TabPanel>
                                                <%--Dummy data--%>


                                                <%--Dummy data--%>
                                            </cc1:TabContainer>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

    <cc1:ModalPopupExtender ID="ModalPopupExtenderDeduct" runat="server" BackgroundCssClass="modalbackground"
        CancelControlID="ibtnCancel" DropShadow="false" PopupControlID="divStockItems"
        OkControlID="btnOK" TargetControlID="btnNULL">
    </cc1:ModalPopupExtender>
    <div runat="server" id="divStockItems" style="display: none;" class="modal_popup">
        <div class="modal-dialog" style="width: 1150px;">
            <%--modal-dialog--%>
            <div class="modal-content">
                <div class="color-line"></div>
                <div class="modal-header">
                    <div style="float: right">
                        <button id="ibtnCancel" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" causesvalidation="false">
                            Close</button>
                    </div>
                    <h4 class="modal-title" id="H2">Project Wholesale Stock Allocation</h4>
                </div>
                <div class="modal-body paddnone deductbox" style="overflow-y: scroll; height: 500px;">
                    <div class="panel-body">
                        <div class="formainline">
                            <div class="row">
                                <div class="col-md-6">
                                    <div style="border: 1px solid #ccc; min-height: 121px">
                                        <table cellpadding="5" cellspacing="5" border="0" class="table">
                                            <tr>
                                                <td width="150px"><b>Customer</b></td>
                                                <td>
                                                    <asp:Label ID="lblCustomer" runat="server"></asp:Label></td>
                                            </tr>
                                            <%--  <tr>
                                                <td width="150px"><b>Project</b></td>
                                                <td>
                                                    <asp:Label ID="lblProject" runat="server" class="control-label"></asp:Label></td>
                                            </tr>--%>
                                            <tr>
                                                <td width="150px"><b>System Details</b></td>
                                                <td>
                                                    <asp:Label ID="lblSystemDetails" runat="server"></asp:Label></td>
                                            </tr>
                                            <%--  <tr>
                                                <td width="150px"><b>Inverter Details</b></td>
                                                <td>
                                                    <asp:Label ID="lblInverterDetails" runat="server"></asp:Label></td>
                                            </tr>--%>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div>
                                        <table cellpadding="5" cellspacing="5" style="border: 1px solid #ccc" class="table">
                                            <tr>
                                                <td width="150px"><b>Install City</b></td>
                                                <td>
                                                    <asp:Label ID="lblInstallCity" runat="server"></asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td width="150px"><b>Install State</b></td>
                                                <td>
                                                    <asp:Label ID="lblInstallState" runat="server"></asp:Label></td>
                                            </tr>
                                            <%-- <tr>
                                                <td width="150px"><b>Roof Type</b></td>
                                                <td>
                                                    <asp:Label ID="lblRoofType" runat="server"></asp:Label></td>
                                            </tr>--%>
                                            <tr>
                                                <td width="150px"><b>Stock From</b></td>
                                                <td>
                                                    <asp:DropDownList ID="ddlStore" runat="server" AppendDataBoundItems="true" AutoPostBack="true"
                                                        aria-controls="DataTables_Table_0" class="myval" OnSelectedIndexChanged="ddlStore_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <br />
                            <div>
                                <div class="form-group">
                                    <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover">
                                        <tr>
                                            <td width="50%"><b>Stock Item</b></td>
                                            <td width="20%"><b>Stock</b></td>
                                            <td width="15%"><b>Qty</b></td>
                                            <td width="15%"><b>Extra Stock</b></td>
                                        </tr>
                                        <asp:Repeater ID="Repeater2" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:HiddenField runat="server" ID="hdnWholesaleOrderItemID" Value='<%#Eval("WholesaleOrderItemID") %>' />
                                                        <asp:HiddenField runat="server" ID="hdnStockCategoryID" Value='<%#Eval("StockCategoryID") %>' />
                                                        <asp:HiddenField runat="server" ID="hdnStockItemID" Value='<%#Eval("StockItemID") %>' />
                                                        <asp:Label ID="rlblStockItem" runat="server" Text='<%#Eval("WholesaleOrderItem") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="rlblStock" runat="server" Text='<%#Eval("IteminStock") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td style="padding-right: 5px;">
                                                                    <asp:TextBox ID="rtxtQty" runat="server" Width="60px" MaxLength="4" CssClass="form-control" Text='<%#Eval("OrderQuantity") %>' ReadOnly="true" OnTextChanged="rtxtQty_TextChanged" AutoPostBack="true">
                                                                    </asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="rtxtQty" FilterType="Numbers" />
                                                                </td>
                                                                <td>                                                                   
                                                                    <asp:Label runat="server" ID="lblerrmsg" style="color:red;"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="rtxt_ExtraStock" runat="server" Width="80px" MaxLength="20" CssClass="form-control"></asp:TextBox>
                                                        <%-- <asp:DropDownList ID="ddlExtraStock" runat="server"  CssClass="myval" AppendDataBoundItems="true">
                                                </asp:DropDownList>--%>
                                                    </td>
                                                </tr>

                                            </ItemTemplate>
                                        </asp:Repeater>


                                        <%-- <div class="clear"></div>
                            <br />
                            <div>
                                <div class="form-group">--%>


                                        <asp:Repeater ID="rptDeduct" runat="server" OnItemDataBound="rptDeduct_ItemDataBound">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <asp:HiddenField ID="hndStockItemID" runat="server" Value='<%# Eval("StockItemID") %>' />
                                                        <asp:DropDownList ID="rddlStockItem" runat="server" AppendDataBoundItems="true"
                                                            CssClass="myval" AutoPostBack="true" OnSelectedIndexChanged="ddlStockItem_SelectedIndexChanged">
                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="rlblQty" runat="server" Text='<%# Eval("StockQuantity") %>'></asp:Label>
                                                    </td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="rtxtExtraStock" MaxLength="4" Width="60px" CssClass="form-control" runat="server" Text='<%# Eval("ExtraStock") %>'></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="rtxtExtraStock" ValidationGroup="abcabc"
                                                            Display="Dynamic" ErrorMessage="Please enter a number" ValidationExpression="^[-,0-9 ]+$"></asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <tr>
                                            <td colspan="4" align="right">
                                                <asp:Button ID="btnAddRow" runat="server" Text="Add" OnClick="btnAddRow_Click" CssClass="btn btn-primary addwhiteicon btnaddicon"
                                                    CausesValidation="false" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--<div class="clear"></div>
                    <br />--%>
                    <div class="panel-body" style="padding-bottom: 0; padding-top: 0;">
                        <div class="col-md-7" style="padding: 0">
                            <div class="form-group" id="panelsearchdiv">
                                <div>
                                    <div class="col-md-8" style="padding: 0">
                                        <asp:TextBox ID="txtpallet" runat="server" CssClass="form-control m-b" data-role="tagsinput"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="" CssClass="reqerror"
                                            ControlToValidate="txtpallet" Display="Dynamic" ValidationGroup="success12"></asp:RequiredFieldValidator>
                                        <%--  <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="txtpallet"
                                            WatermarkText="Pallet Number" />--%>
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Button ID="lnkSearchPallet" runat="server" Text="Search" CssClass="btn btn-info btnsearchicon"
                                            OnClick="btnSearchPallet_Click" ValidationGroup="success12"></asp:Button>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5" style="padding: 0">
                            <div class="form-group" id="panelsearchdiv2">
                                <div>
                                    <div class="col-md-8" style="padding: 0">
                                        <asp:TextBox ID="txtInvoiceNo2" runat="server" CssClass="form-control m-b" ReadOnly="true"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="" CssClass="reqerror"
                                            ControlToValidate="txtInvoiceNo2" Display="Dynamic" ValidationGroup="success13"></asp:RequiredFieldValidator>
                                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender5" runat="server" TargetControlID="txtInvoiceNo2"
                                            WatermarkText="Invoice Number" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:Button ID="btnSearchInvoiceNo2" runat="server" Text="Search" CssClass="btn btn-info btnsearchicon"
                                            ValidationGroup="success13" OnClick="btnSearchInvoiceNo2_Click"></asp:Button>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--<div class="clear"></div>
                    <br />--%>
                    <asp:Panel runat="server" ID="SerialNoList" Visible="false" class="panel-body">
                        <div class="col-md-12" style="padding: 0;">
                            <div class="form-group" style="margin-bottom: 0;">
                                <asp:Label ID="lblhdnpalletno" runat="server"></asp:Label>
                                <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover">
                                    <tr>
                                        <td width="10%" style="text-align: center;"><b>Sr No.</b></td>
                                        <td width="40%" style="text-align: center;"><b>Pallet Number</b></td>
                                        <td width="40%" style="text-align: center;"><b>Serial No.</b></td>
                                        <td width="10%" style="text-align: center;"><b></b></td>
                                    </tr>
                                    <asp:Repeater ID="Repeater3" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center;">

                                                    <asp:Label ID="lblSrNo" runat="server" Text='<%# Container.ItemIndex + 1 %>'></asp:Label>
                                                </td>
                                                <td style="text-align: center;">

                                                    <asp:Label ID="lblPallet" runat="server" Text='<%# Eval("Pallet") %>'></asp:Label>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:Label ID="lblSerialNo" runat="server" Text='<%# Eval("SerialNo") %>'></asp:Label>
                                                </td>
                                                <td style="text-align: center;">
                                                    <%--<asp:Button ID="litremove" runat="server"  Text="Remove" CssClass="btn btn-danger btncancelicon"
                                                                        CausesValidation="false" />--%>
                                                    <label for='<%# Container.FindControl("chkSerialNo").ClientID  %>' runat="server" id="lblchk123">
                                                        <asp:CheckBox ID="chkSerialNo" runat="server" />
                                                        <span class="text">&nbsp;</span>
                                                    </label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <%-- <div class="clear"></div>
                            <br />
                            <div>
                                <div class="form-group">--%>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>

                    <asp:Panel runat="server" ID="SerialNoList2" Visible="false" class="panel-body">
                        <div class="col-md-12" style="padding: 0;">
                            <div class="form-group" style="margin-bottom: 0;">
                                <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover">
                                    <tr>
                                        <td width="10%" style="text-align: center;"><b>Sr No.</b></td>
                                        <td width="80%" style="text-align: center;"><b>Serial No.</b></td>
                                        <td width="10%" style="text-align: center;"><b></b></td>
                                    </tr>
                                    <asp:Repeater ID="Repeater4" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center;">

                                                    <asp:Label ID="lblSrNo2" runat="server" Text='<%# Container.ItemIndex + 1 %>'></asp:Label>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:Label ID="lblSerialNo2" runat="server" Text='<%# Eval("SerialNo") %>'></asp:Label>
                                                </td>
                                                <td style="text-align: center;">
                                                    <%--<asp:Button ID="litremove" runat="server"  Text="Remove" CssClass="btn btn-danger btncancelicon"
                                                                        CausesValidation="false" />--%>
                                                    <label for='<%# Container.FindControl("chkSerialNo2").ClientID  %>' runat="server" id="lblchk1234">
                                                        <asp:CheckBox ID="chkSerialNo2" runat="server" />
                                                        <span class="text">&nbsp;</span>
                                                    </label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <%-- <div class="clear"></div>
                            <br />
                            <div>
                                <div class="form-group">--%>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>

                    <div class="col-md-12">
                        <div class="form-group" style="text-align: center">
                            <div class="alert alert-danger" id="divActive" runat="server" visible="false">
                                <i class="icon-remove-sign"></i><strong>&nbsp;No Records found.</strong>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group" style="text-align: center">
                            <span>
                                <button id="ibtnAdd" type="submit" runat="server" onserverclick="ibtnAdd_Click"
                                    validationgroup="abc" class="btn btn-primary savewhiteicon btnsaveicon">
                                    Save</button>
                                <asp:Button ID="litremove" runat="server" Text="Remove" CssClass="btn btn-danger btncancelicon"
                                    CausesValidation="false" OnClick="litremove_Click" Visible="false" />
                                <asp:Button ID="litremove2" runat="server" Text="Remove" CssClass="btn btn-danger btncancelicon"
                                    CausesValidation="false" OnClick="litremove2_Click" Visible="false" />
                                <asp:Button ID="btnOK" Style="display: none; visible: false;" runat="server"
                                    CssClass="btn" Text=" OK " />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:Button ID="btnNULL" Style="display: none;" runat="server" />

    <cc1:ModalPopupExtender ID="ModalPopupExtenderDR" runat="server" BackgroundCssClass="modalbackground"
        CancelControlID="ibtnCancel2" DropShadow="false" PopupControlID="divStockItems2"
        OkControlID="btnOK2" TargetControlID="btnNULL2">
    </cc1:ModalPopupExtender>
    <div runat="server" id="divStockItems2" style="display: none;">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div style="float: right">
                        <button id="ibtnCancel2" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">
                            Close</button>
                    </div>
                    <h4 class="modal-title" id="myModalLabel">Wholesale Stock Allocation</h4>
                </div>
                <div class="modal-body paddnone" style="overflow-y: scroll; height: 350px;">
                    <div class="panel-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <table cellpadding="5" cellspacing="5" border="0" class="table">
                                    <tr>
                                        <td width="150px"><b>Customer</b></td>
                                        <td>
                                            <asp:Label ID="lblCustomer2" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>System Details</b></td>
                                        <td>
                                            <asp:Label ID="lblSystemDetails2" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Install City</b></td>
                                        <td>
                                            <asp:Label ID="lblInstallCity2" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Install State</b></td>
                                        <td>
                                            <asp:Label ID="lblInstallState2" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px" colspan="2">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <br />
                        <div class="col-md-12">
                            <div class="form-group">
                                <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover">
                                    <tr>
                                        <td width="50%"><b>Stock Item</b></td>
                                        <td width="20%"><b>Deduct</b></td>
                                        <td width="30%"><b>Qty</b></td>
                                    </tr>
                                    <asp:Repeater ID="rptDeductRev" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td width="50%">
                                                    <asp:HiddenField ID="hndStockItemID" runat="server" Value='<%#Eval("StockItemID") %>' />
                                                    <%#Eval("StockItem") %> </td>
                                                <td width="20%">
                                                    <asp:Label ID="lblStockDeduct" runat="server"><%#Eval("Deduct","{0:0}") %></asp:Label>
                                                </td>
                                                <td width="30%">
                                                    <asp:TextBox ID="txtDeductRevert" runat="server" MaxLength="4" Width="50px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                                                        ControlToValidate="txtDeductRevert" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                    <%--<asp:CompareValidator ID="CompareValidatorDR" runat="server" ControlToValidate="txtDeductRevert" ValueToCompare='<%#Eval("Deduct","{0:0}") %>'
                                                                Type="Integer" Display="Dynamic" ErrorMessage="Enter valid stock" Operator="LessThanEqual"></asp:CompareValidator>--%>

                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <br />
                        <div class="col-md-12" align="center">
                            <div class="form-group">
                                <asp:Button ID="btnDeduct" runat="server" Text="Deduct" OnClick="btnDeduct_Click" CssClass="btn btn-primary savewhiteicon"
                                    CausesValidation="false" />
                                <asp:Button ID="btnRevert" runat="server" Text="Revert" OnClick="btnRevert_Click" CssClass="btn btn-purple resetbutton"
                                    CausesValidation="false" />
                                <asp:Button ID="btnRevertAll" runat="server" Text="Revert All" OnClick="btnRevertAll_Click" class="btn btn-purple resetbutton"
                                    CausesValidation="false" />
                                <asp:Button ID="btnOK2" Style="display: none;" runat="server"
                                    CssClass="btn" Text=" OK" Visible="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="btnNULL2" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtenderMove" runat="server" BackgroundCssClass="modalbackground"
        CancelControlID="ibtnCancelMove" DropShadow="false" PopupControlID="divMoveProject"
        OkControlID="btnOKMove" TargetControlID="btnNULLMove">
    </cc1:ModalPopupExtender>
    <div runat="server" id="divMoveProject" style="display: none; padding: 10px; width: 100%;"
        align="center">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div style="float: right">
                        <button id="ibtnCancelMove" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">
                            Close</button>
                    </div>
                    <h4 class="modal-title" id="H1">Project Move</h4>
                </div>
                <div class="modal-body paddnone">
                    <div class="panel-body">
                        <div class="formainline">
                            <div class="form-group spical">
                                <label>Project</label>
                                <asp:DropDownList ID="ddlProjectMove" runat="server" AppendDataBoundItems="true"
                                    aria-controls="DataTables_Table_0" class="myval" Width="150px">
                                    <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required." CssClass="reqerror" ValidationGroup="move"
                                    ControlToValidate="ddlProjectMove" Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                            <div class="form-group">
                                <label>Install</label>
                                <asp:DropDownList ID="ddlInstallerMove" runat="server" AppendDataBoundItems="true"
                                    aria-controls="DataTables_Table_0" class="myval" Width="150px" Enabled="false">
                                    <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="This value is required." CssClass="reqerror" ValidationGroup="move"
                                    ControlToValidate="ddlInstallerMove" Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="Label23" runat="server" class="col-sm-2  control-label">
                                                <strong>Install Date</strong></asp:Label>
                                <div class="input-group date datetimepicker1 col-sm-2">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                    <asp:TextBox ID="txtInstallerDateMove" runat="server" class="form-control" placeholder="Start Date">
                                    </asp:TextBox>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <asp:Button ID="btnNULLMove" Style="display: none;" runat="server" />
    <asp:HiddenField ID="hndWholesaleID" runat="server" />

    <asp:Button ID="Button2" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalbackground"
        DropShadow="false" PopupControlID="divstockdetail" TargetControlID="Button2">
    </cc1:ModalPopupExtender>
    <div id="divstockdetail" runat="server" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div style="float: right">
                        <asp:LinkButton ID="Button3" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal" OnClick="Button3_Click">
                        Close
                        </asp:LinkButton>
                    </div>
                    <h4 class="modal-title" id="H3">
                        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                        Stock Revert Details</h4>
                </div>
                <div class="modal-body paddnone" runat="server" id="divdetail">
                    <div class="panel-body" style="overflow: scroll;">
                        <div class="formainline">
                            <div class="panel panel-default">
                                <div class="panel-body formareapop heghtauto" style="background: none!important;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <span class="name disblock">
                                                    <label class="control-label">
                                                    </label>
                                                </span><span>
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered quotetable">
                                                        <thead>
                                                            <tr>
                                                                <td style="width: 70%">Stock Item
                                                                </td>
                                                                <td style="width: 20%">Stock
                                                                </td>
                                                            </tr>
                                                        </thead>
                                                        <asp:Repeater ID="rptstockdetail" runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td style="width: 70%;">
                                                                        <%#Eval("StockItem")%>
                                                                    </td>
                                                                    <td style="width: 20%">
                                                                        <%#Eval("Stock")%>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </table>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-body paddnone" runat="server" id="divdetailmsg" visible="false">
                    <div class="panel-body" style="overflow: scroll;">
                        <div class="formainline">
                            <div class="panel panel-default">
                                <div class="panel-body formareapop heghtauto" style="background: none!important;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group ">
                                                <span class="name disblock">
                                                    <label class="control-label">
                                                    </label>
                                                </span><span>

                                                    <div class="messesgarea">
                                                        <div class="alert alert-info" id="Div12" runat="server">
                                                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                                        </div>
                                                    </div>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnstocktransferid" />


    <asp:Button ID="Button8" Style="display: none;" runat="server" />
    <cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
        CancelControlID="Button1" DropShadow="false" PopupControlID="div13" TargetControlID="Button8">
    </cc1:ModalPopupExtender>
    <div runat="server" id="div13" style="display: none">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <div style="float: right">
                        <button id="Button1" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">
                            Close</button>
                    </div>
                    <h4 class="modal-title" id="H4">Stock Allocation</h4>
                </div>
                <div class="modal-body paddnone">
                    <div class="panel-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <table cellpadding="5" cellspacing="5" border="0" class="table">
                                    <tr>
                                        <td width="150px"><b>Customer</b></td>
                                        <td>
                                            <asp:Label ID="lblCustomer3" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Project</b></td>
                                        <td>
                                            <asp:Label ID="lblProject3" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Panel Details</b></td>
                                        <td>
                                            <asp:Label ID="lblPanelDetails3" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Inverter Details</b></td>
                                        <td>
                                            <asp:Label ID="lblInverterDetails3" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Install City</b></td>
                                        <td>
                                            <asp:Label ID="lblInstallCity3" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Install State</b></td>
                                        <td>
                                            <asp:Label ID="lblInstallState3" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Roof Type</b></td>
                                        <td>
                                            <asp:Label ID="lblRoofType3" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px" colspan="2">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <br />
                        <div class="col-md-12">
                            <div class="form-group">
                                <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover">
                                    <tr>
                                        <td width="50%"><b>Stock Item</b></td>
                                        <td width="20%"><b>Allocate</b></td>
                                    </tr>
                                    <asp:Repeater ID="Repeater1" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td width="50%">
                                                    <asp:HiddenField ID="hndStockItemID" runat="server" Value='<%#Eval("StockItemID") %>' />
                                                    <%#Eval("StockItem") %> </td>
                                                <td width="20%">
                                                    <asp:Label ID="lblStockDeduct" runat="server"><%#Eval("Deduct","{0:0}") %></asp:Label>
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <br />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        function printContent() {
            var PageHTML = document.getElementById('<%= (PanGrid5.ClientID) %>').innerHTML;
            var html = '<html><head>' +
                '<link href="../../assets/styles/print.css" rel="stylesheet" type="text/css" media="print"/>' +
                '</head><body style="background:#ffffff;">' +
                PageHTML +
                '</body></html>';
            var WindowObject = window.open("", "PrintWindow",
                "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
            WindowObject.document.writeln(html);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
            document.getElementById('print_link').style.display = 'block';
        }
        function printContent_allocated() {
            var PageHTML = document.getElementById('<%= (PanGrid3.ClientID) %>').innerHTML;
            var html = '<html><head>' +
                '<link href="../../assets/styles/print.css" rel="stylesheet" type="text/css" media="print"/>' +
                '</head><body style="background:#ffffff;">' +
                PageHTML +
                '</body></html>';
            var WindowObject = window.open("", "PrintWindow",
                "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
            WindowObject.document.writeln(html);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
            document.getElementById('print_link').style.display = 'block';
        }
        function printContent_revert() {
            var PageHTML = document.getElementById('<%= (PanGrid4.ClientID) %>').innerHTML;
            var html = '<html><head>' +
                '<link href="../../assets/styles/print.css" rel="stylesheet" type="text/css" media="print"/>' +
                '</head><body style="background:#ffffff;">' +
                PageHTML +
                '</body></html>';
            var WindowObject = window.open("", "PrintWindow",
                "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
            WindowObject.document.writeln(html);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
            document.getElementById('print_link').style.display = 'block';
        }
    </script>


    <cc1:ModalPopupExtender ID="ModalPopupExtender3" runat="server" BackgroundCssClass="modalbackground"
        CancelControlID="ibtnCancel6" DropShadow="false" PopupControlID="div14"
        OkControlID="btnOK6" TargetControlID="Button10">
    </cc1:ModalPopupExtender>
    <div runat="server" id="div14" style="display: none">

        <div class="modal-dialog1">
            <div class="modal-content">
                <div class="modal-header">
                    <div style="float: right">
                        <button id="ibtnCancel6" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">
                            Close</button>
                    </div>
                    <h4 class="modal-title" id="H5">Project Stock Allocation</h4>
                </div>
                <div class="modal-body paddnone">
                    <div class="panel-body ">
                        <div class="row">
                            <div class="col-md-12">
                                <table cellpadding="5" cellspacing="5" border="0" class="table">
                                    <tr>
                                        <td width="150px"><b>Customer</b></td>
                                        <td>
                                            <asp:Label ID="lblCustomer6" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Project</b></td>
                                        <td>
                                            <asp:Label ID="lblProject6" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Panel Details</b></td>
                                        <td>
                                            <asp:Label ID="lblPanelDetails6" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Inverter Details</b></td>
                                        <td>
                                            <asp:Label ID="lblInverterDetails6" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Install City</b></td>
                                        <td>
                                            <asp:Label ID="lblInstallCity6" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Install State</b></td>
                                        <td>
                                            <asp:Label ID="lblInstallState6" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px"><b>Roof Type</b></td>
                                        <td>
                                            <asp:Label ID="lblRoofType6" runat="server"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td width="150px" colspan="2">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <br />
                        <div class="col-md-12">
                            <div class="form-group">
                                <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover">
                                    <tr>
                                        <td width="50%"><b>Stock Item</b></td>
                                        <td width="20%"><b>Deduct</b></td>
                                        <td width="30%"><b>Qty</b></td>
                                    </tr>
                                    <asp:Repeater ID="rptDeductRev6" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td width="50%">
                                                    <asp:HiddenField ID="hndStockItemID" runat="server" Value='<%#Eval("StockItemID") %>' />
                                                    <%#Eval("StockItem") %> </td>
                                                <td width="20%">
                                                    <asp:Label ID="lblStockDeduct" runat="server"><%#Eval("Deduct","{0:0}") %></asp:Label>
                                                </td>
                                                <td width="30%">
                                                    <asp:TextBox ID="txtDeductRevert" runat="server" MaxLength="4" Width="50px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                                                        ControlToValidate="txtDeductRevert" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
                                                    <br />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required." CssClass="comperror" ControlToValidate="txtDeductRevert" Display="Dynamic" ValidationGroup="btnrevert"></asp:RequiredFieldValidator>
                                                    <%--<asp:CompareValidator ID="CompareValidatorDR" runat="server" ControlToValidate="txtDeductRevert" ValueToCompare='<%#Eval("Deduct","{0:0}") %>'
                                                                Type="Integer" Display="Dynamic" ErrorMessage="Enter valid stock" Operator="LessThanEqual"></asp:CompareValidator>--%>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <br />
                        <div class="col-md-12" align="center">
                            <div class="form-group">
                                <%-- <asp:Button ID="Button5" runat="server" Text="Deduct" OnClick="btnDeduct_Click" CssClass="btn btn-primary savewhiteicon"
                                            CausesValidation="false" />--%>
                                <asp:Button ID="Button6" runat="server" Text="Revert" OnClick="btnRevert_Click" CssClass="btn btn-purple resetbutton" ValidationGroup="btnrevert" />
                                <asp:Button ID="Button7" runat="server" Text="Revert All" OnClick="btnRevertAll_Click" class="btn btn-purple resetbutton"
                                    CausesValidation="false" />
                                <asp:Button ID="btnOK6" Style="display: none;" runat="server"
                                    CssClass="btn" Text=" OK" Visible="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:Button ID="Button10" Style="display: none;" runat="server" />


    <script type="text/javascript">

        $(".dropdown dt a").on('click', function () {
            $(".dropdown dd ul").slideToggle('fast');

        });

        $(".dropdown dd ul li a").on('click', function () {
            $(".dropdown dd ul").hide();
        });
        $(document).bind('click', function (e) {
            var $clicked = $(e.target);
            if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
        });


        $(document).ready(function () {
            HighlightControlToValidate();

            $('.mutliSelect input[type="checkbox"]').on('click', function () {
                callMultiCheckbox();
            });
        });

        function callMultiCheckbox() {
            var title = "";
            $("#<%=ddproject.ClientID%> :checkbox").each(function () {
                if (this.checked) {
                    title += ", " + $(this).next("label").next("label.chkval").html();
                    //alert(title);
                }
            });
            if (title != "") {
                var html = title.substr(1);
                $('.multiSel').show();
                $('.multiSel').html(html);
                $(".hida").hide();
            }
            else {
                $('#spanselect').show();
                $('.multiSel').hide();
            }

        }


        function formValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    if (!Page_Validators[i].isvalid) {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    }
                    else {
                        $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    }
                }
            }
        }
        function HighlightControlToValidate() {
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {
                    $('#' + Page_Validators[i].controltovalidate).blur(function () {
                        var validatorctrl = getValidatorUsingControl($(this).attr("ID"));
                        if (validatorctrl != null && !validatorctrl.isvalid) {
                            $(this).css("border-color", "#FF5F5F");
                        }
                        else {
                            $(this).css("border-color", "#B5B5B5");
                        }
                    });
                }
            }
        }
        function getValidatorUsingControl(controltovalidate) {
            var length = Page_Validators.length;
            for (var j = 0; j < length; j++) {
                if (Page_Validators[j].controltovalidate == controltovalidate) {
                    return Page_Validators[j];
                }
            }
            return null;
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            //gridviewScroll();
        });
        $("#nav").on("click", "a", function () {
            $('#content').animate({ opacity: 0 }, 500, function () {
                //gridviewScroll();
                $('#content').delay(250).animate({ opacity: 1 }, 500);
            });
        });
        function gridviewScroll() {
            <%--$('#<%=GridView1.ClientID%>').gridviewScroll({
                width: $("#content").width() - 40,
                height: 6000,
                freezesize: 0
            });--%>
        }
    </script>

</asp:Content>
