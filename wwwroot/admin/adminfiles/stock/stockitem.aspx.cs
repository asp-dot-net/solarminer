using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_stockitem : System.Web.UI.Page
{
    protected string SiteURL;
    static DataView dv;
    protected static int custompageIndex;
    protected static int countdata;
    protected static int startindex;
    protected static int lastpageindex;
    protected static string Location;
    protected static int custompagesize = Convert.ToInt32(SiteConfiguration.GetPageSize());
    protected string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        HidePanels();
        if (!IsPostBack)
        {
            //string script = "$(document).ready(function () { $('[id*=ibtnUploadPDF]').click(); });";
            //ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);

            ddlActive.SelectedValue = "True";
            //It is written as on 1st page load only items which are active should appear.

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            custompageIndex = 1;

            BindStockCategory();
            BindLocation();
            BindGrid(0);
            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Purchase Manager")) || (Roles.IsUserInRole("Installation Manager")))
            {
                lnkAdd.Visible = true;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
            }
            else
            {
                lnkAdd.Visible = false;
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
            }
        }
    }
    public void BindStockCategory()
    {
        DataTable dt = ClstblStockCategory.tblStockCategory_Select_ByAsc();
        ddlstockcategory.DataSource = dt;
        ddlstockcategory.DataTextField = "StockCategory";
        ddlstockcategory.DataValueField = "StockCategoryID";
        ddlstockcategory.DataBind();

        ddlcategorysearch.DataSource = dt;
        ddlcategorysearch.DataTextField = "StockCategory";
        ddlcategorysearch.DataValueField = "StockCategoryID";
        ddlcategorysearch.DataBind();

        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();
    }

    public void BindLocation()
    {
        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        rptstocklocation.DataSource = dt;
        rptstocklocation.DataBind();

        ddllocationsearch.DataSource = dt;
        ddllocationsearch.DataTextField = "location";
        ddllocationsearch.DataValueField = "CompanyLocationID";
        ddllocationsearch.DataBind();

        try
        {
            ddllocationsearch.SelectedValue = "1";
        }
        catch { }


    }
    protected DataTable GetGridData()
    {
        //String active = ddlActive.SelectedValue;
        
        DataTable dt = ClstblStockItems.tblStockItemsGetDataBySearch(txtSearchStockItem.Text, txtSearchModel.Text, ddlcategorysearch.SelectedValue.ToString(), ddllocationsearch.SelectedValue.ToString(), ddlSearchState.SelectedValue.ToString(), ddlActive.SelectedValue.ToString(), ddlSalesTag.SelectedValue.ToString(),txtExpiryDate.Text);

        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanAddUpdate.Visible = false;
        PanGridSearch.Visible = true;
        Panel3.Visible = true;
        PanGrid.Visible = true;
        DataTable dt = new DataTable();
        dt = GetGridData();
        dv = new DataView(dt);

        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;

            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        PanGrid.Visible = false;
        string stockcategory = ddlstockcategory.SelectedValue;
        string stockitem = txtstockitem.Text;
        string brand = txtbrand.Text;
        string model = txtmodel.Text;
        string series = txtseries.Text;
        string minstock = txtminstock.Text;
        string salestag = chksalestag.Checked.ToString();
        //string salestag = false.ToString();
        string isactive = chkisactive.Checked.ToString();
        string description = txtdescription.Text;
        string IsDashboard = chkDashboard.Checked.ToString();
        string QuickFormID = txtfixstockitemid.Text;

        //Response.Write(QuickFormID);
        //Response.End();

        int modalexist = ClstblStockItems.tblStockItem_Modal_Exist(model);
        if (modalexist == 1)
        {
            txtmodel.Text = "";
            txtmodel.Focus();
            SetExist2();

        }
        else
        {
            // int success = ClstblStockItems.tblStockItems_Insert(stockcategory, stockitem, brand, model, series, minstock, isactive, salestag, description, txtStockSize.Text, IsDashboard);
            int success = ClstblStockItems.tblStockItems_InsertWithFixStockItemID(stockcategory, stockitem, brand, model, series, minstock, isactive, salestag, description, txtStockSize.Text, IsDashboard,QuickFormID);
            ClstblStockItems.tblStockItems_Update_StockCode(success.ToString(), success.ToString());
            foreach (RepeaterItem item in rptstocklocation.Items)
            {
                HiddenField hyplocationid = (HiddenField)item.FindControl("hndlocationid");
                TextBox txtqty = (TextBox)item.FindControl("txtqty");
                TextBox txtminQty = (TextBox)item.FindControl("txtminQty");
                CheckBox chksalestagrep = (CheckBox)item.FindControl("chksalestagrep");
                if (txtqty.Text != "" || hyplocationid.Value != "")
                {
                    int succstockloc = ClstblStockItemsLocation.tblStockItemsLocation_Insert(success.ToString(), hyplocationid.Value, txtqty.Text.Trim());
                    ClstblStockItemsLocation.tblStockItemsLocation_UpdateSalesTag(succstockloc.ToString(), chksalestagrep.Checked.ToString());
                    bool s1 = ClstblStockItemsLocation.tblStockItemsLocation_UpdateminQty(Convert.ToString(succstockloc.ToString()), txtminQty.Text);
                }
            }


            //--- do not chage this code start------
            if (success > 0)
            {
                SetUpdate();
            }
            else
            {
                SetError();
            }
            BindGrid(0);
            BindScript();
        }

        //--- do not chage this code end------
    }
    protected void btnaddmodule_Click(object sender, EventArgs e)
    {
        int success = 0;
        bool success2 = false;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

        //Response.Expires = 400;
        //Server.ScriptTimeout = 1200;
        if (ModuleFileUpload.HasFile)
        {
            ModuleFileUpload.SaveAs(Request.PhysicalApplicationPath + "\\userfiles" + "\\StockItemModule\\" + ModuleFileUpload.FileName);
            string connectionString = "";

            if (ModuleFileUpload.FileName.EndsWith(".xls"))
            {
                connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\StockItemModule\\" + ModuleFileUpload.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
            }
            else if (ModuleFileUpload.FileName.EndsWith(".xlsx"))
            {
                connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\StockItemModule\\" + ModuleFileUpload.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";

            }


            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                using (DbCommand command = connection.CreateCommand())
                {
                    // Cities$ comes from the name of the worksheet

                    command.CommandText = "SELECT * FROM [Sheet1$]";
                    command.CommandType = CommandType.Text;

                    using (DbDataReader dr = command.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                //for (int k = 0; k < dr.FieldCount; k++)
                                //{
                                    string Manufacturer = "";
                                    string stock = "";
                                    string Model_Number = "";
                                    string Approved_Date = "";
                                    string Expiry_Date = "";
                                    string Fire_tested = "";

                                    Manufacturer = dr["Licensee/Certificate Holder"].ToString();
                                    stock = dr["Licensee/Certificate Holder"].ToString();
                                    Model_Number = dr["Model Number"].ToString();
                                    Approved_Date = dr["CEC Approved Date"].ToString();
                                    Expiry_Date = dr["Expiry Date"].ToString();
                                    Fire_tested = dr["Fire Tested"].ToString();

                                    if ((Manufacturer != string.Empty) && (Model_Number != string.Empty) && (Approved_Date != string.Empty) && (Expiry_Date != string.Empty) && (Fire_tested != string.Empty))
                                    {
                                        int exist = ClstblStockItems.tblStockItem_Modal_Exist(Model_Number);
                                        if (exist == 1)
                                        {
                                            success2 = ClstblStockItems.tblStockItems_UpdateByFileForModule(Manufacturer, Model_Number, Approved_Date, Expiry_Date, Fire_tested);
                                        }
                                        else
                                        {
                                            success = ClstblStockItems.tblStockItems_InsertModuleFile("1", stock, Manufacturer, Model_Number, "", "0", "False", "False", "", "", "False", Approved_Date, Expiry_Date, Fire_tested, "");
                                            ClstblStockItems.tblStockItems_Update_StockCode(success.ToString(), success.ToString());
                                            DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select();
                                            int succstockloc = ClstblStockItemsLocation.Insert_tblStockItemsLocation_Tabel(success.ToString(),"0", userid);
                                            //int suc = ClstblStockItems.tblStockItemInventoryHistory_Insert("4", success, StockAllocationStore, StOldQty.StockQuantity, (0 - Convert.ToInt32(txtQty.Text)).ToString(), userid, ProjectID);
                                        //if (dt.Rows.Count > 0)
                                        //    {
                                        //        foreach (DataRow row in dt.Rows)
                                        //        {
                                        //            string CompanyLocationId = row["CompanyLocationID"].ToString();
                                        //           int succstockloc = ClstblStockItemsLocation.tblStockItemsLocation_Insert(success.ToString(), CompanyLocationId, "0");
                                        //            ClstblStockItemsLocation.tblStockItemsLocation_UpdateSalesTag(succstockloc.ToString(), "True");
                                        //        }
                                        //    }
                                    }
                                    }
                                    if (success > 0 || success2 == true)
                                    {
                                        SetAdd1();
                                    }
                                    else
                                    {
                                        SetError1();
                                    }
                               // }
                            }
                        }
                    }
                }
            }
        }

        BindGrid(0);
    }

    protected void btnAddInverter_Click(object sender, EventArgs e)
    {
        int success = 0;
        bool success2 = false;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        //Response.Expires = 400;
        //Server.ScriptTimeout = 1200;
        if (InverterFileUpload.HasFile)
        {
            InverterFileUpload.SaveAs(Request.PhysicalApplicationPath + "\\userfiles" + "\\StockItemInverter\\" + InverterFileUpload.FileName);
            string connectionString = "";

            if (InverterFileUpload.FileName.EndsWith(".xls"))
            {
                connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\StockItemInverter\\" + InverterFileUpload.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
            }
            else if (InverterFileUpload.FileName.EndsWith(".xlsx"))
            {
                connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\StockItemInverter\\" + InverterFileUpload.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";

            }


            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                using (DbCommand command = connection.CreateCommand())
                {
                    // Cities$ comes from the name of the worksheet

                    command.CommandText = "SELECT * FROM [Sheet1$]";
                    command.CommandType = CommandType.Text;

                    using (DbDataReader dr = command.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                string Manufacturer = "";
                                string stockitem = "";
                                string Model_Number = "";
                                string Series = "";
                                string Approved_Date = "";
                                string Expiry_Date = "";
                                string ACPower = "";

                                Manufacturer = dr["Manufacturer"].ToString();
                                stockitem = dr["Manufacturer"].ToString();
                                Series = dr["Series"].ToString();
                                Model_Number = dr["Model Number"].ToString();
                                ACPower = dr["AC Power (kW)"].ToString();
                                Approved_Date = dr["Approval Date"].ToString();
                                Expiry_Date = dr["Expiry date"].ToString();

                                if ((Manufacturer != string.Empty) && (Model_Number != string.Empty) && (Approved_Date != string.Empty) && (Expiry_Date != string.Empty) && (Series != string.Empty) && (ACPower != string.Empty))
                                {
                                    int exist = ClstblStockItems.tblStockItem_Modal_Exist(Model_Number);
                                    if (exist == 1)
                                    {
                                        success2 = ClstblStockItems.tblStockItems_UpdateByFile(Manufacturer, Model_Number, Approved_Date, Expiry_Date, "",ACPower,Series);
                                    }
                                    else
                                    {
                                        success = ClstblStockItems.tblStockItems_InsertModuleFile("2", stockitem, Manufacturer, Model_Number, Series, "0", "False", "False", "", "", "False", Approved_Date, Expiry_Date, "",ACPower);
                                        ClstblStockItems.tblStockItems_Update_StockCode(success.ToString(), success.ToString());
                                        DataTable dt = ClstblCompanyLocations.tblCompanyLocations_Select();
                                        int succstockloc = ClstblStockItemsLocation.Insert_tblStockItemsLocation_Tabel(success.ToString(), "0", userid);
                                        //this loop is required because without this,record won't appear in grid
                                        //if (dt.Rows.Count > 0)
                                        //{
                                        //    foreach (DataRow row in dt.Rows)
                                        //    {
                                        //        string CompanyLocationId = row["CompanyLocationID"].ToString();
                                        //        int succstockloc = ClstblStockItemsLocation.tblStockItemsLocation_Insert(success.ToString(), CompanyLocationId, "0");
                                        //        ClstblStockItemsLocation.tblStockItemsLocation_UpdateSalesTag(succstockloc.ToString(), "True");
                                        //    }
                                        //}

                                    }
                                }
                                if (success > 0 || success2 == true)
                                {
                                    SetAdd1();
                                }
                                else
                                {
                                    SetError1();
                                }

                            }
                        }
                    }
                }
            }
        }

        BindGrid(0);
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        SttblStockItemsLocation st_loc = ClstblStockItemsLocation.tblStockItemsLocation_SelectByid(id1);
        SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(st_loc.StockItemID);
        string stockcategory = ddlstockcategory.SelectedValue;
        string stockitem = txtstockitem.Text;
        string brand = txtbrand.Text;
        string model = txtmodel.Text;
        string series = txtseries.Text;
        string minstock = txtminstock.Text;
        //string location = ddllocation.SelectedValue;
        string salestag = chksalestag.Checked.ToString();

        //string salestag = false.ToString();
        //if (st.SalesTag != string.Empty)
        //{
        //    salestag = st.SalesTag;
        //}
        //else
        //{
        //    salestag = false.ToString();
        //}
        string isactive = chkisactive.Checked.ToString();
        string description = txtdescription.Text;
        string IsDashboard = chkDashboard.Checked.ToString();
        string QuickFormId = txtfixstockitemid.Text;

        int modalexist = ClstblStockItems.tblStockItem_Modal_Exist2(model, st_loc.StockItemID);
        if (modalexist == 1)
        {
            txtmodel.Text = "";
            txtmodel.Focus();
            SetExist2();

        }
        else
        {
            bool success = ClstblStockItems.tblStockItems_UpdateWithFixStockItemID(st_loc.StockItemID, stockcategory, stockitem, brand, model, series, minstock, isactive, salestag, description, txtStockSize.Text, IsDashboard,QuickFormId);
            ClstblStockItemsLocation.tblStockItemsLocation_Delete_StockItemID(st_loc.StockItemID);
            foreach (RepeaterItem item in rptstocklocation.Items)
            {
                HiddenField hyplocationid = (HiddenField)item.FindControl("hndlocationid");
                TextBox txtqty = (TextBox)item.FindControl("txtqty");
                CheckBox chksalestagrep = (CheckBox)item.FindControl("chksalestagrep");
                if (txtqty.Text != "" || hyplocationid.Value != "")
                {
                    TextBox txtminQty = (TextBox)item.FindControl("txtminQty");
                    int succstockloc = ClstblStockItemsLocation.tblStockItemsLocation_Insert(st_loc.StockItemID, hyplocationid.Value, txtqty.Text.Trim());
                    ClstblStockItemsLocation.tblStockItemsLocation_UpdateSalesTag(succstockloc.ToString(), chksalestagrep.Checked.ToString());
                    bool s1 = ClstblStockItemsLocation.tblStockItemsLocation_UpdateminQty(Convert.ToString(succstockloc.ToString()), txtminQty.Text);
                }
            }

            if(ddlstockcategory.SelectedValue=="1")
            {
                ClstblProjects.tblProjects_UpdatePanelsByStockItemID(st_loc.StockItemID, brand, model, txtStockSize.Text);
            }
            else if(ddlstockcategory.SelectedValue == "2")
            {
                ClstblProjects.tblProjects_UpdateInvererByStockItemID(st_loc.StockItemID, brand, model, series, txtStockSize.Text);
            }

            ////--- do not chage this code Start------
            if (success)
            {
                SetUpdate();
            }
            else
            {
                InitUpdate();
                SetError();
            }
            BindGrid(0);
            BindScript();
        }
        //--- do not chage this code end------
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        ddlhead.Items.Clear();
        ListItem item = new ListItem();
        item.Value = "";
        item.Text = "Select";
        ddlhead.Items.Add(item);

        DataTable dt_head = ClstblStockItems.tblStockItemHeadMaster_Select_StockQuantity();
        ddlhead.DataSource = dt_head;
        ddlhead.DataTextField = "HeadName";
        ddlhead.DataValueField = "id";
        ddlhead.DataBind();

        MPEUpdateStock.Show();
        hdnid.Value = id;
        SttblStockItemsLocation st = ClstblStockItemsLocation.tblStockItemsLocation_SelectByid(id);
        ltstockquantity.Text = st.StockQuantity;
        ltlocation.Text = st.CompanyLocation;
        string StockItemID = st.StockItemID;

        if (st.CompanyLocationID == "12")
        {
            ddlhead.Items.Remove(ddlhead.Items.FindByValue("2"));
        }

        SttblStockItems stStockItem = ClstblStockItems.tblStockItems_SelectByStockItemID(StockItemID);
        ltcategory.Text = stStockItem.StockCategory;
        ltstockitem.Text = stStockItem.StockItem;
        ltbrand.Text = stStockItem.StockManufacturer;
        ltmodel.Text = stStockItem.StockModel;
        ltseries.Text = stStockItem.StockSeries;

        txtquantity.Text = "";
        ddlhead.SelectedValue = "";
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        lnkBack.Visible = true;
        //Response.Write(lnkBack.Visible);
        //Response.End();
        PanGrid.Visible = false;
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        SttblStockItemsLocation st2 = ClstblStockItemsLocation.tblStockItemsLocation_SelectByid(id);
        SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(st2.StockItemID);
        ddlstockcategory.SelectedValue = st2.StockCategoryID;
        txtstockitem.Text = st2.StockItem;
        txtbrand.Text = st2.StockManufacturer;
        txtmodel.Text = st2.StockModel;
        txtseries.Text = st2.StockSeries;
        txtminstock.Text = st2.MinLevel;
        //txtminstock.Text = id;
        txtfixstockitemid.Text = st.FixStockItemID;
        try
        {
            chksalestag.Checked = Convert.ToBoolean(st2.SalesTag);
        }
        catch { }
        try
        {
            chkisactive.Checked = Convert.ToBoolean(st2.Active);
        }
        catch
        {
        }
        txtdescription.Text = st2.StockDescription;
        txtStockSize.Text = st2.StockSize;
        try
        {
            chkDashboard.Checked = Convert.ToBoolean(st2.IsDashboard);
        }
        catch
        {
        }

        foreach (RepeaterItem item in rptstocklocation.Items)
        {
            HiddenField hndlocationid = (HiddenField)item.FindControl("hndlocationid");
            DataTable dt = ClstblStockItemsLocation.tblStockItemsLocation_ByStockItemID(st2.StockItemID, hndlocationid.Value);

            if (dt.Rows.Count > 0)
            {
                TextBox txtqty = (TextBox)item.FindControl("txtqty");
                TextBox txtminQty = (TextBox)item.FindControl("txtminQty");
                CheckBox chksalestagrep = (CheckBox)item.FindControl("chksalestagrep");
                txtqty.Text = dt.Rows[0]["StockQuantity"].ToString();
                if (dt.Rows[0]["MinQty"].ToString() != null && dt.Rows[0]["MinQty"].ToString() != "")
                {
                    txtminQty.Text = dt.Rows[0]["MinQty"].ToString();
                }
                else
                {
                    txtminQty.Text = "0";
                }
                //.Text = dt.Rows[0]["MinQty"].ToString();
                if (dt.Rows[0]["SalesTag"].ToString() != null && dt.Rows[0]["SalesTag"].ToString() != "")
                {
                    chksalestagrep.Checked = Convert.ToBoolean(dt.Rows[0]["SalesTag"].ToString());
                }
            }
        }
        //--- do not chage this code start------
        InitUpdate();
        //--- do not chage this code end------

    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
            BindScript();
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);

    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        SetCancel();
       // BindScript();
        BindGrid(0);
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        //  ModalPopupExtender2.Show();
        Reset();
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        PanGrid.Visible = false;
        Reset();
        // ModalPopupExtender2.Show();
        txtstockitem.Focus();
        InitAdd();
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }
    public void SetExist2()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring2();", true);
    }


    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

    public void SetAdd()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }
    public void SetUpdate()
    {
        Reset();
        PanAddUpdate.Visible = false;

        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        BindGrid(0);
        Reset();
        HidePanels();

        PanAddUpdate.Visible = false;
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        // ModalPopupExtender2.Hide();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = false;

        //lnkBack.Visible = false;
        //lnkAdd.Visible = true;
        ////ModalPopupExtender2.Hide();
        //PanAddUpdate.Visible = false;
        //Reset();
        //btnAdd.Visible = true;
        //btnUpdate.Visible = false;
        //btnReset.Visible = true;
        //btnCancel.Visible = false;
        //lblAddUpdate.Visible = false;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
        PanGridSearch.Visible = false;
        Panel3.Visible = false;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;


        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        btnCancel.Visible = true;

        lblAddUpdate.Text = "Add ";
    }
    public void InitUpdate()
    {
        lnkBack.Visible = true;
        PanGridSearch.Visible = false;
        Panel3.Visible = false;

        // lnkAdd.Visible = false;
        PanAddUpdate.Visible = true;
        //ModalPopupExtender2.Show();
        //  HidePanels();
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnCancel.Visible = true;
        btnReset.Visible = false;

        lblAddUpdate.Text = "Update ";
    }
    private void HidePanels()
    {
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        PanAlreadExists.Visible = false;

        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
    }
    public void Reset()
    {
        PanAddUpdate.Visible = true;
        ddlstockcategory.ClearSelection();
        txtstockitem.Text = string.Empty;
        txtbrand.Text = string.Empty;
        txtmodel.Text = string.Empty;
        txtseries.Text = string.Empty;
        txtminstock.Text = string.Empty;
        txtfixstockitemid.Text = string.Empty;
        //ddllocation.ClearSelection();
        chksalestag.Checked = false;
        chkisactive.Checked = false;
        txtdescription.Text = string.Empty;
        txtStockSize.Text = string.Empty;
        chkDashboard.Checked = false;

        foreach (RepeaterItem item in rptstocklocation.Items)
        {
            TextBox txtqty = (TextBox)item.FindControl("txtqty");
            TextBox txtminQty = (TextBox)item.FindControl("txtminQty");
            
            CheckBox chksalestagrep = (CheckBox)item.FindControl("chksalestagrep");
            txtqty.Text = "0";
            txtminQty.Text = "0";
            chksalestagrep.Checked = false;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
        BindScript();
        //System.Threading.Thread.Sleep(200000);
    }

    protected void ddlhead_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlhead.SelectedValue == "1")
        {
            RegularExpressionValidator1.Visible = true;
            RegularExpressionValidator2.Visible = false;
        }
        else if (ddlhead.SelectedValue == "2")
        {
            RegularExpressionValidator1.Visible = false;
            RegularExpressionValidator2.Visible = true;
        }
        MPEUpdateStock.Show();
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        SttblStockItemsLocation st = ClstblStockItemsLocation.tblStockItemsLocation_SelectByid(hdnid.Value);
        SttblStockItems stStockItem = ClstblStockItems.tblStockItems_SelectByStockItemID(st.StockItemID);
        SttblStockItemsLocation ST1 = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(st.StockItemID, "12");
        string head = ddlhead.SelectedValue;
        string quantity = txtquantity.Text;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();

        string quantity1 = "";
        if (quantity.Substring(0, 1) == "-")
        {
            quantity1 = (Convert.ToInt32(st.StockQuantity) - (Convert.ToInt32(quantity.Substring(1)))).ToString();

            if (head == "2")
            {
                ClstblStockItemsLocation.tblStockItemsLocation_UpdateByStockItemID(st.StockItemID, quantity.Substring(1));
                ClstblStockItems.tblStockItemInventoryHistory_Insert(head, stStockItem.StockItemID, "12", ST1.StockQuantity, quantity.Substring(1), userid, "0");
            }
        }
        else
        {
            quantity1 = (Convert.ToInt32(st.StockQuantity) + (Convert.ToInt32(quantity))).ToString();
        }
        ClstblStockItemsLocation.tblStockItemsLocation_Update(hdnid.Value, quantity1);
        ClstblStockItems.tblStockItemInventoryHistory_Insert(head, stStockItem.StockItemID, st.CompanyLocationID, st.StockQuantity, quantity.Trim(), userid, "0");

        MPEUpdateStock.Hide();
        BindGrid(0);
        BindScript();
    }
    //protected void btnClearAll_Click(object sender, EventArgs e)
    //{
    //    txtSearchStockItem.Text = string.Empty;
    //    txtSearchManufacturer.Text = string.Empty;
    //    ddlcategorysearch.SelectedValue = "";
    //    ddllocationsearch.SelectedValue = "";
    //    ddlSearchState.SelectedValue = "";
    //    ddlActive.SelectedValue = "";
    //    ddlSalesTag.SelectedValue = "";

    //    BindGrid(0);
    //    BindScript();
    //}
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "updatestock")
        {
            ModalPopupExtender1.Show();
            hndStockItemID.Value = e.CommandArgument.ToString();

            SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(hndStockItemID.Value);
            txtStockItemU.Text = st.StockItem;
            txtStockManufacturerU.Text = st.StockManufacturer;
            txtStockModelU.Text = st.StockModel;
            txtStockSeriesU.Text = st.StockSeries;
            chkActiveU.Checked = Convert.ToBoolean(st.Active);
            chkSalesTagU.Checked = Convert.ToBoolean(st.SalesTag);
           
        }

        if (e.CommandName.ToLower() == "upload")
        {
            ModalPopupExtenderUpload.Show();
            hdnStockItemID.Value = e.CommandArgument.ToString();
        }

        if (e.CommandName == "SerialNo")
        {
            string[] arg = new string[2];
            arg = e.CommandArgument.ToString().Split(';');
            string StockItemID = arg[0];
            string CompanyLocationId = arg[1];
            Response.Redirect("~/admin/adminfiles/stock/Stockdetail.aspx?StockItemID=" + StockItemID + "&CompanyLocationId=" + CompanyLocationId);
        }

        BindGrid(0);
    }
    protected void btnSaveItem_Click(object sender, EventArgs e)
    {
        string StockItemID = hndStockItemID.Value;
        string StockItem = txtStockItemU.Text;
        string StockManufacturer = txtStockManufacturerU.Text;
        string StockModel = txtStockModelU.Text;
        string StockSeries = txtStockSeriesU.Text;
        string Active = Convert.ToString(chkActiveU.Checked);
        string SalesTag = Convert.ToString(chkSalesTagU.Checked);

        bool suc = ClstblStockItems.tblStockItems_Update_ItemDetail(StockItemID, StockItem, StockManufacturer, StockModel, StockSeries, Active, SalesTag);
        BindGrid(0);
        BindScript();
    }
   
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }
   
    public void BindScript()
    {
        //  ScriptManager.RegisterStartupScript(update_panel1, this.GetType(), "MyAction", "doMyAction();", true);
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        SetCancel();
        BindGrid(0);
    }

    protected DataView SortDataTable(DataTable ptblDataTable, Boolean pblnIsPageIndexChanging)
    {
        if (ptblDataTable != null)
        {
            DataView dataView = new DataView(ptblDataTable);

            if (GridViewSortExpression != string.Empty)
                if (pblnIsPageIndexChanging)
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                else
                    dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
            return dataView;
        }
        else
            return new DataView();
    }
    private string GridViewSortDirection
    {
        get { return ViewState["SortDirection"] as string ?? "ASC"; }
        set { ViewState["SortDirection"] = value; }
    }
    private string GridViewSortExpression
    {
        get { return ViewState["SortExpression"] as string ?? string.Empty; }
        set { ViewState["SortExpression"] = value; }
    }
    private string GetSortDirection()
    {
        switch (GridViewSortDirection)
        {
            case "ASC":
                GridViewSortDirection = "DESC";
                break;
            case "DESC":
                GridViewSortDirection = "ASC";
                break;
        }
        return GridViewSortDirection;
    }
    protected void btnClearAll_Click1(object sender, EventArgs e)
    {
        txtSearchStockItem.Text = string.Empty;
        //txtSearchManufacturer.Text = string.Empty;
        txtSearchModel.Text = string.Empty;
        ddlcategorysearch.SelectedValue = "";
        //ddllocationsearch.SelectedValue = "";
        ddllocationsearch.SelectedValue = "1";
        ddlSearchState.SelectedValue = "";
        ddlActive.SelectedValue = "";
        ddlSalesTag.SelectedValue = "";

        BindGrid(0);
        BindScript();
    }
    protected void GridView1_DataBound1(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    protected void GridView1_RowCreated1(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);          
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnStockitemID2 = (HiddenField)e.Row.FindControl("hdnStockitemID2");
            string StockItemID = hdnStockitemID2.Value;
            if (!string.IsNullOrEmpty(StockItemID))
            {
                SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(StockItemID);
                HyperLink lnkPDFUploded = (HyperLink)e.Row.FindControl("lnkPDFUploded");
                HyperLink lnkPDFPending = (HyperLink)e.Row.FindControl("lnkPDFPending");
                if(!string.IsNullOrEmpty(st.FileName))
                {
                    lnkPDFUploded.Visible = true;
                    lnkPDFPending.Visible = false;
                    lnkPDFUploded.NavigateUrl=pdfURL+"StockItemPDF/" + st.StockItemID + "_" + st.FileName;
                    lnkPDFUploded.Target = "_blank";
                }
                else
                {
                    lnkPDFUploded.Visible = false;
                    lnkPDFPending.Visible = true;
                }
            }
        }        
    }

    #region pagination
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    Response.Write(lnkpagebtn.);
            //    //lnkpagebtn.Style.Add("class", "pagebtndesign nonepointer");
            //}
        }
    }
    public void PageClick(String id)
    {
        custompageIndex = Convert.ToInt32(id);
        BindGrid(0);
    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        //lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;



        //if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        //{
        //    lastpageindex = lastpageindex + 1;
        //}
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        //ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    #endregion

    protected void ibtnUploadPDF_Click(object sender, EventArgs e)
    {
        try
        {
            //System.Threading.Thread.Sleep(100000);
            if (FileUpload1.HasFile)
            {
                //SiteConfiguration.DeletePDFFile("SQ", st.SQ);
                string PDFFilename = string.Empty;
                string Filename = FileUpload1.FileName;
                string StockItemID = hdnStockItemID.Value;
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stemp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

                if (!string.IsNullOrEmpty(StockItemID))
                {
                    SttblStockItems st = ClstblStockItems.tblStockItems_SelectByStockItemID(StockItemID);

                
                    PDFFilename = StockItemID + "_" + Filename;

                    //if (!string.IsNullOrEmpty(st.FileName))
                    //{
                    //    SiteConfiguration.DeletePDFFile("StockItemPDF", PDFFilename);
                    //}

                    SiteConfiguration.DeletePDFFile("StockItemPDF", PDFFilename);
                    FileUpload1.SaveAs(HttpContext.Current.Request.PhysicalApplicationPath + ("/userfiles/StockItemPDF/") + PDFFilename);
                    SiteConfiguration.UploadPDFFile("StockItemPDF", PDFFilename);
                    SiteConfiguration.deleteimage(PDFFilename, "StockItemPDF");

                    bool update = ClstblStockItems.tblStockItems_UpdateFileName(StockItemID, Filename, stemp.EmployeeID, DateTime.Now.AddHours(14).ToString());
                    SetAdd1();
                    BindGrid(0);
                }
            }
        }
        catch
        {
            SetError1();
        }
    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        BindGrid(0);
    }
}