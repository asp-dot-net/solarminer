﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" 
    AutoEventWireup="true" CodeFile="Stockdetail.aspx.cs" Inherits="admin_adminfiles_stock_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Stock Item History
          <asp:Literal runat="server" ID="ltcompname"></asp:Literal>
                </h5>
                <div id="hbreadcrumb" class="pull-right">
                    <ol class="hbreadcrumb breadcrumb fontsize16">                       
                        <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                    </ol>
                </div>
            </div>
            <%--<script src="<%=Siteurl %>admin/js/bootstrap.file-input.js"></script>--%>
            <script>
                var focusedElementId = "";
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').css('display', 'block');

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {

                    $('.loading-container').css('display', 'none');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();

                    $(".myvalstockitem").select2({
                        //placeholder: "select",
                        allowclear: true
                    });

                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox();
                    });


                    $(".myval1").select2({
                        minimumResultsForSearch: -1
                    });

                    $(".myval").select2({
                        minimumResultsForSearch: -1
                    });
                    if ($(".tooltips").length) {
                        $('.tooltips').tooltip();
                    }


                    $("[data-toggle=tooltip]").tooltip();
                    $('.tooltipwidth').tooltip();
                    $('.tooltips').tooltip();
                    $('.mutliSelect input[type="checkbox"]').on('click', function () {
                        callMultiCheckbox();
                    });

                    $('.datetimepicker1').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });


                }

                function formValidate() {
                    if (typeof (Page_Validators) != "undefined") {
                        for (var i = 0; i < Page_Validators.length; i++) {
                            if (!Page_Validators[i].isvalid) {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                                $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                            }
                            else {
                                $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                                $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                            }
                        }
                    }
                }

            </script>
            
            <div class="page-body padtopzero">               
                 <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panel">
                        <div class="messesgarea">
                           <%-- <div class="alert alert-success" id="PanSuccess" runat="server">
                                <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                            </div>
                            <div class="alert alert-danger" id="PanError" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                    Text="Transaction Failed."></asp:Label></strong>
                            </div>
                            <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                                <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                            </div>--%>
                            <div class="alert alert-info" id="PanNoRecord" runat="server">
                                <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                            </div>
                        </div>                      
                    </div>
                </asp:Panel>
                 
                <div class="finalgrid">
                    <asp:Panel ID="panel1" runat="server" CssClass="xsroll">
                        <div>
                            <div id="PanGrid" runat="server">
                                <div class="table-responsive">
                                    <asp:GridView ID="GridView1" DataKeyNames="StockItemID" runat="server" CssClass="tooltip-demo text-center table table-striped GridviewScrollItem table-bordered table-hover Gridview"
                                        OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                        AllowSorting="true" OnRowDeleting="GridView1_RowDeleting"
                                        OnDataBound="GridView1_DataBound1" OnRowCreated="GridView1_RowCreated1" AllowPaging="true" AutoGenerateColumns="false" PageSize="25">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Category Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="150px" SortExpression="StockItem">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label3" runat="server" >
                                                <%#Eval("CategoryName")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Serial No" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" SortExpression="StockManufacturer" HeaderStyle-Width="200px">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label6" runat="server" data-placement="top" data-original-title='<%#Eval("StockManufacturer")%>' data-toggle="tooltip"
                                                        ><%#Eval("SerialNo")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Item Name" ItemStyle-VerticalAlign="Top"  HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="left" SortExpression="StockCategory">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label5" runat="server">
                                                <%#Eval("StockItem")%></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" SortExpression="StockModel">
                                                <ItemTemplate>
                                                     <asp:LinkButton ID="gvbtnView" runat="server" CommandName="History" CommandArgument='<%#Eval("SerialNo")%>'  CausesValidation="false" CssClass="btn btn-info btn-xs"
                                                        data-toggle="tooltip" data-placement="top" title="View History" data-original-title="Edit">
                                                            <i class="fa fa-edit"></i> View
                                                    </asp:LinkButton>    
                                                </ItemTemplate>
                                            </asp:TemplateField>                                                                                                                                                                       
                                           <%-- <asp:TemplateField HeaderText="" ItemStyle-Width="30px" HeaderStyle-CssClass="brnone spcalmarandpaddnone">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" CssClass="btn btn-info btn-xs"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                            <i class="fa fa-edit"></i> Edit
                                                    </asp:LinkButton>                                                
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" CssClass="verticaaline" />
                                            </asp:TemplateField>--%>
                                        </Columns>
                                        <AlternatingRowStyle />

                                        <PagerTemplate>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            <div class="pagination">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                            </div>
                                        </PagerTemplate>
                                        <PagerStyle CssClass="paginationGrid" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                    </asp:GridView>
                                </div>
                                <div class="paginationnew1" runat="server" id="divnopage">
                                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>

                                </div>

                           </div>                            
                        </div>
                    </asp:Panel>
                </div>
            </div>

        </ContentTemplate>
       <%-- <Triggers>
            <asp:PostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnClearAll" />
            <asp:PostBackTrigger ControlID="btnaddmodule" />
            <asp:PostBackTrigger ControlID="btnAddInverter" />
        </Triggers>--%>
    </asp:UpdatePanel>
   
</asp:Content>

