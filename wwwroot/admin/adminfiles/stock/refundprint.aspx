﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="refundprint.aspx.cs" Inherits="mailtemplate_refundprint" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title></title>
<style>
	
</style>
</head>
<body>
<div class="pagewrap">
        <div runat="server" id="PanGrid" class="printpage">
            <div class="stocktransfer">
                <h1>Stock Transfer Detail</h1>
                <div class="martopbtm20">

                    <table class="table table-bordered table-striped printarea">
                        <tr>
                            <th align="left" width="100">From&nbsp;Location:&nbsp;</th>
                            <td align="left">
                                <asp:Label ID="lblFrom" runat="server"></asp:Label></td>
                            <th align="left" width="100">To&nbsp;Location:&nbsp;</th>
                            <td align="left">
                                <asp:Label ID="lblTo" runat="server"></asp:Label></td>
                        </tr>

                        <tr>
                            <th align="left" width="100">Transfer Date: </th>
                            <td align="left">
                                <asp:Label ID="lblTransferDate" runat="server"></asp:Label></td>
                            <th align="left" width="100">Transfer By: </th>
                            <td align="left">
                                <asp:Label ID="lblTransferBy" runat="server"></asp:Label></td>
                        </tr>

                    </table>

                </div>
            </div>
            <div class="qty marbmt25">
                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="stockitem">
                    <tr>
                        <th width="5%" align="center">Qty</th>
                        <th width="20%" align="left">Code</th>
                        <th align="left">Stock Item</th>
                    </tr>
                    <asp:Repeater ID="rptItems" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lblQty" runat="server"><%#Eval("TransferQty") %></asp:Label></td>
                                <td align="left">
                                    <asp:Label ID="lblCode" runat="server"><%#Eval("StockCode") %></asp:Label></td>
                                <td align="left">
                                    <asp:Label ID="lblItem" runat="server"><%#Eval("StockItem") %></asp:Label></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
            </div>
            <div class="tracking">
                <table width="100%" border="0" cellspacing="5" cellpadding="0">
                    <tr class="bold">
                        <th colspan="2">Total Qty:
                            <asp:Label ID="lbltotalqty" runat="server"></asp:Label>
                        </th>
                    </tr>
                    <tr class="bold">
                        <th>Tracking No:
                            <asp:Label ID="lblTrackingNo" runat="server"></asp:Label></th>
                        <th>Received Date:
                            <asp:Label ID="lblReceivedDate" runat="server"></asp:Label></th>
                    </tr>
                    <tr class="bold">
                        <th>Notes:</th>
                        <th>Received By:&nbsp;<asp:Label ID="lblReceivedBy" runat="server"></asp:Label></th>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblNotes" runat="server"></asp:Label></td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</body>
</html>