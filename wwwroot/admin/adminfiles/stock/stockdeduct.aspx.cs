using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_stock_stockdeduct : System.Web.UI.Page
{

    protected DataTable rpttable;
    static int MaxAttribute = 1;
    static string Operationmode;
    static int countPanelNo;
    static int countSerialNo;
    static DataView dv;
    static DataView dv3;
    static DataView dv4;
    protected static string Siteurl;
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((Roles.IsUserInRole("Administrator")))
        {
            txtQty.ReadOnly = false;
            txtInvQty1.ReadOnly = false;
            txtInvQty2.ReadOnly = false;
        }
        if (!IsPostBack)
        {
            //ModalPopupExtenderDeduct.Show();
            StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
            Siteurl = st.siteurl;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            ddlSelectRecords4.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords4.DataBind();

            DataTable dt = ClstblContacts.tblCustType_SelectVender();


            BindGrid(0);



            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("WarehouseManager")) || (Roles.IsUserInRole("Installation Manager")) || (Roles.IsUserInRole("PreInstaller") || (Roles.IsUserInRole("PostInstaller"))))
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = true;
                //GridView2.Columns[GridView2.Columns.Count - 1].Visible = true;
            }
            else
            {
                GridView1.Columns[GridView1.Columns.Count - 1].Visible = false;
                //GridView2.Columns[GridView2.Columns.Count - 1].Visible = false;
            }

            BindDropDown();

            string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserID(userid);
            if (Convert.ToBoolean(st_emp.showexcel) == true)
            {
                //tdExport.Visible = true;
            }
            else
            {
                // tdExport.Visible = false;
            }
        }
    }

    public void BindDropDown()
    {
        ddlSearchState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState.DataMember = "State";
        ddlSearchState.DataTextField = "State";
        ddlSearchState.DataValueField = "State";
        ddlSearchState.DataBind();

        ddlSearchState3.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlSearchState3.DataMember = "State";
        ddlSearchState3.DataTextField = "State";
        ddlSearchState3.DataValueField = "State";
        ddlSearchState3.DataBind();

        ddllocationsearch.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddllocationsearch.DataTextField = "location";
        ddllocationsearch.DataValueField = "CompanyLocationID";
        ddllocationsearch.DataMember = "CompanyLocationID";
        ddllocationsearch.DataBind();

        ddllocationsearch3.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        ddllocationsearch3.DataTextField = "location";
        ddllocationsearch3.DataValueField = "CompanyLocationID";
        ddllocationsearch3.DataMember = "CompanyLocationID";
        ddllocationsearch3.DataBind();


        //ddlSearchState2.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        //ddlSearchState2.DataMember = "State";
        //ddlSearchState2.DataTextField = "State";
        //ddlSearchState2.DataValueField = "State";
        //ddlSearchState2.DataBind();

        //ddllocationsearch2.DataSource = ClstblCompanyLocations.tblCompanyLocations_Select_Location();
        //ddllocationsearch2.DataTextField = "location";
        //ddllocationsearch2.DataValueField = "CompanyLocationID";
        //ddllocationsearch2.DataMember = "CompanyLocationID";
        //ddllocationsearch2.DataBind();

        lstSearchStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        lstSearchStatus.DataBind();
    }
    protected DataTable GetGridData1()
    {
        DataTable dt1 = ClstblProjects.tblProjects_SelectWarehouse("", txtSearch.Text, txtProjectNumber.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, ddllocationsearch.SelectedValue, chkHistoric.Checked.ToString());

        int iTotalRecords = dt1.Rows.Count;
        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;

        if (iEndRecord > iTotalRecords)
        {
            iEndRecord = iTotalRecords;
        }
        if (iStartsRecods == 0)
        {
            iStartsRecods = 1;
        }
        if (iEndRecord == 0)
        {
            iEndRecord = iTotalRecords;
        }
        //ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt1;
    }
    //protected DataTable GetGridData2()
    //{
    //    DataTable dt2 = ClstblProjects.tblProjects_SelectWarehouse("1", txtSearch2.Text, txtProjectNumber2.Text, txtSerachCity2.Text, ddlSearchState2.SelectedValue, txtStartDate2.Text, txtEndDate2.Text, ddlSearchDate2.SelectedValue, ddllocationsearch2.SelectedValue);
    //    return dt2;
    //}
    protected DataTable GetGridData3()
    {
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }
        //Response.Write("1" + "==" + txtSearch3.Text + "==" + txtProjectNumber3.Text + "==" + txtSerachCity3.Text + "==" + ddlSearchState3.SelectedValue + "==" + txtStartDate3.Text + "==" + txtEndDate3.Text + "==" + ddlSearchDate3.SelectedValue + "==" + ddllocationsearch3.SelectedValue + "==" + selectedItem);
        //Response.End();
        DataTable dt3 = ClstblProjects.tblProjects_SelectWarehouseAllocated("1", txtSearch3.Text, txtProjectNumber3.Text, txtSerachCity3.Text, ddlSearchState3.SelectedValue, txtStartDate3.Text, txtEndDate3.Text, ddlSearchDate3.SelectedValue, ddllocationsearch3.SelectedValue, selectedItem);
        int iTotalRecords = dt3.Rows.Count;
        int iEndRecord = GridView3.PageSize * (GridView3.PageIndex + 1);
        int iStartsRecods = (iEndRecord + 1) - GridView3.PageSize;
        if (iEndRecord > iTotalRecords)
        {
            iEndRecord = iTotalRecords;
        }
        if (iStartsRecods == 0)
        {
            iStartsRecods = 1;
        }
        if (iEndRecord == 0)
        {
            iEndRecord = iTotalRecords;
        }
        //ltrPage3.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt3;
    }
    protected DataTable GetGridData4()
    {
        DataTable dt4 = new DataTable();
        dt4 = ClstblProjects.tblProjects_StockRevert("1", txtSearch3.Text, txtProjectNumber3.Text, txtSerachCity3.Text, ddlSearchState3.SelectedValue, txtStartDate3.Text, txtEndDate3.Text, ddlSearchDate3.SelectedValue, ddllocationsearch3.SelectedValue, ddlRevert.SelectedValue);

        int iTotalRecords = dt4.Rows.Count;
        int iEndRecord = GridView2.PageSize * (GridView2.PageIndex + 1);
        int iStartsRecods = (iEndRecord + 1) - GridView2.PageSize;
        if (iEndRecord > iTotalRecords)
        {
            iEndRecord = iTotalRecords;
        }
        if (iStartsRecods == 0)
        {
            iStartsRecods = 1;
        }
        if (iEndRecord == 0)
        {
            iEndRecord = iTotalRecords;
        }
        // ltrPage4.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt4;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt1 = new DataTable();
        dt1 = GetGridData1();
        dv = new DataView(dt1);

        if (dt1.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
            // divnopage.Visible = false;
        }
        else
        {
            PanGrid.Visible = true;
            GridView1.DataSource = dt1;
            GridView1.DataBind();
            // Panel1.Visible = false;
            PanNoRecord.Visible = false;
            if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt1.Rows.Count)
            {
                //========label Hide
                // divnopage.Visible = false;
            }
            else
            {
                // divnopage.Visible = true;
                int iTotalRecords = dv.ToTable().Rows.Count;
                int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                //ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
        }

        //DataTable dt2 = new DataTable();
        //dt2 = GetGridData2();
        //if (dt2.Rows.Count == 0)
        //{
        //    PanNoRecord2.Visible = true;
        //    PanGrid2.Visible = false;
        //}
        //else
        //{
        //    PanGrid2.Visible = true;
        //    GridView2.DataSource = dt2;
        //    GridView2.DataBind();
        //    PanNoRecord2.Visible = false;
        //}

        DataTable dt3 = new DataTable();
        dt3 = GetGridData3();
        dv3 = new DataView(dt3);
        if (dt3.Rows.Count == 0)
        {

            SetNoRecords();
            //Div15.Visible = true;
            PanGrid3.Visible = false;
            //divnopage3.Visible = false;
        }
        else
        {
            PanGrid3.Visible = true;
            GridView3.DataSource = dt3;
            GridView3.DataBind();
            Div15.Visible = false;

            if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt3.Rows.Count)
            {
                //========label Hide
                //  divnopage3.Visible = false;
            }
            else
            {
                //  divnopage3.Visible = true;
                int iTotalRecords = dv3.ToTable().Rows.Count;
                int iEndRecord = GridView3.PageSize * (GridView3.PageIndex + 1);
                int iStartsRecods = (iEndRecord + 1) - GridView3.PageSize;
                if (iEndRecord > iTotalRecords)
                {
                    iEndRecord = iTotalRecords;
                }
                if (iStartsRecods == 0)
                {
                    iStartsRecods = 1;
                }
                if (iEndRecord == 0)
                {
                    iEndRecord = iTotalRecords;
                }
                //ltrPage3.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
            }
        }

        DataTable dt4 = new DataTable();
        dt4 = GetGridData4();
        dv4 = new DataView(dt4);
        if (dt4.Rows.Count == 0)
        {
            //SetNoRecords();
            //PanNoRecord4.Visible = true;
            PanGrid4.Visible = false;
            // divnopage4.Visible = false;
        }
        else
        {
            PanGrid4.Visible = true;
            GridView2.DataSource = dt4;
            GridView2.DataBind();
            PanNoRecord4.Visible = false;

            if (ddlSelectRecords4.SelectedValue != string.Empty)
            {
                if (Convert.ToInt32(ddlSelectRecords4.SelectedValue) < dt4.Rows.Count)
                {
                    //========label Hide
                    // divnopage4.Visible = false;
                }
                else
                {
                    //  divnopage4.Visible = true;
                    int iTotalRecords = dv4.ToTable().Rows.Count;
                    int iEndRecord = GridView2.PageSize * (GridView2.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView2.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    //ltrPage4.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
        }


    }

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    //protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    //Response.Write(e.CommandName);
    //    //Response.End();
    //    if (e.CommandName == "deduct")
    //    {
    //       //Response.Write(e.CommandName);
    //        ModalPopupExtenderDeduct.Show();
    //      //  Response.Write(ModalPopupExtenderDeduct);
    //        string ProjectID = e.CommandArgument.ToString();
    //        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
    //        if (st.IsDeduct == "False")
    //        {
    //            //DataTable dtHistory = ClstblStockItems.tblStockItemInventoryHistory_SelectByProjectId(ProjectID);
    //            //if (dtHistory.Rows.Count > 0)
    //            //{
    //           // ModalPopupExtenderDeduct.Show();
    //            lblCustomer.Text = st.Customer;
    //            lblProject.Text = st.Project;
    //            lblPanelDetails.Text = st.PanelDetails;
    //            lblInverterDetails.Text = st.InverterDetails;
    //            lblInstallCity.Text = st.InstallCity;
    //            lblInstallState.Text = st.InstallState;
    //            lblRoofType.Text = st.RoofType;

    //            ListItem item6 = new ListItem();
    //            item6.Text = "Select";
    //            item6.Value = "";
    //            ddlStore.Items.Clear();
    //            ddlStore.Items.Add(item6);

    //            DataTable dt = ClstblCompanyLocations.tblCompanyLocations_SelectActive();
    //            ddlStore.DataSource = dt;
    //            ddlStore.DataValueField = "CompanyLocationID";
    //            ddlStore.DataMember = "CompanyLocation";
    //            ddlStore.DataTextField = "CompanyLocation";
    //            ddlStore.DataBind();

    //            if (st.StockAllocationStore != string.Empty)
    //            {
    //                ddlStore.SelectedValue = st.StockAllocationStore;
    //            }

    //            lblStockItem.Text = st.PanelBrandName;
    //            txtQty.Text = st.NumberPanels;

    //            if (st.InverterDetailsID != string.Empty)
    //            {
    //                lblInv1.Text = st.InverterDetailsName;
    //                trInv1.Visible = true;
    //                DataTable dt1 = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStore.SelectedValue, st.InverterDetailsID);
    //                lblStock1.Text = dt1.Rows[0]["StockQuantity"].ToString();
    //                if (st.Installer != string.Empty && st.InverterDetailsID != string.Empty && ddlStore.SelectedValue != string.Empty)
    //                {
    //                    DataTable dtEXStock1 = ClstblExtraStock.tblExtraStock_SelectByInstallerID(st.Installer, st.InverterDetailsID, ddlStore.SelectedValue);
    //                    if (dtEXStock1.Rows.Count > 0)
    //                    {
    //                        ListItem lst = new ListItem();
    //                        lst.Text = "Extra Stock";
    //                        lst.Value = "";
    //                        ddlExtraStock1.Items.Add(lst);
    //                        for (int i = 1; i <= Convert.ToInt32(dtEXStock1.Rows[0]["ExtraStock"].ToString()); i++)
    //                        {
    //                            ddlExtraStock1.Items.Add(i.ToString());
    //                        }
    //                    }
    //                }
    //            }
    //            else
    //            {
    //                trInv1.Visible = false;
    //            }
    //            if (st.SecondInverterDetailsID != string.Empty)
    //            {
    //                lblInv2.Text = st.SecondInverterDetails;
    //                trInv2.Visible = true;
    //                DataTable dt2 = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStore.SelectedValue, st.SecondInverterDetailsID);
    //                lblStock2.Text = dt2.Rows[0]["StockQuantity"].ToString();
    //                DataTable dtEXStock2 = ClstblExtraStock.tblExtraStock_SelectByInstallerID(st.Installer, st.SecondInverterDetailsID, ddlStore.SelectedValue);

    //                if (dtEXStock2.Rows.Count > 0)
    //                {
    //                    ListItem lst = new ListItem();
    //                    lst.Text = "Extra Stock";
    //                    lst.Value = "";
    //                    ddlExtraStock2.Items.Add(lst);
    //                    for (int i = 1; i <= Convert.ToInt32(dtEXStock2.Rows[0]["ExtraStock"].ToString()); i++)
    //                    {
    //                        ddlExtraStock2.Items.Add(i.ToString());
    //                    }
    //                }
    //            }
    //            else
    //            {
    //                trInv2.Visible = false;
    //            }
    //            if (Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("WarehouseManager") || Roles.IsUserInRole("PreInstaller"))
    //            {
    //                txtQty.Enabled = true;
    //                txtInvQty1.Enabled = true;
    //                txtInvQty2.Enabled = true;
    //            }
    //            else
    //            {
    //                txtQty.Enabled = false;
    //                txtInvQty1.Enabled = false;
    //                txtInvQty2.Enabled = false;
    //            }
    //            if (ProjectID != string.Empty)
    //            {
    //                if (ddlStore.SelectedValue != string.Empty)
    //                {
    //                    DataTable dtStock = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStore.SelectedValue, st.PanelBrandID);
    //                    lblStock.Text = dtStock.Rows[0]["StockQuantity"].ToString();
    //                    if (st.Installer != string.Empty && st.PanelBrandID != string.Empty && ddlStore.SelectedValue != string.Empty)
    //                    {
    //                        DataTable dtEXStock = ClstblExtraStock.tblExtraStock_SelectByInstallerID(st.Installer, st.PanelBrandID, ddlStore.SelectedValue);

    //                        if (dtEXStock.Rows.Count > 0)
    //                        {
    //                            ListItem lst = new ListItem();
    //                            lst.Text = "Extra Stock";
    //                            lst.Value = "";
    //                            ddlExtraStock.Items.Add(lst);
    //                            for (int i = 1; i <= Convert.ToInt32(dtEXStock.Rows[0]["ExtraStock"].ToString()); i++)
    //                            {
    //                                ddlExtraStock.Items.Add(i.ToString());
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //            hndProjectID.Value = e.CommandArgument.ToString();
    //            Operationmode = "Add";
    //            MaxAttribute = 1;
    //            bindrepeater();
    //            BindAddedAttribute();
    //            //}
    //        }
    //        else
    //        {
    //            hndProjectID.Value = e.CommandArgument.ToString();
    //            ModalPopupExtenderDR.Show();

    //            lblCustomer2.Text = st.Customer;
    //            lblProject2.Text = st.Project;
    //            lblPanelDetails2.Text = st.PanelDetails;
    //            lblInverterDetails2.Text = st.InverterDetails;
    //            lblInstallCity2.Text = st.InstallCity;
    //            lblInstallState2.Text = st.InstallState;
    //            lblRoofType2.Text = st.RoofType;

    //            DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectStock(ProjectID);
    //            if (dt.Rows.Count > 0)
    //            {
    //                rptDeductRev.DataSource = dt;
    //                rptDeductRev.DataBind();
    //            }
    //        }
    //    }
    //    if (e.CommandName == "move")
    //    {
    //        string ProjectID = e.CommandArgument.ToString();
    //        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
    //        ModalPopupExtenderMove.Show();

    //        ListItem itemP = new ListItem();
    //        itemP.Text = "Select";
    //        itemP.Value = "";
    //        ddlProjectMove.Items.Clear();
    //        ddlProjectMove.Items.Add(itemP);

    //        DataTable dt = ClstblProjects.tblProjects_SelectMove(stPro.PanelBrandID, stPro.InverterDetailsID);
    //        if (dt.Rows.Count > 0)
    //        {
    //            ddlProjectMove.DataSource = dt;
    //            ddlProjectMove.DataValueField = "ProjectID";
    //            ddlProjectMove.DataMember = "Project";
    //            ddlProjectMove.DataTextField = "Project";
    //            ddlProjectMove.DataBind();
    //        }

    //        ListItem itemI = new ListItem();
    //        itemI.Text = "Select";
    //        itemI.Value = "";
    //        ddlInstallerMove.Items.Clear();
    //        ddlInstallerMove.Items.Add(itemI);

    //        ddlInstallerMove.DataSource = ClstblContacts.tblContacts_SelectInverter();
    //        ddlInstallerMove.DataValueField = "ContactID";
    //        ddlInstallerMove.DataMember = "Contact";
    //        ddlInstallerMove.DataTextField = "Contact";
    //        ddlInstallerMove.DataBind();

    //        try
    //        {
    //            ddlInstallerMove.SelectedValue = stPro.Installer;
    //        }
    //        catch { }
    //        try
    //        {
    //            txtInstallerDateMove.Text = Convert.ToDateTime(stPro.InstallBookingDate).ToShortDateString();
    //        }
    //        catch { }
    //        hndProjectID.Value = ProjectID;
    //    }
    //    //if (e.CommandName == "allocatepanel")
    //    //{
    //    //    string ProjectID = e.CommandArgument.ToString();
    //    //    SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
    //    //    ModalPopupExtender2.Show();
    //    //    lblCustomer3.Text = st.Customer;
    //    //    lblProject3.Text = st.Project;
    //    //    lblPanelDetails.Text = st.PanelDetails;
    //    //    lblInverterDetails.Text = st.InverterDetails;
    //    //    lblInstallCity.Text = st.InstallCity;
    //    //    lblInstallState.Text = st.InstallState;
    //    //    lblRoofType.Text = st.RoofType;
    //    //    DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectStock(ProjectID);
    //    //    if (dt.Rows.Count > 0)
    //    //    {
    //    //        Repeater1.DataSource = dt;
    //    //        Repeater1.DataBind();
    //    //    }
    //    //}
    //    BindGrid(0);
    //}

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "deduct")
        {

            string ProjectID = e.CommandArgument.ToString();
            //Response.Write(ProjectID);
            //Response.End();
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (st.IsDeduct == "False")
            {
                //DataTable dtHistory = ClstblStockItems.tblStockItemInventoryHistory_SelectByProjectId(ProjectID);
                //if (dtHistory.Rows.Count > 0)
                //{

                ModalPopupExtenderDeduct.Show();
                //Response.Write(ModalPopupExtenderDeduct);
                //Response.End();
                lblCustomer.Text = st.Customer;
                //Response.Write(st.Customer);
                //Response.End();
                lblProject.Text = st.Project;
                lblPanelDetails.Text = st.PanelDetails;
                lblInverterDetails.Text = st.InverterDetails;
                lblInstallCity.Text = st.InstallCity;
                lblInstallState.Text = st.InstallState;
                lblRoofType.Text = st.RoofType;
                txt_ExtraStock.Text = string.Empty;
                txtExtraStock1.Text = string.Empty;

                txtpallet.Text = string.Empty;
                //txtpallet.Value = string.Empty;
                SerialNoList.Visible = false;
                divActive.Visible = false;
                litremove.Visible = false;

                //txtProjectNo.Text = string.Empty;
                txtProjectNo.Text = st.ProjectNumber;
                SerialNoList2.Visible = false;
                litremove2.Visible = false;
                //savebtndiv.Enabled = false;
                //ibtnAdd.Disabled = true;   

                ListItem item6 = new ListItem();
                item6.Text = "Select";
                item6.Value = "";
                ddlStore.Items.Clear();
                ddlStore.Items.Add(item6);

                DataTable dt = ClstblCompanyLocations.tblCompanyLocations_SelectActive();
                ddlStore.DataSource = dt;
                ddlStore.DataValueField = "CompanyLocationID";
                ddlStore.DataMember = "CompanyLocation";
                ddlStore.DataTextField = "CompanyLocation";
                ddlStore.DataBind();

                if (st.StockAllocationStore != string.Empty)
                {
                    ddlStore.SelectedValue = st.StockAllocationStore;
                }

                lblStockItem.Text = st.PanelBrandName;
                txtQty.Text = st.NumberPanels;
                txtInvQty1.Text = st.inverterqty;
                txtInvQty2.Text = st.inverterqty2;
                if (st.InverterDetailsID != string.Empty)
                {
                    lblInv1.Text = st.InverterDetailsName;
                    trInv1.Visible = true;

                    //Response.Write(ddlStore.SelectedValue + "-" + st.InverterDetailsID);
                    //Response.End();
                    DataTable dt1 = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStore.SelectedValue, st.InverterDetailsID);
                    lblStock1.Text = dt1.Rows[0]["StockQuantity"].ToString();
                    if (st.Installer != string.Empty && st.InverterDetailsID != string.Empty && ddlStore.SelectedValue != string.Empty)
                    {
                        DataTable dtEXStock1 = ClstblExtraStock.tblExtraStock_SelectByInstallerID(st.Installer, st.InverterDetailsID, ddlStore.SelectedValue);
                        if (dtEXStock1.Rows.Count > 0)
                        {
                            //ListItem lst = new ListItem();
                            //lst.Text = "Extra Stock";
                            //lst.Value = "";
                            //ddlExtraStock1.Items.Add(lst);
                            txtExtraStock1.Text = string.Empty;
                            for (int i = 1; i <= Convert.ToInt32(dtEXStock1.Rows[0]["ExtraStock"].ToString()); i++)
                            {
                                // ddlExtraStock1.Items.Add(i.ToString());
                                txtExtraStock1.Text = i.ToString();
                            }
                        }
                    }
                }
                else
                {
                    trInv1.Visible = false;
                }
                if (st.SecondInverterDetailsID != "0")
                {
                    lblInv2.Text = st.SecondInverterDetails;
                    trInv2.Visible = true;
                    //Response.Write(ddlStore.SelectedValue+"==="+ st.SecondInverterDetailsID);
                    //Response.End();
                    DataTable dt2 = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStore.SelectedValue, st.SecondInverterDetailsID);
                    try
                    {
                        lblStock2.Text = dt2.Rows[0]["StockQuantity"].ToString();


                        DataTable dtEXStock2 = ClstblExtraStock.tblExtraStock_SelectByInstallerID(st.Installer, st.SecondInverterDetailsID, ddlStore.SelectedValue);

                        if (dtEXStock2.Rows.Count > 0)
                        {
                            //ListItem lst = new ListItem();
                            //lst.Text = "Extra Stock";
                            //lst.Value = "";
                            //ddlExtraStock2.Items.Add(lst);
                            txtExtraStock2.Text = string.Empty;
                            for (int i = 1; i <= Convert.ToInt32(dtEXStock2.Rows[0]["ExtraStock"].ToString()); i++)
                            {
                                //ddlExtraStock2.Items.Add(i.ToString());
                                txtExtraStock2.Text = i.ToString();
                            }
                        }
                    }
                    catch { }
                }
                else
                {
                    trInv2.Visible = false;
                }
                if (Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("WarehouseManager") || Roles.IsUserInRole("PreInstaller"))
                {
                    txtQty.Enabled = true;
                    txtInvQty1.Enabled = true;
                    txtInvQty2.Enabled = true;
                }
                else
                {
                    txtQty.Enabled = false;
                    txtInvQty1.Enabled = false;
                    txtInvQty2.Enabled = false;
                }
                if (ProjectID != string.Empty)
                {
                    if (ddlStore.SelectedValue != string.Empty)
                    {
                        DataTable dtStock = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStore.SelectedValue, st.PanelBrandID);
                        if (dtStock.Rows.Count > 0)
                        {
                            lblStock.Text = dtStock.Rows[0]["StockQuantity"].ToString();
                        }

                        if (st.Installer != string.Empty && st.PanelBrandID != string.Empty && ddlStore.SelectedValue != string.Empty)
                        {
                            DataTable dtEXStock = ClstblExtraStock.tblExtraStock_SelectByInstallerID(st.Installer, st.PanelBrandID, ddlStore.SelectedValue);

                            if (dtEXStock.Rows.Count > 0)
                            {
                                //ListItem lst = new ListItem();
                                //lst.Text = "Extra Stock";
                                //lst.Value = "";
                                //ddlExtraStock.Items.Add(lst);
                                txt_ExtraStock.Text = string.Empty;
                                for (int i = 1; i <= Convert.ToInt32(dtEXStock.Rows[0]["ExtraStock"].ToString()); i++)
                                {
                                    //ddlExtraStock.Items.Add(i.ToString());
                                    txt_ExtraStock.Text = i.ToString();
                                }
                            }
                        }
                    }
                }
                hndProjectID.Value = e.CommandArgument.ToString();
                Operationmode = "Add";
                MaxAttribute = 1;
                bindrepeater();
                BindAddedAttribute();
                //}
            }
            else
            {
                hndProjectID.Value = e.CommandArgument.ToString();
                ModalPopupExtenderDR.Show();

                lblCustomer2.Text = st.Customer;
                lblProject2.Text = st.Project;
                lblPanelDetails2.Text = st.PanelDetails;
                lblInverterDetails2.Text = st.InverterDetails;
                lblInstallCity2.Text = st.InstallCity;
                lblInstallState2.Text = st.InstallState;
                lblRoofType2.Text = st.RoofType;

                DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectStock(ProjectID);
                if (dt.Rows.Count > 0)
                {
                    rptDeductRev.DataSource = dt;
                    rptDeductRev.DataBind();
                }
            }
        }
        if (e.CommandName == "move")
        {
            string ProjectID = e.CommandArgument.ToString();
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            ModalPopupExtenderMove.Show();

            ListItem itemP = new ListItem();
            itemP.Text = "Select";
            itemP.Value = "";
            ddlProjectMove.Items.Clear();
            ddlProjectMove.Items.Add(itemP);

            DataTable dt = ClstblProjects.tblProjects_SelectMove(stPro.PanelBrandID, stPro.InverterDetailsID);
            if (dt.Rows.Count > 0)
            {
                //    ddlProjectMove.Items.Add(itemP);
                ddlProjectMove.DataSource = dt;
                ddlProjectMove.DataValueField = "ProjectID";
                ddlProjectMove.DataMember = "Project";
                ddlProjectMove.DataTextField = "Project";
                ddlProjectMove.DataBind();
            }

            ListItem itemI = new ListItem();
            itemI.Text = "Select";
            itemI.Value = "";
            ddlInstallerMove.Items.Clear();
            ddlInstallerMove.Items.Add(itemI);

            ddlInstallerMove.DataSource = ClstblContacts.tblContacts_SelectInverter();
            ddlInstallerMove.DataValueField = "ContactID";
            ddlInstallerMove.DataMember = "Contact";
            ddlInstallerMove.DataTextField = "Contact";
            ddlInstallerMove.DataBind();

            try
            {
                ddlInstallerMove.SelectedValue = stPro.Installer;
            }
            catch { }
            try
            {
                txtInstallerDateMove.Text = Convert.ToDateTime(stPro.InstallBookingDate).ToShortDateString();
            }
            catch { }
            hndProjectID.Value = ProjectID;
        }
        //if (e.CommandName == "allocatepanel")
        //{
        //    string ProjectID = e.CommandArgument.ToString();
        //    SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        //    ModalPopupExtender2.Show();
        //    lblCustomer3.Text = st.Customer;
        //    lblProject3.Text = st.Project;
        //    lblPanelDetails.Text = st.PanelDetails;
        //    lblInverterDetails.Text = st.InverterDetails;
        //    lblInstallCity.Text = st.InstallCity;
        //    lblInstallState.Text = st.InstallState;
        //    lblRoofType.Text = st.RoofType;
        //    DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectStock(ProjectID);
        //    if (dt.Rows.Count > 0)
        //    {
        //        Repeater1.DataSource = dt;
        //        Repeater1.DataBind();
        //    }
        //}
        BindGrid(0);
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void btnSearchPallet_Click(object sender, EventArgs e)
    {
        //Hide Project Number search repeater
        //txtProjectNo.Text = string.Empty;
        DataTable dt = new DataTable();
        SerialNoList2.Visible = false;
        litremove2.Visible = false;


        ModalPopupExtenderDeduct.Show();
        string ProjectID = hndProjectID.Value;
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string storelocation = ddlStore.SelectedItem.Value;
        string StockitemID = st.PanelBrandID;
        string PalletNo = txtpallet.Text;
        //string PalletNo = txtpallet.Value;
        string[] Pallets = PalletNo.Split(',');
        for (int i = 0; i < Pallets.Length; i++)
        {
            Pallets[i] = Pallets[i].Trim();
            DataTable dt2 = ClstblStockOrders.tblStockSerialNo_SearchByPallet(StockitemID, storelocation, Pallets[i]);
            dt.Merge(dt2);
        }
        if (dt.Rows.Count > 0)
        {
            countPanelNo = dt.Rows.Count;
            Repeater2.DataSource = dt;
            Repeater2.DataBind();
            SerialNoList.Visible = true;
            divActive.Visible = false;
            litremove.Visible = true;
            if (Convert.ToInt32(txtQty.Text) == dt.Rows.Count)
            {
                // savebtndiv.Enabled = true;
                //ibtnAdd.Disabled = false;
            }
            else
            {
                //  savebtndiv.Enabled = false;
                //ibtnAdd.Disabled = true;
            }
        }
        else
        {
            SerialNoList.Visible = false;
            Repeater2.DataSource = "";
            Repeater2.DataBind();
            divActive.Visible = true;
        }
    }

    protected void litremove_Click(object sender, EventArgs e)
    {

        // int countPanelNo = Repeater2.Items.Count;
        int panelQty = Convert.ToInt32(txtQty.Text);
        ModalPopupExtenderDeduct.Show();
        int srNo = 0;
        foreach (RepeaterItem ri in Repeater2.Items)
        {
            Label lblSrNo = ri.FindControl("lblSrNo") as Label;
            CheckBox chk = ri.FindControl("chkSerialNo") as CheckBox;
            if (ri.Visible == true)
            {
                srNo++;
                lblSrNo.Text = srNo.ToString();
                if (chk.Checked)
                {
                    countPanelNo--;
                    ri.Visible = false;
                    srNo--;
                }
            }
        }

        if (countPanelNo == 0)
        {
            SerialNoList.Visible = false;
            litremove.Visible = false;
            // savebtndiv.Enabled = false;
            //ibtnAdd.Disabled = true;
        }
        if (panelQty == countPanelNo)
        {
            //Response.Write("hello");
            //Response.End();
            //  savebtndiv.Enabled = true;
            //ibtnAdd.Disabled = false;
        }
        else
        {
            // savebtndiv.Enabled = false;
            //ibtnAdd.Disabled = true;
        }

    }

    protected void btnClearAll_Click(object sender, ImageClickEventArgs e)
    {
        ddllocationsearch.SelectedValue = "";
        ddlSearchDate.SelectedValue = "";
        ddlSearchState.SelectedValue = "";
        txtSerachCity.Text = string.Empty;
        txtSearch.Text = string.Empty;
        txtProjectNumber.Text = string.Empty;
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        BindGrid(0);
    }

    /* -------------------------
    protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView2.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    protected void GridView2_RowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView2.EditIndex = e.NewEditIndex;
        BindGrid(0);
    }
    protected void GridView2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView2.EditIndex = -1;
        BindGrid(0);
    }
    protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string ProjectID = GridView2.DataKeys[e.RowIndex].Value.ToString();
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectByProjectId(ProjectID);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string StockItemID = dt.Rows[i]["StockItemID"].ToString();
            string StockSize = dt.Rows[i]["Stock"].ToString();
            string StockLocation = dt.Rows[i]["CompanyLocationID"].ToString();

            decimal size = Math.Round(Convert.ToDecimal(StockSize));
            int myStock = (Convert.ToInt32(size)) * (-1);

            SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(StockItemID, StockLocation);
            ClstblStockItems.tblStockItemInventoryHistory_Insert("5", StockItemID, StockLocation, stOldQty.StockQuantity, Convert.ToString(myStock), userid, ProjectID);
            bool suc = ClsProjectSale.tblStockItems_RevertStock(StockItemID, Convert.ToString(myStock), StockLocation);
        }
        ClstblProjects.tblProjects_UpdateRevert(ProjectID, stEmp.EmployeeID, "0");
        ClstblExtraStock.tblExtraStock_DeleteByProjectID(ProjectID);

        GridView2.EditIndex = -1;
        BindGrid(0);
    }
    protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "viewproject")
        {
            string[] strids = e.CommandArgument.ToString().Split('|');
            string projectid = strids[0];
            string companyid = strids[1];
            Profile.eurosolar.companyid = companyid;
            Profile.eurosolar.projectid = projectid;
            Profile.eurosolar.contactid = "";
            if (e.CommandName == "viewproject")
            {
                Response.Redirect("~/admin/adminfiles/company/company.aspx?m=pro");
            }
        }
    }
    protected void btnSearch2_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }
    protected void btnClearAll2_Click(object sender, ImageClickEventArgs e)
    {
        ddllocationsearch2.SelectedValue = "";
        ddlSearchDate2.SelectedValue = "";
        ddlSearchState2.SelectedValue = "";
        txtSerachCity2.Text = string.Empty;
        txtSearch2.Text = string.Empty;
        txtProjectNumber2.Text = string.Empty;
        txtStartDate2.Text = string.Empty;
        txtEndDate2.Text = string.Empty;

        BindGrid(0);
    }
    ----------------------*/

    protected void GridView3_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView3.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    protected void btnSearch3_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }
    protected void btnClearAll3_Click(object sender, ImageClickEventArgs e)
    {
        ddlSearchDate3.SelectedValue = "";
        ddlSearchState3.SelectedValue = "";
        txtSerachCity3.Text = string.Empty;
        txtSearch3.Text = string.Empty;
        txtProjectNumber3.Text = string.Empty;
        txtStartDate3.Text = string.Empty;
        txtEndDate3.Text = string.Empty;
        ddllocationsearch3.SelectedValue = "";

        BindGrid(0);
    }

    protected void AddmoreAttribute()
    {
        MaxAttribute = MaxAttribute + 1;
        BindAddedAttribute();
        bindrepeater();
    }
    protected void BindAddedAttribute()
    {
        rpttable = new DataTable();
        rpttable.Columns.Add("StockItemID", Type.GetType("System.String"));
        rpttable.Columns.Add("StockQuantity", Type.GetType("System.String"));
        rpttable.Columns.Add("ExtraStock", Type.GetType("System.String"));

        foreach (RepeaterItem item in rptDeduct.Items)
        {
            HiddenField hndStockItemID = (HiddenField)item.FindControl("hndStockItemID");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            Label lblQty = (Label)item.FindControl("lblQty");
            TextBox txtExtraStock = (TextBox)item.FindControl("txtExtraStock");

            DataRow dr = rpttable.NewRow();
            dr["StockItemID"] = hndStockItemID.Value;
            dr["StockItemID"] = ddlStockItem.SelectedValue;
            dr["StockQuantity"] = lblQty.Text;
            dr["ExtraStock"] = txtExtraStock.Text;

            rpttable.Rows.Add(dr);
        }
    }
    protected void bindrepeater()
    {
        if (rpttable == null)
        {
            rpttable = new DataTable();
            rpttable.Columns.Add("StockItemID", Type.GetType("System.Int32"));
            rpttable.Columns.Add("StockQuantity", Type.GetType("System.Int32"));
            rpttable.Columns.Add("ExtraStock", Type.GetType("System.Int32"));
        }
        for (int i = rpttable.Rows.Count; i < MaxAttribute; i++)
        {
            DataRow dr = rpttable.NewRow();
            dr["StockItemID"] = i;
            dr["StockQuantity"] = DBNull.Value;
            dr["ExtraStock"] = DBNull.Value;
            try
            {
                rpttable.Rows.Add(dr);
            }
            catch { }
        }
        rptDeduct.DataSource = rpttable;
        rptDeduct.DataBind();
    }
    protected void rptDeduct_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (hndProjectID.Value != string.Empty)
            {
                SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(hndProjectID.Value);
                DropDownList ddlStockItem = (DropDownList)e.Item.FindControl("ddlStockItem");
                HiddenField hndStockItemID = (HiddenField)e.Item.FindControl("hndStockItemID");

                ListItem item6 = new ListItem();
                item6.Text = "Select";
                item6.Value = "";
                ddlStockItem.Items.Clear();
                ddlStockItem.Items.Add(item6);

                if (st.RoofTypeID != string.Empty)
                {
                    ddlStockItem.DataSource = ClstblStockItems.tblStockItems_SelectElectMount(ddlStore.SelectedValue);
                    ddlStockItem.DataTextField = "StockItem";
                    ddlStockItem.DataMember = "StockItem";
                    ddlStockItem.DataValueField = "StockItemID";
                    ddlStockItem.DataBind();
                }
                else
                {
                    ddlStockItem.DataSource = ClstblStockItems.tblStockItems_SelectElect(ddlStore.SelectedValue);
                    ddlStockItem.DataTextField = "StockItem";
                    ddlStockItem.DataMember = "StockItem";
                    ddlStockItem.DataValueField = "StockItemID";
                    ddlStockItem.DataBind();
                }
                ddlStockItem.SelectedValue = hndStockItemID.Value;
            }
        }
    }
    protected void ddlStore_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderDeduct.Show();
        string ProjectID = hndProjectID.Value;
        if (hndProjectID.Value != string.Empty)
        {
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if (ddlStore.SelectedValue != string.Empty)
            {
                if (st.PanelBrandID != string.Empty)
                {
                    DataTable dt = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStore.SelectedValue, st.PanelBrandID);
                    if (dt.Rows.Count > 0)
                    {
                        lblStock.Text = dt.Rows[0]["StockQuantity"].ToString();
                    }
                }
                if (st.InverterDetailsID != string.Empty)
                {
                    DataTable dt1 = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStore.SelectedValue, st.InverterDetailsID);
                    if (dt1.Rows.Count > 0)
                    {
                        lblStock1.Text = dt1.Rows[0]["StockQuantity"].ToString();
                    }
                }
                if (st.SecondInverterDetailsID != "0")
                {
                    DataTable dt2 = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStore.SelectedValue, st.SecondInverterDetailsID);
                    if (dt2.Rows.Count > 0)
                    {
                        lblStock2.Text = dt2.Rows[0]["StockQuantity"].ToString();
                    }
                }
            }

            foreach (RepeaterItem item in rptDeduct.Items)
            {
                DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
                Label lblQty = (Label)item.FindControl("lblQty");

                ListItem item6 = new ListItem();
                item6.Text = "Select";
                item6.Value = "";
                ddlStockItem.Items.Clear();
                ddlStockItem.Items.Add(item6);

                if (st.RoofTypeID != string.Empty)
                {
                    ddlStockItem.DataSource = ClstblStockItems.tblStockItems_SelectElectMount(ddlStore.SelectedValue);
                    ddlStockItem.DataTextField = "StockItem";
                    ddlStockItem.DataMember = "StockItem";
                    ddlStockItem.DataValueField = "StockItemID";
                    ddlStockItem.DataBind();
                }
                else
                {
                    ddlStockItem.DataSource = ClstblStockItems.tblStockItems_SelectElect(ddlStore.SelectedValue);
                    ddlStockItem.DataTextField = "StockItem";
                    ddlStockItem.DataMember = "StockItem";
                    ddlStockItem.DataValueField = "StockItemID";
                    ddlStockItem.DataBind();
                }

                DataTable dt = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStore.SelectedValue, ddlStockItem.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    lblQty.Text = dt.Rows[0]["StockQuantity"].ToString();
                }
                else
                {
                    lblQty.Text = "0";
                }
            }


            if (!string.IsNullOrEmpty(txtpallet.Text) && ddlStore.SelectedValue != "")
            {
                DataTable dt = new DataTable();
                string PalletNo = txtpallet.Text;
                //string PalletNo = txtpallet.Value;
                string[] Pallets = PalletNo.Split(',');
                for (int i = 0; i < Pallets.Length; i++)
                {
                    Pallets[i] = Pallets[i].Trim();
                    DataTable dt2 = ClstblStockOrders.tblStockSerialNo_SearchByPallet(st.PanelBrandID, ddlStore.SelectedValue, Pallets[i]);
                    dt.Merge(dt2);
                }
                //DataTable dt = ClstblStockOrders.tblStockSerialNo_SearchByPallet(st.PanelBrandID, ddlStore.SelectedValue, txtpallet.Value);
                if (dt.Rows.Count > 0)
                {
                    Repeater2.DataSource = dt;
                    Repeater2.DataBind();
                    countPanelNo = dt.Rows.Count;
                    if (dt.Rows.Count == Convert.ToInt32(txtQty.Text))
                    {
                        // savebtndiv.Enabled = true;
                        // ibtnAdd.Disabled = false;
                    }
                    else
                    {
                        // savebtndiv.Enabled = false;
                        //ibtnAdd.Disabled = true;
                    }
                    SerialNoList.Visible = true;
                    divActive.Visible = false;
                    divActive.Visible = false;
                    litremove.Visible = true;
                }
                else
                {
                    SerialNoList.Visible = false;
                    divActive.Visible = true;
                    divActive.Visible = true;
                    litremove.Visible = false;
                    // savebtndiv.Enabled = false;
                    //ibtnAdd.Disabled = true;
                }

            }
            else
            {

                SerialNoList.Visible = false;
                divActive.Visible = false;
                litremove.Visible = false;
                // savebtndiv.Enabled = false;
                // ibtnAdd.Disabled = true;
            }
        }
    }
    protected void btnAddRow_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderDeduct.Show();
        AddmoreAttribute();
    }
    protected void ddlStockItem_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderDeduct.Show();
        foreach (RepeaterItem item in rptDeduct.Items)
        {
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");
            Label lblQty = (Label)item.FindControl("lblQty");

            if (ddlStockItem.SelectedValue != string.Empty)
            {
                DataTable dt = ClstblStockItems.tblStockItems_SelectQtyByLocation(ddlStore.SelectedValue, ddlStockItem.SelectedValue);
                if (dt.Rows.Count > 0)
                {
                    lblQty.Text = dt.Rows[0]["StockQuantity"].ToString();
                }
            }
        }
    }

    protected void ibtnAdd_Click(object sender, EventArgs e)
    {

        bool sucStock = false;
        string ProjectID = hndProjectID.Value;
        ddlStore.Enabled = true;

        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string StockAllocationStore = ddlStore.SelectedValue;
        string StockSize = txtQty.Text;
        string PanelBrandID = st.PanelBrandID;
        string InverterDetailsID = st.InverterDetailsID;
        String CurrentDateTime = Convert.ToString(ClsAdminSiteConfiguration.GetDate_Aus(DateTime.Now));

        string InvStock1 = "1";

        if (txtInvQty1.Text.Trim() != string.Empty)
        {
            if (Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Administrator"))
            {
                InvStock1 = txtInvQty1.Text;
            }
        }
        else
        {
            InvStock1 = "1";
        }

        string SecondInverterDetailsID = st.SecondInverterDetailsID;
        string InvStock2 = "1";

        if (txtInvQty2.Text.Trim() != string.Empty)
        {
            if (Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Administrator"))
            {
                InvStock2 = txtInvQty2.Text;
            }
        }
        else
        {
            InvStock2 = "1";
        }

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        if (st.PanelBrandID != string.Empty)
        {
            sucStock = ClsProjectSale.tblStockItems_UpdateStock(PanelBrandID, StockSize, StockAllocationStore);
            SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(PanelBrandID, StockAllocationStore);
            //Response.Write(PanelBrandID+"==="+ StockAllocationStore + "==D=" + StOldQty.StockQuantity);
            //Response.End();
            int suc = ClstblStockItems.tblStockItemInventoryHistory_Insert("5", PanelBrandID, StockAllocationStore, StOldQty.StockQuantity, (0 - Convert.ToInt32(txtQty.Text)).ToString(), userid, ProjectID);
            string ExtraStock = "0";
            if (txtQty.Text != string.Empty)
            {
                ExtraStock = (Convert.ToInt32(txtQty.Text) - Convert.ToInt32(st.NumberPanels)).ToString();
            }
            if (Convert.ToInt32(ExtraStock) > 0)
            {
                ClstblExtraStock.tblExtraStock_Insert(ProjectID, st.Installer, st.PanelBrandID, ExtraStock, StockAllocationStore);
            }

            if (SerialNoList.Visible == true)
            {
                foreach (RepeaterItem ri in Repeater2.Items)
                {
                    Label lblpallet = (Label)ri.FindControl("lblPallet");
                    Label lblSerialNo = (Label)ri.FindControl("lblSerialNo");
                    if (ri.Visible == true)
                    {
                        ClstblStockOrders.tblStockDeductSerialNo_Insert(PanelBrandID, StockAllocationStore, lblSerialNo.Text, lblpallet.Text, st.ProjectNumber, stEmp.EmployeeID, CurrentDateTime, Convert.ToString(suc));
                    }
                }
            }
            if (SerialNoList2.Visible == true)
            {
                foreach (RepeaterItem ri in Repeater3.Items)
                {
                    Label lblSerialNo2 = (Label)ri.FindControl("lblSerialNo2");
                    if (ri.Visible == true)
                    {
                        ClstblStockOrders.tblStockDeductSerialNo_Insert(PanelBrandID, StockAllocationStore, lblSerialNo2.Text, "", st.ProjectNumber, stEmp.EmployeeID, CurrentDateTime, Convert.ToString(suc));
                    }
                }
            }


        }

        if (st.InverterDetailsID != string.Empty)
        {
            sucStock = ClsProjectSale.tblStockItems_UpdateStock(InverterDetailsID, InvStock1, StockAllocationStore);
            SttblStockItemsLocation StOldQty1 = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(InverterDetailsID, StockAllocationStore);
            ClstblStockItems.tblStockItemInventoryHistory_Insert("5", InverterDetailsID, StockAllocationStore, StOldQty1.StockQuantity, (0 - Convert.ToInt32(InvStock1)).ToString(), userid, ProjectID);
            string ExtraInv1 = "0";
            if (txtInvQty1.Text != string.Empty)
            {
                ExtraInv1 = (Convert.ToInt32(InvStock1) - Convert.ToInt32(1)).ToString();
            }
            if (Convert.ToInt32(ExtraInv1) > 0)
            {
                ClstblExtraStock.tblExtraStock_Insert(ProjectID, st.Installer, st.InverterDetailsID, ExtraInv1, StockAllocationStore);
            }
        }

        //Response.Write(InverterDetailsID + "mm " + st.SecondInverterDetailsID + "nnn");
        //Response.End();
        // if (SecondInverterDetailsID != "")
        if (st.SecondInverterDetailsID != "" && st.SecondInverterDetailsID != "0")
        {
            sucStock = ClsProjectSale.tblStockItems_UpdateStock(SecondInverterDetailsID, InvStock2, StockAllocationStore);
            SttblStockItemsLocation StOldQty2 = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(SecondInverterDetailsID, StockAllocationStore);

            if (StOldQty2.StockQuantity != "")
            {
                //Response.Write(SecondInverterDetailsID);
                //Response.End();
                ClstblStockItems.tblStockItemInventoryHistory_Insert("5", SecondInverterDetailsID, StockAllocationStore, StOldQty2.StockQuantity, (0 - Convert.ToInt32(InvStock2)).ToString(), userid, ProjectID);
            }

            string ExtraInv2 = "0";
            if (txtInvQty2.Text != string.Empty)
            {
                ExtraInv2 = (Convert.ToInt32(InvStock2) - Convert.ToInt32(1)).ToString();
            }
            if (Convert.ToInt32(ExtraInv2) > 0)
            {
                ClstblExtraStock.tblExtraStock_Insert(ProjectID, st.Installer, st.SecondInverterDetailsID, ExtraInv2, StockAllocationStore);
            }
        }

        if (txt_ExtraStock.Text != string.Empty)
        {
            DataTable dtEXStock = ClstblExtraStock.tblExtraStock_SelectByInstallerID(st.Installer, st.PanelBrandID, ddlStore.SelectedValue);
            int Stock = (Convert.ToInt32(dtEXStock.Rows[0]["ExtraStock"].ToString()) - Convert.ToInt32(txt_ExtraStock.Text));
            ClstblExtraStock.tblExtraStock_Update(ProjectID, st.Installer, st.PanelBrandID, Convert.ToString(Stock), ddlStore.SelectedValue);
        }
        if (txtExtraStock1.Text != string.Empty)
        {
            DataTable dtEXStock1 = ClstblExtraStock.tblExtraStock_SelectByInstallerID(st.Installer, st.InverterDetailsID, ddlStore.SelectedValue);
            int Stock = (Convert.ToInt32(dtEXStock1.Rows[0]["ExtraStock"].ToString()) - Convert.ToInt32(txtExtraStock1.Text));
            ClstblExtraStock.tblExtraStock_Update(ProjectID, st.Installer, st.InverterDetailsID, Convert.ToString(Stock), ddlStore.SelectedValue);
        }
        if (txtExtraStock2.Text != string.Empty)
        {
            DataTable dtEXStock2 = ClstblExtraStock.tblExtraStock_SelectByInstallerID(st.Installer, st.SecondInverterDetailsID, ddlStore.SelectedValue);
            int Stock = (Convert.ToInt32(dtEXStock2.Rows[0]["ExtraStock"].ToString()) - Convert.ToInt32(txtExtraStock2.Text));
            ClstblExtraStock.tblExtraStock_Update(ProjectID, st.Installer, st.SecondInverterDetailsID, Convert.ToString(Stock), ddlStore.SelectedValue);
        }
        foreach (RepeaterItem item in rptDeduct.Items)
        {
            TextBox txtExtraStock = (TextBox)item.FindControl("txtExtraStock");
            DropDownList ddlStockItem = (DropDownList)item.FindControl("ddlStockItem");

            if (ddlStockItem.SelectedValue != string.Empty)
            {
                SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(ddlStockItem.SelectedValue, StockAllocationStore);
                ClstblStockItems.tblStockItemInventoryHistory_Insert("5", ddlStockItem.SelectedValue, StockAllocationStore, stOldQty.StockQuantity, (0 - Convert.ToInt32(txtExtraStock.Text)).ToString(), userid, ProjectID);
                string Stock = txtExtraStock.Text;
                string StockItemID = ddlStockItem.SelectedValue;
                bool suc = ClsProjectSale.tblStockItems_UpdateStock(StockItemID, Stock, ddlStore.SelectedValue);
            }
        }
        /* ---------------------- Stock Assigned ---------------------- */
        if (st.ProjectStatusID == "11")
        {
            int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("12", ProjectID, stEmp.EmployeeID, st.NumberPanels);
            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "12");
        }
        /* ------------------------------------------------------------ */
        ClstblProjects.tblProjects_UpdateDeduct(ProjectID, stEmp.EmployeeID, "1");
        ClsProjectSale.tblProjects_UpdateStockAllocationStore(ProjectID, StockAllocationStore);
        ClstblProjects.tblProjects_UpdateStockDeductDate(ProjectID, DateTime.Now.AddHours(14).ToString());

        ModalPopupExtenderDeduct.Hide();
        BindGrid(0);
    }

    protected void btnDeduct_Click(object sender, EventArgs e)
    {
        string ProjectID = hndProjectID.Value;
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string StockAllocationStore = st.StockAllocationStore;
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();

        foreach (RepeaterItem item in rptDeductRev.Items)
        {
            TextBox txtDeductRevert = (TextBox)item.FindControl("txtDeductRevert");
            HiddenField hndStockItemID = (HiddenField)item.FindControl("hndStockItemID");

            string Stock = txtDeductRevert.Text.Trim();
            string StockItemID = hndStockItemID.Value;

            if (txtDeductRevert.Text.Trim() != string.Empty)
            {
                SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(hndStockItemID.Value, StockAllocationStore);
                ClstblStockItems.tblStockItemInventoryHistory_Insert("5", StockItemID, StockAllocationStore, stOldQty.StockQuantity, (0 - Convert.ToInt32(txtDeductRevert.Text.Trim())).ToString(), userid, ProjectID);
                bool suc = ClsProjectSale.tblStockItems_UpdateStock(StockItemID, Stock, StockAllocationStore);
            }
        }
        ClstblProjects.tblProjects_UpdateStockDeductDate(ProjectID, DateTime.Now.AddHours(14).ToString());
    }
    protected void btnRevert_Click(object sender, EventArgs e)
    {
        string ProjectID = hndProjectID.Value;
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string StockAllocationStore = st.StockAllocationStore;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        foreach (RepeaterItem item in rptDeductRev.Items)
        {
            TextBox txtDeductRevert = (TextBox)item.FindControl("txtDeductRevert");
            HiddenField hndStockItemID = (HiddenField)item.FindControl("hndStockItemID");

            string Stock = txtDeductRevert.Text.Trim();
            string StockItemID = hndStockItemID.Value;

            if (txtDeductRevert.Text.Trim() != string.Empty)
            {
                SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(StockItemID, StockAllocationStore);
                int suc1 = ClstblStockItems.tblStockItemInventoryHistory_Insert("5", StockItemID, StockAllocationStore, stOldQty.StockQuantity, Stock, userid, ProjectID);
                ClstblStockItems.tblStockItemInventoryHistory_UpdateRevert(suc1.ToString(), "1", DateTime.Now.AddHours(14).ToString());
                bool suc = ClsProjectSale.tblStockItems_RevertStock(StockItemID, Stock, StockAllocationStore);
            }
        }
        // ClstblProjects.tblProjects_UpdateStockDeductDate(ProjectID,"");
        //  ClstblProjects.tblProjects_UpdateStockDeductBy(ProjectID, "");
        ModalPopupExtender3.Hide();
        BindGrid(0);
    }
    protected void btnRevertAll_Click(object sender, EventArgs e)
    {
        string ProjectID = hndProjectID.Value;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectStock(ProjectID);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            string StockItemID = dt.Rows[i]["StockItemID"].ToString();
            string StockSize = dt.Rows[i]["Stock"].ToString();
            string StockLocation = st.StockAllocationStore;

            decimal size = Math.Round(Convert.ToDecimal(StockSize));
            int myStock = (Convert.ToInt32(size)) * (-1);

            SttblStockItemsLocation stOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(StockItemID, StockLocation);
            int suc1 = ClstblStockItems.tblStockItemInventoryHistory_Insert("5", StockItemID, StockLocation, stOldQty.StockQuantity, Convert.ToString(myStock), userid, ProjectID);
            ClstblStockItems.tblStockItemInventoryHistory_UpdateRevert(suc1.ToString(), "2", DateTime.Now.AddHours(14).ToString());
            bool suc = ClsProjectSale.tblStockItems_RevertStock(StockItemID, Convert.ToString(myStock), StockLocation);
        }
        ClstblProjects.tblProjects_UpdateRevert(ProjectID, stEmp.EmployeeID, "0");
        ClstblProjects.tblProjects_UpdateStockDeductDate(ProjectID, "");
        ClstblProjects.tblProjects_UpdateStockDeductBy(ProjectID, "");
        ClstblExtraStock.tblExtraStock_DeleteByProjectID(ProjectID);

        /* -------------------- Update Project Status -------------------- */
        if (st.InstallBookingDate != string.Empty)
        {
            int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("11", ProjectID, stEmp.EmployeeID, st.NumberPanels);
            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "11");
        }
        else
        {
            int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("3", ProjectID, stEmp.EmployeeID, st.NumberPanels);
            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "3");
        }
        ModalPopupExtender3.Hide();
        /* -------------------------------------------------------------- */
        BindGrid(0);
    }
    protected void btnMove_Click(object sender, EventArgs e)
    {
        string ProjectID = hndProjectID.Value;
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        if (ddlProjectMove.SelectedValue != string.Empty)
        {
            ClstblStockItems.tblStockItemInventoryHistory_UpdateMove(ProjectID, ddlProjectMove.SelectedValue, userid);
            ClstblProjects.tblProjects_UpdateDeduct(ddlProjectMove.SelectedValue, stEmp.EmployeeID, "1");
            ClstblProjects.tblProjects_UpdateDeduct(ProjectID, stEmp.EmployeeID, "0");
            ClstblProjects.tblProjects_UpdateMove(ddlProjectMove.SelectedValue, ddlInstallerMove.SelectedValue, txtInstallerDateMove.Text.Trim());
            ClstblProjects.tblProjects_UpdateMove(ProjectID, "", "");
        }
        BindGrid(0);
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData1();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected void GridView3_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData3();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView3.DataSource = sortedView;
        GridView3.DataBind();
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);

    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    protected void GridView3_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView3.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView3.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView3.PageIndex - 2;
        page[1] = GridView3.PageIndex - 1;
        page[2] = GridView3.PageIndex;
        page[3] = GridView3.PageIndex + 1;
        page[4] = GridView3.PageIndex + 2;
        page[5] = GridView3.PageIndex + 3;
        page[6] = GridView3.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView3.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView3.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView3.PageIndex == GridView3.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv3.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv3.ToTable().Rows.Count;
            int iEndRecord = GridView3.PageSize * (GridView3.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView3.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    void lb3_Command(object sender, CommandEventArgs e)
    {
        GridView3.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }
    protected void GridView3_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb3_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb3_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb3_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb3_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb3_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb3_Command);
        }
    }

    protected void ddlSelectRecords4_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords4.SelectedValue) == "All")
        {
            GridView2.AllowPaging = false;
            BindGrid(0);
        }


        else if (Convert.ToString(ddlSelectRecords4.SelectedValue) == "")
        {
            GridView2.AllowPaging = false;
        }
        else
        {
            GridView2.AllowPaging = true;
            GridView2.PageSize = Convert.ToInt32(ddlSelectRecords4.SelectedValue);
            BindGrid(0);
        }
    }
    protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView2.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt4 = new DataTable();
        dt4 = GetGridData4();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt4);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView2.DataSource = sortedView;
        GridView2.DataBind();
    }
    protected void GridView2_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView2.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView2.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView2.PageIndex - 2;
        page[1] = GridView2.PageIndex - 1;
        page[2] = GridView2.PageIndex;
        page[3] = GridView2.PageIndex + 1;
        page[4] = GridView2.PageIndex + 2;
        page[5] = GridView2.PageIndex + 3;
        page[6] = GridView2.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView2.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView2.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView2.PageIndex == GridView2.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv4.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv4.ToTable().Rows.Count;
            int iEndRecord = GridView2.PageSize * (GridView2.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView2.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    protected void GridView2_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command4);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command4);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command4);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command4);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command4);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command4);
        }
    }
    void lb_Command4(object sender, CommandEventArgs e)
    {
        GridView2.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);
    }
    protected void btnSearch4_Click(object sender, EventArgs e)
    {
        BindGrid(0);
    }
    protected void btnClearAll4_Click(object sender, ImageClickEventArgs e)
    {
        ddlSearchDate4.SelectedValue = "";
        ddlSearchState4.SelectedValue = "";
        txtSerachCity4.Text = string.Empty;
        txtSearch4.Text = string.Empty;
        txtProjectNumber4.Text = string.Empty;
        txtStartDate4.Text = string.Empty;
        txtEndDate4.Text = string.Empty;
        ddllocationsearch4.SelectedValue = "";

        BindGrid(0);
    }
    protected void lnkReceived_Click(object sender, EventArgs e)
    {
        LinkButton lnkReceived = (LinkButton)sender;
        GridViewRow item1 = lnkReceived.NamingContainer as GridViewRow;
        HiddenField hdnstockid = (HiddenField)item1.FindControl("hdnstockid");

        DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectByProjectId(hdnstockid.Value);
        if (dt.Rows.Count > 0)
        {
            hdnstocktransferid.Value = hdnstockid.Value;
            ModalPopupExtender1.Show();
            divstockdetail.Visible = true;
            divdetail.Visible = true;
            rptstockdetail.DataSource = dt;
            rptstockdetail.DataBind();
            divdetailmsg.Visible = false;
        }
        else
        {
            divdetailmsg.Visible = true;
            ModalPopupExtender1.Show();
            divstockdetail.Visible = true;
        }

    }

    protected void Button3_Click(object sender, EventArgs e)
    {
        hdnstocktransferid.Value = "";
        ModalPopupExtender1.Hide();
        divstockdetail.Visible = false;
        rptstockdetail.DataSource = null;
        rptstockdetail.DataBind();
        BindGrid(0);
        //BindScript();

        divdetailmsg.Visible = false;
        divdetail.Visible = false;

        divright.Visible = true;
    }
    //protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        Label lblAllocatePanels = (Label)e.Row.FindControl("lblAllocatePanels");
    //        HiddenField hdnProjectID = (HiddenField)e.Row.FindControl("hdnProjectID");
    //        SttblProjects stpro = ClstblProjects.tblProjects_SelectByProjectID(hdnProjectID.Value);
    //        if (stpro.StockDeductDate != string.Empty)
    //        {
    //            DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectStock(hdnProjectID.Value);
    //            if (dt.Rows.Count > 0)
    //            {
    //                decimal deduct = 0;
    //                foreach (DataRow dr in dt.Rows)
    //                {
    //                    deduct += Convert.ToDecimal(dr["Deduct"].ToString());
    //                }
    //                lblAllocatePanels.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0,0}", deduct);
    //            }
    //            //lblAllocatePanels.Text = stpro.NumberPanels;
    //        }

    //    }
    //}
    protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hndProjectID = (HiddenField)e.Row.FindControl("hndProjectID");
            Literal ltdeductpanels = (Literal)e.Row.FindControl("ltdeductpanels");
            Literal ltallocatedpanels = (Literal)e.Row.FindControl("ltallocatedpanels");
            Label ltremainingpanels = (Label)e.Row.FindControl("ltremainingpanels");
            Label lblrevert = (Label)e.Row.FindControl("lblrevert");
            //Button btnRevertStock = (Button)e.Row.FindControl("btnRevertStock");
            HiddenField hdnStockItem = (HiddenField)e.Row.FindControl("hdnStockItem");
            LinkButton lbtnDeduct = (LinkButton)e.Row.FindControl("lbtnDeduct");

            SttblProjects stpro = ClstblProjects.tblProjects_SelectByProjectID(hndProjectID.Value);
            ltallocatedpanels.Text = stpro.NumberPanels;
            DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectStock(hndProjectID.Value);
            if (dt.Rows.Count > 0)
            {
                decimal deduct = 0;
                decimal revert = 0;
                DataTable dt1 = ClstblStockItems.tblStockItemInventoryHistory_ProjectIDStockItemID(hndProjectID.Value, hdnStockItem.Value);
                deduct = Convert.ToDecimal(dt1.Rows[0]["Stock"].ToString());
                //Response.Write(hndProjectID.Value+"=="+ hdnStockItem.Value);
                //Response.End();
                DataTable dt2 = ClstblStockItems.tblStockItemInventoryHistory_revert(hndProjectID.Value, hdnStockItem.Value);
                if (dt2.Rows.Count > 0)
                {
                    revert = Convert.ToDecimal(dt2.Rows[0]["Stock"].ToString());
                }
                // lblrevert.Text = dt1.Rows[0]["stock1"].ToString();///tblStockItemInventoryHistory_revert
                // Response.Write(dt1.Rows[0]["RevertFlag"].ToString());
                foreach (DataRow dr in dt1.Rows)
                {
                    // Response.Write(dr["RevertFlag"].ToString());
                    //if (dr["RevertFlag"].ToString() != true.ToString())
                    //{
                    //    revert = Convert.ToDecimal(dr["stock1"].ToString());
                    //}
                    //if (dt.Rows[0]["StockItemID"].ToString() == hdnStockItem.Value)
                    if (dr["StockItemID"].ToString() == hdnStockItem.Value)
                    {
                        //  deduct += Convert.ToDecimal(dr["Stock"].ToString());
                    }
                }

                //Response.Write(hndProjectID.Value + "=" + hdnStockItem.Value + "=" + deduct);
                deduct = System.Math.Abs(deduct);
                revert = System.Math.Abs(revert);
                //if (hndProjectID.Value == "-331651726")
                //{
                //    Response.Write("-->" + dt.Rows[0]["StockItem"].ToString() + "=" + hdnStockItem.Value + "=" + deduct + "<br/>");
                //}
                //Response.End();

                //Response.End();
                //if (dt.Rows[0]["deduct"].ToString() != string.Empty)
                //{
                //    deduct = Convert.ToDecimal(dt.Rows[0]["deduct"].ToString());
                //}


                ltdeductpanels.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0}", deduct);
                lblrevert.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0}", revert);
                if (Convert.ToInt32(ltdeductpanels.Text) == 0)
                {
                    lbtnDeduct.Visible = false;
                }
                decimal noofpanels = 0;
                if (stpro.NumberPanels != string.Empty)
                {
                    noofpanels = Convert.ToDecimal(stpro.NumberPanels);
                }
                if (deduct >= 0)
                {
                    deduct = deduct;
                }
                // decimal remain = (Convert.ToDecimal(ltallocatedpanels.Text) - Convert.ToDecimal(ltdeductpanels.Text));
                decimal remain = noofpanels - deduct + revert;
                ltremainingpanels.Text = remain.ToString();
                if (remain < 0)
                {
                    ltremainingpanels.ForeColor = System.Drawing.Color.Red;
                }

                //if (remain < 0)
                //{
                //    ltremainingpanels.ForeColor = System.Drawing.Color.Red;
                //    btnRevertStock.Visible = true;
                //}
                //else
                //{
                //    if (remain == 0)
                //    {
                //        btnRevertStock.Visible = false;
                //    }
                //    else
                //    {
                //        btnRevertStock.Visible = false;
                //    }
                //}
            }
        }
    }
    protected void GridView3_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        // Response.Write(e.CommandName);
        Response.End();
        if (e.CommandName == "RevertStock")
        {

            decimal remain = 0;
            int deduct1 = 0;
            string ID = e.CommandArgument.ToString(); //projectID

            SttblProjects stpro = ClstblProjects.tblProjects_SelectByProjectID(ID);
            DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectStock(ID);
            if (dt.Rows.Count > 0)
            {

                decimal deduct = 0;
                //foreach (DataRow dr in dt.Rows)
                //{
                //    deduct += Convert.ToDecimal(dr["Deduct"].ToString());
                //}
                if (dt.Rows[0]["deduct"].ToString() != string.Empty)
                {
                    deduct = Convert.ToDecimal(dt.Rows[0]["deduct"].ToString());
                }
                //ltdeductpanels.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0}", deduct);


                remain = (Convert.ToInt32(deduct) - Convert.ToInt32(stpro.NumberPanels));
                deduct1 = Convert.ToInt32(stpro.NumberPanels) - Convert.ToInt32(remain);

                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblStockItemsLocation StOldQty = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(dt.Rows[0]["StockItemID"].ToString(), dt.Rows[0]["CompanyLocationID"].ToString());

                int stocksucc = ClstblStockItems.tblStockItemInventoryHistory_Insert("5", dt.Rows[0]["StockItemID"].ToString(), dt.Rows[0]["CompanyLocationID"].ToString(), StOldQty.StockQuantity, remain.ToString(), userid, dt.Rows[0]["ProjectID"].ToString());
                ClstblStockItems.tblStockItemInventoryHistory_UpdateRevert(stocksucc.ToString(), "1", DateTime.Now.AddHours(14).ToString());


                SttblStockItemsLocation stLocationFrom = ClstblStockItemsLocation.tblStockItemsLocation_SelectByStockItemID(dt.Rows[0]["StockItemID"].ToString(), dt.Rows[0]["CompanyLocationID"].ToString());
                string qty1 = (Convert.ToInt32(stLocationFrom.StockQuantity) + (Convert.ToInt32(remain))).ToString();


                ClstblStockItemsLocation.tblStockItemsLocation_UpdatebyStockItemID_CompanyLocationID(dt.Rows[0]["StockItemID"].ToString(), dt.Rows[0]["CompanyLocationID"].ToString(), qty1);



            }


            //bool s =ClstblProjects.tblProjects_UpdateNumberPanels(ID, deduct1.ToString());

        }
        if (e.CommandName == "deduct")
        {

            string ProjectID = e.CommandArgument.ToString();
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            hndProjectID.Value = e.CommandArgument.ToString();
            ModalPopupExtender3.Show();

            lblCustomer6.Text = st.Customer;
            lblProject6.Text = st.Project;
            lblPanelDetails6.Text = st.PanelDetails;
            lblInverterDetails6.Text = st.InverterDetails;
            lblInstallCity6.Text = st.InstallCity;
            lblInstallState6.Text = st.InstallState;
            lblRoofType6.Text = st.RoofType;

            DataTable dt = ClstblStockItems.tblStockItemInventoryHistory_SelectStock(ProjectID);
            if (dt.Rows.Count > 0)
            {
                rptDeductRev6.DataSource = dt;
                rptDeductRev6.DataBind();
            }
            BindScript();
        }
        BindGrid(0);
    }
    public void BindScript()
    {
        //ScriptManager.RegisterStartupScript(Update_panel, this.GetType(), "MyAction", "doMyAction();", true);

    }
    protected void lbtnExport1_Click(object sender, EventArgs e)
    {
        DataTable dt = ClstblProjects.tblProjects_SelectWarehouse("", txtSearch.Text, txtProjectNumber.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, ddllocationsearch.SelectedValue, chkHistoric.Checked.ToString());
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "StockDeduct" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 11, 14, 301, 299, 52, 300 };
            string[] arrHeader = { "ProjectNumber", "Project", "InstallBookingDate", "Installer", "Details", "Store Name" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void lbtnExport2_Click(object sender, EventArgs e)
    {
        // DataTable dt = ClstblProjects.tblProjects_SelectWarehouse("", txtSearch.Text, txtProjectNumber.Text, txtSerachCity.Text, ddlSearchState.SelectedValue, txtStartDate.Text, txtEndDate.Text, ddlSearchDate.SelectedValue, ddllocationsearch.SelectedValue, chkHistoric.Checked.ToString());
        string selectedItem = "";
        foreach (RepeaterItem item in lstSearchStatus.Items)
        {
            CheckBox chkselect = (CheckBox)item.FindControl("chkselect");
            HiddenField hdnID = (HiddenField)item.FindControl("hdnID");

            if (chkselect.Checked == true)
            {
                selectedItem += "," + hdnID.Value.ToString();
            }
        }
        if (selectedItem != "")
        {
            selectedItem = selectedItem.Substring(1);
        }
        DataTable dt3 = ClstblProjects.tblProjects_SelectWarehouseAllocated("1", txtSearch3.Text, txtProjectNumber3.Text, txtSerachCity3.Text, ddlSearchState3.SelectedValue, txtStartDate3.Text, txtEndDate3.Text, ddlSearchDate3.SelectedValue, ddllocationsearch3.SelectedValue, selectedItem);
        Response.Clear();
        try
        {
            Export oExport = new Export();
            string FileName = "StockAllocated" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 304, 11, 302, 303, 299, 53, 52, 300 };
            string[] arrHeader = { "DeductDate", "P.No.", "P.Status", "InstallDate", "Installer", "N.Panels", "Details", "Store Name" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt3, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }
    protected void lbtnExport3_Click(object sender, EventArgs e)
    {
        DataTable dt5 = ClstblProjects.tblProjects_StockRevert("1", txtSearch3.Text, txtProjectNumber3.Text, txtSerachCity3.Text, ddlSearchState3.SelectedValue, txtStartDate3.Text, txtEndDate3.Text, ddlSearchDate3.SelectedValue, ddllocationsearch3.SelectedValue, ddlRevert.SelectedValue);
        // Response.Clear();
        //Response.Write(dt5.Rows.Count);
        //Response.End();
        try
        {
            Export oExport = new Export();
            string FileName = "StockRevert" + System.DateTime.Now.AddHours(14).ToString("dd-MMM-yy ss") + ".xls";
            int[] ColList = { 2, 3, 6, 5 };
            string[] arrHeader = { "Project No.", "Project", "InstallBookingDate", "Details" };
            //only change file extension to .xls for excel file
            oExport.ExportDetails(dt5, ColList, arrHeader, Export.ExportFormat.Excel, FileName);
        }
        catch (Exception Ex)
        {
            //   lblError.Text = Ex.Message;
        }
    }

    protected void lblexport7_Click(object sender, EventArgs e)
    {

    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }



    protected void btnaddpanel_Click(object sender, EventArgs e)
    {
        if (PanelFileUpload.HasFile)
        {

            string RandomNumber = ClsAdminSiteConfiguration.RandomNumber();
            PanelFileUpload.SaveAs(Request.PhysicalApplicationPath + "\\userfiles" + "\\StockDeductPanel\\" + RandomNumber + "_" + PanelFileUpload.FileName);
            string connectionString = "";

            if (PanelFileUpload.FileName.EndsWith(".xls"))
            {
                connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\StockDeductPanel\\" + RandomNumber + "_" + PanelFileUpload.FileName) + ";Extended Properties='Excel 8.0;HDR=YES;IMEX=1'";
            }
            else if (PanelFileUpload.FileName.EndsWith(".xlsx"))
            {
                connectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + (Request.PhysicalApplicationPath + "userfiles\\StockDeductPanel\\" + RandomNumber + "_" + PanelFileUpload.FileName) + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1'";

            }


            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            using (DbConnection connection = factory.CreateConnection())
            {
                connection.ConnectionString = connectionString;
                connection.Open();

                using (DbCommand command = connection.CreateCommand())
                {
                    // Cities$ comes from the name of the worksheet

                    command.CommandText = "SELECT * FROM [Sheet1$]";
                    command.CommandType = CommandType.Text;

                    using (DbDataReader dr = command.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                string ProjectNo = "";
                                string Serial_No = "";

                                Serial_No = dr["SerialNo"].ToString();
                                ProjectNo = dr["ProjectNumber"].ToString();

                                if (!string.IsNullOrEmpty(Serial_No) && !string.IsNullOrEmpty(ProjectNo))
                                {
                                    //count++;
                                    int success = ClstblStockOrders.tblStockSerialNoByProjNo_Insert(ProjectNo, Serial_No);
                                    if (success > 0)
                                    {
                                        SetAdd1();
                                    }
                                    else
                                    {
                                        SetError1();
                                    }
                                }
                                else
                                {
                                    SetError1();
                                }

                            }
                        }
                    }
                }
            }
        }
    }

    protected void txtQty_SelectedTextChange(object sender, EventArgs e)
    {

        ModalPopupExtenderDeduct.Show();
        string ChangedQty;
        //SerialNoList.Visible = false;
        //litremove.Visible = false;
        //ibtnAdd.Disabled = true;
        if (!string.IsNullOrEmpty(txtQty.Text))
        {
            if (Convert.ToInt32(txtQty.Text) <= Convert.ToInt32(lblStock.Text))
            {
                ChangedQty = txtQty.Text;
                lblerrmsg.Text = "";
                txtQty.Style.Add("border-color", "#B5B5B5");
            }
            else
            {
                lblerrmsg.Text = "Enter valid Qty.";
                ChangedQty = "-10000";
                txtQty.Style.Add("border-color", "#FF5F5F");
            }
        }
        else
        {
            lblerrmsg.Text = "Qty can't be Empty.";
            ChangedQty = "-10000";
            txtQty.Style.Add("border-color", "#FF5F5F");
        }


        if (Repeater2.Items.Count == Convert.ToInt32(ChangedQty))
        {

           //  ibtnAdd.Disabled = false;
        }
        else
        {

            // ibtnAdd.Disabled = true;
        }

    }

    protected void txtInvQty1_SelectedTextChange(object sender, EventArgs e)
    {

        ModalPopupExtenderDeduct.Show();
        ibtnAdd.Disabled = true;
        if (!string.IsNullOrEmpty(txtInvQty1.Text))
        {
            if (Convert.ToInt32(txtInvQty1.Text) <= Convert.ToInt32(lblStock1.Text))
            {
               // ibtnAdd.Disabled = false;
                lblerrmsg2.Text = "";
                txtInvQty1.Style.Add("border-color", "#B5B5B5");

            }
            else
            {
                lblerrmsg2.Text = "Enter valid Qty.";
               // ibtnAdd.Disabled = false;
                txtInvQty1.Style.Add("border-color", "#FF5F5F");
            }
        }
        else
        {
            lblerrmsg2.Text = "Qty can't be Empty.";
           // ibtnAdd.Disabled = true;
            txtInvQty1.Style.Add("border-color", "#FF5F5F");
        }
    }

    protected void txtInvQty2_SelectedTextChange(object sender, EventArgs e)
    {

        ModalPopupExtenderDeduct.Show();
        ibtnAdd.Disabled = true;
        if (!string.IsNullOrEmpty(txtInvQty2.Text))
        {
            if (Convert.ToInt32(txtInvQty2.Text) <= Convert.ToInt32(lblStock2.Text))
            {
               // ibtnAdd.Disabled = false;
                lblerrmsg3.Text = "";
                txtInvQty2.Style.Add("border-color", "#B5B5B5");

            }
            else
            {
                lblerrmsg3.Text = "Enter valid Qty.";
                //ibtnAdd.Disabled = false;
                txtInvQty2.Style.Add("border-color", "#FF5F5F");
            }
        }
        else
        {
            lblerrmsg3.Text = "Qty can't be Empty.";
           // ibtnAdd.Disabled = true;
            txtInvQty2.Style.Add("border-color", "#FF5F5F");
        }
    }

    protected void btnSearchProjectNo_Click(object sender, EventArgs e)
    {
        //hide pallet search repeater
        txtpallet.Text = string.Empty;
        //txtpallet.Value = string.Empty;
        SerialNoList.Visible = false;
        litremove.Visible = false;

        ModalPopupExtenderDeduct.Show();
        string ProjectID = hndProjectID.Value;
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string storelocation = ddlStore.SelectedItem.Value;
        string StockitemID = st.PanelBrandID;
        string ProjectNumber = txtProjectNo.Text;
        DataTable dt = ClstblStockOrders.tblStockSerialNoByProjNo_SearchByProjNo(ProjectNumber);
        if (dt.Rows.Count > 0)
        {
            countSerialNo = dt.Rows.Count;
            Repeater3.DataSource = dt;
            Repeater3.DataBind();
            SerialNoList2.Visible = true;
            divActive.Visible = false;
            litremove2.Visible = true;
            if (Convert.ToInt32(txtQty.Text) == dt.Rows.Count)
            {
                // savebtndiv.Enabled = true;
                //ibtnAdd.Disabled = false;
            }
            else
            {
                //  savebtndiv.Enabled = false;
                //ibtnAdd.Disabled = true;
            }
        }
        else
        {
            divActive.Visible = true;
        }
    }

    protected void litremove2_Click(object sender, EventArgs e)
    {
        // int countPanelNo = Repeater2.Items.Count;
        int panelQty = Convert.ToInt32(txtQty.Text);
        ModalPopupExtenderDeduct.Show();
        int srNo = 0;
        foreach (RepeaterItem ri in Repeater3.Items)
        {

            Label lblSrNo = ri.FindControl("lblSrNo2") as Label;
            CheckBox chk = ri.FindControl("chkSerialNo2") as CheckBox;
            if (ri.Visible == true)
            {
                srNo++;
                lblSrNo.Text = srNo.ToString();
                if (chk.Checked)
                {
                    countSerialNo--;
                    ri.Visible = false;
                    srNo--;
                }
            }
        }

        if (countSerialNo == 0)
        {
            SerialNoList2.Visible = false;
            litremove2.Visible = false;
            // savebtndiv.Enabled = false;
            // ibtnAdd.Disabled = true;
        }
        if (panelQty == countSerialNo)
        {
            //Response.Write("hello");
            //Response.End();
            //  savebtndiv.Enabled = true;
            // ibtnAdd.Disabled = false;
        }
        else
        {
            // savebtndiv.Enabled = false;
            // ibtnAdd.Disabled = true;
        }
    }
}