﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mailtemplate_refundprint : System.Web.UI.Page
{
    public static string MakeImageSrcData(string filename)
    {
        FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
        byte[] filebytes = new byte[fs.Length];
        fs.Read(filebytes, 0, Convert.ToInt32(fs.Length));
        return "data:image/png;base64," +
          Convert.ToBase64String(filebytes, Base64FormattingOptions.None);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string id = Request.QueryString["id"];

            SttblStockTransfers st = ClstblStockTransfers.tblStockTransfers_SelectByStockTransferID(id);
            lblFrom.Text = st.FromLocation;
            lblTo.Text = st.ToLocation;
            try
            {
                lblTransferDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.TransferDate));
            }
            catch { }
            lblTransferBy.Text = st.TransferByName;

            lblTrackingNo.Text = st.TrackingNumber;
            try
            {
                lblReceivedDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.ReceivedDate));
            }
            catch { }
            lblReceivedBy.Text = st.ReceivedByName;
            lblNotes.Text = st.TransferNotes;

            DataTable dt = ClstblStockTransfers.tblStockTransferItems_SelectByStockTransferID(id);
            if (dt.Rows.Count > 0)
            {

                rptItems.DataSource = dt;
                rptItems.DataBind();

                int qty = 0;
                foreach (DataRow dr in dt.Rows)
                {

                    if (dr["TransferQty"].ToString() != string.Empty)
                    {
                        qty += Convert.ToInt32(dr["TransferQty"].ToString());
                    }
                }
                lbltotalqty.Text = qty.ToString();
            }
        }
    }
}