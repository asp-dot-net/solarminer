﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class admin_adminfiles_company_contactnote : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        HidePanels();

        if (!IsPostBack)
        {
            
            PanSearch.Visible = true;
            PanGrid.Visible = false;

            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            
            BindGrid(0);
        }
    }

    protected DataTable GetGridData()
    {
        string id = "";
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string NoteSetBy = st.EmployeeID;

            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                id = Request.QueryString["id"];
            }

            DataTable dt = ClstblContNotes.tblContNotes_SelectByContactID(id, NoteSetBy);
            if (dt.Rows.Count == 0)
            {
                PanSearch.Visible = true;
            }

            return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        PanSearch.Visible = true;

        DataTable dt = GetGridData();
        if (dt.Rows.Count == 0)
        {
            HidePanels();

            if (deleteFlag == 1)
            {
                SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;
            }
            PanGrid.Visible = false;
             
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string ContactID = Request.QueryString["id"];
        if (!string.IsNullOrEmpty(ContactID))
        {
            SttblContacts stcontact = ClstblContacts.tblContacts_SelectByContactID(ContactID);
            string EmployeeID = stcontact.EmployeeID;
            string ContNote = txtnote.Text;
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string NoteSetBy = st.EmployeeID;

            int success = ClstblContNotes.tblContNotes_Insert(ContactID, EmployeeID, NoteSetBy, ContNote);
            //--- do not chage this code start------
            if (Convert.ToString(success) != "")
            {
                SetAdd();
               
                Response.Redirect(Request.Url.PathAndQuery);
            }
            else
            {
                SetError();
            }
            BindGrid(0);
            Reset();
        }
        //--- do not chage this code end------
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);
    }
   

    protected void ddlcategorysearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid(0);
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }


    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {

        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }
   
    
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
        PanSearch.Visible = false;
        //lnkAdd.Visible = false;
        lnkBack.Visible = true;
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("contacts.aspx");
        //SetCancel();
        //BindGrid(0);
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        ModalPopupExtender2.Show();
        PanSearch.Visible = false;
        InitAdd();
    }


    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }


    public void SetAdd()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        lnkBack.Visible = true;
        //lnkAdd.Visible = false;
        PanAlreadExists.Visible = false;
    }
    public void SetUpdate()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        ModalPopupExtender2.Hide();
        Reset();
        btnAdd.Visible = true;
       
        btnReset.Visible = true;
        
        lnkBack.Visible = true;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        ModalPopupExtender2.Show();
        HidePanels();
        PanAddUpdate.Visible = true;

        btnAdd.Visible = true;
        btnReset.Visible = true;

        //lnkAdd.Visible = false;
        lnkBack.Visible = true;
        //lblAddUpdate.Text = "Add ";
    }
    public void InitUpdate()
    {
        ModalPopupExtender2.Show();
        HidePanels();
        PanAddUpdate.Visible = true;

        btnAdd.Visible = false;
        btnReset.Visible = false;

        lnkBack.Visible = true;
        //lblAddUpdate.Text = "Update ";
    }
    private void HidePanels()
    {
        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;
        PanSearch.Visible = true;
    }
    public void Reset()
    {
        PanAddUpdate.Visible = true;
        txtnote.Text = string.Empty;
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.ToString();
        bool sucess1 = ClstblContNotes.tblContNotes_Delete(id);
        //--- do not chage this code start------
        if (sucess1)
        {
            SetDelete();
        }
        else
        {
            SetError();
        }
        //--- do not chage this code end------
        //--- do not chage this code start------
        GridView1.EditIndex = -1;
        BindGrid(0);
        BindGrid(1);
        //--- do not chage this code end------
    }
}