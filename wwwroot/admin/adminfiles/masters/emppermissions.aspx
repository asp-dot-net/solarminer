﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="emppermissions.aspx.cs" Inherits="admin_adminfiles_master_emppermissions" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../../includes/employeemenu.ascx" TagName="employeemenu" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<div class="page-body headertopbox">
              <h5 class="row-title"><i class="typcn typcn-th-small"></i>Employee</h5>
                            <div class="pull-right"> <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton></div>
               </div>

<div class="page-body padtopzero">
<div class="contactsarea">
                
                <div class="bodymianbg empdetailmenu">
                    <div class="row">
                        <uc1:employeemenu ID="employeemenu1" runat="server" />
                    </div>
                </div>
                <asp:Panel runat="server" CssClass="panaddupdate">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body ">
                                    <div class="messesgarea">
                                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                        </div>
                                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                            <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                                        </div>
                                    </div>
                                    <div class="modal-body paddnone">
                                        <div class="panel-body  checkboxinnerarea">
                                            <div class="formainline">
                                                <div class="form-group checkareanew">
                                                    <label>Admin PL</label>
                                                    <asp:CheckBox ID="chkAdminPL" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Bookings PL</label>
                                                    <asp:CheckBox ID="chkBookingsPL" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Companies PL</label>
                                                    <asp:CheckBox ID="chkCompaniesPL" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Contacts PL</label>
                                                    <asp:CheckBox ID="chkContactsPL" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Follow Ups PL</label>
                                                    <asp:CheckBox ID="chkFollowUpPL" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Inv Issued PL</label>
                                                    <asp:CheckBox ID="chkInvIssuedPL" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Invoices Paid PL</label>
                                                    <asp:CheckBox ID="chkInvPaidPL" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Projects PL</label>
                                                    <asp:CheckBox ID="chkProjectsPL" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Stock Items PL</label>
                                                    <asp:CheckBox ID="chkStockItemsPL" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Stock Orders PL</label>
                                                    <asp:CheckBox ID="chkStockOrdersPL" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Super Tax PL</label>
                                                    <asp:CheckBox ID="chkSuperTaxPL" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Wages PL</label>
                                                    <asp:CheckBox ID="chkWagesPL" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Wholesale PL</label>
                                                    <asp:CheckBox ID="chkWholesalePL" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Bills Owing PL</label>
                                                    <asp:CheckBox ID="chkBillsOwingPL" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Bills Paid PL</label>
                                                    <asp:CheckBox ID="chkBillsPaidPL" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Executive Access</label>
                                                    <asp:CheckBox ID="chkExecAccess" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>

                                                <div class="form-group checkareanew">
                                                    <label>Delete Access</label>
                                                    <asp:CheckBox ID="chkDeleteAccess" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Edit Access</label>
                                                    <asp:CheckBox ID="chkAdminAccess" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Proj Display Access</label>
                                                    <asp:CheckBox ID="chkProjDispAccess" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="form-group checkareanew">
                                                    <label>Manager Access</label>
                                                    <asp:CheckBox ID="chkManagerAccess" runat="server"></asp:CheckBox>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-12 textcenterbutton">
                                        <asp:Button ID="btnUpdateDetail" runat="server" CssClass="btn btn-primary savewhiteicon" Text="Save" OnClick="btnUpdateDetail_Click" CausesValidation="true" />
                                        <asp:Button ID="btnCancelDetail" runat="server" CssClass="btn btn-dark-grey calcelwhiteicon" Text="Cancel" OnClick="btnCancelDetail_Click" CausesValidation="false" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
</div>
    
</asp:Content>
