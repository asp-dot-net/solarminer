﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
     CodeFile="empreferences.aspx.cs" Inherits="admin_adminfiles_master_empreferences" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../../../includes/employeemenu.ascx" TagName="employeemenu" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


<div class="page-body headertopbox">
              <h5 class="row-title"><i class="typcn typcn-th-small"></i>Employee</h5>
                            <div class="pull-right"> <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton></div>
               </div>
    <div class="page-body padtopzero">
    	<div class="contactsarea">
                
                <div class="bodymianbg empdetailmenu">
                    <div class="row">
                        <uc1:employeemenu ID="employeemenu1" runat="server" />
                    </div>
                </div>
                <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body ">
                                    <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                                        <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                                    </div>
                                    <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                                        <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="graybgarea">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Super Fund
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtSuperFund" runat="server" MaxLength="50" Width="200px" class="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Acct
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtEmpBankAcct" runat="server" Width="200px" MaxLength="20" class="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                        
                                                         <label>
                               <asp:CheckBox ID="chkAdminStaff" runat="server"/>
                                <span class="text">Admin Person</span>
                            </label>
                            
                            
                                                            
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    City
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtEmpCity" runat="server" MaxLength="50" Width="200px" class="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Bank BSB
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtEmpBSB" runat="server" MaxLength="10" Width="200px" class="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Super Acct
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtSuperFundAccount" runat="server" MaxLength="20" Width="200px"
                                                                    class="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                        
                                                         <label>
                               <asp:CheckBox ID="chkSalesRep" runat="server" />
                                <span class="text"> Is Sales Rep</span>
                            </label>
                            
                                                           
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    State
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtEmpState" runat="server" MaxLength="10" Width="200px" class="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    TaxFile No
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtTaxFileNumber" runat="server" MaxLength="50" Width="200px" CssClass="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Initial Quota
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtInitialSalesQuota" runat="server" MaxLength="15" Width="200px"
                                                                    class="form-control"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                                                                    ControlToValidate="txtInitialSalesQuota" Display="Dynamic" ErrorMessage="Number Only"
                                                                    ValidationExpression="^\d*\.?\d+$">
                                                                </asp:RegularExpressionValidator>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Employee Address
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtEmpAddress" runat="server" MaxLength="50" Width="200px" class="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Post Code
                                                                </label>
                                                            </span><span>
                                                                <asp:TextBox ID="txtEmpPostCode" runat="server" MaxLength="20" Width="200px" class="form-control"></asp:TextBox>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <span></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 textcenterbutton">
                                            <div class="col-sm-8 col-sm-offset-5">
                                                <asp:Button ID="btnUpdateDetail" runat="server" CssClass="btn btn-primary savewhiteicon btnsaveicon" Text="Save" OnClick="btnUpdateDetail_Click" CausesValidation="true" />
                                                <asp:Button ID="btnCancelDetail" runat="server" CssClass="btn btn-danger btncancelicon" Text="Cancel" OnClick="btnCancelDetail_Click" CausesValidation="false" />
                                            </div>
                                        </div>
                                        <br />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
    </div>           
               
   
</asp:Content>
