<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/MasterPageAdmin.master" AutoEventWireup="true"
    CodeFile="companylocations.aspx.cs" Inherits="admin_adminfiles_master_companylocations" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="updatepanelgrid" runat="server">
        <ContentTemplate>

            <script>

                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoadedpro);
                //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
                prm.add_beginRequest(beginrequesthandler);
                // raised after an asynchronous postback is finished and control has been returned to the browser.
                prm.add_endRequest(endrequesthandler);

                function beginrequesthandler(sender, args) {
                    //shows the modal popup - the update progress
                    $('.loading-container').removeClass('loading-inactive');

                }
                function endrequesthandler(sender, args) {
                    //hide the modal popup - the update progress
                }

                function pageLoadedpro() {
                    $('.loading-container').addClass('loading-inactive');
                    $('body').removeClass('modal-open');
                    $('.modal-backdrop').remove();
                    $("[data-toggle=tooltip]").tooltip();
                    $(".myvalcomloc").select2({
                        //placeholder: "select",
                        allowclear: true
                    });



                }


            </script>

            <script>
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_pageLoaded(pageLoaded);
                function pageLoaded() {

                    $(".myvalcomloc").select2({
                        //placeholder: "select",
                        allowclear: true,
                        minimumResultsForSearch: -1
                    });

                    $('.redreq').click(function () {
                        formValidate();
                    });

                    //$("#bootbox-options").on('click', function () {

                    //    bootbox.dialog({
                    //        message: $("#myModal").html(),

                    //        title: "New E-Mail",
                    //        className: "modal-darkorange",
                    //        buttons: {
                    //            success: {
                    //                label: "Send",
                    //                className: "btn-blue",
                    //                callback: function () { }
                    //            },
                    //            "Save as Draft": {
                    //                className: "btn-danger",
                    //                callback: function () { alert($("#myModal").html());  }
                    //            }
                    //        }
                    //    });
                    //});
                }

            </script>





            <div class="page-body headertopbox">
                <h5 class="row-title"><i class="typcn typcn-th-small"></i>Manage Company Locations</h5>
                <div id="hbreadcrumb" class="pull-right">


                    <ol class="hbreadcrumb breadcrumb fontsize16">

                        <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false" OnClick="lnkAdd_Click" CssClass="btn btn-default purple"><i class="fa fa-plus"></i> Add</asp:LinkButton>
                        <asp:LinkButton ID="lnkBack" runat="server" OnClick="lnkBack_Click" CausesValidation="false" CssClass="btn btn-maroon"><i class="fa fa-backward"></i> Back</asp:LinkButton>
                    </ol>
                </div>
            </div>


            <div class="finaladdupdate">
                <div id="PanAddUpdate" runat="server" visible="false">
                    <div class="panel-body animate-panel padtopzero">
                        <div class="well with-header  addform">
                            <div class="header bordered-blue">
                                <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>
                                Company Locations
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label ID="lblusername" runat="server" class="col-sm-2 control-label">
                                                UserName</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtusername" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Display="dynamic"
                                            ControlToValidate="txtusername" ErrorMessage=""></asp:RequiredFieldValidator>
                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required."
                                                            ControlToValidate="txtCompanyLocation" Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                                <div class="form-group" id="divpassword" runat="server">
                                    <asp:Label ID="lblpassword" runat="server" class="col-sm-2 control-label">
                                                Password</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtpassword" runat="server" MaxLength="200" TextMode="Password" CssClass="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="dynamic"
                                            ControlToValidate="txtpassword" ErrorMessage=""></asp:RequiredFieldValidator>
                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required."
                                                            ControlToValidate="txtCompanyLocation" Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                                 <div class="form-group" id="divcpassword" runat="server">
                                    <asp:Label ID="lblcpassword" runat="server" class="col-sm-2 control-label">
                                              Confirm Password</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtcpassword" runat="server" MaxLength="200" TextMode="Password" CssClass="form-control modaltextbox"></asp:TextBox>
                                          <asp:CompareValidator ID="compvalconfirmPassword" runat="server" ControlToCompare="txtpassword"
                                            ControlToValidate="txtcpassword" ErrorMessage="Password MisMatch" SetFocusOnError="True"
                                            Display="Dynamic"></asp:CompareValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="dynamic"
                                            ControlToValidate="txtcpassword" ErrorMessage=""></asp:RequiredFieldValidator>
                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required."
                                                            ControlToValidate="txtCompanyLocation" Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <asp:Label ID="lblEmail" runat="server" class="col-sm-2 control-label">
                                                Email</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtemail" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>
                                         <asp:RegularExpressionValidator ID="regvalEmail" runat="server" ControlToValidate="txtemail"
                                            Display="Dynamic" ErrorMessage=" Enter Valid E-Mail Address" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                        </asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="dynamic"
                                            ControlToValidate="txtemail" ErrorMessage=""></asp:RequiredFieldValidator>
                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required."
                                                            ControlToValidate="txtCompanyLocation" Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lblFirstName" runat="server" class="col-sm-2 control-label">
                                                Company Locations</asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtCompanyLocation" runat="server" MaxLength="200" CssClass="form-control modaltextbox"></asp:TextBox>

                                        <asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" Display="dynamic"
                                            ControlToValidate="txtCompanyLocation" ErrorMessage=""></asp:RequiredFieldValidator>
                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="This value is required."
                                                            ControlToValidate="txtCompanyLocation" Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lblLastName" runat="server" class="col-sm-2 control-label">
                                                State</asp:Label>
                                    <div class="col-sm-6 drpValidate">
                                        <asp:DropDownList ID="ddlState" runat="server" AppendDataBoundItems="true"
                                            aria-controls="DataTables_Table_0" CssClass="myvalcomloc">
                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="dynamic"
                                            ControlToValidate="ddlState" InitialValue="" ErrorMessage=""></asp:RequiredFieldValidator>
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="reqerror" runat="server" ErrorMessage="This value is required."
                                                            ControlToValidate="ddlState" InitialValue="" Display="Dynamic"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                                <div class="form-group" id="seq" runat="server" visible="false">
                                    <asp:Label ID="lblMobile" runat="server" class="col-sm-2 control-label">
                                                <strong>Seq</strong></asp:Label>
                                    <div class="col-md-3">
                                        <asp:TextBox ID="txtSeq" runat="server" MaxLength="3" Text="0" class="form-control modaltextbox"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="regvalSortOrder" runat="server" ErrorMessage="Number Only"
                                            ValidationExpression="^[0-9]*$" ControlToValidate="txtSeq"></asp:RegularExpressionValidator>
                                        <br />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required."
                                            ControlToValidate="txtSeq" Display="Dynamic" CssClass="reqerror"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group checkareanew">
                                    <div class="col-sm-2 rightalign right-text">
                                        <asp:Label ID="Label2" runat="server" class="control-label">
                                               Is Active?</asp:Label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div>
                                            <label for="<%=chkActive.ClientID %>">
                                                <asp:CheckBox ID="chkActive" runat="server" />
                                                <span class="text">&nbsp;</span>
                                            </label>

                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <asp:LinkButton CssClass="btn btn-default purple redreq btnaddicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click"
                                            Text="Add" CausesValidation="true" />
                                        <asp:LinkButton CssClass="btn btn-success redreq btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                            Text="Save" Visible="false" />
                                        <asp:LinkButton CssClass="btn btn-primary btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                            CausesValidation="false" Text="Reset" />
                                        <asp:LinkButton CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                            CausesValidation="false" Text="Cancel" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="page-body padtopzero">
                <asp:Panel runat="server" ID="PanGridSearch">
                    <div class="animate-panelmessesgarea padbtmzero">
                        <div class="alert alert-success" id="PanSuccess" runat="server">

                            <%--  <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>--%>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server">
                            <%--<i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                                Text="Transaction Failed."></asp:Label></strong>--%>
                        </div>
                        <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                            <%-- <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>--%>
                        </div>
                        <div class="alert alert-info" id="PanNoRecord" runat="server">
                            <%-- <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>--%>
                        </div>
                    </div>

                    <div class="searchfinal">
                        <div class="widget-body shadownone brdrgray">
                            <div class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div class="dataTables_filter">
                                    <table border="0" cellspacing="0" width="100%" style="text-align: right; margin-bottom: 0px;" cellpadding="0">

                                        <tr>
                                            <td class="left-text dataTables_length showdata" style="padding-top: 0px!important;">

                                                <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                    aria-controls="DataTables_Table_0" class="myvalcomloc">
                                                    <asp:ListItem Value="25">Show entries</asp:ListItem>
                                                </asp:DropDownList>

                                            </td>
                                            <td>
                                                <div class="input-group">
                                                    <asp:TextBox ID="txtSearch" runat="server" placeholder="Company Locations" CssClass="form-control m-b"></asp:TextBox>
                                                    <%-- <span class="input-group-btn">
                                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-primary" Text="Go" OnClick="btnSearch_Click" />
                                                                </span>--%>
                                                </div>
                                                <div class="input-group col-sm-1">
                                                    <asp:DropDownList ID="ddlActive" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myvalcomloc">
                                                        <asp:ListItem Value="">All Records</asp:ListItem>
                                                        <asp:ListItem Value="1">Active</asp:ListItem>
                                                        <asp:ListItem Value="2">Not Active</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="input-group">
                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="false" CssClass="btn btn-info btnsearchicon" Text="Search" OnClick="btnSearch_Click" />
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="finalgrid">
                        <div class="table-responsive printArea">
                            <div id="PanGrid" runat="server" oncopy="return false">
                                <div class="table-responsive">
                                    <asp:GridView ID="GridView1" DataKeyNames="CompanyLocationID" runat="server" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover"
                                        OnSorting="GridView1_Sorting" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" OnSelectedIndexChanging="GridView1_SelectedIndexChanging"
                                        OnRowCreated="GridView1_RowCreated1" OnDataBound="GridView1_DataBound1" AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" AutoGenerateColumns="false" AllowPaging="true" PageSize="25">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Company Locations" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                <ItemTemplate>
                                                    <%-- <asp:HiddenField ID="hdnid" Value='<%# Eval("CompanyLocationID").ToString()%>' runat="server" />--%>
                                                    <%#Eval("CompanyLocation")%>
                                                </ItemTemplate>
                                                <ItemStyle CssClass="brdnoneleft" />
                                                <HeaderStyle CssClass="brdnoneleft" />
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="User Name" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <%-- <asp:HiddenField ID="hdnid" Value='<%# Eval("CompanyLocationID").ToString()%>' runat="server" />--%>
                                                    <%#Eval("UName")%>
                                                </ItemTemplate>                                              
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Seq" ItemStyle-Width="100px" HeaderStyle-Width="100px" Visible="false" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate><%# Eval("Seq")%> </ItemTemplate>
                                                <ItemStyle Width="150px" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="" ItemStyle-Width="50px" HeaderStyle-Width="50px" HeaderStyle-CssClass="center-text" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <span class="btn btn-primary btn-xs"><%#Eval("Active").ToString()=="True"?"<i class='fa fa-check-square-o'></i>":"<i class='fa fa-square-o'></i>"%>

                                                        <%--    <asp:CheckBox ID="chkStatus" runat="server" Checked='<%# Eval("Active")%>' /><i class="fa-check-square-o"></i>--%>
                                                        <%-- <label for="<%=chkStatus.ClientID %>" runat="server" id="lblchk">
                                                                        <span></span>
                                                                    </label>--%> Active</span>
                                                    <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" CssClass="btn btn-info btn-xs"
                                                        data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                            <i class="fa fa-edit"></i> Edit
                                                    </asp:LinkButton>
                                                    <%--Reser Password--%>
                                                     <asp:LinkButton ID="lbtnReset" runat="server" CssClass="btn btn-blue btn-xs" CommandName="Reset" data-original-title="Reset Password" data-toggle="tooltip" data-placement="top"
                                                            OnClientClick="return confirm('Are you sure you want to change password?');"
                                                            CommandArgument='<%#Eval("CompanyLocationID")%>' CausesValidation="false">
                                                    <i class="fa fa-refresh"></i> Reset
                                                        </asp:LinkButton>

                                                    <!--DELETE Modal Templates-->
                                                    <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-xs" CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("CompanyLocationID")%>'>
                                                                         <i class="fa fa-trash"></i> Delete
                                                    </asp:LinkButton>

                                                    <!--END DELETE Modal Templates-->

                                                </ItemTemplate>
                                            </asp:TemplateField>


                                        </Columns>
                                        <AlternatingRowStyle />
                                        <PagerTemplate>
                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            <div class="pagination" id="divpage" runat="server">
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                            </div>
                                        </PagerTemplate>
                                        <PagerStyle CssClass="paginationGrid" />
                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                                    </asp:GridView>
                                </div>
                                <div class="paginationnew1" runat="server" id="divnopage">
                                    <table class="table table-bordered table-hover" cellspacing="0" cellpadding="0" rules="all" border="0" id="tablepage" style="width: 100%; border-collapse: collapse;">
                                        <tr>
                                            <td>
                                                <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <asp:UpdateProgress ID="updateprogress1" runat="server" AssociatedUpdatePanelID="updatepanelgrid" DisplayAfter="0">
                        <ProgressTemplate>
                            <div class="splash">
                                <div class="color-line"></div>
                                <div class="splash-title">
                                    <%--<h1>Homer - Responsive Admin Theme</h1>
                <p>Special Admin Theme for small and medium webapp with very clean and aesthetic style and feel. </p>--%>
                                    <div class="spinner">
                                        <div class="rect1"></div>
                                        <div class="rect2"></div>
                                        <div class="rect3"></div>
                                        <div class="rect4"></div>
                                        <div class="rect5"></div>
                                    </div>
                                </div>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <cc1:ModalPopupExtender ID="newmodalpopup" runat="server" TargetControlID="updateprogress1"
                        PopupControlID="updateprogress1" BackgroundCssClass="modalPopup z_index_loader" />


                </asp:Panel>
            </div>




            <script type="text/javascript">




                $(document).ready(function () {

                    prm.add_beginRequest(BeginRequestHandler);
                    prm.add_endRequest(EndRequestHandler);

                    $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();

            });
            $('#<%=btnUpdate.ClientID %>').click(function () {

                        formValidate();
                    });


                });
                function doMyAction() {

                    $('#<%=btnAdd.ClientID %>').click(function () {
                formValidate();

            });
            $('#<%=btnUpdate.ClientID %>').click(function () {
                        formValidate();

                    });


                }


            </script>

            <!--Email Modal Templates-->
            <div id="myModal" style="display: none;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="To" required="">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Subject" required="">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Content" rows="5" required=""></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hdnid" runat="server" />
            <asp:Button ID="btndelete" Style="display: none;" runat="server" />
            <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
                PopupControlID="modal_danger" DropShadow="false" CancelControlID="Button4" OkControlID="btnOKMobile" TargetControlID="btndelete">
            </cc1:ModalPopupExtender>
            <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

                <div class="modal-dialog " style="margin-top: -300px">
                    <div class=" modal-content ">
                        <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                        <div class="modal-header text-center">
                            <i class="glyphicon glyphicon-fire"></i>
                        </div>


                        <div class="modal-title">Delete</div>
                        <label id="ghh" runat="server"></label>
                        <div class="modal-body ">Are You Sure Delete This Entry?</div>
                        <div class="modal-footer " style="text-align: center">
                            <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                            <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                        </div>
                    </div>
                </div>

            </div>

            <asp:HiddenField ID="hdndelete" runat="server" />
        </ContentTemplate>
        <%--<Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAdd" />
        </Triggers>--%>
    </asp:UpdatePanel>


    <div class="loaderPopUP">
    </div>
</asp:Content>
