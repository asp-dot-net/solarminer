﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class admin_adminfiles_master_emppermissions : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string EmployeeID = "19";//Request.QueryString["id"];
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByEmployeeID(EmployeeID);

            chkAdminPL.Checked = Convert.ToBoolean(st.AdminPL);
            chkBookingsPL.Checked = Convert.ToBoolean(st.BookingsPL);
            chkCompaniesPL.Checked = Convert.ToBoolean(st.CompaniesPL);
            chkContactsPL.Checked = Convert.ToBoolean(st.ContactsPL);
            chkFollowUpPL.Checked = Convert.ToBoolean(st.FollowUpPL);
            chkInvIssuedPL.Checked = Convert.ToBoolean(st.InvIssuedPL);
            chkInvPaidPL.Checked = Convert.ToBoolean(st.InvPaidPL);
            chkProjectsPL.Checked = Convert.ToBoolean(st.ProjectsPL);
            chkStockItemsPL.Checked = Convert.ToBoolean(st.StockItemsPL);
            chkStockOrdersPL.Checked = Convert.ToBoolean(st.StockOrdersPL);
            chkSuperTaxPL.Checked = Convert.ToBoolean(st.SuperTaxPL);
            chkWagesPL.Checked = Convert.ToBoolean(st.WagesPL);
            chkWholesalePL.Checked = Convert.ToBoolean(st.WholesalePL);
            chkBillsOwingPL.Checked = Convert.ToBoolean(st.BillsOwingPL);
            chkBillsPaidPL.Checked = Convert.ToBoolean(st.BillsPaidPL);
            chkExecAccess.Checked = Convert.ToBoolean(st.ExecAccess);
            chkDeleteAccess.Checked = Convert.ToBoolean(st.DeleteAccess);
            chkAdminAccess.Checked = Convert.ToBoolean(st.AdminAccess);
            chkProjDispAccess.Checked = Convert.ToBoolean(st.ProjDispAccess);
            chkManagerAccess.Checked = Convert.ToBoolean(st.ManagerAccess);
        }
    }
    protected void btnUpdateDetail_Click(object sender, EventArgs e)
    {
        string EmployeeID = Request.QueryString["id"];
        string AdminPL = Convert.ToString(chkAdminPL.Checked);
        string BookingsPL = Convert.ToString(chkBookingsPL.Checked);
        string CompaniesPL = Convert.ToString(chkCompaniesPL.Checked);
        string ContactsPL = Convert.ToString(chkContactsPL.Checked);
        string FollowUpPL = Convert.ToString(chkFollowUpPL.Checked);
        string InvIssuedPL = Convert.ToString(chkInvIssuedPL.Checked);
        string InvPaidPL = Convert.ToString(chkInvPaidPL.Checked);
        string ProjectsPL = Convert.ToString(chkProjectsPL.Checked);
        string StockItemsPL = Convert.ToString(chkStockItemsPL.Checked);
        string StockOrdersPL = Convert.ToString(chkStockOrdersPL.Checked);
        string SuperTaxPL = Convert.ToString(chkSuperTaxPL.Checked);
        string WagesPL = Convert.ToString(chkWagesPL.Checked);
        string WholesalePL = Convert.ToString(chkWholesalePL.Checked);
        string BillsOwingPL = Convert.ToString(chkBillsOwingPL.Checked);
        string BillsPaidPL = Convert.ToString(chkBillsPaidPL.Checked);
        string ExecAccess = Convert.ToString(chkExecAccess.Checked);
        string DeleteAccess = Convert.ToString(chkDeleteAccess.Checked);
        string AdminAccess = Convert.ToString(chkAdminAccess.Checked);
        string ProjDispAccess = Convert.ToString(chkProjDispAccess.Checked);
        string ManagerAccess = Convert.ToString(chkManagerAccess.Checked);

        bool sucDetail = ClstblEmployees.tblEmployees_UpdatePermissions(EmployeeID, AdminPL, BookingsPL, CompaniesPL, ContactsPL, FollowUpPL, InvIssuedPL, InvPaidPL, ProjectsPL, StockItemsPL, StockOrdersPL, SuperTaxPL, WagesPL, WholesalePL, BillsOwingPL, BillsPaidPL, ExecAccess, DeleteAccess, AdminAccess, ProjDispAccess, ManagerAccess);
        //--- do not chage this code Start------
        if (sucDetail)
        {
            SetAdd1();
            //PanSuccess.Visible = true;
        }
        else
        {
            SetError1();
            //PanError.Visible = true;
        }
    }
    protected void btnCancelDetail_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/master/employee.aspx");
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/master/employee.aspx");
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

}