﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class admin_adminfiles_master_empreferences : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string EmployeeID = "19";//Request.QueryString["id"];
            SttblEmployees st = ClstblEmployees.tblEmployees_SelectByEmployeeID(EmployeeID);

            txtTaxFileNumber.Text = st.TaxFileNumber;
            txtSuperFund.Text = st.SuperFund;
            txtSuperFundAccount.Text = st.SuperFundAccount;
            txtEmpBSB.Text = st.EmpBSB;
            txtEmpBankAcct.Text = st.EmpBankAcct;
            chkAdminStaff.Checked = Convert.ToBoolean(st.AdminStaff);
            chkSalesRep.Checked = Convert.ToBoolean(st.SalesRep);
            txtInitialSalesQuota.Text = st.InitialSalesQuota;
            txtEmpAddress.Text = st.EmpAddress;
            txtEmpCity.Text = st.EmpCity;
            txtEmpState.Text = st.EmpState;
            txtEmpPostCode.Text = st.EmpPostCode;           
        }
    }

    protected void btnUpdateDetail_Click(object sender, EventArgs e)
    {
        string EmployeeID = Request.QueryString["id"];
        string TaxFileNumber = txtTaxFileNumber.Text;
        string SuperFund = txtSuperFund.Text;
        string SuperFundAccount = txtSuperFundAccount.Text;
        string EmpBSB = txtEmpBSB.Text;
        string EmpBankAcct = txtEmpBankAcct.Text;
        string AdminStaff = Convert.ToString(chkAdminStaff.Checked);
        string SalesRep = Convert.ToString(chkSalesRep.Checked);
        //string SalesTeamID = ddlSalesTeamID.SelectedValue;
        string InitialSalesQuota = txtInitialSalesQuota.Text;
        string EmpAddress = txtEmpAddress.Text;
        string EmpCity = txtEmpCity.Text;
        string EmpState = txtEmpState.Text;
        string EmpPostCode = txtEmpPostCode.Text;

        bool sucDetail = ClstblEmployees.tblEmployees_UpdateReferences(EmployeeID, SalesRep, InitialSalesQuota, AdminStaff, TaxFileNumber, SuperFund, SuperFundAccount, EmpBSB, EmpBankAcct, EmpAddress, EmpCity, EmpState, EmpPostCode);
        //--- do not chage this code Start------
        if (sucDetail)
        {
            SetAdd1();
            //PanSuccess.Visible = true;
        }
        else
        {
            SetError1();
            //PanError.Visible = true;
        }
    }
    protected void btnCancelDetail_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/master/employee.aspx");
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin/adminfiles/master/employee.aspx");
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }

}