﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_NewSection_ProjectNew1 : System.Web.UI.Page
{
    protected static string Siteurl;
    static DataView dv;

    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        Siteurl = st.siteurl;

        if (!IsPostBack)
        {
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            BindFilterDropdown();

            try
            {
                ddlSelectDate.SelectedValue = "1";
            }
            catch (Exception ex) { }
            txtStartDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14).ToShortDateString()));
            txtEndDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14).ToShortDateString()));

            if (Roles.IsUserInRole("Sales Manager"))
            {
                ddlTeam.Enabled = false;
                ddlSalesRepSearch.Enabled = true;

                //GridView1.Columns[GridView1.Columns.Count - 2].Visible = false;
            }
            else if (Roles.IsUserInRole("SalesRep"))
            {
                ddlTeam.Enabled = false;
                ddlSalesRepSearch.Enabled = false;
                GridView1.Columns[GridView1.Columns.Count - 2].Visible = false;
            }

            BindSalesRepDropdown();
            BindGrid(0);
        }
    }

    public void BindSalesRepDropdown()
    {
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string SalesTeam = "";
        DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
        if (dt_empsale.Rows.Count > 0)
        {
            foreach (DataRow dr in dt_empsale.Rows)
            {
                SalesTeam += dr["SalesTeamID"].ToString() + ",";
            }
            SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
        }

        if (SalesTeam != "")
        {
            ddlTeam.SelectedValue = SalesTeam;
        }

        if (Roles.IsUserInRole("Sales Manager"))
        {
            if (SalesTeam != string.Empty)
            {
                ddlSalesRepSearch.DataSource = ClstblEmployees.tblEmployees_ManagerSelect(SalesTeam);
            }
        }
        else
        {
            ddlSalesRepSearch.DataSource = ClstblEmployees.tblEmployees_SelectASC_Include();
        }

        ddlSalesRepSearch.DataMember = "fullname";
        ddlSalesRepSearch.DataTextField = "fullname";
        ddlSalesRepSearch.DataValueField = "EmployeeID";
        ddlSalesRepSearch.DataBind();

        try
        {
            ddlSalesRepSearch.SelectedValue = st.EmployeeID;
        }
        catch (Exception ex) { }
    }

    public void BindFilterDropdown()
    {
        ddlSource.DataSource = ClstblCustSource.tblCustSource_SelectbyAsc();
        ddlSource.DataMember = "CustSource";
        ddlSource.DataTextField = "CustSource";
        ddlSource.DataValueField = "CustSourceID";
        ddlSource.DataBind();

        ddlTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
        ddlTeam.DataMember = "SalesTeam";
        ddlTeam.DataTextField = "SalesTeam";
        ddlTeam.DataValueField = "SalesTeamID";
        ddlTeam.DataBind();

        ddlProjectStatus.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();
        ddlProjectStatus.DataMember = "ProjectStatusID";
        ddlProjectStatus.DataTextField = "ProjectStatus";
        ddlProjectStatus.DataValueField = "ProjectStatusID";
        ddlProjectStatus.DataBind();

        ddlProjectType.DataSource = ClstblProjectType.tblProjectType_SelectActive();
        ddlProjectType.DataMember = "ProjectType";
        ddlProjectType.DataTextField = "ProjectType";
        ddlProjectType.DataValueField = "ProjectTypeID";
        ddlProjectType.DataBind();

        ddlFinanceOption.DataSource = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
        ddlFinanceOption.DataValueField = "FinanceWithID";
        ddlFinanceOption.DataTextField = "FinanceWith";
        ddlFinanceOption.DataMember = "FinanceWith";
        ddlFinanceOption.DataBind();

        ddlSubSource.DataSource = ClstblCustSourceSub.tblCustSourceSub_SelectActive();
        ddlSubSource.DataMember = "CustSourceSub";
        ddlSubSource.DataTextField = "CustSourceSub";
        ddlSubSource.DataValueField = "CustSourceSubID";
        ddlSubSource.DataBind();

        ddlState.DataSource = ClstblCompanyLocations.tblCompanyLocations_SelectDistinct();
        ddlState.DataMember = "State";
        ddlState.DataTextField = "State";
        ddlState.DataValueField = "State";
        ddlState.DataBind();
    }

    #region BindGrid Code
    protected DataTable GetGridData()
    {
        string RoleName = "";
        string[] Role = Roles.GetRolesForUser();

        for (int i = 0; i < Role.Length; i++)
        {
            if (Role[i] != "crm_Customer")
            {
                RoleName = Role[i].ToString();
            }
        }


        //DataTable dt = ClstblSalesinfo.tblProject_GetData_Project();
        DataTable dt = ClstblSalesinfo.tblProject_GetData_Project_New1(RoleName, ddlSalesRepSearch.SelectedValue, ddlTeam.SelectedValue, txtProjectNumber.Text, txtCustomer.Text, txtManualNo.Text, ddlProjectStatus.SelectedValue, ddlProjectType.SelectedValue, ddlFinanceOption.SelectedValue, ddlReadyActive.SelectedValue, ddlSource.SelectedValue, ddlSubSource.SelectedValue, ddlState.SelectedValue, ddlSelectDate.SelectedValue, txtStartDate.Text, txtEndDate.Text);

        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = GetGridData();
        dv = new DataView(dt);
        if (dt.Rows.Count == 0)
        {
            if (deleteFlag == 1)
            {
                //SetDelete();
            }
            else
            {
                SetNoRecords();
                //PanNoRecord.Visible = true;

            }
            PanGrid.Visible = false;
            divnopage.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            //PanNoRecord.Visible = false;
            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = dv;
        GridView1.DataBind();
        BindGrid(0);
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }

        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridView1.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }

        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;

            int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }

            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }

    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        GridView1.DataSource = dv;
        GridView1.DataBind();
    }

    protected void GridView1_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ViewDetails")
        {
            string ProjectID= e.CommandArgument.ToString();

            ClearDetails();
            BindDetails(ProjectID);

            ModalPopupExtenderFollowUpNew.Show();
            BindScript();
        }

    }

    #endregion

    #region Funcation
    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "doMyAction();", true);
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    #endregion

    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else if (Convert.ToString(ddlSelectRecords.SelectedValue) == "")
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(GridView1.PageSize);
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindScript();
        BindGrid(0);
    }

    protected void btnClearAll_Click(object sender, EventArgs e)
    {
        txtCustomer.Text = string.Empty;
        txtProjectNumber.Text = string.Empty;
        txtManualNo.Text = string.Empty;
        ddlProjectStatus.SelectedValue = "";
        ddlSource.SelectedValue = "";
        ddlSubSource.SelectedValue = "";
        ddlProjectType.SelectedValue = "";
        ddlFinanceOption.SelectedValue = "";
        ddlReadyActive.SelectedValue = "";
        ddlState.SelectedValue = "";
        
        ddlSelectDate.SelectedValue = "";
        txtStartDate.Text = string.Empty;
        txtEndDate.Text = string.Empty;

        BindScript();
        BindGrid(0);
    }

    public void BindDetails(string ProjectID)
    {
        DataTable dt = ClstblSalesinfo.tblProjects_GetDataByProjectIDforNewProject(ProjectID);

        ltrHeader.Text = dt.Rows[0]["ProjectNumber"].ToString() + "-" + dt.Rows[0]["Project"].ToString();
        lblSys.Text = dt.Rows[0]["SystemCapKW"].ToString();
        lblSysDetails.Text = dt.Rows[0]["SystemDetails"].ToString();
        lblHouseType.Text = dt.Rows[0]["HouseType"].ToString();
        lblRoofType.Text = dt.Rows[0]["RoofType"].ToString();
        lblCompleteDate.Text = dt.Rows[0]["InstallCompleted"].ToString();
        lblProjectNotes.Text = dt.Rows[0]["ProjectNotes"].ToString();
        lblState.Text = dt.Rows[0]["InstallState"].ToString();
        lblInstallerNotes.Text = dt.Rows[0]["InstallerNotes"].ToString();
        lblStatus.Text = dt.Rows[0]["ProjectStatus"].ToString();
        lblDepRec.Text = dt.Rows[0]["DepositReceived"].ToString();
        lblPrice.Text = Convert.ToDecimal(dt.Rows[0]["TotalQuotePrice"]).ToString("F");
        lblFinanceWith.Text = dt.Rows[0]["FinanceWith"].ToString();
        lblManQNo.Text = dt.Rows[0]["ManualQuoteNumber"].ToString();

        //lblSysDetails.Attributes("", dt.Rows[0]["SystemDetails"].ToString());
        lblSysDetails.Attributes.Add("data-original-title", dt.Rows[0]["SystemDetails"].ToString());
        lblInstallerNotes.Attributes.Add("data-original-title", dt.Rows[0]["InstallerNotes"].ToString());
        lblProjectNotes.Attributes.Add("data-original-title", dt.Rows[0]["ProjectNotes"].ToString());
    }

    public void ClearDetails()
    {
        ltrHeader.Text = string.Empty;
        lblSys.Text = string.Empty;
        lblSysDetails.Text = string.Empty;
        lblHouseType.Text = string.Empty;
        lblRoofType.Text = string.Empty;
        lblCompleteDate.Text = string.Empty;
        lblProjectNotes.Text = string.Empty;
        lblState.Text = string.Empty;
        lblInstallerNotes.Text = string.Empty;
        lblStatus.Text = string.Empty;
        lblDepRec.Text = string.Empty;
        lblPrice.Text = string.Empty;
        lblFinanceWith.Text = string.Empty;
        lblManQNo.Text = string.Empty;
    }

    protected void lbtnExport_Click(object sender, EventArgs e)
    {

    }
}