﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_adminfiles_searchdata : System.Web.UI.Page
{
     static DataView dv;
    protected static int custompageIndex;
    protected static int lastpageindex;
    protected static int custompagesize = Convert.ToInt32(SiteConfiguration.GetPageSize());
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();

            BindGrid();
        }
    }

    protected DataTable BindGrid()
    {
        string data = "";
        string userid = Membership.GetUser(User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st_emp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string employeeid = st_emp.EmployeeID;
        if (Roles.IsUserInRole("Administrator"))
        {
            employeeid = "";
            userid = "";
        }

        DataTable dt = new DataTable();
        if (!string.IsNullOrEmpty(Request.QueryString["search"]))
        {
            data = System.Web.HttpUtility.UrlDecode(Request.QueryString["search"]);
            int count = 0;
            foreach (char c in data)
            {
                if (c.ToString() == ",")
                {
                    count = 1;
                    break;
                }
                if (c.ToString() == ";")
                {
                    count = 2;
                    break;
                }
            }
            if (count == 1)
            {
                dt = ClstblCustomers.tbl_Customers_searchdataAll(data, userid, employeeid);
                dv = new DataView(dt);
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
            }
            else if (count == 2)
            {
                dt = ClstblCustomers.tbl_Customers_searchdata(data, userid);
                dv = new DataView(dt);
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
            }
            else
            {
                //Response.Write(data + "=>" + userid + "=>" + employeeid);
                //Response.End();
                dt = ClstblCustomers.tbl_Customers_searchdataAll(data, userid, employeeid);
                dv = new DataView(dt);
                if (dt.Rows.Count > 0)
                {
                    GridView1.DataSource = dt;
                    GridView1.DataBind();
                }
            }

            if (dt.Rows.Count > 0 && ddlSelectRecords.SelectedValue != string.Empty && ddlSelectRecords.SelectedValue != "All")
            {
                if (Convert.ToInt32(ddlSelectRecords.SelectedValue) < dt.Rows.Count)
                {
                    //========label Hide
                    divnopage.Visible = false;
                }
                else
                {
                    divnopage.Visible = true;
                    if (Convert.ToInt32(ddlSelectRecords.SelectedValue) > dt.Rows.Count)
                    {
                        lnkfirst.Visible = false;
                        lnklast.Visible = false;
                        lnknext.Visible = false;
                        lnkprevious.Visible = false;
                    }
                    
                    int iTotalRecords = dv.ToTable().Rows.Count;
                    int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
                    int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
                    if (iEndRecord > iTotalRecords)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    if (iStartsRecods == 0)
                    {
                        iStartsRecods = 1;
                    }
                    if (iEndRecord == 0)
                    {
                        iEndRecord = iTotalRecords;
                    }
                    ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
                }
            }
            else
            {
                if (ddlSelectRecords.SelectedValue == "All")
                {
                    divnopage.Visible = true;
                    ltrPage.Text = "Showing " + dt.Rows.Count + " entries";
                }
            }
            //Response.Write(count + "<br/>");
        }
        return dt;
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
        }
        BindGrid();
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        BindGrid();
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = BindGrid();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }

    private string ConvertSortDirectionToSql(SortDirection sortDireciton)
    {
        string m_SortDirection = String.Empty;

        switch (sortDireciton)
        {
            case SortDirection.Ascending:
                m_SortDirection = "ASC";
                break;

            case SortDirection.Descending:
                m_SortDirection = "DESC";
                break;
        }
        return m_SortDirection;
    }
    protected void GridView1_DataBound(object sender, EventArgs e)
    {
    }
    //protected void GridView1_DataBound(object sender, EventArgs e)
    //{
    //    GridViewRow gvrow = GridView1.BottomPagerRow;
    //    Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
    //    lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
    //    int[] page = new int[7];
    //    page[0] = GridView1.PageIndex - 2;
    //    page[1] = GridView1.PageIndex - 1;
    //    page[2] = GridView1.PageIndex;
    //    page[3] = GridView1.PageIndex + 1;
    //    page[4] = GridView1.PageIndex + 2;
    //    page[5] = GridView1.PageIndex + 3;
    //    page[6] = GridView1.PageIndex + 4;
    //    for (int i = 0; i < 7; i++)
    //    {
    //        if (i != 3)
    //        {
    //            if (page[i] < 1 || page[i] > GridView1.PageCount)
    //            {
    //                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
    //                lnkbtn.Visible = false;
    //            }
    //            else
    //            {
    //                LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
    //                lnkbtn.Text = Convert.ToString(page[i]);
    //                lnkbtn.CommandName = "PageNo";
    //                lnkbtn.CommandArgument = lnkbtn.Text;

    //            }
    //        }
    //    }
    //    if (GridView1.PageIndex == 0)
    //    {
    //        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
    //        lnkbtn.Visible = false;
    //        lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
    //        lnkbtn.Visible = false;

    //    }
    //    if (GridView1.PageIndex == GridView1.PageCount - 1)
    //    {
    //        LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
    //        lnkbtn.Visible = false;
    //        lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
    //        lnkbtn.Visible = false;

    //    }
    //    Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
    //    if (dv.ToTable().Rows.Count > 0)
    //    {
    //        int iTotalRecords = dv.ToTable().Rows.Count;
    //        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
    //        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
    //        if (iEndRecord > iTotalRecords)
    //        {
    //            iEndRecord = iTotalRecords;
    //        }
    //        if (iStartsRecods == 0)
    //        {
    //            iStartsRecods = 1;
    //        }
    //        if (iEndRecord == 0)
    //        {
    //            iEndRecord = iTotalRecords;
    //        }
    //        ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
    //    }
    //    else
    //    {
    //        ltrPage.Text = "";
    //    }
    //    //BindScript();
    //}
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {

        //if (e.Row.RowType == DataControlRowType.Pager)
        //{
        //    GridViewRow gvr = e.Row;
        //    LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p1");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p2");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p4");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p5");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //    lb = (LinkButton)gvr.Cells[0].FindControl("p6");
        //    lb.Command += new CommandEventHandler(lb_Command);
        //}

    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        // string CustomerID = Request.QueryString["compid"];

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            HiddenField hdnid = (HiddenField)e.Row.FindControl("hdnid");
            HiddenField hndProjectID = (HiddenField)e.Row.FindControl("hndProjectID");
            HiddenField hndContactID = (HiddenField)e.Row.FindControl("hndContactID");
            //Response.Write(hdnid.Value);
            //Response.End();
            SttblCustomers stcust = ClstblCustomers.tblCustomers_SelectByCustomerID(hdnid.Value);
            //Response.Write(stcust.CompanyNumber);
            //Response.End();

            if (hndProjectID.Value != string.Empty)
            {
                SttblProjects stproject = ClstblProjects.tblProjects_SelectByProjectID(hndProjectID.Value);
                SttblContacts stcontact = ClstblContacts.tblContacts_SelectByContactID(hndContactID.Value);
                SttblProjects2 stproject2 = ClstblProjects.tblProjects2_SelectByProjectID(hndProjectID.Value);
                SttblInvoicePayments StInvoicePayments = ClstblInvoicePayments.tblInvoicePayments_StructSelectByProjectID(hndProjectID.Value);

                //#region Location
                //DataTable dt = new DataTable();
               
                //string datagrid = "select C.StreetState+ ' - '+C.StreetCity as Location from tblCustomers C where C.CustomerID="+ hdnid.Value;
               
                //dt = ClstblCustomers.query_execute(datagrid);
                //Label Label4 = (Label)e.Row.FindControl("Label4");
                //Label4.Text= dt.Rows[0]["Location"].ToString();
                //#endregion


                Label lblcompanyno = (Label)e.Row.FindControl("lblcompanyno");
                Label lblleadstatus = (Label)e.Row.FindControl("lblleadstatus");
                Label lblnextfollowupdate = (Label)e.Row.FindControl("lblnextfollowupdate");

                Label lbldesp = (Label)e.Row.FindControl("lbldesp");
                Label lblprojecttype = (Label)e.Row.FindControl("lblprojecttype");
                Label lblprojectno = (Label)e.Row.FindControl("lblprojectno");

                Label lblmanualno = (Label)e.Row.FindControl("lblmanualno");
                Label lblprjopendate = (Label)e.Row.FindControl("lblprjopendate");
                Label lblsys_detail = (Label)e.Row.FindControl("lblsys_detail");

                Label lblhousetype = (Label)e.Row.FindControl("lblhousetype");
                Label lblrooftype = (Label)e.Row.FindControl("lblrooftype");
                Label lblroofangle = (Label)e.Row.FindControl("lblroofangle");


                Label lbleletricaldist = (Label)e.Row.FindControl("lbleletricaldist");
                Label lblretialer = (Label)e.Row.FindControl("lblretialer");
                Label lblNMI = (Label)e.Row.FindControl("lblNMI");

                Label lblRPN = (Label)e.Row.FindControl("lblRPN");
                Label lblLN = (Label)e.Row.FindControl("lblLN");
                Label lbldistappliedDate = (Label)e.Row.FindControl("lbldistappliedDate");
                Label lblApprocalRef = (Label)e.Row.FindControl("lblApprocalRef");
                Label lblDistApprocedDate = (Label)e.Row.FindControl("lblDistApprocedDate");
                Label lblpaymentoption = (Label)e.Row.FindControl("lblpaymentoption");


                Label lbldepositoption = (Label)e.Row.FindControl("lbldepositoption");

                Label lblfinancepay_type = (Label)e.Row.FindControl("lblfinancepay_type");

                Label lbltotal_cost = (Label)e.Row.FindControl("lbltotal_cost");
                Label lblpaiddate = (Label)e.Row.FindControl("lblpaiddate");
                Label lblbal_owing = (Label)e.Row.FindControl("lblbal_owing");
                Label lblquote_accept = (Label)e.Row.FindControl("lblquote_accept");


                Label lbllastcreatedQuoteDate = (Label)e.Row.FindControl("lbllastcreatedQuoteDate");
                Label lblDepoistDate = (Label)e.Row.FindControl("lblDepoistDate");
                Label lblActiveDate = (Label)e.Row.FindControl("lblActiveDate");
                Label lbllApplicationDate = (Label)e.Row.FindControl("lbllApplicationDate");
                Label lblDocumentSentDate = (Label)e.Row.FindControl("lblDocumentSentDate");
                Label lblDocumentReceivedDate = (Label)e.Row.FindControl("lblDocumentReceivedDate");

                Label lblDocumentReceivedBy = (Label)e.Row.FindControl("lblDocumentReceivedBy");
                Label lblDocumentVerified = (Label)e.Row.FindControl("lblDocumentVerified");
                Label lblSentBy = (Label)e.Row.FindControl("lblSentBy");

                Label lblSentDate = (Label)e.Row.FindControl("lblSentDate");
                Label lblPayDate = (Label)e.Row.FindControl("lblPayDate");
                Label lblTotalpaid = (Label)e.Row.FindControl("lblTotalpaid");

                Label lblPayby = (Label)e.Row.FindControl("lblPayby");
                Label lblRedby = (Label)e.Row.FindControl("lblRedby");
                Label lblPurchaseNo = (Label)e.Row.FindControl("lblPurchaseNo");

                Label lblDocumentSentBy = (Label)e.Row.FindControl("lblDocumentSentBy");
                Label lblPostalTrackingNumber = (Label)e.Row.FindControl("lblPostalTrackingNumber");

                Label lblInstallBookingdate = (Label)e.Row.FindControl("lblInstallBookingdate");
                Label lblInstaller = (Label)e.Row.FindControl("lblInstaller");
                Label lblISformbay = (Label)e.Row.FindControl("lblISformbay");


                Label lblFormbayID = (Label)e.Row.FindControl("lblFormbayID");
                Label lblInstallCompleted = (Label)e.Row.FindControl("lblInstallCompleted");
                Label lblInstallDocRec = (Label)e.Row.FindControl("lblInstallDocRec");

                Label lblCertificateIssued = (Label)e.Row.FindControl("lblCertificateIssued");
                Label lblMeterTime = (Label)e.Row.FindControl("lblMeterTime");
                Label lblMeterApplyRef = (Label)e.Row.FindControl("lblMeterApplyRef");


                Label lblInspectroName = (Label)e.Row.FindControl("lblInspectroName");
                Label lblInspectionDate = (Label)e.Row.FindControl("lblInspectionDate");
                Label lblSTCAppliedDate = (Label)e.Row.FindControl("lblSTCAppliedDate");


                Label lblPVDStatus = (Label)e.Row.FindControl("lblPVDStatus");
                Label lblPVDNumber = (Label)e.Row.FindControl("lblPVDNumber");
                Label lblInvNo = (Label)e.Row.FindControl("lblInvNo");


                Label lblInvAmnt = (Label)e.Row.FindControl("lblInvAmnt");
                Label lblInvDate = (Label)e.Row.FindControl("lblInvDate");
                Label lblAdvanceAmount = (Label)e.Row.FindControl("lblAdvanceAmount");
                Label lblAdvancePayDate = (Label)e.Row.FindControl("lblAdvancePayDate");
                Label lblNotes = (Label)e.Row.FindControl("lblNotes");
                //Label lblPurchaseNo = (Label)e.Row.FindControl("lblPurchaseNo");

                //       ElectricianName

                lblInspectroName.Text = stproject2.InspectorName;
                lblInspectionDate.Text = stproject2.InspectionDate;

                DataTable dtinvoicecommon = ClstblInvoiceCommon.tblInvoiceCommon_SelectByProject(hndProjectID.Value);
                try
                {
                    lblInvAmnt.Text = dtinvoicecommon.Rows[0]["InvAmnt"].ToString();
                    lblAdvanceAmount.Text = dtinvoicecommon.Rows[0]["AdvanceAmount"].ToString();
                }
                catch { }

                DataTable dtCount1 = ClstblInvoicePayments.tblInvoicePayments_CountTotal(hndProjectID.Value);
                try
                {
                    lblTotalpaid.Text = dtCount1.Rows[0]["InvoicePayTotal"].ToString();
                }
                catch { }

                string InvoicePayments = StInvoicePayments.InvoicePayMethodID;
                
                if (!string.IsNullOrEmpty(InvoicePayments))
                {
                    DataTable dtpayby = ClsProjectSale.tblFPTransType_getInvbyFPTransTypeID(InvoicePayments);
                    if (dtpayby.Rows.Count > 0)
                    {
                        if (dtpayby.Rows[0]["FPTransTypeAB"].ToString() != string.Empty)
                        {
                            lblPayby.Text = dtpayby.Rows[0]["FPTransTypeAB"].ToString();
                        }
                    }
                }

                DataTable dtQuote = ClstblProjects.tblProjectQuotes_SelectByProjectID(hndProjectID.Value);
                if (dtQuote.Rows.Count > 0)
                {
                    if (dtQuote.Rows[0]["ProjectQuoteDate"].ToString() != "")
                    {
                        lbllastcreatedQuoteDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(dtQuote.Rows[0]["ProjectQuoteDate"].ToString()));
                    }
                }
                lblDepoistDate.Text = stproject.DepositReceived;

                DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(hndProjectID.Value);
                decimal paiddate = 0;
                decimal balown = 0;
                if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != "")
                {
                    paiddate = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
                    balown = Convert.ToDecimal(stproject.TotalQuotePrice) - Convert.ToDecimal(paiddate);
                    try
                    {
                        lblbal_owing.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balown));
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        lblbal_owing.Text = SiteConfiguration.ChangeCurrencyVal(stproject.TotalQuotePrice);
                    }
                    catch { }
                }


                lblpaymentoption.Text = stproject.financewith;
                
                lblcompanyno.Text = stcust.CompanyNumber;


                lblleadstatus.Text = stcust.lead_status;
                DataTable dtcust = ClstblCustomers.tblCustomers_instinfosearch(hdnid.Value);
                //Response.Write(hdnid.Value);
                //Response.End();
                if (dtcust.Rows[0]["NextFollowupDate"].ToString() != string.Empty)
                 {
                     lblnextfollowupdate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(dtcust.Rows[0]["NextFollowupDate"].ToString()));
                    
                }

                 if (dtcust.Rows[0]["description"].ToString() != string.Empty)
                 {
                     lbldesp.Text = dtcust.Rows[0]["description"].ToString();
                 }
                lblprojecttype.Text = stproject.projecttype;
                lblprojectno.Text = stproject.ProjectNumber;
                lblmanualno.Text = stproject.ManualQuoteNumber;
                lblprjopendate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.ProjectOpened));
                lblsys_detail.Text = stproject.SystemDetails;

                lblhousetype.Text = stproject.HouseType;
                lblrooftype.Text = stproject.RoofType;
                lblroofangle.Text = stproject.RoofAngle;
                //Response.Write(stproject.ElecDistOK);
                //Response.End();
                lbleletricaldist.Text = stproject.ElecDistributor;//stproject.ElectricianName
                lblretialer.Text = stproject.ElecRetailer;
                lblNMI.Text = stproject.NMINumber;

                lblRPN.Text = stproject.RegPlanNo;
                lblLN.Text = stproject.LotNumber;

                if (stproject.ElecDistAppDate != string.Empty)
                {
                    lbldistappliedDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.ElecDistAppDate));
                }

                lblApprocalRef.Text = stproject.ElecDistApprovelRef;
                if (stproject.ElecDistApproved != string.Empty)
                {
                    lblDistApprocedDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.ElecDistApproved));
                }

                lbldepositoption.Text = stproject2.FinanceWithDeposit;
                lblfinancepay_type.Text = stproject.PaymentType;
                lbltotal_cost.Text = stproject.PreviousTotalQuotePrice;
                if (stproject.InvoicePaid != string.Empty)
                {
                    lblpaiddate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.InvoicePaid));
                }

                if (stproject.QuoteAccepted != string.Empty)
                {
                    lblquote_accept.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.QuoteAccepted));
                }
                if (stproject.ActiveDate != string.Empty)
                {
                    lblActiveDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.ActiveDate));
                }
                if (stproject.ApplicationDate != string.Empty)
                {
                    lbllApplicationDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.ApplicationDate));
                }

                if (stproject.DocumentSentDate != string.Empty)
                {
                    lblDocumentSentDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.DocumentSentDate));
                }

                if (stproject.DocumentReceivedDate != string.Empty)
                {
                    lblDocumentReceivedDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.DocumentReceivedDate));
                }
                lblDocumentReceivedBy.Text = stproject.DocumentReceivedByName;
                lblDocumentVerified.Text = stproject.DocumentVerified;
                lblSentBy.Text = stproject.SentBy;
                if (stproject.SentDate != string.Empty)
                {
                    lblSentDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.SentDate));
                }
                if (stproject.InstallerPayDate != string.Empty)
                {
                    lblPayDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.InstallerPayDate));
                }
                lblPostalTrackingNumber.Text = stproject.PostalTrackingNumber;
                lblRedby.Text = stproject.Empname1;
                lblDocumentSentBy.Text = stproject.DocumentSentByName;
                lblPurchaseNo.Text = stproject.PurchaseNo;
                if (stproject.InstallBookingDate != string.Empty)
                {
                    lblInstallBookingdate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.InstallBookingDate));
                }
                lblInstaller.Text = stproject.InstallerName;
                //Response.Write(stproject.IsFormBay);
                //Response.End();
                if (stproject.IsFormBay == "1")
                {
                    lblISformbay.Text = "Yes";
                }
                else if (stproject.IsFormBay == "2")
                {
                    lblISformbay.Text = "No";
                }
                lblFormbayID.Text = stproject.FormbayId;
                if (stproject.InstallCompleted != string.Empty)
                {
                    lblInstallCompleted.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.InstallCompleted));
                }
                if (stproject.InstallDocsReceived != string.Empty)
                {
                    lblInstallDocRec.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.InstallDocsReceived));
                }
                if (stproject.CertificateIssued != string.Empty)
                {
                    lblCertificateIssued.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.CertificateIssued));
                }

                lblMeterTime.Text = stproject.MeterAppliedTime;
                lblMeterApplyRef.Text = stproject.MeterAppliedRef;
                if (stproject.STCApplied != string.Empty)
                {
                    lblSTCAppliedDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.STCApplied));
                }
                lblPVDStatus.Text = stproject.PVDStatus;
                lblPVDNumber.Text = stproject.PVDNumber;
                lblInvNo.Text = stproject.InvoiceNumber;
                if (stproject.InstallerInvDate != string.Empty)
                {
                    lblInvDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.InstallerInvDate));
                }
                if (stproject.InstallerPayDate != string.Empty)
                {
                    lblAdvancePayDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stproject.InstallerPayDate));
                }
                lblNotes.Text = stproject.InvoiceNotes;
            }
        }
    }

    #region pagination
    protected void rptpage_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "pagebtn")
        {
            string id = e.CommandArgument.ToString();
            PageClick(id);

            //DataTable dt = new DataTable();
            //dt = GetGridData();
            //DataView dv = new DataView(dt);

            //dv.Sort = ViewState["Column"] + " " + ViewState["Sortorder"];
            //if (e.CommandName == ViewState["Column"].ToString())
            //{
            //    if (ViewState["Sortorder"].ToString() == "ASC")
            //        ViewState["Sortorder"] = "DESC";
            //    else
            //        ViewState["Sortorder"] = "ASC";
            //}
            //else
            //{
            //    ViewState["Column"] = e.CommandName;
            //    ViewState["Sortorder"] = "ASC";
            //}
            //LinkButton lnkpagebtn = (LinkButton)e.Item.FindControl("lnkpagebtn");
            //if (Convert.ToInt32(id) == custompageIndex)
            //{
            //    Response.Write(lnkpagebtn.);
            //    //lnkpagebtn.Style.Add("class", "pagebtndesign nonepointer");
            //}
        }
    }
    public void PageClick(String id)
    {
        custompageIndex = Convert.ToInt32(id);
        BindGrid();
    }
    protected void lnkfirst_Click(object sender, EventArgs e)
    {
        custompageIndex = 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnkprevious_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex - 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnknext_Click(object sender, EventArgs e)
    {
        custompageIndex = custompageIndex + 1;
        PageClick(custompageIndex.ToString());
    }
    protected void lnklast_Click(object sender, EventArgs e)
    {

        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata / custompagesize + ");", true);
        //Response.Write(hdncountdata.Value + "=" + custompagesize); Response.End();
        lastpageindex = Convert.ToInt32(hdncountdata.Value) / custompagesize;



        if (Convert.ToInt32(hdncountdata.Value) % custompagesize > 0)
        {
            lastpageindex = lastpageindex + 1;
        }
        //Response.Write("-->" + lastpageindex); Response.End();
        custompageIndex = lastpageindex;
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "test(" + countdata + ");", true);
        PageClick(lastpageindex.ToString());
        ScriptManager.GetCurrent(this.Page).RegisterPostBackControl(lnklast);
    }
    #endregion
}