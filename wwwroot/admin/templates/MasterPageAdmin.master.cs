using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class BossAdmin_MasterPageAdmin : System.Web.UI.MasterPage
{
    protected static string Siteurl;
    protected static string SiteName;
    protected void Page_Load(object sender, EventArgs e)
    {
        //   StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        Siteurl = st.siteurl;
        string pagename = Utilities.GetPageName(Request.Url.PathAndQuery);
        if (!IsPostBack)
        {
            SiteName = ConfigurationManager.AppSettings["SiteName"].ToString();
            System.IO.FileInfo image = new System.IO.FileInfo(Request.PhysicalApplicationPath + "/userfiles/sitelogo/" + st.sitelogoupload);
            bool isfileUploaded = image.Exists;
        }

        if (!Request.IsAuthenticated)
        {
            Response.End();
            Response.Redirect("~/admin/Default.aspx");
        }

        switch (pagename.ToLower())
        {
            //case "dashboard":
            //    dash.Visible = true;
            //    Dashboard.Attributes.Add("class", "activePriNav");
            //    break;

            //case "adminutilities":
            //    divutility.Visible = true;
            //    hyputilities.CssClass = "menuOn";
            //    break;

            //case "adminsiteconfiguration":
            //     divutility.Visible = true;
            //     hypsiteconfig.CssClass = "menuOn";
            //    break;

            //case "announcement":
            //    divannouncement.Visible = true;
            //    hypannounctment.CssClass = "menuOn";
            //    liannouncement.Attributes.Add("class", "activePriNav");
            //    break;

            //case "news":
            //    divnews.Visible = true;
            //    linews.Attributes.Add("class", "activePriNav");
            //    hypnews.CssClass = "menuOn";
            //    break;

            //case "newsdetail":
            //    divnews.Visible = true;
            //    linews.Attributes.Add("class", "activePriNav");
            //    hypnews.CssClass = "menuOn";
            //    break;

            //case "demo":
            //    divdemo.Visible = true;
            //    liDemo.Attributes.Add("class", "activePriNav");
            //    hypdemo.CssClass = "menuOn";
            //    break;
        }


        if (pagename != "profile")
        {
            // secpage.Attributes.Add("class", "scrollable padder shdoimg");
        }
        else
        {
            //   secpage.Attributes.Add("class", "scrollable shdoimg");
        }


        //////title
       if (pagename == "CustomerNew")
        {
            pagetitle.Text = "SolarMiner | New Customer";
        }
        if (pagename == "Customer")
        {
            pagetitle.Text = "SolarMiner | New Customer";
        }
        if (pagename == "LeadTrackerNew")
        {
            pagetitle.Text = "SolarMiner | New Lead Tracker";
        }

        if (pagename == "ProjectNew")
        {
            pagetitle.Text = "SolarMiner | New Projects";
        }

        if (pagename == "SalesInfo")
        {
            pagetitle.Text = "SolarMiner | Project Details";
        }
        if (pagename == "NewContact")
        {
            pagetitle.Text = "SolarMiner | New Contact";
        }

        if (pagename == "profile")
        {
            pagetitle.Text = "SolarMiner | Profile";
        }
        if (pagename == "dashboard")
        {
            pagetitle.Text = "SolarMiner | Dashboard";
        }
        if (pagename == "setting")
        {
            pagetitle.Text = "SolarMiner | Setting";
        }
        if (pagename == "profile")
        {
            pagetitle.Text = "SolarMiner | Profile";
        }
        if (pagename == "help")
        {
            pagetitle.Text = "SolarMiner | Help";
        }
        if (pagename == "project")
        {
            pagetitle.Text = "SolarMiner | Project";
        }
        if (pagename == "installations")
        {
            pagetitle.Text = "SolarMiner | Installation";
        }
        if (pagename == "invoicesissued")
        {
            pagetitle.Text = "SolarMiner | Invoices Issued";
        }
        if (pagename == "invoicespaid")
        {
            pagetitle.Text = "SolarMiner | Invoices Paid";
        }
        if (pagename == "lead")
        {
            pagetitle.Text = "SolarMiner | Lead";
        }
        if (pagename == "lteamcalendar")
        {
            pagetitle.Text = "SolarMiner | Team Calender";
        }
        if (pagename == "installercalendar")
        {
            pagetitle.Text = "SolarMiner | Installer Calender";
        }


        #region master
        if (pagename == "companylocations")
        {
            pagetitle.Text = "SolarMiner | Company Locations";
        }
        if (pagename == "custsource")
        {
            pagetitle.Text = "SolarMiner | Company Source";
        }
        if (pagename == "custsourcesub")
        {
            pagetitle.Text = "SolarMiner | Company Source Sub";
        }
        if (pagename == "CustType")
        {
            pagetitle.Text = "SolarMiner | Company Type";
        }
        if (pagename == "elecdistributor")
        {
            pagetitle.Text = "SolarMiner | Elec Distributor";
        }
        if (pagename == "elecretailer")
        {
            pagetitle.Text = "SolarMiner | Elec Retailer";
        }
        if (pagename == "employee")
        {
            pagetitle.Text = "SolarMiner | Employee";
        }
        if (pagename == "FinanceWith")
        {
            pagetitle.Text = "SolarMiner | Finance With";
        }
        if (pagename == "housetype")
        {
            pagetitle.Text = "SolarMiner | House Type";
        }
        if (pagename == "leadcancelreason")
        {
            pagetitle.Text = "SolarMiner | Lead Cancel Reason";
        }
        if (pagename == "mtcereason")
        {
            pagetitle.Text = "SolarMiner | Mtce Reasons";
        }
        if (pagename == "mtcereasonsub")
        {
            pagetitle.Text = "SolarMiner | Mtce Reasons Sub";
        }
        if (pagename == "projectcancel")
        {
            pagetitle.Text = "SolarMiner | Project Cancel";
        }
        if (pagename == "projectonhold")
        {
            pagetitle.Text = "SolarMiner | Project On-Hold";
        }
        if (pagename == "projectstatus")
        {
            pagetitle.Text = "SolarMiner | Project Status";
        }
        if (pagename == "projecttrackers")
        {
            pagetitle.Text = "SolarMiner | Project Trackers";
        }
        if (pagename == "projecttypes")
        {
            pagetitle.Text = "SolarMiner | Project Types";
        }
        if (pagename == "projectvariations")
        {
            pagetitle.Text = "SolarMiner | Project Variations";
        }
        if (pagename == "promotiontype")
        {
            pagetitle.Text = "SolarMiner | Promotion Type";
        }
        if (pagename == "prospectcategory")
        {
            pagetitle.Text = "SolarMiner | Prospect Categories";
        }
        if (pagename == "roofangles")
        {
            pagetitle.Text = "SolarMiner | Roof Angles";
        }
        if (pagename == "roottye")
        {
            pagetitle.Text = "SolarMiner | Root Type";
        }
        if (pagename == "salesteams")
        {
            pagetitle.Text = "SolarMiner | Sales Team";
        }
        if (pagename == "StockCategory")
        {
            pagetitle.Text = "SolarMiner | Stock Category";
        }
        if (pagename == "transactiontypes")
        {
            pagetitle.Text = "SolarMiner | Transaction Types";
        }
        if (pagename == "paymenttype")
        {
            pagetitle.Text = "SolarMiner | Finance Payment Type";
        }
        if (pagename == "invcommontype")
        {
            pagetitle.Text = "SolarMiner | Invoice Type";
        }
        if (pagename == "postcodes")
        {
            pagetitle.Text = "SolarMiner | Post Codes";
        }
        if (pagename == "custinstusers")
        {
            pagetitle.Text = "SolarMiner | Cust/Inst Users";
        }
        if (pagename == "newsletter")
        {
            pagetitle.Text = "SolarMiner | News Letter";
        }
        if (pagename == "logintracker")
        {
            pagetitle.Text = "SolarMiner | Login Tracker";
        }
        if (pagename == "leftempprojects")
        {
            pagetitle.Text = "SolarMiner | Left Employee Projects";
        }
        if (pagename == "lteamtime")
        {
            pagetitle.Text = "SolarMiner | Manage L-Team App Time";
        }
        if (pagename == "refundoptions")
        {
            pagetitle.Text = "SolarMiner | Refund Options";
        }

        #endregion

        if (pagename == "company")
        {
            //Response.Write("Hi");
            //Response.End();
            pagetitle.Text = "SolarMiner | Company";
        }
        if (pagename == "contacts")
        {
            pagetitle.Text = "SolarMiner | Contact";
        }

        if (pagename == "leadtracker")
        {
            pagetitle.Text = "SolarMiner | Lead Tracker";
        }
        if (pagename == "gridconnectiontracker")
        {
            pagetitle.Text = "SolarMiner | GridConnection Tracker.";
        }

        if (pagename == "lteamtracker")
        {
            pagetitle.Text = "SolarMiner | Team Lead Tracker";
        }
        if (pagename == "stctracker")
        {
            pagetitle.Text = "SolarMiner | STC Tracker";
        }
        if (pagename == "instinvoice")
        {
            pagetitle.Text = "SolarMiner | Inst Invoice Tracker";
        }
        if (pagename == "salesinvoice")
        {
            pagetitle.Text = "SolarMiner | Sales Invoice Tracker";
        }
        if (pagename == "installbookingtracker")
        {
            pagetitle.Text = "SolarMiner | Install Booking Tracker";
        }
        if (pagename == "ApplicationTracker")
        {
            pagetitle.Text = "SolarMiner | Application Tracker";
        }
        if (pagename == "accrefund")
        {
            pagetitle.Text = "SolarMiner | Refund";
        }
        if (pagename == "formbay")
        {
            pagetitle.Text = "SolarMiner | FormBay";
        }
        if (pagename == "stockitem")
        {
            pagetitle.Text = "SolarMiner | Stock Item";
        }
        if (pagename == "stocktransfer")
        {
            pagetitle.Text = "SolarMiner | Stock Transfer";
        }
        if (pagename == "stockorder")
        {
            pagetitle.Text = "SolarMiner | Stock Order";
        }
        if (pagename == "stockdatasheet")
        {
            pagetitle.Text = "SolarMiner | Data Sheet";
        }

        if (pagename == "Wholesale")
        {
            pagetitle.Text = "SolarMiner | Wholesale";
        }
        if (pagename == "wholesalededuct")
        {
            pagetitle.Text = "SolarMiner | Wholesale Deduct";
        }
        if (pagename == "stockdeduct")
        {
            pagetitle.Text = "SolarMiner | Stock Deduct";
        }
        if (pagename == "stockin")
        {
            pagetitle.Text = "SolarMiner | Stock In";
        }
        if (pagename == "WholesaleIn")
        {
            pagetitle.Text = "SolarMiner | Wholesale In";
        }
        if (pagename == "stockusage")
        {
            pagetitle.Text = "SolarMiner | Stock Usage";
        }
        if (pagename == "stockorderreport")
        {
            pagetitle.Text = "SolarMiner | Stock Order Report";
        }

        if (pagename == "promosend")
        {
            pagetitle.Text = "SolarMiner | Promo Send";
        }

        if (pagename == "lead")
        {
            pagetitle.Text = "SolarMiner | Lead";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "SolarMiner | Maintenance";
        }
        if (pagename == "casualmtce")
        {
            pagetitle.Text = "SolarMiner| Casual Maintenance";
        }
        if (pagename == "noinstalldate")
        {
            pagetitle.Text = "SolarMiner | NoInstall Date Report";
        }
        if (pagename == "installdate")
        {
            pagetitle.Text = "SolarMiner | Install Date Report";
        }
        if (pagename == "panelscount")
        {
            pagetitle.Text = "SolarMiner | Panel Graph";
        }
        if (pagename == "accountreceive")
        {
            pagetitle.Text = "SolarMiner | Account Receive Report";
        }
        if (pagename == "paymentstatus")
        {
            pagetitle.Text = "SolarMiner | Payment Status Report";
        }
        if (pagename == "formbaydocsreport")
        {
            pagetitle.Text = "SolarMiner | Formbay Docs Report";
        }
        if (pagename == "weeklyreport")
        {
            pagetitle.Text = "SolarMiner | Weekly Report";
        }
        if (pagename == "leadassignreport")
        {
            pagetitle.Text = "SolarMiner | Lead Assign Report";
        }
        if (pagename == "stockreport")
        {
            pagetitle.Text = "SolarMiner | Stock Report";
        }
        if (pagename == "custproject")
        {
            pagetitle.Text = "SolarMiner | Customer System Detail";
        }
        if (pagename == "customerdetail")
        {
            pagetitle.Text = "SolarMiner | Customer System Detail";
        }
        if (pagename == "instavailable")
        {
            pagetitle.Text = "SolarMiner | Installer Available";
        }
        if (pagename == "printinstallation")
        {
            pagetitle.Text = "SolarMiner | Installations";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "SolarMiner | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "SolarMiner | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "SolarMiner | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "SolarMiner | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "SolarMiner | Maintenance";
        }
        if (pagename == "mtcecall")
        {
            pagetitle.Text = "SolarMiner | Maintenance";
        }
        if (pagename == "stocktransferreportdetail")
        {
            pagetitle.Text = "SolarMiner | Stock Transfer Report";
        }
        if (pagename == "promotracker")
        {
            pagetitle.Text = "SolarMiner | Promo Tracker";
        }
        if (pagename == "unittype")
        {
            pagetitle.Text = "SolarMiner | Unit Type";
        }
        if (pagename == "streettype")
        {
            pagetitle.Text = "SolarMiner | Street Type";
        }
        if (pagename == "streetname")
        {
            pagetitle.Text = "SolarMiner | Street Name";
        }
        if (pagename == "promooffer")
        {
            pagetitle.Text = "SolarMiner | Promo Offer";
        }
        if (pagename == "updateformbayId")
        {
            pagetitle.Text = "SolarMiner | updateformbayId";
        }
        if (pagename == "pickup")
        {
            pagetitle.Text = "SolarMiner | Pick Up";
        }
        if (pagename == "paywaytracker")
        {
            pagetitle.Text = "SolarMiner | PayWay";
        }
        if (pagename == "quickform")
        {
            pagetitle.Text = "SolarMiner | Quick Form";
        }
        if (pagename == "clickcustomer")
        {
            pagetitle.Text = "SolarMiner | Click Customer";
        }



    }


    protected void LoginStatus1_LoggedOut(object sender, EventArgs e)
    {
        Session["userid"] = "";
        Session.Abandon();
        Response.Redirect("~/admin/Default.aspx");
    }


}


