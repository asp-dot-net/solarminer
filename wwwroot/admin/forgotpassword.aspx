﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin/templates/loginmaster.master" AutoEventWireup="true" CodeFile="forgotpassword.aspx.cs" Inherits="admin_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
.toast-error2{background-color:#fff!important;
 background: #fff!important; padding:10px; border:1px solid #eaeaea; border-radius:3px;
    border-left: 6px solid #e74c3c!important;}
</style>
    <div class="login-container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center m-b-md">
                    <h3>Forgot Password</h3>
                    <%--<small>This is the best app ever!</small>--%>
                </div>
                
              <asp:Panel ID="pan" CssClass="hpanel" runat="server" >
                    <div class="hpanel">
                        
                        <asp:Panel runat="server" ID="panel11">
                            <div class="panel-body">
                                <asp:Panel ID="PanSuccess" runat="server" Visible="false" CssClass="alert alert-success">

                            <strong>Success ! </strong>
                            &nbsp;
                            <asp:Label ID="lblSuccess" runat="server"
                                Text="Your password has been sent to you. Please check your Email."></asp:Label>

                        </asp:Panel>
                        <asp:Panel ID="Panfail" runat="server" Visible="false">
                            <div class="toast-error2">
                                <strong>
                                    <asp:Literal ID="Literal2" runat="server" Text="Failure !"></asp:Literal>
                                    <%-- Success !--%>
                                </strong>
                                <br />
                                <asp:Label ID="Label1" runat="server"
                                    Text="Invalid Username"></asp:Label>
                                <%-- password_change--%>
                            </div>
                        </asp:Panel>
                                <table class="loginForm w300" runat="server" id="tabform">
                                    <tr>
                                        <td>
                                            <label class="field-text">User Name</label>
                                            <div class="clear"></div>

                                            <asp:TextBox CssClass="form-control" ID="UserName" runat="server" MaxLength="100" Width="300"></asp:TextBox>

                                            <%--   <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="UserName"
                                                    ErrorMessage=" Enter Valid E-Mail Address" Display="Dynamic" SetFocusOnError="True"
                                                    ValidationGroup="PasswordRecovery1" ForeColor="Red" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                                </asp:RegularExpressionValidator>--%>
                                            <asp:RequiredFieldValidator ID="reqvalUserName" runat="server"
                                                ControlToValidate="UserName" CssClass="requiredfield" ErrorMessage="* Required"
                                                Display="dynamic" ValidationGroup="PasswordRecovery1"></asp:RequiredFieldValidator>


                                            <%--    <input class="field lgFld" type="text" name="" >--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%--  <a href="#" class="button logBtn">Submit</a> --%>
                                            <asp:LinkButton ID="SubmitButton" CausesValidation="true" CssClass="btn btn-success btn-block" runat="server"
                                                OnClick="SubmitButton_Click1" CommandName="Submit" ValidationGroup="PasswordRecovery1" Style="width: 100px; float: left;"
                                                Text=" Submit " />
                                            <asp:LinkButton ID="LinkButton1" CausesValidation="false" CssClass="btn btn-default" runat="server"
                                                OnClick="LinkButton1_Click" Style="width: 100px; margin-left: 100px;"
                                                Text=" Cancel " />
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </asp:Panel>
                        
            </div>
                    </asp:Panel>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <%--<strong>HOMER</strong> - AngularJS Responsive WebApp--%>
                <%--<br />--%>
                2016 Copyright Achievers Energy
            </div>
        </div>
    </div>

</asp:Content>

