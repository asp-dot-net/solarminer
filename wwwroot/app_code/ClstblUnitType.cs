using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
	public struct SttblUnitType
	{
		public string UnitTypeName;
		public string UnitTypeCode;
    public string GreenBoatId;

	}


public class ClstblUnitType
{
	public static SttblUnitType tblUnitType_SelectByUnitTypeID (String UnitTypeID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblUnitType_SelectByUnitTypeID";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@UnitTypeID";
		param.Value = UnitTypeID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// execute the stored procedure
		DataTable table = DataAccess.ExecuteSelectCommand(comm);
		// wrap retrieved data into a ProductDetails object
		 SttblUnitType details = new SttblUnitType();
		if (table.Rows.Count > 0)
		{
			// get the first table row
			DataRow dr = table.Rows[0];
			// get product details
			details.UnitTypeName = dr["UnitTypeName"].ToString();
			details.UnitTypeCode = dr["UnitTypeCode"].ToString();
            details.GreenBoatId = dr["GreenBoatId"].ToString();

		}
		// return structure details
		return details;
	}
	public static DataTable tblUnitType_Select ()
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblUnitType_Select";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
    public static DataTable tblUnitType_SelectAsc()
	{
		DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblUnitType_SelectAsc";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
	public static int tblUnitType_Insert ( String UnitTypeName, String UnitTypeCode,string GreenBoatId)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblUnitType_Insert";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@UnitTypeName";
		param.Value = UnitTypeName;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@UnitTypeCode";
		param.Value = UnitTypeCode;
		param.DbType = DbType.String;
		param.Size = 10;
		comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GreenBoatId";
        param.Value = GreenBoatId;
        param.DbType = DbType.Int32;
        param.Size = 10;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
		return id;	}
	public static bool tblUnitType_Update (string UnitTypeID, String UnitTypeName, String UnitTypeCode, string GreenBoatId)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblUnitType_Update";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@UnitTypeID";
		param.Value = UnitTypeID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@UnitTypeName";
		param.Value = UnitTypeName;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@UnitTypeCode";
		param.Value = UnitTypeCode;
		param.DbType = DbType.String;
		param.Size = 10;
		comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GreenBoatId";
        param.Value = GreenBoatId;
        param.DbType = DbType.Int32;
        param.Size = 10;
        comm.Parameters.Add(param);

        int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		return (result != -1);
	}
	public static bool tblUnitType_Delete (string UnitTypeID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblUnitType_Delete";
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@UnitTypeID";
		param.Value = UnitTypeID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
		}
		return (result != -1);
	}
    public static DataTable tblUnitTypeGetDataBySearch(string alpha)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure type
        comm.CommandText = "tblUnitTypeGetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
}