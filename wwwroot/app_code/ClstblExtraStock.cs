using System;
using System.Data;
using System.Data.Common;

public struct SttblExtraStock
{
    public string ExtraStockID;
    public string ProjectID;
    public string InstallerID;
    public string StockItemID;
    public string ExtraStock;
    public string StockLocation;
}


public class ClstblExtraStock
{
    public static SttblExtraStock tblExtraStock_SelectByExtraStockID(String ExtraStockID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblExtraStock_SelectByExtraStockID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ExtraStockID";
        param.Value = ExtraStockID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblExtraStock details = new SttblExtraStock();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.ProjectID = dr["ProjectID"].ToString();
            details.InstallerID = dr["InstallerID"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();
            details.ExtraStock = dr["ExtraStock"].ToString();
            details.StockLocation = dr["StockLocation"].ToString();

        }
        // return structure details
        return details;
    }
    public static SttblExtraStock tblExtraStock_SelectByProjectID(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblExtraStock_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        SttblExtraStock details = new SttblExtraStock();

        if (table.Rows.Count > 0)
        {
            DataRow dr = table.Rows[0];
            details.ExtraStockID = dr["ExtraStockID"].ToString();
            details.InstallerID = dr["InstallerID"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();
            details.ExtraStock = dr["ExtraStock"].ToString();
            details.StockLocation = dr["StockLocation"].ToString();
        }
        return details;
    }
    public static DataTable tblExtraStock_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblExtraStock_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblExtraStock_Insert(String ProjectID, String InstallerID, String StockItemID, String ExtraStock, string StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblExtraStock_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        param.Value = InstallerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExtraStock";
        param.Value = ExtraStock;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        param.Value = StockLocation;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblExtraWholesaleStock_Insert(String ProjectID, String InstallerID, String StockItemID, String ExtraStock, string StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblExtraStock_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        param.Value = InstallerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExtraStock";
        param.Value = ExtraStock;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        param.Value = StockLocation;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblExtraStock_Update(String ProjectID, String InstallerID, String StockItemID, String ExtraStock, string StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblExtraStock_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        param.Value = InstallerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExtraStock";
        param.Value = ExtraStock;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        param.Value = StockLocation;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblExtraStock_InsertUpdate(Int32 ExtraStockID, String ProjectID, String InstallerID, String StockItemID, String ExtraStock)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblExtraStock_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ExtraStockID";
        param.Value = ExtraStockID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        param.Value = InstallerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExtraStock";
        param.Value = ExtraStock;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblExtraStock_Delete(string ExtraStockID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblExtraStock_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ExtraStockID";
        param.Value = ExtraStockID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static bool tblExtraStock_DeleteByProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblExtraStock_DeleteByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static DataTable tblExtraStock_SelectByInstallerID(string InstallerID, string StockItemID, string StockLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblExtraStock_SelectByInstallerID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        param.Value = InstallerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocation";
        param.Value = StockLocation;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }   
}