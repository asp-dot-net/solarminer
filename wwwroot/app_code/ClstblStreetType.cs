using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
	public struct SttblStreetType
	{
		public string StreetTypeName;
		public string StreetTypeCode;
    public string GreenBoatStreetType;

}

    public struct Sttbl_formbaystreettype
    {
        public string Id;
        public string StreetType;
        public string StreetCode;
    public string GreenBoatStreetType;
}


public class ClstblStreetType
{
	public static SttblStreetType tblStreetType_SelectByStreetTypeID (String StreetTypeID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblStreetType_SelectByStreetTypeID";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@StreetTypeID";
		param.Value = StreetTypeID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// execute the stored procedure
		DataTable table = DataAccess.ExecuteSelectCommand(comm);
		// wrap retrieved data into a ProductDetails object
		 SttblStreetType details = new SttblStreetType();
		if (table.Rows.Count > 0)
		{
			// get the first table row
			DataRow dr = table.Rows[0];
			// get product details
			details.StreetTypeName = dr["StreetTypeName"].ToString();
			details.StreetTypeCode = dr["StreetTypeCode"].ToString();

		}
		// return structure details
		return details;
	}
	public static DataTable tblStreetType_Select ()
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblStreetType_Select";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
    public static DataTable tblStreetType_SelectAsc()
	{
		DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStreetType_SelectAsc";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
	public static int tblStreetType_Insert ( String StreetTypeName, String StreetTypeCode)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblStreetType_Insert";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@StreetTypeName";
		param.Value = StreetTypeName;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@StreetTypeCode";
		param.Value = StreetTypeCode;
		param.DbType = DbType.String;
		param.Size = 10;
		comm.Parameters.Add(param);

      

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
		return id;	}
	public static bool tblStreetType_Update (string StreetTypeID, String StreetTypeName, string StreetTypeCode)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblStreetType_Update";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@StreetTypeID";
		param.Value = StreetTypeID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@StreetTypeName";
		param.Value = StreetTypeName;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@StreetTypeCode";
		param.Value = StreetTypeCode;
		param.DbType = DbType.String;
		param.Size = 10;
		comm.Parameters.Add(param);

   

        int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		return (result != -1);
	}
	public static bool tblStreetType_Delete (string StreetTypeID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblStreetType_Delete";
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@StreetTypeID";
		param.Value = StreetTypeID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
		}
		return (result != -1);
	}
    public static DataTable tblStreetTypeGetDataBySearch(string alpha)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure type
        comm.CommandText = "tblStreetTypeGetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tbl_formbaystreettype_SelectbyAsc()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_formbaystreettype_SelectbyAsc";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tbl_formbaystreetname_SelectbyAsc()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_formbaystreetname_SelectbyAsc";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tbl_formbayunittype_SelectbyAsc()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_formbayunittype_SelectbyAsc";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_formbaystreettype_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_formbaystreettype_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_formbaystreettype_GetDataBySearch(string alpha)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure type
        comm.CommandText = "tbl_formbaystreettype_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static int tbl_formbayStreetName_Insert(String StreetTypeName, String StreetTypeCode,string GreenBoatStreetType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_formbayStreetName_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StreetTypeName";
        param.Value = StreetTypeName;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetTypeCode";
        param.Value = StreetTypeCode;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GreenBoatStreetType";
        param.Value = GreenBoatStreetType;
        param.DbType = DbType.Int32;
        param.Size = 10;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tbl_formbaystreettype_Update(string StreetTypeID, String StreetTypeName, String StreetTypeCode,string GreenBoatStreetType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_formbaystreettype_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StreetTypeID";
        param.Value = StreetTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetTypeName";
        param.Value = StreetTypeName;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetTypeCode";
        param.Value = StreetTypeCode;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@GreenBoatStreetType";
        param.Value = GreenBoatStreetType;
        param.DbType = DbType.Int32;
        param.Size = 10;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_formbaystreettype_Delete(string StreetTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_formbaystreettype_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StreetTypeID";
        param.Value = StreetTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static Sttbl_formbaystreettype tbl_formbaystreettype_SelectByStreetTypeID(String Id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_formbaystreettype_SelectByStreetTypeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StreetTypeID";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        Sttbl_formbaystreettype details = new Sttbl_formbaystreettype();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.Id = dr["Id"].ToString();
            details.StreetCode = dr["StreetCode"].ToString();
            details.StreetType = dr["StreetType"].ToString();
            details.GreenBoatStreetType = dr["GreenBoatStreetType"].ToString();
        }
        // return structure details
        return details;
    }


    public static Sttbl_formbaystreettype tbl_formbaystreettype_SelectByStreetCode(String StreetCode)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_formbaystreettype_SelectByStreetCode";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StreetCode";
        param.Value = @StreetCode;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        Sttbl_formbaystreettype details = new Sttbl_formbaystreettype();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.Id = dr["Id"].ToString();
            details.StreetCode = dr["StreetCode"].ToString();
            details.StreetType = dr["StreetType"].ToString();
            details.GreenBoatStreetType = dr["GreenBoatStreetType"].ToString();
        }
        // return structure details
        return details;
    }
}