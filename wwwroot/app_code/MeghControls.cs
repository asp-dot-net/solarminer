﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Text;

/// <summary>
/// Summary description for MeghControls
/// </summary>
public class MeghControls
{
    public MeghControls()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static string Left(string param, int length)
    {
        //we start at 0 since we want to get the characters starting from the
        //left and with the specified lenght and assign it to a variable
        string result = param.Substring(0, length);
        //return the result of the operation
        return result;
    }
    public static string DrawLink(string querystring, int pageNumber)
    {
        if (querystring != "")
        {
            return "?" + querystring + "&PageNo=" + pageNumber; //+ "&cid=" + Request.QueryString["id"].ToString();
        }
        else
        {
            return "?PageNo=" + pageNumber; //+ "&cid=" + Request.QueryString["id"].ToString();
        }
    }
    public static string DrawPaging(int pageNumber, int pageCount, bool toppagination, bool bottompagination, string querystring, string PagingStartTag, string PagingEndTag)
    {
        StringBuilder sb = new StringBuilder();
        int x = 0;
        int y = 0;
        int z = 0;
        int pageEnd = 0;
        int pageStart = 0;
        bool prevflag = true;
        bool nextflag = true;

        if (pageCount > 1)
        {
            //handle 10 at a time
            //get the start and end
            //10, 20, 30 appear with old set

            if (pageNumber % 10 == 0)
            {
                pageStart = pageNumber - 1;
            }
            else
            {
                y = pageNumber.ToString().Length;
                //1 - 9
                if (y == 1)
                {
                    pageStart = 1;
                }
                else
                {
                    z = Convert.ToInt32(Left(pageNumber.ToString(), y - 1));
                    pageStart = (z * 10) + 1;
                }
            }

            if (pageStart + 9 > pageCount)
            {
                pageEnd = pageCount;
                if (pageNumber - (10 - ((pageEnd - (pageNumber)) + 1)) > 0)
                {
                    pageStart = pageNumber - (10 - ((pageEnd - (pageNumber)) + 1));
                }
            }
            else
            {
                pageEnd = pageStart + 9;
            }
            //draw the page numbers (current page is not a hyperlink it is inbold in square brackets)
            for (x = pageStart; x <= pageEnd; x++)
            {
                if (x == pageNumber)
                {
                    sb.Append(PagingStartTag + "<a class='active'>");
                    sb.Append(x);
                    sb.Append("</a>" + PagingEndTag);
                    //lblpangeno.Text = x.ToString();
                }
                else
                {
                    sb.Append(PagingStartTag + "<a href=");
                    sb.Append(DrawLink(querystring, x));
                    sb.Append(">");
                    sb.Append(x);
                    sb.Append("</a>" + PagingEndTag);
                }
            }
            //draw the previous button if not at the first item on the first page
            if (pageNumber != 1)
            {
                prevflag = true;
            }
            else
            {
                prevflag = false;
            }

            //draw the next button if not at the last page
            if (pageNumber < pageCount)
            {
                nextflag = true;

            }
            else
            {
                nextflag = false;
            }
        }
        else
        {
            // tblnavi.Visible = false;
        }
        return sb.ToString() + "|" + prevflag.ToString() + "|" + nextflag.ToString();
    }

}
