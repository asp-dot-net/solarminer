using System;
using System.Data;
using System.Data.Common;

public struct SttblCustSourceSub
{
    public string CustSourceSubID;
    public string CustSourceID;
    public string CustSourceSub;
    public string Active;
    public string Seq;
}


public class ClstblCustSourceSub
{
    public static SttblCustSourceSub tblCustSourceSub_SelectByCustSourceSubID(String CustSourceSubID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_SelectByCustSourceSubID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        param.Value = CustSourceSubID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblCustSourceSub details = new SttblCustSourceSub();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustSourceID = dr["CustSourceID"].ToString();
            details.CustSourceSub = dr["CustSourceSub"].ToString();
            details.Active = dr["Active"].ToString();
            details.Seq = dr["Seq"].ToString();

        }
       
        // return structure details
        return details;
    }

    public static SttblCustSourceSub tblCustSourceSub_SelectByCustSourceSub(String CustSourceSub)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_SelectByCustSourceSub";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSub";
        param.Value = CustSourceSub;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblCustSourceSub details = new SttblCustSourceSub();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustSourceSubID = dr["CustSourceSubID"].ToString();
            details.CustSourceID = dr["CustSourceID"].ToString();
            details.CustSourceSub = dr["CustSourceSub"].ToString();
            details.Active = dr["Active"].ToString();
            details.Seq = dr["Seq"].ToString();

        }
        // return structure details
        return details;
    }


    public static DataTable tblCustSourceSub_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustSourceSub_SelectActive()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_SelectActive";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustSourceSub_SelectByCSId(string CustSourceID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_SelectByCSId";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        param.Value = CustSourceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblCustSourceSub_Insert(String CustSourceID, String CustSourceSub, String Active, String Seq)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSub";
        param.Value = CustSourceSub;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblCustSourceSub_Update(string CustSourceSubID, String CustSourceID, String CustSourceSub, String Active, String Seq)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        param.Value = CustSourceSubID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSub";
        param.Value = CustSourceSub;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblCustSourceSub_InsertUpdate(Int32 CustSourceSubID, String CustSourceID, String CustSourceSub, String Active, String Seq)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        param.Value = CustSourceSubID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        param.Value = CustSourceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSub";
        param.Value = CustSourceSub;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblCustSourceSub_Delete(string CustSourceSubID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        param.Value = CustSourceSubID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblCustSourceSub_GetDataByAlpha(string alpha)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_GetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblCustSourceSub_GetDataBySearch(string alpha ,string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static int tblCustSourceSub_Exists(string CustSourceSub)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSub";
        param.Value = CustSourceSub;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int tblCustSourceSub_ExistsById(string CustSourceSub, string CustSourceSubID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustSourceSub_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSub";
        param.Value = CustSourceSub;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        param.Value = CustSourceSubID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }
}