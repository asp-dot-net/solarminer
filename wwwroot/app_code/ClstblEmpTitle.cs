using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
public struct SttblEmpTitle
{
    public string EmpTitle;
    public string Active;

}


public class ClstblEmpTitle
{
    public static SttblEmpTitle tblEmpTitle_SelectByEmpTitleID(String EmpTitleID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmpTitle_SelectByEmpTitleID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmpTitleID";
        param.Value = EmpTitleID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblEmpTitle details = new SttblEmpTitle();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.EmpTitle = dr["EmpTitle"].ToString();
            details.Active = dr["Active"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblEmpTitle_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmpTitle_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblEmpTitle_SelectActive()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmpTitle_SelectActive";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblEmpTitle_Insert(String EmpTitle, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmpTitle_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmpTitle";
        param.Value = EmpTitle;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblEmpTitle_Update(string EmpTitleID, String EmpTitle, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmpTitle_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmpTitleID";
        param.Value = EmpTitleID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpTitle";
        param.Value = EmpTitle;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblEmpTitle_InsertUpdate(Int32 EmpTitleID, String EmpTitle, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmpTitle_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmpTitleID";
        param.Value = EmpTitleID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpTitle";
        param.Value = EmpTitle;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblEmpTitle_Delete(string EmpTitleID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblEmpTitle_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmpTitleID";
        param.Value = EmpTitleID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static DataTable tblEmpTitleGetDataBySearch(string alpha)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure type
        comm.CommandText = "tblEmpTitleGetDataBySearch";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static int EmpTitleExists(string title)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "EmpTitleExists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@title";
        param.Value = title;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int EmpTitleExistsWithID(string title, string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "EmpTitleExistsWithID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@title";
        param.Value = title;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }

}