using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
public struct SttblStreetName
{
    public string StreetName;
}

public class ClstblStreetName
{
    public static SttblStreetName tblStreetName_SelectByStreetTypeID(String Id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStreetName_SelectByStreetTypeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblStreetName details = new SttblStreetName();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.StreetName = dr["StreetName"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblStreetName_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStreetName_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    //public static DataTable tblStreetType_SelectAsc()
    //{
    //    DbCommand comm = DataAccess.CreateCommand();
    //    comm.CommandText = "tblStreetType_SelectAsc";

    //    DataTable result = new DataTable();
    //    try
    //    {
    //        result = DataAccess.ExecuteSelectCommand(comm);
    //    }
    //    catch
    //    {
    //        // log errors if any
    //    }
    //    return result;
    //}
    public static int tblStreetName_Insert(String StreetName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStreetName_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StreetName";
        param.Value = StreetName;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblStreetName_Update(string Id, String StreetName)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblStreetName_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Id";
        param.Value = Id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetName";
        param.Value = StreetName;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    //public static bool tblStreetType_Delete(string StreetTypeID)
    //{
    //    DbCommand comm = DataAccess.CreateCommand();
    //    comm.CommandText = "tblStreetType_Delete";
    //    DbParameter param = comm.CreateParameter();
    //    param.ParameterName = "@StreetTypeID";
    //    param.Value = StreetTypeID;
    //    param.DbType = DbType.Int32;
    //    comm.Parameters.Add(param);
    //    int result = -1;
    //    try
    //    {
    //        result = DataAccess.ExecuteNonQuery(comm);
    //    }
    //    catch
    //    {
    //    }
    //    return (result != -1);
    //}
    public static DataTable tblStreetNameGetDataBySearch(string alpha)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure type
        comm.CommandText = "tblStreetNameGetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable tbl_formbaystreettype_SelectbyAsc()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_formbaystreettype_SelectbyAsc";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tbl_formbaystreetname_SelectbyAsc()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_formbaystreetname_SelectbyAsc";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tbl_formbayunittype_SelectbyAsc()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_formbayunittype_SelectbyAsc";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}