using System;
using System.Data;
using System.Data.Common;

public struct SttblProjectMaintenance
{
    public string ProjectID;
    public string EmployeeID;
    public string ProjectMtceReasonID;
    public string ProjectMtceReasonSubID;
    public string ProjectMtceCallID;
    public string ProjectMtceStatusID;
    public string OpenDate;
    public string CustomerInput;
    public string Description;
    public string InstallerID;
    public string FaultIdentified;
    public string ActionRequired;
    public string PropResolutionDate;
    public string CompletionDate;
    public string WorkDone;
    public string CurrentPhoneContact;
    public string Warranty;
    public string CasualCall;
    public string CasualCustomer;
    public string CasualContFirst;
    public string CasualContLast;
    public string CasualCustPhone;
    public string CasualContMobile;
    public string CasualContEmail;
    public string CasualAddress;
    public string CasualCity;
    public string CasualState;
    public string CasualPostCode;
    public string CasualSystemDetails;
    public string CasualRefNumber;
    public string MtceCost;
    public string MtceDiscount;
    public string MtceBalance;
    public string FPTransTypeID;
    public string MtceRecBy;
    public string upsize_ts;
    public string ServiceCost;

    public string Installer;
    public string MtceRecByName;
    public string Employee;
    public string ProjectMtceReason;
    public string ProjectMtceReasonSub;
    public string ProjectMtceCall;
    public string ProjectMtceStatus;
    public string FPTransType;
}

public class ClstblProjectMaintenance
{


    public static string tblMaintainance_SystemDetail(string ProjectID, string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMaintainance_SystemDetail";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        string dt = DataAccess.ExecuteScalar(comm);
        return dt;

    }

    public static DataTable tblProjectMaintenance_checkExistProjectID(string ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMaintenance_checkExistProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable dt = DataAccess.ExecuteSelectCommand(comm);
        return dt;

    }



    public static SttblProjectMaintenance tblProjectMaintenance_SelectByProjectMaintenanceID(String ProjectMaintenanceID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMaintenance_SelectByProjectMaintenanceID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectMaintenanceID";
        param.Value = ProjectMaintenanceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblProjectMaintenance details = new SttblProjectMaintenance();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.ProjectID = dr["ProjectID"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.ProjectMtceReasonID = dr["ProjectMtceReasonID"].ToString();
            details.ProjectMtceReasonSubID = dr["ProjectMtceReasonSubID"].ToString();
            details.ProjectMtceCallID = dr["ProjectMtceCallID"].ToString();
            details.ProjectMtceStatusID = dr["ProjectMtceStatusID"].ToString();
            details.OpenDate = dr["OpenDate"].ToString();
            details.CustomerInput = dr["CustomerInput"].ToString();
            details.Description = dr["Description"].ToString();
            details.InstallerID = dr["InstallerID"].ToString();
            details.FaultIdentified = dr["FaultIdentified"].ToString();
            details.ActionRequired = dr["ActionRequired"].ToString();
            details.PropResolutionDate = dr["PropResolutionDate"].ToString();
            details.CompletionDate = dr["CompletionDate"].ToString();
            details.WorkDone = dr["WorkDone"].ToString();
            details.CurrentPhoneContact = dr["CurrentPhoneContact"].ToString();
            details.Warranty = dr["Warranty"].ToString();
            details.CasualCall = dr["CasualCall"].ToString();
            details.CasualCustomer = dr["CasualCustomer"].ToString();
            details.CasualContFirst = dr["CasualContFirst"].ToString();
            details.CasualContLast = dr["CasualContLast"].ToString();
            details.CasualCustPhone = dr["CasualCustPhone"].ToString();
            details.CasualContMobile = dr["CasualContMobile"].ToString();
            details.CasualContEmail = dr["CasualContEmail"].ToString();
            details.CasualAddress = dr["CasualAddress"].ToString();
            details.CasualCity = dr["CasualCity"].ToString();
            details.CasualState = dr["CasualState"].ToString();
            details.CasualPostCode = dr["CasualPostCode"].ToString();
            details.CasualSystemDetails = dr["CasualSystemDetails"].ToString();
            details.CasualRefNumber = dr["CasualRefNumber"].ToString();
            details.MtceCost = dr["MtceCost"].ToString();
            details.MtceDiscount = dr["MtceDiscount"].ToString();
            details.MtceBalance = dr["MtceBalance"].ToString();
            details.FPTransTypeID = dr["FPTransTypeID"].ToString();
            details.MtceRecBy = dr["MtceRecBy"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.ServiceCost = dr["ServiceCost"].ToString();

            details.Installer = dr["Installer"].ToString();
            details.MtceRecByName = dr["MtceRecByName"].ToString();
            details.Employee = dr["Employee"].ToString();
            details.ProjectMtceReason = dr["ProjectMtceReason"].ToString();
            details.ProjectMtceReasonSub = dr["ProjectMtceReasonSub"].ToString();
            details.ProjectMtceCall = dr["ProjectMtceCall"].ToString();
            details.ProjectMtceStatus = dr["ProjectMtceStatus"].ToString();
            details.FPTransType = dr["FPTransType"].ToString();
        }
        // return structure details
        return details;
    }

    public static SttblProjectMaintenance tblProjectMaintenance_SelectByProjectMaintainanceByProjectID(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMaintenance_SelectByProjectMaintainanceByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblProjectMaintenance details = new SttblProjectMaintenance();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.ProjectID = dr["ProjectID"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.ProjectMtceReasonID = dr["ProjectMtceReasonID"].ToString();
            details.ProjectMtceReasonSubID = dr["ProjectMtceReasonSubID"].ToString();
            details.ProjectMtceCallID = dr["ProjectMtceCallID"].ToString();
            details.ProjectMtceStatusID = dr["ProjectMtceStatusID"].ToString();
            details.OpenDate = dr["OpenDate"].ToString();
            details.CustomerInput = dr["CustomerInput"].ToString();
            details.Description = dr["Description"].ToString();
            details.InstallerID = dr["InstallerID"].ToString();
            details.FaultIdentified = dr["FaultIdentified"].ToString();
            details.ActionRequired = dr["ActionRequired"].ToString();
            details.PropResolutionDate = dr["PropResolutionDate"].ToString();
            details.CompletionDate = dr["CompletionDate"].ToString();
            details.WorkDone = dr["WorkDone"].ToString();
            details.CurrentPhoneContact = dr["CurrentPhoneContact"].ToString();
            details.Warranty = dr["Warranty"].ToString();
            details.CasualCall = dr["CasualCall"].ToString();
            details.CasualCustomer = dr["CasualCustomer"].ToString();
            details.CasualContFirst = dr["CasualContFirst"].ToString();
            details.CasualContLast = dr["CasualContLast"].ToString();
            details.CasualCustPhone = dr["CasualCustPhone"].ToString();
            details.CasualContMobile = dr["CasualContMobile"].ToString();
            details.CasualContEmail = dr["CasualContEmail"].ToString();
            details.CasualAddress = dr["CasualAddress"].ToString();
            details.CasualCity = dr["CasualCity"].ToString();
            details.CasualState = dr["CasualState"].ToString();
            details.CasualPostCode = dr["CasualPostCode"].ToString();
            details.CasualSystemDetails = dr["CasualSystemDetails"].ToString();
            details.CasualRefNumber = dr["CasualRefNumber"].ToString();
            details.MtceCost = dr["MtceCost"].ToString();
            details.MtceDiscount = dr["MtceDiscount"].ToString();
            details.MtceBalance = dr["MtceBalance"].ToString();
            details.FPTransTypeID = dr["FPTransTypeID"].ToString();
            details.MtceRecBy = dr["MtceRecBy"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.ServiceCost = dr["ServiceCost"].ToString();

            details.Installer = dr["Installer"].ToString();
            details.MtceRecByName = dr["MtceRecByName"].ToString();
            details.Employee = dr["Employee"].ToString();
            details.ProjectMtceReason = dr["ProjectMtceReason"].ToString();
            details.ProjectMtceReasonSub = dr["ProjectMtceReasonSub"].ToString();
            details.ProjectMtceCall = dr["ProjectMtceCall"].ToString();
            details.ProjectMtceStatus = dr["ProjectMtceStatus"].ToString();
            details.FPTransType = dr["FPTransType"].ToString();
        }
        // return structure details
        return details;
    }
    public static DataTable tblProjectMaintenance_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMaintenance_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjectMaintenance_SelectCasual_1(string CustomerName, string MobileNo, string StreetName, string City, string CasualCall, string ProjectNumber, string InstallerID, string EmployeeID, string EmpEmail, string startdate, string enddate, string InstallState, string InstallPostCode, string DateType, string completiondate, string calldate, string propdate, string warranty, string cnclreason)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMaintenance_SelectCasual_1";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerName";
        if (CustomerName != string.Empty)
            param.Value = CustomerName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MobileNo";
        if (MobileNo != string.Empty)
            param.Value = MobileNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetName";
        if (StreetName != string.Empty)
            param.Value = StreetName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@City";
        if (City != string.Empty)
            param.Value = City;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCall";
        param.Value = CasualCall;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpEmail";
        if (EmpEmail != string.Empty)
            param.Value = EmpEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCode";
        if (InstallPostCode != string.Empty)
            param.Value = InstallPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@completiondate";
        if (completiondate != string.Empty)
            param.Value = completiondate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@calldate";
        if (calldate != string.Empty)
            param.Value = calldate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@propdate";
        if (propdate != string.Empty)
            param.Value = propdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@warrnty";
        if (warranty != string.Empty)
            param.Value = warranty;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@cnclreason";
        if (cnclreason != string.Empty)
            param.Value = cnclreason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjectMaintenance_SelectCasual(string CustomerName,string MobileNo,string StreetName,string City,string CasualCall, string ProjectNumber, string InstallerID, string EmployeeID,string EmpEmail, string startdate, string enddate, string InstallState, string InstallPostCode, string DateType, string completiondate, string calldate, string propdate, string warranty, string cnclreason)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMaintenance_SelectCasual";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerName";
        if (CustomerName != string.Empty)
            param.Value = CustomerName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MobileNo";
        if (MobileNo != string.Empty)
            param.Value = MobileNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetName";
        if (StreetName != string.Empty)
            param.Value = StreetName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@City";
        if (City != string.Empty)
            param.Value = City;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCall";
        param.Value = CasualCall;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectNumber";
        if (ProjectNumber != string.Empty)
            param.Value = ProjectNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmpEmail";
        if (EmpEmail != string.Empty)
            param.Value = EmpEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallPostCode";
        if (InstallPostCode != string.Empty)
            param.Value = InstallPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@completiondate";
        if (completiondate != string.Empty)
            param.Value = completiondate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@calldate";
        if (calldate != string.Empty)
            param.Value = calldate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@propdate";
        if (propdate != string.Empty)
            param.Value = propdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@warrnty";
        if (warranty != string.Empty)
            param.Value = warranty;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@cnclreason";
        if (cnclreason != string.Empty)
            param.Value = cnclreason;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjectMaintenance_SelectByDate(string CasualCall, String OpenDate, string alpha, string InstallState, string Installer, string userid, string formbay)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMaintenance_SelectByDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CasualCall";
        param.Value = CasualCall;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OpenDate";
        param.Value = OpenDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        if (alpha != string.Empty)
            param.Value = alpha;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallState";
        if (InstallState != string.Empty)
            param.Value = InstallState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Installer";
        if (Installer != string.Empty)
            param.Value = Installer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        if (userid != string.Empty)
            param.Value = userid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@formbay";
        if (formbay != string.Empty)
            param.Value = formbay;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjectMaintenance_SelectByProjectID(String ProjectID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMaintenance_SelectByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblProjectMaintenance_Insert(String ProjectID, String EmployeeID, String ProjectMtceReasonID, String ProjectMtceReasonSubID, String ProjectMtceCallID, String ProjectMtceStatusID, String OpenDate, String CustomerInput, String Description, String InstallerID, String FaultIdentified, String ActionRequired, String PropResolutionDate, String CompletionDate, String WorkDone, String CurrentPhoneContact, String Warranty, String CasualCall, String CasualCustomer, String CasualContFirst, String CasualContLast, String CasualCustPhone, String CasualContMobile, String CasualContEmail, String CasualAddress, String CasualCity, String CasualState, String CasualPostCode, String CasualSystemDetails, String CasualRefNumber, String MtceCost, String MtceDiscount, String MtceBalance, String FPTransTypeID, String MtceRecBy, string ServiceCost)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMaintenance_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceReasonID";
        if (ProjectMtceReasonID != string.Empty)
            param.Value = ProjectMtceReasonID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceReasonSubID";
        if (ProjectMtceReasonSubID != string.Empty)
            param.Value = ProjectMtceReasonSubID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceCallID";
        if (ProjectMtceCallID != string.Empty)
            param.Value = ProjectMtceCallID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceStatusID";
        if (ProjectMtceStatusID != string.Empty)
            param.Value = ProjectMtceStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OpenDate";
        if (OpenDate != string.Empty)
            param.Value = OpenDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerInput";
        if (CustomerInput != string.Empty)
            param.Value = CustomerInput;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Description";
        if (Description != string.Empty)
            param.Value = Description;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FaultIdentified";
        if (FaultIdentified != string.Empty)
            param.Value = FaultIdentified;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActionRequired";
        if (ActionRequired != string.Empty)
            param.Value = ActionRequired;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PropResolutionDate";
        if (PropResolutionDate != string.Empty)
            param.Value = PropResolutionDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompletionDate";
        if (CompletionDate != string.Empty)
            param.Value = CompletionDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WorkDone";
        if (WorkDone != string.Empty)
            param.Value = WorkDone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CurrentPhoneContact";
        if (CurrentPhoneContact != string.Empty)
            param.Value = CurrentPhoneContact;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Warranty";
        if (Warranty != string.Empty)
            param.Value = Warranty;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCall";
        if (CasualCall != string.Empty)
            param.Value = CasualCall;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCustomer";
        if (CasualCustomer != string.Empty)
            param.Value = CasualCustomer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContFirst";
        if (CasualContFirst != string.Empty)
            param.Value = CasualContFirst;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContLast";
        if (CasualContLast != string.Empty)
            param.Value = CasualContLast;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCustPhone";
        if (CasualCustPhone != string.Empty)
            param.Value = CasualCustPhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContMobile";
        if (CasualContMobile != string.Empty)
            param.Value = CasualContMobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContEmail";
        if (CasualContEmail != string.Empty)
            param.Value = CasualContEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualAddress";
        if (CasualAddress != string.Empty)
            param.Value = CasualAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCity";
        if (CasualCity != string.Empty)
            param.Value = CasualCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualState";
        if (CasualState != string.Empty)
            param.Value = CasualState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualPostCode";
        if (CasualPostCode != string.Empty)
            param.Value = CasualPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualSystemDetails";
        if (CasualSystemDetails != string.Empty)
            param.Value = CasualSystemDetails;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 254;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualRefNumber";
        if (CasualRefNumber != string.Empty)
            param.Value = CasualRefNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MtceCost";
        if (MtceCost != string.Empty)
            param.Value = MtceCost;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ServiceCost";
        if (ServiceCost != string.Empty)
            param.Value = ServiceCost;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MtceDiscount";
        if (MtceDiscount != string.Empty)
            param.Value = MtceDiscount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MtceBalance";
        if (MtceBalance != string.Empty)
            param.Value = MtceBalance;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FPTransTypeID";
        if (FPTransTypeID != string.Empty)
            param.Value = FPTransTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MtceRecBy";
        if (MtceRecBy != string.Empty)
            param.Value = MtceRecBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int tblProjectMaintenance_InsertCasual(String CasualCustomer, String CasualContFirst, String CasualContLast, String CasualCustPhone, String CasualContMobile, String CasualContEmail, String CasualAddress, String CasualCity, String CasualState, String CasualPostCode, String CasualSystemDetails, String CasualRefNumber, String EmployeeID, string InstallerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMaintenance_InsertCasual";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CasualCustomer";
        param.Value = CasualCustomer;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContFirst";
        if (CasualContFirst != string.Empty)
            param.Value = CasualContFirst;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContLast";
        if (CasualContLast != string.Empty)
            param.Value = CasualContLast;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCustPhone";
        if (CasualCustPhone != string.Empty)
            param.Value = CasualCustPhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContMobile";
        if (CasualContMobile != string.Empty)
            param.Value = CasualContMobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContEmail";
        if (CasualContEmail != string.Empty)
            param.Value = CasualContEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualAddress";
        if (CasualAddress != string.Empty)
            param.Value = CasualAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCity";
        if (CasualCity != string.Empty)
            param.Value = CasualCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualState";
        if (CasualState != string.Empty)
            param.Value = CasualState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualPostCode";
        if (CasualPostCode != string.Empty)
            param.Value = CasualPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualSystemDetails";
        if (CasualSystemDetails != string.Empty)
            param.Value = CasualSystemDetails;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 254;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualRefNumber";
        if (CasualRefNumber != string.Empty)
            param.Value = CasualRefNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblProjectMaintenance_UpdateCasual(string ProjectMaintenanceID, String CasualCustomer, String CasualContFirst, String CasualContLast, String CasualCustPhone, String CasualContMobile, String CasualContEmail, String CasualAddress, String CasualCity, String CasualState, String CasualPostCode, String CasualSystemDetails, String CasualRefNumber, string InstallerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMaintenance_UpdateCasual";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectMaintenanceID";
        param.Value = ProjectMaintenanceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCustomer";
        if (CasualCustomer != string.Empty)
            param.Value = CasualCustomer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContFirst";
        if (CasualContFirst != string.Empty)
            param.Value = CasualContFirst;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContLast";
        if (CasualContLast != string.Empty)
            param.Value = CasualContLast;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCustPhone";
        if (CasualCustPhone != string.Empty)
            param.Value = CasualCustPhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContMobile";
        if (CasualContMobile != string.Empty)
            param.Value = CasualContMobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContEmail";
        if (CasualContEmail != string.Empty)
            param.Value = CasualContEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualAddress";
        if (CasualAddress != string.Empty)
            param.Value = CasualAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCity";
        if (CasualCity != string.Empty)
            param.Value = CasualCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualState";
        if (CasualState != string.Empty)
            param.Value = CasualState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualPostCode";
        if (CasualPostCode != string.Empty)
            param.Value = CasualPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualSystemDetails";
        if (CasualSystemDetails != string.Empty)
            param.Value = CasualSystemDetails;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 254;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualRefNumber";
        if (CasualRefNumber != string.Empty)
            param.Value = CasualRefNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjectMaintenance_Update(string ProjectMaintenanceID, String ProjectID, String EmployeeID, String ProjectMtceReasonID, String ProjectMtceReasonSubID, String ProjectMtceCallID, String ProjectMtceStatusID, String OpenDate, String CustomerInput, String Description, String InstallerID, String FaultIdentified, String ActionRequired, String PropResolutionDate, String CompletionDate, String WorkDone, String CurrentPhoneContact, String Warranty, String CasualCall, String CasualCustomer, String CasualContFirst, String CasualContLast, String CasualCustPhone, String CasualContMobile, String CasualContEmail, String CasualAddress, String CasualCity, String CasualState, String CasualPostCode, String CasualSystemDetails, String CasualRefNumber, String MtceCost, String MtceDiscount, String MtceBalance, String FPTransTypeID, String MtceRecBy, string ServiceCost)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMaintenance_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectMaintenanceID";
        param.Value = ProjectMaintenanceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceReasonID";
        if (ProjectMtceReasonID != string.Empty)
            param.Value = ProjectMtceReasonID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceReasonSubID";
        if (ProjectMtceReasonSubID != string.Empty)
            param.Value = ProjectMtceReasonSubID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceCallID";
        if (ProjectMtceCallID != string.Empty)
            param.Value = ProjectMtceCallID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceStatusID";
        if (ProjectMtceStatusID != string.Empty)
            param.Value = ProjectMtceStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OpenDate";
        if (OpenDate != string.Empty)
            param.Value = OpenDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerInput";
        param.Value = CustomerInput;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Description";
        param.Value = Description;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FaultIdentified";
        param.Value = FaultIdentified;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActionRequired";
        param.Value = ActionRequired;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PropResolutionDate";
        if (PropResolutionDate != string.Empty)
            param.Value = PropResolutionDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompletionDate";
        if (CompletionDate != string.Empty)
            param.Value = CompletionDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WorkDone";
        param.Value = WorkDone;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CurrentPhoneContact";
        param.Value = CurrentPhoneContact;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Warranty";
        if (Warranty != string.Empty)
            param.Value = Warranty;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCall";
        if (CasualCall != string.Empty)
            param.Value = CasualCall;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCustomer";
        param.Value = CasualCustomer;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContFirst";
        param.Value = CasualContFirst;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContLast";
        param.Value = CasualContLast;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCustPhone";
        param.Value = CasualCustPhone;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContMobile";
        param.Value = CasualContMobile;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContEmail";
        param.Value = CasualContEmail;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualAddress";
        param.Value = CasualAddress;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCity";
        param.Value = CasualCity;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualState";
        param.Value = CasualState;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualPostCode";
        param.Value = CasualPostCode;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualSystemDetails";
        param.Value = CasualSystemDetails;
        param.DbType = DbType.String;
        param.Size = 254;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualRefNumber";
        if (CasualRefNumber != string.Empty)
            param.Value = CasualRefNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MtceCost";
        if (MtceCost != string.Empty)
            param.Value = MtceCost;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ServiceCost";
        if (ServiceCost != string.Empty)
            param.Value = ServiceCost;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);



        param = comm.CreateParameter();
        param.ParameterName = "@MtceDiscount";
        if (MtceDiscount != string.Empty)
            param.Value = MtceDiscount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MtceBalance";
        if (MtceBalance != string.Empty)
            param.Value = MtceBalance;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FPTransTypeID";
        if (FPTransTypeID != string.Empty)
            param.Value = FPTransTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MtceRecBy";
        if (MtceRecBy != string.Empty)
            param.Value = MtceRecBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static bool tblProjectMaintenance_UpdateByProjectID(String ProjectID, String EmployeeID, String ProjectMtceReasonID, String ProjectMtceReasonSubID, String ProjectMtceCallID, String ProjectMtceStatusID, String OpenDate, String CustomerInput, String Description, String InstallerID, String FaultIdentified, String ActionRequired, String PropResolutionDate, String CompletionDate, String WorkDone, String CurrentPhoneContact, String Warranty, String CasualCall, String CasualCustomer, String CasualContFirst, String CasualContLast, String CasualCustPhone, String CasualContMobile, String CasualContEmail, String CasualAddress, String CasualCity, String CasualState, String CasualPostCode, String CasualSystemDetails, String CasualRefNumber, String MtceCost, String MtceDiscount, String MtceBalance, String FPTransTypeID, String MtceRecBy, string ServiceCost)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMaintenance_UpdateByProjectID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (ProjectID != string.Empty)
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceReasonID";
        if (ProjectMtceReasonID != string.Empty)
            param.Value = ProjectMtceReasonID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceReasonSubID";
        if (ProjectMtceReasonSubID != string.Empty)
            param.Value = ProjectMtceReasonSubID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceCallID";
        if (ProjectMtceCallID != string.Empty)
            param.Value = ProjectMtceCallID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceStatusID";
        if (ProjectMtceStatusID != string.Empty)
            param.Value = ProjectMtceStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OpenDate";
        if (OpenDate != string.Empty)
            param.Value = OpenDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerInput";
        param.Value = CustomerInput;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Description";
        param.Value = Description;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FaultIdentified";
        param.Value = FaultIdentified;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActionRequired";
        param.Value = ActionRequired;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PropResolutionDate";
        if (PropResolutionDate != string.Empty)
            param.Value = PropResolutionDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompletionDate";
        if (CompletionDate != string.Empty)
            param.Value = CompletionDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WorkDone";
        param.Value = WorkDone;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CurrentPhoneContact";
        param.Value = CurrentPhoneContact;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Warranty";
        if (Warranty != string.Empty)
            param.Value = Warranty;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCall";
        if (CasualCall != string.Empty)
            param.Value = CasualCall;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCustomer";
        param.Value = CasualCustomer;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContFirst";
        param.Value = CasualContFirst;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContLast";
        param.Value = CasualContLast;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCustPhone";
        param.Value = CasualCustPhone;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContMobile";
        param.Value = CasualContMobile;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContEmail";
        param.Value = CasualContEmail;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualAddress";
        param.Value = CasualAddress;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCity";
        param.Value = CasualCity;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualState";
        param.Value = CasualState;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualPostCode";
        param.Value = CasualPostCode;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualSystemDetails";
        param.Value = CasualSystemDetails;
        param.DbType = DbType.String;
        param.Size = 254;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualRefNumber";
        if (CasualRefNumber != string.Empty)
            param.Value = CasualRefNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MtceCost";
        if (MtceCost != string.Empty)
            param.Value = MtceCost;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ServiceCost";
        if (ServiceCost != string.Empty)
            param.Value = ServiceCost;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);



        param = comm.CreateParameter();
        param.ParameterName = "@MtceDiscount";
        if (MtceDiscount != string.Empty)
            param.Value = MtceDiscount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MtceBalance";
        if (MtceBalance != string.Empty)
            param.Value = MtceBalance;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FPTransTypeID";
        if (FPTransTypeID != string.Empty)
            param.Value = FPTransTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MtceRecBy";
        if (MtceRecBy != string.Empty)
            param.Value = MtceRecBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblProjectMaintenance_InsertUpdate(Int32 ProjectMaintenanceID, String ProjectID, String EmployeeID, String ProjectMtceReasonID, String ProjectMtceReasonSubID, String ProjectMtceCallID, String ProjectMtceStatusID, String OpenDate, String CustomerInput, String Description, String InstallerID, String FaultIdentified, String ActionRequired, String PropResolutionDate, String CompletionDate, String WorkDone, String CurrentPhoneContact, String Warranty, String CasualCall, String CasualCustomer, String CasualContFirst, String CasualContLast, String CasualCustPhone, String CasualContMobile, String CasualContEmail, String CasualAddress, String CasualCity, String CasualState, String CasualPostCode, String CasualSystemDetails, String CasualRefNumber, String MtceCost, String MtceDiscount, String MtceBalance, String FPTransTypeID, String MtceRecBy, String upsize_ts)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMaintenance_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectMaintenanceID";
        param.Value = ProjectMaintenanceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        param.Value = ProjectID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceReasonID";
        param.Value = ProjectMtceReasonID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceReasonSubID";
        param.Value = ProjectMtceReasonSubID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceCallID";
        param.Value = ProjectMtceCallID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectMtceStatusID";
        param.Value = ProjectMtceStatusID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OpenDate";
        param.Value = OpenDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerInput";
        param.Value = CustomerInput;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Description";
        param.Value = Description;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        param.Value = InstallerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FaultIdentified";
        param.Value = FaultIdentified;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActionRequired";
        param.Value = ActionRequired;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PropResolutionDate";
        param.Value = PropResolutionDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompletionDate";
        param.Value = CompletionDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WorkDone";
        param.Value = WorkDone;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CurrentPhoneContact";
        param.Value = CurrentPhoneContact;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Warranty";
        param.Value = Warranty;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCall";
        param.Value = CasualCall;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCustomer";
        param.Value = CasualCustomer;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContFirst";
        param.Value = CasualContFirst;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContLast";
        param.Value = CasualContLast;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCustPhone";
        param.Value = CasualCustPhone;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContMobile";
        param.Value = CasualContMobile;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualContEmail";
        param.Value = CasualContEmail;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualAddress";
        param.Value = CasualAddress;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualCity";
        param.Value = CasualCity;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualState";
        param.Value = CasualState;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualPostCode";
        param.Value = CasualPostCode;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualSystemDetails";
        param.Value = CasualSystemDetails;
        param.DbType = DbType.String;
        param.Size = 254;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CasualRefNumber";
        param.Value = CasualRefNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MtceCost";
        param.Value = MtceCost;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MtceDiscount";
        param.Value = MtceDiscount;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MtceBalance";
        param.Value = MtceBalance;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FPTransTypeID";
        param.Value = FPTransTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@MtceRecBy";
        param.Value = MtceRecBy;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblProjectMaintenance_Delete(string ProjectMaintenanceID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMaintenance_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectMaintenanceID";
        param.Value = ProjectMaintenanceID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static DataTable tblProjectMaintenance_Search_Casual(string alpha, string InstallerID, string startdate, string enddate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMaintenance_Search_Casual";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InstallerID";
        if (InstallerID != string.Empty)
            param.Value = InstallerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
}