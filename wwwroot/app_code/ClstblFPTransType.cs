using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
public struct SttblFPTransType
{
    public string FPTransType;
    public string FPTransTypeAB;
    public string InvoicePay;
    public string SupplierPay;
    public string WagesPay;
    public string Active;
    public string Seq;
}

public class ClstblFPTransType
{
    public static SttblFPTransType tblFPTransType_SelectByFPTransTypeID(String FPTransTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblFPTransType_SelectByFPTransTypeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@FPTransTypeID";
        param.Value = FPTransTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblFPTransType details = new SttblFPTransType();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.FPTransType = dr["FPTransType"].ToString();
            details.FPTransTypeAB = dr["FPTransTypeAB"].ToString();
            details.InvoicePay = dr["InvoicePay"].ToString();
            details.SupplierPay = dr["SupplierPay"].ToString();
            details.WagesPay = dr["WagesPay"].ToString();
            details.Active = dr["Active"].ToString();
            details.Seq = dr["Seq"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblFPTransType_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblFPTransType_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblFPTransType_SelectActive()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblFPTransType_SelectActive";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblFPTransType_Insert(String FPTransType, String FPTransTypeAB, String InvoicePay, String SupplierPay, String WagesPay, String Active, String Seq)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblFPTransType_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@FPTransType";
        param.Value = FPTransType;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FPTransTypeAB";
        param.Value = FPTransTypeAB;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePay";
        param.Value = InvoicePay;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SupplierPay";
        param.Value = SupplierPay;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WagesPay";
        param.Value = WagesPay;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblFPTransType_Update(string FPTransTypeID, String FPTransType, String FPTransTypeAB, String InvoicePay, String SupplierPay, String WagesPay, String Active, String Seq)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblFPTransType_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@FPTransTypeID";
        param.Value = FPTransTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FPTransType";
        param.Value = FPTransType;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FPTransTypeAB";
        param.Value = FPTransTypeAB;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoicePay";
        param.Value = InvoicePay;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SupplierPay";
        param.Value = SupplierPay;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WagesPay";
        param.Value = WagesPay;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblFPTransType_Delete(string FPTransTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblFPTransType_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@FPTransTypeID";
        param.Value = FPTransTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tblFPTransType_Exists(string FPTransType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblFPTransType_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@FPTransType";
        param.Value = FPTransType;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static int tblFPTransType_ExistsById(string FPTransType, string FPTransTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblFPTransType_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@FPTransType";
        param.Value = FPTransType;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@FPTransTypeID";
        param.Value = FPTransTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static DataTable tblFPTransType_GetDataByAlpha(string alpha)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblFPTransType_GetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblFPTransType_GetDataBySearch(string alpha, string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblFPTransType_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        return DataAccess.ExecuteSelectCommand(comm);
    }
}