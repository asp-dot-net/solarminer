using System;
using System.Data;
using System.Data.Common;

public struct SttblNewsLetter
{
    public string NewsTitle;
    public string NewsDescription;
}

public class ClstblNewsLetter
{
    public static SttblNewsLetter tblNewsLetter_SelectByNewsID(String NewsID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblNewsLetter_SelectByNewsID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@NewsID";
        param.Value = NewsID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblNewsLetter details = new SttblNewsLetter();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.NewsTitle = dr["NewsTitle"].ToString();
            details.NewsDescription = dr["NewsDescription"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblNewsLetter_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblNewsLetter_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblNewsLetter_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblNewsLetter_SelectASC";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblNewsLetter_Insert(String NewsTitle, String NewsDescription)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblNewsLetter_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@NewsTitle";
        param.Value = NewsTitle;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NewsDescription";
        param.Value = NewsDescription;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblNewsLetter_Update(string NewsID, String NewsTitle, String NewsDescription)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblNewsLetter_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@NewsID";
        param.Value = NewsID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NewsTitle";
        param.Value = NewsTitle;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NewsDescription";
        param.Value = NewsDescription;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblNewsLetter_InsertUpdate(Int32 NewsID, String NewsTitle, String NewsDescription)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblNewsLetter_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@NewsID";
        param.Value = NewsID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NewsTitle";
        param.Value = NewsTitle;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NewsDescription";
        param.Value = NewsDescription;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblNewsLetter_Delete(string NewsID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblNewsLetter_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@NewsID";
        param.Value = NewsID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static DataTable tblNewsLetter_DataBySearch(string alpha)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblNewsLetter_DataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static int tblNewsLetter_Exists(string NewsTitle)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblNewsLetter_Exists";

        // create a new parameter
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@NewsTitle";
        param.Value = NewsTitle;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int tblNewsLetter_ExistsById(string NewsTitle, string NewsID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblNewsLetter_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@NewsTitle";
        param.Value = NewsTitle;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NewsID";
        param.Value = NewsID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }
}