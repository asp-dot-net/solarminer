using System;
using System.Data;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using MeghMailUtility;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using System.Net.Mime;

/// <summary>
/// Summary description for Utilities
/// </summary>
/// 
public struct MailManagerDetails
{
    public string mailserver;
    public string username;
    public string password;
    public string adminemail;
    public string operatoremail;
    public string webmasteremail;
    public string contactemail;
    public string regemail;
    public string birthdayemail;
    public string ordermail;
    public string promotionmail;


}
public class Utilities
{
    public Utilities()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    static string displayname = ClsAdminUtilities.StUtilitiesGetDataStructById("1").sitename.ToString();

    // static string displayname = ConfigurationManager.AppSettings["displayname"].ToString();
    //static string mailbodystart = "<style>BODY{    margin: 0px;    padding: 0px;    font-family: 'Georgia', Times New Roman, Times, serif;    line-height: 17px;}</style><table cellpadding=0 cellspacing=0 width=600px><tr><td><img src='http://www.nghdiet.com/images/emailtop.jpg'></td></tr><tr><td background='http://www.nghdiet.com/images/emailbdr.jpg'><table cellpadding=10 cellspacing=10 width=90% align=center><tr><td>";
    //static string mailbodyend = "</td></tr><tr><td>Best Regards,<br><b>NGHdiet Team</b></td></tr></table></td></tr><tr><td><img src='http://www.nghdiet.com/images/emailbtm.jpg'></td></tr></table>";
    public static void LogError(Exception ex)
    {

        string dateTime = DateTime.Now.ToLongDateString() + ", at "
                        + DateTime.Now.ToShortTimeString();

        string errorMessage = "Exception generated on " + dateTime;

        System.Web.HttpContext context = System.Web.HttpContext.Current;
        errorMessage += "\n\n Page location: " + context.Request.RawUrl;

        errorMessage += "\n\n Message: " + ex.Message;
        errorMessage += "\n\n Source: " + ex.Source;
        errorMessage += "\n\n Method: " + ex.TargetSite;
        errorMessage += "\n\n Stack Trace: \n\n" + ex.StackTrace;


        if (SiteConfiguration.EnableErrorLogEmail)
        {
            string from = "noreply@domainname.com";
            string to = SiteConfiguration.ErrorLogEmail;
            string subject = "Error Report";
            string body = errorMessage;
            SendMail(from, to, subject, body);
        }
    }
    public static void SendMail(string from, string to, string subject, string body)
    {
        String mailbody = body;
        string authnticate = ClsAdminUtilities.StUtilitiesGetDataStructById("1").Authenticate.ToString();
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        if (authnticate == "2")
        {
            mailbody = body;
            Mail mail = new Mail();
            string mailFrom = from;
            string mailTo = to;
            mail.Subject = subject;
            mail.IsHtml = true;
            mail.To = to;
            mail.From = from;
            mail.Body = mailbody;
            mail.IsAuthentification = true;
            mail.MailType = Mail.TypeMail.WebMail;
            Mail.SendMail(mail);
        }
        else
        {
            SmtpClient smtpc = new SmtpClient("smtp.gmail.com");
            smtpc.Port = 587;
            smtpc.EnableSsl = true;
            smtpc.UseDefaultCredentials = false;
            string user = st.username;
            string pass = st.Password;
            string sub = subject; //Subject for your website
            string msg = mailbody; //Message body
            smtpc.Credentials = new NetworkCredential(user, pass);
            MailMessage email = new MailMessage(user, to, sub, msg);
            email.IsBodyHtml = true;

            //smtpc.Send(email);
            if (authnticate == "1")
            {
                System.Net.NetworkCredential myMailCredential = new System.Net.NetworkCredential();
                myMailCredential.UserName = ClsAdminUtilities.StUtilitiesGetDataStructById("1").username.ToString();
                myMailCredential.Password = ClsAdminUtilities.StUtilitiesGetDataStructById("1").Password.ToString();
                smtpc.UseDefaultCredentials = false;
                smtpc.Credentials = myMailCredential;
            }
            smtpc.Send(email);
        }
    }
    public static void SendMailWithOutTemplate(string from, string to, string subject, string body)
    {
        String mailbody = body;
        string authnticate = ClsAdminUtilities.StUtilitiesGetDataStructById("1").Authenticate.ToString();
        if (authnticate == "2")
        {
            mailbody = body;
            Mail mail = new Mail();
            string mailFrom = from;
            string mailTo = to;
            mail.Subject = subject;
            mail.IsHtml = true;
            mail.To = to;
            mail.From = from;
            mail.Body = mailbody;
            mail.IsAuthentification = true;
            mail.MailType = Mail.TypeMail.WebMail;
            Mail.SendMail(mail);
        }
        else
        {
            SmtpClient mailClient = new SmtpClient(SiteConfiguration.MailServer);
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(from, displayname);
            mailMessage.To.Add(to);
            mailMessage.Subject = subject;
            mailMessage.Body = mailbody;
            mailMessage.Priority = MailPriority.High;
            mailMessage.IsBodyHtml = true;

            if (authnticate == "1")
            {
                System.Net.NetworkCredential myMailCredential = new System.Net.NetworkCredential();
                myMailCredential.UserName = ClsAdminUtilities.StUtilitiesGetDataStructById("1").username.ToString();
                myMailCredential.Password = ClsAdminUtilities.StUtilitiesGetDataStructById("1").Password.ToString();
                mailClient.UseDefaultCredentials = false;
                mailClient.Credentials = myMailCredential;
            }
            mailClient.Send(mailMessage);

        }
    }


    public static void SendMailWithAttachment(string from, string to, string subject, string body, string FileURL, string FileName)
    {
        String mailbody = body;
        string authnticate = ClsAdminUtilities.StUtilitiesGetDataStructById("1").Authenticate.ToString();
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        if (authnticate == "2")
        {
            mailbody = body;
            Mail mail = new Mail();
            string mailFrom = from;
            string mailTo = to;
            mail.Subject = subject;
            mail.IsHtml = true;
            mail.To = to;
            mail.From = from;
            mail.Body = mailbody;
            mail.IsAuthentification = true;
            mail.MailType = Mail.TypeMail.WebMail;
            Mail.SendMail(mail);
        }
        else
        {

            SmtpClient smtpc = new SmtpClient("smtp.gmail.com");
            smtpc.Port = 587;
            smtpc.EnableSsl = true;
            smtpc.UseDefaultCredentials = false;
            string user = st.username;
            string pass = st.Password;
            string sub = subject; //Subject for your website
            string msg = mailbody; //Message body
            smtpc.Credentials = new NetworkCredential(user, pass);
            MailMessage email = new MailMessage(user, to, sub, msg);
            email.IsBodyHtml = true;

            var stream = new WebClient().OpenRead(FileURL);
            Attachment at = new Attachment(stream, FileName, MediaTypeNames.Application.Pdf);
            email.Attachments.Add(at);
            email.Priority = MailPriority.High;
            email.IsBodyHtml = true;            

            if (authnticate == "1")
            {
                System.Net.NetworkCredential myMailCredential = new System.Net.NetworkCredential();
                myMailCredential.UserName = ClsAdminUtilities.StUtilitiesGetDataStructById("1").username.ToString();
                myMailCredential.Password = ClsAdminUtilities.StUtilitiesGetDataStructById("1").Password.ToString();
                smtpc.UseDefaultCredentials = false;
                smtpc.Credentials = myMailCredential;
                smtpc.EnableSsl = true;
            }
            smtpc.Send(email);
            stream.Close();

        }
    }

    public static void SendMailWithMultipleAttachment(string from, string to, string subject, string body, string[] FileURL, string[] FileName,int FileCount)
    {
        String mailbody = body;
        string authnticate = ClsAdminUtilities.StUtilitiesGetDataStructById("1").Authenticate.ToString();
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        if (authnticate == "2")
        {
            mailbody = body;
            Mail mail = new Mail();
            string mailFrom = from;
            string mailTo = to;
            mail.Subject = subject;
            mail.IsHtml = true;
            mail.To = to;
            mail.From = from;
            mail.Body = mailbody;
            mail.IsAuthentification = true;
            mail.MailType = Mail.TypeMail.WebMail;
            Mail.SendMail(mail);
        }
        else
        {           
            SmtpClient smtpc = new SmtpClient("smtp.gmail.com");
            smtpc.Port = 587;
            smtpc.EnableSsl = true;
            smtpc.UseDefaultCredentials = false;
            string user = st.username;
            string pass = st.Password;
            string sub = subject; //Subject for your website
            string msg = mailbody; //Message body
            smtpc.Credentials = new NetworkCredential(user, pass);
            MailMessage email = new MailMessage(user, to, sub, msg);
            email.IsBodyHtml = true;
            smtpc.Timeout = 60000;

            for (int i = 0; i <= FileCount; i++)
            {
                if (!string.IsNullOrEmpty(FileURL[i]))
                {
                    if (!string.IsNullOrEmpty(FileName[i]))
                    {
                        var stream = new WebClient().OpenRead(FileURL[i]);
                        Attachment at = new Attachment(stream, FileName[i], MediaTypeNames.Application.Pdf);
                        email.Attachments.Add(at);
                    }
                }
            }

            email.Priority = MailPriority.High;
            email.IsBodyHtml = true;

            if (authnticate == "1")
            {
                System.Net.NetworkCredential myMailCredential = new System.Net.NetworkCredential();
                myMailCredential.UserName = ClsAdminUtilities.StUtilitiesGetDataStructById("1").username.ToString();
                myMailCredential.Password = ClsAdminUtilities.StUtilitiesGetDataStructById("1").Password.ToString();
                smtpc.UseDefaultCredentials = false;
                smtpc.Credentials = myMailCredential;
                smtpc.EnableSsl = true;
            }
            smtpc.Send(email);

        }
    }

    public static void SendMailWithMultipleAttachmentWithReplyList(string from, string to, string subject, string body, string[] FileURL, string[] FileName, int FileCount,String ReplyAddress,String ReplyName)
    {
        String mailbody = body;
        string authnticate = ClsAdminUtilities.StUtilitiesGetDataStructById("1").Authenticate.ToString();
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        if (authnticate == "2")
        {
            mailbody = body;
            Mail mail = new Mail();
            string mailFrom = from;
            string mailTo = to;
            mail.Subject = subject;
            mail.IsHtml = true;
            mail.To = to;
            mail.From = from;
            mail.Body = mailbody;
            mail.IsAuthentification = true;
            mail.MailType = Mail.TypeMail.WebMail;
            Mail.SendMail(mail);
        }
        else
        {
            SmtpClient smtpc = new SmtpClient("smtp.gmail.com");
            smtpc.Port = 587;
            smtpc.EnableSsl = true;
            smtpc.UseDefaultCredentials = false;
            string user = st.username;
            string pass = st.Password;
            string sub = subject; //Subject for your website
            string msg = mailbody; //Message body
            smtpc.Credentials = new NetworkCredential(user, pass);
            MailMessage email = new MailMessage(user, to, sub, msg);
            email.ReplyToList.Add(new System.Net.Mail.MailAddress(ReplyAddress, ReplyName));
            //email.Headers.Add("Sender", ReplyAddress);
            email.IsBodyHtml = true;
            smtpc.Timeout = 50000;

            for (int i = 0; i <= FileCount; i++)
            {
                if (!string.IsNullOrEmpty(FileURL[i]))
                {
                    if (!string.IsNullOrEmpty(FileName[i]))
                    {
                        var stream = new WebClient().OpenRead(FileURL[i]);
                        Attachment at = new Attachment(stream, FileName[i], MediaTypeNames.Application.Pdf);
                        email.Attachments.Add(at);
                    }
                }
            }

            email.Priority = MailPriority.High;
            email.IsBodyHtml = true;

            if (authnticate == "1")
            {
                System.Net.NetworkCredential myMailCredential = new System.Net.NetworkCredential();
                myMailCredential.UserName = ClsAdminUtilities.StUtilitiesGetDataStructById("1").username.ToString();
                myMailCredential.Password = ClsAdminUtilities.StUtilitiesGetDataStructById("1").Password.ToString();
                smtpc.UseDefaultCredentials = false;
                smtpc.Credentials = myMailCredential;
                smtpc.EnableSsl = true;
            }
            smtpc.Send(email);

        }
    }

    public static void SendMail(string from, string to, string subject, string body, string cc)
    {
        string mailbody = body;
        string authnticate = ClsAdminUtilities.StUtilitiesGetDataStructById("1").Authenticate.ToString();
        if (authnticate == "2")
        {
            mailbody = body;
            Mail mail = new Mail();
            string mailFrom = from;
            string mailTo = to;
            mail.Subject = subject;
            mail.IsHtml = true;
            mail.To = to;
            mail.From = from;
            mail.Body = mailbody;
            mail.Cc = cc;
            mail.IsAuthentification = true;
            mail.MailType = Mail.TypeMail.WebMail;
            Mail.SendMail(mail);
        }
        else
        {
            SmtpClient mailClient = new SmtpClient(SiteConfiguration.MailServer);
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(from, displayname);
            mailMessage.To.Add(to);
            mailMessage.Subject = subject;
            mailMessage.Body = mailbody;
            mailClient.Port = SiteConfiguration.MailServerPort;
            mailMessage.Priority = MailPriority.High;
            mailMessage.IsBodyHtml = true;
            mailMessage.CC.Add(cc);

            if (authnticate == "1")
            {
                System.Net.NetworkCredential myMailCredential = new System.Net.NetworkCredential();
                myMailCredential.UserName = ClsAdminUtilities.StUtilitiesGetDataStructById("1").username.ToString();
                myMailCredential.Password = ClsAdminUtilities.StUtilitiesGetDataStructById("1").Password.ToString();
                mailClient.UseDefaultCredentials = false;
                mailClient.Credentials = myMailCredential;
            }
            mailClient.Send(mailMessage);
        }
    }
    public static void SendMail(string from, string to, string subject, string body, string cc, string bcc)
    {
        String mailbody = body;
        string authnticate = ClsAdminUtilities.StUtilitiesGetDataStructById("1").Authenticate.ToString();
        if (authnticate == "2")
        {
            mailbody = body;
            Mail mail = new Mail();
            string mailFrom = from;
            string mailTo = to;
            mail.Subject = subject;
            mail.IsHtml = true;
            mail.To = to;
            mail.From = from;
            mail.Body = mailbody;
            mail.Cc = cc;
            mail.Bcc = bcc;
            mail.IsAuthentification = true;
            mail.MailType = Mail.TypeMail.WebMail;
            Mail.SendMail(mail);
        }
        else
        {
            SmtpClient mailClient = new SmtpClient(SiteConfiguration.MailServer);
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(from, displayname);
            mailMessage.To.Add(to);
            mailMessage.Subject = subject;
            mailMessage.Body = mailbody;
            mailClient.Port = SiteConfiguration.MailServerPort;
            mailMessage.Priority = MailPriority.High;
            mailMessage.IsBodyHtml = true;
            mailMessage.CC.Add(cc);
            mailMessage.Bcc.Add(bcc);

            if (authnticate == "1")
            {
                System.Net.NetworkCredential myMailCredential = new System.Net.NetworkCredential();
                myMailCredential.UserName = ClsAdminUtilities.StUtilitiesGetDataStructById("1").username.ToString();
                myMailCredential.Password = ClsAdminUtilities.StUtilitiesGetDataStructById("1").Password.ToString();
                mailClient.UseDefaultCredentials = false;
                mailClient.Credentials = myMailCredential;
            }
            mailClient.Send(mailMessage);
        }
    }
    public static string GetPageName(string URL)
    {
        string url = URL;

        char[] patt = { '/' };
        char[] p = { '.' };
        string[] arr = url.Split(patt);
        url = arr[arr.Length - 1];

        url = url.Remove(url.IndexOf('.'));

        return url;
    }
    public static string GetFileName(string URL)
    {
        string url = URL;
        try
        {
            char[] patt = { '/' };
            char[] p = { '.' };
            string[] arr = url.Split(patt);
            url = arr[arr.Length - 1];
            //url = url.Remove(url.IndexOf('.'));
        }
        catch
        {
        }
        return url;
    }
    public static string[] GetFileExtensions()
    {
        string[] filenames = { ".zip", ".rar", ".exe", ".jpeg", ".png", ".jpg", ".mp3", ".swf", ".flv", ".wmv", ".mov" };
        return filenames;
    }
    public static string[] GetFileExtensionsForJukebox()//add video file extensions
    {
        string[] filenames = { ".mp3" };
        return filenames;
    }
    public static string[] GetFileExtensionsForGallery()//add video file extensions
    {
        string[] filenames = { ".jpg", ".png", "gif", "jpeg" };
        return filenames;
    }

    public static DataTable GetStateList()
    {

        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "GetStateList";
        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static DataTable GetCountryList()
    {

        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "GetCountryList";
        return DataAccess.ExecuteSelectCommand(comm);
    }

    public static MailManagerDetails AdminGetMailManagerDetails()
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();
        // set the stored procedure name
        comm.CommandText = "AdminGetMailManagerDetails";

        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object

        MailManagerDetails details = new MailManagerDetails();

        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get  details
            details.mailserver = dr["mailserver"].ToString();
            details.username = dr["username"].ToString();
            details.password = dr["password"].ToString();
            details.adminemail = dr["adminemail"].ToString();
            details.operatoremail = dr["operatoremail"].ToString();
            details.webmasteremail = dr["webmasteremail"].ToString();
            details.contactemail = dr["contactemail"].ToString();
            details.regemail = dr["regemail"].ToString();
            details.birthdayemail = dr["birthdayemail"].ToString();
            details.ordermail = dr["ordermail"].ToString();
            details.promotionmail = dr["promotionmail"].ToString();

        }
        // return department details
        return details;
    }

    public static string ProperCase(string stringInput)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        bool fEmptyBefore = true;
        foreach (char ch in stringInput)
        {
            char chThis = ch;
            if (Char.IsWhiteSpace(chThis))
                fEmptyBefore = true;
            else
            {
                if (Char.IsLetter(chThis) && fEmptyBefore)
                    chThis = Char.ToUpper(chThis);
                else
                    chThis = Char.ToLower(chThis);
                fEmptyBefore = false;
            }
            sb.Append(chThis);
        }
        return sb.ToString();
    }

    public static int ClientInsertDownloadCount(string pid, string cid, string sectionid)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "ClientInsertDownloadCount";

        // create a new parameter
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@pid";
        param.Value = pid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);



        param = comm.CreateParameter();
        param.ParameterName = "@cid";
        param.Value = cid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@sectionid";
        param.Value = sectionid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // result will represent the number of changed rows
        int result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));

        return result;
    }
    public static int ClientItemCountAddCount(string pid, string cid, string count)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "ClientItemCountAddCount";

        // create a new parameter
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@pid";
        param.Value = pid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@cid";
        param.Value = cid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // create a new parameter
        param = comm.CreateParameter();
        param.ParameterName = "@count";
        param.Value = count;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);



        // result will represent the number of changed rows
        int result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));

        return result;
    }

    public static int ClientItemCountAddCount_AR(string pid, string cid, string count)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure name
        comm.CommandText = "ClientItemCountAddCount_AR";

        // create a new parameter
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@pid";
        param.Value = pid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@cid";
        param.Value = cid;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // create a new parameter
        param = comm.CreateParameter();
        param.ParameterName = "@count";
        param.Value = count;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);



        // result will represent the number of changed rows
        int result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));

        return result;
    }


    public static string GetCurLan()
    {
        string lang = string.Empty;
        if (HttpContext.Current.Request.Cookies["cookielang"] != null)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies.Get("cookielang");
            lang = cookie.Values["lang"].ToString();
        }
        else
        {
            //=========================For Default Language
            lang = "en-US";
            //lang = "ar-KW";
        }
        return lang;
    }

    public static int GetControlIndex(String controlID)
    {
        try
        {
            Regex regex = new Regex("([0-9.*]{2})",
                       RegexOptions.RightToLeft);
            Match match = regex.Match(controlID);
            return Convert.ToInt32(match.Value);


        }
        catch
        {


            Regex regex = new Regex("([0-9.*])",
                          RegexOptions.RightToLeft);
            Match match = regex.Match(controlID);
            return Convert.ToInt32(match.Value);
        }
    }
}
