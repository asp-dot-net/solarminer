﻿using System;
using System.Data;
using System.Data.Common;

/// <summary>
/// Summary description for ClsInvoicePaid
/// </summary>
public class ClsInvoicePaid
{
    public ClsInvoicePaid()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static int tbl_BankInvoicePayments_Insert(DataTable dt, string createdOn, string createdBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_BankInvoicePayments_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@dt";
        if (dt != null)
            param.Value = dt;
        else
            param.Value = DBNull.Value;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@createdOn";
        if (createdOn != string.Empty)
            param.Value = createdOn;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@createdBy";
        if (createdBy != string.Empty)
            param.Value = createdBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int result = 0;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable Sp_InvoicePaid(string invoiceNo, string customer, string state, string payBy, string financeWith, string dateType, string startDate, string endDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "Sp_InvoicePaid";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@invoiceNo";
        if (invoiceNo != string.Empty)
            param.Value = invoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@customer";
        if (customer != string.Empty)
            param.Value = customer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@state";
        if (state != string.Empty)
            param.Value = state;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@payBy";
        if (payBy != string.Empty)
            param.Value = payBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        
        param = comm.CreateParameter();
        param.ParameterName = "@financeWith";
        if (financeWith != string.Empty)
            param.Value = financeWith;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@dateType";
        if (dateType != string.Empty)
            param.Value = dateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startDate";
        if (startDate != string.Empty)
            param.Value = startDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@endDate";
        if (endDate != string.Empty)
            param.Value = endDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static string tbl_BankInvoicePayments_LastExcelUploadDate()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_BankInvoicePayments_LastExcelUploadDate";

        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static int tbl_BankInvoicePaymentsExcelLog_Insert(string fileName, string createdOn, string createdBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_BankInvoicePaymentsExcelLog_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@fileName";
        if (fileName != string.Empty)
            param.Value = fileName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@createdOn";
        if (createdOn != string.Empty)
            param.Value = createdOn;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@createdBy";
        if (createdBy != string.Empty)
            param.Value = createdBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int result = 0;
        try
        {
            result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_BankInvoicePaymentsExcelLog_GetData()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_BankInvoicePaymentsExcelLog_GetData";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_BankInvoicePaymentsExcelLog_Delete(string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_BankInvoicePaymentsExcelLog_Delete";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        if (id != string.Empty)
            param.Value = id;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
        }
        return (result != -1);
    }
}