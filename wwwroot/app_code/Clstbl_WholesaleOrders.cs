﻿using System;
using System.Data;
using System.Data.Common;
using System.Web;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
public struct Sttbl_WholesaleOrders
{
    public string OrderNumber;
    public string WholesaleCategoryID;
    public string StockItemID;
    public string OrderQuantity;
    public string DateOrdered;
    public string ExpectedDelivery;
    public string Delivered;
    public string Cancelled;
    public string OrderedBy;
    public string ReceivedBy;
    public string Notes;
    public string CompanyLocationID;
    public string CustomerID;
    public string BOLReceived;
    public string upsize_ts;
    public string ReferenceNo;
    public string ActualDelivery;

    public string CompanyLocation;
    public string CompanyLocationState;
    public string Invoice;
    public string OrderedName;
    public string InvoiceNo;
    public string vendor;
    public string IsDeduct;

    public string JobTypeID;
    public string JobType;
    public string InvoiceAmount;
    public string PVDNumber;
    public string StatusID;
    public string Status;
}

public struct Sttbl_WholesaleOrderItems
{
    public string WholesaleOrderID;
    public string WholesaleOrderItemID;
    public string StockItemID;
    public string OrderQuantity;
    public string WholesaleOrderItem;
    public string WholesaleCode;
    public string WholesaleLocation;
    public string WholesaleOrderItemlist;


}
public class Clstbl_WholesaleOrders
{
    public static Sttbl_WholesaleOrders tbl_WholesaleOrders_SelectByWholesaleOrderID(String WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_SelectByWholesaleOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        Sttbl_WholesaleOrders details = new Sttbl_WholesaleOrders();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.OrderNumber = dr["OrderNumber"].ToString();
            details.WholesaleCategoryID = dr["WholesaleCategoryID"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();
            //details.OrderQuantity = dr["OrderQuantity"].ToString();
            details.DateOrdered = dr["DateOrdered"].ToString();
            details.ExpectedDelivery = dr["ExpectedDelivery"].ToString();
            details.Delivered = dr["Delivered"].ToString();
            details.Cancelled = dr["Cancelled"].ToString();
            details.OrderedBy = dr["OrderedBy"].ToString();
            details.ReceivedBy = dr["ReceivedBy"].ToString();
            details.Notes = dr["Notes"].ToString();
            details.CompanyLocationID = dr["CompanyLocationID"].ToString();
            details.CustomerID = dr["CustomerID"].ToString();
            details.BOLReceived = dr["BOLReceived"].ToString();
            details.upsize_ts = dr["upsize_ts"].ToString();
            details.ReferenceNo = dr["ReferenceNo"].ToString();
            details.ActualDelivery = dr["ActualDelivery"].ToString();

            details.CompanyLocation = dr["CompanyLocation"].ToString();
            //details.CompanyLocationState = dr["CompanyLocationState"].ToString();
            //details.Invoice = dr["Invoice"].ToString();
            details.OrderedName = dr["OrderedName"].ToString();
            details.InvoiceNo = dr["InvoiceNo"].ToString();
            details.vendor = dr["vendor"].ToString();
            details.IsDeduct = dr["IsDeduct"].ToString();

            details.JobTypeID = dr["JobTypeID"].ToString();
            details.JobType = dr["JobType"].ToString();
            details.InvoiceAmount = dr["InvoiceAmount"].ToString();
            details.PVDNumber = dr["PVDNumber"].ToString();
            details.StatusID = dr["StatusID"].ToString();
            details.Status = dr["Status"].ToString();
}
        // return structure details
        return details;
    }

    public static DataTable tbl_WholesaleOrders_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Select";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrders_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_SelectASC";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrders_SelectBySearch(string Delivered, string Due, string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string ReferenceNo, string State, string stockitem, string IsDeliveredOrNot)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_SelectBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Due";
        if (Due != string.Empty)
            param.Value = Due;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 10;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitem";
        if (stockitem != string.Empty)
            param.Value = stockitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferenceNo";
        if (ReferenceNo != string.Empty)
            param.Value = ReferenceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeliveredOrNot";
        if (IsDeliveredOrNot != string.Empty)
            param.Value = IsDeliveredOrNot;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleDeduct_SelectBySearch(string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string CompanyLocationID, string stockitem, string Historic)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleDeduct_SelectBySearch";

        DbParameter param = comm.CreateParameter();

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //  System.Web.HttpContext.Current.Response.Write(param.Value);


        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = "0";
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //   System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@stockitem";
        if (stockitem != string.Empty)
            param.Value = stockitem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);
        //  System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@Historic";
        if (Historic != string.Empty)
            param.Value = Historic;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);
        //  System.Web.HttpContext.Current.Response.Write(param.Value);

        // System.Web.HttpContext.Current.Response.End();
        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrders_SelectWarehouseAllocated(string IsDeduct, string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string CompanyLocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_SelectWarehouseAllocated";

        DbParameter param = comm.CreateParameter();

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        if (IsDeduct != string.Empty)
            param.Value = IsDeduct;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //  System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //  System.Web.HttpContext.Current.Response.Write(param.Value);


        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = "0";
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //   System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        // System.Web.HttpContext.Current.Response.End();
        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblWholesaleOrderItems_StockRevert(string IsDeduct, string CustomerID, string startdate, string enddate, string DateType, string OrderNumber, string CompanyLocationID, string RevertFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleOrderItems_StockRevert";

        DbParameter param = comm.CreateParameter();

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        if (IsDeduct != string.Empty)
            param.Value = IsDeduct;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //  System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //  System.Web.HttpContext.Current.Response.Write(param.Value);


        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        if (OrderNumber != string.Empty)
            param.Value = OrderNumber;
        else
            param.Value = "0";
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        //   System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // System.Web.HttpContext.Current.Response.Write(param.Value);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertFlag";
        if (RevertFlag != string.Empty)
            param.Value = RevertFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // System.Web.HttpContext.Current.Response.End();
        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tbl_WholesaleOrders_Insert(String DateOrdered, String OrderedBy, String Notes, String CompanyLocationID, String CustomerID, String BOLReceived, string ReferenceNo, string ExpectedDelivery)
    {

        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@DateOrdered";
        param.Value = DateOrdered;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderedBy";
        param.Value = OrderedBy;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BOLReceived";
        if (BOLReceived != string.Empty)
            param.Value = BOLReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferenceNo";
        if (ReferenceNo != string.Empty)
            param.Value = ReferenceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExpectedDelivery";
        if (ExpectedDelivery != string.Empty)
            param.Value = ExpectedDelivery;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));

        return id;


    }

    public static bool tbl_WholesaleOrders_Update_OrderNumber(string WholesaleOrderID, String OrderNumber)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_OrderNumber";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderNumber";
        param.Value = OrderNumber;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_Cancelled(string WholesaleOrderID, String Cancelled)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_Cancelled";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Cancelled";
        param.Value = Cancelled;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_Delivered(string WholesaleOrderID, string ReceivedBy, string Delivered)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_Delivered";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReceivedBy";
        param.Value = ReceivedBy;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        param.Value = Delivered;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_ActualDelivery(string WholesaleOrderID, string ActualDelivery)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_ActualDelivery";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ActualDelivery";
        if (ActualDelivery != string.Empty)
            param.Value = ActualDelivery;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update(string WholesaleOrderID, String Notes, String CompanyLocationID, String CustomerID, String BOLReceived, string ReferenceNo, string ExpectedDelivery)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Notes";
        if (Notes != string.Empty)
            param.Value = Notes;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@BOLReceived";
        if (BOLReceived != string.Empty)
            param.Value = BOLReceived;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReferenceNo";
        if (ReferenceNo != string.Empty)
            param.Value = ReferenceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ExpectedDelivery";
        if (ExpectedDelivery != string.Empty)
            param.Value = ExpectedDelivery;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_Job(string WholesaleOrderID, String JobTypeID, String JobType, String InvoiceAmount, String PVDNumber, string StatusID, string Status)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_Job";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@JobTypeID";
        if (JobTypeID != string.Empty)
            param.Value = JobTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;        
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@JobType";
        if (JobType != string.Empty)
            param.Value = JobType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceAmount";
        if (InvoiceAmount != string.Empty)
            param.Value = InvoiceAmount;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Decimal;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PVDNumber";
        if (PVDNumber != string.Empty)
            param.Value = PVDNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StatusID";
        if (StatusID != string.Empty)
            param.Value = StatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Status";
        if (Status != string.Empty)
            param.Value = Status;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Delete(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrders_Update_VendorInvoiceNo(string WholesaleOrderID, String InvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Update_VendorInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static int tbl_WholesaleOrders_Exits_VendorInvoiceNo(string InvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_Exits_VendorInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }

    public static int tbl_WholesaleOrders_ExistsByIdVendorInvoiceNo(string WholesaleOrderID, string InvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_ExistsByIdVendorInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static int tbl_WholesaleOrders_ExistsByReferenceNo(string ReferenceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_ExistsByReferenceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ReferenceNo";
        param.Value = ReferenceNo;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    public static int tbl_WholesaleOrders_ExistsByInvoiceNo(string InvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_ExistsByInvoiceNo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }

    /////////////////////////////////////////////////	tbl_WholesaleOrderItems

    public static Sttbl_WholesaleOrderItems tbl_WholesaleOrderItems_SelectByWholesaleOrderID(String WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_SelectByWholesaleOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        Sttbl_WholesaleOrderItems details = new Sttbl_WholesaleOrderItems();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.WholesaleOrderID = dr["WholesaleOrderID"].ToString();
            details.WholesaleOrderItemID = dr["WholesaleOrderItemID"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();
            details.OrderQuantity = dr["OrderQuantity"].ToString();
            details.WholesaleOrderItem = dr["WholesaleOrderItem"].ToString();
            details.WholesaleCode = dr["WholesaleCode"].ToString();
            details.WholesaleLocation = dr["WholesaleLocation"].ToString();
            details.WholesaleOrderItemlist = dr["WholesaleOrderItemlist"].ToString();

        }
        // return structure details
        return details;
    }



    public static Sttbl_WholesaleOrderItems tbl_WholesaleOrderItems_SelectByWholesaleOrderItemID(String WholesaleOrderItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_SelectByWholesaleOrderItemID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderItemID";
        param.Value = WholesaleOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        Sttbl_WholesaleOrderItems details = new Sttbl_WholesaleOrderItems();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.WholesaleOrderID = dr["WholesaleOrderID"].ToString();
            details.StockItemID = dr["StockItemID"].ToString();
            details.OrderQuantity = dr["OrderQuantity"].ToString();
            details.WholesaleOrderItem = dr["WholesaleOrderItem"].ToString();
            details.WholesaleCode = dr["WholesaleCode"].ToString();
            details.WholesaleLocation = dr["WholesaleLocation"].ToString();
            details.WholesaleOrderItemlist = dr["WholesaleOrderItemlist"].ToString();

        }
        // return structure details
        return details;
    }

    public static DataTable tbl_WholesaleOrderItems_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_Select_ByWholesaleOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrderItems_Selectby_IdandLocation(string WholesaleOrderID, string CompanyLocationID, string StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_Selectby_Id&Location";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrderItems_SelectByWSOrderId_CompLocID(string WholesaleOrderID, string CompanyLocationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_SelectByWSOrderId_CompLocID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        param.Value = CompanyLocationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tbl_WholesaleOrderItems_Insert(String WholesaleOrderID, String StockItemID, String OrderQuantity, String WholesaleOrderItem, String WholesaleLocation, string wholesaleorderPanelID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderQuantity";
        if (OrderQuantity != string.Empty)
            param.Value = OrderQuantity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderItem";
        if (WholesaleOrderItem != string.Empty)
            param.Value = WholesaleOrderItem;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 255;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleLocation";
        if (WholesaleLocation != string.Empty)
            param.Value = WholesaleLocation;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@wholesaleorderPanelID";
        if (wholesaleorderPanelID != string.Empty)
            param.Value = wholesaleorderPanelID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int16;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tbl_WholesaleOrderItems_Update(string WholesaleOrderItemID, String WholesaleOrderID, String StockItemID, String OrderQuantity, String WholesaleOrderItem, String WholesaleCode, String WholesaleLocation)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderItemID";
        param.Value = WholesaleOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@OrderQuantity";
        param.Value = OrderQuantity;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderItem";
        param.Value = WholesaleOrderItem;
        param.DbType = DbType.String;
        param.Size = 250;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleCode";
        param.Value = WholesaleCode;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleLocation";
        param.Value = WholesaleLocation;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tbl_WholesaleOrderItems_Delete(string WholesaleOrderItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderItemID";
        param.Value = WholesaleOrderItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }


    public static bool tbl_WholesaleOrderItems_DeleteWholesaleOrderID(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_DeleteWholesaleOrderID";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tbl_WholesaleOrderItems_SelectQty(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_SelectQty";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrdersReport_Search(string Delivered, string CustomerID, string startdate, string enddate, string DateType, string StockItemID, String Location, string WholesaleCategoryID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrdersReport_Search";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleCategoryID";
        if (WholesaleCategoryID != string.Empty)
            param.Value = WholesaleCategoryID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();

        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrdersReport_ByLocSearch(string Delivered, string CustomerID, string startdate, string enddate, string DateType, string StockItemID, String Location)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrdersReport_ByLocSearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Delivered";
        if (Delivered != string.Empty)
            param.Value = Delivered;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DateType";
        if (DateType != string.Empty)
            param.Value = DateType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrders_getWholesaleItemState(string StockItemID, String State)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrders_getWholesaleItemState";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@State";
        if (State != string.Empty)
            param.Value = State;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    ///ORDER INSTALLED REPORT

    //public static DataTable tblOrderInstalledReport_Search(string CustomerID, string startdate, string enddate, string StockItemID, String Location)
    //{
    //    DbCommand comm = DataAccess.CreateCommand();
    //    comm.CommandText = "tblOrderInstalledReport_Search";

    //    DbParameter param = comm.CreateParameter();
    //    param.ParameterName = "@CustomerID";
    //    if (CustomerID != string.Empty)
    //        param.Value = CustomerID;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.Int32;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@startdate";
    //    if (startdate != string.Empty)
    //        param.Value = startdate;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.DateTime;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@enddate";
    //    if (enddate != string.Empty)
    //        param.Value = enddate;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.DateTime;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@StockItemID";
    //    if (StockItemID != string.Empty)
    //        param.Value = StockItemID;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.Int32;
    //    comm.Parameters.Add(param);

    //    param = comm.CreateParameter();
    //    param.ParameterName = "@Location";
    //    if (Location != string.Empty)
    //        param.Value = Location;
    //    else
    //        param.Value = DBNull.Value;
    //    param.DbType = DbType.String;
    //    param.Size = 100;
    //    comm.Parameters.Add(param);

    //    DataTable result = new DataTable();
    //    result = DataAccess.ExecuteSelectCommand(comm);
    //    try
    //    {
    //    }
    //    catch
    //    {
    //        // log errors if any
    //    }
    //    return result;
    //}

    public static DataTable tbl_WholesaleOrderInstalledReport_ByLocSearch(string CustomerID, string startdate, string enddate, string StockItemID, String Location)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderInstalledReport_ByLocSearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tbl_WholesaleOrderInstalledReport_ByInstalled(string CustomerID, string startdate, string enddate, string StockItemID, String Location, string type)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderInstalledReport_ByInstalled";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        if (CustomerID != string.Empty)
            param.Value = CustomerID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Location";
        if (Location != string.Empty)
            param.Value = Location;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@type";
        if (type != string.Empty)
            param.Value = type;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_WholesaleOrderItems_whole_Delete(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_WholesaleOrderItems_whole_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tblWholesaleStockItemInventoryHistory_Insert(String HeadId, String StockItemID, String CompanyLocationID, String CurrentStock, String Stock, String UserId, string WholesaleOrderID, string WholesaleOrderItemID, string InvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleStockItemInventoryHistory_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@HeadId";
        param.Value = HeadId;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyLocationID";
        if (CompanyLocationID != string.Empty)
            param.Value = CompanyLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CurrentStock";
        if (CurrentStock != string.Empty)
            param.Value = CurrentStock;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Stock";
        if (Stock != string.Empty)
            param.Value = Stock;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@UserId";
        if (UserId != string.Empty)
            param.Value = UserId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderItemID";
        if (WholesaleOrderItemID != string.Empty)
            param.Value = WholesaleOrderItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        if (InvoiceNo != string.Empty)
            param.Value = InvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static DataTable tblWholesaleStockItemInventoryHistory_SelectStock(string WholesaleOrderID, String InvoiceNo)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleStockItemInventoryHistory_SelectStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblWholesaleOrders_UpdateDeduct(string WholesaleOrderID, string StockDeductBy, string IsDeduct, String StockAllocationStore, String StockDeductDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleOrders_UpdateDeduct";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductBy";
        if (StockDeductBy != string.Empty)
            param.Value = StockDeductBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        param.Value = IsDeduct;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockAllocationStore";
        param.Value = StockAllocationStore;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductDate";
        param.Value = StockDeductDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblWholesaleOrders_UpdateDeductDate(string WholesaleOrderID, String StockDeductDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleOrders_UpdateDeductDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductDate";
        param.Value = StockDeductDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblWholesaleStockItemInventoryHistory_UpdateRevert(string id, string RevertFlag, string RevertDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleStockItemInventoryHistory_UpdateRevert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertFlag";
        if (RevertFlag != string.Empty)
            param.Value = RevertFlag;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertDate";
        if (RevertDate != string.Empty)
            param.Value = RevertDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static bool tblWholesaleOrders_UpdateRevert(string WholesaleOrderID, string StockDeductBy, string IsDeduct)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleOrders_UpdateRevert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockDeductBy";
        if (StockDeductBy != string.Empty)
            param.Value = StockDeductBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsDeduct";
        param.Value = IsDeduct;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblWholesaleStockItemInventoryHistory_WholesaleOrderID(string WholesaleOrderID, String StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleStockItemInventoryHistory_WholesaleOrderID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblWholesaleItemInventoryHistory_SelectStock(string WholesaleOrderID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleItemInventoryHistory_SelectStock";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblWholesaleItemInventoryHistory_revert(string WholesaleOrderID, String StockItemID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleItemInventoryHistory_revert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        param.Value = StockItemID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblWholesaleStockDeductSerialNo_Insert(String StockItemID, String StockLocationID, String SerialNo, String Pallet, String InvoiceNo,String WholesaleOrderID, String DeductedBy, string DeductedDate, string InventoryHistoryId)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleStockDeductSerialNo_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@StockItemID";
        if (StockItemID != string.Empty)
            param.Value = StockItemID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StockLocationID";
        if (StockLocationID != string.Empty)
            param.Value = StockLocationID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        if (SerialNo != string.Empty)
            param.Value = SerialNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        if (InvoiceNo != string.Empty)
            param.Value = InvoiceNo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        if (WholesaleOrderID != string.Empty)
            param.Value = WholesaleOrderID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeductedBy";
        if (DeductedBy != string.Empty)
            param.Value = DeductedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@DeductedDate";
        if (DeductedDate != string.Empty)
            param.Value = DeductedDate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@InventoryHistoryId";
        if (InventoryHistoryId != string.Empty)
            param.Value = InventoryHistoryId;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = DataAccess.ExecuteNonQuery(comm);
        return id;
    }

    public static DataTable tblDeductStockDeductSerialNo_SelectBy_InvNoWoid_RFlag(string InvoiceNo,string WholesaleOrderID, string RevertFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblDeductStockDeductSerialNo_SelectBy_InvNoWoid_RFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@InvoiceNo";
        param.Value = InvoiceNo;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertFlag";
        param.Value = RevertFlag;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblWholesaleStockDeductSerialNo_UpdateRevert(string SerialNo, String Pallet, string WholesaleOrderID, string RevertDate, string RevertedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblWholesaleStockDeductSerialNo_UpdateRevert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@SerialNo";
        param.Value = SerialNo;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Pallet";
        if (Pallet != string.Empty)
            param.Value = Pallet;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@WholesaleOrderID";
        param.Value = WholesaleOrderID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertDate";
        param.Value = RevertDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@RevertedBy";
        param.Value = RevertedBy;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

}