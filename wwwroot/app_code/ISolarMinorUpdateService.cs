﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISolarMinorUpdateService" in both code and config file together.
[ServiceContract]
public interface ISolarMinorUpdateService
{
    [OperationContract]
    void DoWork();
    [WebInvoke(Method = "POST",
         RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "/tblProjects_UpdateDeductNote_FromService")]
    SolarMinorUpdateService.UpdateData tblProjects_UpdateDeductNote_FromService(string projectId, string projectnumber, string note,
         string pickupdate, string installerid, string pickupnote, string updatedby,
         string updateddate, string picklistid);

    [WebInvokeAttribute(Method = "GET",
         RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "/GetProjectsRelatedDetails/{ProjectId}")]
    SolarMinorUpdateService.ProjectDetails GetProjectsRelatedDetails(string ProjectId);

    [WebInvoke(Method = "POST",
         RequestFormat = WebMessageFormat.Json,
          ResponseFormat = WebMessageFormat.Json,
         BodyStyle = WebMessageBodyStyle.Wrapped,
         UriTemplate = "/tblprojectsUpdateStatus")]
    SolarMinorUpdateService.UpdateData tblprojectsUpdateStatus(string ProjectId);

}
