using System;
using System.Data;
using System.Data.Common;
using System.Web;

public struct SttblPromo
{
    public string ContactID;
    public string Promo;
    public string PromoTypeID;
    public string PromoSent;
    public string Interested;
    public string NewsID;
    public string CreateDate;
    public string CreatedBy;
    public string PromoGUID;
    public string ReadFlag;
}

public class ClstblPromo
{
    public static SttblPromo tblPromo_SelectByPromoID(String PromoID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_SelectByPromoID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PromoID";
        param.Value = PromoID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblPromo details = new SttblPromo();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.ContactID = dr["ContactID"].ToString();
            details.Promo = dr["Promo"].ToString();
            details.PromoTypeID = dr["PromoTypeID"].ToString();
            details.PromoSent = dr["PromoSent"].ToString();
            details.Interested = dr["Interested"].ToString();
            details.NewsID = dr["NewsID"].ToString();
            details.CreateDate = dr["CreateDate"].ToString();
            details.CreatedBy = dr["CreatedBy"].ToString();
            details.PromoGUID = dr["PromoGUID"].ToString();
            details.ReadFlag = dr["ReadFlag"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblPromo_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblPromo_GetData_Search(string EmployeeID, String Promo, String Interested, String StreetState, String contactname)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_GetData_Search";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Promo";
        if (Promo != string.Empty)
            param.Value = Promo;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Interested";
        if (Interested != string.Empty)
            param.Value = Interested;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        param.Value = StreetState;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@contactname";
        param.Value = contactname;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblPromo_Insert(String ContactID, String Promo, String PromoTypeID, String PromoSent, String Interested, string NewsID, string CreatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Promo";
        param.Value = Promo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PromoTypeID";
        if (PromoTypeID != string.Empty)
            param.Value = PromoTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PromoSent";
        if (PromoSent != string.Empty)
            param.Value = PromoSent;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Interested";
        if (Interested != string.Empty)
            param.Value = Interested;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NewsID";
        if (NewsID != string.Empty) 
            param.Value = NewsID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedBy";
        if (CreatedBy != string.Empty)
            param.Value = CreatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblPromo_Update(string PromoID, String ContactID, String Promo, String PromoTypeID, String PromoSent, String Interested, string NewsID, string CreatedBy)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PromoID";
        param.Value = PromoID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        if (ContactID != string.Empty)
            param.Value = ContactID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Promo";
        param.Value = Promo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PromoTypeID";
        if (PromoTypeID != string.Empty)
            param.Value = PromoTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PromoSent";
        if (PromoSent != string.Empty)
            param.Value = PromoSent;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Interested";
        if (Interested != string.Empty)
            param.Value = Interested;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@NewsID";
        if (NewsID != string.Empty)
            param.Value = NewsID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreatedBy";
        if (CreatedBy != string.Empty)
            param.Value = CreatedBy;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblPromo_InsertUpdate(Int32 PromoID, String ContactID, String Promo, String PromoTypeID, String PromoSent, String Interested)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PromoID";
        param.Value = PromoID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Promo";
        param.Value = Promo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PromoTypeID";
        param.Value = PromoTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PromoSent";
        param.Value = PromoSent;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Interested";
        if (Interested != string.Empty)
            param.Value = Interested;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblPromo_Delete(string PromoID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PromoID";
        param.Value = PromoID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static DataTable tblPromo_Select_ByContactID(String ContactID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_Select_ByContactID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblPromo_update_Interested(string PromoID, String Interested)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_update_Interested";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PromoID";
        param.Value = PromoID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Interested";
        if (Interested != string.Empty)
            param.Value = Interested;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblPromo_SendMail()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_SendMail";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblPromo_UnSubscribe(string PromoGUID, string Interested)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_UnSubscribe";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PromoGUID";
        param.Value = PromoGUID;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Interested";
        param.Value = Interested;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblPromo_UpdatePromoOfferID(string PromoID, String PromoOfferID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_UpdatePromoOfferID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PromoID";
        param.Value = PromoID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PromoOfferID";
        if (PromoOfferID != string.Empty)
            param.Value = PromoOfferID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);



        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblPromo_UpdateIsArchive(string ContMobile, String IsArchive)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_UpdateIsArchive";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        param.Value = ContMobile;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsArchive";
        if (IsArchive != string.Empty)
            param.Value = IsArchive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);



        int result = -1;
        result = DataAccess.ExecuteNonQuery(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblPromo_UpdateResponseMsg(string ContMobile, String ResponseMsg)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_UpdateResponseMsg";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        param.Value = ContMobile;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ResponseMsg";
        if (ResponseMsg != string.Empty)
            param.Value = ResponseMsg;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);



        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblPromo_UpdateInterested(string ContMobile, String Interested)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_UpdateInterested";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        param.Value = ContMobile;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Interested";
        if (Interested != string.Empty)
            param.Value = Interested;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);



        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static DataTable tblPromo_tracker(string startdate, String enddate, String Interested, String contactname, string userid, string curuserid, string SalesTeamID, string EmployeeID, string historic, string ProjectStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_tracker";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Interested";
        if (Interested != string.Empty)
            param.Value = Interested;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@contactname";
        param.Value = contactname;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@curuserid";
        param.Value = curuserid;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@historic";
        if (historic != string.Empty)
            param.Value = historic;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }


    public static DataTable tblPromo_othertracker1(string userid, string curuserid, string startdate, String enddate, String Interested, String contactname, string SalesTeamID, string EmployeeID, string historic, string ProjectStatusID, string prjno, string datetype, string Intrest, string PromoType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_othertracker";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@curuserid";
        param.Value = curuserid;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Interested";
        if (Interested != string.Empty)
            param.Value = Interested;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@contactname";
        param.Value = contactname;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Intrest";
        if (Intrest != null && Intrest != "")
            param.Value = Intrest;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@historic";
        if (historic != null && historic != "")
            param.Value = historic;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@prjnum";
        if (prjno != string.Empty)
            param.Value = prjno;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@datetype";
        if (datetype != string.Empty)
            param.Value = datetype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PromoType";
        if (PromoType != string.Empty)
            param.Value = PromoType;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {

        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblPromo_othertracker(string userid, string curuserid, string startdate, String enddate, String Interested, String contactname, string SalesTeamID, string EmployeeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_othertracker";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@curuserid";
        param.Value = curuserid;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Interested";
        if (Interested != string.Empty)
            param.Value = Interested;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@contactname";
        param.Value = contactname;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //param = comm.CreateParameter();
        //param.ParameterName = "@ProjectStatusID";
        //if (ProjectStatusID != string.Empty)
        //    param.Value = ProjectStatusID;
        //else
        //    param.Value = DBNull.Value;
        //param.DbType = DbType.String;
        //param.Size = 100;
        //comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
            
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblPromo_UpdatePromoUser(string PromoID, String updatepromouserid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_UpdatePromoUser";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PromoID";
        param.Value = PromoID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@updatepromouserid";
        if (updatepromouserid != string.Empty)
            param.Value = updatepromouserid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);



        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblPromo_UpdateReadFlag(string PromoID, String ReadFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_UpdateReadFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PromoID";
        param.Value = PromoID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReadFlag";
        if (ReadFlag != string.Empty)
            param.Value = ReadFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblPromo_UpdateResponseFlag(string ContMobile, String ReadFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_UpdateResponseFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        param.Value = ContMobile;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReadFlag";
        if (ReadFlag != string.Empty)
            param.Value = ReadFlag;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);



        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblPromo_Checkpromo(string ContMobile)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_Checkpromo";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        param.Value = ContMobile;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblPromo_GetContact(string ContMobile)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_GetContact";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        param.Value = ContMobile;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tblPromo_UpdateIsArchivebyPromoID(string PromoID, String IsArchive)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_UpdateIsArchivebyPromoID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PromoID";
        param.Value = PromoID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsArchive";
        if (IsArchive != string.Empty)
            param.Value = IsArchive;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tblPromo_PromoSend(string CustTypeID, string ContMobile, string ContPhone, string ResCom, string CustSourceID, string Client, string CompanyNumber, string ContFirst, string ContLast, string StreetPostCode, string StreetState, string StreetCity, String ContEmail, String EmployeeID, string SalesTeamID, string startdate, string enddate, string ProjectStatusID, string StreetAddress, string CustSourceSubID ,String ContLeadStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_PromoSend";



        //HttpContext.Current.Response.Write("'" + CustTypeID + "','" + ContMobile + "','" + ContPhone + "','" + ResCom + "','" + CustSourceID + "','" + Client + "','" + CompanyNumber + "','" + ContFirst + "','" + ContLast + "','" + StreetPostCode + "','" + StreetState + "','" + StreetCity + "','" + ContEmail + "','" + EmployeeID + "','" + SalesTeamID + "','" + startdate + "','" + enddate + "','" + ProjectStatusID + "','" + StreetAddress + "','" + CustSourceSubID + "','" + ContLeadStatusID + "'");
        //HttpContext.Current.Response.End();

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        if (CustTypeID != string.Empty)
            param.Value = CustTypeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContMobile";
        if (ContMobile != string.Empty)
            param.Value = ContMobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContPhone";
        if (ContPhone != string.Empty)
            param.Value = ContPhone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ResCom";
        if (ResCom != string.Empty)
            param.Value = ResCom;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceID";
        if (CustSourceID != string.Empty)
            param.Value = CustSourceID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Client";
        if (Client != string.Empty)
            param.Value = Client;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CompanyNumber";
        if (CompanyNumber != string.Empty)
            param.Value = CompanyNumber;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContFirst";
        if (ContFirst != string.Empty)
            param.Value = ContFirst;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContLast";
        if (ContLast != string.Empty)
            param.Value = ContLast;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetPostCode";
        if (StreetPostCode != string.Empty)
            param.Value = StreetPostCode;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetState";
        if (StreetState != string.Empty)
            param.Value = StreetState;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetCity";
        if (StreetCity != string.Empty)
            param.Value = StreetCity;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ContEmail";
        if (ContEmail != string.Empty)
            param.Value = ContEmail;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        if (EmployeeID != string.Empty)
            param.Value = EmployeeID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesTeamID";
        if (SalesTeamID != string.Empty)
            param.Value = SalesTeamID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);


        param = comm.CreateParameter();
        param.ParameterName = "@startdate";
        if (startdate != string.Empty)
            param.Value = startdate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@enddate";
        if (enddate != string.Empty)
            param.Value = enddate;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        if (ProjectStatusID != string.Empty)
            param.Value = ProjectStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@StreetAddress";
        if (StreetAddress != string.Empty)
            param.Value = StreetAddress;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustSourceSubID";
        if (CustSourceSubID != string.Empty)
            param.Value = CustSourceSubID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        param = comm.CreateParameter();
        param.ParameterName = "@ContLeadStatusID";
        if (ContLeadStatusID != string.Empty)
            param.Value = ContLeadStatusID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
       
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
        //return DataAccess.ExecuteSelectCommand(comm);
    }
    public static int tblPromoLog_Insert(String ContactID, String ResponseMsg)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromoLog_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        if (ContactID != string.Empty)
            param.Value = ContactID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ResponseMsg";
        param.Value = ResponseMsg;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);
 

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static DataTable tblPromo_promocount(string userid, string curuserid)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_promocount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@userid";
        param.Value = userid;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@curuserid";
        param.Value = curuserid;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblPromo_UpdateData(String Promo, String PromoSent, string promotype)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_UpdateData";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@Promo";
        param.Value = Promo;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PromoSent";
        if (PromoSent != string.Empty)
            param.Value = PromoSent;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@promotype";
        if (promotype != string.Empty)
            param.Value = promotype;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        //int result = -1;
        //try
        //{
        //    result = Convert.ToInt32( DataAccess.ExecuteNonQuery(comm));
        //}
        //catch
        //{
        //    // log errors if any
        //}
        //return (result !=-1);
        int result = Convert.ToInt32(DataAccess.ExecuteNonQuery(comm));
        return result;
    }

    public static int tblPromo_Exists(String ContactID, String Created)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ContactID";
        param.Value = ContactID;
        param.DbType = DbType.Int32;
        //param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Created";
        if (Created != string.Empty)
            param.Value = Created;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }

    public static bool tblPromo_UpdateCreateDate(string PromoID, String Created)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_UpdateCreateDate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PromoID";
        param.Value = PromoID;
        param.DbType = DbType.Int32;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Created";
        if (Created != string.Empty)
            param.Value = Created;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);
        
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static bool tblPromo_UpdateResponseMsgUpdated(string PromoID, String ResponseMsg)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_UpdateResponseMsgUpdated";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PromoID";
        param.Value = PromoID;
        param.DbType = DbType.Int32;
        //param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ResponseMsg";
        if (ResponseMsg != string.Empty)
            param.Value = ResponseMsg;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable tbl_SMSReceivedLog_GetData()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_SMSReceivedLog_GetData";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static int tblPromo_UpdateQuery()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_UpdateQuery";

        int result = Convert.ToInt32(DataAccess.ExecuteNonQuery(comm));
        return result;
    }

    public static bool tblPromo_UpdateArcResponseMsg(string PromoID, String ResponsMsg)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPromo_UpdateArcResponseMsg";//Add DEfault Zero IS Archive

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@promoid";
        param.Value = PromoID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ResponseMsg";
        if (ResponsMsg != string.Empty)
            param.Value = ResponsMsg;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch (Exception ex)
        {
            // log errors if any
        }
        return (result != -1);
    }

    public static DataTable GetCOntactID(string Var)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "GetCOntactID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@string";
        param.Value = Var;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}