﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Security;

// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICompanyService" in both code and config file together.
[ServiceContract]
public interface ICompanyService
{
    [OperationContract]
    [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        //UriTemplate = "FTP/{RestToken}")]
        UriTemplate = "PrintData/{html}")]
    //UriTemplate = "FTP/euro@2015")]
    string PrintData(string html);


    [OperationContract]
    [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "CheckUserStatus/{Uname}/{Pass}")]
    string CheckUserStatus(string Uname, string Pass);

    //CheckUserByRole

    [OperationContract]
    [WebInvoke(Method = "GET",
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped,
        UriTemplate = "CheckUserByRole/{userid}")]
    string CheckUserByRole(string userid);

    [OperationContract]
    [WebInvoke(Method = "GET",
        UriTemplate = "GetCustomerCount/{userid}",
          BodyStyle = WebMessageBodyStyle.Wrapped,
        ResponseFormat = WebMessageFormat.Json)]
    CompanyService.GetCustomer[] GetCustomerCount(string userid);



    //[OperationContract]
    //[WebInvoke(Method = "GET",
    //    UriTemplate = "Getreportingemp/{userid}",
    //      BodyStyle = WebMessageBodyStyle.Wrapped,
    //    ResponseFormat = WebMessageFormat.Json)]
    //AppService.Reportdata[] Getreportingemp(string userid);

    [OperationContract]
    [WebInvoke(Method = "GET",
        UriTemplate = "GetCustomerType",
          BodyStyle = WebMessageBodyStyle.Wrapped,
        ResponseFormat = WebMessageFormat.Json)]
    CompanyService.CustomerType[] GetCustomerType();

    [OperationContract]
    [WebInvoke(Method = "GET",
        UriTemplate = "GetCustomerSource",
        BodyStyle = WebMessageBodyStyle.Wrapped,
        ResponseFormat = WebMessageFormat.Json)]
    CompanyService.CustomerSource[] GetCustomerSource();


    //[OperationContract]
    //[WebInvoke(Method = "GET",
    //    UriTemplate = "GetCustomerSubSource/{sourceid}",
    //      BodyStyle = WebMessageBodyStyle.Wrapped,
    //    ResponseFormat = WebMessageFormat.Json)]
    //CompanyService.CustomerSubSource[] GetCustomerSubSource(string userid);

    [OperationContract]
    [WebInvoke(Method = "GET",
        UriTemplate = "GetStateData",
          BodyStyle = WebMessageBodyStyle.Wrapped,
        ResponseFormat = WebMessageFormat.Json)]
    CompanyService.GetState[] GetStateData();


    [OperationContract]
    [WebInvoke(Method = "GET",
        UriTemplate = "GetUnitType",
        BodyStyle = WebMessageBodyStyle.Wrapped,
        ResponseFormat = WebMessageFormat.Json)]
    CompanyService.UnitTypes[] GetUnitType();



    [OperationContract]
    [WebInvoke(Method = "GET",
        UriTemplate = "GetPostalUnitType",
        BodyStyle = WebMessageBodyStyle.Wrapped,
        ResponseFormat = WebMessageFormat.Json)]
    CompanyService.PostalUnitType[] GetPostalUnitType();

    [OperationContract]
    [WebInvoke(Method = "GET",
        UriTemplate = "GetStreetTypes",
        BodyStyle = WebMessageBodyStyle.Wrapped,
        ResponseFormat = WebMessageFormat.Json)]
    CompanyService.StreetTypes[] GetStreetTypes();

    [OperationContract]
    [WebInvoke(Method = "GET",
          UriTemplate = "GetCompany/{userid}/{CompanyNo}/{State}/{Customer}/{streetaddress}/{streetcity}/{startdate}/{enddate}/{startindex}",
          BodyStyle = WebMessageBodyStyle.Wrapped,
         ResponseFormat = WebMessageFormat.Json)]
    CompanyService.company[] GetCompany(string userid, string CompanyNo, string State, string Customer, string streetaddress, string streetcity, string startdate, string enddate, string startindex);


   
    [OperationContract]
    [WebInvoke(Method = "GET",
          UriTemplate = "CustomersInsert/{userid}/{Salutation}/{FirstName}/{LastName}/{Mobile}/{Email}/{Phone}/{StreetAddress}/{Area}",
          BodyStyle = WebMessageBodyStyle.Wrapped,
         ResponseFormat = WebMessageFormat.Json)]
    string CustomersInsert(string userid, string Salutation, string FirstName, string LastName, string Mobile, string Email, string Phone, string StreetAddress, string Area);

    [OperationContract]
    [WebInvoke(Method = "GET",
          UriTemplate = "GetCompanyDetailOnEdit/{CustomerID}",
          BodyStyle = WebMessageBodyStyle.Wrapped,
         ResponseFormat = WebMessageFormat.Json)]
    CompanyService.company[] GetCompanyDetailOnEdit(string CustomerID);


    [OperationContract]
    [WebInvoke(Method = "GET",
          UriTemplate = "CustomersUpdate/{CustomerID}/{userid}/{Salutation}/{FirstName}/{LastName}/{Mobile}/{Email}/{Phone}/{StreetAddress}/{Area}",
          BodyStyle = WebMessageBodyStyle.Wrapped,
         ResponseFormat = WebMessageFormat.Json)]
    string CustomersUpdate(string CustomerID,  string userid, string Salutation, string FirstName, string LastName, string Mobile, string Email, string Phone, string StreetAddress, string Area);

     [OperationContract]
    [WebInvoke(Method = "GET",
          UriTemplate = "GetCompanyDetail/{CustomerID}/{userid}",
          BodyStyle = WebMessageBodyStyle.Wrapped,
         ResponseFormat = WebMessageFormat.Json)]
    CompanyService.company[] GetCompanyDetail(string CustomerID,string userid);


     [OperationContract]
     [WebInvoke(Method = "GET",
           UriTemplate = "CustomersDelete/{CustomerID}",
           BodyStyle = WebMessageBodyStyle.Wrapped,
          ResponseFormat = WebMessageFormat.Json)]
     string CustomersDelete(string CustomerID);


     [OperationContract]
     [WebInvoke(Method = "GET",
           UriTemplate = "GetCustomerImage/{CustomerID}",
           BodyStyle = WebMessageBodyStyle.Wrapped,
          ResponseFormat = WebMessageFormat.Json)]
     CompanyService.CustomerImage[] GetCustomerImage(string CustomerID);



     [OperationContract]
     [WebInvoke(Method = "GET",
           UriTemplate = "CustImageDelete/{CustImageID}",
           BodyStyle = WebMessageBodyStyle.Wrapped,
          ResponseFormat = WebMessageFormat.Json)]
     string CustImageDelete(string CustImageID);

        
    //[OperationContract]
    //[WebInvoke(Method = "GET",
    //      UriTemplate = "GetCustomerString/{userid}/{CompanyNo}/{State}/{Customer}/{streetaddress}/{streetcity}/{startdate}/{enddate}/{startindex}",
    //      BodyStyle = WebMessageBodyStyle.Wrapped,
    //     ResponseFormat = WebMessageFormat.Json)]
    //string GetCustomerString(string userid, string CompanyNo, string State, string Customer, string streetaddress, string streetcity, string startdate, string enddate, string startindex);

}
