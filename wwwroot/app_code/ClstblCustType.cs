using System;
using System.Data;
using System.Data.Common;

public struct SttblCustType
{
    public string CustTypeID;
    public string CustType;
    public string Active;
    public string Seq;
}

public class ClstblCustType
{
    public static SttblCustType tblCustType_SelectByCustTypeID(String CustTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustType_SelectByCustTypeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        param.Value = CustTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblCustType details = new SttblCustType();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustType = dr["CustType"].ToString();
            details.Active = dr["Active"].ToString();
            details.Seq = dr["Seq"].ToString();

        }
        // return structure details
        return details;
    }

    public static SttblCustType tblCustType_SelectByCustTypeIDActive(String CustType)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustType_SelectByCustTypeIDActive";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustType";
        param.Value = CustType;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblCustType details = new SttblCustType();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustTypeID = dr["CustTypeID"].ToString();
            details.CustType = dr["CustType"].ToString();
            details.Active = dr["Active"].ToString();
            details.Seq = dr["Seq"].ToString();

        }
        // return structure details
        return details;
    }

    public static DataTable tblCustType_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustType_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustType_SelectActive()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustType_SelectActive";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblCustType_SelectActiveByAsc()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustType_SelectActiveByAsc";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static DataTable tblCustType_SelectActive_COmpany()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustType_SelectActive_COmpany";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblCustType_Insert(String CustType, String Active, String Seq)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustType_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustType";
        param.Value = CustType;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblCustType_Update(string CustTypeID, String CustType, String Active, String Seq)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustType_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        param.Value = CustTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustType";
        param.Value = CustType;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblCustType_InsertUpdate(Int32 CustTypeID, String CustType, String Active, String Seq)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustType_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        param.Value = CustTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustType";
        param.Value = CustType;
        param.DbType = DbType.String;
        param.Size = 20;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblCustType_Delete(string CustTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustType_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        param.Value = CustTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static int tblCustType_Exists(string CustType)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure CustType
        comm.CommandText = "tblCustType_Exists";

        // create a new parameter
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustType";
        param.Value = CustType;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int tblCustType_ExistsById(string CustType, string CustTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustType_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustType";
        param.Value = CustType;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustTypeID";
        param.Value = CustTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }
    public static DataTable tblCustTypeGetDataBySearch(string alpha, string Active)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblCustTypeGetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblCustTypeGetDataByAlpha(string alpha)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure type
        comm.CommandText = "tblCustTypeGetDataByAlpha";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
}