﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

/// <summary>
/// Summary description for ClsSMS
/// </summary>
public class ClsSMS
{
    public ClsSMS()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static int SendSMS(string to, string Msg)
    {
        int SucMsg = 0;

        //string PostData = "{\"From\":\"+61488824984\",\"To\":\"+61451831980\",\"Text\":\"Test Message\"}";
        //string PostData = "{\"From\":\"+61451831980\",\"To\":\"+919924860723\",\"Text\":\"Test Message\"}";

        if (!string.IsNullOrEmpty(to))
        {
            string[] To = to.Split(',');

            foreach (var item in To)
            {
                try
                {
                    string strurl = string.Format("https://api.fonedynamics.com/v2/Properties/SPBBXC4F7AJKLMSZAUYUL4FPDHMKVUZA/Messages");
                    WebRequest requestobject = WebRequest.Create(strurl);

                    #region MSgSentCode

                    requestobject.Credentials = new NetworkCredential("AA76IMPVNDSNF6GBRMFZ5TGQOT4S5W2A", "T5RVFUBY2QBY567J7DXSIJEIC5SW2FNA");
                    requestobject.Method = "POST";
                    requestobject.ContentType = "application/json";
                    requestobject.Headers.Add("Authorization", "basic " + "QUE3NklNUFZORFNORjZHQlJNRlo1VEdRT1Q0UzVXMkE6VDVSVkZVQlkyUUJZNTY3SjdEWFNJSkVJQzVTVzJGTkE=");
                    requestobject.UseDefaultCredentials = true;
                    requestobject.PreAuthenticate = true;

                    //string a = item;
                    string PostData = "{\"From\":\"+61488824984\",\"To\":\"" + item + "\",\"Text\":\" " + Msg + " \"}";

                    using (var StreamWriter = new StreamWriter(requestobject.GetRequestStream()))
                    {
                        StreamWriter.Write(PostData);
                        StreamWriter.Flush();
                        StreamWriter.Close();

                        #region FatchSendingData
                        try
                        {
                            var httpresponse = (HttpWebResponse)requestobject.GetResponse();
                            using (var streamReader = new StreamReader(httpresponse.GetResponseStream()))
                            {
                                //var result2 = streamReader.ReadToEnd();

                                //RootObject obj = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(result2);

                                //string MessageS_id = obj.Message.MessageSid;
                                //string Account_S_id = obj.Message.PropertySid;
                                //string From = obj.Message.From;
                                //string to = obj.Message.To;
                                //string MsgText = obj.Message.Text;
                                //string TimeStamp = obj.Message.Created;

                                //DataSet dataSet = Newtonsoft.Json.JsonConvert.DeserializeObject<DataSet>(result2);
                                //DataTable dt = dataSet.Tables["Messages"];
                            }
                        }
                        catch (Exception e1) { }
                        #endregion

                        SucMsg++;
                    }
                }
                catch (Exception ex)
                {

                }
            }
        }


        #endregion


        return SucMsg;
    }

    public static int GetReceivedSMS(string StartDate, string EndDate)
    {
        int Suc = 0;
        string PropertySid = "S534B2PUDL4GGPKK3DZMNKSLG5RU4GFQ";
        string Direction = "Receive";
        string Date_From = ClsSMS.StartDateTimeStamp(StartDate);
        string Date_To = ClsSMS.EndDateTimeStamp(EndDate);

        //string strurl = string.Format("https://api.fonedynamics.com/v2/Properties/" + PropertySid + "/Messages?Date_From=1590485004&Date_To=1590997390&Direction= " + Direction + "");
        string strurl = string.Format("https://api.fonedynamics.com/v2/Properties/" + PropertySid + "/Messages?Date_From=" + Date_From + " &Date_To=" + Date_To + " &Direction=" + Direction + "");
        WebRequest requestobject = WebRequest.Create(strurl);

        #region GetReplay
        string UserName = "AZD4CLT2KPKIMSEY3NMGSQOP5C4EBU3Y";
        string Password = "TH4YJ2AJCZIDEI3OYVNMDQEXQGY6O3ZY";
        requestobject.Credentials = new NetworkCredential(UserName, Password);
        requestobject.Method = "GET";
        requestobject.ContentType = "application/json";
        requestobject.Headers.Add("Authorization", "basic " + "QVpENENMVDJLUEtJTVNFWTNOTUdTUU9QNUM0RUJVM1k6VEg0WUoyQUpDWklERUkzT1lWTk1EUUVYUUdZNk8zWlk=");
        requestobject.UseDefaultCredentials = true;
        requestobject.PreAuthenticate = true;

        var httpresponse = (HttpWebResponse)requestobject.GetResponse();
        using (var streamReader = new StreamReader(httpresponse.GetResponseStream()))
        {
            var result2 = streamReader.ReadToEnd();

            DataSet dataSet = Newtonsoft.Json.JsonConvert.DeserializeObject<DataSet>(result2);
            DataTable dt = dataSet.Tables["Messages"];

            #region Insert Datatable Create
            DataTable dtSMSData = dt.Clone();

            dtSMSData.Columns.Remove("Created");
            dtSMSData.Columns.Add("Created", typeof(DateTime));
            //dtSMSData.Columns.Add("ReadFlag", typeof(Boolean));

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow Row = dtSMSData.NewRow();
                Row["MessageSid"] = dt.Rows[i]["MessageSid"].ToString();
                Row["AccountSid"] = dt.Rows[i]["AccountSid"].ToString();
                Row["PropertySid"] = dt.Rows[i]["PropertySid"].ToString();

                //Row["From"] = dt.Rows[i]["From"].ToString();
                //Row["To"] = dt.Rows[i]["To"].ToString();
                Row["From"] = GetNumberWithoutCountyCode(dt.Rows[i]["From"].ToString());
                Row["To"] = GetNumberWithoutCountyCode(dt.Rows[i]["To"].ToString());

                Row["Text"] = dt.Rows[i]["Text"].ToString();
                Row["DeliveryReceipt"] = Convert.ToBoolean(dt.Rows[i]["DeliveryReceipt"].ToString());
                Row["NumSegments"] = Convert.ToInt32(dt.Rows[i]["NumSegments"].ToString());
                Row["Status"] = dt.Rows[i]["Status"].ToString();
                Row["Direction"] = dt.Rows[i]["Direction"].ToString();
                Row["ErrorCode"] = dt.Rows[i]["ErrorCode"].ToString();

                Row["Created"] = ClsSMS.ConvertTimeStampToDateTime_Aus(dt.Rows[i]["Created"].ToString());

                //DateTime date = ClsSMS.ConvertTimeStampToDateTime_Aus(dt.Rows[i]["Created"].ToString());
                //Row["ReadFlag"] = false;

                dtSMSData.Rows.Add(Row);
            }
            #endregion

            if (dt.Rows.Count > 0)
            {
                Suc = ClsSMS.USP_InsertUpdate_tbl_SMSReceivedLog(dtSMSData);
            }

            #region Insert Into tblPromo
            //DataTable dt = ClstblPromo.tbl_SMSReceivedLog_GetData();

            if (dtSMSData.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string From = dtSMSData.Rows[i]["From"].ToString();
                    DataTable ContID = ClstblPromo.tblPromo_GetContact(From);

                    if (ContID.Rows.Count > 0)
                    {
                        int Exists = ClstblPromo.tblPromo_Exists(ContID.Rows[0]["ContactID"].ToString(), dtSMSData.Rows[i]["Created"].ToString());

                        string Status = "3";
                        if (dtSMSData.Rows[i]["Text"].ToString().ToLower() == "yes")//1
                        {
                            Status = "1";
                        }
                        else if (dtSMSData.Rows[i]["Text"].ToString().ToLower() == "stop")//2
                        {
                            Status = "2";
                            //ltmsg.Text = "";
                        }

                        if (Exists > 0)// Update
                        {

                        }
                        else // Insert
                        {
                            int SucPromo = ClstblPromo.tblPromo_Insert(ContID.Rows[0]["ContactID"].ToString(), "", "2", "", Status, "", "");
                            ClstblPromo.tblPromo_UpdateInterested(From, Status);
                            ClstblPromo.tblPromo_UpdateResponseMsgUpdated(SucPromo.ToString(), dtSMSData.Rows[i]["Text"].ToString());
                            ClstblPromo.tblPromo_UpdateCreateDate(SucPromo.ToString(), dtSMSData.Rows[i]["Created"].ToString());
                        }
                    }

                }
            }
            else
            {
                
            }
        }
        #endregion

        #endregion
        return Suc;
    }

    public static DateTime ConvertTimeStampToDateTime_Aus(string ts)
    {
        double TimeStamp = Convert.ToDouble(ts);
        System.DateTime dateTime = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
        dateTime = dateTime.AddSeconds(TimeStamp);

        //TimeZoneInfo TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("E. Australia Standard Time");
        //dateTime = TimeZoneInfo.ConvertTime(dateTime, TimeZoneInfo);

        DateTime formatted = Convert.ToDateTime(dateTime.ToString("dd/MM/yyyy hh:mm:ss.fff", System.Globalization.CultureInfo.InvariantCulture));

        return formatted;
    }

    public static int USP_InsertUpdate_tbl_SMSReceivedLog(DataTable SMSData)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "USP_InsertUpdate_tbl_SMSReceivedLog";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@UDT_SMSDate_Data";
        if (SMSData != null)
            param.Value = SMSData;
        else
            param.Value = DBNull.Value;
        //param.DbType = DbType.Object;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteNonQuery(comm));
        try
        {

        }
        catch (Exception ex)
        {
        }
        return id;
    }

    public static string StartDateTimeStamp(string date)
    {
        DateTime Date = Convert.ToDateTime(date).Date;

        //DateTime value = DateTime.Now;
        long epoch = (Date.Ticks - 621355968000000000) / 10000000;

        return epoch.ToString();
    }

    public static string EndDateTimeStamp(string date)
    {
        DateTime Date = Convert.ToDateTime(date).Date.AddDays(1).AddTicks(-1);

        long epoch = (Date.Ticks - 621355968000000000) / 10000000;

        return epoch.ToString();
    }

    public static string GetNumberWithoutCountyCode(string Number)
    {
        string Result = "";
        if (!string.IsNullOrEmpty(Number))
        {
            Result = Number.Substring(3).ToString();

            Result = "0" + Result;
        }
        return Result;
    }

    public static string GetNumberWithCountyCode(string Number, string CountyCode)
    {
        string Result = "";
        if (!string.IsNullOrEmpty(Number))
        {
            Result = "+" + CountyCode + Number;
        }
        return Result;
    }
}