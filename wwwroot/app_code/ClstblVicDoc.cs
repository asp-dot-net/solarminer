using System;
using System.Data;
using System.Data.Common;
using System.Web;

public struct SttblVicDoc
{
    public string ContactID;
    public string Promo;
    public string PromoTypeID;
    public string PromoSent;
    public string Interested;
    public string NewsID;
    public string CreateDate;
    public string CreatedBy;
    public string PromoGUID;
    public string ReadFlag;
}

public class ClstblVicDoc
{
    //public static SttblPromo tblPromo_SelectByPromoID(String PromoID)
    //{
    //    DbCommand comm = DataAccess.CreateCommand();
    //    comm.CommandText = "tblPromo_SelectByPromoID";

    //    DbParameter param = comm.CreateParameter();
    //    param.ParameterName = "@PromoID";
    //    param.Value = PromoID;
    //    param.DbType = DbType.Int32;
    //    comm.Parameters.Add(param);
    //    // execute the stored procedure
    //    DataTable table = DataAccess.ExecuteSelectCommand(comm);
    //    // wrap retrieved data into a ProductDetails object
    //    SttblPromo details = new SttblPromo();
    //    if (table.Rows.Count > 0)
    //    {
    //        // get the first table row
    //        DataRow dr = table.Rows[0];
    //        // get product details
    //        details.ContactID = dr["ContactID"].ToString();
    //        details.Promo = dr["Promo"].ToString();
    //        details.PromoTypeID = dr["PromoTypeID"].ToString();
    //        details.PromoSent = dr["PromoSent"].ToString();
    //        details.Interested = dr["Interested"].ToString();
    //        details.NewsID = dr["NewsID"].ToString();
    //        details.CreateDate = dr["CreateDate"].ToString();
    //        details.CreatedBy = dr["CreatedBy"].ToString();
    //        details.PromoGUID = dr["PromoGUID"].ToString();
    //        details.ReadFlag = dr["ReadFlag"].ToString();

    //    }
    //    // return structure details
    //    return details;
    //}
    public static DataTable tbl_ewr_upload_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_ewr_upload_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }

    public static bool tbl_ewr_upload_Insert(int ewr_id, String ewr_filename,string ewr_randomfilename,string ewr_filepath)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_ewr_upload_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ewr_id";
        param.Value = ewr_id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ewr_filename";
        param.Value = ewr_filename;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ewr_randomfilename";
        param.Value = ewr_randomfilename;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ewr_filepath";
        param.Value = ewr_filepath;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        //int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        //return id;
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    public static bool tbl_ewr_upload_Update(int ewr_id, String ewr_filename, string ewr_randomfilename, string ewr_filepath)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tbl_ewr_upload_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ewr_id";
        param.Value = ewr_id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ewr_filename";
        param.Value = ewr_filename;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ewr_randomfilename";
        param.Value = ewr_randomfilename;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ewr_filepath";
        param.Value = ewr_filepath;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }


    //public static bool tblPromo_Delete(string PromoID)
    //{
    //    DbCommand comm = DataAccess.CreateCommand();
    //    comm.CommandText = "tblPromo_Delete";
    //    DbParameter param = comm.CreateParameter();
    //    param.ParameterName = "@PromoID";
    //    param.Value = PromoID;
    //    param.DbType = DbType.Int32;
    //    comm.Parameters.Add(param);
    //    int result = -1;
    //    try
    //    {
    //        result = DataAccess.ExecuteNonQuery(comm);
    //    }
    //    catch
    //    {
    //    }
    //    return (result != -1);
    //}
}

   
   
   
    