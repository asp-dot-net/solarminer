using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


/// <summary>
/// Summary description for DataAccess
/// </summary>
public class DataAccess
{
    public DataAccess()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static DataTable ExecuteSelectCommand(DbCommand command)
    {
        // The DataTable to be returned 
        DataTable table;
        // Execute the command making sure the connection gets closed in the end
        //try
        //{
        // Open the data connection 
        command.Connection.Open();
        // Execute the command and save the results in a DataTable
        DbDataReader reader = command.ExecuteReader();
        table = new DataTable();
        table.Load(reader);
        // Close the reader 
        reader.Close();
        command.CommandTimeout = 900;
        command.Connection.Close();
        //}
        return table;
    }

    // execute an update, delete, or insert command 
    // and return the number of affected rows
    public static int ExecuteNonQuery(DbCommand command)
    {
        // The number of affected rows 
        int affectedRows = -1;
        // Execute the command making sure the connection gets closed in the end
        //try
        //{
        // Open the connection of the command
        command.Connection.Open();
        // Execute the command and get the number of affected rows
        affectedRows = command.ExecuteNonQuery();
        //}
        //catch (Exception ex)
        //{
        //    // Log eventual errors and rethrow them
        //    Utilities.LogError(ex);
        //    throw ex;
        //}
        //finally
        //{
        // Close the connection
        command.CommandTimeout = 900;
        command.Connection.Close();
        //}
        // return the number of affected rows
        return affectedRows;
    }

    // execute a select command and return a single result as a string
    public static string ExecuteScalar(DbCommand command)
    {
        // The value to be returned 
        string value = "";


        command.Connection.Open();
        // Execute the command and get the number of affected rows

        value = command.ExecuteScalar().ToString();
        command.CommandTimeout = 900;

        command.Connection.Close();

        return value;
    }

    // creates and prepares a new DbCommand object on a new connection
    public static DbCommand CreateCommand()

    {


        string connectionString = SiteConfiguration.DbConnectionString;


        System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(connectionString);


        DbCommand comm = conn.CreateCommand();



        comm.CommandType = CommandType.StoredProcedure;
        comm.CommandTimeout = 900;
        return comm;
    }

    public static IDataReader ExecuteReder(DbCommand command)
    {
        // The DataTable to be returned 
        IDataReader rdr = null;
        // Execute the command making sure the connection gets closed in the end
        try
        {
            // Open the data connection 
            command.Connection.Open();
            // Execute the command and save the results in a DataTable
            rdr = command.ExecuteReader();
            command.CommandTimeout = 900;
            // Close the reader 
            rdr.Close();
        }
        catch (Exception ex)
        {
            Utilities.LogError(ex);
            throw ex;
        }
        finally
        {
            // Close the connection
            command.Connection.Close();
        }
        return rdr;
    }
}
