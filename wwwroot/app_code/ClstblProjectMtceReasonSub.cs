using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
	public struct SttblProjectMtceReasonSub
	{
		public string ProjectMtceReasonSub;
		public string Active;
		public string Seq;

	}


public class ClstblProjectMtceReasonSub
{
	public static SttblProjectMtceReasonSub tblProjectMtceReasonSub_SelectByProjectMtceReasonSubID (String ProjectMtceReasonSubID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectMtceReasonSub_SelectByProjectMtceReasonSubID";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectMtceReasonSubID";
		param.Value = ProjectMtceReasonSubID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		// execute the stored procedure
		DataTable table = DataAccess.ExecuteSelectCommand(comm);
		// wrap retrieved data into a ProductDetails object
		 SttblProjectMtceReasonSub details = new SttblProjectMtceReasonSub();
		if (table.Rows.Count > 0)
		{
			// get the first table row
			DataRow dr = table.Rows[0];
			// get product details
			details.ProjectMtceReasonSub = dr["ProjectMtceReasonSub"].ToString();
			details.Active = dr["Active"].ToString();
			details.Seq = dr["Seq"].ToString();

		}
		// return structure details
		return details;
	}
	public static DataTable tblProjectMtceReasonSub_Select ()
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectMtceReasonSub_Select";

		DataTable result = new DataTable();
		try
		{
			result = DataAccess.ExecuteSelectCommand(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
    public static DataTable tblProjectMtceReasonSub_SelectASC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectMtceReasonSub_SelectASC";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
	public static int tblProjectMtceReasonSub_Insert ( String ProjectMtceReasonSub, String Active, String Seq)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectMtceReasonSub_Insert";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectMtceReasonSub";
		param.Value = ProjectMtceReasonSub;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


		int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
		return id;	}
	public static bool tblProjectMtceReasonSub_Update (string ProjectMtceReasonSubID, String ProjectMtceReasonSub, String Active, String Seq)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectMtceReasonSub_Update";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectMtceReasonSubID";
		param.Value = ProjectMtceReasonSubID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ProjectMtceReasonSub";
		param.Value = ProjectMtceReasonSub;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
			// log errors if any
		}
		return (result != -1);
	}
	public static string tblProjectMtceReasonSub_InsertUpdate (Int32 ProjectMtceReasonSubID, String ProjectMtceReasonSub, String Active, String Seq)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectMtceReasonSub_InsertUpdate";

		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectMtceReasonSubID";
		param.Value = ProjectMtceReasonSubID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@ProjectMtceReasonSub";
		param.Value = ProjectMtceReasonSub;
		param.DbType = DbType.String;
		param.Size = 50;
		comm.Parameters.Add(param);

		param = comm.CreateParameter();
		param.ParameterName = "@Active";
		param.Value = Active;
		param.DbType = DbType.Boolean;
		comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        if (Seq != string.Empty)
            param.Value = Seq;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


		string result = "";
		try
		{
			result = DataAccess.ExecuteScalar(comm);
		}
		catch
		{
			// log errors if any
		}
		return result;
	}
	public static bool tblProjectMtceReasonSub_Delete (string ProjectMtceReasonSubID)
	{
		DbCommand comm = DataAccess.CreateCommand();
		comm.CommandText = "tblProjectMtceReasonSub_Delete";
		DbParameter param = comm.CreateParameter();
		param.ParameterName = "@ProjectMtceReasonSubID";
		param.Value = ProjectMtceReasonSubID;
		param.DbType = DbType.Int32;
		comm.Parameters.Add(param);
		int result = -1;
		try
		{
			result = DataAccess.ExecuteNonQuery(comm);
		}
		catch
		{
		}
		return (result != -1);
	}
    public static int MtceReasonSubNameExists(string title)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "MtceReasonSubNameExists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@title";
        param.Value = title;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int MtceReasonSubNameExistsWithID(string title, string id)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "MtceReasonSubNameExistsWithID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@title";
        param.Value = title;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int returnid = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return returnid;
    }
    public static DataTable tblMtceReasonSubGetDataByAlpha(string alpha)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblMtceReasonSubGetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblMtceReasonSubGetDataBySearch(string alpha, string Active)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure type
        comm.CommandText = "tblMtceReasonSubGetDataBySearch";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
}