using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
public struct SttblProjectStatus
{
    public string ProjectStatus;
    public string Active;
    public string Seq;

}


public class ClstblProjectStatus
{
    public static SttblProjectStatus tblProjectStatus_SelectByProjectStatusID(String ProjectStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_SelectByProjectStatusID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        param.Value = ProjectStatusID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblProjectStatus details = new SttblProjectStatus();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.ProjectStatus = dr["ProjectStatus"].ToString();
            details.Active = dr["Active"].ToString();
            details.Seq = dr["Seq"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblProjectStatus_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjectStatus_SelectByActive()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_SelectByActive";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjectStatus_SelectActive()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_SelectActive";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjectStatus_SelectStatus()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_SelectStatus";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblProjectStatus_Insert(String ProjectStatus, String Active, String Seq)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatus";
        param.Value = ProjectStatus;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblProjectStatus_Update(string ProjectStatusID, String ProjectStatus, String Active, String Seq)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        param.Value = ProjectStatusID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatus";
        param.Value = ProjectStatus;
        param.DbType = DbType.String;
        param.Size = 30;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Seq";
        param.Value = Seq;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static bool tblProjectStatus_Delete(string ProjectStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        param.Value = ProjectStatusID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static int tblProjectStatus_Exists(string ProjectStatus)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_Exists";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatus";
        param.Value = ProjectStatus;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static int tblProjectStatus_ExistsById(string ProjectStatus, string ProjectStatusID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_ExistsById";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatus";
        param.Value = ProjectStatus;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectStatusID";
        param.Value = ProjectStatusID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        result = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return result;
    }
    public static DataTable tblProjectStatus_GetDataByAlpha(string alpha)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_GetDataByAlpha";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblProjectStatus_GetDataBySearch(string alpha, string Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_GetDataBySearch";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 50;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblProjectStatus_SelectDepRec()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_SelectDepRec";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjectStatus_SelectActiveStatus()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_SelectActiveStatus";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblProjectStatus_SelectADC()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjectStatus_SelectADC";

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}