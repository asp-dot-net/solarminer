﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClsClickCustomer
/// </summary>
public class ClsClickCustomer
{
    public static DataTable tblProjects_IsClickCustomerTrue(string CustomerName, string Address, string Email, string Mobile, string Phone, string ProjectID, string IsClickCustomer,string employeeid,string SalesRep)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblProjects_IsClickCustomerTrue";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerName";
        if (!string.IsNullOrEmpty(CustomerName))
            param.Value = CustomerName;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Address";
        if (!string.IsNullOrEmpty(Address))
            param.Value = Address;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        param.Size = 500;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Email";
        if (!string.IsNullOrEmpty(Email))
            param.Value = Email;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Mobile";
        if (!string.IsNullOrEmpty(Mobile))
            param.Value = Mobile;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Phone";
        if (!string.IsNullOrEmpty(Phone))
            param.Value = Phone;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ProjectID";
        if (!string.IsNullOrEmpty(ProjectID))
            param.Value = ProjectID;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@IsClickCustomer";
        if (!string.IsNullOrEmpty(IsClickCustomer))
            param.Value = IsClickCustomer;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.String;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@employeeid";
        if (!string.IsNullOrEmpty(employeeid))
            param.Value = employeeid;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int64;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@SalesRep";
        if (SalesRep != string.Empty)
            param.Value = SalesRep;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);


        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}