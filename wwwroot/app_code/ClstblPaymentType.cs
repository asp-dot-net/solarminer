using System;
using System.Data;
using System.Data.Common;


/// <summary>
/// Summary description for DataAccessn
/// </summary>
public struct SttblPaymentType
{
    public string PaymentType;
    public string Active;

}


public class ClstblPaymentType
{
    public static SttblPaymentType tblPaymentType_SelectByPaymentTypeID(String PaymentTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPaymentType_SelectByPaymentTypeID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PaymentTypeID";
        param.Value = PaymentTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblPaymentType details = new SttblPaymentType();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.PaymentType = dr["PaymentType"].ToString();
            details.Active = dr["Active"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblPaymentType_Select()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPaymentType_Select";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static DataTable tblPaymentType_SelectActive()
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPaymentType_SelectActive";

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblPaymentType_Insert(String PaymentType, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPaymentType_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PaymentType";
        param.Value = PaymentType;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblPaymentType_Update(string PaymentTypeID, String PaymentType, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPaymentType_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PaymentTypeID";
        param.Value = PaymentTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentType";
        param.Value = PaymentType;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblPaymentType_InsertUpdate(Int32 PaymentTypeID, String PaymentType, String Active)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPaymentType_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PaymentTypeID";
        param.Value = PaymentTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@PaymentType";
        param.Value = PaymentType;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        param.Value = Active;
        param.DbType = DbType.Boolean;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblPaymentType_Delete(string PaymentTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPaymentType_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PaymentTypeID";
        param.Value = PaymentTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }
    public static DataTable tblPaymentTypeGetDataBySearch(string alpha,string Active)
    {
        // get a configured DbCommand object
        DbCommand comm = DataAccess.CreateCommand();

        // set the stored procedure type
        comm.CommandText = "tblPaymentTypeGetDataBySearch";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@alpha";
        param.Value = alpha;
        param.DbType = DbType.String;
        param.Size = 100;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Active";
        if (Active != string.Empty)
            param.Value = Active;
        else
            param.Value = DBNull.Value;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        return DataAccess.ExecuteSelectCommand(comm);
    }
    public static DataTable tblPaymentType_SelectByPaymentTypeIDNew(String PaymentTypeID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblPaymentType_SelectByPaymentTypeIDNew";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@PaymentTypeID";
        param.Value = PaymentTypeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
}