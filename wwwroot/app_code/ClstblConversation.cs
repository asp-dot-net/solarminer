using System;
using System.Data;
using System.Data.Common;

public struct SttblConversation
{
    public string CustomerID;
    public string EmployeeID;
    public string Detail;
    public string CreateDate;
}

public class ClstblConversation
{
    public static SttblConversation tblConversation_SelectByConversationID(String ConversationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblConversation_SelectByConversationID";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ConversationID";
        param.Value = ConversationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        // execute the stored procedure
        DataTable table = DataAccess.ExecuteSelectCommand(comm);
        // wrap retrieved data into a ProductDetails object
        SttblConversation details = new SttblConversation();
        if (table.Rows.Count > 0)
        {
            // get the first table row
            DataRow dr = table.Rows[0];
            // get product details
            details.CustomerID = dr["CustomerID"].ToString();
            details.EmployeeID = dr["EmployeeID"].ToString();
            details.Detail = dr["Detail"].ToString();
            details.CreateDate = dr["CreateDate"].ToString();

        }
        // return structure details
        return details;
    }
    public static DataTable tblConversation_Select(string CustomerID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblConversation_Select";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        result = DataAccess.ExecuteSelectCommand(comm);
        try
        {
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static int tblConversation_Insert(String CustomerID, String EmployeeID, String Detail)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblConversation_Insert";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Detail";
        param.Value = Detail;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        int id = Convert.ToInt32(DataAccess.ExecuteScalar(comm));
        return id;
    }
    public static bool tblConversation_Update(string ConversationID, String CustomerID, String EmployeeID, String Detail, String CreateDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblConversation_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ConversationID";
        param.Value = ConversationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Detail";
        param.Value = Detail;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreateDate";
        param.Value = CreateDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static string tblConversation_InsertUpdate(Int32 ConversationID, String CustomerID, String EmployeeID, String Detail, String CreateDate)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblConversation_InsertUpdate";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ConversationID";
        param.Value = ConversationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CustomerID";
        param.Value = CustomerID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeID";
        param.Value = EmployeeID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@Detail";
        param.Value = Detail;
        param.DbType = DbType.String;
        param.Size = 1073741823;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@CreateDate";
        param.Value = CreateDate;
        param.DbType = DbType.DateTime;
        comm.Parameters.Add(param);


        string result = "";
        try
        {
            result = DataAccess.ExecuteScalar(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblConversation_Delete(string ConversationID)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblConversation_Delete";
        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@ConversationID";
        param.Value = ConversationID;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);
        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
        }
        return (result != -1);
    }

    public static bool tblConversation_EmployeeConv_Update(string id, string EmployeeConv, string ReadFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblConversation_EmployeeConv_Update";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@id";
        param.Value = id;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@EmployeeConv";
        param.Value = EmployeeConv;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReadFlag";
        param.Value = ReadFlag;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }
    public static DataTable tblConversation_GetCount(string EmployeeConv)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblConversation_GetCount";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeConv";
        param.Value = EmployeeConv;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        DataTable result = new DataTable();
        try
        {
            result = DataAccess.ExecuteSelectCommand(comm);
        }
        catch
        {
            // log errors if any
        }
        return result;
    }
    public static bool tblConversation_Update_ReadFlag(string EmployeeConv, string ReadFlag)
    {
        DbCommand comm = DataAccess.CreateCommand();
        comm.CommandText = "tblConversation_Update_ReadFlag";

        DbParameter param = comm.CreateParameter();
        param.ParameterName = "@EmployeeConv";
        param.Value = EmployeeConv;
        param.DbType = DbType.String;
        param.Size = 200;
        comm.Parameters.Add(param);

        param = comm.CreateParameter();
        param.ParameterName = "@ReadFlag";
        param.Value = ReadFlag;
        param.DbType = DbType.Int32;
        comm.Parameters.Add(param);

        int result = -1;
        try
        {
            result = DataAccess.ExecuteNonQuery(comm);
        }
        catch
        {
            // log errors if any
        }
        return (result != -1);
    }

}