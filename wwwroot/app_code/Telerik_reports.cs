﻿using System;
using System.Web;
using System.Data;
using System.IO;
using System.Configuration;
using System.Web.UI;

//using iTextSharp.text;
//using iTextSharp.text.pdf;

using Spire.Pdf;
using Spire.Pdf.Exporting;

/// <summary>
/// Summary description for Telerik_reports
/// </summary>
public class Telerik_reports
{

    public Telerik_reports()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    #region NileshRai
  
    public static void Generatequotenew(string ProjectID, string QuoteID)
    {
        // Telerik.Reporting.Report rpt = new EurosolarReporting.SolarMinorQuotePdfNew();.
        Telerik.Reporting.Report rpt = new EurosolarReporting.Quote_new();
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
        DataTable dtQuote = ClstblProjects.tblProjectQuotes_SelectByProjectID(ProjectID);
        string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
        dt = Clscustomerlead.tblCustomers_SelectByCustomerID_telerikdemo(stPro.CustomerID);
        dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        Telerik.Reporting.TextBox txtsw2 = rpt.Items.Find("textBox901", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsalesnm = rpt.Items.Find("txtsalesnm", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtSalesEmail = rpt.Items.Find("txtEmail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsalesph = rpt.Items.Find("txtsalesph", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtpanel = rpt.Items.Find("txtpanel", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtinv = rpt.Items.Find("txtinv", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox47 = rpt.Items.Find("textBox47", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsystemspcw = rpt.Items.Find("txtsystemspcw", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtinstcharge = rpt.Items.Find("textBox132", true)[0] as Telerik.Reporting.TextBox;
        //sTelerik.Reporting.TextBox txtother = rpt.Items.Find("txtother", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtaddprice = rpt.Items.Find("textBox1011", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtprice1 = rpt.Items.Find("txtprice1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtlessince = rpt.Items.Find("txtlessince", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txttotalprice = rpt.Items.Find("txttotalprice", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtttlprice1 = rpt.Items.Find("txtttlprice1", true)[0] as Telerik.Reporting.TextBox; 
        Telerik.Reporting.TextBox txtdeposit = rpt.Items.Find("txtdeposit", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtInstallAddress = rpt.Items.Find("txtInstallAddress", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtEmailSalesRep = rpt.Items.Find("txtEmailSalesRep", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtSysCap = rpt.Items.Find("txtcapcw", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtcapcw = rpt.Items.Find("txtcapcw", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtCustName = rpt.Items.Find("txtcustnm", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdate = rpt.Items.Find("textBox48", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtaddress = rpt.Items.Find("txtaddress", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtcustnm = rpt.Items.Find("txtcustnm", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtcustnm1 = rpt.Items.Find("txtcustnm1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtphone = rpt.Items.Find("txtphone", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtcustemail = rpt.Items.Find("txtcustemail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox137 = rpt.Items.Find("textBox1011", true)[0] as Telerik.Reporting.TextBox;//Txt addition Text
        Telerik.Reporting.TextBox txtbalance = rpt.Items.Find("txtbalance", true)[0] as Telerik.Reporting.TextBox;
       Telerik.Reporting.TextBox txtsysprice = rpt.Items.Find("txtsysprice", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.PictureBox pic1 = rpt.Items.Find("pictureBox14", true)[0] as Telerik.Reporting.PictureBox;
        Telerik.Reporting.PictureBox pic2 = rpt.Items.Find("pictureBox15", true)[0] as Telerik.Reporting.PictureBox;
        Telerik.Reporting.PictureBox pic3 = rpt.Items.Find("pictureBox16", true)[0] as Telerik.Reporting.PictureBox;
        Telerik.Reporting.TextBox txtvictorialoan = rpt.Items.Find("txtvictorialoan", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsolarvicrebate = rpt.Items.Find("txtsolarvicrebate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtquotno = rpt.Items.Find("txtquotno", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsw1 = rpt.Items.Find("textBox836", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtrooftype = rpt.Items.Find("txtrooftype", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txthousetype = rpt.Items.Find("txthousetype", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtswu = rpt.Items.Find("txtswu", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmeterphase = rpt.Items.Find("txtmeterphase", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtnmino = rpt.Items.Find("txtnmino", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtenerret = rpt.Items.Find("txtenerret", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtenerdist = rpt.Items.Find("txtenerdist", true)[0] as Telerik.Reporting.TextBox;
         Telerik.Reporting.TextBox txtprojectnotes = rpt.Items.Find("txtprojectnotes", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox1085 = rpt.Items.Find("textBox88", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox1019 = rpt.Items.Find("textBox1019", true)[0] as Telerik.Reporting.TextBox;
        //txtqno.Value = stPro.ProjectNumber;
        txtquotno.Value = stPro.ProjectNumber;
        //txtquotno1.Value = stPro.ProjectNumber;
        // txtdate1

        // Telerik.Reporting.TextBox textBox984 = rpt.Items.Find("textBox984", true)[0] as Telerik.Reporting.TextBox;
        txtdate.Value = DateTime.Now.AddHours(14).ToShortDateString();
        
        decimal TotalQuotePrice=0;
        decimal Rebate = 0;
        decimal SysPrice = 0;
        decimal TotalPrice = 0;
        decimal baltopay = 0;
        decimal depostReq = 0;
        decimal SolarViCRebate = 0;
        decimal SolarVicLoanDisc = 0;

        string stcvalue = stPro.STCValue != "" ? Convert.ToDecimal(stPro.STCValue).ToString("F") : "";
        textBox1019.Value = "No of STC: " + stPro.STCNumber + " X STC Price: $" + stcvalue; 

        if (dt1 != null && dt1.Rows.Count > 0)
        {
            
            if (dt1.Rows[0]["meterupgrade"].ToString() != null && dt1.Rows[0]["meterupgrade"].ToString() != "")
            {
                txtswu.Value = dt1.Rows[0]["meterupgrade"].ToString();
            }
            else
            {
                txtswu.Value = "";
            }

            if (dt1.Rows[0]["MeterPhase"].ToString() != null && dt1.Rows[0]["MeterPhase"].ToString() != "")
            {
                txtmeterphase.Value = dt1.Rows[0]["MeterPhase"].ToString();
            }
            else
            {
                txtmeterphase.Value = "";
            }
            if (dt1.Rows[0]["NMINumber"].ToString() != null && dt1.Rows[0]["NMINumber"].ToString() != "")
            {
                txtnmino.Value = dt1.Rows[0]["NMINumber"].ToString();
            }
            else
            {
                txtnmino.Value = "";
            }

            if (dt1.Rows[0]["ProjectNotes"].ToString() != null && dt1.Rows[0]["ProjectNotes"].ToString() != "")
            {
                txtprojectnotes.Value = dt1.Rows[0]["ProjectNotes"].ToString();
            }
            else
            {
                txtprojectnotes.Value = "";
            }

            if (dt1.Rows[0]["ElecRetailer"].ToString() != null && dt1.Rows[0]["ElecRetailer"].ToString() != "")
            {
                txtenerret.Value = dt1.Rows[0]["ElecRetailer"].ToString();
            }
            else
            {
                txtenerret.Value = "";
            }
            if (dt1.Rows[0]["ElecDistributor"].ToString() != null && dt1.Rows[0]["ElecDistributor"].ToString() != "")
            {
                txtenerdist.Value = dt1.Rows[0]["ElecDistributor"].ToString();
            }
            else
            {
                txtenerdist.Value = "";
            }
            if (dt1.Rows[0]["RoofType"].ToString() != null && dt1.Rows[0]["RoofType"].ToString() != "")
            {
                txtrooftype.Value = dt1.Rows[0]["RoofType"].ToString();
            }
            else
            {
                txtrooftype.Value = "";
            }
            if (dt1.Rows[0]["HouseType"].ToString() != null && dt1.Rows[0]["HouseType"].ToString() != "")
            {
                txthousetype.Value = dt1.Rows[0]["HouseType"].ToString();
            }
            else
            {
                txthousetype.Value = "";

            }
            if (dt1.Rows[0]["solarvicrebate"].ToString() != null && dt1.Rows[0]["solarvicrebate"].ToString() != "")
            {
                SolarViCRebate = decimal.Parse(dt1.Rows[0]["solarvicrebate"].ToString());
                decimal SolarViCRebate1 = Math.Round(SolarViCRebate, 2);
                txtsolarvicrebate.Value= "$ "+"-" +Convert.ToString(SolarViCRebate1);
            }
            else
            {
                txtsolarvicrebate.Value = "$ ";
                //txtsolarvicrebate.Value = "$ " + " -" + "0.00";
            }
            if (dt1.Rows[0]["solarvicloandisc"].ToString() != null && dt1.Rows[0]["solarvicloandisc"].ToString() != "")
            {
                SolarVicLoanDisc = decimal.Parse(dt1.Rows[0]["solarvicloandisc"].ToString());
                decimal SolarVicLoanDisc1 = Math.Round(SolarVicLoanDisc, 2);
                txtvictorialoan.Value = "$ " + " -" + Convert.ToString(SolarVicLoanDisc1);
            }
            else
            {
                txtvictorialoan.Value = "$ ";
                //txtvictorialoan.Value = "$ " + " -" + "0.00";
            }
            //try
            //{
            //    txtphone.Value = dt1.Rows[0]["ContPhone"].ToString();
            //}
            //catch (Exception ex)
            //{

            //}
            try
            {
                txtcustnm1.Value = dt1.Rows[0]["ContFirst"].ToString()+" "+ dt1.Rows[0]["ContLast"].ToString();
                txtcustnm.Value = dt1.Rows[0]["ContFirst"].ToString() + " " + dt1.Rows[0]["ContLast"].ToString();
                txtCustName.Value = dt1.Rows[0]["ContFirst"].ToString() + " " + dt1.Rows[0]["ContLast"].ToString();
                textBox1085.Value= dt1.Rows[0]["ContFirst"].ToString() + " " + dt1.Rows[0]["ContLast"].ToString();
            }
            catch (Exception ex)
            {
                
            }
            try
            {
                txtaddress.Value = dt1.Rows[0]["InstallationFullAddress"].ToString().Trim();
                txtInstallAddress.Value = dt1.Rows[0]["InstallationFullAddress"].ToString();
            }
            catch (Exception ex)
            {

            }
            try
            {
                if (dt1.Rows[0]["ContMobile"].ToString() != null && dt1.Rows[0]["ContMobile"].ToString() != "")
                {
                    txtphone.Value = dt1.Rows[0]["ContMobile"].ToString();
                    // string[] splittedArray = dt1.Rows[0]["ContPhone"].ToString().Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    //// string str = "";
                    // if (splittedArray.Length > 1)
                    // {
                    //     txtphone.Value = splittedArray[1].ToString();
                    // }
                    // else
                    // {
                    //     txtphone.Value = splittedArray[0].ToString();

                    // }
                    // txtphone.Value = str;
                }
                else
                {
                    txtphone.Value = "";
                }
            }
            catch (Exception ex)
            {

            }
            try
            {
                txtcustemail.Value = dt1.Rows[0]["ContEmail"].ToString();
            }
            catch (Exception ex)
            {

            }
            //table2.DataSource = dt1;
            
            try
            {
                txtsalesnm.Value = dt1.Rows[0]["SalesRepName"].ToString();
            }
            catch (Exception ex)
            {
                
            }
            try
            {
                txtEmailSalesRep.Value = dt1.Rows[0]["SalesRepEmail"].ToString();
            }
            catch (Exception ex)
            {

            }
            try
            {
                string no = dt1.Rows[0]["SalesRepNo"].ToString();
                string[] cn = no.Split(',');
                if (cn.Length > 1)
                {
                    txtsalesph.Value = cn[1].ToString();
                }
                else
                {
                    txtsalesph.Value = cn[0].ToString();

                }
            }
            catch (Exception Ex) 
            {

                //throw;
            }
            try
            {
                string panelnm = dt1.Rows[0]["PanelStockName"].ToString();
                string NumberPanels = dt1.Rows[0]["NumberPanels"].ToString();
                string PanelOutput = dt1.Rows[0]["PanelOutput"].ToString();
                string TExt = NumberPanels + " X " + PanelOutput + " " + "watt " + panelnm;
                txtpanel.Value = TExt;

            }
            catch (Exception ex)
            {//throw;
            }
            //txtpanel.Value = TExt;
            try
            {
                string inverterDetails = stPro.inverterqty +" X "+dt1.Rows[0]["InverterBrand"].ToString()+"("+ dt1.Rows[0]["InverterModel"].ToString() + ")";
                if(dt1.Rows[0]["SecondInverterDetails"].ToString() != "")
                {
                    //inverterDetails += " " + stPro.inverterqty2 + " X " + dt1.Rows[0]["SecondInverterDetails"].ToString();
                    inverterDetails += " " + stPro.inverterqty2 + " X " + dt1.Rows[0]["SecondInverterDetails"].ToString() + " (" + dt1.Rows[0]["SecondInvertermodel"].ToString() + ")";
                }
                txtinv.Value = inverterDetails;
                txtinv.Style.Font.Size = new Telerik.Reporting.Drawing.Unit(10);
                
            }
            catch (Exception ex)
            {
            }
            try
            {
                //txtcapcw.Value = dt1.Rows[0]["SystemCapKw"].ToString();
                //textBox984.Value = dt1.Rows[0]["SystemCapKw"].ToString();
                //textBox47.Value = dt1.Rows[0]["SystemCapKw"].ToString();
                txtsystemspcw.Value = dt1.Rows[0]["SystemCapKw"].ToString() +" KW"+" System";
                textBox47.Value = "YOUR SOLAR PROPOSAL      "+ dt1.Rows[0]["SystemCapKw"].ToString() + " KW";
                //txtsw1.Value ="YOUR SOLAR PROPOSAL       "+dt1.Rows[0]["SystemCapKw"].ToString() + " KW";
                txtsw1.Value =dt1.Rows[0]["SystemCapKw"].ToString() + " KW";
                txtsw2.Value =dt1.Rows[0]["SystemCapKw"].ToString() + " KW";
            }
            catch (Exception ex)
            {

            }
            if (stPro.VarTravelTime != null && stPro.VarTravelTime != "")
            {
                decimal TravelTime = decimal.Parse(stPro.VarTravelTime);
                decimal TravelTime1 = Math.Round(TravelTime, 2);
                //txtttlprice.Value = "$ " + TravelTime1;
               // txtinstcharge.Value = "$ " + TravelTime1;
            }
            else
            {
                //txtinstcharge.Value = "$ " + "0.00";
            }

        }
        decimal reb =0;
        if (stPro.RECRebate != null && stPro.RECRebate != "")
        {
            Rebate = decimal.Parse(stPro.RECRebate);
             reb = Math.Round(Rebate, 2);

         }
        if (stPro.TotalQuotePrice != null && stPro.TotalQuotePrice != "")
        {
           TotalQuotePrice=decimal.Parse(stPro.TotalQuotePrice);
            decimal TotalQuotePrice1 = Math.Round(TotalQuotePrice, 2);
            //txtttlprice1.Value ="$ " + Convert.ToString(TotalQuotePrice1);
            //txtttlprice.Value = "$ " + TotalQuotePrice1;
            decimal price = reb + TotalQuotePrice1;
            decimal price1 = Math.Round(price, 2);
           // txtttlprice2.Value = "$ " + price1;
        }
        else
        {
           // txtttlprice.Value = "$ " +"0.00";
        }

        //if (stPro.AdditionalCharges != null && stPro.AdditionalCharges != "")
        //{
        //    textBox137.Value = stPro.AdditionalCharges;

        //}
        //else
        //{
        //    textBox137.Value = "";

        //}
        if (stPro.VarOther != null && stPro.VarOther != "")
        {
            decimal VarOther = decimal.Parse(stPro.VarOther);
            decimal VarOther1 = Math.Round(VarOther, 2);
           // txtother.Value = "$ " + VarOther1;
        }
        else
        {
            //txtother.Value = "$ " + "0.00";
        }
        if (stPro.HotWaterMeter != null && stPro.HotWaterMeter != "")
        {
            decimal HotWaterMeter = decimal.Parse(stPro.HotWaterMeter);
            decimal HotWaterMeter1 = Math.Round(HotWaterMeter, 2);
            txtaddprice.Value = "$ " + HotWaterMeter1;
        }
        else
        {
            txtaddprice.Value = "$ ";
            //txtaddprice.Value = "$ " + "0.00";
        }
        if (stPro.RECRebate != null && stPro.RECRebate != "")
        {
            Rebate = decimal.Parse(stPro.RECRebate);
            decimal RECRebate1 = Math.Round(Rebate, 2);
          //  txtlessince.Value = "$ " + Convert.ToString(RECRebate1);
            txtlessince.Value = "$ " + " -" + Convert.ToString(RECRebate1);
        }
        else
        {
            txtlessince.Value = "$ ";
            //txtlessince.Value = "$ " + " -"+"0.00";
        }
        SysPrice = TotalQuotePrice + Rebate;
        decimal SysPrice1 = Math.Round(SysPrice, 2);
        // txtsysprice.Value = Convert.ToString(SysPrice1);
        txtsysprice.Value = "$ " +Convert.ToString(SysPrice1);
        //TotalPrice = TotalQuotePrice - Rebate;
        //decimal TotalPrice1 = Math.Round(TotalPrice, 2);
        decimal Disc = Rebate + SolarVicLoanDisc + SolarViCRebate;
        TotalPrice = SysPrice - Disc;
        decimal TotalPrice1 = Math.Round(TotalPrice, 2);
        txttotalprice.Value = "$ " + Convert.ToString(TotalPrice1);
        if (stPro.DepositRequired != null && stPro.DepositRequired != "")
        {
            decimal DepositRequired1 = decimal.Parse(stPro.DepositRequired);
            depostReq = decimal.Parse(stPro.DepositRequired);
            decimal DepositRequired = Math.Round(DepositRequired1, 2);
            txtdeposit.Value = "$ " + Convert.ToString(DepositRequired);
        }
        else
        {
            //txtdeposit.Value = "$ " + "0.00";
            txtdeposit.Value = "$ ";
        }
      

        baltopay = TotalPrice1 - depostReq;
        decimal baltopay1 = Math.Round(baltopay, 2);
        txtbalance.Value = "$ " + Convert.ToString(baltopay1);

        Telerik.Reporting.TextBox txtInverterLocation = rpt.Items.Find("txtInverterLocation", true)[0] as Telerik.Reporting.TextBox;
        txtInverterLocation.Value = stPro.InvLocation;

        int existinSignLog = Clstbl_SignatureLog.tbl_SignatureLogExistByProjectID_QDocNo(ProjectID, QuoteID);

        Telerik.Reporting.PictureBox pictureBox4 = rpt.Items.Find("pictureBox13", true)[0] as Telerik.Reporting.PictureBox;
        //Telerik.Reporting.PictureBox pictureBox3 = rpt.Items.Find("pictureBox3", true)[0] as Telerik.Reporting.PictureBox;
        if (stPro.nearmapdoc != null && stPro.nearmapdoc != "")
        {
            Page p = new Page();
            string Url = pdfURL + "NearMap/" + stPro.nearmapdoc;
            pic1.Value = Url;
        }
        else
        {
            pic1.Value = null;
        }
        if (stPro.nearmapdoc1 != null && stPro.nearmapdoc1 != "")
        {
            Page p = new Page();
            string Url = pdfURL + "NearMap/" + stPro.nearmapdoc1;
            pic2.Value = Url;
        }
        else
        {
            pic2.Value = null;
        }
        if (stPro.nearmapdoc2 != null && stPro.nearmapdoc2 != "")
        {
            Page p = new Page();
            string Url = pdfURL + "NearMap/" + stPro.nearmapdoc2;
            pic3.Value = Url;
        }
        else
        {
            pic3.Value = null;
        }

        if (existinSignLog == 1)
        {

            Sttbl_SignatureLog stsign = Clstbl_SignatureLog.tbl_SignatureLog_SelectByProjectID_QDocNo(ProjectID, QuoteID);

            String ImageAdd = stsign.Image_Address;
            String Token = stsign.Token;

            Sttbl_TokenData stToken = Clstbl_TokenData.tbl_TokenData_SelectByToken_ProjectID(Token, ProjectID);

            //Telerik.Reporting.TextBox txtcustdate = rpt.Items.Find("textBox102", true)[0] as Telerik.Reporting.TextBox;
            Page p = new Page();
            String Virtualpath = stsign.Image_Address;
            int index = Virtualpath.IndexOf("userfiles");
            Virtualpath = Virtualpath.Substring(index);
            string img1 = p.Server.MapPath("~/" + Virtualpath);
            // pictureBox3.Value = img1;
            pictureBox4.Value = img1;


            Clstbl_TokenData.tbl_TokenData_Update_SignFlag(Token, ProjectID, "True");


            string nQoute = stToken.QDocNo;
            SavePDF(rpt, nQoute);

            DataTable dt2 = ClstblProjects.tblProjectQuotes_SelectByProjectQuoteDoc(stToken.QDocNo);
            string ProjectQuoteDoc = dt2.Rows[0]["ProjectQuoteDoc"].ToString();
            string QuoteDoc = nQoute + "SolarMinerQuote.pdf";
            //string QuoteDoc = nQoute + "SolarMinorQuotePdfNew.pdf";
            //string QuoteDoc = nQoute + "Qoute.pdf";
            ClstblProjects.tblProjectQuotes_UpdateQuoteDoc(ProjectQuoteDoc, QuoteDoc);

            //string SQFileNmae = ProjectID + "_" + nQoute + "_" + Token + "SolarMinorQuotePdfNew.pdf";
            //string SQFileNmae = ProjectID + "_" + nQoute + "_" + Token + " Qoute.pdf";
            string SQFileNmae = ProjectID + "_" + nQoute + "_" + Token + "SolarMinerQuote.pdf";
            SavePDFForSQ(rpt, SQFileNmae);
        }

        //if (existinSignLog == 1)
        //{

        //    Sttbl_SignatureLog stsign = Clstbl_SignatureLog.tbl_SignatureLog_SelectByProjectID_QDocNo(ProjectID, QuoteID);

        //    String ImageAdd = stsign.Image_Address;
        //    String Token = stsign.Token;

        //    Sttbl_TokenData stToken = Clstbl_TokenData.tbl_TokenData_SelectByToken_ProjectID(Token, ProjectID);

        //    //Telerik.Reporting.TextBox txtcustdate = rpt.Items.Find("textBox102", true)[0] as Telerik.Reporting.TextBox;
        //    Page p = new Page();
        //    String Virtualpath = stsign.Image_Address;
        //    int index = Virtualpath.IndexOf("userfiles");
        //    Virtualpath = Virtualpath.Substring(index);
        //    string img1 = p.Server.MapPath("~/" + Virtualpath);
        //    // pictureBox3.Value = img1;
        //    pictureBox4.Value = img1;


        //    Clstbl_TokenData.tbl_TokenData_Update_SignFlag(Token, ProjectID, "True");


        //    string nQoute = stToken.QDocNo;
        //    SavePDF(rpt, nQoute);

        //    DataTable dt2 = ClstblProjects.tblProjectQuotes_SelectByProjectQuoteDoc(stToken.QDocNo);
        //    string ProjectQuoteDoc = dt2.Rows[0]["ProjectQuoteDoc"].ToString();
        //    string QuoteDoc = nQoute + "SolarMinerQuote.pdf";
        //    //string QuoteDoc = nQoute + "SolarMinorQuotePdfNew.pdf";
        //    //string QuoteDoc = nQoute + "Qoute.pdf";
        //    ClstblProjects.tblProjectQuotes_UpdateQuoteDoc(ProjectQuoteDoc, QuoteDoc);

        //    //string SQFileNmae = ProjectID + "_" + nQoute + "_" + Token + "SolarMinorQuotePdfNew.pdf";
        //    //string SQFileNmae = ProjectID + "_" + nQoute + "_" + Token + " Qoute.pdf";
        //    string SQFileNmae = ProjectID + "_" + nQoute + "_" + Token + "SolarMinerQuote.pdf";
        //    SavePDFForSQ(rpt, SQFileNmae);
        //}
        else
        {
            //pictureBox3.Value = null;
            SavePDF(rpt, QuoteID);
            //ExportPdf(rpt);
        }



        //ExportPdf(rpt);
    }
    public static void SavePDF(Telerik.Reporting.Report rpt, string QuoteID)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        Random rnd = new Random();

        string fileName = QuoteID + result.DocumentName + "." + result.Extension;

        string ExcelFilename = fileName;

        string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\quotedoc\\", ExcelFilename);

        //FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
        //fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
        //fs.Close();
        //SiteConfiguration.UploadPDFFile("quotedoc", fileName);
        //SiteConfiguration.deleteimage(fileName, "quotedoc");

        //var convertApi = new ConvertApi("<YOUR SECRET HERE>");
        //convertApi.ConvertAsync("pdf", "compress",
        //    new ConvertApiFileParam(@"C:\path\to\my_file.pdf")
        //).Result.SaveFiles(@"C:\converted-files\");

        PdfDocument doc = new PdfDocument(result.DocumentBytes);

        doc.FileInfo.IncrementalUpdate = false;
        // doc.CompressionLevel = PdfCompressionLevel.BestSpeed;
        //foreach (PdfPageBase page in doc.Pages)

        //{

        //    //Extracts images from page
        //    Image[] images = page.ExtractImages();

        //    if (images != null && images.Length > 0)
        //    {
        //        for (int j = 0; j < images.Length; j++)

        //        {
        //            Image image = images[j];

        //            PdfBitmap bp = new PdfBitmap(image);
        //            bp.Quality = 20;
        //            page.ReplaceImage(j, bp);
        //        }
        //    }
        //}

        foreach (PdfPageBase page in doc.Pages)
        {
            if (page != null)
            {
                if (page.ImagesInfo != null)
                {
                    foreach (PdfImageInfo info in page.ImagesInfo)
                    {
                        page.TryCompressImage(info.Index);
                    }
                }
            }
        }
        doc.SaveToFile(fullPath);
        SiteConfiguration.UploadPDFFile("quotedoc", fileName);
        SiteConfiguration.deleteimage(fileName, "quotedoc");

    }
    public static void generate_quatationSolarMiner(string ProjectID, string QuoteID)
    {

        Telerik.Reporting.Report rpt = new EurosolarReporting.SolarMinerQuote();
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
        DataTable dtQuote = ClstblProjects.tblProjectQuotes_SelectByProjectID(ProjectID);
        string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
        dt = Clscustomerlead.tblCustomers_SelectByCustomerID_telerikdemo(stPro.CustomerID);
        dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table tblCust = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table tblAdd = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table tblSystDetail = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table tblttldisc = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table tblpanelPrice = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox txtemail = rpt.Items.Find("txtemail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsystemcap = rpt.Items.Find("txtsystemcap", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.Table table1 = rpt.Items.Find("tbl1", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox txtcontacttwo = rpt.Items.Find("txtcontacttwo", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtcontactno = rpt.Items.Find("txtcontactno", true)[0] as Telerik.Reporting.TextBox;

        
        if (dt1 != null && dt1.Rows.Count > 0)
        {
            string no = dt1.Rows[0]["SalesRepNo"].ToString();
            if (no != null && no != "")
            {
                string[] cn = no.Split(',');
                if (cn.Length > 1)
                {
                    txtcontactno.Value = cn[1].ToString();
                }
                else
                {
                    txtcontactno.Value = cn[0].ToString();

                }

            }
        }
        table1.DataSource = dt1;
        tblCust.DataSource = dt1;
        tblAdd.DataSource = dt1;
        tblSystDetail.DataSource = dt1;
        tblttldisc.DataSource = dt1;
        tblpanelPrice.DataSource = dt1;
        table6.DataSource = dt1;
        //table7.DataSource = dt1;
        Telerik.Reporting.TextBox txtsignDate = rpt.Items.Find("txtsignDate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtaddbelw = rpt.Items.Find("textBox101", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtCustName = rpt.Items.Find("txtCustName", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtcustdatebelow = rpt.Items.Find("textBox102", true)[0] as Telerik.Reporting.TextBox;
        txtcustdatebelow.Value = DateTime.Now.ToShortDateString();
        Telerik.Reporting.TextBox quotno = rpt.Items.Find("txtquoteno", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsalescode = rpt.Items.Find("txtsalescode", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdate = rpt.Items.Find("txtdate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtfname = rpt.Items.Find("txtfname", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtlname = rpt.Items.Find("txtlaname", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtcontact = rpt.Items.Find("txtcontact", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtpackage = rpt.Items.Find("txtpackage", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtadditional = rpt.Items.Find("textBox45", true)[0] as Telerik.Reporting.TextBox;
        quotno.Value = stPro.ProjectNumber;
        Telerik.Reporting.TextBox txtdistributor = rpt.Items.Find("txtdistributor", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtenerretail = rpt.Items.Find("txtenerretail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdiscount = rpt.Items.Find("txtdiscount", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdepoamt = rpt.Items.Find("txtdepoamt", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtpvPanel = rpt.Items.Find("txtpvPanel", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtwatt = rpt.Items.Find("txtpanelwatt", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstcince = rpt.Items.Find("txtstcince", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsubttl = rpt.Items.Find("txtsubttl",
            true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtfinalprice = rpt.Items.Find("txtfinalprice",
            true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmtrUpgrade = rpt.Items.Find("txtmtrUpgrade", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtinvdet = rpt.Items.Find("txtinvdet", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox29 = rpt.Items.Find("textBox29", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox116 = rpt.Items.Find("textBox116",
          true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtvicrebate = rpt.Items.Find("txtvicrebate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtvicloanDisc = rpt.Items.Find("textBox20", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtHouseStory = rpt.Items.Find("textBox183", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtroofsurcharge = rpt.Items.Find("txtroofsurcharge", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtothercost = rpt.Items.Find("txtothercost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtothernote = rpt.Items.Find("txtothernote", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtpanelwatt = rpt.Items.Find("txtpanelwatt", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtinverter1 = rpt.Items.Find("txtinverter1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtinverterkw1 = rpt.Items.Find("txtinverterkw1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsubttl1 = rpt.Items.Find("txtsubttl1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtgst = rpt.Items.Find("txtgst", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtspediscdisp = rpt.Items.Find("textBox243", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txttiltframe = rpt.Items.Find("textBox21", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtcnm = rpt.Items.Find("txtcnm", true)[0] as Telerik.Reporting.TextBox;
       
        //if (stPro.Asbestoss != null && stPro.Asbestoss != "")
        //{
        //    decimal txtvicrebate1 = decimal.Parse(stPro.Asbestoss);
        //    decimal pac1 = Math.Round(txtvicrebate1, 2);
        //    txttiltframe.Value = "$ " + pac1;
        //}
        //else
        //{
        //    txttiltframe.Value = "$ " + "0.00";
        //}
        if (dt1.Rows[0]["RoofType"].ToString() != null && dt1.Rows[0]["RoofType"].ToString() != "")
        {
            textBox29.Value = dt1.Rows[0]["RoofType"].ToString();
        }
        if (dt1.Rows[0]["HouseType"].ToString() != null && dt1.Rows[0]["HouseType"].ToString() != "")
        {
            txtHouseStory.Value = dt1.Rows[0]["HouseType"].ToString();
        }


        if (dt1.Rows[0]["SystemCapKw"].ToString() != null && dt1.Rows[0]["SystemCapKw"].ToString() != "")
        {
            txtsystemcap.Value = dt1.Rows[0]["SystemCapKw"].ToString() + " KW";
        }
        else
        {
            txtsystemcap.Value = dt1.Rows[0]["SystemCapKw"].ToString() + " KW";
        }
        if (stPro.DepositRequired != null && stPro.DepositRequired != "")
        {
            decimal txtvicrebate1 = decimal.Parse(stPro.DepositRequired);
            decimal pac1 = Math.Round(txtvicrebate1, 2);
            txtdepoamt.Value = "$ " + pac1;
        }
        else
        {
            txtdepoamt.Value = "$ " + "0.00";
        }
        if (stPro.VicRebate1 != null && stPro.VicRebate1 != "")
        {
            decimal txtvicrebate1 = decimal.Parse(stPro.VicRebate1);
            decimal pac1 = Math.Round(txtvicrebate1, 2);
            txtvicrebate.Value = "$ " + Convert.ToString(pac1);
        }
        else
        {
            txtvicrebate.Value = "$ " + "0.00";
        }
        decimal vcloandisc = 0;
        if (stPro.VicLoanDisc != null && stPro.VicLoanDisc != "")
        {
            decimal txtvicrebate1 = decimal.Parse(stPro.VicLoanDisc);
            decimal pac1 = Math.Round(txtvicrebate1, 2);
            txtvicloanDisc.Value = "$ " + Convert.ToString(pac1);
            vcloandisc = pac1;
        }
        else
        {
            txtvicloanDisc.Value = "$ " + "0.00";
        }
        if (dt1.Columns.Contains("MeterNumber1"))
        {
            textBox116.Value = dt1.Rows[0]["MeterNumber1"].ToString();
        }
        if (dt1.Columns.Contains("ProjectNotes"))
        {
            txtothernote.Value = dt1.Rows[0]["ProjectNotes"].ToString();
        }
        if (dt1.Columns.Contains("CustLast"))
        {
            //txtlname.Value = dt1.Rows[0]["CustLast"].ToString();
        }
        if (dt1.Columns.Contains("ContEmail"))
        {
            txtemail.Value = dt1.Rows[0]["ContEmail"].ToString();
        }
        if (dt1.Columns.Contains("CustPhone"))
        {
            txtcontacttwo.Value = dt1.Rows[0]["CustPhone"].ToString();

        }
        if (dt1.Columns.Contains("ContMobile"))
        {
            txtcontact.Value = dt1.Rows[0]["ContMobile"].ToString();
        }

        //stPro.VarMeterUG
        if (stPro.VarMeterUG != null && stPro.VarMeterUG != "")
        {
            decimal rc = decimal.Parse(stPro.VarMeterUG);
            decimal pac1 = Math.Round(rc, 2);
            txtmtrUpgrade.Value = "$ " + pac1;
        }
        else
        {
            txtmtrUpgrade.Value = "$ " + "0.00";
        }
        #region RoofSurchargeCalculation
        string roofsurcharge = SiteConfiguration.ChangeCurrency_Val(
          (Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle)).ToString());
        if (roofsurcharge != null && roofsurcharge != "")
        {
            decimal rc = decimal.Parse(roofsurcharge);
            decimal pac1 = Math.Round(rc, 2);
            txtroofsurcharge.Value = "$ " + pac1;
        }
        else
        { txtroofsurcharge.Value = "$ " + "0.00"; }
        #endregion RoofSurchargeCalculation
        //if (dt1.Rows[0]["ServiceValue"].ToString() != null && dt1.Rows[0]["ServiceValue"].ToString() != "")
        if (dt1.Rows[0]["TotalQuotePrice"].ToString() != null && dt1.Rows[0]["TotalQuotePrice"].ToString() != "")
        {
            decimal TotalQuotePrice = decimal.Parse(dt1.Rows[0]["TotalQuotePrice"].ToString());

            #region GSTcalculation
            try
            {
                decimal gst = Convert.ToDecimal(TotalQuotePrice) / 11;
                decimal gst1 = Math.Round(gst, 2);
                txtgst.Value = "$ " + Convert.ToString(gst1);
            }
            catch (Exception Ex)
            {

                //throw;
            }
            #endregion GSTcalculation
            #region PackageCalculation
            decimal basicsystemcost = 0;
            if (dt1.Rows[0]["ServiceValue"].ToString() != null && dt1.Rows[0]["ServiceValue"].ToString() != "")
            {
                basicsystemcost = decimal.Parse(dt1.Rows[0]["ServiceValue"].ToString());
            }
            else
            {
                basicsystemcost = 0;
            }

            decimal pac = 0;
            if (dt1.Rows[0]["RECRebate"].ToString() != null && dt1.Rows[0]["RECRebate"].ToString() != "")
            {
                pac = basicsystemcost + decimal.Parse(dt1.Rows[0]["RECRebate"].ToString());
            }
            decimal pac1 = Math.Round(pac, 2);
            txtpackage.Value = "$ " + pac1.ToString();
            #endregion PackageCalculation

            #region AdditionalCalculation
            string additional = SiteConfiguration.ChangeCurrency_Val(
              (Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarOther) + Convert.ToDecimal(stPro.VarMeterUG)).ToString());
            if (additional != null && additional != "")
            {
                txtadditional.Value = "$ " + additional;
            }
            else
            {
                txtadditional.Value = "$ " + "0.00";
            }

            #endregion AdditionalCalculation
            #region SubTotalCalculation
            if (additional != null && additional != "")
            {
                decimal subttl1 = decimal.Parse(additional) + pac;
                decimal subttl2 = Math.Round(subttl1, 2);
                txtsubttl1.Value = "$ " + Convert.ToString(subttl2);
            }
            else
            {
                txtsubttl1.Value = "$ " + "0.00";
            }
            #endregion SubTotalCalculation

            #region GetOtherCostVal
            if (stPro.VarOther != null && stPro.VarOther != "")
            {
                decimal subttl1 = decimal.Parse(stPro.VarOther);
                decimal subttl2 = Math.Round(subttl1, 2);
                txtothercost.Value = "$ " + Convert.ToString(subttl2);
            }
            else
            {
                txtothercost.Value = "$ " + "0.0";
            }
            #endregion GetOtherCostVal
            #region DiscountCalculation
            string SpeDisc = "";
            decimal spdisc1 = 0;
            if (stPro.SpecialDiscount != null)
            {
                SpeDisc = stPro.SpecialDiscount;
                txtspediscdisp.Value = "$ " + Math.Round((decimal.Parse(stPro.SpecialDiscount)), 2);
                spdisc1 = decimal.Parse(stPro.SpecialDiscount);
            }
            else
            {
                SpeDisc = "0.00";
                txtspediscdisp.Value = "0.00";
            }
            try
            {
                decimal VicRebate1 = 0;
                decimal VicLoanDisc = 0;
                decimal ViCLoan = 0;
                decimal RECRebate = 0;
                decimal spedisc = 0;
                decimal recRebate = 0;
                if (stPro.VicRebate1 != null && stPro.VicRebate1 != "")
                {
                    VicRebate1 = decimal.Parse(stPro.VicRebate1);
                }
                if (stPro.VicLoanDisc != null && stPro.VicLoanDisc != "")
                {
                    VicLoanDisc = decimal.Parse(stPro.VicLoanDisc);
                }
                if (stPro.RECRebate != null && stPro.RECRebate != "")
                {
                    RECRebate = decimal.Parse(stPro.RECRebate);
                }
                if (dt1.Rows[0]["RECRebate"].ToString() != null && dt1.Rows[0]["RECRebate"].ToString() != "")
                {
                    recRebate = decimal.Parse(dt1.Rows[0]["RECRebate"].ToString());
                }
                // if (stPro.VicRebate1 != null && stPro.VicLoanDisc != null && stPro.VicLoanDisc != "" && stPro.ViCLoan != "" && dt1.Rows[0]["RECRebate"].ToString() != null && dt1.Rows[0]["RECRebate"].ToString() != "" && SpeDisc != null && SpeDisc != "")
                //if()
                //{
                // decimal spdis = decimal.Parse(SpeDisc);
                decimal disc = 0;
                //decimal vicReb = decimal.Parse(stPro.VicRebate1);
                //decimal vicRebLoan = decimal.Parse(stPro.VicLoanDisc);
                //decimal recRebate = decimal.Parse(dt1.Rows[0]["RECRebate"].ToString());
                disc = recRebate + VicRebate1 + VicLoanDisc + spdisc1;
                decimal disc1 = Math.Round(disc, 2);
                txtdiscount.Value = "$ " + disc1.ToString();
            }
            catch (Exception ex)
            {

            }
            //}
            //else
            //{
            //    txtdiscount.Value = "$ " + "0.0";
            //}
            #endregion DiscountCalculation

            #region FinalPriceCalculation
            if (txtsubttl1.Value != null && txtsubttl1.Value != "" && txtdiscount.Value != null && txtdiscount.Value != "")
            {
                string subttl = txtsubttl1.Value.ToString().Remove(0, 1);
                string disc2 = txtdiscount.Value.ToString().Remove(0, 1);
                //subttl.Replace("$"," ");
                //disc.Replace("$"," ");
                decimal totalp = decimal.Parse(subttl) - decimal.Parse(disc2);
                decimal totalp1 = Math.Round(totalp, 2);
                string totalPrice = "$ " + totalp1.ToString();
                txtfinalprice.Value = Convert.ToString(totalPrice);
            }
            else
            {
                txtfinalprice.Value = "$ " + "0.0";
            }
            #endregion FinalPriceCalculation
        }

        try
        {
            if (dt1.Rows[0]["RECRebate"].ToString() != null && dt1.Rows[0]["RECRebate"].ToString() != "")
            {
                decimal since = decimal.Parse(dt1.Rows[0]["RECRebate"].ToString());
                decimal since1 = Math.Round(since, 2);
                txtstcince.Value = "$ " + since1;
            }
            else
            {
                txtstcince.Value = "$ " + "0.0";
            }
        }
        catch (Exception Ex)
        {

        }

        try
        {
            txtwatt.Value = dt1.Rows[0]["PanelBrand"].ToString() + "  " + "(" + dt1.Rows[0]["PanelModel"].ToString() + ")";
            //txtdate.Value = dt1.Rows[0]["ProjectOpened"].ToString();
        }
        catch (Exception Ex)
        {
        }
        try
        {
            if (dt1.Rows[0]["NumberPanels"].ToString() != null && dt1.Rows[0]["NumberPanels"].ToString() != "")
            {
                txtpvPanel.Value = dt1.Rows[0]["NumberPanels"].ToString();
                txtpanelwatt.Value = dt1.Rows[0]["PanelOutput"].ToString();
            }
            //txtdate.Value = dt1.Rows[0]["ProjectOpened"].ToString();
        }
        catch (Exception Ex)
        {
        }
        try
        {
            txtdate.Value= (Convert.ToDateTime(DateTime.Today).ToString(("dd-MMM-yyyy")));
            // txtdate.Value = (Convert.ToDateTime(dt1.Rows[0]["ProjectOpened"])).ToString(("dd-MMM-yyyy"));
            //txtdate.Value = dt1.Rows[0]["ProjectOpened"].ToString();
        }
        catch (Exception Ex)
        {
        }
        try
        {
            txtinverter1.Value = stPro.InverterOutput;
        }
        catch (Exception ex)
        { //throw;
        }
        string inverterdetail = string.Empty;
        // stPro.InverterDetailsName
        if (stPro.InverterDetailsID != "")
        {
            inverterdetail = stPro.inverterqty;
            txtinverterkw1.Value = stPro.InverterOutput;
        }
        if (stPro.SecondInverterDetailsID != "0")
        {
            inverterdetail = stPro.inverterqty;
            txtinverterkw1.Value = stPro.InverterOutput;
        }
        if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "0")
        {
            inverterdetail = stPro.inverterqty;
            txtinverterkw1.Value = stPro.InverterOutput;
        }
        if ((stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID == "0"))
        {
            txtinvdet.Value = dt1.Rows[0]["InverterBrand"].ToString();

            txtinverterkw1.Value = stPro.InverterOutput;
        }
        try
        {
            txtinvdet.Value = stPro.InverterBrand + "  " + "(" + stPro.InverterModel + ")";
            txtinverter1.Value = stPro.inverterqty;

            //txtinvdet.Value = inverterdetail.ToString();
            txtinverterkw1.Value = stPro.InverterOutput;

        }
        catch (Exception ex)
        {

        }
        try
        {
            txtsignDate.Value = (Convert.ToDateTime(DateTime.Today).ToString(("dd-MMM-yyyy")));
        }
        catch (Exception es)
        {

            
        }
        try
        {
            txtenerretail.Value = dt1.Rows[0]["ElecRetailer"].ToString();
        }
        catch (Exception ex)
        {
        }
        try
        {
            txtdistributor.Value = dt1.Rows[0]["ElecDistributor"].ToString();
        }
        catch (Exception ex)
        {
        }
        try
        {
            txtsalescode.Value = dt1.Rows[0]["SalesRepName"].ToString();
        }
        catch { }
        try
        {
            txtfname.Value = dt1.Rows[0]["ContFirst"].ToString();
            txtcnm.Value = dt1.Rows[0]["Customer"].ToString();

        }
        catch { }
        try
        {
            //txtlname.Value = dt1.Rows[0]["ContLast"].ToString();
        }
        catch (Exception Ex)
        {
        }

        //if (dtQuote != null && dtQuote.Rows.Count > 0)
        //{
        //    for (int i = 0; i < dtQuote.Rows.Count; i++)
        //    {
        //        DataRow[] drFind = dtQuote.Select("ProjectQuoteDoc"+ QuoteID);
        //        if (drFind.Length > 0)
        //        {
        //            string ProjectQuoteDoc = dtQuote.Rows[i]["ProjectQuoteDoc"].ToString();
        //            int Signed = Clstbl_SignatureLog.tbl_SignatureLogExistByProjectID_QDocNo(ProjectID, ProjectQuoteDoc);
        //            if (Signed == 1)
        //            {

        //                string Date = (Convert.ToDateTime(dtQuote.Rows[i]["ProjectQuoteDate"])).ToString(("dd-MMM-yyyy"));
        //                txtsignDate.Value = Date;
        //            }
        //        }
        //    }
        //}
        int existinSignLog = Clstbl_SignatureLog.tbl_SignatureLogExistByProjectID_QDocNo(ProjectID, QuoteID);

        Telerik.Reporting.PictureBox pictureBox4 = rpt.Items.Find("pictureBox4", true)[0] as Telerik.Reporting.PictureBox;
        Telerik.Reporting.PictureBox pictureBox3 = rpt.Items.Find("pictureBox3", true)[0] as Telerik.Reporting.PictureBox;
        if (existinSignLog == 1)
        {

            Sttbl_SignatureLog stsign = Clstbl_SignatureLog.tbl_SignatureLog_SelectByProjectID_QDocNo(ProjectID, QuoteID);
            if (stsign.Signed_Date != null && stsign.Signed_Date != "")
            {
                string Date = (Convert.ToDateTime(stsign.Signed_Date).ToString(("dd-MMM-yyyy")));
                txtsignDate.Value = Date;
            }
            else
            {
                txtsignDate.Value = "";
            }
            String ImageAdd = stsign.Image_Address;
            String Token = stsign.Token;

            Sttbl_TokenData stToken = Clstbl_TokenData.tbl_TokenData_SelectByToken_ProjectID(Token, ProjectID);

            Telerik.Reporting.TextBox txtcustdate = rpt.Items.Find("textBox102", true)[0] as Telerik.Reporting.TextBox;
            Page p = new Page();
            String Virtualpath = stsign.Image_Address;
            int index = Virtualpath.IndexOf("userfiles");
            Virtualpath = Virtualpath.Substring(index);
            string img1 = p.Server.MapPath("~/" + Virtualpath);
            pictureBox3.Value = img1;
            pictureBox4.Value = img1;

            try
            {
                txtcustdate.Value = (Convert.ToDateTime(stsign.Signed_Date)).ToString(("dd-MMM-yyyy"));
            }
            catch { }
            Clstbl_TokenData.tbl_TokenData_Update_SignFlag(Token, ProjectID, "True");


            string nQoute = stToken.QDocNo;
            SavePDF(rpt, nQoute);

            DataTable dt2 = ClstblProjects.tblProjectQuotes_SelectByProjectQuoteDoc(stToken.QDocNo);
            string ProjectQuoteDoc = dt2.Rows[0]["ProjectQuoteDoc"].ToString();
            string QuoteDoc = nQoute + "SolarMinerQuote.pdf";
            ClstblProjects.tblProjectQuotes_UpdateQuoteDoc(ProjectQuoteDoc, QuoteDoc);

            string SQFileNmae = ProjectID + "_" + nQoute + "_" + Token + "SolarMinerQuote.pdf";
            SavePDFForSQ(rpt, SQFileNmae);
        }
        else
        {
            pictureBox3.Value = null;
            SavePDF(rpt, QuoteID);
            //ExportPdf(rpt);

        }


    }
    #endregion
    public static void generate_STCformSolarMiner(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(stPro.ContactID);
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(stPro.EmployeeID);
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);



        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.SolarMinerStc();


        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table1 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table2 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table10 = rpt.Items.Find("table10", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table11 = rpt.Items.Find("table11", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table12 = rpt.Items.Find("table15", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table13 = rpt.Items.Find("table20", true)[0] as Telerik.Reporting.Table;

        Telerik.Reporting.TextBox textBox124 = rpt.Items.Find("textBox124", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox128 = rpt.Items.Find("textBox128", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox125 = rpt.Items.Find("textBox125", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox126 = rpt.Items.Find("textBox126", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox127 = rpt.Items.Find("textBox127", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox16 = rpt.Items.Find("textBox16", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox17 = rpt.Items.Find("textBox17", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox21 = rpt.Items.Find("textBox21", true)[0] as Telerik.Reporting.TextBox;


        //table19.DataSource = dt;
        try
        {
            // textBox124.Value = dt.Rows[0]["Customer"].ToString();
        }
        catch { }

        // Telerik.Reporting.TextBox textBox45 = rpt.Items.Find("textBox45", true)[0] as Telerik.Reporting.TextBox;
        try
        {
            //textBox25.Value = string.Format("{0: dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));

        }
        catch { }


        if (st.RequiredCompliancePaperwork == "true")
        {
            textBox128.Value = "YES";

        }
        else
        {
            textBox128.Value = "NO";
        }
        if (st.ReceivedCredits == "True")
        {
            textBox125.Value = "YES";
        }
        else
        {
            textBox125.Value = "NO";
        }
        if (st.CreditEligible == "True")
        {
            textBox126.Value = "YES";
        }
        else
        {
            textBox126.Value = "NO";
        }

        if (st.MoreThanOneInstall == "True")
        {
            textBox127.Value = "YES";
        }
        else
        {
            textBox127.Value = "NO";
        }



        //Change for New End
        table1.DataSource = dt;
        table2.DataSource = dt;
        table3.DataSource = dt;
        table6.DataSource = dt;
        table7.DataSource = dt;
        table8.DataSource = dt;
        table10.DataSource = dt;
        table11.DataSource = dt;
        table12.DataSource = dt;
        //table13.DataSource = dt;
        //table14.DataSource = dt;
        //table15.DataSource = dt;
        //Change for New


        //testing
        textBox16.Value = dt.Rows[0]["ContFirst"].ToString();
        textBox17.Value = dt.Rows[0]["ContLast"].ToString();
        //textBox97.Value = dt.Rows[0]["StreetAddress"].ToString();
        //textBox94.Value = dt.Rows[0]["StreetCity"].ToString();
        //textBox92.Value = dt.Rows[0]["StreetPostCode"].ToString();
        //textBox91.Value = dt.Rows[0]["InstallerName"].ToString();
        //textBox90.Value = dt.Rows[0]["InstallerName"].ToString();
        //textBox87.Value = dt.Rows[0]["Electricianmolbile"].ToString();
        //textBox84.Value = dt.Rows[0]["ElecLicence"].ToString();
        //try
        //{
        //    textBox82.Value = string.Format("{0:dd}", Convert.ToDateTime(dt.Rows[0]["InstallBookingDate"].ToString()));
        //    textBox81.Value = string.Format("{0:MMM}", Convert.ToDateTime(dt.Rows[0]["InstallBookingDate"].ToString()));
        //    textBox78.Value = string.Format("{0:yyyy}", Convert.ToDateTime(dt.Rows[0]["InstallBookingDate"].ToString()));
        //    //

        //    textBox56.Value = string.Format("{0: dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
        //}
        //catch { }




        //Telerik.Reporting.TextBox ProjectNumber3 = rpt.Items.Find("ProjectNumber3", true)[0] as Telerik.Reporting.TextBox;
        //ProjectNumber3.Value = stPro.ProjectNumber;
        //Telerik.Reporting.TextBox litPanelDetails = rpt.Items.Find("litPanelDetails", true)[0] as Telerik.Reporting.TextBox;
        //litPanelDetails.Value = stPro.PanelDetails;

        //Telerik.Reporting.TextBox lblInverterBrand2 = rpt.Items.Find("lblInverterBrand2", true)[0] as Telerik.Reporting.TextBox;
        //lblInverterBrand2.Value = stPro.InverterBrand;

        //Telerik.Reporting.TextBox lblInverterModel2 = rpt.Items.Find("lblInverterModel2", true)[0] as Telerik.Reporting.TextBox;
        //lblInverterModel2.Value = stPro.InverterModel;

        //Telerik.Reporting.TextBox lblInverterSeries2 = rpt.Items.Find("lblInverterSeries2", true)[0] as Telerik.Reporting.TextBox;
        //lblInverterSeries2.Value = stPro.InverterSeries;



        //Telerik.Reporting.TextBox lblProjectNumber5 = rpt.Items.Find("lblProjectNumber5", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox lblProjectNumber4 = rpt.Items.Find("lblProjectNumber4", true)[0] as Telerik.Reporting.TextBox;
        //lblProjectNumber5.Value = lblProjectNumber5.Value = stPro.ProjectNumber;


        //Telerik.Reporting.TextBox lblInstallerName7 = rpt.Items.Find("lblInstallerName7", true)[0] as Telerik.Reporting.TextBox;
        //lblInstallerName7.Value = stPro.InstallerName;
        //Telerik.Reporting.TextBox lblFinanceOption = rpt.Items.Find("lblFinanceOption", true)[0] as Telerik.Reporting.TextBox;
        //lblFinanceOption.Value = stPro.FinanceWith;


        //Telerik.Reporting.TextBox lblMeterPhase = rpt.Items.Find("lblMeterPhase", true)[0] as Telerik.Reporting.TextBox;
        //if (stPro.MeterPhase == "1")
        //{
        //    lblMeterPhase.Value = "Single";
        //}
        //if (stPro.MeterPhase == "2")
        //{
        //    lblMeterPhase.Value = "Double";
        //}
        //if (stPro.MeterPhase == "3")
        //{
        //    lblMeterPhase.Value = "Three";
        //}

        //Telerik.Reporting.TextBox lblOffPeak = rpt.Items.Find("lblOffPeak", true)[0] as Telerik.Reporting.TextBox;
        //if (stPro.OffPeak == "True")
        //{
        //    lblOffPeak.Value = "Yes";
        //}
        //else
        //{
        //    lblOffPeak.Value = "No";
        //}
        //Telerik.Reporting.TextBox lblRetailer = rpt.Items.Find("lblRetailer", true)[0] as Telerik.Reporting.TextBox;
        //lblRetailer.Value = stPro.ElecRetailer;

        //Telerik.Reporting.TextBox lblPeakMeterNo = rpt.Items.Find("lblPeakMeterNo", true)[0] as Telerik.Reporting.TextBox;
        //lblPeakMeterNo.Value = stPro.MeterNumber1;


        //Telerik.Reporting.HtmlTextBox txtpaymnetfrm = rpt.Items.Find("txtpaymnetfrm", true)[0] as Telerik.Reporting.HtmlTextBox;
        //txtpaymnetfrm.Value = "<b> Paymnet Form </b><br/>Project: " + stPro.ProjectNumber + "<br/>Customer: " + stCont.ContFirst + " " + stCont.ContLast + "<br/> Installation at: " + stPro.InstallAddress + ", " + stPro.InstallCity + ", " + stPro.InstallState + ", " + stPro.InstallPostCode;


        //Telerik.Reporting.TextBox lblInstallBookingDate = rpt.Items.Find("lblInstallBookingDate", true)[0] as Telerik.Reporting.TextBox;
        //try
        //{
        //    lblInstallBookingDate.Value = string.Format("{0: dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));

        //}
        //catch { }


        ExportPdf(rpt);

    }
    public static void generate_TaxinvoiceForSolarMiner(string ProjectID, String SaveOrDownload)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        Telerik.Reporting.Report rpt = new EurosolarReporting.SmTaxInvoice();

        rpt.DocumentName = "TAX_INVOICE" + ProjectID;


        Telerik.Reporting.TextBox txtheader = rpt.Items.Find("txtinvoiceno", true)[0] as Telerik.Reporting.TextBox;
        txtheader.Value = stPro.InvoiceNumber;

        try
        {
            Telerik.Reporting.TextBox textBox1 = rpt.Items.Find("txtinvoicedate", true)[0] as Telerik.Reporting.TextBox;
            textBox1.Value = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(stPro.InvoiceSent));
        }
        catch { }
        //try
        //{
        //    Telerik.Reporting.TextBox lblSalesInvDate = rpt.Items.Find("lblSalesInvDate", true)[0] as Telerik.Reporting.TextBox;
        //    lblSalesInvDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.ToString()));
        //}
        //catch { }


        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;

        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;


        table2.DataSource = dt;
        table4.DataSource = dt;
        table5.DataSource = dt;
        table6.DataSource = dt;

        table8.DataSource = dt;

        Telerik.Reporting.TextBox txtinverters = rpt.Items.Find("txtinverters", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtpanel = rpt.Items.Find("txtpanel", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtCity = rpt.Items.Find("textBox17", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdep = rpt.Items.Find("txtdep", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.TextBox txtInvNotes = rpt.Items.Find("txtnotes", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsystemcap = rpt.Items.Find("txtsys", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtAdd = rpt.Items.Find("textBox44", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtunitprice = rpt.Items.Find("txtunitprice", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtgst = rpt.Items.Find("txtgst", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtamt = rpt.Items.Find("txtamt", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtbal = rpt.Items.Find("txtbal", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstate = rpt.Items.Find("txtstate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtpostcode = rpt.Items.Find("textBox41", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.TextBox txttotal = rpt.Items.Find("txttotal", true)[0] as Telerik.Reporting.TextBox;
        if (dt.Rows[0]["SystemCapKw"].ToString() != null && dt.Rows[0]["SystemCapKw"].ToString() != "")
        {
            txtsystemcap.Value = dt.Rows[0]["SystemCapKw"].ToString() + " KW";
        }
        else
        {
            txtsystemcap.Value = dt1.Rows[0]["SystemCapKw"].ToString() + " KW";
        }

        if (stPro.InstallAddress != "")
        {
            txtAdd.Value = stPro.InstallAddress;
        }
        if (stPro.InstallState != "")
        {
            txtstate.Value = stPro.InstallState;
        }
        if (stPro.InstallPostCode != "")
        {
            txtpostcode.Value = stPro.InstallPostCode;
        }
        if (stPro.InstallCity != "")
        {
            txtCity.Value = stPro.InstallCity;
        }
        if (stPro.InverterDetailsID != "")
        {
            txtinverters.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";
        }
        if (stPro.SecondInverterDetailsID != "0")
        {
            txtinverters.Value = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";
        }
        if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "0")
        {
            txtinverters.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";

        }

        txtpanel.Value = dt.Rows[0]["NumberPanels"].ToString() + " X " + dt.Rows[0]["PanelOutput"].ToString() + " " + dt.Rows[0]["PanelStockName"].ToString();
        if (stPro.FDA != "")
        {
            txtdep.Value = "$" + stPro.FDA;
        }
        if (stPro.InvoiceNotes != "")
        {
            txtInvNotes.Value = stPro.InvoiceNotes;
        }
        #region PriceCalculation
        if (stPro.TotalQuotePrice != null && stPro.TotalQuotePrice != "")
        {
            decimal TotalPrice = decimal.Parse(stPro.TotalQuotePrice);
            decimal TotalPrice1 = Math.Round(TotalPrice, 2);
            txtunitprice.Value = Convert.ToString(TotalPrice1);
            string up = txtunitprice.Value;
            if (up != ("0") && up.Length > 4)
            {
                string up1 = up.Insert(1, ",");
                txtunitprice.Value = up1;
            }
            else
            {
                txtunitprice.Value = Convert.ToString(TotalPrice1);
            }

            decimal gstAmt = (TotalPrice1 * 10) / 100;
            decimal gstAmt1 = Math.Round(gstAmt, 2);
            txtgst.Value = Convert.ToString(gstAmt1);
            decimal FinalAmt = gstAmt1 + TotalPrice1;
            decimal FinalAmt1 = Math.Round(FinalAmt, 2);
            txtamt.Value = Convert.ToString(FinalAmt1);
            string amt = txtamt.Value;
            if (amt != ("0") && amt.Length > 4)
            {
                string up2 = amt;
                string up1 = up2.Insert(1, ",");
                txtamt.Value = up1;
            }
            else
            {
                txtamt.Value = Convert.ToString(FinalAmt1);
            }
            if (stPro.FDA != "")
            {
                decimal Depo = decimal.Parse(stPro.FDA);
                decimal Depo1 = Math.Round(Depo, 2);
                decimal balance = FinalAmt1 - Depo1;
                txtbal.Value = "$" + Convert.ToString(balance);
                string bal = Convert.ToString(balance);
                if (bal != ("0") && bal.Length > 4)
                {
                    string up2 = amt;
                    string up1 = bal.Insert(1, ",");
                    txtbal.Value = up1;
                }
                else
                {
                    txtbal.Value = "$" + Convert.ToString(balance);
                }
            }
            string amt2 = Convert.ToString(FinalAmt1);
            if (amt2 != ("0") && amt2.Length > 4)
            {
                string up2 = amt2;
                string up1 = up2.Insert(1, ",");
                txttotal.Value = "$" + up1;
            }
            else
            {
                txttotal.Value = "$" + Convert.ToString(FinalAmt1);
            }
        }
        #endregion
        if (SaveOrDownload == "Save")
        {
            SavePDF2(rpt, stPro.ProjectNumber + "_TaxInvoice.pdf", "SavePDF2");
        }
        else if (SaveOrDownload == "Download")
        {
            ExportPdf(rpt);
        }

    }

    public static void generate_TaxinvoiceForSolarMinerNew(string ProjectID, String SaveOrDownload)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        Telerik.Reporting.Report rpt = new EurosolarReporting.SmTaxInvoiceNew();

        rpt.DocumentName = "TAX_INVOICE" + ProjectID;


        Telerik.Reporting.TextBox txtheader = rpt.Items.Find("txtinvoiceno", true)[0] as Telerik.Reporting.TextBox;
        txtheader.Value = stPro.InvoiceNumber;

        try
        {
            Telerik.Reporting.TextBox textBox1 = rpt.Items.Find("txtinvoicedate", true)[0] as Telerik.Reporting.TextBox;
            Telerik.Reporting.TextBox txtinvcreateDate = rpt.Items.Find("txtinvDate", true)[0] as Telerik.Reporting.TextBox;
            if (stPro.InvoiceCreatedDate != null && stPro.InvoiceCreatedDate != "")
            {
                txtinvcreateDate.Value = Convert.ToDateTime(stPro.InvoiceCreatedDate).ToShortDateString(); ;
            }
            textBox1.Value = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(stPro.InvoiceSent));
        }
        catch(Exception ex) { }
        //try
        //{
        //    Telerik.Reporting.TextBox lblSalesInvDate = rpt.Items.Find("lblSalesInvDate", true)[0] as Telerik.Reporting.TextBox;
        //    lblSalesInvDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.ToString()));
        //}
        //catch { }

        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox txtinverters = rpt.Items.Find("txtinverters", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtpanel = rpt.Items.Find("txtpanel", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsysSize = rpt.Items.Find("txtsys", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstcince = rpt.Items.Find("txtstcince", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtvicrebate = rpt.Items.Find("txtvicrebate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtvicloanDisc = rpt.Items.Find("txtvicloanDisc", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtnetcost = rpt.Items.Find("txtnetcost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdepositepaid = rpt.Items.Find("txtdepositepaid", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtbaldue = rpt.Items.Find("txtbaldue", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txttotal = rpt.Items.Find("txttotal", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtnote = rpt.Items.Find("txtnote", true)[0] as Telerik.Reporting.TextBox;

        table4.DataSource = dt;
        table5.DataSource = dt;
        table6.DataSource = dt;
        table7.DataSource = dt;
        if (dt.Rows[0]["SystemCapKw"].ToString() != null && dt.Rows[0]["SystemCapKw"].ToString() != "")
        {
            txtsysSize.Value = dt.Rows[0]["SystemCapKw"].ToString() + " KW";
        }
        else
        {
            txtsysSize.Value = dt1.Rows[0]["SystemCapKw"].ToString() + " KW";
        }
        if (stPro.InverterDetailsID != "")
        {
            txtinverters.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";
        }
        if (stPro.SecondInverterDetailsID != "0")
        {
            txtinverters.Value = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";
        }
        if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "0")
        {
            txtinverters.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";

        }
        txtpanel.Value = dt.Rows[0]["NumberPanels"].ToString() + " X " + dt.Rows[0]["PanelOutput"].ToString() + " " + dt.Rows[0]["PanelStockName"].ToString();
        decimal since = 0, ViCLoanval = 0, VicLoanDiscval = 0, Total = 0, VicRebate = 0;
        try
        {
            if (dt.Rows[0]["RECRebate"].ToString() != null && dt.Rows[0]["RECRebate"].ToString() != "")
            {
                since = decimal.Parse(dt.Rows[0]["RECRebate"].ToString());
                decimal since1 = Math.Round(since, 2);
                txtstcince.Value = "$ " + since1;
            }
            else
            {
                txtstcince.Value = "$ " + "0.0";
            }
        }
        catch (Exception Ex)
        {

        }
        if (stPro.VicRebate1 != null && stPro.VicRebate1 != "")
        {
            VicRebate = decimal.Parse(stPro.VicRebate1);
            decimal vi = Math.Round(VicRebate, 2);
            VicRebate = vi;
            txtvicrebate.Value = "$ " + Convert.ToString(VicRebate);
        }
        else
        {
            txtvicrebate.Value = "$ " + "0.00";
        }
        //if (stPro.ViCLoan != null && stPro.ViCLoan != "")
        //{
        //    ViCLoanval = decimal.Parse(stPro.ViCLoan);
        //    decimal pac1 = Math.Round(ViCLoanval, 2);
        //    txtvicrebate.Value = "$ " + Convert.ToString(pac1);
        //}

        if (stPro.VicLoanDisc != null && stPro.VicLoanDisc != "")
        {
            VicLoanDiscval = decimal.Parse(stPro.VicLoanDisc);
            decimal pac1 = Math.Round(VicLoanDiscval, 2);
            txtvicloanDisc.Value = "$ " + Convert.ToString(pac1);
        }
        else
        {
            txtvicloanDisc.Value = "$ " + "0.00";
        }
        decimal netcost = 0, DepositReq = 0, netcost1 = 0, TotalCost = 0, amtpaid=0;
        netcost = decimal.Parse(stPro.TotalQuotePrice);
        if (dtCount != null && dtCount.Rows.Count > 0)
        {
            if (dtCount.Rows[0]["BankInvoicePayNew"].ToString() != null && dtCount.Rows[0]["BankInvoicePayNew"].ToString() != "")
            {
                DepositReq = decimal.Parse(dtCount.Rows[0]["BankInvoicePayNew"].ToString());
                decimal pac1 = Math.Round(DepositReq, 2);
                txtdepositepaid.Value = "$ " + Convert.ToString(pac1);
                amtpaid = pac1;
            }
            else
            {
                txtdepositepaid.Value = "$ " + "0.00";
            }
        }
        else
        {
            txtdepositepaid.Value = "$ " + "0.00";
        }

        if (stPro.TotalQuotePrice != null && stPro.TotalQuotePrice != "")
        {
            netcost1 = decimal.Parse(stPro.TotalQuotePrice);
            TotalCost = decimal.Parse(stPro.TotalQuotePrice);
        }
        // txttotal.Value = "$ " +Math.Round((since+ ViCLoanval+ VicLoanDiscval+ netcost),2);
        txttotal.Value = "$ " + Math.Round((since + netcost1), 2);

        Total = Math.Round((since + netcost1), 2);
        if (stPro.TotalQuotePrice != null && stPro.TotalQuotePrice != "")
        {
            netcost = decimal.Parse(stPro.PreviousTotalQuotePrice);
            decimal oth = since + VicRebate + VicLoanDiscval;
            decimal netCost3 = Total - oth;
            ///decimal netCost3 = netcost1 - DepositReq;
            decimal pac1 = Math.Round(netCost3, 2);
            txtnetcost.Value = "$ " + Convert.ToString(pac1);
            //decimal BalDue = Math.Round((netcost1 - DepositReq), 2);
            decimal BalDue = pac1 - amtpaid;
            // txtbaldue.Value = "$ " + Convert.ToString(BalDue);
            txtbaldue.Value = "$ " + Convert.ToString(BalDue);
            txtnote.Value = "The Net Cost above includes GST of: $ " + Math.Round((Total / 11), 2);
        }
        else
        {
            txtnetcost.Value = "$ " + "0.00";
            txtbaldue.Value = "$ " + "0.00";
        }


        if (SaveOrDownload == "Save")
        {
            SavePDF2(rpt, stPro.ProjectNumber + "_TaxInvoice.pdf", "SavePDF2");
        }
        else if (SaveOrDownload == "Download")
        {
            ExportPdf(rpt);
        }
    }

    public static void generate_PaymentReceiptForSolarMiner(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        Telerik.Reporting.Report rpt = new EurosolarReporting.Smpmt();
        //DataTable dtPmtDetails = ClstblProjects.(ProjectID);
        rpt.DocumentName = "Sm_Pmt_Receipt" + ProjectID;

        Telerik.Reporting.TextBox txtheader = rpt.Items.Find("txtinvoiceno", true)[0] as Telerik.Reporting.TextBox;
        txtheader.Value = stPro.InvoiceNumber;
        Telerik.Reporting.TextBox txtInvoTotal = rpt.Items.Find("txtInvoTotal", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txttotalcost = rpt.Items.Find("txttotalcost", true)[0] as Telerik.Reporting.TextBox;

        try
        {
            Telerik.Reporting.TextBox textBox1 = rpt.Items.Find("txtinvoicedate", true)[0] as Telerik.Reporting.TextBox;
            textBox1.Value = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(stPro.InvoiceSent));
        }
        catch { }
        //try
        //{
        //    Telerik.Reporting.TextBox lblSalesInvDate = rpt.Items.Find("lblSalesInvDate", true)[0] as Telerik.Reporting.TextBox;
        //    lblSalesInvDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.ToString()));
        //}
        //catch { }
        if (stPro.TotalQuotePrice != null && stPro.TotalQuotePrice != "")
        {
            decimal ttlCost = decimal.Parse(stPro.TotalQuotePrice);
            decimal ttl = Math.Round(ttlCost, 2);
            txtInvoTotal.Value = ttl.ToString();
            txttotalcost.Value = ttl.ToString();
        }
        else
        {
            txtInvoTotal.Value = "0.00";
            txttotalcost.Value = "0.00";

        }

        Telerik.Reporting.Table table2 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;

        table2.DataSource = dt;
        table4.DataSource = dt;
        table5.DataSource = dt;
        table6.DataSource = dt;

        //table7.DataSource = dt;

        //Telerik.Reporting.TextBox txtinverters = rpt.Items.Find("txtinverters", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtpanel = rpt.Items.Find("txtpanel", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtCity = rpt.Items.Find("textBox17", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtdep = rpt.Items.Find("txtdep", true)[0] as Telerik.Reporting.TextBox;

        ////Telerik.Reporting.TextBox txtInvoTotal = rpt.Items.Find("txtInvoTotal", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtsystemcap = rpt.Items.Find("txtsys", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtAdd = rpt.Items.Find("textBox44", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtunitprice = rpt.Items.Find("txtunitprice", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtgst = rpt.Items.Find("txtgst", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtamt = rpt.Items.Find("txtamt", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtnetcost = rpt.Items.Find("txtnetcost", true)[0] as Telerik.Reporting.TextBox;
        ////Telerik.Reporting.TextBox txtAdd = rpt.Items.Find("txtAdd", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtstcrebate = rpt.Items.Find("txtstcrebate", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txttotal = rpt.Items.Find("txttotal", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtlessdisc = rpt.Items.Find("txtlessdisc", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtnetcost = rpt.Items.Find("txtnetcost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtAdd = rpt.Items.Find("txtAdd", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstcrebate = rpt.Items.Find("txtstcrebate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txttotal = rpt.Items.Find("txttotal", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtlessdisc = rpt.Items.Find("txtlessdisc", true)[0] as Telerik.Reporting.TextBox;

        if (stPro.RECRebate != null)
        {
            decimal Rebate = decimal.Parse(stPro.RECRebate);
            decimal Rebate1 = Math.Round(Rebate, 2);
            txtstcrebate.Value = Rebate1.ToString();
        }
        if (stPro.InstallAddress != "")
        {
            txtAdd.Value = stPro.InstallAddress;
        }
        txtlessdisc.Value = "0.00";
        //if (stPro.InstallCity != "")
        //{
        //    txtCity.Value = stPro.InstallCity;
        //}
        if (txtInvoTotal.Value != null && stPro.RECRebate != null && txtlessdisc.Value != null)
        {

            decimal ttlCost = decimal.Parse(txtInvoTotal.Value.ToString());
            decimal lessDisc = decimal.Parse(txtlessdisc.Value.ToString());
            decimal Rebate = decimal.Parse(stPro.RECRebate);
            decimal Rebate1 = Math.Round(Rebate, 2);
            decimal netCost = ttlCost - Rebate1 - lessDisc;
            txtnetcost.Value = Convert.ToString(netCost);

        }
        DataTable dtpmt = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(ProjectID);
        if (dtpmt != null && dtpmt.Rows.Count > 0)
        {
            if (!dtpmt.Columns.Contains("DtPayDate"))
            {
                dtpmt.Columns.Add("DtPayDate");
            }
            if (!dtpmt.Columns.Contains("CCSurcharge1"))
            {
                dtpmt.Columns.Add("CCSurcharge1");
            }
            if (!dtpmt.Columns.Contains("InvoicePayTotal1"))
            {
                dtpmt.Columns.Add("InvoicePayTotal1");
            }
            for (int i = 0; i < dtpmt.Rows.Count; i++)
            {
                string date = dtpmt.Rows[i]["InvoicePayDate"].ToString();
                dtpmt.Rows[i]["DtPayDate"] = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(date));
                string CCSurcharge = dtpmt.Rows[i]["CCSurcharge"].ToString();
                string InvoicePayTotal1 = dtpmt.Rows[i]["InvoicePayTotal"].ToString();
                if (InvoicePayTotal1 != null && InvoicePayTotal1 != "")
                {
                    decimal InvoicePayTotal = decimal.Parse(InvoicePayTotal1);
                    decimal InvoicePayTotal2 = Math.Round(InvoicePayTotal, 2);
                    dtpmt.Rows[i]["InvoicePayTotal1"] = InvoicePayTotal2.ToString();
                }
                else
                {

                }
                if (CCSurcharge == null || CCSurcharge == "")
                {
                    dtpmt.Rows[i]["CCSurcharge1"] = "-";
                }
                else
                {
                    decimal CCSurcharge1 = decimal.Parse(CCSurcharge);
                    decimal CCSurcharge2 = Math.Round(CCSurcharge1, 2);
                    dtpmt.Rows[i]["CCSurcharge1"] = CCSurcharge2.ToString();
                }
            }
            table7.DataSource = dtpmt;
        }
        if (dtCount != null && dtCount.Rows.Count > 0)
        {
            if (dtCount.Rows[0]["BankInvoicePay"].ToString() != null && dtCount.Rows[0]["BankInvoicePay"].ToString() != "")
            {
                decimal InvoicePayTotal = decimal.Parse(dtCount.Rows[0]["BankInvoicePay"].ToString());
                decimal InvoicePayTotal1 = Math.Round(InvoicePayTotal, 2);
                if (stPro.TotalQuotePrice != null && stPro.TotalQuotePrice != "")
                {
                    decimal QuotPrice = decimal.Parse(stPro.TotalQuotePrice);
                    decimal Total = QuotPrice - InvoicePayTotal;
                    decimal Total1 = Math.Round(Total, 2);
                    txttotal.Value = "$" + Total1.ToString();
                }
                else
                {
                    txttotal.Value = "$0.00";
                }
            }
        }
        ExportPdf(rpt);
        #region PriceCalculation Old
        //if (stPro.TotalQuotePrice != null && stPro.TotalQuotePrice != "")
        //{
        //    decimal TotalPrice = decimal.Parse(stPro.TotalQuotePrice);
        //    decimal TotalPrice1 = Math.Round(TotalPrice, 2);
        //    txtunitprice.Value = Convert.ToString(TotalPrice1);
        //    string up = txtunitprice.Value;
        //    if (up != ("0") && up.Length > 4)
        //    {
        //        string up1 = up.Insert(1, ",");
        //        txtunitprice.Value = up1;
        //    }
        //    else
        //    {
        //        txtunitprice.Value = Convert.ToString(TotalPrice1);
        //    }

        //    decimal gstAmt = (TotalPrice1 * 10) / 100;
        //    decimal gstAmt1 = Math.Round(gstAmt, 2);
        //    txtgst.Value = Convert.ToString(gstAmt1);
        //    decimal FinalAmt = gstAmt1 + TotalPrice1;
        //    decimal FinalAmt1 = Math.Round(FinalAmt, 2);
        //    txtamt.Value = Convert.ToString(FinalAmt1);
        //    string amt = txtamt.Value;
        //    if (amt != ("0") && amt.Length > 4)
        //    {
        //        string up2 = amt;
        //        string up1 = up2.Insert(1, ",");
        //        txtamt.Value = "$" + up1;
        //    }
        //    else
        //    {
        //        txtamt.Value = Convert.ToString(FinalAmt1);
        //    }
        //    string InvoicepayTotal = dtCount.Rows[0]["InvoicepayTotal"].ToString();
        //    if (InvoicepayTotal != "" && dep != "")
        //    {
        //       // txtbal.Value = "$" + InvoicepayTotal;
        //        decimal finalAmt = 0;
        //        decimal baldue = 0;
        //        decimal InvoicepayTotal1 = decimal.Parse(InvoicepayTotal);
        //        decimal dep1 = decimal.Parse(dep);
        //        finalAmt = InvoicepayTotal1 + dep1;
        //        baldue = FinalAmt - finalAmt;
        //        string bd = Convert.ToString(baldue);
        //        if (bd != ("0") && bd.Length > 4)
        //        {
        //            string up2 = bd;
        //            string up1 = up2.Insert(1, ",");
        //            txttotal.Value = "$" + up2;
        //        }
        //        else
        //        {
        //            txttotal.Value = "$" + bd;
        //        }

        //    }
        //    else
        //    {
        //        InvoicepayTotal = "$" + "0.00";
        //        //txtbal.Value = InvoicepayTotal;
        //    }


        //}
        #endregion OLD


    }
    public static void generate_PickListForSolarMiner(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.SmPicklist_new();

        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;

        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table12 = rpt.Items.Find("table12", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table10 = rpt.Items.Find("table10", true)[0] as Telerik.Reporting.Table;

        table1.DataSource = dt;
        table2.DataSource = dt;
        table3.DataSource = dt;
        table4.DataSource = dt;
        table5.DataSource = dt;
        table6.DataSource = dt;
        table7.DataSource = dt;
        table8.DataSource = dt;
        table10.DataSource = dt;
        table12.DataSource = dt;
        // table8.DataSource = dt;

        Telerik.Reporting.TextBox textBox3 = rpt.Items.Find("txtinstdate", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox textBox113 = rpt.Items.Find("textBox113", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox70 = rpt.Items.Find("textBox70", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.TextBox txtaddress = rpt.Items.Find("txtaddress", true)[0] as Telerik.Reporting.TextBox;
        // Telerik.Reporting.TextBox textBox119 = rpt.Items.Find("textBox119", true)[0] as Telerik.Reporting.TextBox;
        // Telerik.Reporting.TextBox txrintadd2 = rpt.Items.Find("txrintadd2", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txrintadd1_1 = rpt.Items.Find("txrintadd1_1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox63 = rpt.Items.Find("textBox63", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox130instnote = rpt.Items.Find("textBox130", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtaddress2 = rpt.Items.Find("txtaddress2", true)[0] as Telerik.Reporting.TextBox;
        if (stPro.PanelBrandID != string.Empty)
        {
            SttblStockItems stPan = ClstblStockItems.tblStockItems_SelectByStockItemID(stPro.PanelBrandID);

            Telerik.Reporting.TextBox lblPanDesc = rpt.Items.Find("textBox57", true)[0] as Telerik.Reporting.TextBox;
            lblPanDesc.Value = stPan.StockDescription;
            // Telerik.Reporting.TextBox lblPanDesc1 = rpt.Items.Find("lblPanDesc1", true)[0] as Telerik.Reporting.TextBox;
            //lblPanDesc1.Value = stPan.StockDescription;
        }
        //Telerik.Reporting.TextBox textBox61 = rpt.Items.Find("textBox63", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox lblqty1 = rpt.Items.Find("textBox58", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox lblqty2 = rpt.Items.Find("textBox61", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox lblqty11 = rpt.Items.Find("lblqty11", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox lblqty21 = rpt.Items.Find("lblqty21", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtnumofpnl = rpt.Items.Find("textBox51", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtint1qty = rpt.Items.Find("textBox71", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtint2qty = rpt.Items.Find("textBox46", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtint3qty = rpt.Items.Find("textBox50", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtnumofpnl2 = rpt.Items.Find("textBox110", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtint1qty2 = rpt.Items.Find("textBox114", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtint2qty2 = rpt.Items.Find("textBox118", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtint3qty2 = rpt.Items.Find("textBox122", true)[0] as Telerik.Reporting.TextBox;
        txtnumofpnl.Value = stPro.NumberPanels;
        txtnumofpnl2.Value = stPro.NumberPanels;

        if (stPro.inverterqty != string.Empty)
        {

            //lblqty1.Value = stPro.inverterqty;
            // lblqty11.Value = stPro.inverterqty;
        }
        else
        {

            //lblqty1.Value = "0";
            // lblqty11.Value = "0";
        }
        if (stPro.inverterqty2 != string.Empty)
        {
            //lblqty2.Value = stPro.inverterqty2;
            // lblqty21.Value = stPro.inverterqty2;
        }
        else
        {
            // lblqty2.Value = "0";
            //lblqty21.Value = "0";
        }


        if (stPro.InverterDetailsID != string.Empty)
        {

            table4.DataSource = dt;
            // table7.DataSource = dt;
            txtint1qty.Value = stPro.inverterqty;
            txtint1qty2.Value = stPro.inverterqty;
        }
        else
        {
            table4.Visible = false;
            // table7.Visible = false;
        }
        if (stPro.SecondInverterDetailsID != string.Empty)
        {
            //table5.DataSource = dt;
            //table6.DataSource = dt;
            txtint2qty.Value = stPro.inverterqty2;
            txtint2qty2.Value = stPro.inverterqty2;
        }
        else
        {
            // table5.Visible = false;
            //table6.DataSource = dt;
        }
        if (stPro.ThirdInverterDetailsID != string.Empty)
        {
            //table5.DataSource = dt;
            //table6.DataSource = dt;
            txtint3qty.Value = stPro.inverterqty3;
            txtint3qty2.Value = stPro.inverterqty3;
        }

        textBox63.Value = stPro.InstallerNotes;
        textBox130instnote.Value = stPro.InstallerNotes;
        try
        {
            textBox3.Value = "Date : " + string.Format("{0:dd MMM yyyy}", Convert.ToDateTime("InstallBookingDate".ToString()));
            textBox70.Value = "Date : " + string.Format("{0:dd MMM yyyy}", Convert.ToDateTime("InstallBookingDate".ToString()));
        }
        catch { }
        if (stPro.InstallBookingDate == "" || stPro.InstallBookingDate == null)
        {
            textBox3.Value = "";
            textBox70.Value = "";
            // textBox113.Value = "";
        }
        else
        {
            try
            {
                textBox3.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
                textBox70.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
                // textBox113.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
            }
            catch { }
        }
        try
        {
            // txrintadd1.Value = dt.Rows[0]["PostalAddress"].ToString();
            txtaddress.Value = dt.Rows[0]["PostalAddress"].ToString() + "" + dt.Rows[0]["PostalCity"].ToString() + ", " + dt.Rows[0]["PostalState"].ToString() + " - " + dt.Rows[0]["PostalPostCode"].ToString();
            txtaddress2.Value = dt.Rows[0]["PostalAddress"].ToString() + "" + dt.Rows[0]["PostalCity"].ToString() + ", " + dt.Rows[0]["PostalState"].ToString() + " - " + dt.Rows[0]["PostalPostCode"].ToString();
            // txrintadd2.Value = dt.Rows[0]["Installeraddress2"].ToString();
            // txrintadd1_1.Value = dt.Rows[0]["PostalAddress"].ToString();
            // textBox63.Value = dt.Rows[0]["PostalCity"].ToString() + ", " + dt.Rows[0]["PostalState"].ToString() + " - " + dt.Rows[0]["PostalPostCode"].ToString();
            // textBox130instnote.Value = dt.Rows[0]["PostalCity"].ToString() + ", " + dt.Rows[0]["PostalState"].ToString() + " - " + dt.Rows[0]["PostalPostCode"].ToString();
            //txrintadd2_2.Value = dt.Rows[0]["Installeraddress2"].ToString();
        }
        catch { }


        // Telerik.Reporting.TextBox textBox122 = rpt.Items.Find("textBox122", true)[0] as Telerik.Reporting.TextBox;
        // textBox61.Value = stPro.InstallerNotes; /*stPro.notes;*/
        //textBox122.Value = stPro.InstallerNotes; /*stPro.notes;*/ /*stPro.InstallerNotes;*/
        ExportPdf(rpt);


    }
    public static void generate_maintenanceForSolarMIner(string ProjectID, string ProjectMaintenanceID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        //dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notesMaintenance(ProjectID, ProjectMaintenanceID);
        SttblProjectMaintenance st = ClstblProjectMaintenance.tblProjectMaintenance_SelectByProjectMaintainanceByProjectID(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.SolarMinerMaintenance();
        //Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();



        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;


        Telerik.Reporting.TextBox txtInstallBookingDate = rpt.Items.Find("txtInstallBookingDate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtMtceCost = rpt.Items.Find("txtMtceCost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtMtceDiscount = rpt.Items.Find("txtMtceDiscount", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtMtceBalance = rpt.Items.Find("txtMtceBalance", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox24 = rpt.Items.Find("textBox24", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox25 = rpt.Items.Find("textBox25", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox30 = rpt.Items.Find("textBox30", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox16 = rpt.Items.Find("textBox16", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox13 = rpt.Items.Find("textBox13", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox42 = rpt.Items.Find("textBox42", true)[0] as Telerik.Reporting.TextBox;

        //Telerik.Reporting.TextBox textBox29 = rpt.Items.Find("textBox29", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox textBox18 = rpt.Items.Find("textBox18", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox textBox34 = rpt.Items.Find("textBox34", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox textBox36 = rpt.Items.Find("textBox36", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox46 = rpt.Items.Find("textBox46", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox textBox21 = rpt.Items.Find("textBox21", true)[0] as Telerik.Reporting.TextBox;

        table3.DataSource = dt;
        table4.DataSource = dt;
        table8.DataSource = dt;
        table6.DataSource = dt;
        table1.DataSource = dt;
        txtInstallBookingDate.Value = Convert.ToDateTime(stPro.InstallBookingDate).ToString("dd MMM yyyy");
        txtInstallBookingDate.Value = stPro.InstallBookingDate;
        if (stPro.InstallBookingDate == "" || stPro.InstallBookingDate == null)
        {
            txtInstallBookingDate.Value = "";
        }
        else
        {
            try
            {
                txtInstallBookingDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
            }
            catch { }
        }

        try
        {
            txtMtceCost.Value = SiteConfiguration.ChangeCurrency_Val(st.MtceCost);
        }
        catch { }

        try
        {
            txtMtceDiscount.Value = SiteConfiguration.ChangeCurrency_Val(st.MtceDiscount);
        }
        catch { }
        try
        {
            txtMtceBalance.Value = SiteConfiguration.ChangeCurrency_Val(st.MtceBalance);
        }
        catch { }
        try
        {
            textBox24.Value = stPro.ProjectNumber;
        }
        catch { }
        try
        {
            textBox25.Value = stPro.ManualQuoteNumber;
        }
        catch { }
        try
        {
            textBox30.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(dt.Rows[0]["CallDate"]));
        }
        catch { }
        try
        {
            textBox16.Value = dt.Rows[0]["CustomerInput"].ToString();
        }
        catch { }
        try
        {
            textBox13.Value = dt.Rows[0]["FaultIdentified"].ToString();
        }
        catch { }
        try
        {
            textBox42.Value = dt.Rows[0]["Contact"].ToString();
        }
        catch { }

        //try
        //{
        //    textBox18.Value = dt.Rows[0]["ActionRequired"].ToString();
        //}
        //catch { }
        //try
        //{
        //    textBox29.Value = dt.Rows[0]["FaultIdentified"].ToString();
        //}
        //catch { }
        //try
        //{
        //    textBox34.Value = dt.Rows[0]["WorkDone"].ToString();
        //}
        //catch { }
        //try
        //{
        //    textBox36.Value = dt.Rows[0]["Contact"].ToString();
        //}
        //catch { }
        try
        {
            string panelbrand = dt.Rows[0]["PanelBrandName"].ToString();
            string panelqty = dt.Rows[0]["NumberPanels"].ToString();
            textBox46.Value = panelqty + "X" + panelbrand;
        }
        catch { }
        try
        {
            string inv1brand = dt.Rows[0]["InverterDetailsName"].ToString();
            string inv1qty = dt.Rows[0]["inverterqty"].ToString();
            string inv2brand = dt.Rows[0]["SecondInverterDetails"].ToString();
            string inv2qty = dt.Rows[0]["inverterqty"].ToString();
            //textBox21.Value = inv1qty + "X" + inv1brand;
            if (!string.IsNullOrEmpty(inv2qty))
            {
                //textBox21.Value = inv1qty + "X" + inv1brand + "+" + inv2qty + "X" + inv2brand;
            }
        }
        catch { }
        ExportPdf(rpt);

    }
    public static void generate_customeracknowledgement_SolarMiner(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.SolarMinerCustomerAc1();
        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table9 = rpt.Items.Find("table9", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox textBox51 = rpt.Items.Find("textBox48", true)[0] as Telerik.Reporting.TextBox;

        table1.DataSource = dt;
        table9.DataSource = dt;
        try
        {
            textBox51.Value = dt.Rows[0]["Customer"].ToString();
        }
        catch { }

        Telerik.Reporting.TextBox textBox45 = rpt.Items.Find("textBox45", true)[0] as Telerik.Reporting.TextBox;
        try
        {
            textBox45.Value = string.Format("{0: dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
        }
        catch { }

        ExportPdf(rpt);
    }
    public static void generate_quatation(string ProjectID, string QuoteID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
        string pdfURL = ConfigurationManager.AppSettings["cdnURL"];
        dt = Clscustomerlead.tblCustomers_SelectByCustomerID_telerikdemo(stPro.CustomerID);

        dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.Nquotation();

        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table_Contact = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table2 = rpt.Items.Find("tbl_sitedetail", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;

        table1.DataSource = dt1;
        table_Contact.DataSource = dt1;
        // table2.DataSource = dt1;
        table3.DataSource = dt1;
        table4.DataSource = dt1;
        table5.DataSource = dt1;
        table6.DataSource = dt1;
        table7.DataSource = dt1;
        table8.DataSource = dt1;


        //Telerik.Reporting.TextBox txtInverterdetail = rpt.Items.Find("txtInverterdetail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtInverterdetail1 = rpt.Items.Find("txtInverterdetail1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtbasiccost = rpt.Items.Find("txtbasiccost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtreq = rpt.Items.Find("txtreq", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtcompl = rpt.Items.Find("txtcompl", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtextra = rpt.Items.Find("txtextra", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtfullretail = rpt.Items.Find("txtfullretail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdiscount2 = rpt.Items.Find("txtdiscount2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstc = rpt.Items.Find("txtstc", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtgst = rpt.Items.Find("txtgst", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdeposit = rpt.Items.Find("txtdeposit", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtlessdeposit = rpt.Items.Find("txtlessdeposit", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmeter = rpt.Items.Find("txtmeter", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtswitch = rpt.Items.Find("txtswitch", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmeterphase = rpt.Items.Find("txtmeterphase", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtcustname = rpt.Items.Find("txtcustname", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox195 = rpt.Items.Find("textBox195", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtProjectNumber = rpt.Items.Find("txtProjectNumber", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtpaymentplan = rpt.Items.Find("txtpaymentplan", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtotherdetail = rpt.Items.Find("txtotherdetail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox214 = rpt.Items.Find("textBox214", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtABN = rpt.Items.Find("textBox215", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox179 = rpt.Items.Find("textBox179", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox180 = rpt.Items.Find("textBox180", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox191 = rpt.Items.Find("textBox191", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.PictureBox pictureBox2 = rpt.Items.Find("pictureBox2", true)[0] as Telerik.Reporting.PictureBox;

        txtProjectNumber.Value = stPro.ProjectNumber;

        string inverterdetail = string.Empty;
        string lblbasiccost = string.Empty;
        string lblreq = "+ N/A";
        string lblcompl = "+ N/A";
        string lbltax = string.Empty;
        string lblfullretail = string.Empty;
        string lbldiscount2 = string.Empty;
        string lblstc = string.Empty;
        string lbltotalcost = string.Empty;
        string lbldeposit = string.Empty;
        string lbllessdeposit = string.Empty;
        string otherdetail = string.Empty;
        string paymentplan = string.Empty;

        //==================================================
        try
        {
            if (dt1.Rows[0]["FinanceWith"].ToString() != string.Empty)
            {
                paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString();//
                if (st2.FinanceWithDeposit != string.Empty)
                {
                    paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString() + " with " + (Convert.ToInt32(st2.FinanceWithDeposit) / 100).ToString() + "%";
                    if (dt1.Rows[0]["PaymentType"].ToString() != string.Empty)
                    {
                        paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString() + " with " + (Convert.ToInt32(st2.FinanceWithDeposit) / 100).ToString() + "%" + " for " + dt1.Rows[0]["PaymentType"].ToString();
                    }
                }
            }

            txtpaymentplan.Value = paymentplan;

            if (st2.PromoText != string.Empty)
            {
                textBox214.Visible = true;
                otherdetail = st2.PromoText;
            }
            else
            {
                textBox214.Visible = false;
            }
            txtotherdetail.Value = otherdetail;
        }
        catch
        {

        }
        if (stPro.InverterDetailsID != "")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";
        }
        if (stPro.SecondInverterDetailsID != "0")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";
        }
        if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "0")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";
        }
        if ((stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID == "0"))
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. ";
        }
        if (stPro.ServiceValue != string.Empty || stPro.ServiceValue != "")
        {
            lblbasiccost = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.ServiceValue) + Convert.ToDecimal(stPro.RECRebate)).ToString());
        }
        else
        {
            lblbasiccost = "0";
        }

        lbltax = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarMeterInstallation) + Convert.ToDecimal(stPro.VarMeterUG) + Convert.ToDecimal(stPro.VarAsbestos) + Convert.ToDecimal(stPro.VarSplitSystem) + Convert.ToDecimal(stPro.VarCherryPicker) + Convert.ToDecimal(stPro.VarTravelTime) + Convert.ToDecimal(stPro.VarOther)).ToString());
        if (lbltax == string.Empty)
        {
            lbltax = "0";
        }
        try
        {
            lblfullretail = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lblbasiccost) + Convert.ToDecimal(lbltax)).ToString());
        }
        catch { }
        if (stPro.SpecialDiscount != string.Empty || stPro.SpecialDiscount != "")
        {
            lbldiscount2 = SiteConfiguration.ChangeCurrency_Val(stPro.SpecialDiscount.ToString());
        }
        else
        {
            lbldiscount2 = "0";
        }
        if (stPro.RECRebate != string.Empty || stPro.RECRebate != "")
        {
            lblstc = SiteConfiguration.ChangeCurrency_Val(stPro.RECRebate.ToString());
        }
        else
        {
            lblstc = "0";
        }
        try
        {
            string cost = (Convert.ToDecimal(lblfullretail) - Convert.ToDecimal(lbldiscount2) - Convert.ToDecimal(lblstc)).ToString();
            //String.Format("${0:#.##}", x)
            lbltotalcost = string.Format("{0:0.00}", Convert.ToDecimal(cost));
        }
        catch { }

        if (stPro.DepositRequired != string.Empty || stPro.DepositRequired != "")
        {
            lbldeposit = SiteConfiguration.ChangeCurrency_Val(stPro.DepositRequired.ToString());
        }
        else
        {
            lbldeposit = "0";
        }
        try
        {
            lbllessdeposit = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lbltotalcost) - Convert.ToDecimal(lbldeposit)).ToString());
        }
        catch { }
        try
        {
            textBox179.Value = dt1.Rows[0]["ProjectNumber"].ToString();
        }
        catch { }
        try
        {
            textBox180.Value = dt1.Rows[0]["SalesRepName"].ToString();
        }
        catch { }
        try
        {
            textBox191.Value = dt1.Rows[0]["ContFirst"].ToString() + " " + dt1.Rows[0]["ContLast"].ToString();
        }
        catch { }
        txtmeterphase.Value = stPro.MeterPhase;

        txtmeter.Value = "Meter Phase : " + stPro.MeterPhase;
        txtswitch.Value = "Switchboard Upgrades : " + stPro.meterupgrade;

        //txtInverterdetail.Value = inverterdetail.ToString();
        txtInverterdetail1.Value = inverterdetail.ToString();
        txtbasiccost.Value = lblbasiccost.ToString();
        txtextra.Value = "+ " + lbltax.ToString();
        txtreq.Value = lblreq.ToString();
        txtcompl.Value = lblcompl.ToString();
        txtfullretail.Value = lblfullretail.ToString();
        txtdiscount2.Value = "- " + lbldiscount2.ToString();
        txtstc.Value = "- " + lblstc.ToString();
        txtgst.Value = lbltotalcost.ToString();
        txtdeposit.Value = lbldeposit.ToString();
        txtlessdeposit.Value = lbllessdeposit.ToString();
        //txtcustname.Value = stPro.Customer;
        textBox195.Value = (DateTime.Now.AddHours(14).ToString(("dd-MMM-yyyy")));

        // int existinSignLog = Clstbl_SignatureLog.tbl_SignatureLogExistByProjectID(ProjectID);
        int existinSignLog = Clstbl_SignatureLog.tbl_SignatureLogExistByProjectID_QDocNo(ProjectID, QuoteID);

        if (existinSignLog == 1)
        {
            // Sttbl_SignatureLog stsign = Clstbl_SignatureLog.tbl_SignatureLog_SelectByProjectID(ProjectID);
            Sttbl_SignatureLog stsign = Clstbl_SignatureLog.tbl_SignatureLog_SelectByProjectID_QDocNo(ProjectID, QuoteID);
            String ImageAdd = stsign.Image_Address;
            String Token = stsign.Token;
            //string Token = System.Web.HttpContext.Current.Request.QueryString["rt"];
            //if(string.IsNullOrEmpty(Token))
            //{
            //   Token = stsign.Token;
            //}
            Sttbl_TokenData stToken = Clstbl_TokenData.tbl_TokenData_SelectByToken_ProjectID(Token, ProjectID);

            Page p = new Page();
            String Virtualpath = stsign.Image_Address;
            int index = Virtualpath.IndexOf("userfiles");
            Virtualpath = Virtualpath.Substring(index);
            string img1 = p.Server.MapPath("~/" + Virtualpath);
            pictureBox2.Value = img1;

            try
            {
                textBox195.Value = (Convert.ToDateTime(stsign.Signed_Date)).ToString(("dd-MMM-yyyy"));
            }
            catch { }
            Clstbl_TokenData.tbl_TokenData_Update_SignFlag(Token, ProjectID, "True");

            /////////////////////////////////////////////////////////////
            //string fileName = stToken.QDocNo + "Quotation.pdf";
            //// ExportPdf(rpt);
            //SiteConfiguration.UploadPDFFile("quotedoc", fileName);
            //SiteConfiguration.deleteimage(fileName, "quotedoc");
            /////////////////////////////////////////////////////////////////////
            // SavePDF(rpt, QuoteID);
            //SiteConfiguration.DeletePDFFile("quotedoc", fileName);

            // string nQoute = "s" + stToken.QDocNo;
            string nQoute = stToken.QDocNo;
            SavePDF(rpt, nQoute);

            DataTable dt2 = ClstblProjects.tblProjectQuotes_SelectByProjectQuoteDoc(stToken.QDocNo);
            string ProjectQuoteDoc = dt2.Rows[0]["ProjectQuoteDoc"].ToString();
            string QuoteDoc = nQoute + "Quotation.pdf";
            ClstblProjects.tblProjectQuotes_UpdateQuoteDoc(ProjectQuoteDoc, QuoteDoc);

            string SQFileNmae = ProjectID + "_" + nQoute + "_" + Token + "Quotation.pdf";
            SavePDFForSQ(rpt, SQFileNmae);

            //Server.Execute("~/app_code/);
            //HttpContext.Current.Response.Redirect(HttpContext.Current.Request.RawUrl);
        }
        else
        {
            pictureBox2.Value = null;
            SavePDF(rpt, QuoteID);
        }




        //try
        //{
        //    string ABN = dt.Rows[0]["CustWebSiteLink"].ToString();
        //    if (ABN != "" && ABN != "0" && ABN != null)
        //    {
        //        txtABN.Value = ABN;
        //    }
        //    //else
        //    //{
        //    //    txtABN.Value = "";
        //    //}
        //}
        //catch { }
        //SavePDF(rpt, QuoteID);
        // ExportPdf(rpt);

    }
    public static void generate_receipt(string ProjectID)
    {
        DataTable dt = new DataTable();

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.new_paymentreceipt();

        //Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;

        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table11 = rpt.Items.Find("table11", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;

        //table1.DataSource = dt;
        table4.DataSource = dt;
        table3.DataSource = dt;
        table8.DataSource = dt;
        //table11.DataSource = dt;
        //table7.DataSource = dt;

        Telerik.Reporting.TextBox lblInvoiceNumber = rpt.Items.Find("lblInvoiceNumber", true)[0] as Telerik.Reporting.TextBox;
        lblInvoiceNumber.Value = stPro.InvoiceNumber;
        Telerik.Reporting.TextBox lblInvoiceSent = rpt.Items.Find("lblInvoiceSent", true)[0] as Telerik.Reporting.TextBox;

        try
        {
            lblInvoiceSent.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InvoiceSent));
        }
        catch { }


        DataTable dt1 = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(ProjectID);

        Telerik.Reporting.TextBox textBox3 = rpt.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox10 = rpt.Items.Find("textBox10", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox20 = rpt.Items.Find("textBox20", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox textBox1 = rpt.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
        table1.DataSource = dt1;



        Telerik.Reporting.TextBox txtfullretail = rpt.Items.Find("txtfullretail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdiscount2 = rpt.Items.Find("txtdiscount2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstc = rpt.Items.Find("txtstc", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtgst = rpt.Items.Find("txtgst", true)[0] as Telerik.Reporting.TextBox;

        string lbltax = string.Empty;
        string lblbasiccost = string.Empty;
        string lblfullretail = string.Empty;
        string lbldiscount2 = string.Empty;
        string lblstc = string.Empty;
        string lbltotalcost = string.Empty;
        string lbllessdeposit = string.Empty;


        lbltax = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarMeterInstallation) + Convert.ToDecimal(stPro.VarMeterUG) + Convert.ToDecimal(stPro.VarAsbestos) + Convert.ToDecimal(stPro.VarSplitSystem) + Convert.ToDecimal(stPro.VarCherryPicker) + Convert.ToDecimal(stPro.VarTravelTime) + Convert.ToDecimal(stPro.VarOther)).ToString());
        if (lbltax == string.Empty)
        {
            lbltax = "0";
        }
        if (stPro.ServiceValue != string.Empty || stPro.ServiceValue != "")
        {
            lblbasiccost = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.ServiceValue) + Convert.ToDecimal(stPro.RECRebate)).ToString());
        }
        else
        {
            lblbasiccost = "0";
        }
        try
        {
            lblfullretail = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lblbasiccost) + Convert.ToDecimal(lbltax)).ToString());
        }
        catch { }
        try
        {
            string cost = (Convert.ToDecimal(lblfullretail) - Convert.ToDecimal(lbldiscount2) - Convert.ToDecimal(lblstc)).ToString();
            //String.Format("${0:#.##}", x)
            lbltotalcost = string.Format("{0:0.00}", Convert.ToDecimal(cost));
        }
        catch { }
        if (stPro.SpecialDiscount != string.Empty || stPro.SpecialDiscount != "")
        {
            lbldiscount2 = SiteConfiguration.ChangeCurrency_Val(stPro.SpecialDiscount.ToString());
        }
        else
        {
            lbldiscount2 = "0";
        }
        if (stPro.RECRebate != string.Empty || stPro.RECRebate != "")
        {
            lblstc = SiteConfiguration.ChangeCurrency_Val(stPro.RECRebate.ToString());
        }
        else
        {
            lblstc = "0";
        }
        try
        {
            string cost = (Convert.ToDecimal(lblfullretail) - Convert.ToDecimal(lbldiscount2) - Convert.ToDecimal(lblstc)).ToString();
            //String.Format("${0:#.##}", x)
            lbltotalcost = string.Format("{0:0.00}", Convert.ToDecimal(cost));
        }
        catch { }


        txtfullretail.Value = "$ " + lblfullretail.ToString();
        txtdiscount2.Value = "- $ " + lbldiscount2.ToString();
        txtstc.Value = "- $ " + lblstc.ToString();
        txtgst.Value = "$ " + lbltotalcost.ToString();

        //textBox3.Value = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(dt1.Rows[0]["InvoicePayTotal"]));
        //if (!string.IsNullOrEmpty(Convert.ToString(dt1.Rows[0]["CCSurcharge"])))
        //{
        //    textBox10.Value = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(dt1.Rows[0]["CCSurcharge"]));
        //}
        //else
        //{
        //    textBox10.Value = "";
        //}
        try
        {
            textBox20.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(dt1.Rows[0]["TotalQuotePrice"]));
            //textBox1.Value = SiteConfiguration.ConvertToSystemDateDefault(Convert.ToString(dt1.Rows[0]["InvoicePayDate"]));
        }
        catch { }
        decimal paiddate = 0;
        decimal balown = 0;
        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
        {
            paiddate = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
            balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(paiddate);
            //var balminus = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"]);
            //decimal balown = Convert.ToDecimal(stPro.TotalQuotePrice); /*- balminus;*/
            //try
            //{
            Telerik.Reporting.TextBox lblBalanceOwing = rpt.Items.Find("lblBalanceOwing", true)[0] as Telerik.Reporting.TextBox;
            //Telerik.Reporting.TextBox lblBalanceOwing1 = rpt.Items.Find("lblBalanceOwing1", true)[0] as Telerik.Reporting.TextBox;
            lblBalanceOwing.Value = "Balance Due: $ " + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(balown)) + "        [Including – GST]";
            //  lblBalanceOwing1.Value = "$ "+SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balown));
            //}
            //catch { }
        }
        //Telerik.Reporting.TextBox lblReceiptDate = rpt.Items.Find("lblReceiptDate", true)[0] as Telerik.Reporting.TextBox;
        //lblReceiptDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14).ToString()));
        ExportPdf(rpt);
    }
    public static void generate_performainvoice(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.NInvoice();
        rpt.DocumentName = "PERFORMA INV";
        Telerik.Reporting.TextBox txtheader = rpt.Items.Find("txtheader", true)[0] as Telerik.Reporting.TextBox;
        txtheader.Value = "PERFORMA INV - " + stPro.InvoiceNumber;
        txtheader.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(25D);


        try
        {
            Telerik.Reporting.TextBox lblSalesInvDate = rpt.Items.Find("lblSalesInvDate", true)[0] as Telerik.Reporting.TextBox;
            lblSalesInvDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.ToString()));
        }
        catch { }

        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;

        //Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;

        table1.DataSource = dt;
        table2.DataSource = dt;
        table3.DataSource = dt;
        table4.DataSource = dt;
        table5.DataSource = dt;
        table6.DataSource = dt;

        //table8.DataSource = dt;

        Telerik.Reporting.TextBox txtinverters = rpt.Items.Find("txtinverters", true)[0] as Telerik.Reporting.TextBox;

        ////==================================================
        if (stPro.InverterDetailsID != "")
        {

            txtinverters.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";

        }
        if (stPro.SecondInverterDetailsID != "0")
        {
            txtinverters.Value = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";

        }
        if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "0")
        {
            txtinverters.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";

        }


        ////********************  amount ****************
        //Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox lblTotalQuotePrice = rpt.Items.Find("lblTotalQuotePrice", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblrebate = rpt.Items.Find("lblrebate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblNetCost = rpt.Items.Find("lblNetCost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblDepositPaid = rpt.Items.Find("lblDepositPaid", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.TextBox lblCommissionGST = rpt.Items.Find("lblCommissionGST", true)[0] as Telerik.Reporting.TextBox;

        //Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox lblBalanceDue = rpt.Items.Find("lblBalanceDue", true)[0] as Telerik.Reporting.TextBox;
        try
        {
            decimal newtotalcost = Convert.ToDecimal(stPro.RECRebate) + Convert.ToDecimal(stPro.TotalQuotePrice);
            try
            {
                lblTotalQuotePrice.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(newtotalcost));
            }
            catch { }
        }
        catch { }

        string lblstc = string.Empty;
        if (stPro.RECRebate != string.Empty || stPro.RECRebate != "")
        {
            lblstc = SiteConfiguration.ChangeCurrency_Val(stPro.RECRebate.ToString());
        }
        else
        {
            lblstc = "0";
        }

        lblrebate.Value = "- $ " + lblstc.ToString();

        try
        {
            lblNetCost.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(stPro.TotalQuotePrice);
        }
        catch { }
        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        decimal paiddate = 0;
        decimal balown = 0;
        if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
        {
            paiddate = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
            balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(paiddate);
            try
            {
                lblDepositPaid.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(paiddate.ToString());
            }
            catch { }
            try
            {
                lblBalanceDue.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(balown));
            }
            catch { }
        }
        else
        {
            lblDepositPaid.Value = "0";
            try
            {
                lblBalanceDue.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(stPro.TotalQuotePrice);
            }
            catch { }
        }

        decimal invamnt = 0;

        try
        {

            invamnt = (Convert.ToDecimal(stPro.TotalQuotePrice) * Convert.ToDecimal(0.9091));

        }
        catch { }
        //if (stEmp.GSTPayment == "True")
        {
            decimal GST = 0;
            try
            {

                GST = ((invamnt) * 10) / 100;
            }
            catch { }


            lblCommissionGST.Value = "$" + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(GST));
        }


        ExportPdf(rpt);
    }
    public static void generate_Taxinvoice(string ProjectID, String SaveOrDownload)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.NInvoice();

        rpt.DocumentName = "TAX INVOICE";


        Telerik.Reporting.TextBox txtheader = rpt.Items.Find("txtheader", true)[0] as Telerik.Reporting.TextBox;
        txtheader.Value = "TAX INVOICE - " + stPro.InvoiceNumber;

        try
        {
            Telerik.Reporting.TextBox textBox1 = rpt.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
            textBox1.Value = "INVOICE DATE : " + string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(stPro.InvoiceSent));
        }
        catch { }
        try
        {
            Telerik.Reporting.TextBox lblSalesInvDate = rpt.Items.Find("lblSalesInvDate", true)[0] as Telerik.Reporting.TextBox;
            lblSalesInvDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.ToString()));
        }
        catch { }

        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;

        //Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;

        table1.DataSource = dt;
        table2.DataSource = dt;
        table3.DataSource = dt;
        table4.DataSource = dt;
        table5.DataSource = dt;
        table6.DataSource = dt;

        //table8.DataSource = dt;

        Telerik.Reporting.TextBox txtinverters = rpt.Items.Find("txtinverters", true)[0] as Telerik.Reporting.TextBox;

        ////==================================================
        if (stPro.InverterDetailsID != "")
        {

            txtinverters.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";

        }
        if (stPro.SecondInverterDetailsID != "0")
        {
            txtinverters.Value = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";

        }
        if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "0")
        {
            txtinverters.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";

        }


        ////********************  amount ****************
        //Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox lblTotalQuotePrice = rpt.Items.Find("lblTotalQuotePrice", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblrebate = rpt.Items.Find("lblrebate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblNetCost = rpt.Items.Find("lblNetCost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblDepositPaid = rpt.Items.Find("lblDepositPaid", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.TextBox lblCommissionGST = rpt.Items.Find("lblCommissionGST", true)[0] as Telerik.Reporting.TextBox;

        //Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox lblBalanceDue = rpt.Items.Find("lblBalanceDue", true)[0] as Telerik.Reporting.TextBox;
        try
        {
            decimal newtotalcost = Convert.ToDecimal(stPro.RECRebate) + Convert.ToDecimal(stPro.TotalQuotePrice);
            try
            {
                lblTotalQuotePrice.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(newtotalcost));
            }
            catch { }
        }
        catch { }

        string lblstc = string.Empty;
        if (stPro.RECRebate != string.Empty || stPro.RECRebate != "")
        {
            lblstc = SiteConfiguration.ChangeCurrency_Val(stPro.RECRebate.ToString());
        }
        else
        {
            lblstc = "0";
        }

        lblrebate.Value = "- $ " + lblstc.ToString();

        try
        {
            lblNetCost.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(stPro.TotalQuotePrice);
        }
        catch { }
        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        decimal paiddate = 0;
        decimal balown = 0;
        if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
        {
            paiddate = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
            balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(paiddate);
            try
            {
                lblDepositPaid.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(paiddate.ToString());
            }
            catch { }
            try
            {
                lblBalanceDue.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(balown));
            }
            catch { }
        }
        else
        {
            lblDepositPaid.Value = "0";
            try
            {
                lblBalanceDue.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(stPro.TotalQuotePrice);
            }
            catch { }
        }

        decimal invamnt = 0;

        try
        {

            invamnt = (Convert.ToDecimal(stPro.TotalQuotePrice) * Convert.ToDecimal(0.9091));

        }
        catch { }
        //if (stEmp.GSTPayment == "True")
        {
            decimal GST = 0;
            try
            {

                GST = ((invamnt) * 10) / 100;
            }
            catch { }


            lblCommissionGST.Value = "$ " + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(GST));
        }

        if (SaveOrDownload == "Save")
        {
            SavePDF2(rpt, stPro.ProjectNumber + "_TaxInvoice.pdf", "SavePDF2");
        }
        else if (SaveOrDownload == "Download")
        {
            ExportPdf(rpt);
        }

    }
    public static void generate_refundform(string ProjectID, string RefundID)
    {
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string inverterdetail = string.Empty;

        DataTable dt = Clscustomerlead.tblCustomers_SelectByCustomerID_telerikdemo(stPro.CustomerID);
        // dt1 = Clscustomerlead.tblProjects_SelectByProjectID_demotelerik(ProjectID);
        DataTable dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist2(ProjectID, RefundID);
        SttblProjectRefund st = ClstblProjectRefund.tblProjectRefund_SelectByProjectID(ProjectID);
        SttblProjectRefund stRefund = ClstblProjectRefund.tblProjectRefund_SelectByRefundID(RefundID);
        DataTable stsales = ClstblProjectRefund.tblEmployees_sales_manager(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.NRefundForm();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();
        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        // Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        // Telerik.Reporting.Table table5 = rpt.Items.Find("tabl5", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table tbl_sitedetail = rpt.Items.Find("tbl_sitedetail", true)[0] as Telerik.Reporting.Table;

        //table1.DataSource = dt1;
        // table4.DataSource = dt;
        //table1.DataSource = dt1;
        //tbl_sitedetail.DataSource = dt1;

        table1.DataSource = dt1;
        table4.DataSource = dt1;
        Telerik.Reporting.TextBox txtSTATUS = rpt.Items.Find("txtSTATUS", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtNOOFPANELS = rpt.Items.Find("txtNOOFPANELS", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtAPPLICATION = rpt.Items.Find("txtAPPLICATION", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtDEPOSITDATE = rpt.Items.Find("txtDEPOSITDATE", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtENTRYDATE = rpt.Items.Find("txtENTRYDATE", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtPROMOTION = rpt.Items.Find("txtPROMOTION", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtBANKNAME = rpt.Items.Find("txtBANKNAME", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtBSBNO = rpt.Items.Find("txtBSBNO", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtACCNO = rpt.Items.Find("txtACCNO", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtAMOUNT = rpt.Items.Find("txtAMOUNT", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtACCOUNTNAME = rpt.Items.Find("txtACCOUNTNAME", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtREFUNDDATE = rpt.Items.Find("txtREFUNDDATE", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtSIGNATURE = rpt.Items.Find("txtSIGNATURE", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtProjectNumber = rpt.Items.Find("txtProjectNumber", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtProjectNumber1 = rpt.Items.Find("txtProjectNumber1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox21 = rpt.Items.Find("textBox21", true)[0] as Telerik.Reporting.TextBox;


        Telerik.Reporting.TextBox textBox25 = rpt.Items.Find("textBox25", true)[0] as Telerik.Reporting.TextBox;
        txtProjectNumber.Value = stPro.ProjectNumber;
        txtProjectNumber1.Value = stPro.ProjectNumber;
        txtBSBNO.Value = stRefund.BSBNo;
        txtACCNO.Value = stRefund.AccNo;
        txtAMOUNT.Value = "$ " + Convert.ToDecimal(stRefund.Amount).ToString("#,##0.00");
        txtACCOUNTNAME.Value = stRefund.AccName;
        txtBANKNAME.Value = stRefund.BankName;

        if (stRefund.AccActionDate == "" || stRefund.AccActionDate == null)
        {
            txtREFUNDDATE.Value = "";
        }
        else
        {
            try
            {
                txtREFUNDDATE.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stRefund.AccActionDate));
            }
            catch { }
        }

        textBox25.Value = stRefund.Notes;

        if (stPro.PrjStatus != null && stPro.PrjStatus != "")
        {
            txtSTATUS.Value = stPro.PrjStatus;
        }
        //if (stRefund.Status == "True")
        //{
        //    txtSTATUS.Value = "Successfull";
        //}
        //else
        //{
        //    txtSTATUS.Value = "Failed";
        //}
        txtNOOFPANELS.Value = stPro.NumberPanels;
        if (stRefund.CreateDate == "" || stRefund.CreateDate == null)
        {
            txtENTRYDATE.Value = "";
        }
        else
        {
            try
            {
                txtENTRYDATE.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stRefund.CreateDate));
            }
            catch { }
        }
        if (stPro.DepositReceived == "" || stPro.DepositReceived == null)
        {
            txtDEPOSITDATE.Value = "";
        }
        else
        {
            try
            {
                txtDEPOSITDATE.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.DepositReceived));
            }
            catch { }
        }
        try
        {
            textBox21.Value = stsales.Rows[0]["sales_manager"].ToString();
        }
        catch { }

        ExportPdf(rpt);

    }

    public static void generate_PickList(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();

        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.Npicklist_new();

        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;


        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;


        table1.DataSource = dt;
        table2.DataSource = dt;

        table3.DataSource = dt;
        table8.DataSource = dt;

        Telerik.Reporting.TextBox textBox3 = rpt.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox113 = rpt.Items.Find("textBox113", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.TextBox txrintadd1 = rpt.Items.Find("txrintadd1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox119 = rpt.Items.Find("textBox119", true)[0] as Telerik.Reporting.TextBox;
        //  Telerik.Reporting.TextBox txrintadd2 = rpt.Items.Find("txrintadd2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txrintadd1_1 = rpt.Items.Find("txrintadd1_1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox63 = rpt.Items.Find("textBox63", true)[0] as Telerik.Reporting.TextBox;
        // Telerik.Reporting.TextBox txrintadd2_2 = rpt.Items.Find("txrintadd2_2", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox lblProject = rpt.Items.Find("lblProject", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox lblProject1 = rpt.Items.Find("lblProject1", true)[0] as Telerik.Reporting.TextBox;
        //lblProject.Value = stPro.Project;
        //lblProject1.Value = stPro.Project;

        //Telerik.Reporting.TextBox lblPrinted = rpt.Items.Find("lblPrinted", true)[0] as Telerik.Reporting.TextBox;
        //lblPrinted.Value = "Printed:  " + string.Format("{0:dddd - dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14)));
        //Telerik.Reporting.TextBox lblPrinted1 = rpt.Items.Find("lblPrinted1", true)[0] as Telerik.Reporting.TextBox;
        //lblPrinted1.Value = "Printed:  " + string.Format("{0:dddd - dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.AddHours(14)));


        if (stPro.PanelBrandID != string.Empty)
        {
            SttblStockItems stPan = ClstblStockItems.tblStockItems_SelectByStockItemID(stPro.PanelBrandID);
            Telerik.Reporting.TextBox lblPanDesc = rpt.Items.Find("lblPanDesc", true)[0] as Telerik.Reporting.TextBox;
            lblPanDesc.Value = stPan.StockDescription;
            Telerik.Reporting.TextBox lblPanDesc1 = rpt.Items.Find("lblPanDesc1", true)[0] as Telerik.Reporting.TextBox;
            lblPanDesc1.Value = stPan.StockDescription;
        }

        Telerik.Reporting.TextBox lblqty1 = rpt.Items.Find("lblqty1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblqty2 = rpt.Items.Find("lblqty2", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox lblqty11 = rpt.Items.Find("lblqty11", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox lblqty21 = rpt.Items.Find("lblqty21", true)[0] as Telerik.Reporting.TextBox;

        if (stPro.inverterqty != string.Empty)
        {

            lblqty1.Value = stPro.inverterqty;
            //  lblqty11.Value = stPro.inverterqty;
        }
        else
        {

            lblqty1.Value = "0";
            //  lblqty11.Value = "0";
        }
        if (stPro.inverterqty2 != string.Empty)
        {
            lblqty2.Value = stPro.inverterqty2;
            // lblqty21.Value = stPro.inverterqty2;
        }
        else
        {
            lblqty2.Value = "0";
            //lblqty21.Value = "0";
        }


        if (stPro.InverterDetailsID != string.Empty)
        {
            table4.DataSource = dt;
            table7.DataSource = dt;
        }
        else
        {
            table4.Visible = false;
            table7.Visible = false;
        }
        if (stPro.SecondInverterDetailsID != string.Empty)
        {
            table5.DataSource = dt;
            table6.DataSource = dt;
        }
        else
        {
            table5.Visible = false;
            table6.DataSource = dt;
        }

        //try
        //{
        //    textBox3.Value = "Date : " + string.Format("{0:dd MMM yyyy}", Convert.ToDateTime("InstallBookingDate".ToString()));
        //}
        //catch { }
        if (stPro.InstallBookingDate == "" || stPro.InstallBookingDate == null)
        {
            textBox3.Value = "";
            textBox113.Value = "";
        }
        else
        {
            try
            {
                textBox3.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
                textBox113.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
            }
            catch { }
        }
        try
        {
            txrintadd1.Value = dt.Rows[0]["PostalAddress"].ToString();
            textBox119.Value = dt.Rows[0]["PostalCity"].ToString() + ", " + dt.Rows[0]["PostalState"].ToString() + " - " + dt.Rows[0]["PostalPostCode"].ToString();
            // txrintadd2.Value = dt.Rows[0]["Installeraddress2"].ToString();
            txrintadd1_1.Value = dt.Rows[0]["PostalAddress"].ToString();
            textBox63.Value = dt.Rows[0]["PostalCity"].ToString() + ", " + dt.Rows[0]["PostalState"].ToString() + " - " + dt.Rows[0]["PostalPostCode"].ToString();
            // txrintadd2_2.Value = dt.Rows[0]["Installeraddress2"].ToString();
        }
        catch { }

        //Telerik.Reporting.TextBox textBox63 = rpt.Items.Find("textBox63", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox textBox122 = rpt.Items.Find("textBox122", true)[0] as Telerik.Reporting.TextBox;
        textBox63.Value = stPro.InstallerNotes; /*stPro.notes;*/
        //textBox122.Value = stPro.InstallerNotes; /*stPro.notes;*/ /*stPro.InstallerNotes;*/
        ExportPdf(rpt);


    }

    public static void generate_solar_system(string ProjectID)
    {
        //System.Web.HttpContext.Current.Response.Write(ProjectID);
        //System.Web.HttpContext.Current.Response.End();
        DataTable dt1 = new DataTable();
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        string inverterdetail = string.Empty;
        dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.solar_system();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.TextBox textBox1 = rpt.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox2 = rpt.Items.Find("textBox2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox3 = rpt.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox4 = rpt.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox5 = rpt.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox6 = rpt.Items.Find("textBox6", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox7 = rpt.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;

        try
        {
            textBox1.Value = dt1.Rows[0]["ContFirst"].ToString();
            textBox2.Value = dt1.Rows[0]["ContLast"].ToString();
            textBox3.Value = dt1.Rows[0]["StreetAddress"].ToString();
            // textBox4.Value = dt1.Rows[0]["StreetAddress"].ToString();
            textBox4.Value = dt1.Rows[0]["InstallAddress"].ToString();
            textBox5.Value = dt1.Rows[0]["PostalCity"].ToString();
            textBox6.Value = dt1.Rows[0]["PostalState"].ToString();
            textBox7.Value = dt1.Rows[0]["PostalPostCode"].ToString();
            //System.Web.HttpContext.Current.Response.Write(dt1.Rows[0]["ContFirst"].ToString()+"--"+ dt1.Rows[0]["ContLast"].ToString());
            //System.Web.HttpContext.Current.Response.End();
        }
        catch { }

        ExportPdf(rpt);
    }

    public static void generate_WholesalePicklist(string WholesaleOrderID)
    {

        Sttbl_WholesaleOrders st = Clstbl_WholesaleOrders.tbl_WholesaleOrders_SelectByWholesaleOrderID(WholesaleOrderID);
        DataTable dt = Clstbl_WholesaleOrders.tbl_WholesaleOrderItems_Select_ByWholesaleOrderID(WholesaleOrderID);


        Telerik.Reporting.Report rpt = new EurosolarReporting.WholesalePicklist();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox textBox3 = rpt.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox7 = rpt.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox11 = rpt.Items.Find("textBox11", true)[0] as Telerik.Reporting.TextBox;

        try
        {
            try
            {
                textBox3.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(st.DateOrdered));
            }
            catch { }
            textBox7.Value = st.InvoiceNo;
            textBox11.Value = st.ReferenceNo;

            table3.DataSource = dt;
        }
        catch { }

        ExportPdf(rpt);
    }

    public static void generate_EWR(string ProjectID)
    {
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);
        SttblContacts stcont = ClstblContacts.tblContacts_SelectByCustomerID(stPro.CustomerID);
        DataTable dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        //To get installer eleclicense
        string InstallerContactId = stPro.Installer;
        DataTable dt2;
        if (string.IsNullOrEmpty(InstallerContactId))
        {
            InstallerContactId = "";
            dt2 = ClstblContacts.tblContacts_SelectElectricianByContactID(InstallerContactId);
        }
        else
        {
            dt2 = ClstblContacts.tblContacts_SelectElectricianByContactID(InstallerContactId);
        }



        Telerik.Reporting.Report rpt = new EurosolarReporting.EWR();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.TextBox txtcustname = rpt.Items.Find("txtcustname", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmobile = rpt.Items.Find("txtmobile", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstreetno = rpt.Items.Find("txtstreetno", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtaddress = rpt.Items.Find("txtaddress", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsuburb = rpt.Items.Find("txtsuburb", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsolarpanel = rpt.Items.Find("txtsolarpanel", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtinverter = rpt.Items.Find("txtinverter", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtinstaller = rpt.Items.Find("txtinstaller", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtaddress2 = rpt.Items.Find("txtaddress2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtcust2 = rpt.Items.Find("txtcust2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txt_elec_licence = rpt.Items.Find("txt_elec_licence", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmobile2 = rpt.Items.Find("txtmobile2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtday = rpt.Items.Find("txtday", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmonth = rpt.Items.Find("txtmonth", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtyear = rpt.Items.Find("txtyear", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtday2 = rpt.Items.Find("txtday2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmonth2 = rpt.Items.Find("txtmonth2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtyear2 = rpt.Items.Find("txtyear2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtprojectno = rpt.Items.Find("txtprojectno", true)[0] as Telerik.Reporting.TextBox;


        try
        {

            //System.Web.HttpContext.Current.Response.Write(string.Format("{0:yyyy}", Convert.ToDateTime(stPro.InstallBookingDate)));
            //System.Web.HttpContext.Current.Response.End();
            string day = "", month = "", year = "";
            if (!string.IsNullOrEmpty(stPro.InstallBookingDate))
            {
                try
                {
                    day = string.Format("{0:dd}", Convert.ToDateTime(stPro.InstallBookingDate));
                    month = string.Format("{0:MM}", Convert.ToDateTime(stPro.InstallBookingDate));
                    year = string.Format("{0:yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
                }
                catch { }
            }
            txtcustname.Value = st.Customer;
            txtmobile.Value = stcont.ContMobile;
            txtstreetno.Value = st.street_number;
            txtaddress.Value = st.street_name + " " + st.street_type + " " + st.StreetCity;
            txtsuburb.Value = st.StreetState + " " + st.StreetPostCode;
            txtsolarpanel.Value = stPro.SystemCapKW;
            txtinverter.Value = stPro.InverterOutput;
            txtinstaller.Value = dt.Rows[0]["InstallerName"].ToString();
            txtaddress2.Value = st.StreetAddress + " " + st.StreetState + " " + st.StreetPostCode;
            txtcust2.Value = dt.Rows[0]["InstallerName"].ToString();
            txtmobile2.Value = stcont.ContMobile;
            txtday.Value = day;
            txtday2.Value = day;
            txtmonth.Value = month;
            txtmonth2.Value = month;
            txtyear.Value = year;
            txtyear2.Value = year;
            txtprojectno.Value = stPro.ProjectNumber;
            if (dt2.Rows.Count > 0)
            {
                txt_elec_licence.Value = dt2.Rows[0]["ElecLicence"].ToString();
            }
            else
            {
                txt_elec_licence.Value = "";
            }

        }
        catch { }


        ExportPdf(rpt);
    }

    public static void generate_customeracknowledgement(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.Ncustomeracknowledgement();
        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox textBox44 = rpt.Items.Find("textBox48", true)[0] as Telerik.Reporting.TextBox;

        table1.DataSource = dt;
        try
        {
            textBox44.Value = dt.Rows[0]["Customer"].ToString();
        }
        catch { }

        Telerik.Reporting.TextBox textBox45 = rpt.Items.Find("textBox45", true)[0] as Telerik.Reporting.TextBox;
        try
        {
            textBox45.Value = string.Format("{0: dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
        }
        catch { }

        ExportPdf(rpt);
    }
    public static void generate_maintenance(string ProjectID, string ProjectMaintenanceID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notesMaintenance(ProjectID, ProjectMaintenanceID);
        SttblProjectMaintenance st = ClstblProjectMaintenance.tblProjectMaintenance_SelectByProjectMaintainanceByProjectID(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.Nmaintainance();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.TextBox lblinvoiceno = rpt.Items.Find("lblmaintainaceno", true)[0] as Telerik.Reporting.TextBox;
        lblinvoiceno.Value = "Maintenance Contract - " + stPro.InvoiceNumber;

        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;


        Telerik.Reporting.TextBox txtInstallBookingDate = rpt.Items.Find("txtInstallBookingDate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtMtceCost = rpt.Items.Find("txtMtceCost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtMtceDiscount = rpt.Items.Find("txtMtceDiscount", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtMtceBalance = rpt.Items.Find("txtMtceBalance", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox24 = rpt.Items.Find("textBox24", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox25 = rpt.Items.Find("textBox25", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox30 = rpt.Items.Find("textBox30", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox16 = rpt.Items.Find("textBox16", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox29 = rpt.Items.Find("textBox29", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox18 = rpt.Items.Find("textBox18", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox34 = rpt.Items.Find("textBox34", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox36 = rpt.Items.Find("textBox36", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox46 = rpt.Items.Find("textBox46", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox21 = rpt.Items.Find("textBox21", true)[0] as Telerik.Reporting.TextBox;

        table3.DataSource = dt;
        table4.DataSource = dt;
        table8.DataSource = dt;
        table6.DataSource = dt;
        table1.DataSource = dt;
        txtInstallBookingDate.Value = Convert.ToDateTime(stPro.InstallBookingDate).ToString("dd MMM yyyy");
        txtInstallBookingDate.Value = stPro.InstallBookingDate;
        if (stPro.InstallBookingDate == "" || stPro.InstallBookingDate == null)
        {
            txtInstallBookingDate.Value = "";
        }
        else
        {
            try
            {
                txtInstallBookingDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
            }
            catch { }
        }

        try
        {
            txtMtceCost.Value = SiteConfiguration.ChangeCurrency_Val(st.MtceCost);
        }
        catch { }

        try
        {
            txtMtceDiscount.Value = SiteConfiguration.ChangeCurrency_Val(st.MtceDiscount);
        }
        catch { }
        try
        {
            txtMtceBalance.Value = SiteConfiguration.ChangeCurrency_Val(st.MtceBalance);
        }
        catch { }
        try
        {
            textBox24.Value = stPro.ProjectNumber;
        }
        catch { }
        try
        {
            textBox25.Value = stPro.ManualQuoteNumber;
        }
        catch { }
        try
        {
            textBox30.Value = stPro.NextMaintenanceCall;
        }
        catch { }
        try
        {
            textBox16.Value = dt.Rows[0]["CustomerInput"].ToString();
        }
        catch { }
        try
        {
            textBox18.Value = dt.Rows[0]["ActionRequired"].ToString();
        }
        catch { }
        try
        {
            textBox29.Value = dt.Rows[0]["FaultIdentified"].ToString();
        }
        catch { }
        try
        {
            textBox34.Value = dt.Rows[0]["WorkDone"].ToString();
        }
        catch { }
        try
        {
            textBox36.Value = dt.Rows[0]["Contact"].ToString();
        }
        catch { }
        try
        {
            string panelbrand = dt.Rows[0]["PanelBrandName"].ToString();
            string panelqty = dt.Rows[0]["NumberPanels"].ToString();
            textBox46.Value = panelqty + "X" + panelbrand;
        }
        catch { }
        try
        {
            string inv1brand = dt.Rows[0]["InverterDetailsName"].ToString();
            string inv1qty = dt.Rows[0]["inverterqty"].ToString();
            string inv2brand = dt.Rows[0]["SecondInverterDetails"].ToString();
            string inv2qty = dt.Rows[0]["inverterqty"].ToString();
            textBox21.Value = inv1qty + "X" + inv1brand;
            if (!string.IsNullOrEmpty(inv2qty))
            {
                textBox21.Value = inv1qty + "X" + inv1brand + "+" + inv2qty + "X" + inv2brand;
            }
        }
        catch { }
        ExportPdf(rpt);

    }

    public static void generate_stcug(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(stPro.ContactID);
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(stPro.EmployeeID);
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);



        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.STCUGForm();

        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table10 = rpt.Items.Find("table10", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table11 = rpt.Items.Find("table11", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table12 = rpt.Items.Find("table12", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table13 = rpt.Items.Find("table13", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table15 = rpt.Items.Find("table15", true)[0] as Telerik.Reporting.Table;
        table1.DataSource = dt;
        table1.DataSource = dt;
        table2.DataSource = dt;
        table3.DataSource = dt;
        table6.DataSource = dt;
        table7.DataSource = dt;
        table8.DataSource = dt;
        table10.DataSource = dt;
        table11.DataSource = dt;
        table12.DataSource = dt;
        table13.DataSource = dt;
        table15.DataSource = dt;



        Telerik.Reporting.TextBox ProjectNumber3 = rpt.Items.Find("ProjectNumber3", true)[0] as Telerik.Reporting.TextBox;
        ProjectNumber3.Value = stPro.ProjectNumber;
        Telerik.Reporting.TextBox litPanelDetails = rpt.Items.Find("litPanelDetails", true)[0] as Telerik.Reporting.TextBox;
        litPanelDetails.Value = stPro.PanelDetails;

        Telerik.Reporting.TextBox lblInverterBrand2 = rpt.Items.Find("lblInverterBrand2", true)[0] as Telerik.Reporting.TextBox;
        lblInverterBrand2.Value = stPro.InverterBrand;

        Telerik.Reporting.TextBox lblInverterModel2 = rpt.Items.Find("lblInverterModel2", true)[0] as Telerik.Reporting.TextBox;
        lblInverterModel2.Value = stPro.InverterModel;

        Telerik.Reporting.TextBox lblInverterSeries2 = rpt.Items.Find("lblInverterSeries2", true)[0] as Telerik.Reporting.TextBox;
        lblInverterSeries2.Value = stPro.InverterSeries;



        Telerik.Reporting.TextBox lblProjectNumber5 = rpt.Items.Find("lblProjectNumber5", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblProjectNumber4 = rpt.Items.Find("lblProjectNumber4", true)[0] as Telerik.Reporting.TextBox;
        lblProjectNumber5.Value = lblProjectNumber5.Value = stPro.ProjectNumber;


        Telerik.Reporting.TextBox lblInstallerName7 = rpt.Items.Find("lblInstallerName7", true)[0] as Telerik.Reporting.TextBox;
        lblInstallerName7.Value = stPro.InstallerName;
        Telerik.Reporting.TextBox lblFinanceOption = rpt.Items.Find("lblFinanceOption", true)[0] as Telerik.Reporting.TextBox;
        lblFinanceOption.Value = stPro.FinanceWith;


        Telerik.Reporting.TextBox lblMeterPhase = rpt.Items.Find("lblMeterPhase", true)[0] as Telerik.Reporting.TextBox;
        if (stPro.MeterPhase == "1")
        {
            lblMeterPhase.Value = "Single";
        }
        if (stPro.MeterPhase == "2")
        {
            lblMeterPhase.Value = "Double";
        }
        if (stPro.MeterPhase == "3")
        {
            lblMeterPhase.Value = "Three";
        }

        Telerik.Reporting.TextBox lblOffPeak = rpt.Items.Find("lblOffPeak", true)[0] as Telerik.Reporting.TextBox;
        if (stPro.OffPeak == "True")
        {
            lblOffPeak.Value = "Yes";
        }
        else
        {
            lblOffPeak.Value = "No";
        }
        Telerik.Reporting.TextBox lblRetailer = rpt.Items.Find("lblRetailer", true)[0] as Telerik.Reporting.TextBox;
        lblRetailer.Value = stPro.ElecRetailer;

        Telerik.Reporting.TextBox lblPeakMeterNo = rpt.Items.Find("lblPeakMeterNo", true)[0] as Telerik.Reporting.TextBox;
        lblPeakMeterNo.Value = stPro.MeterNumber1;


        Telerik.Reporting.HtmlTextBox txtpaymnetfrm = rpt.Items.Find("txtpaymnetfrm", true)[0] as Telerik.Reporting.HtmlTextBox;
        txtpaymnetfrm.Value = "<b> Paymnet Form </b><br/>Project: " + stPro.ProjectNumber + "<br/>Customer: " + stCont.ContFirst + " " + stCont.ContLast + "<br/> Installation at: " + stPro.InstallAddress + ", " + stPro.InstallCity + ", " + stPro.InstallState + ", " + stPro.InstallPostCode;






        Telerik.Reporting.TextBox lblInstallBookingDate = rpt.Items.Find("lblInstallBookingDate", true)[0] as Telerik.Reporting.TextBox;
        try
        {
            lblInstallBookingDate.Value = string.Format("{0: dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));

        }
        catch { }
        ExportPdf(rpt);

    }
    public static void generate_STCform(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(stPro.ContactID);
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(stPro.EmployeeID);
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);



        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.STCFormNewImage();


        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table10 = rpt.Items.Find("table10", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table11 = rpt.Items.Find("table11", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table12 = rpt.Items.Find("table12", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table13 = rpt.Items.Find("table13", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table14 = rpt.Items.Find("table15", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table15 = rpt.Items.Find("table15", true)[0] as Telerik.Reporting.Table;
        //Change for New
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table9 = rpt.Items.Find("table9", true)[0] as Telerik.Reporting.Table;

        Telerik.Reporting.TextBox textBox56 = rpt.Items.Find("textBox56", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox128 = rpt.Items.Find("textBox128", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox140 = rpt.Items.Find("textBox140", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox146 = rpt.Items.Find("textBox146", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox153 = rpt.Items.Find("textBox153", true)[0] as Telerik.Reporting.TextBox;


        //Testing
        Telerik.Reporting.TextBox textBox102 = rpt.Items.Find("textBox102", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox99 = rpt.Items.Find("textBox99", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox97 = rpt.Items.Find("textBox97", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox94 = rpt.Items.Find("textBox94", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox92 = rpt.Items.Find("textBox92", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox90 = rpt.Items.Find("textBox90", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox91 = rpt.Items.Find("textBox91", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox87 = rpt.Items.Find("textBox87", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox84 = rpt.Items.Find("textBox84", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox78 = rpt.Items.Find("textBox78", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox82 = rpt.Items.Find("textBox82", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox81 = rpt.Items.Find("textBox81", true)[0] as Telerik.Reporting.TextBox;
        //

        Telerik.Reporting.Table table19 = rpt.Items.Find("table19", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox textBox25 = rpt.Items.Find("textBox25", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox52 = rpt.Items.Find("textBox52", true)[0] as Telerik.Reporting.TextBox;


        table19.DataSource = dt;
        try
        {
            textBox52.Value = dt.Rows[0]["Customer"].ToString();
        }
        catch { }

        Telerik.Reporting.TextBox textBox45 = rpt.Items.Find("textBox45", true)[0] as Telerik.Reporting.TextBox;
        try
        {
            textBox25.Value = string.Format("{0: dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));

        }
        catch { }



        if (st.ReceivedCredits == "True")
        {
            textBox128.Value = "YES";
        }
        else
        {
            textBox128.Value = "NO";
        }
        if (st.RebateApproved == "True")
        {
            textBox140.Value = "YES";
        }
        else
        {
            textBox140.Value = "NO";
        }
        if (st.CreditEligible == "True")
        {
            textBox146.Value = "YES";
        }
        else
        {
            textBox146.Value = "NO";
        }
        if (st.AdditionalSystem == "True")
        {
            textBox153.Value = "YES";
        }
        else
        {
            textBox153.Value = "NO";
        }

        //Change for New End
        table1.DataSource = dt;
        table2.DataSource = dt;
        table3.DataSource = dt;
        table6.DataSource = dt;
        table7.DataSource = dt;
        table8.DataSource = dt;
        table10.DataSource = dt;
        table11.DataSource = dt;
        table12.DataSource = dt;
        table13.DataSource = dt;
        //table14.DataSource = dt;
        //table15.DataSource = dt;
        //Change for New
        table4.DataSource = dt;
        table5.DataSource = dt;
        table9.DataSource = dt;

        //testing
        textBox102.Value = dt.Rows[0]["ContFirst"].ToString();
        textBox99.Value = dt.Rows[0]["ContLast"].ToString();
        textBox97.Value = dt.Rows[0]["StreetAddress"].ToString();
        textBox94.Value = dt.Rows[0]["StreetCity"].ToString();
        textBox92.Value = dt.Rows[0]["StreetPostCode"].ToString();
        textBox91.Value = dt.Rows[0]["InstallerName"].ToString();
        textBox90.Value = dt.Rows[0]["InstallerName"].ToString();
        textBox87.Value = dt.Rows[0]["Electricianmolbile"].ToString();
        textBox84.Value = dt.Rows[0]["ElecLicence"].ToString();
        try
        {
            textBox82.Value = string.Format("{0:dd}", Convert.ToDateTime(dt.Rows[0]["InstallBookingDate"].ToString()));
            textBox81.Value = string.Format("{0:MMM}", Convert.ToDateTime(dt.Rows[0]["InstallBookingDate"].ToString()));
            textBox78.Value = string.Format("{0:yyyy}", Convert.ToDateTime(dt.Rows[0]["InstallBookingDate"].ToString()));
            //

            textBox56.Value = string.Format("{0: dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
        }
        catch { }




        //Telerik.Reporting.TextBox ProjectNumber3 = rpt.Items.Find("ProjectNumber3", true)[0] as Telerik.Reporting.TextBox;
        //ProjectNumber3.Value = stPro.ProjectNumber;
        //Telerik.Reporting.TextBox litPanelDetails = rpt.Items.Find("litPanelDetails", true)[0] as Telerik.Reporting.TextBox;
        //litPanelDetails.Value = stPro.PanelDetails;

        //Telerik.Reporting.TextBox lblInverterBrand2 = rpt.Items.Find("lblInverterBrand2", true)[0] as Telerik.Reporting.TextBox;
        //lblInverterBrand2.Value = stPro.InverterBrand;

        //Telerik.Reporting.TextBox lblInverterModel2 = rpt.Items.Find("lblInverterModel2", true)[0] as Telerik.Reporting.TextBox;
        //lblInverterModel2.Value = stPro.InverterModel;

        //Telerik.Reporting.TextBox lblInverterSeries2 = rpt.Items.Find("lblInverterSeries2", true)[0] as Telerik.Reporting.TextBox;
        //lblInverterSeries2.Value = stPro.InverterSeries;



        //Telerik.Reporting.TextBox lblProjectNumber5 = rpt.Items.Find("lblProjectNumber5", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox lblProjectNumber4 = rpt.Items.Find("lblProjectNumber4", true)[0] as Telerik.Reporting.TextBox;
        //lblProjectNumber5.Value = lblProjectNumber5.Value = stPro.ProjectNumber;


        //Telerik.Reporting.TextBox lblInstallerName7 = rpt.Items.Find("lblInstallerName7", true)[0] as Telerik.Reporting.TextBox;
        //lblInstallerName7.Value = stPro.InstallerName;
        //Telerik.Reporting.TextBox lblFinanceOption = rpt.Items.Find("lblFinanceOption", true)[0] as Telerik.Reporting.TextBox;
        //lblFinanceOption.Value = stPro.FinanceWith;


        //Telerik.Reporting.TextBox lblMeterPhase = rpt.Items.Find("lblMeterPhase", true)[0] as Telerik.Reporting.TextBox;
        //if (stPro.MeterPhase == "1")
        //{
        //    lblMeterPhase.Value = "Single";
        //}
        //if (stPro.MeterPhase == "2")
        //{
        //    lblMeterPhase.Value = "Double";
        //}
        //if (stPro.MeterPhase == "3")
        //{
        //    lblMeterPhase.Value = "Three";
        //}

        //Telerik.Reporting.TextBox lblOffPeak = rpt.Items.Find("lblOffPeak", true)[0] as Telerik.Reporting.TextBox;
        //if (stPro.OffPeak == "True")
        //{
        //    lblOffPeak.Value = "Yes";
        //}
        //else
        //{
        //    lblOffPeak.Value = "No";
        //}
        //Telerik.Reporting.TextBox lblRetailer = rpt.Items.Find("lblRetailer", true)[0] as Telerik.Reporting.TextBox;
        //lblRetailer.Value = stPro.ElecRetailer;

        //Telerik.Reporting.TextBox lblPeakMeterNo = rpt.Items.Find("lblPeakMeterNo", true)[0] as Telerik.Reporting.TextBox;
        //lblPeakMeterNo.Value = stPro.MeterNumber1;


        //Telerik.Reporting.HtmlTextBox txtpaymnetfrm = rpt.Items.Find("txtpaymnetfrm", true)[0] as Telerik.Reporting.HtmlTextBox;
        //txtpaymnetfrm.Value = "<b> Paymnet Form </b><br/>Project: " + stPro.ProjectNumber + "<br/>Customer: " + stCont.ContFirst + " " + stCont.ContLast + "<br/> Installation at: " + stPro.InstallAddress + ", " + stPro.InstallCity + ", " + stPro.InstallState + ", " + stPro.InstallPostCode;


        //Telerik.Reporting.TextBox lblInstallBookingDate = rpt.Items.Find("lblInstallBookingDate", true)[0] as Telerik.Reporting.TextBox;
        //try
        //{
        //    lblInstallBookingDate.Value = string.Format("{0: dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));

        //}
        //catch { }


        ExportPdf(rpt);

    }
    //public static void generate_Testing(string ProjectID)
    //{
    //    DataTable dt = new DataTable();

    //    dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
    //    Telerik.Reporting.Report rpt = new EurosolarReporting.Testing_new();

    //    //Telerik.Reporting.Table table15 = rpt.Items.Find("table15", true)[0] as Telerik.Reporting.Table;
    //    //table15.DataSource = dt;
    //    Telerik.Reporting.TextBox textBox1 = rpt.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox textBox2 = rpt.Items.Find("textBox2", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox textBox3 = rpt.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox textBox4 = rpt.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox textBox5 = rpt.Items.Find("textBox5", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox textBox6 = rpt.Items.Find("textBox6", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox textBox7 = rpt.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox textBox8 = rpt.Items.Find("textBox8", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox textBox9 = rpt.Items.Find("textBox9", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox textBox10 = rpt.Items.Find("textBox10", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox textBox11 = rpt.Items.Find("textBox11", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox textBox12 = rpt.Items.Find("textBox12", true)[0] as Telerik.Reporting.TextBox;

    //    textBox1.Value = dt.Rows[0]["ContFirst"].ToString();
    //    textBox2.Value = dt.Rows[0]["ContLast"].ToString();
    //    textBox3.Value = dt.Rows[0]["StreetAddress"].ToString();
    //    textBox4.Value = dt.Rows[0]["StreetCity"].ToString();
    //    textBox5.Value = dt.Rows[0]["StreetPostCode"].ToString();
    //    textBox6.Value = dt.Rows[0]["InstallerName"].ToString();
    //    textBox7.Value = dt.Rows[0]["InstallerName"].ToString();
    //    textBox8.Value = dt.Rows[0]["Electricianmolbile"].ToString();
    //    textBox9.Value = dt.Rows[0]["ElecLicence"].ToString();
    //    try
    //    {
    //        textBox10.Value = string.Format("{0:dd}", Convert.ToDateTime(dt.Rows[0]["InstallBookingDate"].ToString()));
    //        textBox11.Value = string.Format("{0:MMM}", Convert.ToDateTime(dt.Rows[0]["InstallBookingDate"].ToString()));
    //        textBox12.Value = string.Format("{0:yyyy}", Convert.ToDateTime(dt.Rows[0]["InstallBookingDate"].ToString()));
    //    }
    //    catch { }


    //    ExportPdf(rpt);
    //}

    //public static void generate_mtceinvoice(string ProjectID, string SaveOrDownload)
    //{
    //    DataTable dt = new DataTable();
    //    DataTable dt1 = new DataTable();


    //    SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
    //    dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

    //    Telerik.Reporting.Report rpt = new EurosolarReporting.performamtceinvoice();
    //    Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

    //    Telerik.Reporting.TextBox lblinvoiceno = rpt.Items.Find("lblinvoiceno", true)[0] as Telerik.Reporting.TextBox;
    //    lblinvoiceno.Value = "Tax Invoice No: " + stPro.InvoiceNumber;
    //    try
    //    {
    //        Telerik.Reporting.TextBox lblSalesInvDate = rpt.Items.Find("lblSalesInvDate", true)[0] as Telerik.Reporting.TextBox;
    //        lblSalesInvDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.ToString()));
    //    }
    //    catch { }

    //    Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
    //    Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;

    //    table3.DataSource = dt;
    //    table8.DataSource = dt;

    //    Telerik.Reporting.TextBox lblInverterDetails = rpt.Items.Find("lblInverterDetails", true)[0] as Telerik.Reporting.TextBox;
    //    lblInverterDetails.Value = stPro.InverterDetails;



    //    //********************  amount ****************

    //    Telerik.Reporting.TextBox lblTotalQuotePrice = rpt.Items.Find("lblTotalQuotePrice", true)[0] as Telerik.Reporting.TextBox;
    //    // Telerik.Reporting.TextBox lblNetCost = rpt.Items.Find("lblNetCost", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox lblDepositPaid = rpt.Items.Find("lblDepositPaid", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox lblBalanceDue = rpt.Items.Find("lblBalanceDue", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox lblCommissionGST = rpt.Items.Find("lblCommissionGST", true)[0] as Telerik.Reporting.TextBox;





    //    try
    //    {
    //        decimal newtotalcost = Convert.ToDecimal(stPro.RECRebate) + Convert.ToDecimal(stPro.TotalQuotePrice);
    //        try
    //        {
    //            lblTotalQuotePrice.Value = "$" + SiteConfiguration.ChangeCurrencyVal(Convert.ToString(newtotalcost));
    //        }
    //        catch { }
    //    }
    //    catch { }

    //    DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
    //    decimal paiddate = 0;
    //    decimal balown = 0;
    //    if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
    //    {
    //        paiddate = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
    //        balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(paiddate);
    //        try
    //        {
    //            lblDepositPaid.Value = "$" + SiteConfiguration.ChangeCurrencyVal(Convert.ToString(paiddate));
    //        }
    //        catch { }
    //        try
    //        {
    //            lblBalanceDue.Value = "$" + SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balown));
    //        }
    //        catch { }
    //    }
    //    else
    //    {
    //        lblDepositPaid.Value = "0";
    //        try
    //        {
    //            lblBalanceDue.Value = "$" + SiteConfiguration.ChangeCurrencyVal(stPro.TotalQuotePrice);
    //        }
    //        catch { }
    //    }
    //    try
    //    {
    //        lblCommissionGST.Value = "Note: The Net Cost above includes GST of: $" + SiteConfiguration.ChangeCurrencyVal(stPro.InvoiceGST);
    //    }
    //    catch { }

    //    if (SaveOrDownload == "Download")
    //    {
    //        ExportPdf(rpt);
    //    }
    //    else if (SaveOrDownload == "Save")
    //    {
    //        SavePDF2(rpt, stPro.ProjectNumber + "_TaxInvoice.pdf", "SavePDF2");
    //    }
    //}
    public static void generate_performamtceinvoice(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.performamtceinvoice();
        Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();

        Telerik.Reporting.TextBox lblinvoiceno = rpt.Items.Find("lblinvoiceno", true)[0] as Telerik.Reporting.TextBox;
        lblinvoiceno.Value = "Tax Invoice No: " + stPro.InvoiceNumber;
        try
        {
            Telerik.Reporting.TextBox lblSalesInvDate = rpt.Items.Find("lblSalesInvDate", true)[0] as Telerik.Reporting.TextBox;
            lblSalesInvDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.ToString()));
        }
        catch { }

        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;

        table3.DataSource = dt;
        table8.DataSource = dt;

        Telerik.Reporting.TextBox lblInverterDetails = rpt.Items.Find("lblInverterDetails", true)[0] as Telerik.Reporting.TextBox;
        lblInverterDetails.Value = stPro.InverterDetails;



        //********************  amount ****************

        Telerik.Reporting.TextBox lblTotalQuotePrice = rpt.Items.Find("lblTotalQuotePrice", true)[0] as Telerik.Reporting.TextBox;
        // Telerik.Reporting.TextBox lblNetCost = rpt.Items.Find("lblNetCost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblDepositPaid = rpt.Items.Find("lblDepositPaid", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblBalanceDue = rpt.Items.Find("lblBalanceDue", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox lblCommissionGST = rpt.Items.Find("lblCommissionGST", true)[0] as Telerik.Reporting.TextBox;





        try
        {
            decimal newtotalcost = Convert.ToDecimal(stPro.RECRebate) + Convert.ToDecimal(stPro.TotalQuotePrice);
            try
            {
                lblTotalQuotePrice.Value = "$" + SiteConfiguration.ChangeCurrencyVal(Convert.ToString(newtotalcost));
            }
            catch { }
        }
        catch { }

        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        decimal paiddate = 0;
        decimal balown = 0;
        if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
        {
            paiddate = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
            balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(paiddate);
            try
            {
                lblDepositPaid.Value = "$" + SiteConfiguration.ChangeCurrencyVal(Convert.ToString(paiddate));
            }
            catch { }
            try
            {
                lblBalanceDue.Value = "$" + SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balown));
            }
            catch { }
        }
        else
        {
            lblDepositPaid.Value = "0";
            try
            {
                lblBalanceDue.Value = "$" + SiteConfiguration.ChangeCurrencyVal(stPro.TotalQuotePrice);
            }
            catch { }
        }
        try
        {
            lblCommissionGST.Value = "Note: The Net Cost above includes GST of: $" + SiteConfiguration.ChangeCurrencyVal(stPro.InvoiceGST);
        }
        catch { }

        ExportPdf(rpt);






    }

    public static void generate_welcomelettervic(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(stPro.ContactID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.welcomelettervic();

        Telerik.Reporting.TextBox lblContact = rpt.Items.Find("lblContact", true)[0] as Telerik.Reporting.TextBox;
        lblContact.Value = stCont.ContFirst + " " + stCont.ContLast;
        ExportPdf(rpt);
    }

    //public static void generate_performainvoice(string ProjectID)
    //{
    //    DataTable dt = new DataTable();
    //    DataTable dt1 = new DataTable();


    //    SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
    //    dt = ClstblProjects.tblProjects_SelectByProjectID_picklist(ProjectID);

    //    Telerik.Reporting.Report rpt = new EurosolarReporting.perfome_invoice_new();

    //    Telerik.Reporting.TextBox lblinvoiceno = rpt.Items.Find("lblinvoiceno", true)[0] as Telerik.Reporting.TextBox;
    //    lblinvoiceno.Value = "Performa Invoice - " + stPro.InvoiceNumber;
    //    try
    //    {
    //        Telerik.Reporting.TextBox lblSalesInvDate = rpt.Items.Find("lblSalesInvDate", true)[0] as Telerik.Reporting.TextBox;
    //        lblSalesInvDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.ToString()));
    //    }
    //    catch { }

    //    Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
    //    Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;

    //    Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;

    //    table3.DataSource = dt;
    //    table4.DataSource = dt;
    //    //table5.DataSource = dt;
    //    table8.DataSource = dt;

    //    Telerik.Reporting.TextBox textBox20 = rpt.Items.Find("textBox20", true)[0] as Telerik.Reporting.TextBox;

    //    ////==================================================
    //    if (stPro.InverterDetailsID != "")
    //    {

    //        textBox20.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";

    //    }
    //    if (stPro.SecondInverterDetailsID != "0")
    //    {
    //        textBox20.Value = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";

    //    }
    //    if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "0")
    //    {
    //        textBox20.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";

    //    }


    //    ////********************  amount ****************
    //    Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
    //    Telerik.Reporting.TextBox lblTotalQuotePrice = rpt.Items.Find("lblTotalQuotePrice", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox lblNetCost = rpt.Items.Find("lblNetCost", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox lblDepositPaid = rpt.Items.Find("lblDepositPaid", true)[0] as Telerik.Reporting.TextBox;

    //    Telerik.Reporting.TextBox lblCommissionGST = rpt.Items.Find("lblCommissionGST", true)[0] as Telerik.Reporting.TextBox;

    //    //Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
    //    Telerik.Reporting.TextBox lblBalanceDue = rpt.Items.Find("lblBalanceDue", true)[0] as Telerik.Reporting.TextBox;
    //    try
    //    {
    //        decimal newtotalcost = Convert.ToDecimal(stPro.RECRebate) + Convert.ToDecimal(stPro.TotalQuotePrice);
    //        try
    //        {
    //            lblTotalQuotePrice.Value = "$" + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(newtotalcost));
    //        }
    //        catch { }
    //    }
    //    catch { }

    //    try
    //    {
    //        lblNetCost.Value = "$" + SiteConfiguration.ChangeCurrency_Val(stPro.TotalQuotePrice);
    //    }
    //    catch { }
    //    DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
    //    decimal paiddate = 0;
    //    decimal balown = 0;
    //    if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
    //    {
    //        paiddate = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
    //        balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(paiddate);
    //        try
    //        {
    //            lblDepositPaid.Value = "$" + SiteConfiguration.ChangeCurrency_Val(paiddate.ToString());
    //        }
    //        catch { }
    //        try
    //        {
    //            lblBalanceDue.Value = "$" + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(balown));
    //        }
    //        catch { }
    //    }
    //    else
    //    {
    //        lblDepositPaid.Value = "0";
    //        try
    //        {
    //            lblBalanceDue.Value = "$" + SiteConfiguration.ChangeCurrency_Val(stPro.TotalQuotePrice);
    //        }
    //        catch { }
    //    }

    //    decimal invamnt = 0;

    //    try
    //    {

    //        invamnt = (Convert.ToDecimal(stPro.TotalQuotePrice) * Convert.ToDecimal(0.9091));

    //    }
    //    catch { }
    //    //if (stEmp.GSTPayment == "True")
    //    {
    //        decimal GST = 0;
    //        try
    //        {

    //            GST = ((invamnt) * 10) / 100;
    //        }
    //        catch { }


    //        lblCommissionGST.Value = "$" + SiteConfiguration.ChangeCurrency_Val(Convert.ToString(GST));
    //    }


    //    ExportPdf(rpt);
    //}





    //public static void generate_refundform(string ProjectID, string RefundID)
    //{
    //    SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
    //    string inverterdetail = string.Empty;

    //    DataTable dt = Clscustomerlead.tblCustomers_SelectByCustomerID_telerikdemo(stPro.CustomerID);
    //    // dt1 = Clscustomerlead.tblProjects_SelectByProjectID_demotelerik(ProjectID);
    //    DataTable dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist2(ProjectID, RefundID);
    //    SttblProjectRefund st = ClstblProjectRefund.tblProjectRefund_SelectByProjectID(ProjectID);
    //    SttblProjectRefund stRefund = ClstblProjectRefund.tblProjectRefund_SelectByRefundID(RefundID);

    //    Telerik.Reporting.Report rpt = new EurosolarReporting.RefundForm();
    //    Telerik.Reporting.InstanceReportSource ir = new Telerik.Reporting.InstanceReportSource();
    //    Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
    //    Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
    //    // Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
    //    // Telerik.Reporting.Table table5 = rpt.Items.Find("tabl5", true)[0] as Telerik.Reporting.Table;
    //    //Telerik.Reporting.Table tbl_sitedetail = rpt.Items.Find("tbl_sitedetail", true)[0] as Telerik.Reporting.Table;

    //    //table1.DataSource = dt1;
    //   // table4.DataSource = dt;
    //    //table1.DataSource = dt1;
    //    //tbl_sitedetail.DataSource = dt1;

    //    table2.DataSource = dt1;
    //    table6.DataSource = dt1;
    //    Telerik.Reporting.TextBox txtSTATUS = rpt.Items.Find("txtSTATUS", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtNOOFPANELS = rpt.Items.Find("txtNOOFPANELS", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtAPPLICATION = rpt.Items.Find("txtAPPLICATION", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtDEPOSITDATE = rpt.Items.Find("txtDEPOSITDATE", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtENTRYDATE = rpt.Items.Find("txtENTRYDATE", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtPROMOTION = rpt.Items.Find("txtPROMOTION", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtBANKNAME = rpt.Items.Find("txtBANKNAME", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtBSBNO = rpt.Items.Find("txtBSBNO", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtACCNO = rpt.Items.Find("txtACCNO", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtAMOUNT = rpt.Items.Find("txtAMOUNT", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtACCOUNTNAME = rpt.Items.Find("txtACCOUNTNAME", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtREFUNDDATE = rpt.Items.Find("txtREFUNDDATE", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtSIGNATURE = rpt.Items.Find("txtSIGNATURE", true)[0] as Telerik.Reporting.TextBox;
    //    Telerik.Reporting.TextBox txtProjectNumber = rpt.Items.Find("txtProjectNumber", true)[0] as Telerik.Reporting.TextBox;



    //    Telerik.Reporting.TextBox txtCancleReason = rpt.Items.Find("txtCancleReason", true)[0] as Telerik.Reporting.TextBox;
    //    txtProjectNumber.Value = stPro.ProjectNumber; 
    //    txtBSBNO.Value = stRefund.BSBNo;
    //    txtACCNO.Value = stRefund.AccNo;
    //    txtAMOUNT.Value = stRefund.Amount;
    //    txtACCOUNTNAME.Value = stRefund.AccName;
    //    txtBANKNAME.Value = stRefund.BankName;

    //    if (stRefund.AccActionDate == "" || stRefund.AccActionDate == null)
    //    {
    //        txtREFUNDDATE.Value = "";
    //    }
    //    else
    //    {

    //        txtREFUNDDATE.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stRefund.AccActionDate));
    //    }
    //    txtCancleReason.Value = stRefund.Notes;


    //    if (stRefund.Status == "1")
    //    {
    //        txtSTATUS.Value = "Successfull";
    //    }
    //    else
    //    {
    //        txtSTATUS.Value = "Failed";
    //    }
    //    txtNOOFPANELS.Value = stPro.NumberPanels;
    //    if (stRefund.CreateDate == "" || stRefund.CreateDate == null)
    //    {
    //        txtENTRYDATE.Value = "";
    //    }
    //    else
    //    {

    //        txtENTRYDATE.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stRefund.CreateDate));
    //    }
    //    if (stPro.DepositReceived == "" || stPro.DepositReceived == null)
    //    {
    //        txtDEPOSITDATE.Value = "";
    //    }
    //    else
    //    {

    //        txtDEPOSITDATE.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.DepositReceived));
    //    }


    //    ExportPdf(rpt);

    //}


    public static void ExportPdf(Telerik.Reporting.Report rpt)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        string fileName = result.DocumentName + "." + result.Extension;

        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = result.MimeType;
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.Private);
        HttpContext.Current.Response.Expires = -1;
        HttpContext.Current.Response.Buffer = true;

        HttpContext.Current.Response.AddHeader("Content-Disposition",
                           string.Format("{0};FileName=\"{1}\"",
                                         "attachment",
                                         fileName));

        HttpContext.Current.Response.BinaryWrite(result.DocumentBytes);

        //HttpContext.Current.Response.End();
    }

    public static void SavePDFForSQ(Telerik.Reporting.Report rpt, string FileName)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        Random rnd = new Random();

        //string fileName = QuoteID + result.DocumentName + "." + result.Extension;
        string fileName = FileName;

        string ExcelFilename = fileName;

        string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\SQ\\", ExcelFilename);

        //FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
        //fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
        //fs.Close();
        //SiteConfiguration.UploadPDFFile("SQ", fileName);
        //SiteConfiguration.deleteimage(fileName, "SQ");

        PdfDocument doc = new PdfDocument(result.DocumentBytes);
        doc.FileInfo.IncrementalUpdate = false;
        foreach (PdfPageBase page in doc.Pages)
        {
            if (page != null)
            {
                if (page.ImagesInfo != null)
                {
                    foreach (PdfImageInfo info in page.ImagesInfo)
                    {
                        page.TryCompressImage(info.Index);
                    }
                }
            }
        }
        doc.SaveToFile(fullPath);
        SiteConfiguration.UploadPDFFile("SQ", fileName);
        SiteConfiguration.deleteimage(fileName, "SQ");

    }

    public static void SavePDF2(Telerik.Reporting.Report rpt, string FileName, string FolderName)
    {
        Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
        Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

        instanceReportSource.ReportDocument = rpt;

        Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

        Random rnd = new Random();

        string fileName = FileName;

        string ExcelFilename = fileName;

        string fullPath = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath + "\\userfiles\\" + FolderName + "\\", ExcelFilename);

        FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
        fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
        fs.Close();

        //PdfDocument doc = new PdfDocument(result.DocumentBytes);
        //doc.FileInfo.IncrementalUpdate = false;
        //foreach (PdfPageBase page in doc.Pages)
        //{
        //    if (page != null)
        //    {
        //        if (page.ImagesInfo != null)
        //        {
        //            foreach (PdfImageInfo info in page.ImagesInfo)
        //            {
        //                page.TryCompressImage(info.Index);
        //            }
        //        }
        //    }
        //}
        //doc.SaveToFile(fullPath);
    }

    public static void generate_performainvoicenew(string ProjectID, String SaveOrDownload)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        Telerik.Reporting.Report rpt = new EurosolarReporting.PerformaInvoice();

        rpt.DocumentName = "PERFORMA INV" + stPro.InvoiceNumber;


        //Telerik.Reporting.TextBox txtheader = rpt.Items.Find("txtinvoiceno", true)[0] as Telerik.Reporting.TextBox;
        //txtheader.Value = stPro.InvoiceNumber;
        try
        {
            Telerik.Reporting.TextBox txtinvoicenum = rpt.Items.Find("txtinvoicenum", true)[0] as Telerik.Reporting.TextBox;
            txtinvoicenum.Value = stPro.InvoiceNumber;
        }
        catch
        { }
        try
        {
            Telerik.Reporting.TextBox textBox1 = rpt.Items.Find("txtinvoicedate", true)[0] as Telerik.Reporting.TextBox;
            textBox1.Value = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(stPro.InvoiceSent));
        }
        catch { }
        //try
        //{
        //    Telerik.Reporting.TextBox lblSalesInvDate = rpt.Items.Find("lblSalesInvDate", true)[0] as Telerik.Reporting.TextBox;
        //    lblSalesInvDate.Value = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(DateTime.Now.ToString()));
        //}
        //catch { }

        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox txtinverters = rpt.Items.Find("txtinverters", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtpanel = rpt.Items.Find("txtpanel", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtsysSize = rpt.Items.Find("txtsys", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstcince = rpt.Items.Find("txtstcince", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtvicrebate = rpt.Items.Find("txtvicrebate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtvicloanDisc = rpt.Items.Find("txtvicloanDisc", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtnetcost = rpt.Items.Find("txtnetcost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdepositepaid = rpt.Items.Find("txtdepositepaid", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtbaldue = rpt.Items.Find("txtbaldue", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txttotal = rpt.Items.Find("txttotal", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtnote = rpt.Items.Find("txtnote", true)[0] as Telerik.Reporting.TextBox;

        table4.DataSource = dt;
        table5.DataSource = dt;
        table6.DataSource = dt;
        table7.DataSource = dt;
        if (dt.Rows[0]["SystemCapKw"].ToString() != null && dt.Rows[0]["SystemCapKw"].ToString() != "")
        {
            txtsysSize.Value = dt.Rows[0]["SystemCapKw"].ToString() + " KW";
        }
        else
        {
            txtsysSize.Value = dt1.Rows[0]["SystemCapKw"].ToString() + " KW";
        }
        if (stPro.InverterDetailsID != "")
        {
            txtinverters.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";
        }
        if (stPro.SecondInverterDetailsID != "0")
        {
            txtinverters.Value = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";
        }
        if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "0")
        {
            txtinverters.Value = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";

        }
        txtpanel.Value = dt.Rows[0]["NumberPanels"].ToString() + " X " + dt.Rows[0]["PanelOutput"].ToString() + " " + dt.Rows[0]["PanelStockName"].ToString();
        decimal since = 0, ViCLoanval = 0, VicLoanDiscval = 0, Total = 0, VicRebate = 0;
        try
        {
            if (dt.Rows[0]["RECRebate"].ToString() != null && dt.Rows[0]["RECRebate"].ToString() != "")
            {
                since = decimal.Parse(dt.Rows[0]["RECRebate"].ToString());
                decimal since1 = Math.Round(since, 2);
                txtstcince.Value = "$ " + since1;
            }
            else
            {
                txtstcince.Value = "$ " + "0.0";
            }
        }
        catch (Exception Ex)
        {

        }
        if (stPro.VicRebate1 != null && stPro.VicRebate1 != "")
        {
            VicRebate = decimal.Parse(stPro.VicRebate1);
            decimal vi = Math.Round(VicRebate, 2);
            VicRebate = vi;
            txtvicrebate.Value = "$ " + Convert.ToString(VicRebate);
        }
        else
        {
            txtvicrebate.Value = "$ " + "0.00";
        }
        //if (stPro.ViCLoan != null && stPro.ViCLoan != "")
        //{
        //    ViCLoanval = decimal.Parse(stPro.ViCLoan);
        //    decimal pac1 = Math.Round(ViCLoanval, 2);
        //    txtvicrebate.Value = "$ " + Convert.ToString(pac1);
        //}

        if (stPro.VicLoanDisc != null && stPro.VicLoanDisc != "")
        {
            VicLoanDiscval = decimal.Parse(stPro.VicLoanDisc);
            decimal pac1 = Math.Round(VicLoanDiscval, 2);
            txtvicloanDisc.Value = "$ " + Convert.ToString(pac1);
        }
        else
        {
            txtvicloanDisc.Value = "$ " + "0.00";
        }
        decimal netcost = 0, DepositReq = 0, netcost1 = 0, TotalCost = 0;
        netcost = decimal.Parse(stPro.TotalQuotePrice);
        if (dtCount != null && dtCount.Rows.Count > 0)
        {
            if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != null && dtCount.Rows[0]["InvoicePayTotal"].ToString() != "")
            {
                DepositReq = decimal.Parse(dtCount.Rows[0]["InvoicePayTotal"].ToString());
                decimal pac1 = Math.Round(DepositReq, 2);
                txtdepositepaid.Value = "$ " + Convert.ToString(pac1);
            }
            else
            {
                txtdepositepaid.Value = "$ " + "0.00";
            }
        }
        else
        {
            txtdepositepaid.Value = "$ " + "0.00";
        }

        if (stPro.TotalQuotePrice != null && stPro.TotalQuotePrice != "")
        {
            netcost1 = decimal.Parse(stPro.TotalQuotePrice);
            TotalCost = decimal.Parse(stPro.TotalQuotePrice);
        }
        // txttotal.Value = "$ " +Math.Round((since+ ViCLoanval+ VicLoanDiscval+ netcost),2);
        txttotal.Value = "$ " + Math.Round((since + netcost1), 2);

        Total = Math.Round((since + netcost1), 2);
        if (stPro.TotalQuotePrice != null && stPro.TotalQuotePrice != "")
        {
            netcost = decimal.Parse(stPro.PreviousTotalQuotePrice);
            decimal oth = since + VicRebate + VicLoanDiscval;
            decimal netCost3 = Total - oth;
            ///decimal netCost3 = netcost1 - DepositReq;
            decimal pac1 = Math.Round(netCost3, 2);
            txtnetcost.Value = "$ " + Convert.ToString(pac1);
            decimal BalDue = Math.Round((netcost1 - DepositReq), 2);
            txtbaldue.Value = "$ " + Convert.ToString(BalDue);
            txtnote.Value = "The Net Cost above includes GST of: $ " + Math.Round((netCost3 / 11), 2);
        }
        else
        {
            txtnetcost.Value = "$ " + "0.00";
            txtbaldue.Value = "$ " + "0.00";
        }


        //if (SaveOrDownload == "Save")
        //{
        //    SavePDF2(rpt, stPro.ProjectNumber + "_PERFORMAINV.pdf", "SavePDF2");
        //}
        // else 
        if (SaveOrDownload == "Download")
        {
            ExportPdf(rpt);
        }
    }
    public static void generate_welcomeletter(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
        dt = Clscustomerlead.tblCustomers_SelectByCustomerID_telerikdemo(stPro.CustomerID);

        dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.new_welcomletter1();


        Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table_Contact = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table2 = rpt.Items.Find("tbl_sitedetail", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;

        table1.DataSource = dt1;
        table_Contact.DataSource = dt1;
        // table2.DataSource = dt1;
        table3.DataSource = dt1;
        table4.DataSource = dt1;
        table5.DataSource = dt1;
        table6.DataSource = dt1;
        table7.DataSource = dt1;
        table8.DataSource = dt1;

        //Telerik.Reporting.TextBox txtInverterdetail = rpt.Items.Find("txtInverterdetail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtInverterdetail1 = rpt.Items.Find("txtInverterdetail1", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtbasiccost = rpt.Items.Find("txtbasiccost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtreq = rpt.Items.Find("txtreq", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtcompl = rpt.Items.Find("txtcompl", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtextra = rpt.Items.Find("txtextra", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtfullretail = rpt.Items.Find("txtfullretail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdiscount2 = rpt.Items.Find("txtdiscount2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstc = rpt.Items.Find("txtstc", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtgst = rpt.Items.Find("txtgst", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdeposit = rpt.Items.Find("txtdeposit", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtlessdeposit = rpt.Items.Find("txtlessdeposit", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmeter = rpt.Items.Find("txtmeter", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtswitch = rpt.Items.Find("txtswitch", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtmeterphase = rpt.Items.Find("txtmeterphase", true)[0] as Telerik.Reporting.TextBox;
        // Telerik.Reporting.TextBox txtcustname = rpt.Items.Find("txtcustname", true)[0] as Telerik.Reporting.TextBox;
        // Telerik.Reporting.TextBox txtgetdate = rpt.Items.Find("txtgetdate", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtProjectNumber = rpt.Items.Find("txtProjectNumber", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtpaymentplan = rpt.Items.Find("txtpaymentplan", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtotherdetail = rpt.Items.Find("txtotherdetail", true)[0] as Telerik.Reporting.TextBox;
        // Telerik.Reporting.TextBox textBox214 = rpt.Items.Find("textBox214", true)[0] as Telerik.Reporting.TextBox;
        // Telerik.Reporting.TextBox txtABN = rpt.Items.Find("textBox215", true)[0] as Telerik.Reporting.TextBox;
        txtProjectNumber.Value = stPro.ProjectNumber;

        string inverterdetail = string.Empty;
        string lblbasiccost = string.Empty;
        string lblreq = "+ N/A";
        string lblcompl = "+ N/A";
        string lbltax = string.Empty;
        string lblfullretail = string.Empty;
        string lbldiscount2 = string.Empty;
        string lblstc = string.Empty;
        string lbltotalcost = string.Empty;
        string lbldeposit = string.Empty;
        string lbllessdeposit = string.Empty;
        string otherdetail = string.Empty;
        string paymentplan = string.Empty;



        //==================================================
        try
        {
            if (dt1.Rows[0]["FinanceWith"].ToString() != string.Empty)
            {
                paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString();//
                if (st2.FinanceWithDeposit != string.Empty)
                {
                    paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString() + " with " + (Convert.ToInt32(st2.FinanceWithDeposit) / 100).ToString() + "%";
                    if (dt1.Rows[0]["PaymentType"].ToString() != string.Empty)
                    {
                        paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString() + " with " + (Convert.ToInt32(st2.FinanceWithDeposit) / 100).ToString() + "%" + " for " + dt1.Rows[0]["PaymentType"].ToString();

                    }
                }

            }

            //txtpaymentplan.Value = paymentplan;

            //if (st2.PromoText != string.Empty)
            //{
            //    textBox214.Visible = true;
            //    otherdetail = st2.PromoText;
            //}
            //else
            //{
            //    textBox214.Visible = false;
            //}

            txtotherdetail.Value = otherdetail;
        }
        catch
        {

        }
        if (stPro.InverterDetailsID != "")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";

        }
        if (stPro.SecondInverterDetailsID != "0")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";

        }
        if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "0")
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";

        }
        if ((stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID == "0"))
        {
            inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. ";
        }
        if (stPro.ServiceValue != string.Empty || stPro.ServiceValue != "")
        {
            lblbasiccost = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.ServiceValue) + Convert.ToDecimal(stPro.RECRebate)).ToString());
        }
        else
        {
            lblbasiccost = "0";
        }

        lbltax = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarMeterInstallation) + Convert.ToDecimal(stPro.VarMeterUG) + Convert.ToDecimal(stPro.VarAsbestos) + Convert.ToDecimal(stPro.VarSplitSystem) + Convert.ToDecimal(stPro.VarCherryPicker) + Convert.ToDecimal(stPro.VarTravelTime) + Convert.ToDecimal(stPro.VarOther)).ToString());
        if (lbltax == string.Empty)
        {
            lbltax = "0";
        }
        try
        {
            lblfullretail = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lblbasiccost) + Convert.ToDecimal(lbltax)).ToString());
        }
        catch { }
        if (stPro.SpecialDiscount != string.Empty || stPro.SpecialDiscount != "")
        {
            lbldiscount2 = SiteConfiguration.ChangeCurrency_Val(stPro.SpecialDiscount.ToString());
        }
        else
        {
            lbldiscount2 = "0";
        }
        if (stPro.RECRebate != string.Empty || stPro.RECRebate != "")
        {
            lblstc = SiteConfiguration.ChangeCurrency_Val(stPro.RECRebate.ToString());
        }
        else
        {
            lblstc = "0";
        }
        try
        {
            string cost = (Convert.ToDecimal(lblfullretail) - Convert.ToDecimal(lbldiscount2) - Convert.ToDecimal(lblstc)).ToString();
            //String.Format("${0:#.##}", x)
            lbltotalcost = string.Format("{0:0.00}", Convert.ToDecimal(cost));
        }
        catch { }

        if (stPro.DepositRequired != string.Empty || stPro.DepositRequired != "")
        {
            lbldeposit = SiteConfiguration.ChangeCurrency_Val(stPro.DepositRequired.ToString());
        }
        else
        {
            lbldeposit = "0";
        }
        try
        {
            lbllessdeposit = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lbltotalcost) - Convert.ToDecimal(lbldeposit)).ToString());
        }
        catch { }

        txtmeterphase.Value = stPro.NMINumber;

        txtmeter.Value = "Meter Phase : " + stPro.MeterPhase;
        txtswitch.Value = "Switchboard Upgrades : " + stPro.meterupgrade;

        //txtInverterdetail.Value = inverterdetail.ToString();
        txtInverterdetail1.Value = inverterdetail.ToString();
        txtbasiccost.Value = lblbasiccost.ToString();
        txtextra.Value = "+ " + lbltax.ToString();
        txtreq.Value = lblreq.ToString();
        txtcompl.Value = lblcompl.ToString();
        txtfullretail.Value = lblfullretail.ToString();
        txtdiscount2.Value = "- " + lbldiscount2.ToString();
        txtstc.Value = "- " + lblstc.ToString();
        txtgst.Value = lbltotalcost.ToString();
        txtdeposit.Value = lbldeposit.ToString();
        txtlessdeposit.Value = lbllessdeposit.ToString();
        // txtcustname.Value = stPro.Customer;
        // txtgetdate.Value = (DateTime.Now.AddHours(14).ToString(("dd-MMM-yyyy")));


        try
        {



            Telerik.Reporting.PictureBox picbxnearby = rpt.Items.Find("picbxnearby", true)[0] as Telerik.Reporting.PictureBox;

            if (!string.IsNullOrEmpty(stPro.nearmapdoc))
            {
                string pdfURL = ConfigurationManager.AppSettings["cdnURL"];

                string img1 = pdfURL + "NearMap/" + stPro.nearmapdoc;
                //Image myImg1 = Image.FromFile(img1);
                picbxnearby.Value = pdfURL + "NearMap/" + stPro.nearmapdoc;
            }
            else
            {
                picbxnearby.Visible = false;
                picbxnearby.Value = null;
            }
        }
        catch { }




        //try
        //{
        //    string ABN = dt.Rows[0]["CustWebSiteLink"].ToString();


        //    if (ABN != "" && ABN != "0" && ABN != null)
        //    {
        //        txtABN.Value = ABN;
        //    }
        //    //else
        //    //{
        //    //    txtABN.Value = "";
        //    //}
        //}
        //catch { }
        ExportPdf(rpt);
    }
    public static void generate_welcomeletternew(string ProjectID)
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();


        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ProjectID);
        dt = Clscustomerlead.tblCustomers_SelectByCustomerID_telerikdemo(stPro.CustomerID);

        dt1 = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);

        Telerik.Reporting.Report rpt = new EurosolarReporting.Welcomeletternew();


        Telerik.Reporting.Table tblcustomer = rpt.Items.Find("tblcustomer", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table tblsales = rpt.Items.Find("tblsales", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table tblsitedetail = rpt.Items.Find("tblsitedetail", true)[0] as Telerik.Reporting.Table;

        Telerik.Reporting.TextBox txtProjectNumber = rpt.Items.Find("txtProjectNumber", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.Table tblsitedetails1 = rpt.Items.Find("tblsitedetails1", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table tblsitedetails2 = rpt.Items.Find("tblsitedetails2", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.Table tblsysinsdetail = rpt.Items.Find("tblsysinsdetail", true)[0] as Telerik.Reporting.Table;
        Telerik.Reporting.TextBox txtotherdetail = rpt.Items.Find("txtotherdetail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtbasiccost = rpt.Items.Find("txtbasiccost", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtreq = rpt.Items.Find("txtreq", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtcompl = rpt.Items.Find("txtcompl", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtextra = rpt.Items.Find("txtextra", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtfullretail = rpt.Items.Find("txtfullretail", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdiscount2 = rpt.Items.Find("txtdiscount2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtstc = rpt.Items.Find("txtstc", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtgst = rpt.Items.Find("txtgst", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtdeposit = rpt.Items.Find("txtdeposit", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtlessdeposit = rpt.Items.Find("txtlessdeposit", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox txtprojectnote = rpt.Items.Find("txtprojectnote", true)[0] as Telerik.Reporting.TextBox;




        tblcustomer.DataSource = dt1;
        tblsales.DataSource = dt1;
        tblsitedetail.DataSource = dt1;
        tblsitedetails1.DataSource = dt1;
        tblsitedetails2.DataSource = dt1;
        tblsysinsdetail.DataSource = dt1;

        string otherdetail = string.Empty;
        string lblbasiccost = string.Empty;
        string lbltax = string.Empty;
        string lblfullretail = string.Empty;
        string lblreq = "+ N/A";
        string lblcompl = "+ N/A";
        string lbldiscount2 = string.Empty;
        string lblstc = string.Empty;
        string lbltotalcost = string.Empty;
        string lbldeposit = string.Empty;
        string lbllessdeposit = string.Empty;
        string paymentplan = string.Empty;

        txtProjectNumber.Value = stPro.ProjectNumber;
        txtprojectnote.Value = stPro.ProjectNotes;
        try
        {

            if (st2.PromoText != string.Empty)
            {
                // textBox214.Visible = true;
                otherdetail = st2.PromoText;
                txtotherdetail.Value = otherdetail;
            }



        }
        catch
        {

        }
        if (stPro.ServiceValue != string.Empty || stPro.ServiceValue != "")
        {
            lblbasiccost = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.ServiceValue) + Convert.ToDecimal(stPro.RECRebate)).ToString());
        }
        else
        {
            lblbasiccost = "0";
        }

        lbltax = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarMeterInstallation) + Convert.ToDecimal(stPro.VarMeterUG) + Convert.ToDecimal(stPro.VarAsbestos) + Convert.ToDecimal(stPro.VarSplitSystem) + Convert.ToDecimal(stPro.VarCherryPicker) + Convert.ToDecimal(stPro.VarTravelTime) + Convert.ToDecimal(stPro.VarOther)).ToString());
        if (lbltax == string.Empty)
        {
            lbltax = "0";
        }
        try
        {
            lblfullretail = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lblbasiccost) + Convert.ToDecimal(lbltax)).ToString());
        }
        catch { }
        try
        {
            string cost = (Convert.ToDecimal(lblfullretail) - Convert.ToDecimal(lbldiscount2) - Convert.ToDecimal(lblstc)).ToString();
            //String.Format("${0:#.##}", x)
            lbltotalcost = string.Format("{0:0.00}", Convert.ToDecimal(cost));
        }
        catch { }

        if (stPro.SpecialDiscount != string.Empty || stPro.SpecialDiscount != "")
        {
            lbldiscount2 = SiteConfiguration.ChangeCurrency_Val(stPro.SpecialDiscount.ToString());
        }
        else
        {
            lbldiscount2 = "0";
        }
        if (stPro.RECRebate != string.Empty || stPro.RECRebate != "")
        {
            lblstc = SiteConfiguration.ChangeCurrency_Val(stPro.RECRebate.ToString());
        }
        else
        {
            lblstc = "0";
        }
        try
        {
            string cost = (Convert.ToDecimal(lblfullretail) - Convert.ToDecimal(lbldiscount2) - Convert.ToDecimal(lblstc)).ToString();
            //String.Format("${0:#.##}", x)
            lbltotalcost = string.Format("{0:0.00}", Convert.ToDecimal(cost));
        }
        catch { }
        if (stPro.DepositRequired != string.Empty || stPro.DepositRequired != "")
        {
            lbldeposit = SiteConfiguration.ChangeCurrency_Val(stPro.DepositRequired.ToString());
        }
        else
        {
            lbldeposit = "0";
        }
        try
        {
            lbllessdeposit = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lbltotalcost) - Convert.ToDecimal(lbldeposit)).ToString());
        }
        catch { }

        txtbasiccost.Value = lblbasiccost.ToString();
        txtreq.Value = lblreq.ToString();
        txtcompl.Value = lblcompl.ToString();
        txtextra.Value = "+ " + lbltax.ToString();
        txtfullretail.Value = lblfullretail.ToString();
        txtdiscount2.Value = "- " + lbldiscount2.ToString();
        txtstc.Value = "- " + lblstc.ToString();
        txtgst.Value = lbltotalcost.ToString();
        txtdeposit.Value = lbldeposit.ToString();
        txtlessdeposit.Value = lbllessdeposit.ToString();

        #region comment
        //Telerik.Reporting.Table table1 = rpt.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table_Contact = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
        ////Telerik.Reporting.Table table2 = rpt.Items.Find("tbl_sitedetail", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table3 = rpt.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table4 = rpt.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table6 = rpt.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table7 = rpt.Items.Find("table7", true)[0] as Telerik.Reporting.Table;
        //Telerik.Reporting.Table table8 = rpt.Items.Find("table8", true)[0] as Telerik.Reporting.Table;

        //table1.DataSource = dt1;
        //table_Contact.DataSource = dt1;
        //// table2.DataSource = dt1;
        //table3.DataSource = dt1;
        //table4.DataSource = dt1;
        //table5.DataSource = dt1;
        //table6.DataSource = dt1;
        //table7.DataSource = dt1;
        //table8.DataSource = dt1;

        ////Telerik.Reporting.TextBox txtInverterdetail = rpt.Items.Find("txtInverterdetail", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtInverterdetail1 = rpt.Items.Find("txtInverterdetail1", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtbasiccost = rpt.Items.Find("txtbasiccost", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtreq = rpt.Items.Find("txtreq", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtcompl = rpt.Items.Find("txtcompl", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtextra = rpt.Items.Find("txtextra", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtfullretail = rpt.Items.Find("txtfullretail", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtdiscount2 = rpt.Items.Find("txtdiscount2", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtstc = rpt.Items.Find("txtstc", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtgst = rpt.Items.Find("txtgst", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtdeposit = rpt.Items.Find("txtdeposit", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtlessdeposit = rpt.Items.Find("txtlessdeposit", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtmeter = rpt.Items.Find("txtmeter", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtswitch = rpt.Items.Find("txtswitch", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtmeterphase = rpt.Items.Find("txtmeterphase", true)[0] as Telerik.Reporting.TextBox;
        //// Telerik.Reporting.TextBox txtcustname = rpt.Items.Find("txtcustname", true)[0] as Telerik.Reporting.TextBox;
        //// Telerik.Reporting.TextBox txtgetdate = rpt.Items.Find("txtgetdate", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtProjectNumber = rpt.Items.Find("txtProjectNumber", true)[0] as Telerik.Reporting.TextBox;
        ////Telerik.Reporting.TextBox txtpaymentplan = rpt.Items.Find("txtpaymentplan", true)[0] as Telerik.Reporting.TextBox;
        //Telerik.Reporting.TextBox txtotherdetail = rpt.Items.Find("txtotherdetail", true)[0] as Telerik.Reporting.TextBox;
        //// Telerik.Reporting.TextBox textBox214 = rpt.Items.Find("textBox214", true)[0] as Telerik.Reporting.TextBox;
        //// Telerik.Reporting.TextBox txtABN = rpt.Items.Find("textBox215", true)[0] as Telerik.Reporting.TextBox;
        //txtProjectNumber.Value = stPro.ProjectNumber;

        //string inverterdetail = string.Empty;
        //string lblbasiccost = string.Empty;
        //string lblreq = "+ N/A";
        //string lblcompl = "+ N/A";
        //string lbltax = string.Empty;
        //string lblfullretail = string.Empty;
        //string lbldiscount2 = string.Empty;
        //string lblstc = string.Empty;
        //string lbltotalcost = string.Empty;
        //string lbldeposit = string.Empty;
        //string lbllessdeposit = string.Empty;
        //string otherdetail = string.Empty;
        //string paymentplan = string.Empty;



        ////==================================================
        //try
        //{
        //    if (dt1.Rows[0]["FinanceWith"].ToString() != string.Empty)
        //    {
        //        paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString();//
        //        if (st2.FinanceWithDeposit != string.Empty)
        //        {
        //            paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString() + " with " + (Convert.ToInt32(st2.FinanceWithDeposit) / 100).ToString() + "%";
        //            if (dt1.Rows[0]["PaymentType"].ToString() != string.Empty)
        //            {
        //                paymentplan = " Payment option to be organized with " + dt1.Rows[0]["FinanceWith"].ToString() + " with " + (Convert.ToInt32(st2.FinanceWithDeposit) / 100).ToString() + "%" + " for " + dt1.Rows[0]["PaymentType"].ToString();

        //            }
        //        }

        //    }

        //    //txtpaymentplan.Value = paymentplan;

        //    //if (st2.PromoText != string.Empty)
        //    //{
        //    //    textBox214.Visible = true;
        //    //    otherdetail = st2.PromoText;
        //    //}
        //    //else
        //    //{
        //    //    textBox214.Visible = false;
        //    //}

        //    txtotherdetail.Value = otherdetail;
        //}
        //catch
        //{

        //}
        //if (stPro.InverterDetailsID != "")
        //{
        //    inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter";

        //}
        //if (stPro.SecondInverterDetailsID != "0")
        //{
        //    inverterdetail = stPro.inverterqty + " X " + stPro.SecondInverterDetails + " KW Inverter";

        //}
        //if (stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID != "0")
        //{
        //    inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. Plus " + stPro.inverterqty2 + " X " + stPro.SecondInverterDetails + " KW Inverter.";

        //}
        //if ((stPro.InverterDetailsID != "" && stPro.SecondInverterDetailsID == "0"))
        //{
        //    inverterdetail = stPro.inverterqty + " X " + stPro.InverterDetailsName + " KW Inverter. ";
        //}
        //if (stPro.ServiceValue != string.Empty || stPro.ServiceValue != "")
        //{
        //    lblbasiccost = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.ServiceValue) + Convert.ToDecimal(stPro.RECRebate)).ToString());
        //}
        //else
        //{
        //    lblbasiccost = "0";
        //}

        //lbltax = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(stPro.VarHouseType) + Convert.ToDecimal(stPro.VarRoofType) + Convert.ToDecimal(stPro.VarRoofAngle) + Convert.ToDecimal(stPro.VarMeterInstallation) + Convert.ToDecimal(stPro.VarMeterUG) + Convert.ToDecimal(stPro.VarAsbestos) + Convert.ToDecimal(stPro.VarSplitSystem) + Convert.ToDecimal(stPro.VarCherryPicker) + Convert.ToDecimal(stPro.VarTravelTime) + Convert.ToDecimal(stPro.VarOther)).ToString());
        //if (lbltax == string.Empty)
        //{
        //    lbltax = "0";
        //}
        //try
        //{
        //    lblfullretail = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lblbasiccost) + Convert.ToDecimal(lbltax)).ToString());
        //}
        //catch { }
        //if (stPro.SpecialDiscount != string.Empty || stPro.SpecialDiscount != "")
        //{
        //    lbldiscount2 = SiteConfiguration.ChangeCurrency_Val(stPro.SpecialDiscount.ToString());
        //}
        //else
        //{
        //    lbldiscount2 = "0";
        //}
        //if (stPro.RECRebate != string.Empty || stPro.RECRebate != "")
        //{
        //    lblstc = SiteConfiguration.ChangeCurrency_Val(stPro.RECRebate.ToString());
        //}
        //else
        //{
        //    lblstc = "0";
        //}
        //try
        //{
        //    string cost = (Convert.ToDecimal(lblfullretail) - Convert.ToDecimal(lbldiscount2) - Convert.ToDecimal(lblstc)).ToString();
        //    //String.Format("${0:#.##}", x)
        //    lbltotalcost = string.Format("{0:0.00}", Convert.ToDecimal(cost));
        //}
        //catch { }

        //if (stPro.DepositRequired != string.Empty || stPro.DepositRequired != "")
        //{
        //    lbldeposit = SiteConfiguration.ChangeCurrency_Val(stPro.DepositRequired.ToString());
        //}
        //else
        //{
        //    lbldeposit = "0";
        //}
        //try
        //{
        //    lbllessdeposit = SiteConfiguration.ChangeCurrency_Val((Convert.ToDecimal(lbltotalcost) - Convert.ToDecimal(lbldeposit)).ToString());
        //}
        //catch { }

        //txtmeterphase.Value = stPro.NMINumber;

        //txtmeter.Value = "Meter Phase : " + stPro.MeterPhase;
        //txtswitch.Value = "Switchboard Upgrades : " + stPro.meterupgrade;

        ////txtInverterdetail.Value = inverterdetail.ToString();
        //txtInverterdetail1.Value = inverterdetail.ToString();
        //txtbasiccost.Value = lblbasiccost.ToString();
        //txtextra.Value = "+ " + lbltax.ToString();
        //txtreq.Value = lblreq.ToString();
        //txtcompl.Value = lblcompl.ToString();
        //txtfullretail.Value = lblfullretail.ToString();
        //txtdiscount2.Value = "- " + lbldiscount2.ToString();
        //txtstc.Value = "- " + lblstc.ToString();
        //txtgst.Value = lbltotalcost.ToString();
        //txtdeposit.Value = lbldeposit.ToString();
        //txtlessdeposit.Value = lbllessdeposit.ToString();
        //// txtcustname.Value = stPro.Customer;
        //// txtgetdate.Value = (DateTime.Now.AddHours(14).ToString(("dd-MMM-yyyy")));


        //try
        //{



        //    Telerik.Reporting.PictureBox picbxnearby = rpt.Items.Find("picbxnearby", true)[0] as Telerik.Reporting.PictureBox;

        //    if (!string.IsNullOrEmpty(stPro.nearmapdoc))
        //    {
        //        string pdfURL = ConfigurationManager.AppSettings["cdnURL"];

        //        string img1 = pdfURL + "NearMap/" + stPro.nearmapdoc;
        //        //Image myImg1 = Image.FromFile(img1);
        //        picbxnearby.Value = pdfURL + "NearMap/" + stPro.nearmapdoc;
        //    }
        //    else
        //    {
        //        picbxnearby.Visible = false;
        //        picbxnearby.Value = null;
        //    }
        //}
        //catch { }




        //try
        //{
        //    string ABN = dt.Rows[0]["CustWebSiteLink"].ToString();


        //    if (ABN != "" && ABN != "0" && ABN != null)
        //    {
        //        txtABN.Value = ABN;
        //    }
        //    //else
        //    //{
        //    //    txtABN.Value = "";
        //    //}
        //}
        //catch { }
        #endregion
        ExportPdf(rpt);
    }

    public static void Generate_NewPickList(string ProjectID)
    {
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        DataTable dt = ClstblProjects.tblProjects_SelectByProjectID_picklist_withtop1notes(ProjectID);
        DataTable dtitemlist = ClsQuickStock.tbl_PicklistItemDetail_LastPickListWise(ProjectID);
        DataTable dtlist = ClsQuickStock.tbl_Picklistlog_LastPickListWise(ProjectID);
        SttblContacts ContInstaller = ClstblContacts.tblContacts_SelectByContactID(stPro.Installer);
        
        Telerik.Reporting.Report rpt = new EurosolarReporting.PickList();

        Telerik.Reporting.TextBox textBox2 = rpt.Items.Find("textBox2", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox3 = rpt.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox7 = rpt.Items.Find("textBox7", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.TextBox textBox17 = rpt.Items.Find("textBox17", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox18 = rpt.Items.Find("textBox18", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox19 = rpt.Items.Find("textBox19", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox20 = rpt.Items.Find("textBox20", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox21 = rpt.Items.Find("textBox21", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox22 = rpt.Items.Find("textBox22", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox23 = rpt.Items.Find("textBox23", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox24 = rpt.Items.Find("textBox24", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox25 = rpt.Items.Find("textBox25", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.TextBox textBox27 = rpt.Items.Find("textBox27", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.Table table2 = rpt.Items.Find("table2", true)[0] as Telerik.Reporting.Table;

        Telerik.Reporting.TextBox textBox41 = rpt.Items.Find("textBox41", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.TextBox textBox51 = rpt.Items.Find("textBox51", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox31 = rpt.Items.Find("textBox31", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox52 = rpt.Items.Find("textBox52", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.TextBox textBox62 = rpt.Items.Find("textBox62", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox63 = rpt.Items.Find("textBox63", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox64 = rpt.Items.Find("textBox64", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox65 = rpt.Items.Find("textBox65", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox66 = rpt.Items.Find("textBox66", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox67 = rpt.Items.Find("textBox67", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox68 = rpt.Items.Find("textBox68", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox69 = rpt.Items.Find("textBox69", true)[0] as Telerik.Reporting.TextBox;
        Telerik.Reporting.TextBox textBox70 = rpt.Items.Find("textBox70", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.TextBox textBox72 = rpt.Items.Find("textBox72", true)[0] as Telerik.Reporting.TextBox;

        Telerik.Reporting.Table table5 = rpt.Items.Find("table5", true)[0] as Telerik.Reporting.Table;

        Telerik.Reporting.TextBox textBox80 = rpt.Items.Find("textBox80", true)[0] as Telerik.Reporting.TextBox;

        try
        {
            textBox2.Value = string.Format("{0:MMM dd, yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
            textBox3.Value = dtlist.Rows[0]["InstallerName"].ToString();
            textBox7.Value = ContInstaller.ContMobile;

            textBox17.Value = dt.Rows[0]["Customer"].ToString();
            textBox18.Value = stPro.HouseType;
            textBox19.Value = stPro.ProjectNumber + "/" + dtlist.Rows[0]["ID"].ToString();
            textBox20.Value = dt.Rows[0]["ContPhone"].ToString();
            textBox21.Value = stPro.RoofType;
            textBox22.Value = stPro.RoofAngle;
            textBox23.Value = dt.Rows[0]["ContMobile"].ToString();
            textBox24.Value = stPro.ManualQuoteNumber;
            textBox25.Value = stPro.StoreName;

            textBox27.Value = dt.Rows[0]["InstallAddress"].ToString() + ", " + dt.Rows[0]["InstallCity"].ToString() + ", " + dt.Rows[0]["InstallState"].ToString() + " - " + dt.Rows[0]["InstallPostCode"].ToString();

            table2.DataSource = dtitemlist;

            textBox41.Value = stPro.InstallerNotes;

            textBox51.Value = string.Format("{0:MMM dd, yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
            textBox31.Value = dtlist.Rows[0]["InstallerName"].ToString();
            textBox52.Value = ContInstaller.ContMobile;

            textBox62.Value = dt.Rows[0]["Customer"].ToString();
            textBox63.Value = stPro.HouseType;
            textBox64.Value = stPro.ProjectNumber + "/" + dtlist.Rows[0]["ID"].ToString();
            textBox65.Value = dt.Rows[0]["ContPhone"].ToString();
            textBox66.Value = stPro.RoofType;
            textBox67.Value = stPro.RoofAngle;
            textBox68.Value = dt.Rows[0]["ContMobile"].ToString();
            textBox69.Value = stPro.ManualQuoteNumber;
            textBox70.Value = stPro.StoreName;

            textBox72.Value = dt.Rows[0]["InstallAddress"].ToString() + ", " + dt.Rows[0]["InstallCity"].ToString() + ", " + dt.Rows[0]["InstallState"].ToString() + " - " + dt.Rows[0]["InstallPostCode"].ToString();

            table5.DataSource = dtitemlist;

            textBox80.Value = stPro.InstallerNotes;
        }
        catch(Exception ex) { }

        ExportPdf(rpt);
    }
}