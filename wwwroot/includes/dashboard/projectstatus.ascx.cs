﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_projectstatus : System.Web.UI.UserControl
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        if (!IsPostBack)
        {
            if (Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("Installation Manager"))
            {
                divInst.Visible = true;
                divMain.Visible = false;
            }
            else
            {
                divInst.Visible = false;
            }
        }
    }
}