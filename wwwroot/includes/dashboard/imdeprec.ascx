<%@ Control Language="C#" AutoEventWireup="true" CodeFile="imdeprec.ascx.cs" Inherits="includes_dashboard_imdeprec" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
    .selected_row
    {
        background-color: #A1DCF2!important;
    }
    
    .table_row_red {
        border-width: 3px;
        padding-bottom: 10px;
        font-size: 16px;
    }
    .table_row_normal {
        font-size: 15px;
    }

</style>
<script type="text/javascript">
    //$(function () {
    //    $("[id*=GridView1] td").bind("click", function () {
    //        var row = $(this).parent();
    //        $("[id*=GridView1] tr").each(function () {
    //            if ($(this)[0] != row[0]) {
    //                $("td", this).removeClass("selected_row");
    //            }
    //        });
    //        $("td", row).each(function () {
    //            if (!$(this).hasClass("selected_row")) {
    //                $(this).addClass("selected_row");
    //            } else {
    //                $(this).removeClass("selected_row");
    //            }
    //        });
    //    });
    //});
</script>
<%--<div class="col-md-6" id="divMain" runat="server">
    <div class="panlenew">
        <section class="panel">
            <div class="panel-heading"><a href="#">Dep Rec</a> </div>
            <div class="panel-body">
                <div class="marginnone">
                    <div class="tableblack">
                        <div class="table-responsive" runat="server" id="divdeprec">
                            <div class="messesgarea">
                                <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                </div>
                            </div>
                            <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" OnPageIndexChanging="GridView1_PageIndexChanging"
                                OnRowCommand="GridView1_RowCommand">
                                <Columns>
                                    <asp:TemplateField HeaderText="Project #">
                                        <ItemTemplate>
                                            <%# Eval("ProjectNumber")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="100px" HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Customer">
                                        <ItemTemplate>
                                            <%# Eval("Customer")%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dep. Amount">
                                        <ItemTemplate>
                                            <%# Eval("DepositAmount","{0:0.00}")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="120px" HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="gvbtnUpdate" runat="server" CommandArgument='<%# Eval("ProjectID")%>' CommandName="deprec" ImageUrl="~/images/icon_edit.png"
                                                CausesValidation="false" data-toggle="tooltip" data-placement="top" title="Edit" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="verticaaline" />
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle BackColor="#EFF3FB" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                                <PagerTemplate>
                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                </PagerTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>--%>



<div class="col-md-6">
                <div class="databox" style="margin-bottom:unset;height:unset;">
                    <div class="databox-row bg-orange no-padding">                       
                        <div class="databox-cell cell-8 padding-top-5 padding-left-5 text-align-left">
                            <span class="databox-number white" style="font-size:17px;padding: 9px 0;">Dep Rec</span>
                        </div>                       
                    </div>
                      </div>
                 <div class="col-md-12" style="padding: 0px;">
                      <div class="table-responsive well" runat="server" id="divdeprec">
                                <div class="messesgarea">
                                <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                                </div>
                            </div>
                            <asp:GridView ID="GridView1" DataKeyNames="ProjectID" runat="server" OnPageIndexChanging="GridView1_PageIndexChanging"
                                OnRowCommand="GridView1_RowCommand" CssClass="table table-hover"  AutoGenerateColumns="False" HeaderStyle-BorderWidth="2" HeaderStyle-CssClass="bordered-darkorange" BorderWidth="0" GridLines="Horizontal">
                                <Columns>
                                    <asp:TemplateField HeaderText="Project #" HeaderStyle-CssClass="table_row_red" >
                                        <ItemTemplate>
                                            <%# Eval("ProjectNumber")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="100px" HorizontalAlign="Left"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Customer" HeaderStyle-CssClass="table_row_red">
                                        <ItemTemplate>
                                            <%# Eval("Customer")%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="250px"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dep. Amount" HeaderStyle-CssClass="table_row_red">
                                        <ItemTemplate>
                                            <%# Eval("DepositAmount","{0:0.00}")%>
                                        </ItemTemplate>
                                        <ItemStyle Width="150px" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit" HeaderStyle-CssClass="table_row_red">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="gvbtnUpdate" runat="server" CommandArgument='<%# Eval("ProjectID")%>' CommandName="deprec" ImageUrl="~/images/icon_edit.png"
                                                CausesValidation="false" data-toggle="tooltip" data-placement="top" title="Edit" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" CssClass="verticaaline" />
                                    </asp:TemplateField>
                                </Columns>
                              <%--  <RowStyle BackColor="#EFF3FB" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <AlternatingRowStyle BackColor="White" />
                                <PagerTemplate>
                                    <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                    <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                    <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                </PagerTemplate>--%>
                            </asp:GridView>
                                   </div>  
                 </div>
    </div>
<cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
    PopupControlID="divAddComment" TargetControlID="btnNULL" CancelControlID="ibtnCancelComment">
</cc1:ModalPopupExtender>
<div id="divAddComment" runat="server">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <div style="float: right">
                <asp:LinkButton ID="ibtnCancelComment" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal">Close
                </asp:LinkButton>
                     </div>
                <h4 class="modal-title" id="myModalLabel">Update Dep</h4>
            </div>
            <div class="modal-body paddnone">
                <div class="panel-body">
                    <div class="formainline">
                        <div class="form-group">
                            <label>Dep Rec</label>
                            <span class="dateimg">
                                <asp:TextBox ID="txtDepRec" runat="server" Width="100px" CssClass="form-control"></asp:TextBox>
                                <asp:ImageButton ID="Image20" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                <cc1:CalendarExtender ID="CalendarExtender20" runat="server" PopupButtonID="Image20"
                                    TargetControlID="txtDepRec" Format="dd/MM/yyyy">
                                </cc1:CalendarExtender>
                                <asp:RegularExpressionValidator ValidationGroup="AddNotes" ControlToValidate="txtDepRec" ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter valid date"
                                    ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$" Display="Dynamic"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDepRec"
                                    ErrorMessage="This value is required." ValidationGroup="AddNotes" Display="Dynamic"></asp:RequiredFieldValidator>
                            </span>
                        </div>
                        <div class="form-group">
                            <span class="name">
                                <label class="control-label">&nbsp;</label>
                            </span>
                            <asp:Button ID="ibtnAddComment" runat="server" Text="Update" OnClick="ibtnAddComment_Click"
                                ValidationGroup="AddNotes" CssClass="btn btn-primary" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<asp:Button ID="btnNULL" Style="display: none;" runat="server" />
<asp:HiddenField ID="hndProID" runat="server" />
<script type="text/javascript">
    $(document).ready(function () {
        $('#<%=ibtnAddComment.ClientID %>').click(function () {
            formValidate();
        });
    });
    </script>