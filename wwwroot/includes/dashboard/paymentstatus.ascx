﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="paymentstatus.ascx.cs" Inherits="includes_dashboard_paymentstatus" %>


<script>
    //var day_data = [
    //{ "elapsed": "I", "value": 34 },
    //{ "elapsed": "II", "value": 24 },
    //{ "elapsed": "III", "value": 3 },
    //{ "elapsed": "IV", "value": 12 },
    //{ "elapsed": "V", "value": 13 },
    //{ "elapsed": "VI", "value": 22 },
    //{ "elapsed": "VII", "value": 5 },
    //{ "elapsed": "VIII", "value": 26 },
    //{ "elapsed": "IX", "value": 12 },
    //{ "elapsed": "X", "value": 19 }
    //];

    //// Use Morris.Area instead of Morris.Line
    //Morris.Area({
    //    element: 'graph-area',
    //    behaveLikeLine: true,
    //    gridEnabled: false,
    //    gridLineColor: '#dddddd',
    //    axes: true,
    //    fillOpacity: .7,
    //    data: [
    //        { period: '2010 Q1', iphone: 10, ipad: 10, itouch: 10 },
    //        { period: '2010 Q2', iphone: 1778, ipad: 7294, itouch: 18441 },
    //        { period: '2010 Q3', iphone: 4912, ipad: 12969, itouch: 3501 },
    //        { period: '2010 Q4', iphone: 3767, ipad: 3597, itouch: 5689 },
    //        { period: '2011 Q1', iphone: 6810, ipad: 1914, itouch: 2293 },
    //        { period: '2011 Q2', iphone: 5670, ipad: 4293, itouch: 1881 },
    //        { period: '2011 Q3', iphone: 4820, ipad: 3795, itouch: 1588 },
    //        { period: '2011 Q4', iphone: 25073, ipad: 5967, itouch: 5175 },
    //        { period: '2012 Q1', iphone: 10687, ipad: 34460, itouch: 22028 },
    //        { period: '2012 Q2', iphone: 1000, ipad: 5713, itouch: 1791 }


    //    ],
    //    lineColors: ['#4eaa94', '#b2d703', '#ff9c47'],
    //    xkey: 'period',
    //    ykeys: ['iphone', 'ipad', 'itouch'],
    //    labels: ['iPhone', 'iPad', 'iPod Touch'],
    //    pointSize: 0,
    //    lineWidth: 0,
    //    hideHover: 'auto'

    //});
    $(document).ready(function () {
        //    var d0 = [
        //[0,0],[1,0],[2,1],[3,2],[4,15],[5,5],[6,12],[7,10],[8,55],[9,13],[10,25],[11,10],[12,12],[13,6],[14,2],[15,0],[16,0]
        //    ];
        var d0 = [<%=jschart1 %>];
        //var d00 = [
        //  [0,0],[1,0],[2,1],[3,0],[4,1],[5,0],[6,2],[7,0],[8,3],[9,1],[10,0],[11,1],[12,0],[13,2],[14,1],[15,0],[16,0]
        //];
        var d00 = [<%=jschart2 %>];
        $("#flot-sp1ine").length && $.plot($("#flot-sp1ine"), [
                d0, d00
        ],
            {
                series: {
                    lines: {
                        show: false
                    },
                    splines: {
                        show: true,
                        tension: 0.4,
                        lineWidth: 1,
                        fill: 0.4
                    },
                    points: {
                        radius: 0,
                        show: true
                    },
                    shadowSize: 2
                },
                grid: {
                    hoverable: true,
                    clickable: true,
                    tickColor: "#d9dee9",
                    borderWidth: 1,
                    color: '#d9dee9'
                },
                colors: ["#19b39b", "#644688"],
                xaxis: {
                },
                yaxis: {
                    ticks: 4
                },
                tooltip: true,
                tooltipOpts: {
                    content: "chart: %x.1 is %y.4",
                    defaultTheme: false,
                    shifts: {
                        x: 0,
                        y: 20
                    }
                }
            }
        );
    });
</script>

<div class="col-md-6 paddrightzero">
    <div class="panlenew">
        <section class="panel backnone">
            <%--<div class="panel-heading"><a href="#">Payment Status</a> </div>--%>
            <div class="panel-body" runat="server" id="pangraph">
                <section>
                  <div class="pull-left">
                        <%--<div class="pull-right">
                            <div class="btn-group">
                                <button data-toggle="dropdown" class="btn btn-sm btn-rounded btn-default dropdown-toggle"><span class="dropdown-label">Week</span> <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-select">
                                    <li><a href="#">
                                        <input type="radio" name="b">
                                        Month</a></li>
                                    <li><a href="#">
                                        <input type="radio" name="b">
                                        Week</a></li>
                                    <li><a href="#">
                                        <input type="radio" name="b">
                                        Day</a></li>
                                </ul>
                            </div>
                            <a href="#" class="btn btn-default btn-icon btn-rounded btn-sm">Go</a>
                        </div>--%>
                     <h4>   Statistics</h4>
                    </div>
                    <div class="clear"></div>
                    <div class="panel-body">
                        <div id="flot-sp1ine" style="height: 210px"></div>
                    </div>
                    <div class="row text-center no-gutter">
                        <div class="col-xs-3"><span class="h4 font-bold m-t block">
                            <asp:Literal runat="server" ID="lttotalquote"></asp:Literal></span> <small class="text-muted m-b block">Total Quotes Created</small> </div>
                        <div class="col-xs-3"><span class="h4 font-bold m-t block">
                            <asp:Literal runat="server" ID="lttotalfirstdep"></asp:Literal></span> <small class="text-muted m-b block">Total First Dep Rec</small> </div>
                        <%--<div class="col-xs-3"><span class="h4 font-bold m-t block">21,230</span> <small class="text-muted m-b block">Items</small> </div>
                        <div class="col-xs-3"><span class="h4 font-bold m-t block">7,230</span> <small class="text-muted m-b block">Customers</small> </div>--%>
                    </div>
                </section>
                <%--<div class="imgresponsive">
                    <div id="graph-area"></div>
                    <div class="listdata">
                        <table border="0" cellpadding="0" cellspacing="4">
                            <tr>
                                <td>
                                    <img src="../../images/img_total.jpg" width="12" height="12" /></td>
                                <td>Total</td>
                                <td>
                                    <asp:Literal ID="litTotal" runat="server"></asp:Literal></td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="../../images/img_dept.jpg" width="12" height="12" /></td>
                                <td>Dep. Rec</td>
                                <td>
                                    <asp:Literal ID="litDepRec" runat="server"></asp:Literal></td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="../../images/img_active.jpg" width="12" height="12" /></td>
                                <td>Active</td>
                                <td>
                                    <asp:Literal ID="litActive" runat="server"></asp:Literal></td>
                            </tr>
                        </table>
                    </div>
                </div>--%>
            </div>
        </section>
    </div>
</div>
