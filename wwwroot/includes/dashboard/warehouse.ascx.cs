﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_warehouse : System.Web.UI.UserControl
{
    static DataView dv;
    static DataView dv3;
    static DataView dv1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Roles.IsUserInRole("WarehouseManager"))
            {
                BindGrid(0);
            }
        }
    }

    protected DataTable GetGridDataUp()
    {
        DataTable dt2 = new DataTable();
        dt2 = ClsDashboard.tblStockOrders_UpComingStock();

        int iTotalRecords = dt2.Rows.Count;
        int iEndRecord = GridViewUp.PageSize * (GridViewUp.PageIndex + 1);
        int iStartsRecods = (iEndRecord + 1) - GridViewUp.PageSize;
        if (iEndRecord > iTotalRecords)
        {
            iEndRecord = iTotalRecords;
        }
        if (iStartsRecods == 0)
        {
            iStartsRecods = 1;
        }
        if (iEndRecord == 0)
        {
            iEndRecord = iTotalRecords;
        }
        // ltrPage4.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt2;
    }

    protected DataTable GetGridDataStock()
    {
        DataTable dt2 = new DataTable();
        dt2 = ClsDashboard.tblStockItems_PMStock();

        int iTotalRecords = dt2.Rows.Count;
        int iEndRecord = GridViewStock.PageSize * (GridViewStock.PageIndex + 1);
        int iStartsRecods = (iEndRecord + 1) - GridViewStock.PageSize;
        if (iEndRecord > iTotalRecords)
        {
            iEndRecord = iTotalRecords;
        }
        if (iStartsRecods == 0)
        {
            iStartsRecods = 1;
        }
        if (iEndRecord == 0)
        {
            iEndRecord = iTotalRecords;
        }
        // ltrPage4.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt2;
    }

    protected DataTable GetGridData1()
    {
        DataTable dt2 = new DataTable();
        dt2 = ClsDashboard.tblProjects_WMJobs();

        int iTotalRecords = dt2.Rows.Count;
        int iEndRecord = GridView1.PageSize * (GridView1.PageIndex + 1);
        int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
        if (iEndRecord > iTotalRecords)
        {
            iEndRecord = iTotalRecords;
        }
        if (iStartsRecods == 0)
        {
            iStartsRecods = 1;
        }
        if (iEndRecord == 0)
        {
            iEndRecord = iTotalRecords;
        }
        // ltrPage4.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt2;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt1 = new DataTable();
        dt1 = GetGridDataUp();
        dv = new DataView(dt1);


        if (dt1.Rows.Count > 0)
        {
            GridViewUp.DataSource = dt1;
            GridViewUp.DataBind();
        }

        DataTable dt2 = new DataTable();
        dt2 = GetGridDataStock();
        dv3 = new DataView(dt2);


        if (dt2.Rows.Count > 0)
        {
            GridViewStock.DataSource = dt2;
            GridViewStock.DataBind();
        }

        DataTable dt3 = new DataTable();
        dt3 = GetGridData1();
        dv1 = new DataView(dt3);


        if (dt3.Rows.Count > 0)
        {
            GridView1.DataSource = dt3;
            GridView1.DataBind();
        }
    }

    protected void GridViewUp_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewUp.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    void lb_Command(object sender, CommandEventArgs e)
    {
        GridViewUp.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);

    }
    void lbstock_Command(object sender, CommandEventArgs e)
    {
        GridViewStock.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);

    }
    void lb1_Command(object sender, CommandEventArgs e)
    {
        GridView1.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);

    }
    protected void GridViewStock_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewStock.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridViewUp_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb_Command);
        }
    }
    protected void GridViewUp_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridViewUp.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridViewUp.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridViewUp.PageIndex - 2;
        page[1] = GridViewUp.PageIndex - 1;
        page[2] = GridViewUp.PageIndex;
        page[3] = GridViewUp.PageIndex + 1;
        page[4] = GridViewUp.PageIndex + 2;
        page[5] = GridViewUp.PageIndex + 3;
        page[6] = GridViewUp.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridViewUp.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridViewUp.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridViewUp.PageIndex == GridViewUp.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridViewUp.PageSize * (GridViewUp.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridViewUp.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    protected void GridViewStock_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridViewStock.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridViewStock.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridViewStock.PageIndex - 2;
        page[1] = GridViewStock.PageIndex - 1;
        page[2] = GridViewStock.PageIndex;
        page[3] = GridViewStock.PageIndex + 1;
        page[4] = GridViewStock.PageIndex + 2;
        page[5] = GridViewStock.PageIndex + 3;
        page[6] = GridViewStock.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridViewStock.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridViewStock.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridViewStock.PageIndex == GridViewUp.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv3.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv3.ToTable().Rows.Count;
            int iEndRecord = GridViewStock.PageSize * (GridViewStock.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridViewStock.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    protected void GridViewStock_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lbstock_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lbstock_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lbstock_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lbstock_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lbstock_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lbstock_Command);
        }
    }
    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridView1.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridView1.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridView1.PageIndex - 2;
        page[1] = GridView1.PageIndex - 1;
        page[2] = GridView1.PageIndex;
        page[3] = GridView1.PageIndex + 1;
        page[4] = GridView1.PageIndex + 2;
        page[5] = GridView1.PageIndex + 3;
        page[6] = GridView1.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridView1.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridView1.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridView1.PageIndex == GridViewUp.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv3.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv3.ToTable().Rows.Count;
            int iEndRecord = GridViewStock.PageSize * (GridView1.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridView1.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lb1_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lb1_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lb1_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lb1_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lb1_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lb1_Command);
        }
    }
}