﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_calender : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void EventCalendarDayRender(object sender, DayRenderEventArgs e)
    {
        DataTable dt = new DataTable();
        CalendarDay d = ((DayRenderEventArgs)e).Day;
        TableCell c = ((DayRenderEventArgs)e).Cell;
        c.Style.Value = "border:solid 0px #ffffff;border-width:1px;border-collapse:collapse;";
         
        if (dt.Rows.Count > 0)
        {
            int j = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DateTime dtime = Convert.ToDateTime(dt.Rows[i]["startdate"].ToString());
                DateTime dtime1 = Convert.ToDateTime(dt.Rows[i]["enddate"].ToString());

                string id = dt.Rows[i]["id"].ToString();
                string color = dt.Rows[i]["colorcode"].ToString();

                dtime = dtime.Date;
                dtime1 = dtime1.Date;
                DateTime curdate = d.Date;
                if (curdate >= dtime && curdate <= dtime1)
                {
                    e.Cell.Controls.Clear();
                    e.Cell.BackColor = System.Drawing.Color.FromName("#" + "" + color);
                    HyperLink h = new HyperLink();
                    h.NavigateUrl = "activityplanner.aspx?id=" + id;
                    h.Text = e.Day.DayNumberText;
                    h.ToolTip = dt.Rows[i]["title"].ToString();
                    e.Cell.Controls.Add(h);
                    j = 1;
                }
            }
            if (j == 0)
            {
                e.Cell.Controls.Clear();
                Label lbl = new Label();
                lbl.Text = e.Day.DayNumberText;
                e.Cell.Controls.Add(lbl);
            }
        }

        else
        {
            e.Cell.Controls.Clear();
            Label lbl = new Label();
            lbl.Text = e.Day.DayNumberText;
            e.Cell.Controls.Add(lbl);
        }

    }
    protected void Calendar1_OnVisibleMonthChanged(object sender, MonthChangedEventArgs e)
    {

    }
    protected void Calendar1_DataBinding(object sender, EventArgs e)
    {

    }
}