﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_projectstatus2 : System.Web.UI.UserControl
{
    protected string jschart2;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindProjectStatus();
        }
    }
    public void BindProjectStatus()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st = ClstblEmployees.tblEmployees_SelectByUserId(userid);

        string EmployeeID = "0";
        if (Roles.IsUserInRole("Customer") || Roles.IsUserInRole("Installer"))
        {
            EmployeeID = "0";
        }
        else
        {
            EmployeeID = st.EmployeeID;
        }

        //DataTable dtStatus2 = ClstblProjects.tblProjects_PreInstCountStatus(EmployeeID);
        //string chartvalue2 = "";
        //foreach (DataRow row2 in dtStatus2.Rows)
        //{
        //    chartvalue2 += "['" + row2["ProjectStatus"].ToString() + "'," + row2["Status"].ToString() + "],";
        //}
        //jschart2 = chartvalue2;

        //DataTable dt2 = ClstblProjects.tblProjects_PreInstCount(EmployeeID);
        //if (dt2.Rows.Count > 0)
        //{
        //lblActiveInst.Text = dt2.Rows[0]["Active"].ToString();
        //lblJobBooked.Text = dt2.Rows[0]["JobBooked"].ToString();
        //lblStockAssigned.Text = dt2.Rows[0]["PaperworkRec"].ToString();
        //lblPaperworkRec.Text = dt2.Rows[0]["STCInvoicePaid"].ToString();
        //lblSTCPaid.Text = dt2.Rows[0]["StockAssigned"].ToString();
        //lblCompleteInst.Text = dt2.Rows[0]["Complete"].ToString();
        //}
        DataTable dtStatus = ClstblProjects.tblProjectStatus_CountStatus(EmployeeID);
        string chartvalue = "";
        foreach (DataRow row in dtStatus.Rows)
        {
            //chartvalue += "['" + row["ProjectStatus"].ToString() + "'," + row["Status"].ToString() + "],";
            chartvalue += "{label:'" + row["ProjectStatus"].ToString() + "',data:" + row["Status"].ToString() + "},";
        }
        jschart2 = chartvalue;
    }
}