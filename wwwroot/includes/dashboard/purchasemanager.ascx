﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="purchasemanager.ascx.cs" Inherits="includes_dashboard_purchasemanager" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<div class="col-md-12" id="divUpComingStock" runat="server">
    <div class="panlenew">
        <section class="panel">
            <div class="panel-heading">
                <div class="pull-right">
                    <div class="btn-group">
                        <asp:DropDownList ID="ddlView2" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                            <asp:ListItem Value="">All</asp:ListItem>
                            <asp:ListItem Value="1">Day</asp:ListItem>
                            <asp:ListItem Value="2">Week</asp:ListItem>
                            <asp:ListItem Value="3">Month</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:Button ID="btnGo" runat="server" CssClass="btn btn-primary" Text="Go" />
                </div>
                <a href="#">UpComing Stock</a>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <asp:GridView ID="GridViewUp" DataKeyNames="StockOrderID" runat="server" 
                        PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false" AllowPaging="true" OnDataBound="GridViewCash_DataBound" OnRowCreated="GridViewCash_RowCreated"
                        class="table table-bordered table-hover" OnPageIndexChanging="GridViewUp_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Order #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridheadertext">
                                <ItemTemplate>
                                    <%#Eval("OrderNumber")%>
                                </ItemTemplate>
                                <ItemStyle Width="90px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ManualOrder #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%#Eval("ManualOrderNumber")%>
                                </ItemTemplate>
                                <ItemStyle Width="130px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item Ordered" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%#Eval("StockItem")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ExpectedDelivery" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%#Eval("ExpectedDelivery","{0:dd MMM yyyy}")%>
                                </ItemTemplate>
                                <ItemStyle Width="130px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Icon" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnRefund" CommandName="Refund" CausesValidation="false"
                                        CommandArgument='<%#Eval("StockOrderID") %>' runat="server" data-toggle="tooltip" data-placement="top" title="Icon">
                                    Add
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                        </Columns>
                          <AlternatingRowStyle />

                                                        <PagerTemplate>
                                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                            <div class="pagination">
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                            </div>
                                                        </PagerTemplate>
                                                        <PagerStyle CssClass="paginationGrid" />
                                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                    </asp:GridView>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="col-md-12" id="divStock" runat="server">
    <div class="panlenew">
        <section class="panel">
            <div class="panel-heading">
                <div class="pull-right">
                    <div class="btn-group">
                        <asp:DropDownList ID="ddlView" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                            <asp:ListItem Value="">All</asp:ListItem>
                            <asp:ListItem Value="1">Day</asp:ListItem>
                            <asp:ListItem Value="2">Week</asp:ListItem>
                            <asp:ListItem Value="3">Month</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:Button ID="btnGo2" runat="server" CssClass="btn btn-primary" Text="Go" />
                </div>
                <a href="#">Stock</a>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <asp:GridView ID="GridViewStock" DataKeyNames="StockItemID" runat="server" OnDataBound="GridViewFinance_DataBound"
                        AllowPaging="true" PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false" OnRowCreated="GridViewFinance_RowCreated"
                        AllowSorting="true" class="table table-bordered table-hover" OnPageIndexChanging="GridViewStock_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="StockItem" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%#Eval("StockItem")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Current Stock" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridheadertext">
                                <ItemTemplate>
                                    <%#Eval("CurStock")%>
                                </ItemTemplate>
                                <ItemStyle Width="130px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sold" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <%#Eval("Sold")%>
                                </ItemTemplate>
                                <ItemStyle Width="100px" />
                            </asp:TemplateField>
                        </Columns>
                         <AlternatingRowStyle />

                                                        <PagerTemplate>
                                                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                                                            <div class="pagination">
                                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                                                            </div>
                                                        </PagerTemplate>
                                                        <PagerStyle CssClass="paginationGrid" />
                                                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                    </asp:GridView>
                </div>
            </div>
        </section>
    </div>
</div>
<style type="text/css">
        .selected_row {
            background-color: #A1DCF2!important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridViewUp] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridViewUp] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
	    $("[id*=GridViewStock] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridViewStock] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>