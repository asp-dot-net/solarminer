﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="receivable.ascx.cs" Inherits="includes_dashboard_receivable" %>
<div class="col-md-6" id="divCashReceivable" runat="server">
    <div class="panlenew">
        <section class="panel">
            <div class="panel-heading">
                <div class="pull-right">
                    <div class="btn-group">
                        <asp:DropDownList ID="ddlView" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                            <asp:ListItem Value="">All</asp:ListItem>
                            <asp:ListItem Value="1">Day</asp:ListItem>
                            <asp:ListItem Value="2">Week</asp:ListItem>
                            <asp:ListItem Value="3">Month</asp:ListItem>
                        </asp:DropDownList>
                        </div>
                        <div class="btn-group">
                        <asp:DropDownList ID="ddlCash" runat="server"  AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval" style="Width:30%;">
                            <asp:ListItem Value="">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:Button ID="btnGo" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnGo_Click" />
                </div>
                <div class="pull-left">
                <a><h4>Cash Receivable</h4></a>
                    </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <asp:GridView ID="GridViewCash" DataKeyNames="ProjectID" runat="server" OnDataBound="GridViewCash_DataBound"
                        AllowPaging="true" PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false" OnRowCreated="GridViewCash_RowCreated"
                        AllowSorting="true" class="table table-bordered table-hover" OnPageIndexChanging="GridViewCash_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridheadertext">
                                <ItemTemplate>
                                    <%#Eval("InstallBookingDate","{0:dd MMM yyyy}")%>
                                </ItemTemplate>
                                <ItemStyle Width="120px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Project" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%#Eval("Project")%>
                                </ItemTemplate>
                                <ItemStyle Width="170px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Due" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Right" ItemStyle-Width="120px">
                                <ItemTemplate>
                                    <%#Eval("Due")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns> 
                        <PagerTemplate>
                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                            <div class="pagination">
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                            </div>
                        </PagerTemplate>
                        <PagerStyle CssClass="paginationGrid" />
                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                    </asp:GridView>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="col-md-6" id="divFinanceReceivable" runat="server">
    <div class="panlenew">
        <section class="panel">
            <div class="panel-heading">
                <div class="pull-right">
                    <div class="btn-group">
                        <asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                            <asp:ListItem Value="">All</asp:ListItem>
                            <asp:ListItem Value="1">Day</asp:ListItem>
                            <asp:ListItem Value="2">Week</asp:ListItem>
                            <asp:ListItem Value="3">Month</asp:ListItem>
                        </asp:DropDownList>
                        </div>
                        <div class="btn-group">
                        <asp:DropDownList ID="ddlFinance" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                            <asp:ListItem Value="">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:Button ID="btnGo2" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnGo2_Click" />
                </div>
                 <div class="pull-left">
                <a><h4>Finance Receivable</h4></a>
                     </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <asp:GridView ID="GridViewFinance" DataKeyNames="ProjectID" runat="server" OnDataBound="GridViewFinance_DataBound"
                        AllowPaging="true" PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false" OnRowCreated="GridViewFinance_RowCreated"
                        AllowSorting="true" class="table table-bordered table-hover" OnPageIndexChanging="GridViewFinance_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridheadertext">
                                <ItemTemplate>
                                    <%#Eval("InstallBookingDate","{0:dd MMM yyyy}")%>
                                </ItemTemplate>
                                <ItemStyle Width="120px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Project" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%#Eval("Project")%>
                                </ItemTemplate>
                                <ItemStyle Width="170px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Due" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Right" ItemStyle-Width="120px">
                                <ItemTemplate>
                                    <%#Eval("Due","{0:0.00}")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerTemplate>
                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                            <div class="pagination">
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                            </div>
                        </PagerTemplate>
                        <PagerStyle CssClass="paginationGrid" />
                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                    </asp:GridView>
                </div>
            </div>
        </section>
    </div>
</div>
 <style type="text/css">
        .selected_row {
            background-color: #A1DCF2!important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridViewCash] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridViewCash] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
	     $("[id*=GridViewFinance] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridViewFinance] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>