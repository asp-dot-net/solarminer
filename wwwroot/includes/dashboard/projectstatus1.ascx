﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectstatus1.ascx.cs" Inherits="includes_dashboard_projectstatus1" %>

<script>
    $(document).ready(function () {
        var da = [<%=jschart1 %>],
   da1 = [],
   series = Math.floor(Math.random() * 4) + 3;


        for (var i = 0; i < series; i++) {
            da1[i] = {
                label: "Series" + (i + 1),
                data: Math.floor(Math.random() * 100) + 1
            }
        }

        $("#flot-pie-donut").length && $.plot($("#flot-pie-donut"), da, {
            series: {
                pie: {
                    innerRadius: 0.4,
                    show: true,
                    stroke: {
                        width: 0
                    },
                    label: {
                        show: true,
                        threshold: 0.05
                    },

                }
            },
            colors: ["#65b5c2", "#4da7c1", "#3993bb", "#2e7bad", "#23649e"],
            grid: {
                hoverable: true,
                clickable: false
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s: %p.0%"
            }
        });



        //var s1 = [<%=jschart1 %>];
        //var plot1 = $.jqplot('chart3', [s1], {
        //    seriesDefaults: {
        //       renderer: $.jqplot.DonutRenderer,
        //        rendererOptions: {
        //            sliceMargin: 3,
        //           startAngle: -90,
        //            showDataLabels: true,
        //            dataLabels: 'value',
        //            shadow: false
        //        }
        //  },
        //    grid: {
        //        drawGridLines: false,        // wether to draw lines across the grid or not.
        //         gridLineColor: '#fff',   // CSS color spec of the grid lines.
        //         background: '#fff',      // CSS color spec for background color of grid.
        //         borderColor: '#fff',     // CSS color spec for border around grid.
        //        borderWidth: 0.0,           // pixel width of border around grid.
        //         shadow: false,               // draw a shadow for grid.
        //
        //     },
        //      //legend: { show: true, location: 'e' }
        //  });

    });
</script>
<div class="">
    <section>
       <div>
            <%--<div class="btn-group pull-right">
                <button data-toggle="dropdown" class="btn btn-sm btn-rounded btn-default dropdown-toggle"><span class="dropdown-label">Last 24 Hours</span> <span class="caret"></span></button>
                <ul class="dropdown-menu dropdown-select">
                    <li><a href="#">
                        <input type="radio" name="a">Today</a></li>
                    <li><a href="#">
                        <input type="radio" name="a">Yesterday</a></li>
                    <li><a href="#">
                        <input type="radio" name="a">Last 24 Hours</a></li>
                    <li><a href="#">
                        <input type="radio" name="a">Last 7 Days</a></li>
                    <li><a href="#">
                        <input type="radio" name="a">Last 30 days</a></li>
                    <li><a href="#">
                        <input type="radio" name="a">Last Month</a></li>
                    <li><a href="#">
                        <input type="radio" name="a">All Time</a></li>
                </ul>
            </div>--%>
          <h4>  Project Status </h4>
           </div>
        <div class="panel-body flot-legend"  style="
    background: rgba(220, 220, 220, 0.63);">
            <div id="flot-pie-donut" style="height: 240px"></div>
        </div>
    </section>
    <%--<div id="chart3"></div>--%>
    <%--<div class="listdata">
        <table border="0" cellpadding="0" cellspacing="4">
            <tr>
                <td>
                    <img src="../../images/img_planned.jpg" width="12" height="12" /></td>
                <td>Planned</td>
                <td>
                    <asp:Label ID="lblPlanned" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <img src="../../images/img_on_hold.jpg" width="12" height="12" /></td>
                <td>On-Hold</td>
                <td>
                    <asp:Label ID="lblOnHold" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <img src="../../images/img_cancelled.jpg" width="12" height="12" /></td>
                <td>Cancelled</td>
                <td>
                    <asp:Label ID="lblCancelled" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <img src="../../images/img_dep_rec.jpg" width="12" height="12" /></td>
                <td>Dep Rec.</td>
                <td>
                    <asp:Label ID="lblDepRec" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <img src="../../images/img_active1.jpg" width="12" height="12" /></td>
                <td>Active</td>
                <td>
                    <asp:Label ID="lblActive" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <img src="../../images/img_complete1.jpg" width="12" height="12" /></td>
                <td>Complete</td>
                <td>
                    <asp:Label ID="lblCompleted" runat="server"></asp:Label></td>
            </tr>
        </table>
    </div>--%>
</div>
