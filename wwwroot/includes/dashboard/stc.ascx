﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="stc.ascx.cs" Inherits="includes_dashboard_stc" %>

<div class="col-md-6" id="divPaperworkRemaining" runat="server">
    <div class="panlenew">
        <section class="panel">
            <div class="panel-heading">
                <div class="pull-right">
                    <div class="btn-group">
                        <asp:DropDownList ID="ddlView" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                            <asp:ListItem Value="">All</asp:ListItem>
                            <asp:ListItem Value="1">Day</asp:ListItem>
                            <asp:ListItem Value="2">Week</asp:ListItem>
                            <asp:ListItem Value="3">Month</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:Button ID="btnGo" runat="server" CssClass="btn btn-primary" Text="Go" />
                </div>
                <a href="#">Paperwork Remaining</a>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover gridcss">
                        <tr>
                            <th>Month</th>
                            <th>Total Paperwork Remaining</th>
                            <asp:Repeater ID="rptPaperRem" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                             <%#Eval("MonthName") %>
                                        </td>
                                        <td>
                                            <%#Eval("totalpan") %>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tr>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>

<div class="col-md-6" id="divStcRemaining" runat="server">
    <div class="panlenew">
        <section class="panel">
            <div class="panel-heading">
                <div class="pull-right">
                    <div class="btn-group">
                        <asp:DropDownList ID="ddlView2" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                            <asp:ListItem Value="">All</asp:ListItem>
                            <asp:ListItem Value="1">Day</asp:ListItem>
                            <asp:ListItem Value="2">Week</asp:ListItem>
                            <asp:ListItem Value="3">Month</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:Button ID="btnGo2" runat="server" CssClass="btn btn-primary" Text="Go" />
                </div>
                <a href="#">STC Remaining</a>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                     <table cellpadding="5" cellspacing="0" border="0" class="table table-bordered table-hover gridcss">
                        <tr>
                            <th>Month</th>
                            <th>InstallBooking</th>
                            <th>STC Applied</th>
                            <asp:Repeater ID="rptStCRem" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                             <%#Eval("MonthName") %>
                                        </td>
                                        <td>
                                            <%#Eval("totalinst") %>
                                        </td>
                                        <td>
                                            <%#Eval("totalstc") %>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tr>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>

