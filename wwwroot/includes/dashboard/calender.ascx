﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="calender.ascx.cs" Inherits="includes_dashboard_calender" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<div class="col-md-6">
    <div class="panlenew">
        <section class="panel">
            <%--<div class="panel-heading"><span class="badge bg-warning bg-red pull-right">10</span> <a href="#">Task</a> </div>--%>
            <div class="panel-body calendarmain">
                <h4><a href="#">Calendar</a></h4>
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="monthbox">
            <tr>
                <td>
                    <asp:Calendar ID="Calendar1" runat="server" Width="100%" OnDayRender="EventCalendarDayRender"
                        ShowTitle="true" Enabled="true"  
                        ToolTip="" Title="" NextMonthText="<img src='../../images/arrow_next.jpg'>"  PrevMonthText="<img src='../../images/arrow_previous.jpg'>"
                        OnVisibleMonthChanged="Calendar1_OnVisibleMonthChanged" NextPrevStyle-Height="18"
                        BorderStyle="None" BorderColor="white" OnDataBinding="Calendar1_DataBinding">
                        <NextPrevStyle CssClass="nextprev" BackColor="#1ca5d9" BorderStyle="None" />
                        <DayHeaderStyle CssClass="weekheading" Font-Bold="true" />
                        <TitleStyle CssClass="titlecss" BackColor="#1ca5d9" Font-Bold="true" ForeColor="white" />
                    </asp:Calendar>
                    <%--<DayStyle HorizontalAlign="Left" CssClass="box2" VerticalAlign="Top" />--%>
                    <%-- <SelectorStyle HorizontalAlign="Left" CssClass="box2" />--%>
                    <%--<SelectedDayStyle HorizontalAlign="Left" CssClass="box2" ForeColor="Red" />--%>
                </td>
            </tr>
        </table>
            </div>
        </section>
    </div>
</div>
