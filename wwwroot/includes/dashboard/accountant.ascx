<%@ Control Language="C#" AutoEventWireup="true" CodeFile="accountant.ascx.cs" Inherits="includes_dashboard_accountant" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<script>
    $(document).ready(function () {
        $.jqplot.config.enablePlugins = true;
        var s1 = [<%=jscharts1 %>];
        var ticks = [<%=jschartticks %>];

        plot1 = $.jqplot('chart1', [s1], {
            // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
            animate: !$.jqplot.use_excanvas,
            seriesDefaults: {
                renderer: $.jqplot.BarRenderer,
                pointLabels: { show: true }
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                }
            },
            highlighter: { show: false }
        });

        $('#chart1').bind('jqplotDataClick',
            function (ev, seriesIndex, pointIndex, data) {
                $('#info1').html('series: ' + seriesIndex + ', point: ' + pointIndex + ', data: ' + data);
            }
        );
    });
</script>
<script>
    function doMyAction() {
        if ($.fn.select2 !== 'undefined') {
            $(".myval").select2({
                //placeholder: "select",
                allowclear: true
            });
            $(".myval1").select2({
                minimumResultsForSearch: -1
            });
        }
    }
</script>
<div class="col-md-6" id="divRefund" runat="server">
    <div class="panlenew">
        <section class="panel">
            <div class="panel-heading">
                <div class="pull-right">
                    <div class="btn-group">
                        <asp:DropDownList ID="ddlView" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                            <asp:ListItem Value="">All</asp:ListItem>
                            <asp:ListItem Value="1">Day</asp:ListItem>
                            <asp:ListItem Value="2">Week</asp:ListItem>
                            <asp:ListItem Value="3">Month</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:Button ID="btnGo" runat="server" CssClass="btn btn-default btn-icon btn-rounded btn-sm marginleft3" Text="Go" />
                </div>
                <a href="#">Refund</a>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <asp:GridView ID="GridViewRefund" DataKeyNames="RefundID" runat="server" OnDataBound="GridViewRefund_DataBound" OnRowCreated="GridViewRefund_RowCreated"
                        AllowPaging="true" PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false" OnRowCommand="GridViewRefund_RowCommand"
                    AllowSorting="true" class="table table-bordered table-hover" OnPageIndexChanging="GridViewRefund_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Project #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridheadertext">
                                <ItemTemplate>
                                    <%#Eval("ProjectNumber")%>
                                </ItemTemplate>
                                <ItemStyle Width="90px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Customer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%#Eval("Customer")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amount" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <%#Eval("Amount","{0:0.00}")%>
                                </ItemTemplate>
                                <ItemStyle Width="100px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Refund" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnRefund" CommandName="Refund" Visible='<%#Eval("Status").ToString()=="False"?true:false %>' CausesValidation="false"
                                        CommandArgument='<%#Eval("RefundID") %>' runat="server" data-toggle="tooltip" data-placement="top" title="Refund">
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icon_refund2.png" />
                                    </asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="50px" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerTemplate>
                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                            <div class="pagination">
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                            </div>
                        </PagerTemplate>
                        <PagerStyle CssClass="paginationGrid" />
                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                    </asp:GridView>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="col-md-6" id="divElecInv" runat="server">
    <div class="panlenew">
        <section class="panel">
            <div class="panel-heading">
                <div class="pull-right">
                    <div class="btn-group">
                        <asp:DropDownList ID="ddlView2" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                            <asp:ListItem Value="">All</asp:ListItem>
                            <asp:ListItem Value="1">Day</asp:ListItem>
                            <asp:ListItem Value="2">Week</asp:ListItem>
                            <asp:ListItem Value="3">Month</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:Button ID="btnGo2" runat="server" CssClass="btn btn-default btn-icon btn-rounded btn-sm marginleft3" Text="Go" />
                </div>
                <a href="#">Elec Invoice</a>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <asp:GridView ID="GridViewElecInv" DataKeyNames="ProjectID" runat="server" OnRowCreated="GridViewElecInv_RowCreated"
                        PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false" OnDataBound="GridViewElecInv_DataBound" AllowPaging="true"
                        CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" OnPageIndexChanging="GridViewElecInv_PageIndexChanging">
                        <Columns>
                            <asp:TemplateField HeaderText="Project #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="gridheadertext">
                                <ItemTemplate>
                                    <%#Eval("ProjectNumber")%>
                                </ItemTemplate>
                                <ItemStyle Width="90px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Invoice #" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%#Eval("InvoiceNumber")%>
                                </ItemTemplate>
                                <ItemStyle Width="90px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Inv Amount" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Right">
                                <ItemTemplate>
                                    <%#Eval("InvAmount","{0:0.00}")%>
                                </ItemTemplate>
                                <ItemStyle Width="110px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Installer" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%#Eval("Installer")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerTemplate>
                            <asp:Label ID="ltrPage" runat="server" CssClass="pageshowing" Style="float: left;"></asp:Label>
                            <div class="pagination">
                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="First" CssClass="Linkbutton firstpage nextbtn">First</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Prev" CssClass="Linkbutton prebtn">Previous</asp:LinkButton>
                                <asp:LinkButton ID="p0" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p1" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p2" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:Label ID="CurrentPage" runat="server" Text="Label" Style="min-width: 25px; display: inline-block;"></asp:Label>
                                <asp:LinkButton ID="p4" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p5" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="p6" runat="server" CssClass="Linkbutton" CausesValidation="false">LinkButton</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Next" CssClass="Linkbutton nextbtn">Next</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton4" runat="server" CommandName="Page" CausesValidation="false" CommandArgument="Last" CssClass="Linkbutton lastpage nextbtn">Last</asp:LinkButton>
                            </div>
                        </PagerTemplate>
                        <PagerStyle CssClass="paginationGrid" />
                        <PagerSettings Mode="NumericFirstLast" FirstPageText="FIRST" LastPageText="LAST" PageButtonCount="4" />
                    </asp:GridView>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="col-md-12" id="divGraph" runat="server">
    <div class="panlenew">
        <section class="panel">
            <div class="panel-heading">
                <div class="pull-right">
                    <div class="btn-group">
                        <asp:DropDownList ID="ddlView3" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                            <asp:ListItem Value="">All</asp:ListItem>
                            <asp:ListItem Value="1">Day</asp:ListItem>
                            <asp:ListItem Value="2">Week</asp:ListItem>
                            <asp:ListItem Value="3">Month</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:Button ID="btnGo3" runat="server" CssClass="btn btn-primary" Text="Go" />
                </div>
                <a href="#">Graph</a>
            </div>
            <div class="panel-body paddnone" id="divMain" runat="server">
                <div class="row m-n">
                    <div id="chart1"></div>
                </div>
            </div>
        </section>
    </div>
</div>


<cc1:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalbackground"
    PopupControlID="divAddComment" TargetControlID="btnNULL" CancelControlID="ibtnCancelComment">
</cc1:ModalPopupExtender>
<div id="divAddComment" runat="server">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                 <div style="float: right">
                <asp:LinkButton ID="ibtnCancelComment" runat="server" type="button" class="btn btn-danger btncancelicon" data-dismiss="modal"
                    OnClick="ibtnCancelComment_Onclick">Close
                </asp:LinkButton>
                     </div>
                <h4 class="modal-title" id="myModalLabel">Update Refund</h4>
            </div>
            <div class="modal-body paddnone">
                <div class="panel-body">
                    <div class="formainline">
                        <div class="form-group">
                            <label>Pay Method</label>
                            <asp:DropDownList ID="ddlPayMethod" runat="server" AppendDataBoundItems="true"
                                aria-controls="DataTables_Table_0" class="myval">
                                <asp:ListItem Value="">Select</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlPayMethod"
                                ErrorMessage="This value is required." ValidationGroup="AddNotes" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <label>Paid Date</label>
                            <span class="dateimg">
                                <asp:TextBox ID="txtPaidDate" runat="server" Width="100px" CssClass="form-control"></asp:TextBox>
                                <asp:ImageButton ID="Image20" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                <cc1:CalendarExtender ID="CalendarExtender20" runat="server" PopupButtonID="Image20"
                                    TargetControlID="txtPaidDate" Format="dd/MM/yyyy">
                                </cc1:CalendarExtender>
                                <asp:RegularExpressionValidator ValidationGroup="AddNotes" ControlToValidate="txtPaidDate" ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter valid date"
                                    ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$" Display="Dynamic"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPaidDate"
                                    ErrorMessage="This value is required." ValidationGroup="AddNotes" Display="Dynamic"></asp:RequiredFieldValidator>
                            </span>
                        </div>
                        <div class="form-group modaldesc">
                            <label>Remarks</label>
                            <asp:TextBox ID="txtRemarks" TextMode="MultiLine" runat="server" Width="300px" Height="60px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqvaltxtNotesName" runat="server" ControlToValidate="txtRemarks"
                                ErrorMessage="This value is required." ValidationGroup="AddNotes" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <span class="name">
                                <label class="control-label">&nbsp;</label>
                            </span>
                            <asp:Button ID="ibtnAddComment" runat="server" Text="Update" OnClick="ibtnAddComment_Onclick"
                                ValidationGroup="AddNotes" CssClass="btn btn-primary savewhiteicon" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<asp:Button ID="btnNULL" Style="display: none;" runat="server" />
<asp:HiddenField ID="hndRefundID" runat="server" />
<style type="text/css">
    .selected_row {
        background-color: #A1DCF2!important;
    }
</style>
<script type="text/javascript">
    $(function () {
        $("[id*=GridViewRefund] td").bind("click", function () {
            var row = $(this).parent();
            $("[id*=GridViewRefund] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    $("td", this).removeClass("selected_row");
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("selected_row")) {
                    $(this).addClass("selected_row");
                } else {
                    $(this).removeClass("selected_row");
                }
            });
        });
        $("[id*=GridViewElecInv] td").bind("click", function () {
            var row = $(this).parent();
            $("[id*=GridViewElecInv] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    $("td", this).removeClass("selected_row");
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("selected_row")) {
                    $(this).addClass("selected_row");
                } else {
                    $(this).removeClass("selected_row");
                }
            });
        });
    });
</script>
