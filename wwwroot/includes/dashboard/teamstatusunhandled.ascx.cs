﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_teamstatusunhandled : System.Web.UI.UserControl
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        if (!IsPostBack)
        {
            rptTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectASC();
            rptTeam.DataBind();
        }
    }
    protected void rptTeam_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndSalesTeamID = (HiddenField)e.Item.FindControl("hndSalesTeamID");
            Repeater rptTeamCount = (Repeater)e.Item.FindControl("rptTeamCount");

            rptTeamCount.DataSource = ClsDashboard.TeamStatus_UnhandledLead(hndSalesTeamID.Value);
            rptTeamCount.DataBind();
        }
    }
   
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ModalPopupExtender2.Show();
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = ClsDashboard.TeamStatus_UnhandledLeadDetail(hndID.Value);
        GridView1.DataBind();
    }
    protected void rptTeamCount_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.CommandName.ToLower() == "detail")
            {
                string SalesTeamID = e.CommandArgument.ToString();
                hndID.Value = e.CommandArgument.ToString();
                SttblSalesTeams st = ClstblSalesTeams.tblSalesTeams_SelectBySalesTeamID(SalesTeamID);
                lblDetail.Text = st.SalesTeam;

                GridView1.DataSource = ClsDashboard.TeamStatus_UnhandledLeadDetail(SalesTeamID);
                GridView1.DataBind();
                ModalPopupExtender2.Show();
            }
        }
    }
}