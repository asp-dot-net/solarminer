﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_teamstatusdeprec : System.Web.UI.UserControl
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;

        if (!IsPostBack)
        {
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("Lead Manager"))
            {
                BindTeam();
            }
        }
    }
    public void BindTeam()
    {
        rptTeam.DataSource = ClstblSalesTeams.tblSalesTeams_SelectDFI();
        rptTeam.DataBind();
    }
    protected void rptTeam_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            HiddenField hndSalesTeamID = (HiddenField)e.Item.FindControl("hndSalesTeamID");
            Repeater rptTeamCount = (Repeater)e.Item.FindControl("rptTeamCount");

            rptTeamCount.DataSource = ClsDashboard.TeamStatus_DepRec(hndSalesTeamID.Value, ddldays.SelectedValue);
            rptTeamCount.DataBind();
        }
    }
    public void BindScript()
    {
        ScriptManager.RegisterStartupScript(UpdateTeamStatus, this.GetType(), "MyAction", "BindTeam();", true);
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        ModalPopupExtender2.Show();
        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataSource = ClsDashboard.TeamStatus_DepRecDetail(hndID.Value, ddldays.SelectedValue);
        GridView1.DataBind();
    }
    protected void rptTeamCount_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (e.CommandName.ToLower() == "detail")
            {
                string SalesTeamID = e.CommandArgument.ToString();
                hndID.Value = e.CommandArgument.ToString();
                SttblSalesTeams st = ClstblSalesTeams.tblSalesTeams_SelectBySalesTeamID(SalesTeamID);
                lblDetail.Text = st.SalesTeam;

                GridView1.DataSource = ClsDashboard.TeamStatus_DepRecDetail(SalesTeamID, ddldays.SelectedValue);
                GridView1.DataBind();
                ModalPopupExtender2.Show();
                BindScript();
            }
        }
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {
        BindTeam();
        BindScript();
    }
}