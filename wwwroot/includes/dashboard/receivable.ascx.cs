﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_dashboard_receivable : System.Web.UI.UserControl
{
    static DataView dv;
    static DataView dv3;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Roles.IsUserInRole("PostInstaller") || Roles.IsUserInRole("Installation Manager"))
            {
                ddlCash.DataSource = ClstblFPTransType.tblFPTransType_SelectActive();
                ddlCash.DataValueField = "FPTransTypeID";
                ddlCash.DataTextField = "FPTransType";
                ddlCash.DataMember = "FPTransType";
                ddlCash.DataBind();

                ddlFinance.DataSource = ClstblFinanceWith.tblFinanceWith_SelectByIsActive();
                ddlFinance.DataValueField = "FinanceWithID";
                ddlFinance.DataTextField = "FinanceWith";
                ddlFinance.DataMember = "FinanceWith";
                ddlFinance.DataBind();

                BindGrid(0);
            }
        }
    }

    protected DataTable GetGridDataCash()
    {
        DataTable dt1 = new DataTable();
        dt1 = ClsDashboard.tblProjects_CashReceivable(ddlCash.SelectedValue);

        int iTotalRecords = dt1.Rows.Count;
        int iEndRecord = GridViewCash.PageSize * (GridViewCash.PageIndex + 1);
        int iStartsRecods = (iEndRecord + 1) - GridViewCash.PageSize;
        if (iEndRecord > iTotalRecords)
        {
            iEndRecord = iTotalRecords;
        }
        if (iStartsRecods == 0)
        {
            iStartsRecods = 1;
        }
        if (iEndRecord == 0)
        {
            iEndRecord = iTotalRecords;
        }
        // ltrPage4.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt1;
    }

    protected DataTable GetGridDataFinance()
    {
        DataTable dt2 = new DataTable();
        dt2 = ClsDashboard.tblProjects_FinanceReceivable(ddlFinance.SelectedValue);

        //Response.Write(dt2.Rows.Count);
        //Response.End();

        int iTotalRecords = dt2.Rows.Count;
        int iEndRecord = GridViewFinance.PageSize * (GridViewFinance.PageIndex + 1);
        int iStartsRecods = (iEndRecord + 1) - GridViewFinance.PageSize;
        if (iEndRecord > iTotalRecords)
        {
            iEndRecord = iTotalRecords;
        }
        if (iStartsRecods == 0)
        {
            iStartsRecods = 1;
        }
        if (iEndRecord == 0)
        {
            iEndRecord = iTotalRecords;
        }
        // ltrPage4.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        return dt2;
    }

    public void BindGrid(int deleteFlag)
    {
        DataTable dt1 = new DataTable();
        dt1 = GetGridDataCash();
        dv = new DataView(dt1);


        if (dt1.Rows.Count > 0)
        {
            GridViewCash.DataSource = dt1;
            GridViewCash.DataBind();
        }


        DataTable dt2 = new DataTable();
        dt2 = GetGridDataFinance();

        dv3 = new DataView(dt2);
        //Response.Write(dt2.Rows.Count);
        //Response.End();
        if (dt2.Rows.Count > 0)
        {
            GridViewFinance.DataSource = dt2;
            GridViewFinance.DataBind();
        }

    }

    protected void GridViewCash_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewCash.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    protected void GridViewFinance_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridViewFinance.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    protected void btnGo_Click(object sender, EventArgs e)
    {

    }
    protected void btnGo2_Click(object sender, EventArgs e)
    {

    }
    void lbCash_Command(object sender, CommandEventArgs e)
    {
        GridViewCash.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);

    }
    protected void GridViewCash_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridViewCash.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridViewCash.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridViewCash.PageIndex - 2;
        page[1] = GridViewCash.PageIndex - 1;
        page[2] = GridViewCash.PageIndex;
        page[3] = GridViewCash.PageIndex + 1;
        page[4] = GridViewCash.PageIndex + 2;
        page[5] = GridViewCash.PageIndex + 3;
        page[6] = GridViewCash.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridViewCash.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridViewCash.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridViewCash.PageIndex == GridViewCash.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv.ToTable().Rows.Count;
            int iEndRecord = GridViewCash.PageSize * (GridViewCash.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridViewCash.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    protected void GridViewCash_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lbCash_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lbCash_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lbCash_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lbCash_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lbCash_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lbCash_Command);
        }
    }
    void lbFinance_Command(object sender, CommandEventArgs e)
    {
        GridViewFinance.PageIndex = Convert.ToInt32(e.CommandArgument) - 1;
        BindGrid(0);

    }
    protected void GridViewFinance_DataBound(object sender, EventArgs e)
    {
        GridViewRow gvrow = GridViewFinance.BottomPagerRow;
        Label lblcurrentpage = (Label)gvrow.Cells[0].FindControl("CurrentPage");
        lblcurrentpage.Text = Convert.ToString(GridViewFinance.PageIndex + 1);
        int[] page = new int[7];
        page[0] = GridViewFinance.PageIndex - 2;
        page[1] = GridViewFinance.PageIndex - 1;
        page[2] = GridViewFinance.PageIndex;
        page[3] = GridViewFinance.PageIndex + 1;
        page[4] = GridViewFinance.PageIndex + 2;
        page[5] = GridViewFinance.PageIndex + 3;
        page[6] = GridViewFinance.PageIndex + 4;
        for (int i = 0; i < 7; i++)
        {
            if (i != 3)
            {
                if (page[i] < 1 || page[i] > GridViewFinance.PageCount)
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Visible = false;
                }
                else
                {
                    LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("p" + Convert.ToString(i));
                    lnkbtn.Text = Convert.ToString(page[i]);
                    lnkbtn.CommandName = "PageNo";
                    lnkbtn.CommandArgument = lnkbtn.Text;

                }
            }
        }
        if (GridViewFinance.PageIndex == 0)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton1");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton2");
            lnkbtn.Visible = false;

        }
        if (GridViewFinance.PageIndex == GridViewFinance.PageCount - 1)
        {
            LinkButton lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton3");
            lnkbtn.Visible = false;
            lnkbtn = (LinkButton)gvrow.Cells[0].FindControl("LinkButton4");
            lnkbtn.Visible = false;

        }
        Label ltrPage = (Label)gvrow.Cells[0].FindControl("ltrPage");
        if (dv3.ToTable().Rows.Count > 0)
        {
            int iTotalRecords = dv3.ToTable().Rows.Count;
            int iEndRecord = GridViewCash.PageSize * (GridViewCash.PageIndex + 1);
            int iStartsRecods = (iEndRecord + 1) - GridViewCash.PageSize;
            if (iEndRecord > iTotalRecords)
            {
                iEndRecord = iTotalRecords;
            }
            if (iStartsRecods == 0)
            {
                iStartsRecods = 1;
            }
            if (iEndRecord == 0)
            {
                iEndRecord = iTotalRecords;
            }
            ltrPage.Text = "Showing " + iStartsRecods + " to " + iEndRecord + " of " + iTotalRecords + " entries";
        }
        else
        {
            ltrPage.Text = "";
        }
    }
    protected void GridViewFinance_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            GridViewRow gvr = e.Row;
            LinkButton lb = (LinkButton)gvr.Cells[0].FindControl("p0");
            lb.Command += new CommandEventHandler(lbFinance_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p1");
            lb.Command += new CommandEventHandler(lbFinance_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p2");
            lb.Command += new CommandEventHandler(lbFinance_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p4");
            lb.Command += new CommandEventHandler(lbFinance_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p5");
            lb.Command += new CommandEventHandler(lbFinance_Command);
            lb = (LinkButton)gvr.Cells[0].FindControl("p6");
            lb.Command += new CommandEventHandler(lbFinance_Command);
        }
    }
}