﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="statewisepre.ascx.cs" Inherits="includes_dashboard_statewisepre" %>
<script>
    $(document).ready(function () {
        $.jqplot.config.enablePlugins = true;
        var s1 = [<%=jscharts1 %>];
        var ticks = [<%=jschartticks %>];

         plot1 = $.jqplot('chart4', [s1], {
             // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
             animate: !$.jqplot.use_excanvas,
             seriesDefaults: {
                 renderer: $.jqplot.BarRenderer,
                 pointLabels: { show: true }
             },
             axes: {
                 xaxis: {
                     renderer: $.jqplot.CategoryAxisRenderer,
                     ticks: ticks
                 }
             },
             highlighter: { show: false }
         });

         $('#chart4').bind('jqplotDataClick',
             function (ev, seriesIndex, pointIndex, data) {
                 $('#info1').html('series: ' + seriesIndex + ', point: ' + pointIndex + ', data: ' + data);
             }
         );
     });
</script>

<div class="col-md-6">
    <div class="panlenew">
        <section class="panel">
            <div class="panel-heading">
                <div class="pull-right">
                    <div class="btn-group">
                        <asp:DropDownList ID="ddlView" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                            <asp:ListItem Value="">All</asp:ListItem>
                            <asp:ListItem Value="1">Day</asp:ListItem>
                            <asp:ListItem Value="2">Week</asp:ListItem>
                            <asp:ListItem Value="3">Month</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:Button ID="btnGo" runat="server" CssClass="btn btn-primary" Text="Go" OnClick="btnGo_Click" />
                </div>
                <a href="#">State Wise Chart</a>
            </div>
            <div class="panel-body paddnone" id="divMain" runat="server">
                <div class="row m-n">
                    <div id="chart4"></div>
                </div>
            </div>
        </section>
    </div>
</div>