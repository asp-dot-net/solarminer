﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Setting.ascx.cs" Inherits="includes_Profile_Contact" %>
<div id="settings" class="tab-pane">
    <form role="form">
        <div class="form-title">
            Personal Information
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <span class="input-icon icon-right">
                        <asp:TextBox ID="txtname" runat="server" class="form-control" placeholder="Name"></asp:TextBox>
                        <i class="fa fa-user blue"></i>
                    </span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <span class="input-icon icon-right">
                        <asp:TextBox ID="txtfamily" runat="server" class="form-control" placeholder="Family"></asp:TextBox>
                        <i class="fa fa-user orange"></i>
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <span class="input-icon icon-right">
                        <asp:TextBox ID="txtphone" runat="server" class="form-control" placeholder="Phone"></asp:TextBox>
                        <i class="glyphicon glyphicon-earphone yellow"></i>
                    </span>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <span class="input-icon icon-right">
                        <asp:TextBox ID="txtmobile" runat="server" class="form-control" placeholder="Mobile"></asp:TextBox>
                        <i class="glyphicon glyphicon-phone palegreen"></i>
                    </span>
                </div>
            </div>
        </div>
        <hr class="wide">
        <div class="row">
            <div class="col-sm-6">
                <div class="input-group date datetimepicker1">
                    <span class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                    </span>
                    <asp:TextBox ID="txtbirthdate" runat="server" class="form-control">
                    </asp:TextBox>

                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <span class="input-icon icon-right">
                        <input type="text" class="form-control" placeholder="Birth Place">
                        <i class="fa fa-globe"></i>
                    </span>
                </div>
            </div>
        </div>



     
        <button type="submit" class="btn btn-primary">Save Profile</button>
    </form>
</div>
<div class="loaderPopUP">
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
        }

        function pageLoadedpro() {

            $('.loading-container').css('display', 'none');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $("[data-toggle=tooltip]").tooltip();
            $('.datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }
    </script>

</div>
