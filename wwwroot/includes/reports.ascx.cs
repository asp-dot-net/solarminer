﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_reports : System.Web.UI.UserControl
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        string pagename = Utilities.GetPageName(Request.Url.PathAndQuery).ToLower();

        if (!IsPostBack)
        {
            switch (pagename)
            {
                case "salesreport":
                    ddlReport.SelectedValue = "salesreport";
                    break;
                case "noinstalldate":
                    ddlReport.SelectedValue = "noinstalldate";
                    break;
                case "installdate":
                    ddlReport.SelectedValue = "installdate";
                    break;
                case "panelscount":
                    ddlReport.SelectedValue = "panelscount";
                    break;
                case "accountreceive":
                    ddlReport.SelectedValue = "accountreceive";
                    break;
                case "paymentstatus":
                    ddlReport.SelectedValue = "paymentstatus";
                    break;
                case "weeklyreport":
                    ddlReport.SelectedValue = "weeklyreport";
                    break;
                case "leadassignreport":
                    ddlReport.SelectedValue = "leadassignreport";
                    break;
                case "stockreport":
                    ddlReport.SelectedValue = "stockreport";
                    break;
                case "leadtrackreport":
                    ddlReport.SelectedValue = "leadtrackreport";
                    break;
                case "panelstock":
                    ddlReport.SelectedValue = "panelstock";
                    break;
                case "rooftypecount":
                    ddlReport.SelectedValue = "rooftypecount";
                    break;
                case "capkwcount":
                    ddlReport.SelectedValue = "capkwcount";
                    break;
                case "statustrack":
                    ddlReport.SelectedValue = "statustrack";
                    break;
                case "formbaydocsreport":
                    ddlReport.SelectedValue = "formbaydocsreport";
                    break;
            }
        }
    }
    protected void ddlReport_SelectedIndexChanged(object sender, EventArgs e)
    {
        switch (ddlReport.SelectedValue)
        {
            case "salesreport":
                Response.Redirect(SiteURL + "admin/adminfiles/reports/salesreport.aspx");
                break;
            case "noinstalldate":
                Response.Redirect(SiteURL + "admin/adminfiles/reports/noinstalldate.aspx");
                break;
            case "installdate":
                Response.Redirect(SiteURL + "admin/adminfiles/reports/installdate.aspx");
                break;
            case "panelscount":
                Response.Redirect(SiteURL + "admin/adminfiles/reports/panelscount.aspx");
                break;
            case "accountreceive":
                Response.Redirect(SiteURL + "admin/adminfiles/reports/accountreceive.aspx");
                break;
            case "paymentstatus":
                Response.Redirect(SiteURL + "admin/adminfiles/reports/paymentstatus.aspx");
                break;
            case "weeklyreport":
                Response.Redirect(SiteURL + "admin/adminfiles/reports/weeklyreport.aspx");
                break;
            case "leadassignreport":
                Response.Redirect(SiteURL + "admin/adminfiles/reports/leadassignreport.aspx");
                break;
            case "stockreport":
                Response.Redirect(SiteURL + "admin/adminfiles/reports/stockreport.aspx");
                break;
            case "leadtrackreport":
                Response.Redirect(SiteURL + "admin/adminfiles/reports/leadtrackreport.aspx");
                break;
            case "panelstock":
                Response.Redirect(SiteURL + "admin/adminfiles/reports/panelstock.aspx");
                break;
            case "rooftypecount":
                Response.Redirect(SiteURL + "admin/adminfiles/reports/rooftypecount.aspx");
                break;
            case "capkwcount":
                Response.Redirect(SiteURL + "admin/adminfiles/reports/capkwcount.aspx");
                break;
            case "statustrack":
                Response.Redirect(SiteURL + "admin/adminfiles/reports/statustrack.aspx");
                break;
            case "formbaydocsreport":
                Response.Redirect(SiteURL + "admin/adminfiles/reports/formbaydocsreport.aspx");
                break;
        }
    }
}