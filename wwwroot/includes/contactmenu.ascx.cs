﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class includes_contactmenu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string pagename = Utilities.GetPageName(Request.Url.PathAndQuery).ToLower();
        if (pagename == "contactdetail")
        {
            li_updateDetail.Attributes.Add("class", "selected");
        }
        if (pagename == "contactnote")
        {
            li_updateNotes.Attributes.Add("class", "selected");
        }
        if (pagename == "promo")
        {
            li_updatePromo.Attributes.Add("class", "selected");
        }
        if (pagename == "contactinfo")
        {
            li_updateInfo.Attributes.Add("class", "selected");
        }
        if (pagename == "contactaccount")
        {
            li_createaccount.Attributes.Add("class", "selected");
        }

        if (!IsPostBack)
        {
            if (Request.QueryString["val"] == "view")
            {
                hypDetail.NavigateUrl = "~/admin/adminfiles/company/contactdetail.aspx?id=" + Request.QueryString["id"] + "&val=view";
                hypNotes.NavigateUrl = "~/admin/adminfiles/master/contactnote.aspx?id=" + Request.QueryString["id"] + "&val=view";
                hypPromo.NavigateUrl = "~/admin/adminfiles/master/promo.aspx?id=" + Request.QueryString["id"] + "&val=view";
                hypInfo.NavigateUrl = "~/admin/adminfiles/company/contactinfo.aspx?id=" + Request.QueryString["id"] + "&val=view";
            }
            else
            {
                hypDetail.NavigateUrl = "~/admin/adminfiles/company/contactdetail.aspx?id=" + Request.QueryString["id"];
                hypNotes.NavigateUrl = "~/admin/adminfiles/master/contactnote.aspx?id=" + Request.QueryString["id"];
                hypPromo.NavigateUrl = "~/admin/adminfiles/master/promo.aspx?id=" + Request.QueryString["id"];
                hypInfo.NavigateUrl = "~/admin/adminfiles/company/contactinfo.aspx?id=" + Request.QueryString["id"];
            }
        }
    }
}