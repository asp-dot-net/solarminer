<%@ Control Language="C#" AutoEventWireup="true" CodeFile="promo.ascx.cs" Inherits="includes_controls_promo" %>
<style type="text/css">
        .selected_row {
            background-color: #A1DCF2!important;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            $("[id*=GridView1] td").bind("click", function () {
                var row = $(this).parent();
                $("[id*=GridView1] tr").each(function () {
                    if ($(this)[0] != row[0]) {
                        $("td", this).removeClass("selected_row");
                    }
                });
                $("td", row).each(function () {
                    if (!$(this).hasClass("selected_row")) {
                        $(this).addClass("selected_row");
                    } else {
                        $(this).removeClass("selected_row");
                    }
                });
            });
        });
    </script>
<script>
    function doMyAction() {
        $(".myval").select2({
            //placeholder: "select",
            allowclear: true
        });
    }
</script>
<section class="row m-b-md" id="pancontactpromo" runat="server">
    <div class="col-sm-12">
        <div class="contactsarea">
            <div class="messesgarea">
                <div class="alert alert-success" id="PanSuccess" runat="server">
                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                </div>
                <div class="alert alert-danger" id="PanError" runat="server">
                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server"
                        Text="Transaction Failed."></asp:Label></strong>
                </div>
                <div class="alert alert-info" id="PanNoRecord" runat="server">
                    <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in this view</strong>
                </div>
                <div class="alert alert-danger" id="PanAlreadExists" runat="server">
                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                </div>
            </div>
            <asp:Panel ID="PanGrid" runat="server">
                <div class="contacttoparea paddleftright10" id="PanSearch" runat="server">
                    <div class="leftarea1">
                        <div class="showenteries">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>Show</td>
                                    <td>
                                        <asp:DropDownList ID="ddlSelectRecords" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                            aria-controls="DataTables_Table_0" class="myval1">
                                        </asp:DropDownList></td>
                                    <td>entries</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div>
                        &nbsp;
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="contactbottomarea">
                    <div class="tableblack">
                        <div class="table-responsive">
                            <asp:GridView ID="GridView1" DataKeyNames="ContactID" runat="server" AllowPaging="true"
                                PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false" PageSize="10"
                                OnPageIndexChanging="GridView1_PageIndexChanging"
                                AllowSorting="true" class="table table-bordered table-hover">
                                <Columns>

                                    <asp:TemplateField HeaderText="Date Sent" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                        ItemStyle-HorizontalAlign="left" ItemStyle-Width="150px">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hndid" runat="server" Value='<%# Eval("PromoID") %>' />
                                            <%# DataBinder.Eval(Container.DataItem, "PromoSent", "{0:dd/MM/yyyy}") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Promo" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <%# Eval("Promo")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Promo Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="150px">
                                        <ItemTemplate>
                                            <%# Eval("promotype")%>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Int" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                        ItemStyle-HorizontalAlign="Left" ItemStyle-Width="40px">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chktag" runat="server" OnCheckedChanged="chktag_OnCheckedChanged" AutoPostBack="true" Checked='<%# Eval("Interested") %>' />
                                            <label for="<%=chktag.ClientID %>" runat="server" id="lblchk">
                                                <span></span>
                                            </label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</section>
