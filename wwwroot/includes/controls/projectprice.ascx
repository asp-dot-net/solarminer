<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectprice.ascx.cs"
    Inherits="includes_controls_projectprice" %>

<script>
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoadedsalePrice);
    function pageLoadedsalePrice() {
        $(".myvalproprice").select2({
            //placeholder: "select",
            allowclear: true
        });
    }
</script>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
     
        <section class="row m-b-md" id="panprojectprice" runat="server">
            <div class="col-sm-12 minhegihtarea">
                <div class="contactsarea projectprice">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                        </div>
                    </div>
                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate pricetab">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="pricesectiondiv row">
                                    <div class="varitionsmain">
                                        <div class="divall">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <span class="name" style="float: left;">
                                                        <label class="control-label disblock" style="font-size: 14px; padding-top: 6px;">
                                                            Basic System Cost
                                                        </label>
                                                    </span>
                                                    <span class="floatleftin width120">
                                                        <asp:TextBox ID="txtBasicSystemCost" TabIndex="1" runat="server" MaxLength="15" Width="100px" CssClass="form-control alignright floatleft dolarsingn" OnTextChanged="txtBasicSystemCost_TextChanged"
                                                            AutoPostBack="true"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtBasicSystemCost"
                                                            ValidationGroup="price" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group" style="padding-left: 66px;">
                                                    <span>
                                                        <asp:TextBox ID="txtVariations" TabIndex="2" runat="server" CssClass="form-control" Enabled="false">
                                                        </asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8">

                                <div class="pricesectiondiv">
                                    <div class="varitionsmain">
                                        <h5 class="colorblack martop15">Variations</h5>
                                        <div class="divall">
                                            <div class="divfirst">
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            House Type
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtHouseType" TabIndex="2" runat="server" MaxLength="15" CssClass="form-control alignright dolarsingn"
                                                            OnTextChanged="txtHouseType_TextChanged" AutoPostBack="true" Width="100px"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtHouseType"
                                                            ValidationGroup="price" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="divsecond">
                                                <div class="form-group">
                                                    <span>
                                                        <asp:TextBox ID="txtHouseTypePrice" TabIndex="3" runat="server" class="form-control"
                                                            Enabled="false"></asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="divall">
                                            <div class="divfirst">
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Roof Type
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtRoofType" TabIndex="4" runat="server" MaxLength="15" CssClass="form-control alignright dolarsingn"
                                                            OnTextChanged="txtRoofType_TextChanged" AutoPostBack="true" Width="100px"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtRoofType"
                                                            ValidationGroup="price" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="divsecond">
                                                <div class="form-group">
                                                    <span>
                                                        <asp:TextBox ID="txtRoofTypePrice" TabIndex="5" runat="server" class="form-control"
                                                            Enabled="false"></asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="divall">
                                            <div class="divfirst">
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Roof Angle
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtRoofAngle" TabIndex="6" runat="server" MaxLength="15" CssClass="form-control alignright dolarsingn"
                                                            OnTextChanged="txtRoofAngle_TextChanged" AutoPostBack="true" Width="100px"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ControlToValidate="txtRoofAngle"
                                                            ValidationGroup="price" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="divsecond">
                                                <div class="form-group">
                                                    <span>
                                                        <asp:TextBox ID="txtRoofAnglePrice" TabIndex="7" runat="server" class="form-control"
                                                            Enabled="false"></asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="divall">
                                            <div class="divfirst">
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Meter Installation
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtMeterInstallation" TabIndex="8" runat="server" MaxLength="15"
                                                            Width="100px" CssClass="form-control alignright dolarsingn" Text="0" OnTextChanged="txtMeterInstallation_TextChanged"
                                                            AutoPostBack="true"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtMeterInstallation"
                                                            ValidationGroup="price" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="divsecond">
                                                <div class="form-group padtop5">
                                                    <div class=" checkbox-info checkbox">
                                                        <span class="fistname">
                                                            <label for="<%=chkMeterEnoughSpace.ClientID %>">
                                                                <asp:CheckBox ID="chkMeterEnoughSpace" TabIndex="9" runat="server" AutoPostBack="true"
                                                                    OnCheckedChanged="chkMeterEnoughSpace_CheckedChanged" />
                                                                <span class="text">&nbsp;</span>
                                                            </label>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="divall">
                                            <div class="divfirst">
                                                <div class="form-group ">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Meter Upgrade
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtMeterUpgrade" TabIndex="10" runat="server" MaxLength="15" CssClass="form-control alignright dolarsingn"
                                                            Width="100px" Text="0" OnTextChanged="txtMeterUpgrade_TextChanged" AutoPostBack="true"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                                                            ValidationGroup="price" ControlToValidate="txtMeterUpgrade" Display="Dynamic"
                                                            ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="divsecond">
                                                <div class="form-group padtop5">
                                                    <div class=" checkbox-info checkbox">
                                                        <span class="fistname">
                                                            <label for="<%=chkMeterUG.ClientID %>">
                                                                <asp:CheckBox ID="chkMeterUG" TabIndex="11" runat="server" AutoPostBack="true" OnCheckedChanged="chkMeterUG_CheckedChanged" />
                                                                <span class="text">&nbsp;</span>
                                                            </label>
                                                        </span>
                                                    </div>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="divall">
                                            <div class="divfirst">
                                                <div class="form-group ">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Asbestos
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtAsbestos" TabIndex="12" runat="server" MaxLength="15" CssClass="form-control alignright dolarsingn"
                                                            Width="100px" Text="0" OnTextChanged="txtAsbestos_TextChanged" AutoPostBack="true"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server"
                                                            ValidationGroup="price" ControlToValidate="txtAsbestos" Display="Dynamic" ErrorMessage="Number Only"
                                                            ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="divsecond">
                                                <div class="form-group padtop5">
                                                    <div class=" checkbox-info checkbox">
                                                        <span class="fistname">
                                                            <label for="<%=chkAsbestoss.ClientID %>">
                                                                <asp:CheckBox ID="chkAsbestoss" TabIndex="12" runat="server" AutoPostBack="true"
                                                                    OnCheckedChanged="chkAsbestoss_CheckedChanged" />
                                                                <span class="text">&nbsp;</span>
                                                            </label>
                                                        </span>
                                                    </div>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="divall">
                                            <div class="divfirst">
                                                <div class="form-group ">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Split System
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtSplitSystem" TabIndex="13" runat="server" MaxLength="15" CssClass="form-control alignright dolarsingn"
                                                            Width="100px" Text="0" OnTextChanged="txtSplitSystem_TextChanged" AutoPostBack="true"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                                                            ValidationGroup="price" ControlToValidate="txtSplitSystem" Display="Dynamic"
                                                            ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="divsecond">
                                                <div>
                                                    <span class="name">
                                                        <table border="0" cellpadding="0" cellspacing="0" class="pricetable">
                                                            <tr>
                                                                <td style="padding-right: 7px; vertical-align: middle">
                                                                    <div class=" checkbox-info checkbox">
                                                                        <span class="fistname">
                                                                            <label for="<%=chkSplitSystem.ClientID %>">
                                                                                <asp:CheckBox ID="chkSplitSystem" TabIndex="14" runat="server" AutoPostBack="true"
                                                                                    OnCheckedChanged="chkSplitSystem_CheckedChanged" />
                                                                                <span class="text">&nbsp;</span>
                                                                            </label>
                                                                        </span>
                                                                    </div>
                                                                </td>
                                                                <td style="padding-right: 7px; padding-top: 13px; vertical-align: top">
                                                                    <label class="control-label">
                                                                        NW
                                                                    </label>
                                                                </td>
                                                                <td style="padding-right: 15px; padding-top: 4px; vertical-align: top" class="newtexbox">
                                                                    <asp:TextBox ID="txtNW" runat="server" MaxLength="20" TabIndex="15" CssClass="form-control padd4" AutoPostBack="true" OnTextChanged="txtNW_TextChanged"></asp:TextBox>
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator18" runat="server"
                                                                        ValidationGroup="price" ControlToValidate="txtNW" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                                </td>
                                                                <td style="padding-right: 7px; padding-top: 13px; vertical-align: top">
                                                                    <label class="control-label">
                                                                        OTH&nbsp;</label>
                                                                </td>
                                                                <td class="newtexbox" style="padding-top: 4px; vertical-align: top">
                                                                    <asp:TextBox ID="txtOTH" runat="server" MaxLength="10" TabIndex="16" CssClass="form-control padd4"
                                                                        OnTextChanged="txtOTH_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator19" runat="server"
                                                                        ValidationGroup="price" ControlToValidate="txtOTH" Display="Dynamic" ErrorMessage="Number Only"
                                                                        ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="divall">
                                            <div class="divfirst">
                                                <div class="form-group dateimgarea">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Cherry Picker
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtCherryPicker" TabIndex="17" runat="server" MaxLength="15" CssClass="form-control alignright dolarsingn"
                                                            Text="0" OnTextChanged="txtCherryPicker_TextChanged" AutoPostBack="true" Width="100px"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server"
                                                            ValidationGroup="price" ControlToValidate="txtCherryPicker" Display="Dynamic"
                                                            ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="divsecond">
                                                <div class="form-group">
                                                    <div class=" checkbox-info checkbox">
                                                        <span class="fistname">
                                                            <label for="<%=chkCherryPicker.ClientID %>">
                                                                <asp:CheckBox ID="chkCherryPicker" TabIndex="18" runat="server" AutoPostBack="true"
                                                                    OnCheckedChanged="chkCherryPicker_CheckedChanged" />
                                                                <span class="text">&nbsp;</span>
                                                            </label>
                                                        </span>
                                                    </div>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <%--<div class="divall">
                                            <div class="divfirst">
                                                <div class="form-group ">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Travel Cost
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtTravelCost" TabIndex="19" runat="server" MaxLength="15" CssClass="form-control alignright dolarsingn"
                                                            Enabled="false" Width="100px"></asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>--%>


                                        <div class="divall">
                                            <div class="divfirst">
                                                <div class="form-group ">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Installation Charge
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtInstallationCharges" TabIndex="19" runat="server" Enabled="False" Text="0.00" MaxLength="15" CssClass="form-control alignright dolarsingn"
                                                             Width="100px"></asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>


                                           <%-- <div class="divfirst">
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Hot Water Meter
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txthotmeter" TabIndex="23" runat="server" MaxLength="15" OnTextChanged="txthotmeter_TextChanged"
                                                            Width="100px" CssClass="form-control alignright dolarsingn" 
                                                            AutoPostBack="true"></asp:TextBox>--%>

                                           <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                            ValidationGroup="price" ControlToValidate="txthotmeter" Display="Dynamic"
                                                            ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>--%>

                                           <div class="divfirst">
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">
                                                           Additional Charges
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtAdditionalCharges" TabIndex="23" runat="server" Text="0.00" MaxLength="15" OnTextChanged="txtAdditionalCharges_TextChanged"
                                                            Width="100px" CssClass="form-control alignright dolarsingn" 
                                                            AutoPostBack="true"></asp:TextBox>
                                                         
                                                        



                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                            ValidationGroup="price" ControlToValidate="txtAdditionalCharges" Display="Dynamic"
                                                            ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="divsecond">
                                                <div class="form-group">
                                                    <span class="dateimg">
                                                        <asp:TextBox ID="txtVariantAdditionalCharges" TabIndex="22" runat="server" MaxLength="200"
                                                         CssClass="form-control" Width="200px"></asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div><div class="clear">
                                                    </div>
                                             <div class="divfirst">
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Smart Meter
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtsmartmeter" TabIndex="23" runat="server" MaxLength="15"
                                                            Width="100px" CssClass="form-control alignright dolarsingn"  OnTextChanged="txtsmartmeter_TextChanged"
                                                            AutoPostBack="true"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                            ValidationGroup="price" ControlToValidate="txtsmartmeter" Display="Dynamic"
                                                            ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="divsecond">
                                                <div class="form-group dateimgarea">
                                                    <span class="dateimg newtexbox">
                                                        <asp:TextBox ID="txtTravelTime" TabIndex="20" runat="server" Text="20" CssClass="form-control floatleft" MaxLength="10" Width="80px" OnTextChanged="txtTravelTime_TextChanged" AutoPostBack="true"> </asp:TextBox>
                                                        &nbsp;KM
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator20" runat="server"
                                                    ValidationGroup="price" ControlToValidate="txtTravelTime" Display="Dynamic" ErrorMessage="Number Only"
                                                    ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="divall">
                                            <div class="divfirst">
                                                <div class="form-group">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tr>

                                                            <td><span class="name">
                                                                <label class="control-label">
                                                                    Other
                                                                </label>
                                                            </span></td>
                                                            <td><span class="floatleftin">
                                                                <asp:TextBox ID="txtOther" TabIndex="21" runat="server" MaxLength="15" CssClass="form-control alignright dolarsingn"
                                                                    Width="100px" OnTextChanged="txtOther_TextChanged" AutoPostBack="true"></asp:TextBox>

                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server"
                                                                    ValidationGroup="price" ControlToValidate="txtOther" Display="Dynamic" ErrorMessage="Number Only"
                                                                    ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                            </span></td>
                                                        </tr>
                                                    </table>


                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="divsecond">
                                                <div class="form-group">
                                                    <span class="dateimg">
                                                        <asp:TextBox ID="txtVariationOther" TabIndex="22" runat="server" MaxLength="200"
                                                            Text="0" CssClass="form-control" Width="200px"></asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="divall">                                     

                                            <div class="divfirst">
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Special Discount
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtSpecialDiscount" TabIndex="23" runat="server" MaxLength="15"
                                                            Width="100px" CssClass="form-control alignright dolarsingn" OnTextChanged="txtSpecialDiscount_TextChanged"
                                                            AutoPostBack="true"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                                                            ValidationGroup="price" ControlToValidate="txtSpecialDiscount" Display="Dynamic"
                                                            ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="divfirst">
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Collection Charge
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtcharge" TabIndex="23" runat="server" MaxLength="15"
                                                            Width="100px" CssClass="form-control alignright dolarsingn"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                            ValidationGroup="price" ControlToValidate="txtcharge" Display="Dynamic"
                                                            ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="divsecond" id="divusername" runat="server" visible="false">
                                                <div class="form-group">
                                                    <div class=" checkbox-info checkbox">
                                                        <span class="fistname">
                                                            <label for="<%=chkusername.ClientID %>">
                                                                <asp:CheckBox ID="chkusername" TabIndex="18" runat="server" AutoPostBack="true" />
                                                                <span class="text">Price Approved By </span>
                                                            </label>
                                                            <asp:Label ID="lblusername" runat="server" Text=""></asp:Label>
                                                        </span>
                                                    </div>

                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="divall">
                                            <div class="divfirst">
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            <strong>Total Cost</strong>
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtTotalCost" runat="server" Enabled="false" TabIndex="14" CssClass="form-control alignright dolarsingn"
                                                            Width="100px"></asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            Deposit Required
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtDepositRequired" runat="server" TabIndex="25" MaxLength="15"
                                                            Width="100px" CssClass="form-control alignright dolarsingn" OnTextChanged="txtDepositRequired_TextChanged"
                                                            AutoPostBack="true"></asp:TextBox>

                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server"
                                                            ValidationGroup="price" ControlToValidate="txtDepositRequired" Display="Dynamic"
                                                            ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="divsecond">
                                                <div id="Div1" class="form-group" runat="server" visible="false">
                                                    <span class="name">
                                                        <table border="0" cellpadding="0" cellspacing="0" class="pricetable">
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="btnDeposit" runat="server" TabIndex="27" ImageUrl="~/images/new/btn_10perdeposit.png"
                                                                        OnClick="btnDeposit_Click" CausesValidation="false" />&nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtDeposit" TabIndex="28" runat="server" Enabled="false" CssClass="form-control"
                                                                        Width="80px"></asp:TextBox>
                                                                </td>
                                                                <td><span class="doller floatleft">$</span></td>
                                                            </tr>
                                                        </table>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                                <div id="Div2" class="form-group" runat="server" visible="false">
                                                    <span class="fistdv">
                                                        <label class="control-label floatleft">
                                                            Calc. Balance Of&nbsp;
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtCalcBalance" runat="server" MaxLength="10" TabIndex="29" Width="100px"
                                                            CssClass="form-control" OnTextChanged="txtCalcBalance_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                        <asp:CompareValidator ID="CompareValidatorCalcBalance" TabIndex="26" runat="server"
                                                            ValidationGroup="price" ControlToValidate="txtCalcBalance" ErrorMessage="Must Enter LessThan Total Cost"
                                                            Display="Dynamic" Operator="LessThan" Type="Double"></asp:CompareValidator>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator21" runat="server"
                                                            ValidationGroup="price" ControlToValidate="txtCalcBalance" Display="Dynamic"
                                                            ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="divall">
                                            <div class="divfirst">
                                                <div class="form-group">
                                                    <span class="name">
                                                        <label class="control-label">
                                                            <strong>Bal to Pay</strong>
                                                        </label>
                                                    </span><span class="floatleftin">
                                                        <asp:TextBox ID="txtBaltoPay" runat="server" Enabled="false" TabIndex="30" Width="100px"
                                                            CssClass="form-control alignright dolarsingn"></asp:TextBox>
                                                    </span>
                                                    <div class="clear">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="form-group">

                                    <span class="name disblock">
                                        <label class="control-label">
                                            Payment Option
                                        </label>
                                    </span>
                                    <span class="dateimg" style="width: 150px;">
                                        <asp:DropDownList ID="ddlPaymentOption" TabIndex="31" runat="server"
                                            aria-controls="DataTables_Table_0" CssClass="myval" AutoPostBack="true"
                                            Width="312px" AppendDataBoundItems="true"
                                            OnSelectedIndexChanged="ddlPaymentOption_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </span>
                                    <div class="clear">
                                    </div>

                                </div>
                                <div class="form-group" id="divDepositOption" runat="server" visible="false">
                                    <span class="name disblock">
                                        <label class="control-label">
                                            Deposit Option
                                        </label>
                                    </span><span class="dateimg">
                                        <asp:DropDownList ID="ddlDepositOption" TabIndex="37" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                            Width="100px" AppendDataBoundItems="true">
                                            <asp:ListItem Value=""> </asp:ListItem>
                                        </asp:DropDownList>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group" id="divPaymentType" runat="server" visible="false">
                                    <span class="name disblock">
                                        <label class="control-label">
                                            Finance Payment Type
                                        </label>
                                    </span><span class="dateimg">
                                        <asp:DropDownList ID="ddlPaymentType" TabIndex="37" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                            AppendDataBoundItems="true">
                                            <asp:ListItem Value=""> </asp:ListItem>
                                        </asp:DropDownList>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="name disblock">
                                        <label class="control-label">
                                            Promo Offer1
                                        </label>
                                    </span><span class="dateimg">
                                        <asp:DropDownList ID="ddlPromo1ID" runat="server" TabIndex="32" aria-controls="DataTables_Table_0" CssClass="myval"
                                            AppendDataBoundItems="true" Style="width: 312px">
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                        </asp:DropDownList>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="name disblock">
                                        <label class="control-label">
                                            House Status
                                        </label>
                                    </span><span class="dateimg">
                                        <asp:DropDownList ID="ddlPromo2ID" TabIndex="33" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                            AppendDataBoundItems="true" Style="width: 312px">
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                        </asp:DropDownList>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="form-group ">

                                    <div class=" checkbox-info checkbox">
                                        <span class="fistname">
                                            <label for="<%=chkPromo3.ClientID %>">
                                                <asp:CheckBox ID="chkPromo3" runat="server" TabIndex="34" />
                                                <span class="text">Other &nbsp;</span>
                                            </label>
                                        </span>
                                    </div>

                                    <span class="dateimg">
                                        <asp:TextBox ID="txtPromoText" runat="server" MaxLength="200" Style="margin-top: 10px;"
                                            TextMode="MultiLine" Height="250px" CssClass="form-control" TabIndex="35"></asp:TextBox>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="divAddUpdate" runat="server">
                            <div class="col-md-12 textcenterbutton center-text">
                                <div>
                                    <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdatePrice" TabIndex="36" runat="server"
                                        ValidationGroup="price" OnClick="btnUpdatePrice_Click" Text="Save" CausesValidation="true" />
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </section>
    
        </ContentTemplate>
    <Triggers>
       <%-- <asp:PostBackTrigger ControlID="btnUpdatePrice" />--%>
    </Triggers>
</asp:UpdatePanel>





