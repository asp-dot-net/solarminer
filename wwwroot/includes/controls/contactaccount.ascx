<%@ Control Language="C#" AutoEventWireup="true" CodeFile="contactaccount.ascx.cs" Inherits="includes_controls_contactaccount" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<script type="text/javascript">
    $(document).ready(function () {
        doMyActionAcc();
       
    });
    function doMyActionAcc() {
        $('#<%=btnAdd.ClientID %>').click(function () {
            formValidateacc();
        });
    }
    function formValidateacc() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#FF5F5F");
                    $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#FF5F5F");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                    $('#s2id_' + Page_Validators[i].controltovalidate + ' a').css("border-color", "#B5B5B5");
                }
            }
        }
    }
</script>

<section class="row m-b-md">
    <div class="col-sm-12">
        <div class="contactsarea">
            <div class="messesgarea">
                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                </div>
                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                    <i class="icon-remove-sign"></i><strong>&nbsp;<asp:Label ID="lblError" runat="server" Text="Transaction Failed."></asp:Label></strong>
                </div>
            </div>
            <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                User Account
                            </div>
                            <div class="lightgraybgarea">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <span class="name">
                                                    <label class="control-label">
                                                        UserName 
                                                    </label>
                                                </span><span>
                                                    <asp:TextBox ID="txtuname" runat="server" MaxLength="20" Width="250px" CssClass="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="This value is required." CssClass="comperror"
                                                        ControlToValidate="txtuname" Display="Dynamic" ValidationGroup="InstAccount"></asp:RequiredFieldValidator>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div class="form-group " id="hidepass" runat="server">
                                                <span class="name">
                                                    <label class="control-label">
                                                        Password 
                                                    </label>
                                                </span><span>
                                                    <asp:TextBox ID="txtpassword" runat="server" MaxLength="200" CssClass="form-control" Width="250px"
                                                        TextMode="Password"></asp:TextBox>
                                                    
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server" ErrorMessage="Password has to be 5 characters !" ValidationGroup="InstAccount" Display="Dynamic"
                                                        ControlToValidate="txtpassword" ValidationExpression="^.{5,}$" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="This value is required." CssClass="comperror" ValidationGroup="InstAccount"
                                                        ControlToValidate="txtpassword" Display="Dynamic"></asp:RequiredFieldValidator>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div class="form-group " id="hidecpass" runat="server">
                                                <span class="name">
                                                    <label class="control-label">
                                                        Confirm Password 
                                                    </label>
                                                </span><span>
                                                    <asp:TextBox ID="txtcpassword" runat="server" MaxLength="200" CssClass="form-control" Width="250px"
                                                        TextMode="Password"></asp:TextBox>
                                                    
                                                    <asp:CompareValidator ID="compvalconfirmPassword" runat="server" ControlToCompare="txtpassword" ValidationGroup="InstAccount"
                                                        ControlToValidate="txtcpassword" ErrorMessage="Password MisMatch" SetFocusOnError="True"
                                                        Display="Dynamic"></asp:CompareValidator>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="This value is required." CssClass="comperror" ValidationGroup="InstAccount"
                                                        ControlToValidate="txtcpassword" Display="Dynamic"></asp:RequiredFieldValidator>
                                                </span>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" id="divAdd" runat="server" visible="false">
                                        <div class="col-md-6">
                                            <div class="form-group ">
                                                <span class="name">&nbsp;</span><span>
                                                    <asp:Button CssClass="btn btn-primary addwhiteicon" ID="btnAdd" runat="server" OnClick="btnAdd_Click" ValidationGroup="InstAccount"
                                                        Text="Add" CausesValidation="true" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</section>
