<%@ Control Language="C#" AutoEventWireup="true" CodeFile="contactdetail.ascx.cs"
    Inherits="includes_controls_contactdetail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<script>
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoadedcontact);
    function pageLoadedcontact() {
        $(".myvalcontdetail").select2({
            //placeholder: "select",
            allowclear: true
        });
        $('#<%=btnUpdateContDetail.ClientID %>').click(function () {
            formValidate();
        });
    }
</script>
<asp:UpdatePanel ID="updatepanelgrid" runat="server">
    <ContentTemplate>
        <section class="row m-b-md" runat="server" id="update11">
            <div class="col-sm-12">
                <div class="contactsarea">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                        </div>
                        <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                        </div>
                        <div class="alert alert-danger" id="lbleror1" runat="server" visible="false">
                            <i class="icon-info-sign"></i><strong>&nbsp;Record with this Email or Mobile aleardy Exist</strong>
                        </div>
                    </div>
                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                        <div class="row">
                            <div class="col-md-6">
                                <asp:Panel ID="Panel1" runat="server">
                                    <div class="form-group" runat="server" visible="false">
                                        <span class="name">
                                            <label class="control-label">
                                                Title
                                            </label>
                                        </span><span>
                                            <asp:TextBox ID="txtContTitle" runat="server" CssClass="form-control"></asp:TextBox>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">
                                                Primary Contact
                                            </label>
                                        </span><span class="spicaldivarea">
                                            <div>
                                                <div class="row ">

                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="txtContMr" runat="server" MaxLength="10" CssClass="form-control" placeholder="Salutation"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="txtContFirst" runat="server" MaxLength="30" CssClass="form-control" placeholder="First Name"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="txtContLast" runat="server" MaxLength="40" CssClass="form-control" placeholder="Last Name"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">
                                                Secondary Contact
                                            </label>
                                        </span><span class="spicaldivarea">
                                            <div>
                                                <div class="row">

                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="txtContMr2" runat="server" MaxLength="10" CssClass="form-control" placeholder="Salutation"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="txtContFirst2" runat="server" MaxLength="30" CssClass="form-control" placeholder="First Name"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="txtContLast2" runat="server" MaxLength="40" CssClass="form-control" placeholder="Last Name"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">
                                                Email
                                            </label>
                                        </span><span>
                                            <asp:TextBox ID="txtContEmail" runat="server" CssClass="form-control"></asp:TextBox>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">
                                                Site
                                            </label>
                                        </span><span>
                                            <asp:TextBox ID="txtSite" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">
                                                Street
                                            </label>
                                        </span><span>
                                            <asp:TextBox ID="txtStreet" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">
                                                City
                                            </label>
                                        </span><span class="spicaldivarea">
                                            <div>
                                                <div class="row">

                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="txtCity" runat="server" Enabled="false" CssClass="form-control marginbtm10"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="txtState" runat="server" Enabled="false" CssClass="form-control marginbtm10"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="txtCode" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </asp:Panel>
                                <div class="form-group checkboxarea">
                                    <span class="name">
                                        <label class="control-label">
                                            &nbsp;
                                        </label>
                                    </span><span class="spicaldivarea contactcheckbox cntcheckbox2">
                                        <div>
                                            <div class="row">
                                                <div class="checkbox">
                                                    <div class="col-md-4 checkbox-info ">
                                                        <span class="mrdiv">
                                                            <label for="<%=chkSendEmail.ClientID %>">
                                                                <asp:CheckBox ID="chkSendEmail" runat="server" />
                                                                <span class="text">Send Email&nbsp;</span>
                                                            </label>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 checkbox-info checkbox">
                                                    <span class="fistname">
                                                        <label for="<%=chkSendSMS.ClientID %>">
                                                            <asp:CheckBox ID="chkSendSMS" runat="server" />
                                                            <span class="text">Send SMS&nbsp;</span>
                                                        </label>
                                                    </span>
                                                </div>
                                                <div class="col-md-4 checkbox-info checkbox">

                                                    <asp:Panel ID="Panel2" runat="server">
                                                        <span class="lastname" style="width: 41%;">
                                                            <label for="<%=chkActiveTag.ClientID %>">
                                                                <asp:CheckBox ID="chkActiveTag" runat="server" Checked="true" />
                                                                <span class="text">Sales Active&nbsp;</span>
                                                            </label>
                                                        </span>

                                                    </asp:Panel>
                                                </div>


                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 checkbox-info checkbox">

                                                    <asp:Panel ID="Panel4" runat="server">
                                                        <span class="lastname" style="width: 41%;">
                                                            <label for="<%=chkcharge.ClientID %>">
                                                                <asp:CheckBox ID="chkcharge" runat="server" Checked="true" />
                                                                <span class="text">Referral Program&nbsp;</span>
                                                            </label>
                                                        </span>

                                                    </asp:Panel>
                                                </div>
                                            </div>
                                        </div>
                                    </span>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <asp:Panel ID="Panel3" runat="server">
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">
                                                Lead Status
                                            </label>
                                        </span><span>
                                            <asp:DropDownList ID="ddlContLeadStatusID" Width="100px" runat="server" aria-controls="DataTables_Table_0" CssClass="myval"
                                                AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlContLeadStatusID_SelectedIndexChanged">
                                                <asp:ListItem Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="form-group" id="divCancel" runat="server" visible="false">
                                        <span class="name">
                                            <label class="control-label">
                                                Cancel
                                            </label>
                                        </span><span>
                                            <asp:DropDownList ID="ddlCancel" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                AppendDataBoundItems="true">
                                                <asp:ListItem Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">
                                                Sales Rep
                                            </label>
                                        </span><span>
                                            <asp:DropDownList ID="ddlSalesRep" runat="server" aria-controls="DataTables_Table_0" class="myval"
                                                AppendDataBoundItems="true">
                                                <asp:ListItem Value="">Select</asp:ListItem>
                                            </asp:DropDownList>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">
                                                Mobile
                                            </label>
                                        </span><span>
                                            <asp:TextBox ID="txtContMobile" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtContMobile"
                                                Display="Dynamic" ErrorMessage="Please enter valid mobile number (eg. 04XXXXXXXX)"
                                                ValidationExpression="^04[\d]{8}$" ValidationGroup="contactdetail"></asp:RegularExpressionValidator>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">
                                                Work
                                            </label>
                                        </span><span>
                                            <asp:TextBox ID="txtContPhone" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
<%--                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtContPhone"
                                                Display="Dynamic" ErrorMessage="Please enter valid number (eg. 07/03/08XXXXXXXX)"
                                                ValidationExpression="^(07|03|08)[\d]{8}$" ValidationGroup="contactdetail"></asp:RegularExpressionValidator>--%>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">
                                                Fax
                                            </label>
                                        </span><span>
                                            <asp:TextBox ID="txtContFax" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtContFax"
                                                Display="Dynamic" ErrorMessage="Please enter valid number" ValidationExpression="^[\d]{10}$"
                                                ValidationGroup="contactdetail"></asp:RegularExpressionValidator>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">
                                                Home
                                            </label>
                                        </span><span>
                                            <asp:TextBox ID="txtContHomePhone" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                            <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtContHomePhone"
                                        Display="Dynamic" ErrorMessage="Please enter valid number (eg. 07/03/08XXXXXXXX)"
                                        ValidationGroup="contactdetail" ValidationExpression="^(07|03|08)[\d]{8}$"></asp:RegularExpressionValidator>--%>
                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <span class="name">
                                            <label class="control-label">
                                                Main
                                            </label>
                                        </span><span>
                                            <asp:TextBox ID="txtCustContact" runat="server" MaxLength="10" CssClass="form-control modaltextbox"></asp:TextBox>

                                        </span>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                        <div class="row" id="divAddUpdate" runat="server">
                            <div class="col-md-12 textcenterbutton center-text">
                                <div>
                                    <span class="name">&nbsp;</span><span>
                                        <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdateContDetail" runat="server" OnClick="btnUpdateContDetail_Click"
                                            ValidationGroup="contactdetail" Text="Save" CausesValidation="true" /></span>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </section>
    </ContentTemplate>
</asp:UpdatePanel>

<div class="loaderPopUP">
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
        }

        function pageLoadedpro() {

            $('.loading-container').css('display', 'none');

            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

        }
    </script>
    <%--<asp:UpdateProgress ID="updateprogress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>
            
        <div class="loading-container">
        <div class="loader"></div>
        </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <cc1:ModalPopupExtender ID="newmodalpopup" runat="server" TargetControlID="updateprogress2"
        PopupControlID="updateprogress2" BackgroundCssClass="modalPopup" />--%>
</div>
