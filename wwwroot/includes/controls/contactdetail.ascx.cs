﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;

public partial class includes_controls_contactdetail : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e) 
    {
        HidePanel();
        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>doMyAction();</script>");
    }
    public void BindContactGrid()
    {
        GridView GridView1 = (GridView)this.Parent.Parent.FindControl("GridView1");
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblContacts.tblContacts_SelectByUserIdCust(userid, Request.QueryString["compid"]);
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }
    public void BindContactDetail()
    {
        PanAddUpdate.Enabled = true;
        CheckRecordCount();
        if (!string.IsNullOrEmpty(Request.QueryString["contid"]))
        {
            BindDropDown();

            if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Sales Manager"))
            {
                ddlContLeadStatusID.Enabled = true;
            }
            else
            {
                ddlContLeadStatusID.Enabled = false;
            }
            SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(Request.QueryString["contid"]);
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(st2.CustomerID);

            txtSite.Text = stCust.Customer;
            txtStreet.Text = stCust.StreetAddress;
            txtCity.Text = stCust.StreetCity;
            txtState.Text = stCust.StreetState;
            txtCode.Text = stCust.StreetPostCode;
            txtContEmail.Text = st2.ContEmail;

            ddlContLeadStatusID.SelectedValue = st2.ContLeadStatusID;
            ddlCancel.SelectedValue = st2.ContLeadCancelReasonID;

            chkSendEmail.Checked = Convert.ToBoolean(st2.SendEmail);
            chkSendSMS.Checked = Convert.ToBoolean(st2.SendSMS);
            chkActiveTag.Checked = Convert.ToBoolean(st2.ActiveTag);
            if(!string.IsNullOrEmpty(st2.referralProgram))
            {
                chkcharge.Checked = Convert.ToBoolean(st2.referralProgram);
            }
            else
            {
                chkcharge.Checked = false;
            }
           

            txtContTitle.Text = st2.ContTitle;
            txtContMr.Text = st2.ContMr;
            txtContFirst.Text = st2.ContFirst;
            txtContLast.Text = st2.ContLast;
            txtContMr2.Text = st2.ContMr2;
            txtContFirst2.Text = st2.ContFirst2;
            txtContLast2.Text = st2.ContLast2;

            txtContMobile.Text = st2.ContMobile;
            txtContPhone.Text = st2.ContPhone;
            txtContFax.Text = st2.ContFax;
            txtContHomePhone.Text = st2.ContHomePhone;
            txtCustContact.Text = stCust.CustPhone;

            try
            {
                ddlSalesRep.SelectedValue = st2.EmployeeID;
            }
            catch { }
            if (ddlContLeadStatusID.SelectedValue == "4")
            {
                divCancel.Visible = true;
            }
            else
            {
                divCancel.Visible = false;
            }
        }
    }
    public void HidePanel()
    {
        PanSuccess.Visible = false;
        PanError.Visible = false;
    }
    public void CheckRecordCount()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["contid"]))
        {
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            DataTable dt = ClstblContacts.AdmintblContactsGetCount(userid, Request.QueryString["contid"]);
            if (dt.Rows.Count == 0)
            {
                Panel1.Enabled = false;
                Panel2.Enabled = false;
                Panel3.Enabled = false;
                //PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Administrator"))
            {
                Panel1.Enabled = true;
                Panel2.Enabled = true;
                Panel3.Enabled = true;
                PanAddUpdate.Enabled = true;
            }
            if ((Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("DSales Manager")))
            {
                ddlSalesRep.Enabled = true;
            }
            else
            {
                ddlSalesRep.Enabled = false;
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
            {
                SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                string CEmpType = stEmpC.EmpType;
                string CSalesTeamID = stEmpC.SalesTeamID;

                if (Request.QueryString["contid"] != string.Empty)
                {
                    SttblContacts stCont = ClstblContacts.tblContacts_SelectByContactID(Request.QueryString["contid"]);
                    SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(stCont.EmployeeID);
                    string EmpType = stEmp.EmpType;
                    string SalesTeamID = stEmp.SalesTeamID;

                    if (CEmpType == EmpType && CSalesTeamID == SalesTeamID)
                    {
                        Panel1.Enabled = true;
                        Panel2.Enabled = true;
                        Panel3.Enabled = true;
                        //PanAddUpdate.Enabled = true;
                    }
                    else
                    {
                        Panel1.Enabled = false;
                        Panel2.Enabled = false;
                        Panel3.Enabled = false;
                        //PanAddUpdate.Enabled = false;
                    }
                }
            }
            if (Roles.IsUserInRole("Installer"))
            {
                PanAddUpdate.Enabled = false;
            }
        }
    }
    public void BindDropDown()
    {
        ListItem item = new ListItem();
        item.Text = "Select";
        item.Value = "";
        ddlContLeadStatusID.Items.Clear();
        ddlContLeadStatusID.Items.Add(item);

        ddlContLeadStatusID.DataSource = ClstblContacts.tblContLeadStatus_SelectASC();
        ddlContLeadStatusID.DataValueField = "ContLeadStatusID";
        ddlContLeadStatusID.DataTextField = "ContLeadStatus";
        ddlContLeadStatusID.DataMember = "ContLeadStatus";
        ddlContLeadStatusID.DataBind();

        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlCancel.Items.Clear();
        ddlCancel.Items.Add(item1);

        ddlCancel.DataSource = ClstblContLeadCancelReason.tblContLeadCancelReason_Select();
        ddlCancel.DataValueField = "ContLeadCancelReasonID";
        ddlCancel.DataTextField = "ContLeadCancelReason";
        ddlCancel.DataMember = "ContLeadCancelReason";
        ddlCancel.DataBind();

        ListItem item5 = new ListItem();
        item5.Text = "Select";
        item5.Value = "";
        ddlSalesRep.Items.Clear();
        ddlSalesRep.Items.Add(item5);

        ddlSalesRep.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlSalesRep.DataValueField = "EmployeeID";
        ddlSalesRep.DataTextField = "fullname";
        ddlSalesRep.DataMember = "fullname";
        ddlSalesRep.DataBind();
    }
    protected void btnUpdateContDetail_Click(object sender, EventArgs e)
    {
        string ContactID = Request.QueryString["contid"];
        SttblContacts st2 = ClstblContacts.tblContacts_SelectByContactID(ContactID);
        //string EmployeeID = st2.EmployeeID;
        string EmployeeID = ddlSalesRep.SelectedValue;
        string ContLeadStatusID = ddlContLeadStatusID.SelectedValue;
        string ContLeadCancelReasonID = ddlCancel.SelectedValue;
        string ContEmail = txtContEmail.Text;
        string ActiveTag = Convert.ToString(chkActiveTag.Checked);
        string SendEmail = Convert.ToString(chkSendEmail.Checked);
        string SendSMS = Convert.ToString(chkSendSMS.Checked);

        string ContTitle = txtContTitle.Text;
        string ContMr = txtContMr.Text;
        string ContFirst = txtContFirst.Text.Trim();
        string ContLast = txtContLast.Text.Trim();
        string ContMr2 = txtContMr2.Text;
        string ContFirst2 = txtContFirst2.Text.Trim();
        string ContLast2 = txtContLast2.Text.Trim();

        string ContMobile = txtContMobile.Text;
        string ContPhone = txtContPhone.Text;
        string ContHomePhone = txtContHomePhone.Text;
        string ContFax = txtContFax.Text;
        string ContName = ContFirst + ' ' + ContLast;
        string collefctioncharge = Convert.ToString(chkcharge.Checked);

        DataTable dt = ClstblContacts.tblContacts_SelectTop1ByCustId(st2.CustomerID);

        int dtMobileEmailExist = 0;
        int flag1 = 0;
        int flag2 = 0;
        string email = dt.Rows[0]["ContEmail"].ToString().Trim();
        string mobile = dt.Rows[0]["ContMobile"].ToString();
        if (ContMobile != mobile)
        {
            if (ContMobile != "0400000000")
                flag1 = 1;
        }
        if (ContEmail != email)
        {
            if (ContEmail != "no-reply@arisesolar.com.au")
                flag2 = 1;
        }
        if (flag1 == 1 || flag2 == 1)
        {
            if (flag1 == 0)
            {
              ContMobile = "0400000000";
            }
            if (flag2 == 0)
            {
                ContEmail = "no-reply@arisesolar.com.au";
            }

            dtMobileEmailExist = ClstblCustomers.tblContacts_Exits_Select_MobileEmailByID2(st2.CustomerID, ContMobile, ContEmail);
        }
        if (dtMobileEmailExist == 0)
        {
            //Response.Write(existaddress);
            // Response.End();
            lbleror1.Visible = false;

            int exist = ClstblContacts.tblContacts_ExistsByContactID(st2.CustomerID, ContName, ContactID);
            if (exist == 0)
            {
                //DataTable dt = ClstblContacts.tblContacts_SelectTop1ByCustId(st2.CustomerID);
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["ContactID"].ToString() == ContactID)
                    {
                        ClstblCustomers.tblCustomers_UpdateCustomer(st2.CustomerID, txtContFirst.Text + ' ' + txtContLast.Text);
                    }
                }

                bool sucDetail = ClstblContacts.tblContacts_UpdateDetail(ContactID, EmployeeID, ContLeadStatusID, ContLeadCancelReasonID, ActiveTag, ContTitle, ContMr, ContFirst, ContLast, txtContMobile.Text, ContPhone, ContHomePhone, ContFax, SendEmail, "False", SendSMS, txtContEmail.Text, ContMr2, ContFirst2, ContLast2);
                bool sucPhone = ClstblContacts.tblContacts_UpdateCustPhone(txtCustContact.Text, st2.CustomerID);
                bool succolleccharge = ClstblContacts.tblContacts_UpdatereferralProgram(collefctioncharge, ContactID);

                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                if (ddlSalesRep.SelectedValue.ToString() != st2.EmployeeID)
                {
                    ClstblCustomers.tblCustomers_Update_Assignemployee(st2.CustomerID, ddlSalesRep.SelectedValue.ToString(), stEmp.EmployeeID, DateTime.Now.AddHours(14).ToString(), Convert.ToString(true));
                    ClstblProjects.tblProjects_UpdateEmployee(st2.CustomerID, ddlSalesRep.SelectedValue.ToString());
                    ClstblContacts.tblContacts_UpdateEmployee(st2.CustomerID, ddlSalesRep.SelectedValue.ToString());
                    ClstblCustomers.tbl_contactdetaillog_InsertContacts(ContactID, ContLeadStatusID, EmployeeID, stEmp.EmployeeID);
                }
                //--- do not chage this code Start------
                if (sucDetail)
                {
                    SetAdd1();
                    //PanSuccess.Visible = true;
                    PanAlreadExists.Visible = false;
                }
                else
                {
                    SetError1();
                    //PanError.Visible = true;
                }
                BindContactDetail();
                BindContactGrid();
            }
            else
            {
                PanAddUpdate.Visible = true;
                SetExist();
                //PanAlreadExists.Visible = true;
            }
        }
        else
        {
            lbleror1.Visible = true;
        }
    }
    protected void ddlContLeadStatusID_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlContLeadStatusID.SelectedValue == "4")
        {
            divCancel.Visible = true;
        }
        else
        {
            divCancel.Visible = false;
        }
    }

    public void Reset()
    {
        txtSite.Text = string.Empty;
        txtStreet.Text = string.Empty;
        txtCity.Text = string.Empty;
        txtState.Text = string.Empty;
        txtCode.Text = string.Empty;

        ddlContLeadStatusID.ClearSelection();
        ddlCancel.ClearSelection();

        chkSendEmail.Checked = false;
        chkSendSMS.Checked = false;
        chkActiveTag.Checked = false;

        txtContTitle.Text = string.Empty;
        txtContMr.Text = string.Empty;
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        txtContMr2.Text = string.Empty;
        txtContFirst2.Text = string.Empty;
        txtContLast2.Text = string.Empty;

        txtContMobile.Text = string.Empty;
        txtContPhone.Text = string.Empty;
        txtContFax.Text = string.Empty;
        txtContHomePhone.Text = string.Empty;
        txtCustContact.Text = string.Empty;
        chkcharge.Checked = false;
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
}