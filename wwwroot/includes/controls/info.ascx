<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="info.ascx.cs" Inherits="includes_controls_info" %>

<script>
    function ComfirmDelete(event, ctl) {
        event.preventDefault();
        var defaultAction = $(ctl).prop("href");

        swal({
            title: "Are you sure you want to delete this Record?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: true,
            closeOnCancel: true
        },

          function (isConfirm) {
              if (isConfirm) {
                  // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                  eval(defaultAction);

                  //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                  return true;
              } else {
                  // swal("Cancelled", "Your imaginary file is safe :)", "error");
                  return false;
              }
          });
    }
</script>

<script>
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoaded);
    function pageLoaded() {
        $(".myvalinfo").select2({
            //placeholder: "select",
            allowclear: true
        });

        $(".myval").select2({
            //placeholder: "select",
            allowclear: true
        });

        $('.datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY'
        });

        $('.redreq').click(function () {
            formValidate();
        });


        $('.redreq1').click(function () {
            form_Validate();
        });

        function form_Validate() {
            //alert(Page_Validators);
            var myid = "";
            if (typeof (Page_Validators) != "undefined") {
                for (var i = 0; i < Page_Validators.length; i++) {


                    if (myid != Page_Validators[i].controltovalidate) {

                        if (!Page_Validators[i].isvalid) {
                            myid = Page_Validators[i].controltovalidate;
                            // ctl00_ContentPlaceHolder1_TabContainer1_TabInfo_info1_RequiredFieldValidator4
                            //ctl00_ContentPlaceHolder1_TabContainer1_TabInfo_info1_cmpNextDate
                            $('#' + Page_Validators[i].controltovalidate).addClass("errormassage");
                            $('#s2id_' + Page_Validators[i].controltovalidate).addClass("errormassage");
                        }
                        else {
                            $('#' + Page_Validators[i].controltovalidate).removeClass("errormassage");
                            $('#s2id_' + Page_Validators[i].controltovalidate).removeClass("errormassage");
                        }
                    }
                }
            }
        }
    }
</script>


<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <section class="row m-b-md">
            <div class="col-sm-12">
                <div class="contactsarea">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                            <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                        </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                        </div>
                        <div class="alert alert-info" id="PanNoRecord" runat="server" visible="false">
                            <i class="icon-info-sign"></i><strong>&nbsp;There are no items to show in This view</strong>
                        </div>
                        <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                            <i class="icon-remove-sign"></i><strong>&nbsp;Record with This name already exists.</strong>
                        </div>
                    </div>
                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate" Visible="false">
                        <div class="infopage">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- start: FORM VALIDATION 1 PANEL -->


                                    <div class="widget flat radius-bordered borderone">
                                        <div class="widget-header bordered-bottom bordered-blue">
                                            <span class="widget-caption">
                                                <asp:Label ID="lblAddUpdate" runat="server" Text=""></asp:Label>Follow Up</span>
                                        </div>
                                        <div class="widget-body">
                                            <div>
                                                <div class="row">
                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <asp:Label ID="lblLastName" runat="server">
                                                Contact</asp:Label>
                                                            <div class="marginbtm15">
                                                                <asp:DropDownList ID="ddlContact" runat="server" AppendDataBoundItems="true"
                                                                    aria-controls="DataTables_Table_0" CssClass="myvalinfo">
                                                                    <asp:ListItem Value="">Select</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <br />
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="" CssClass=""
                                                                    ControlToValidate="ddlContact" Display="Dynamic" ValidationGroup="info" InitialValue=""></asp:RequiredFieldValidator>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-md-6">
                                                                <asp:Label ID="Label23" runat="server">
                                                Follow Up Date </asp:Label>
                                                                <div class=" marginbtm15 datevalidation">
                                                                    <div class="input-group date">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtFollowupDate" runat="server" class="form-control" placeholder="Followup Date" Enabled="false">
                                                                        </asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFollowupDate" CssClass="comperror"
                                                                            ValidationGroup="info" ErrorMessage="This value is required." Display="Dynamic"> </asp:RequiredFieldValidator>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <asp:Label ID="Label1" runat="server">
                                                Next Follow Up Date </asp:Label>
                                                                <div class="marginbtm15 datevalidation">

                                                                    <div class="input-group date datetimepicker1">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtNextFollowupDate" runat="server" class="form-control" placeholder="Next Followup Date">
                                                                        </asp:TextBox>
                                                                      
                                                                    </div>
                                                                      <asp:CompareValidator ID="cmpNextDate" runat="server" ControlToValidate="txtNextFollowupDate" ControlToCompare="txtFollowupDate"
                                                                            ErrorMessage="Next followup date cannot be less than Followup date" Operator="GreaterThanEqual"
                                                                            ValidationGroup="info" Type="Date" Display="Dynamic" CssClass=""></asp:CompareValidator>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtNextFollowupDate" CssClass="comperror"
                                                                            ValidationGroup="info" ErrorMessage="" Display="Dynamic"> </asp:RequiredFieldValidator>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label21" runat="server" class="col-sm-2 control-label">
                                              Description</asp:Label>
                                                        <div class="col-sm-6">
                                                            <asp:TextBox ID="txtDescription" runat="server" MaxLength="200" Style="min-height: 102px" TextMode="MultiLine" class="form-control modaltextbox"></asp:TextBox>
                                                            <br />
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="" CssClass=""
                                                                ValidationGroup="info" ControlToValidate="txtDescription"></asp:RequiredFieldValidator>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 textcenterbutton">
                                                        <div class="center-text">
                                                            <span class="name">&nbsp;</span><span>
                                                                <asp:Button class="btn btn-primary addwhiteicon redreq redreq1 btnaddicon" ID="btnAddFollowUp" runat="server" OnClick="btnAddFollowUp_Click"
                                                                    Text="Add" ValidationGroup="info" />
                                                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click"
                                                                    Text="Save" Visible="false" ValidationGroup="info" />
                                                                <asp:Button class="btn btn-purple resetbutton btnreseticon" ID="btnReset" runat="server" OnClick="btnReset_Click"
                                                                    CausesValidation="false" Text="Reset" />

                                                                <asp:Button CssClass="btn btn-danger btncancelicon" ID="btnCancel" runat="server" OnClick="btnCancel_Click"
                                                                    CausesValidation="false" Text="Cancel" />
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- end: FORM VALIDATION 1 PANEL -->
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="contacttoparea paddleftright10 Responsive-search row marginbtm15 paddingtopzero" id="PanSearch" runat="server">
                        <div class="leftarea1 col-md-2">
                            <div class="showenteries searchfinal showdata martopzero padtopzero">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>

                                        <td>


                                            <asp:DropDownList ID="ddlSelectRecords" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectRecords_SelectedIndexChanged"
                                                aria-controls="DataTables_Table_0" class="myval">
                                                <asp:ListItem Value="25">Show entries</asp:ListItem>
                                            </asp:DropDownList>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div id="divrightbtn" runat="server" style="text-align: right;" class="pull-right">
                                <asp:LinkButton ID="lnkAdd" runat="server" CausesValidation="false"
                                    OnClick="lnkAdd_Click" CssClass="btn btn-default purple"><i class="fa fa-plus"></i> Add</asp:LinkButton>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="contactbottomarea finalgrid">
                        <div class="tableblack wrap">
                            <div class="table-responsive" id="PanGrid" runat="server">
                                <asp:GridView ID="GridView1" DataKeyNames="CustInfoID" runat="server" AllowPaging="true"
                                    PagerStyle-HorizontalAlign="Right" AutoGenerateColumns="false" PageSize="10" OnRowDataBound="GridView1_RowDataBound"
                                    OnSelectedIndexChanging="GridView1_SelectedIndexChanging" OnDataBound="GridView1_DataBound" OnPageIndexChanging="GridView1_PageIndexChanging"
                                    AllowSorting="true" OnRowDeleting="GridView1_RowDeleting" CssClass="table table-bordered table-hover"
                                    OnSorting="GridView1_Sorting">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Description" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="brdrgrayleft" ItemStyle-Width="400px">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hndEmployeeID" runat="server" Value='<%#Eval("CustInfoEnteredBy") %>' />
                                                <%#Eval("Description")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Follow up date" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="150px"
                                            HeaderStyle-CssClass="center-text">
                                            <ItemTemplate>
                                                <%#Eval("FollowupDate", "{0:dd MMM yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Next follow up date" ItemStyle-VerticalAlign="Top"
                                            HeaderStyle-CssClass="center-text" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                            ItemStyle-Width="190px">
                                            <ItemTemplate>
                                                <%#Eval("NextFollowupDate", "{0:dd MMM yyyy}")%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" HeaderText="">
                                            <ItemTemplate>
                                                <%--   <%#Eval("firstrecord").ToString()%>--%>
                                                <%--Visible='<%# SiteConfiguration.FromSqlDate(Convert.ToDateTime(Eval("date"))) == SiteConfiguration.FromSqlDate(Convert.ToDateTime(Eval("NextFollowupDate")))?false:false %>'--%>
                                                <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CausesValidation="false" Visible='<%#Eval("firstrecord").ToString()=="1"?true:false%>'
                                                    data-toggle="tooltip" data-placement="top" title="Detail View" data-original-title="Detail View" CssClass="btn btn-info btn-xs">
                                           <%-- <img src="../../../images/icon_edit.png"/>--%>     <i class="fa fa-edit"></i> Edit</asp:LinkButton>
                                                <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-xs" CausesValidation="false" Visible='<%#Eval("firstrecord").ToString() =="1" || Eval("lastrecord").ToString()=="0" ?true:false%>'
                                                    CommandName="Delete" CommandArgument='<%#Eval("CustInfoID")%>'>
                                            <i class="fa fa-trash"></i> Delete
                                                </asp:LinkButton>

                                            </ItemTemplate>
                                            <ItemStyle Width="1%" HorizontalAlign="Center" CssClass="verticaaline" />
                                        </asp:TemplateField>

                                    </Columns>
                                    <PagerStyle CssClass="paginationGrid" />
                                </asp:GridView>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Danger Modal Templates-->
        <asp:Button ID="btndelete" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
            PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
        </cc1:ModalPopupExtender>
        <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

            <div class="modal-dialog " style="margin-top: -300px">
                <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                    <div class="modal-header text-center">
                        <i class="glyphicon glyphicon-fire"></i>
                    </div>


                    <div class="modal-title">Delete</div>
                    <label id="ghh" runat="server"></label>
                    <div class="modal-body ">Are You Sure Delete This Entry?</div>
                    <div class="modal-footer " style="text-align: center">
                        <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>

        <asp:HiddenField ID="hdndelete" runat="server" />
        <!--End Danger Modal Templates-->
    </ContentTemplate>
</asp:UpdatePanel>



<div class="loaderPopUP">
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');

        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
        }

        function pageLoadedpro() {

            $('.loading-container').css('display', 'none');

            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();

        }
    </script>
    <%--<asp:UpdateProgress ID="updateprogress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>
        <div class="loading-container">
        <div class="loader"></div>
        </div>	
        </ProgressTemplate>
    </asp:UpdateProgress>
    <cc1:ModalPopupExtender ID="newmodalpopup" runat="server" TargetControlID="updateprogress2"
        PopupControlID="updateprogress2" BackgroundCssClass="modalPopup" />--%>
</div>
