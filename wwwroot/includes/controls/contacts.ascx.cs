﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;

public partial class includes_controls_contacts : System.Web.UI.UserControl
{
    protected string SiteURL;
    protected void Page_Load(object sender, EventArgs e)
    {

        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        //BindGrid(0);
        //int i = 0;
        //i++;
        //lt1.Text += (i).ToString() + " ";       
        //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>doMyAction();</script>");
    }

    public void HideTabContainer()
    {
        divcontacttab.Visible = false;
        //Profile.eurosolar.contactid = "";
    }

    public void HideContactAction()
    {
      //  GridView1.Columns[7].Visible = false;
        divrightbtn.Visible = false;

        if (Roles.IsUserInRole("Administrator"))
        {
            //divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Accountant"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("BookInstallation"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Customer"))
        {
            //divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Installer"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("PostInstaller"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("PreInstaller"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("STC"))
        {
            divrightbtn.Visible = false;
        }
    }
    public void ShowContactAction()
    {
        //GridView1.Columns[7].Visible = true;
        //divrightbtn.Visible = true;
    }

    public void BindContact()
    {
        HidePanels();
        TabAccount.Visible = false;
        //divrightbtn.Visible = true;
        BindGrid(0);
        //ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
        //ddlSelectRecords.DataBind();

        string CustomerID = Request.QueryString["compid"];
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);
        if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Installation Manager"))
        {

            if (stCust.CustTypeID == "3" || stCust.CustTypeID == "4")
            {
                TabAccount.Visible = true;
            }
            else
            {
                TabAccount.Visible = false;
            }
        }
    }
    protected DataTable GetGridData()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblContacts.tblContacts_SelectByUserIdCust(userid, Request.QueryString["compid"]);

        
        if (dt.Rows.Count == 0)
        {
            PanSearch.Visible = true;
        }
        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        PanSearch.Visible = true;

        

        DataTable dt = GetGridData();
        

        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            PanNoRecord.Visible = false;
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(pancontacts, this.GetType(), "MyAction", "doMyAction();", true);

        //string username = txtuname.Text.Trim();
        //string password = txtcpassword.Text.Trim();
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        string CustomerID = Request.QueryString["compid"];
        SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);
        string EmployeeID = st.EmployeeID;

        string ContactEntered = DateTime.Now.AddHours(14).ToString();
        string userid2 = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st2 = ClstblEmployees.tblEmployees_SelectByUserId(userid2);
        string ContactEnteredBy = st.CustEnteredBy;

        string ContTitle = txtContTitle.Text;
        string ContMr = txtContMr.Text;
        string ContFirst = txtContFirst.Text.Trim();
        string ContLast = txtContLast.Text.Trim();
        string ContMobile = txtContMobile.Text;
        string ContEmail = txtContEmail.Text;
        string ContNotes = txtContNotes.Text;
        string ContMr2 = txtContMr2.Text;
        string ContFirst2 = txtContFirst2.Text.Trim();
        string ContLast2 = txtContLast2.Text.Trim();

        string LeadPromo = "True";
        string SendEmail = "True";
        string SendMail = "True";
        string SendSMS = "True";

        string ContName = ContFirst + ' ' + ContLast;
        int exist = ClstblContacts.tblContacts_ExistsByCustomerID(CustomerID, ContName);
        if (exist == 0)
        {
            //try
            //{

            int success = ClstblContacts.tblContacts_Insert(CustomerID, EmployeeID, "", "", "False", "False", "False", "False", "False", LeadPromo, "False", "False", "False", ContactEntered, ContactEnteredBy, ContTitle, ContMr, ContFirst, ContLast, ContMobile, "", "", "", ContEmail, "", "", "False", "", SendEmail, SendMail, SendSMS, ContNotes, "", "", "", "", "", "False", "False", "False", "False", "", "False", "", "", "False", "False", "", "", "", ContMr2, ContFirst2, ContLast2);
          //  ClstblContacts.tblContacts_Update_UserId(Convert.ToString(success), userid);
            //int sucContNotes = ClstblCustomers.tblCustomers_InsertContNotes(Convert.ToString(success), ContNotes);

            bool sucContLead = ClstblContacts.tblContacts_UpdateContLeadStatus("5", Request.QueryString["compid"]);
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);

            if (Convert.ToString(success) != "")
            {
                SetAdd();
            }
            else
            {
                SetError();
            }
            //}
            //catch{}
        }
        else
        {
            InitAdd();
            SetExist();
            //PanAlreadExists.Visible = true;

        }
        BindGrid(0);
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        string CustomerID = Request.QueryString["compid"];
        SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);
        string EmployeeID = st.EmployeeID;
        string ContactEntered = DateTime.Now.AddHours(14).ToString();

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees st2 = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string ContactEnteredBy = st.EmployeeID;

        string ContTitle = txtContTitle.Text;
        string ContMr = txtContMr.Text;
        string ContFirst = txtContFirst.Text;
        string ContLast = txtContLast.Text;
        string ContMobile = txtContMobile.Text;
        string ContNotes = txtContNotes.Text;
        string ContEmail = txtContEmail.Text;
        string ContMr2 = txtContMr2.Text;
        string ContFirst2 = txtContFirst2.Text.Trim();
        string ContLast2 = txtContLast2.Text.Trim();

        SttblContacts stCon = ClstblContacts.tblContacts_SelectByContactID(id1);
        bool success = ClstblContacts.tblContacts_Update(id1, CustomerID, EmployeeID, "", "", "", "False", "False", "False", "False", "False", "False", "False", "False", "False", "False", ContactEntered, ContactEnteredBy, ContTitle, ContMr, ContFirst, ContLast, ContMobile, "", "", "", ContEmail, "", "", "False", "", "", "", "", ContNotes, "", "", "", "", "", "False", "False", "False", "False", "", "False", "", "", "False", "False", "", "", "", ContMr2, ContFirst2, ContLast2);

        //--- do not chage this code Start------
        if (success)
        {
            SetUpdate();
        }
        else
        {
            SetError();
        }
        BindGrid(0);
        //--- do not chage this code end------
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);

    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    public void ClearContactSelection()
    {
        divcontacttab.Visible = false;
        //Profile.eurosolar.contactid = "";
    }
    protected void lnkclose_Click(object sender, EventArgs e)
    {
        ClearContactSelection();
    }

    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        //Profile.eurosolar.contactid = "";
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        SttblContacts st = ClstblContacts.tblContacts_SelectByContactID(id);
        ltcontactname.Text = st.ContFirst + " " + st.ContLast;
        //Profile.eurosolar.contactid = id;
        ViewContactDetail();
    }
    public void ViewContactDetail()
    {
        divcontacttab.Visible = true;
        TabContainer2.ActiveTabIndex = 0;
        if (!string.IsNullOrEmpty(Request.QueryString["m"]))
        {
            if (Request.QueryString["m"] == "c")
            {
                if (!string.IsNullOrEmpty(Request.QueryString["contid"]))
                {
                    SttblContacts st = ClstblContacts.tblContacts_SelectByContactID(Request.QueryString["contid"]);
                    ltcontactname.Text = st.ContFirst + " " + st.ContLast;
                }
                TabContainer2.ActiveTabIndex = 0;
                contactdetail2.BindContactDetail();
            }
            //if (Request.QueryString["m"] == "n")
            //{
            //    TabContainer2.ActiveTabIndex = 1;
            //    contactnote2.BindContactNote();
            //}
            //if (Request.QueryString["m"] == "p")
            //{
            //    TabContainer2.ActiveTabIndex = 2;
            //}
        }
        //contactnote1.BindContactNote();
        //contactinfo1.BindContactInfo();
        //promo1.BindPromo();
    }
    //protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    ScriptManager.RegisterStartupScript(pancontacts, this.GetType(), "MyAction", "doMyAction();", true);

    //    if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
    //    {
    //        GridView1.AllowPaging = false;
    //        BindGrid(0);
    //    }
    //    else
    //    {
    //        GridView1.AllowPaging = true;
    //        GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
    //        BindGrid(0);
    //    }
    //}

    protected void lnkAdd_Click(object sender, EventArgs e)
    {


        txtContMr.Focus();
        PanGrid.Visible = false;
        PanSearch.Visible = false;

        ClearContactSelection();
        InitAdd();
        divrightbtn.Visible = false;
        //ScriptManager.RegisterStartupScript(pancontacts, this.GetType(), "MyAction", "doMyAction();", true);
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
        lnkAdd.Visible = false;
        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "  <script>loader();</script>");
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(pancontacts, this.GetType(), "MyAction", "doMyAction();", true);

        PanGrid.Visible = true;
        PanSearch.Visible = true;

        Reset();
        PanAddUpdate.Visible = false;
        divrightbtn.Visible = true;
    }
    public void SetAdd()
    {
        divrightbtn.Visible = true;
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }
    public void SetUpdate()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();
        btnAdd.Visible = true;
        btnReset.Visible = true;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
        PanAddUpdate.Visible = true;

        btnAdd.Visible = true;
        btnReset.Visible = true;
        // btnCancel.Visible = true;
        lblAddUpdate.Text = "Add ";
    }
    public void InitUpdate()
    {
        HidePanels();
        PanAddUpdate.Visible = true;

        btnAdd.Visible = false;

        btnReset.Visible = false;
        lblAddUpdate.Text = "Update ";
    }
    private void HidePanels()
    {
        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        PanNoRecord.Visible = false;

        PanAddUpdate.Visible = false;
    }
    public void Reset()
    {
        PanAddUpdate.Visible = true;
        txtContTitle.Text = string.Empty;
        txtContMr.Text = string.Empty;
        txtContEmail.Text = string.Empty;
        txtContFirst.Text = string.Empty;
        txtContLast.Text = string.Empty;
        txtContMobile.Text = string.Empty;
        txtContNotes.Text = string.Empty;
        txtContMr2.Text = string.Empty;
        txtContFirst2.Text = string.Empty;
        txtContLast2.Text = string.Empty;

        //divrightbtn.Visible = true;
    }
    protected void GridView1_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink detail1 = (HyperLink)e.Row.FindControl("gvbtnDetail");
            HyperLink detail2 = (HyperLink)e.Row.FindControl("gvbtnDetail2");
            if (Roles.IsUserInRole("leadgen") || Roles.IsUserInRole("Lead Manager"))
            {
                detail1.Visible = false;
                detail2.Visible = true;
            }
        }
    }

    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        // ScriptManager.RegisterStartupScript(pancontacts, this.GetType(), "MyAction", "doMyAction();", true);
        //Response.Write(TabContainer2.ActiveTabIndex);
        //Response.End();
        if (TabContainer2.ActiveTabIndex == 0)
        {
            //contactdetail1.BindContactDetail();
        }
        else if (TabContainer2.ActiveTabIndex == 1)
        {
            contactinfo2.BindContactInfo();
        }
        else if (TabContainer2.ActiveTabIndex == 3)
        {
             contactaccount2.BindCreateAccount();
        }
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(pancontacts, this.GetType(), "MyAction", "doMyAction();", true);

        string id = hdndelete.Value.ToString();
        bool sucess1 = ClstblContacts.tblContacts_Delete(id);
        //--- do not chage this code start------
        if (sucess1)
        {
            divrightbtn.Visible = true;
            SetAdd1();
            //PanSuccess.Visible = true;
        }
        else
        {
            SetError();
            //PanError.Visible = true;
        }
        Reset();
        PanAddUpdate.Visible = false;
        //--- do not chage this code end------
        //--- do not chage this code start------

        BindGrid(0);

        //--- do not chage this code end------
    }
}