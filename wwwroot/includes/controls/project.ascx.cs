﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using AjaxControlToolkit;
public partial class includes_controls_project : System.Web.UI.UserControl
{
    protected string SiteURL;
    protected string address;
    protected void Page_Load(object sender, EventArgs e)
    {
        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

            try
            {
                if (!string.IsNullOrEmpty(st2.InstallState))
                {
                    if (st2.InstallState != "VIC")
                    {
                        TabVicDoc.Visible = false;
                    }
                }
            }
            catch { }
        }
    }
    public void BindProjects()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["compid"]))
        {
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>alert('');</script>");
            SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"]);
            //if (st.isformbayadd != String.Empty)
            //{
            if (Convert.ToBoolean(st.isformbayadd) == true)
            {
                txtInstallAddressline.Enabled = true;
                CustomInstallAddressline.Enabled = true;
                //rfvinstall.Enabled = true;
                txtInstallCity.Enabled = true;
                txtInstallAddress.Enabled = true;
                AutoCompleteSearch.Enabled = true;
            }
            else if (Convert.ToBoolean(st.isformbayadd) == false)
            {
                txtInstallAddressline.Enabled = false;
                CustomInstallAddressline.Enabled = false;
                //rfvinstall.Enabled = false;
                txtInstallCity.Enabled = false;
                txtInstallAddress.Enabled = false;
                AutoCompleteSearch.Enabled = false;
            }
            //}
            HidePanels();
            BindGrid(0);
            BindDropDown();
            //divrightbtn.Visible = true;
            ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
            ddlSelectRecords.DataBind();
            if (Roles.IsUserInRole("SalesRep"))
            {
                divSalesRep.Visible = false;
            }
            else
            {
                divSalesRep.Visible = true;
            }
            try
            {
                txtInstallAddressline.Text = st.StreetAddress + ", " + st.StreetCity + " " + st.StreetState + " " + st.StreetPostCode;
            }
            catch { }
            if (txtInstallAddress.Text == string.Empty)
            {
                txtInstallAddress.Text = st.unit_type + " " + st.unit_number + " " + st.street_number + " " + st.street_name + " " + st.street_type;//st.street_address;
            }
            if (txtformbayUnitNo.Text == string.Empty)
            {
                txtformbayUnitNo.Text = st.unit_number;
            }

            if (ddlformbayunittype.SelectedValue == string.Empty)
            {
                ddlformbayunittype.SelectedValue = st.unit_type;
            }
            if (txtformbayStreetNo.Text == string.Empty)
            {
                txtformbayStreetNo.Text = st.street_number;
            }
            if (txtformbaystreetname.Text == string.Empty)
            {
                txtformbaystreetname.Text = st.street_name;
            }
            if (ddlformbaystreettype.SelectedValue == "")
            {
                ddlformbaystreettype.SelectedValue = st.street_type;
            }
            if (txtInstallCity.Text == string.Empty)
            {
                txtInstallCity.Text = st.StreetCity;
            }
            if (txtInstallState.Text == string.Empty)
            {
                txtInstallState.Text = st.StreetState;
            }
            if (txtInstallPostCode.Text == string.Empty)
            {
                txtInstallPostCode.Text = st.StreetPostCode;
            }
            txtProjectOpened.Text = DateTime.Now.AddHours(14).ToShortDateString();
            //if (Profile.eurosolar.projectid == string.Empty)
            //{
            //    divprojecttab.Visible = false;
            //}
            //if (st2.LinkProjectID != string.Empty)
            //{
            //    ddllinkprojectid.Visible = true;
            //    ddlProjectTypeID_SelectedIndexChanged(sender, e);
            //    ddllinkprojectid.SelectedValue = st2.LinkProjectID;
            //}
        }
    }
    public void BindDropDown()
    {
        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlContact.Items.Clear();
        ddlContact.Items.Add(item1);
        ddlContact.DataSource = ClstblContacts.tblContacts_SelectByCustId(Request.QueryString["compid"]);
        ddlContact.DataValueField = "ContactID";
        ddlContact.DataTextField = "Contact";
        ddlContact.DataMember = "Contact";
        ddlContact.DataBind();
        ListItem item2 = new ListItem();
        item2.Text = "Select";
        item2.Value = "";
        ddlProjectTypeID.Items.Clear();
        ddlProjectTypeID.Items.Add(item2);
        ddlProjectTypeID.DataSource = ClstblProjectType.tblProjectType_SelectActive();
        ddlProjectTypeID.DataValueField = "ProjectTypeID";
        ddlProjectTypeID.DataTextField = "ProjectType";
        ddlProjectTypeID.DataMember = "ProjectType";
        ddlProjectTypeID.DataBind();
        ddlformbaystreettype.DataSource = ClstblStreetType.tbl_formbaystreettype_SelectbyAsc();
        ddlformbaystreettype.DataMember = "StreetType";
        ddlformbaystreettype.DataTextField = "StreetType";
        ddlformbaystreettype.DataValueField = "StreetCode";
        ddlformbaystreettype.DataBind();
        ddlformbayunittype.DataSource = ClstblStreetType.tbl_formbayunittype_SelectbyAsc();
        ddlformbayunittype.DataMember = "UnitType";
        ddlformbayunittype.DataTextField = "UnitType";
        ddlformbayunittype.DataValueField = "UnitType";
        ddlformbayunittype.DataBind();
        DataTable dtProjectType = ClstblProjectType.tblProjectType_SelectType();
        if (dtProjectType.Rows.Count > 0)
        {
            ddlProjectTypeID.SelectedValue = dtProjectType.Rows[0]["ProjectTypeID"].ToString();
        }
        DataTable dtContact = ClstblContacts.tblContacts_SelectTop(Request.QueryString["compid"]);
        if (dtContact.Rows.Count > 0)
        {
            ddlContact.SelectedValue = dtContact.Rows[0]["ContactID"].ToString();
        }
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        ddlSalesRep.Text = stEmp.EmpFirst + " " + stEmp.EmpLast;
    }
    public void HideTabContainer()
    {
        divprojecttab.Visible = false;
        //Profile.eurosolar.projectid = "";
    }
    public void HideProjectAction()
    {
        if (Roles.IsUserInRole("Administrator"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Accountant"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("BookInstallation"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Customer"))
        {
            //divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("DSales Manager"))
        {
            //divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("DSalesRep"))
        {
            //divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Maintenance"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("PostInstaller"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("PreInstaller"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Purchase Manager"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Sales Manager"))
        {
            //divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("SalesRep"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("STC"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Verification"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("WarehouseManager"))
        {
            divrightbtn.Visible = false;
        }
        if (Roles.IsUserInRole("Installation Manager"))
        {
            divrightbtn.Visible = true;
        }
        if (Roles.IsUserInRole("Installer"))
        {
            divrightbtn.Visible = false;
        }
    }
    protected DataTable GetGridData()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = new DataTable();
        dt = ClstblProjects.tblProjects_SelectByUCustomerID(Request.QueryString["compid"]);
        //Response.Write(dt.Rows.Count);
        //Response.End();
        return dt;
    }
    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;
        DataTable dt = GetGridData();
        if (dt.Rows.Count == 0)
        {
            //  PanNoRecord.Visible = true;
            PanGrid.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            // PanNoRecord.Visible = false;
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string CustomerID = Request.QueryString["compid"];
        string ContactID = ddlContact.SelectedValue;

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = stEmp.EmployeeID;
        string SalesRep = stEmp.EmployeeID;
        string ProjectTypeID = ddlProjectTypeID.SelectedValue;
        string OldProjectNumber = "";
        if (ddllinkprojectid.SelectedItem.Text != "Select")
        {
            OldProjectNumber = ddllinkprojectid.SelectedItem.Text;// txtOldProjectNumber.Text;
        }
        string ManualQuoteNumber = txtManualQuoteNumber.Text;
        string ProjectOpened = txtProjectOpened.Text;
        SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"]);
        string Project = txtInstallCity.Text.Trim() + "-" + txtInstallAddress.Text.Trim();
        string InstallAddress = ddlformbayunittype.SelectedValue.Trim() + " " + txtformbayUnitNo.Text.Trim() + " " + txtformbayStreetNo.Text.Trim() + " " + txtformbaystreetname.Text.Trim() + " " + ddlformbaystreettype.SelectedValue.Trim();//txtInstallAddress.Text;
        string InstallCity = txtInstallCity.Text.Trim();
        string InstallState = txtInstallState.Text.Trim();
        string InstallPostCode = txtInstallPostCode.Text.Trim();
        string ProjectNotes = txtProjectNotes.Text;
        
        string AdditionalSystem = "False";
        string GridConnected = "False";
        string RebateApproved = "False";
        string ReceivedCredits = "False";
        string CreditEligible = "False";
        string MoreThanOneInstall = "False";
        string RequiredCompliancePaperwork = "False";
        string OutOfPocketDocs = "False";
        string OwnerGSTRegistered = "False";
        string HouseTypeID = "";
        string RoofTypeID = "";
        string RoofAngleID = "";
        string InstallBase = "1";
        string StandardPack = "False";
        string NumberPanels = "0";
        string PanelConfigNW = "0";
        string PanelConfigOTH = "0";
        string SystemCapKW = "0";
        string STCMultiplier = "0";
        string STCZoneRating = "0";
        string STCNumber = "0";
        string ElecDistOK = "False";
        string Asbestoss = "False";
        string MeterUG = "False";
        string MeterEnoughSpace = "False";
        string SplitSystem = "False";
        string CherryPicker = "False";
        string TravelTime = "0";
        string VarRoofType = "0";
        string VarRoofAngle = "0";
        string VarHouseType = "0";
        string VarAsbestos = "0";
        string VarMeterInstallation = "0";
        string VarMeterUG = "0";
        string VarTravelTime = "0";
        string VarSplitSystem = "0";
        string VarEnoughSpace = "0";
        string VarCherryPicker = "0";
        string VarOther = "0";
        string SpecialDiscount = "0";
        string DepositRequired = "0";
        string TotalQuotePrice = "0";
        string PreviousTotalQuotePrice = "0";
        string InvoiceExGST = "0";
        string InvoiceGST = "0";
        string BalanceGST = "0";
        string FinanceWithID = "1";
        string ServiceValue = "0";
        string QuoteSentNo = "0";
        string SignedQuote = "False";
        string MeterBoxPhotosSaved = "False";
        string ElecBillSaved = "False";
        string ProposedDesignSaved = "False";
        string InvoiceTag = "False";
        string InvRefund = "0";
        string DepositAmount = "0";
        string ReceiptSent = "False";
        string MeterIncluded = "False";
        string OffPeak = "False";
        string RECRebate = "0";
        string InstallAM1 = "False";
        string InstallPM1 = "False";
        string InstallAM2 = "False";
        string InstallPM2 = "False";
        string InstallDays = "0";
        string STCFormsDone = "False";
        string CustNotifiedInstall = "False";
        string InstallerNotes = txtInstallerNotes.Text;
        string WelcomeLetterDone = "False";
        string InstallRequestSaved = "False";
        string CertificateSaved = "False";
        string STCFormSaved = "False";
        string CustNotifiedMeter = "False";
        string MeterInstallerNotes = txtMeterInstallerNotes.Text;
        //string projectcancel = chkprojectcancel.Checked.ToString();
        string ProjectEnteredBy = stEmp.EmployeeID;
        bool sucCustType = ClstblCustomers.tblCustomers_UpdateCustType("2", CustomerID);
        bool sucContStatus = ClstblContacts.tblContacts_UpdateContLeadStatus("5", ddlContact.SelectedValue);
        if (stCust.isformbayadd == "True")
        {
            if (txtformbayStreetNo.Text != string.Empty || ddlformbaystreettype.SelectedValue != string.Empty || txtformbaystreetname.Text != string.Empty)
            {
                if (txtInstallCity.Text != string.Empty || txtInstallState.Text != string.Empty || txtInstallPostCode.Text != string.Empty)
                {
                    DataTable dtaddress = ClstblCustomers.tblCustomers_tblProjects_Exits_Address_new(CustomerID, InstallAddress, InstallCity, InstallState, InstallPostCode);
                    if (dtaddress.Rows.Count > 0)
                    {
                        PanAddUpdate.Visible = true;
                        Panel1.Visible = false;
                        //lblerror.Visible = true;
                        ModalPopupExtenderAddressExistency.Show();
                        //Reset();
                        divrightbtn.Visible = false;
                        PanGrid.Visible = false;
                        PanSearch.Visible = false;
                        // DataTable dt = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress_ByCustId(CustomerID, formbayunittype, formbayUnitNo, formbayStreetNo, formbaystreetname, formbaystreettype, InstallCity, InstallState, InstallPostCode);
                        //DataTable dt1 = ClstblCustomers.tblCustomers_Exits_Select_Address(txtstreetaddressline.Text, StreetCity, StreetState, StreetPostCode);
                        rptaddress.DataSource = dtaddress;
                        rptaddress.DataBind();
                    }
                    else
                    {
                        PanAddressValid.Visible = false;
                        // int success = ClstblProjects.tblProjects_Insert(CustomerID, ContactID, EmployeeID, SalesRep, ProjectTypeID, "2", "", "", ProjectOpened, "", OldProjectNumber, ManualQuoteNumber, Project, "", InstallAddress, InstallCity, InstallState, InstallPostCode, ProjectNotes, AdditionalSystem, GridConnected, RebateApproved, ReceivedCredits, CreditEligible, MoreThanOneInstall, RequiredCompliancePaperwork, OutOfPocketDocs, OwnerGSTRegistered, "", HouseTypeID, RoofTypeID, RoofAngleID, InstallBase, "", StandardPack, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", NumberPanels, "", PanelConfigNW, PanelConfigOTH, SystemCapKW, STCMultiplier, STCZoneRating, STCNumber, "", "", "", "", "", "", "", "", "", ElecDistOK, "", "", Asbestoss, MeterUG, MeterEnoughSpace, SplitSystem, CherryPicker, TravelTime, "", VarRoofType, VarRoofAngle, VarHouseType, VarAsbestos, VarMeterInstallation, VarMeterUG, VarTravelTime, VarSplitSystem, VarEnoughSpace, VarCherryPicker, VarOther, SpecialDiscount, DepositRequired, TotalQuotePrice, PreviousTotalQuotePrice, InvoiceExGST, InvoiceGST, BalanceGST, FinanceWithID, ServiceValue, "", QuoteSentNo, "", SignedQuote, MeterBoxPhotosSaved, ElecBillSaved, ProposedDesignSaved, "", "", "", InvoiceTag, "", "", "", "", "", InvRefund, "", "", "", DepositAmount, ReceiptSent, "", "", MeterIncluded, "", "", "", "", "", "", "", OffPeak, "", "", "", "", "", "", "", "", "", "", "", "", "", RECRebate, "", "", "", "", InstallAM1, InstallPM1, InstallAM2, InstallPM2, InstallDays, STCFormsDone, "", "", CustNotifiedInstall, "", InstallerNotes, "", "", "", WelcomeLetterDone, InstallRequestSaved, "", "", "", "", CertificateSaved, "", "", STCFormSaved, "", "", "", "", "", "", "", "", "", "", "", "", "", CustNotifiedMeter, "", "", MeterInstallerNotes, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ProjectEnteredBy);
                        //int success = ClstblProjects.tblProjects_Insert(CustomerID, ContactID, "1", SalesRep, ProjectTypeID, "2", "", "", ProjectOpened, "", OldProjectNumber, ManualQuoteNumber, Project, "", InstallAddress, InstallCity, InstallState, InstallPostCode, ProjectNotes, AdditionalSystem, GridConnected, RebateApproved, ReceivedCredits, CreditEligible, MoreThanOneInstall, RequiredCompliancePaperwork, OutOfPocketDocs, OwnerGSTRegistered, "", HouseTypeID, RoofTypeID, RoofAngleID, InstallBase, "", StandardPack, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", NumberPanels, "", PanelConfigNW, PanelConfigOTH, SystemCapKW, STCMultiplier, STCZoneRating, STCNumber, "", "", "", "", "", "", "", "", "", ElecDistOK, "", "", Asbestoss, MeterUG, MeterEnoughSpace, SplitSystem, CherryPicker, TravelTime, "", VarRoofType, VarRoofAngle, VarHouseType, VarAsbestos, VarMeterInstallation, VarMeterUG, VarTravelTime, VarSplitSystem, VarEnoughSpace, VarCherryPicker, VarOther, SpecialDiscount, DepositRequired, TotalQuotePrice, PreviousTotalQuotePrice, InvoiceExGST, InvoiceGST, BalanceGST, FinanceWithID, ServiceValue, "", QuoteSentNo, "", SignedQuote, MeterBoxPhotosSaved, ElecBillSaved, ProposedDesignSaved, "", "", "", InvoiceTag, "", "", "", "", "", InvRefund, "", "", "", DepositAmount, ReceiptSent, "", "", MeterIncluded, "", "", "", "", "", "", "", OffPeak, "", "", "", "", "", "", "", "", "", "", "", "", "", RECRebate, "", "", "", "", InstallAM1, InstallPM1, InstallAM2, InstallPM2, InstallDays, STCFormsDone, "", "", CustNotifiedInstall, "", InstallerNotes, "", "", "", WelcomeLetterDone, InstallRequestSaved, "", "", "", "", CertificateSaved, "", "", STCFormSaved, "", "", "", "", "", "", "", "", "", "", "", "", "", CustNotifiedMeter, "", "", MeterInstallerNotes, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ProjectEnteredBy);
                        int success = ClstblProjects.tblProjects_Insert(CustomerID, ContactID,EmployeeID, SalesRep, ProjectTypeID, "2", "", "", ProjectOpened, "", OldProjectNumber, ManualQuoteNumber, Project, "", InstallAddress, InstallCity, InstallState, InstallPostCode, ProjectNotes, AdditionalSystem, GridConnected, RebateApproved, ReceivedCredits, CreditEligible, MoreThanOneInstall, RequiredCompliancePaperwork, OutOfPocketDocs, OwnerGSTRegistered, "", HouseTypeID, RoofTypeID, RoofAngleID, InstallBase, "", StandardPack, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", NumberPanels, "", PanelConfigNW, PanelConfigOTH, SystemCapKW, STCMultiplier, STCZoneRating, STCNumber, "", "", "", "", "", "", "", "", "", ElecDistOK, "", "", Asbestoss, MeterUG, MeterEnoughSpace, SplitSystem, CherryPicker, TravelTime, "", VarRoofType, VarRoofAngle, VarHouseType, VarAsbestos, VarMeterInstallation, VarMeterUG, VarTravelTime, VarSplitSystem, VarEnoughSpace, VarCherryPicker, VarOther, SpecialDiscount, DepositRequired, TotalQuotePrice, PreviousTotalQuotePrice, InvoiceExGST, InvoiceGST, BalanceGST, FinanceWithID, ServiceValue, "", QuoteSentNo, "", SignedQuote, MeterBoxPhotosSaved, ElecBillSaved, ProposedDesignSaved, "", "", "", InvoiceTag, "", "", "", "", "", InvRefund, "", "", "", DepositAmount, ReceiptSent, "", "", MeterIncluded, "", "", "", "", "", "", "", OffPeak, "", "", "", "", "", "", "", "", "", "", "", "", "", RECRebate, "", "", "", "", InstallAM1, InstallPM1, InstallAM2, InstallPM2, InstallDays, STCFormsDone, "", "", CustNotifiedInstall, "", InstallerNotes, "", "", "", WelcomeLetterDone, InstallRequestSaved, "", "", "", "", CertificateSaved, "", "", STCFormSaved, "", "", "", "", "", "", "", "", "", "", "", "", "", CustNotifiedMeter, "", "", MeterInstallerNotes, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ProjectEnteredBy);
                        //if (ddlProjectTypeID.SelectedValue == "8")
                        //{
                        //    DataTable dtCust = ClstblProjects.tblProjects_SelectTop1(CustomerID);
                        //    if (dtCust.Rows.Count > 0)
                        //    {
                        //        string OldProjectID = dtCust.Rows[0]["ProjectID"].ToString();
                        //        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(OldProjectID);
                        //        ClsProjectSale.tblProjects_UpdateMtce(Convert.ToString(success), stPro.PanelBrandID, stPro.PanelBrand, stPro.PanelOutput, stPro.RECRebate, stPro.STCMultiplier, stPro.STCZoneRating, stPro.PanelModel, stPro.NumberPanels, stPro.SystemCapKW, stPro.InverterDetailsID, stPro.InverterBrand, stPro.InverterSeries, stPro.SecondInverterDetailsID, stPro.STCNumber, stPro.STCValue, stPro.InverterModel, stPro.InverterOutput);
                        //    }
                        //    int sucMtce = ClstblProjectMaintenance.tblProjectMaintenance_Insert(Convert.ToString(success), EmployeeID, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "False", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                        //}
                        int sucProject2 = ClstblProjects.tblProjects_InserttblProjects2(Convert.ToString(success), EmployeeID);
                        //bool succ_updateprojectcancel = ClstblProjects.tblProjects_projectcancel_update(Convert.ToString(success), projectcancel);
                        //bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(Convert.ToString(success), hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, hndstreetsuffix.Value);
                        bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(Convert.ToString(success), ddlformbayunittype.SelectedValue.Trim(), txtformbayUnitNo.Text.Trim(), txtformbayStreetNo.Text.Trim(), txtformbaystreetname.Text.Trim(), ddlformbaystreettype.SelectedValue.Trim(), hndstreetsuffix.Value.Trim(), Project);
                        //bool succ_street = ClsProjectSale.tblCustomer_Update_street_address(Convert.ToString(success), txtInstallAddressline.Text);
                        bool succ_street = ClsProjectSale.tblCustomer_Update_street_address(Convert.ToString(success), InstallAddress);
                        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(success.ToString());
                        int successpk = ClstblProjects.tbl_StockPickUpDetail_Insert(success.ToString(), st.ProjectNumber, "", "", "", "", "", "");
                        int successpkr = ClstblProjects.tbl_StockPickUpReturnDetail_Insert(success.ToString(), st.ProjectNumber, "", "", "", "", "", "");
                        if (ddlProjectTypeID.SelectedValue == "2")
                        {
                            ClsProjectSale.tblProjects_UpdateFormsSolar(Convert.ToString(success));
                        }
                        if (ddlProjectTypeID.SelectedValue == "3")
                        {
                            ClsProjectSale.tblProjects_UpdateFormsSolarUG(Convert.ToString(success));
                        }
                        string EmpID = stEmp.EmployeeID;


                        if (ddlProjectTypeID.SelectedValue == "8")
                        {
                            int sucTrack1 = ClstblProjects.tblTrackProjStatus_Insert("21", Convert.ToString(success), EmpID, NumberPanels);
                            ClsProjectSale.tblProjects_UpdateProjectStatusID(Convert.ToString(success), "21");
                        }
                        else
                        {
                            int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("2", Convert.ToString(success), EmpID, NumberPanels);
                        }
                        String ProjectID = Convert.ToString(success);
                        bool s1 = ClstblProjects.tblProjects_UpdateLinkProjectID(ProjectID, ddllinkprojectid.SelectedValue);
                        if (ddlProjectTypeID.SelectedValue == "7")
                        {
                            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "15");//new inquiry
                        }
                        #region linkproject
                        if (ddlProjectTypeID.SelectedValue == "8")
                        {
                            if (ddllinkprojectid.SelectedValue != string.Empty)
                            {
                                //if (stproject.LinkProjectID != string.Empty)
                                {
                                    SttblProjects st_linkproject = ClstblProjects.tblProjects_SelectByProjectID(ddllinkprojectid.SelectedValue);
                                    string PanelBrandID = st_linkproject.PanelBrandID;
                                    string PanelBrand = st_linkproject.PanelBrand;
                                    string PanelOutput = st_linkproject.PanelOutput;
                                    string RECRebate1 = st_linkproject.RECRebate;
                                    string STCMultiplier1 = st_linkproject.STCMultiplier;
                                    string STCZoneRating1 = "";
                                    string PanelModel = st_linkproject.PanelModel;
                                    string NumberPanels1 = st_linkproject.NumberPanels;
                                    string SystemCapKW1 = st_linkproject.SystemCapKW;
                                    string VicRebate = st_linkproject.ViCLoan;
                                    string VicRebateLoan = st_linkproject.VicLoanDisc;

                                    string PanelDetails = "";
                                    if (PanelBrandID != string.Empty)
                                    {
                                        SttblStockItems stPanel = ClstblStockItems.tblStockItems_SelectByStockItemID(PanelBrandID);
                                        if (PanelBrandID != "")
                                        {
                                            PanelDetails = NumberPanels1 + " X " + PanelOutput + " Watt " + stPanel.StockItem + " Panels. (" + PanelModel + ")";
                                        }
                                    }
                                    string InverterDetailsID = st_linkproject.InverterDetailsID;
                                    string InverterBrand = st_linkproject.InverterBrand;
                                    string InverterSeries = st_linkproject.InverterSeries;
                                    string SecondInverterDetailsID = st_linkproject.SecondInverterDetailsID;
                                    string ThirdInverterDetailsID = st_linkproject.ThirdInverterDetailsID;
                                    string STCNumber1 = st_linkproject.STCNumber;
                                    DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
                                    string STCValue = "0";
                                    if (dtSTCValue.Rows.Count > 0)
                                    {
                                        STCValue = dtSTCValue.Rows[0]["STCValue"].ToString();
                                    }
                                    string InverterModel = st_linkproject.InverterModel;
                                    string InverterOutput = st_linkproject.InverterOutput;
                                    string InverterDetails = "";
                                    if (InverterDetailsID != "")
                                    {
                                        SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(InverterDetailsID);
                                        InverterDetails = "One " + stInverter.StockItem + " KW Inverter ";
                                    }
                                    if (SecondInverterDetailsID != "")
                                    {
                                        SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(SecondInverterDetailsID);
                                        InverterDetails = "One " + stInverter2.StockItem + " KW Inverter";
                                    }
                                    if (InverterDetailsID != "" && SecondInverterDetailsID != "")
                                    {
                                        SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(InverterDetailsID);
                                        SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(SecondInverterDetailsID);
                                        InverterDetails = "One " + stInverter.StockItem + " KW Inverter. Plus " + stInverter2.StockItem + " KW Inverter.";
                                    }
                                    string SystemDetails = "";
                                    if (PanelBrandID != "")
                                    {
                                        SttblStockItems stPanel = ClstblStockItems.tblStockItems_SelectByStockItemID(PanelBrandID);
                                        SystemDetails = NumberPanels1 + " X " + stPanel.StockItem;
                                    }
                                    if (InverterDetailsID != "")
                                    {
                                        SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(InverterDetailsID);
                                        SystemDetails += " + " + stInverter.StockItem;
                                    }
                                    if (SecondInverterDetailsID != "")
                                    {
                                        SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(SecondInverterDetailsID);
                                        SystemDetails += " + " + stInverter2.StockItem;
                                    }
                                    string ElecDistributorID = st_linkproject.ElecDistributorID;
                                    string ElecDistApprovelRef = st_linkproject.ElecDistApprovelRef;
                                    string ElecRetailerID = st_linkproject.ElecRetailerID;
                                    string RegPlanNo = st_linkproject.RegPlanNo;
                                    string HouseTypeID1 = st_linkproject.HouseTypeID;
                                    string RoofTypeID1 = st_linkproject.RoofTypeID;
                                    string RoofAngleID1 = st_linkproject.RoofAngleID;
                                    string LotNumber = st_linkproject.LotNumber;
                                    string NMINumber = st_linkproject.NMINumber;
                                    string MeterNumber1 = st_linkproject.MeterNumber1;
                                    string MeterNumber2 = st_linkproject.MeterNumber2;
                                    string MeterPhase = st_linkproject.MeterPhase;
                                    string MeterEnoughSpace1 = Convert.ToString(st_linkproject.MeterEnoughSpace);
                                    string OffPeak1 = Convert.ToString(st_linkproject.OffPeak);
                                    if (st_linkproject.InstallPostCode != "")
                                    {
                                        DataTable dtZoneCode1 = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(st_linkproject.InstallPostCode);
                                        if (dtZoneCode1.Rows.Count > 0)
                                        {
                                            string STCZoneID = dtZoneCode1.Rows[0]["STCZoneID"].ToString();
                                            DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                                            STCZoneRating = dtZoneRt.Rows[0]["STCRating"].ToString();
                                        }
                                    }
                                    string ElecDistApplied = st_linkproject.ElecDistApplied.Trim();
                                    string ElecDistApplyMethod = st_linkproject.ElecDistApplyMethod;
                                    string ElecDistApplySentFrom = st_linkproject.ElecDistApplySentFrom;
                                    SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ddllinkprojectid.SelectedValue);
                                    string SPAInvoiceNumber = st2.SPAInvoiceNumber;
                                    string SPAPaid = st2.SPAPaid.Trim();
                                    string SPAPaidBy = st2.SPAPaidBy;
                                    string SPAPaidAmount = st2.SPAPaidAmount;
                                    string SPAPaidMethod = st2.SPAPaidMethod;
                                    string FlatPanels = st_linkproject.FlatPanels;
                                    string PitchedPanels = st_linkproject.PitchedPanels;
                                    string SurveyCerti = st_linkproject.SurveyCerti;
                                    string CertiApprove = st_linkproject.CertiApprove;
                                    string ElecDistAppDate = st_linkproject.ElecDistAppDate;
                                    string ElecDistAppBy = st_linkproject.ElecDistAppBy;
                                    DataTable dtZoneCode2 = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(st_linkproject.InstallPostCode);
                                    if (dtZoneCode2.Rows.Count > 0)
                                    {
                                        string STCZoneID = dtZoneCode2.Rows[0]["STCZoneID"].ToString();
                                        DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                                        STCZoneRating1 = dtZoneRt.Rows[0]["STCRating"].ToString();
                                    }
                                    SttblEmployees stEmp1 = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                                    string UpdatedBy1 = stEmp1.EmployeeID;
                                    if (NumberPanels1 != st_linkproject.NumberPanels)
                                    {
                                        bool sucSalePrice = ClsProjectSale.tblProjects_UpdateSalePrice(ProjectID);
                                    }
                                    bool successSale = ClsProjectSale.tblProjects_UpdateSale(ProjectID, PanelBrandID, PanelBrand, PanelOutput, RECRebate1, STCMultiplier1, STCZoneRating1, PanelModel, NumberPanels1, SystemCapKW1, PanelDetails, InverterDetailsID, InverterBrand, InverterSeries, SecondInverterDetailsID, STCNumber1, STCValue, InverterModel, InverterOutput, InverterDetails, SystemDetails, ElecDistributorID, ElecDistApprovelRef, ElecRetailerID, RegPlanNo, HouseTypeID1, RoofTypeID1, RoofAngleID1, LotNumber, NMINumber, MeterNumber1, MeterNumber2, MeterPhase, MeterEnoughSpace1, OffPeak1, UpdatedBy1, ElecDistApplied, ElecDistApplyMethod, ElecDistApplySentFrom, FlatPanels, PitchedPanels, SurveyCerti, ElecDistAppDate, ElecDistAppBy, CertiApprove, ThirdInverterDetailsID,VicRebate,VicRebateLoan);
                                    bool sucQuote2 = ClsProjectSale.tblProjects2_UpdateQuote(ProjectID, SPAInvoiceNumber, SPAPaid, SPAPaidBy, SPAPaidAmount, SPAPaidMethod);
                                    ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "15");//new inquiry
                                    DataTable dt_promtce = ClstblProjectMaintenance.tblProjectMaintenance_SelectByProjectID(ProjectID);
                                    if (st_linkproject.ProjectTypeID == "8")
                                    {
                                        ClsProjectSale.tblProjectMaintenance_UpdateDetail(ProjectID, DateTime.Now.AddHours(14).ToShortDateString(), dt_promtce.Rows[0]["ProjectMtceReasonID"].ToString(), dt_promtce.Rows[0]["ProjectMtceReasonSubID"].ToString(), dt_promtce.Rows[0]["ProjectMtceCallID"].ToString(), dt_promtce.Rows[0]["ProjectMtceStatusID"].ToString(), dt_promtce.Rows[0]["Warranty"].ToString(), dt_promtce.Rows[0]["CustomerInput"].ToString(), dt_promtce.Rows[0]["FaultIdentified"].ToString(), dt_promtce.Rows[0]["ActionRequired"].ToString(), dt_promtce.Rows[0]["WorkDone"].ToString());
                                    }
                                }
                            }
                        }
                        #endregion
                        //--- do not chage this code start------
                        if (Convert.ToString(success) != "")
                        {
                            Reset();
                            SetUpdate();
                        }
                        else
                        {
                            SetError();
                            divrightbtn.Visible = true;
                        }
                        divrightbtn.Visible = true;
                    }
                }
                else
                {
                    SetError();
                    divrightbtn.Visible = true;
                    litError.Text = "Please Enter Customer Address.";
                }
            }
            else
            {
                ModalPopupExtenderAddress.Show();
            }
        }
        else
        {
            if (stCust.street_number != string.Empty || stCust.street_type != string.Empty || stCust.street_name != string.Empty)
            {
                PanAddressValid.Visible = false;
                if (stCust.StreetCity != string.Empty || stCust.StreetState != string.Empty || stCust.StreetPostCode != string.Empty)
                {
                    int success = ClstblProjects.tblProjects_Insert(CustomerID, ContactID, EmployeeID, SalesRep, ProjectTypeID, "2", "", "", ProjectOpened, "", OldProjectNumber, ManualQuoteNumber, Project, "", InstallAddress, InstallCity, InstallState, InstallPostCode, ProjectNotes, AdditionalSystem, GridConnected, RebateApproved, ReceivedCredits, CreditEligible, MoreThanOneInstall, RequiredCompliancePaperwork, OutOfPocketDocs, OwnerGSTRegistered, "", HouseTypeID, RoofTypeID, RoofAngleID, InstallBase, "", StandardPack, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", NumberPanels, "", PanelConfigNW, PanelConfigOTH, SystemCapKW, STCMultiplier, STCZoneRating, STCNumber, "", "", "", "", "", "", "", "", "", ElecDistOK, "", "", Asbestoss, MeterUG, MeterEnoughSpace, SplitSystem, CherryPicker, TravelTime, "", VarRoofType, VarRoofAngle, VarHouseType, VarAsbestos, VarMeterInstallation, VarMeterUG, VarTravelTime, VarSplitSystem, VarEnoughSpace, VarCherryPicker, VarOther, SpecialDiscount, DepositRequired, TotalQuotePrice, PreviousTotalQuotePrice, InvoiceExGST, InvoiceGST, BalanceGST, FinanceWithID, ServiceValue, "", QuoteSentNo, "", SignedQuote, MeterBoxPhotosSaved, ElecBillSaved, ProposedDesignSaved, "", "", "", InvoiceTag, "", "", "", "", "", InvRefund, "", "", "", DepositAmount, ReceiptSent, "", "", MeterIncluded, "", "", "", "", "", "", "", OffPeak, "", "", "", "", "", "", "", "", "", "", "", "", "", RECRebate, "", "", "", "", InstallAM1, InstallPM1, InstallAM2, InstallPM2, InstallDays, STCFormsDone, "", "", CustNotifiedInstall, "", InstallerNotes, "", "", "", WelcomeLetterDone, InstallRequestSaved, "", "", "", "", CertificateSaved, "", "", STCFormSaved, "", "", "", "", "", "", "", "", "", "", "", "", "", CustNotifiedMeter, "", "", MeterInstallerNotes, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ProjectEnteredBy);
                    int sucProject2 = ClstblProjects.tblProjects_InserttblProjects2(Convert.ToString(success), EmployeeID);
                    bool succ_updateAddress = ClsProjectSale.tblProjects_Update_Address(Convert.ToString(success), ddlformbayunittype.SelectedValue.Trim(), txtformbayUnitNo.Text.Trim(), txtformbayStreetNo.Text.Trim(), txtformbaystreetname.Text.Trim(), ddlformbaystreettype.SelectedValue.Trim(), hndstreetsuffix.Value.Trim(), Project);
                    bool succ_street = ClsProjectSale.tblCustomer_Update_street_address(Convert.ToString(success), InstallAddress);
                    SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(success.ToString());
                    int successpk = ClstblProjects.tbl_StockPickUpDetail_Insert(success.ToString(), st.ProjectNumber, "", "", "", "", "", "");
                    int successpkr = ClstblProjects.tbl_StockPickUpReturnDetail_Insert(success.ToString(), st.ProjectNumber, "", "", "", "", "", "");
                    if (ddlProjectTypeID.SelectedValue == "2")
                    {
                        ClsProjectSale.tblProjects_UpdateFormsSolar(Convert.ToString(success));
                    }
                    if (ddlProjectTypeID.SelectedValue == "3")
                    {
                        ClsProjectSale.tblProjects_UpdateFormsSolarUG(Convert.ToString(success));
                    }
                    string EmpID = stEmp.EmployeeID;
                    if (ddlProjectTypeID.SelectedValue == "8")
                    {

                        int sucTrack1 = ClstblProjects.tblTrackProjStatus_Insert("21", Convert.ToString(success), EmpID, NumberPanels);

                    }
                    else
                    {
                        int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("2", Convert.ToString(success), EmpID, NumberPanels);
                    }
                    String ProjectID = Convert.ToString(success);
                    bool s1 = ClstblProjects.tblProjects_UpdateLinkProjectID(ProjectID, ddllinkprojectid.SelectedValue);



                    #region linkproject
                    if (ddlProjectTypeID.SelectedValue == "8")
                    {
                        if (ddllinkprojectid.SelectedValue != string.Empty)
                        {
                            //if (stproject.LinkProjectID != string.Empty)
                            {
                                SttblProjects st_linkproject = ClstblProjects.tblProjects_SelectByProjectID(ddllinkprojectid.SelectedValue);
                                string PanelBrandID = st_linkproject.PanelBrandID;
                                string PanelBrand = st_linkproject.PanelBrand;
                                string PanelOutput = st_linkproject.PanelOutput;
                                string RECRebate1 = st_linkproject.RECRebate;
                                string STCMultiplier1 = st_linkproject.STCMultiplier;
                                string STCZoneRating1 = "";
                                string PanelModel = st_linkproject.PanelModel;
                                string NumberPanels1 = st_linkproject.NumberPanels;
                                string SystemCapKW1 = st_linkproject.SystemCapKW;
                                string PanelDetails = "";
                                if (PanelBrandID != string.Empty)
                                {
                                    SttblStockItems stPanel = ClstblStockItems.tblStockItems_SelectByStockItemID(PanelBrandID);
                                    if (PanelBrandID != "")
                                    {
                                        PanelDetails = NumberPanels1 + " X " + PanelOutput + " Watt " + stPanel.StockItem + " Panels. (" + PanelModel + ")";
                                    }
                                }
                                string InverterDetailsID = st_linkproject.InverterDetailsID;
                                string InverterBrand = st_linkproject.InverterBrand;
                                string InverterSeries = st_linkproject.InverterSeries;
                                string SecondInverterDetailsID = st_linkproject.SecondInverterDetailsID;
                                string ThirdInverterDetailID = st_linkproject.ThirdInverterDetailsID;
                                string STCNumber1 = st_linkproject.STCNumber;
                                DataTable dtSTCValue = ClsProjectSale.tblDefaults_Select();
                                string STCValue = "0";
                                if (dtSTCValue.Rows.Count > 0)
                                {
                                    STCValue = dtSTCValue.Rows[0]["STCValue"].ToString();
                                }
                                string InverterModel = st_linkproject.InverterModel;
                                string InverterOutput = st_linkproject.InverterOutput;
                                string InverterDetails = "";
                                if (InverterDetailsID != "")
                                {
                                    SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(InverterDetailsID);
                                    InverterDetails = "One " + stInverter.StockItem + " KW Inverter ";
                                }
                                if (SecondInverterDetailsID != "")
                                {
                                    SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(SecondInverterDetailsID);
                                    InverterDetails = "One " + stInverter2.StockItem + " KW Inverter";
                                }
                                if (InverterDetailsID != "" && SecondInverterDetailsID != "")
                                {
                                    SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(InverterDetailsID);
                                    SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(SecondInverterDetailsID);
                                    InverterDetails = "One " + stInverter.StockItem + " KW Inverter. Plus " + stInverter2.StockItem + " KW Inverter.";
                                }
                                string SystemDetails = "";
                                if (PanelBrandID != "")
                                {
                                    SttblStockItems stPanel = ClstblStockItems.tblStockItems_SelectByStockItemID(PanelBrandID);
                                    SystemDetails = NumberPanels1 + " X " + stPanel.StockItem;
                                }
                                if (InverterDetailsID != "")
                                {
                                    SttblStockItems stInverter = ClstblStockItems.tblStockItems_SelectByStockItemID(InverterDetailsID);
                                    SystemDetails += " + " + stInverter.StockItem;
                                }
                                if (SecondInverterDetailsID != "")
                                {
                                    SttblStockItems stInverter2 = ClstblStockItems.tblStockItems_SelectByStockItemID(SecondInverterDetailsID);
                                    SystemDetails += " + " + stInverter2.StockItem;
                                }
                                string ElecDistributorID = st_linkproject.ElecDistributorID;
                                string ElecDistApprovelRef = st_linkproject.ElecDistApprovelRef;
                                string ElecRetailerID = st_linkproject.ElecRetailerID;
                                string RegPlanNo = st_linkproject.RegPlanNo;
                                string HouseTypeID1 = st_linkproject.HouseTypeID;
                                string RoofTypeID1 = st_linkproject.RoofTypeID;
                                string RoofAngleID1 = st_linkproject.RoofAngleID;
                                string LotNumber = st_linkproject.LotNumber;
                                string NMINumber = st_linkproject.NMINumber;
                                string MeterNumber1 = st_linkproject.MeterNumber1;
                                string MeterNumber2 = st_linkproject.MeterNumber2;
                                string MeterPhase = st_linkproject.MeterPhase;
                                string MeterEnoughSpace1 = Convert.ToString(st_linkproject.MeterEnoughSpace);
                                string OffPeak1 = Convert.ToString(st_linkproject.OffPeak);
                                if (st_linkproject.InstallPostCode != "")
                                {
                                    DataTable dtZoneCode1 = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(st_linkproject.InstallPostCode);
                                    if (dtZoneCode1.Rows.Count > 0)
                                    {
                                        string STCZoneID = dtZoneCode1.Rows[0]["STCZoneID"].ToString();
                                        DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                                        STCZoneRating = dtZoneRt.Rows[0]["STCRating"].ToString();
                                    }
                                }
                                string ElecDistApplied = st_linkproject.ElecDistApplied.Trim();
                                string ElecDistApplyMethod = st_linkproject.ElecDistApplyMethod;
                                string ElecDistApplySentFrom = st_linkproject.ElecDistApplySentFrom;
                                SttblProjects2 st2 = ClstblProjects.tblProjects2_SelectByProjectID(ddllinkprojectid.SelectedValue);
                                string SPAInvoiceNumber = st2.SPAInvoiceNumber;
                                string SPAPaid = st2.SPAPaid.Trim();
                                string SPAPaidBy = st2.SPAPaidBy;
                                string SPAPaidAmount = st2.SPAPaidAmount;
                                string SPAPaidMethod = st2.SPAPaidMethod;
                                string FlatPanels = st_linkproject.FlatPanels;
                                string PitchedPanels = st_linkproject.PitchedPanels;
                                string SurveyCerti = st_linkproject.SurveyCerti;
                                string CertiApprove = st_linkproject.CertiApprove;
                                string ElecDistAppDate = st_linkproject.ElecDistAppDate;
                                string ElecDistAppBy = st_linkproject.ElecDistAppBy;
                                DataTable dtZoneCode2 = ClsProjectSale.tblSTCPostCodes_SelectZoneByState(st_linkproject.InstallPostCode);
                                if (dtZoneCode2.Rows.Count > 0)
                                {
                                    string STCZoneID = dtZoneCode2.Rows[0]["STCZoneID"].ToString();
                                    DataTable dtZoneRt = ClsProjectSale.tblSTCZoneRatings_SelectBySTCZoneID(STCZoneID);
                                    STCZoneRating1 = dtZoneRt.Rows[0]["STCRating"].ToString();
                                }
                                SttblEmployees stEmp1 = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                                string UpdatedBy1 = stEmp1.EmployeeID;
                                if (NumberPanels1 != st_linkproject.NumberPanels)
                                {
                                    bool sucSalePrice = ClsProjectSale.tblProjects_UpdateSalePrice(ProjectID);
                                }
                                bool successSale = ClsProjectSale.tblProjects_UpdateSale(ProjectID, PanelBrandID, PanelBrand, PanelOutput, RECRebate1, STCMultiplier1, STCZoneRating1, PanelModel, NumberPanels1, SystemCapKW1, PanelDetails, InverterDetailsID, InverterBrand, InverterSeries, SecondInverterDetailsID, STCNumber1, STCValue, InverterModel, InverterOutput, InverterDetails, SystemDetails, ElecDistributorID, ElecDistApprovelRef, ElecRetailerID, RegPlanNo, HouseTypeID1, RoofTypeID1, RoofAngleID1, LotNumber, NMINumber, MeterNumber1, MeterNumber2, MeterPhase, MeterEnoughSpace1, OffPeak1, UpdatedBy1, ElecDistApplied, ElecDistApplyMethod, ElecDistApplySentFrom, FlatPanels, PitchedPanels, SurveyCerti, ElecDistAppDate, ElecDistAppBy, CertiApprove, ThirdInverterDetailID,"","");
                                bool sucQuote2 = ClsProjectSale.tblProjects2_UpdateQuote(ProjectID, SPAInvoiceNumber, SPAPaid, SPAPaidBy, SPAPaidAmount, SPAPaidMethod);
                                ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "15");//new inquiry
                                DataTable dt_promtce = ClstblProjectMaintenance.tblProjectMaintenance_SelectByProjectID(ProjectID);
                                if (st_linkproject.ProjectTypeID == "8")
                                {
                                    ClsProjectSale.tblProjectMaintenance_UpdateDetail(ProjectID, DateTime.Now.AddHours(14).ToShortDateString(), dt_promtce.Rows[0]["ProjectMtceReasonID"].ToString(), dt_promtce.Rows[0]["ProjectMtceReasonSubID"].ToString(), dt_promtce.Rows[0]["ProjectMtceCallID"].ToString(), dt_promtce.Rows[0]["ProjectMtceStatusID"].ToString(), dt_promtce.Rows[0]["Warranty"].ToString(), dt_promtce.Rows[0]["CustomerInput"].ToString(), dt_promtce.Rows[0]["FaultIdentified"].ToString(), dt_promtce.Rows[0]["ActionRequired"].ToString(), dt_promtce.Rows[0]["WorkDone"].ToString());
                                }
                            }
                        }
                    }
                    #endregion
                    //--- do not chage this code start------
                    if (Convert.ToString(success) != "")
                    {
                        Reset();
                        SetUpdate();
                    }
                    else
                    {
                        SetError();
                    }
                    divrightbtn.Visible = true;
                }
                else
                {
                    SetError();
                    litError.Text = "Please Enter Customer Address.";
                }
            }
            else
            {
                PanAddressValid.Visible = true;
            }
        }
        BindGrid(0);
        //--- do not chage this code end------
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        //string ProjectID = GridView1.SelectedDataKey.Value.ToString();
        //string CustomerID = Request.QueryString["compid"];
        //string ContactID = ddlContact.SelectedValue;
        //string EmployeeID = ddlSalesRep.SelectedValue;
        //string SalesRep = ddlSalesRep.SelectedValue;
        //string ProjectTypeID = ddlProjectTypeID.SelectedValue;
        //string ProjectOpened = txtProjectOpened.Text;
        //string OldProjectNumber = txtOldProjectNumber.Text;
        //string ManualQuoteNumber = txtManualQuoteNumber.Text;
        //SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"]);
        //string Project = txtInstallCity.Text + "-" + txtInstallAddress;
        //string InstallAddress = txtInstallAddress.Text;
        //string InstallCity = txtInstallCity.Text;
        //string InstallState = txtInstallState.Text;
        //string InstallPostCode = txtInstallPostCode.Text;
        //string ProjectNotes = txtProjectNotes.Text;
        //string InstallerNotes = txtInstallerNotes.Text;
        //string MeterInstallerNotes = txtMeterInstallerNotes.Text;
        //string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        //SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        //string UpdatedBy = stEmp.EmployeeID;
        //bool sucDetail = ClsProjectSale.tblProjects_UpdateDetails(ProjectID, ContactID, EmployeeID, SalesRep, ProjectTypeID, ProjectOpened, OldProjectNumber, ManualQuoteNumber, ProjectNotes, "", "", InstallerNotes, MeterInstallerNotes, InstallAddress, InstallCity, InstallState, InstallPostCode, UpdatedBy, Project);
        ////--- do not chage this code Start------
        //if (sucDetail)
        //{
        //    SetUpdate();
        //}
        //else
        //{
        //    SetError();
        //}
        //BindGrid(0);
        //--- do not chage this code end------
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);
    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(id);
        ltproject.Text = st.ProjectNumber + " -" + st.Project;
        //Profile.eurosolar.projectid = id;
        ViewProjectDetail();
    }
    public void HideAndShowPanel()
    {
        TabPrice.Visible = false;
        TabQuote.Visible = false;
        TabFinance.Visible = false;
        TabForms.Visible = false;
        TabSTC.Visible = false;
        TabPreInst.Visible = false;
        TabPostInst.Visible = false;
        TabElecInv.Visible = false;
        TabMtce.Visible = false;
        TabDocs.Visible = false;
        TabRefund.Visible = false;
        TabMaintenance.Visible = true;
    }
    public void ViewProjectDetail()
    {

        divprojecttab.Visible = true;
        TabContainer1.ActiveTabIndex = 0;
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        ltproject.Text = st.ProjectNumber + " -" + st.Project;
        if (Request.QueryString["m"] == "proelec")
        {
            TabContainer1.ActiveTabIndex = 9;
            projectelecinv1.BindProjectElecInv();
        }

        if (Request.QueryString["m"] == "propre")
        {
            TabContainer1.ActiveTabIndex = 7;
            projectpreinst1.GetPreInstClick();
            //projectpreinst1.BindProjectPreInst();
        }
        if (Roles.IsUserInRole("Installer"))
        {
            projectdocs1.BindDocs();
        }
        else
        {
            projectdetail1.BindProjectDetail();
        }

        if ((Roles.IsUserInRole("SalesRep")) || (Roles.IsUserInRole("DSalesRep")))
        {
            TabRefund.Visible = true;
            TabForms.Visible = false;
            TabPreInst.Visible = false;
            TabPostInst.Visible = false;
            TabElecInv.Visible = false;

            TabSTC.Visible = false;
            TabDocs.Visible = false;
            TabMtce.Visible = false;

            this.TabContainer1.Tabs[12].Visible = true;
        }
        if ((Roles.IsUserInRole("Accountant")) || (Roles.IsUserInRole("Administrator")))
        {

            if (Session["openMaintainace"].ToString() == "close")
            {
                //Response.Write("Hiiiiiiii");
                //Response.End();
                TabForms.Visible = true;
                TabPreInst.Visible = true;
                TabPostInst.Visible = true;
                TabElecInv.Visible = true;
                TabRefund.Visible = true;
                TabDocs.Visible = true;
                TabMtce.Visible = true;
                projectmaintenance1.Visible = false;

                //TabPrice.Visible = true;
                //TabSTC.Visible = true;
                //TabQuote.Visible = true;
                //TabFinance.Visible = true;
            }
            else
            {
                if ((!Roles.IsUserInRole("SalesRep")))
                {
                    HideAndShowPanel();
                }
            }
        }
        if ((Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("Sales Manager")))
        {

            TabForms.Visible = false;
            TabPreInst.Visible = false;
            TabPostInst.Visible = false;
            TabElecInv.Visible = false;
            TabSTC.Visible = false;
            TabMtce.Visible = false;

        }

        if ((Roles.IsUserInRole("PostInstaller")))
        {

            TabPostInst.Visible = true;
            TabElecInv.Visible = true;
            //TabRefund.Visible = false;

        }
        if ((Roles.IsUserInRole("Installer")))
        {

            TabDetail.Visible = false;
            // TabSale.Visible = false;
            TabPrice.Visible = false;
            TabQuote.Visible = false;
            TabFinance.Visible = false;
            TabForms.Visible = false;
            TabPreInst.Visible = false;
            TabPostInst.Visible = false;
            //TabRefund.Visible = false;
            TabSTC.Visible = false;
            TabElecInv.Visible = false;
            TabDocs.Visible = true;

        }
        else
        {
            if (Roles.IsUserInRole("Administrator"))
            {
                if (Session["openMaintainace"].ToString() == "close")
                {
                    TabDocs.Visible = true;
                    projectmaintenance1.Visible = false;
                }
                else
                {
                    TabDocs.Visible = false;
                }
            }
            else
            {
                TabDocs.Visible = false;
            }
        }
        if (Roles.IsUserInRole("Installation Manager"))
        {
            //Response.Write("Hiiiiiiii");
            //Response.End();
            TabDocs.Visible = true;
            TabRefund.Visible = true;
            //TabVicDoc.Visible = false;

        }
        if ((Roles.IsUserInRole("PostInstaller")))
        {
            TabDocs.Visible = true;
        }
        if ((Roles.IsUserInRole("PreInstaller")))
        {

            TabPostInst.Visible = false;
            TabElecInv.Visible = false;
            TabDocs.Visible = true;

        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(panprojects,
                                           this.GetType(),
                                           "MyAction",
                                           "doMyAction();",
                                           true);
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();
        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End
        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    public void ClearProjectSelection()
    {
        divprojecttab.Visible = false;
    }
    protected void lnkclose_Click(object sender, EventArgs e)
    {
        ClearProjectSelection();
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(panprojects,
                                           this.GetType(),
                                           "MyAction",
                                           "doMyAction();",
                                           true);
        ClearProjectSelection();
        InitAdd(); SttblCustomers st = ClstblCustomers.tblCustomers_SelectByCustomerID(Request.QueryString["compid"]);
        if (txtInstallAddress.Text == string.Empty)
        {
            txtInstallAddress.Text = st.unit_type + " " + st.unit_number + " " + st.street_number + " " + st.street_name + " " + st.street_type;//st.street_address;
        }
        if (txtInstallCity.Text == string.Empty)
        {
            txtInstallCity.Text = st.StreetCity;
        }
        if (txtInstallState.Text == string.Empty)
        {
            txtInstallState.Text = st.StreetState;
        }
        if (txtInstallPostCode.Text == string.Empty)
        {
            txtInstallPostCode.Text = st.StreetPostCode;
        }
        divrightbtn.Visible = false;
        PanGrid.Visible = false;
        PanSearch.Visible = false;
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
        lnkAdd.Visible = false;
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(panprojects,
                                           this.GetType(),
                                           "MyAction",
                                           "doMyAction();",
                                           true);
        //  Reset();
        PanAddUpdate.Visible = false;
        lnkAdd.Visible = true;
        divrightbtn.Visible = true;
        Panel1.Visible = true;
        PanGrid.Visible = true;
        PanSearch.Visible = true;
    }
    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);
    }
    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }
    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }
    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    public void SetAdd()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
        PanAlreadExists.Visible = false;
    }
    public void SetUpdate()
    {
        Reset();
        HidePanels();
        //PanSuccess.Visible = true;
        SetAdd1();
        PanSearch.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd1();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError1();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
        PanAddUpdate.Visible = true;
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        //lblAddUpdate.Text = "Add ";
    }
    public void InitUpdate()
    {
        HidePanels();
        PanAddUpdate.Visible = true;
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnReset.Visible = false;
        //lblAddUpdate.Text = "Update ";
        divrightbtn.Visible = false;
    }
    private void HidePanels()
    {
        PanAlreadExists.Visible = false;
        PanSuccess.Visible = false;
        PanError.Visible = false;
        //  PanNoRecord.Visible = false;
        PanAddUpdate.Visible = false;
    }
    public void Reset()
    {
        PanAddUpdate.Visible = true;
        txtformbayStreetNo.Text = string.Empty;
        txtformbaystreetname.Text = string.Empty;
        ddlformbaystreettype.ClearSelection();
        txtformbayUnitNo.Text = string.Empty;
        ddlformbayunittype.ClearSelection();
        txtInstallAddress.Text = string.Empty;
        txtInstallCity.Text = string.Empty;
        txtInstallerNotes.Text = string.Empty;
        txtInstallPostCode.Text = string.Empty;
        txtInstallState.Text = string.Empty;
        txtManualQuoteNumber.Text = string.Empty;
        txtMeterInstallerNotes.Text = string.Empty;
        //txtOldProjectNumber.Text = string.Empty;
        ddllinkprojectid.SelectedValue = string.Empty;
        txtProjectNotes.Text = string.Empty;
        txtProjectOpened.Text = DateTime.Now.AddHours(14).ToShortDateString();
        DataTable dtProjectType = ClstblProjectType.tblProjectType_SelectType();
        if (dtProjectType.Rows.Count > 0)
        {
            ddlProjectTypeID.SelectedValue = dtProjectType.Rows[0]["ProjectTypeID"].ToString();
        }
        DataTable dtContact = ClstblContacts.tblContacts_SelectTop(Request.QueryString["compid"]);
        if (dtContact.Rows.Count > 0)
        {
            ddlContact.SelectedValue = dtContact.Rows[0]["ContactID"].ToString();
        }
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        //ddlprojectmgr.SelectedValue = stEmp.EmployeeID;
        if (Roles.IsUserInRole("SalesRep"))
        {
            //ddlSalesRep.SelectedValue = stEmp.EmployeeID;
        }
        //divrightbtn.Visible = true;
    }
    protected void txtInstallCity_OnTextChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        string[] cityarr = txtInstallCity.Text.Split('|');
        if (cityarr.Length > 1)
        {
            txtInstallCity.Text = cityarr[0].Trim();
            txtInstallState.Text = cityarr[1].Trim();
            txtInstallPostCode.Text = cityarr[2].Trim();
        }
        else
        {
            txtInstallState.Text = string.Empty;
            txtInstallPostCode.Text = string.Empty;
        }
        //checkExistsAddress();
    }
    protected void ddlFind_SelectedIndexChanged(object sender, EventArgs e)
    {
        //PanAddUpdate.Visible = true;
        //if (ddlFind.SelectedValue != string.Empty)
        //{
        //    SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ddlFind.SelectedValue);
        //    SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(st.CustomerID);
        //    lblcompanyname.Text = stCust.Customer;
        //    lblcity.Text = st.InstallCity;
        //    lblstreet.Text = st.InstallAddress;
        //    lbladdress.Text = st.InstallAddress + "," + st.InstallCity + "," + st.InstallState;
        //    txtInstallAddress.Text = st.InstallAddress;
        //    txtInstallCity.Text = st.InstallCity;
        //    txtInstallState.Text = st.InstallState;
        //    txtInstallPostCode.Text = st.InstallPostCode;
        //}
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton gvbtnDelete = (LinkButton)e.Row.FindControl("gvbtnDelete");
            HiddenField hndProjectID = (HiddenField)e.Row.FindControl("hndProjectID");
            LinkButton hypStatus = (LinkButton)e.Row.FindControl("hypStatus");
            gvbtnDelete.Visible = false;
            if (Roles.IsUserInRole("Administrator"))
            {
                gvbtnDelete.Visible = true;
                if (Roles.IsUserInRole("SubAdministrator"))
                {
                    gvbtnDelete.Visible = false;
                }
            }
            HyperLink detail1 = (HyperLink)e.Row.FindControl("gvbtnDetail");
            HyperLink detail2 = (HyperLink)e.Row.FindControl("gvbtnDetail2");
            if (Roles.IsUserInRole("leadgen") || Roles.IsUserInRole("Lead Manager"))
            {
                detail1.Visible = false;
                detail2.Visible = true;
            }
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string EmployeeID = stEmp.EmployeeID;
            string SalesTeamID = stEmp.SalesTeamID;
            string EmpType = stEmp.EmpType;
            HiddenField hndEmployeeID = (HiddenField)e.Row.FindControl("hndEmployeeID");
            SttblEmployees stSalesRep = ClstblEmployees.tblEmployees_SelectByEmployeeID(hndEmployeeID.Value);
            DataTable dt = ClstblProjects.tblProjects_SelectByUCustomerID(Request.QueryString["compid"]);
            if (Roles.IsUserInRole("Sales Manager"))
            {
                if (dt.Rows.Count > 1)
                {
                    if (stSalesRep.EmpType == EmpType && stSalesRep.SalesTeamID == SalesTeamID.ToString())
                    {
                        gvbtnDelete.Visible = true;
                    }
                    else
                    {
                        gvbtnDelete.Visible = false;
                    }
                }
            }
            //DataTable dt = ClstblProjects.tblProjects_SelectTop1(Request.QueryString["compid"]);
            //if (dt.Rows.Count > 0)
            //{
            //    string ProjectID = dt.Rows[0]["ProjectID"].ToString();
            //    if (ProjectID == hndProjectID.Value)
            //    {
            //        gvbtnDelete.Visible = false;
            //    }
            //    else
            //    {
            //        gvbtnDelete.Visible = true;
            //    }
            //}
            //if (Roles.IsUserInRole("Sales Manager"))
            //{
            //    hypStatus.Visible = true;
            //}
            //else
            //{
            //    hypStatus.Visible = false;
            //}
        }
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToLower() == "status")
        {
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>alert('');</script>");
            hndStatusProjectID.Value = e.CommandArgument.ToString();
            ModalPopupExtenderStatus.Show();
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(e.CommandArgument.ToString());
            ListItem item4 = new ListItem();
            item4.Text = "Select";
            item4.Value = "";
            ddlProjectStatusID.Items.Clear();
            ddlProjectStatusID.Items.Add(item4);
            if (st2.ProjectStatusID == "2")
            {
                if ((Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep")))
                {
                    Response.Write("Hi1");
                    Response.End();
                    ddlProjectStatusID.DataSource = ClstblProjects.tblProjectStatus_StatusDSalesRep();  // 4,6
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
                else if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
                {
                    Response.Write("Hi2");
                    Response.End();
                    ddlProjectStatusID.DataSource = ClstblProjects.tblProjectStatus_StatusSalesRep();  // 2,4,6,7,9
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
                else if ((Roles.IsUserInRole("Maintenance")))
                {
                    Response.Write("Hi3");
                    Response.End();
                    ddlProjectStatusID.DataSource = ClstblProjects.tblProjectStatus_Mtce();  //  2,9
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
                else if ((Roles.IsUserInRole("Installation Manager")))
                {
                    Response.Write("Hi4");
                    Response.End();
                    ddlProjectStatusID.DataSource = ClstblProjects.tblProjectStatus_InstMgr();  //  6,9
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
                else if (Roles.IsUserInRole("Administrator"))
                {
                    ddlProjectStatusID.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();  // all
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
                else
                {
                    //ddlProjectStatusID.DataSource = ClstblProjects.tblProjectStatus_StatusSM();   // 2,3,4,6,7,9
                    //ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    //ddlProjectStatusID.DataTextField = "ProjectStatus";
                    //ddlProjectStatusID.DataMember = "ProjectStatus";
                    //ddlProjectStatusID.DataBind();
                }
            }
            else
            {
                if (Roles.IsUserInRole("Administrator"))
                {
                    ddlProjectStatusID.DataSource = ClstblProjectStatus.tblProjectStatus_SelectActive();  // all
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
            }
            if (st2.ProjectStatusID == "3" || st2.ProjectStatusID == "8")
            {
                if (Roles.IsUserInRole("Installation Manager") || Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
                {
                    ddlProjectStatusID.DataSource = ClstblProjects.tblProjectStatus_StatusDSalesRep();  //  4,6
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
            }
            if (st2.ProjectStatusID == "4")
            {
                if ((Roles.IsUserInRole("Installation Manager")))
                {
                    Response.Write("Hiiii");
                    Response.End();
                    ddlProjectStatusID.DataSource = ClstblProjects.tblProjectStatus_StatusInstMgr();  // 3,6,8
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
            }
            if (st2.ProjectStatusID == "20")
            {
                if ((Roles.IsUserInRole("Installation Manager")))
                {
                    ddlProjectStatusID.DataSource = ClstblProjects.tblProjectStatus_StatusInstallMgr();  //6
                    ddlProjectStatusID.DataValueField = "ProjectStatusID";
                    ddlProjectStatusID.DataTextField = "ProjectStatus";
                    ddlProjectStatusID.DataMember = "ProjectStatus";
                    ddlProjectStatusID.DataBind();
                }
            }
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string CEmpType = stEmpC.EmpType;
            string CSalesTeamID = stEmpC.SalesTeamID;
            //if (Profile.eurosolar.projectid != string.Empty)
            //{
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st2.EmployeeID);
            string EmpType = stEmp.EmpType;
            string SalesTeamID = stEmp.SalesTeamID;
            if (CEmpType == EmpType && CSalesTeamID == SalesTeamID)
            {
                hndStatusProjectID.Value = e.CommandArgument.ToString();
                ModalPopupExtenderStatus.Show();
            }
            else
            {
            }
            //}
        }
        BindGrid(0);
    }
    protected void ibtnUpdateStatus_Onclick(object sender, EventArgs e)
    {
        string ProjectID = hndStatusProjectID.Value;
        string ProjectStatusID = ddlProjectStatusID.SelectedValue;
        string ProjectCancelID = ddlProjectCancelID.SelectedValue;
        string ProjectOnHoldID = ddlProjectOnHoldID.SelectedValue;
        string StatusComment = txtComment.Text;
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmpID = stEmp.EmployeeID;
        bool suc = ClstblProjects.tblProjects_UpdateProjectStatus(ProjectID, ProjectStatusID, ProjectCancelID, ProjectOnHoldID, StatusComment);
        int sucTrack = ClstblProjects.tblTrackProjStatus_Insert(ProjectStatusID, ProjectID, EmpID, st.NumberPanels);
        if (ProjectStatusID == "6")
        {
            SttblProjects stpro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            ClstblContacts.tblContacts_UpdateContLeadStatus("4", stpro.ContactID);
        }
        if (suc)
        {
            ModalPopupExtenderStatus.Hide();
            txtComment.Text = string.Empty;
            ddlProjectStatusID.SelectedValue = "";
            ddlProjectOnHoldID.SelectedValue = "";
            ddlProjectCancelID.SelectedValue = "";
            trCancel.Visible = false;
            trComment.Visible = false;
            trHold.Visible = false;
        }
        BindGrid(0);
        divprojecttab.Visible = false;
        PanAddUpdate.Visible = false;
        lnkAdd.Visible = true;
    }
    protected void ibtnCancelStatus_Onclick(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = false;
        ModalPopupExtenderStatus.Hide();
        ddlProjectStatusID.SelectedValue = "";
    }
    protected void ddlProjectStatusID_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = false;
        ModalPopupExtenderStatus.Show();
        if (ddlProjectStatusID.SelectedValue != string.Empty)
        {
            if (ddlProjectStatusID.SelectedValue == "4")
            {
                trHold.Visible = true;
                ListItem item7 = new ListItem();
                item7.Text = "Select";
                item7.Value = "";
                ddlProjectOnHoldID.Items.Clear();
                ddlProjectOnHoldID.Items.Add(item7);
                ddlProjectOnHoldID.DataSource = ClstblProjectOnHold.tblProjectOnHold_Select();
                ddlProjectOnHoldID.DataValueField = "ProjectOnHoldID";
                ddlProjectOnHoldID.DataTextField = "ProjectOnHold";
                ddlProjectOnHoldID.DataMember = "ProjectOnHold";
                ddlProjectOnHoldID.DataBind();
            }
            else
            {
                trHold.Visible = false;
            }
            if (ddlProjectStatusID.SelectedValue == "6")
            {
                trCancel.Visible = true;
                ListItem item8 = new ListItem();
                item8.Text = "Select";
                item8.Value = "";
                ddlProjectCancelID.Items.Clear();
                ddlProjectCancelID.Items.Add(item8);
                ddlProjectCancelID.DataSource = ClstblProjectCancel.tblProjectCancel_SelectASC();
                ddlProjectCancelID.DataValueField = "ProjectCancelID";
                ddlProjectCancelID.DataTextField = "ProjectCancel";
                ddlProjectCancelID.DataMember = "ProjectCancel";
                ddlProjectCancelID.DataBind();
            }
            else
            {
                trCancel.Visible = false;
            }
            if (ddlProjectStatusID.SelectedValue == "6" || ddlProjectStatusID.SelectedValue == "4")
            {
                trComment.Visible = true;
            }
            else
            {
                trComment.Visible = false;
                trHold.Visible = false;
                trCancel.Visible = false;
            }
        }
        else
        {
            trComment.Visible = false;
            trHold.Visible = false;
            trCancel.Visible = false;
        }
    }
    protected void TabContainer1_ActiveTabChanged(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(panprojects,
                                            this.GetType(),
                                            "MyAction",
                                            "doMyAction();",
                                            true);

        if (TabContainer1.ActiveTabIndex == 0)
        {
            projectdetail1.BindProjectDetail();
            if ((Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Sales Manager")))
            {

                TabForms.Visible = false;
                TabPreInst.Visible = false;
                TabPostInst.Visible = false;
                TabElecInv.Visible = false;
                TabSTC.Visible = false;
                TabMtce.Visible = false;
                TabDocs.Visible = false;
                projectmaintenance1.Visible = false;

            }
            if (Roles.IsUserInRole("Installation Manager"))
            {
                TabDocs.Visible = false;
            }
            if (Roles.IsUserInRole("PreInstaller"))
            {
                TabPostInst.Visible = false;
                TabElecInv.Visible = false;
            }
        }
        else if (TabContainer1.ActiveTabIndex == 1)
        {
            projectsale1.BindProjectSale();
            if (Roles.IsUserInRole("SalesManager"))
            {
                projectmaintenance1.Visible = false;
            }
            if ((Session["openMaintainace"].ToString()) == "open")
            {
                TabMtce.Visible = false;
                projectmaintenance1.Visible = true;
                projectsale1.Visible = false;


                projectmaintenance1.Bindmaintainance();
            }
            else
            {
                TabMtce.Visible = false;
                projectmaintenance1.Visible = false;
                projectsale1.Visible = true;
                projectsale1.BindProjectSale();
            }


            //projectsale1.BindProjectSale();
        }
        else if (TabContainer1.ActiveTabIndex == 2)
        {
            projectprice1.BindProjectPrice();
        }
        else if (TabContainer1.ActiveTabIndex == 3)
        {
            projectquote1.BindProjectQuote();
        }
        else if (TabContainer1.ActiveTabIndex == 4)
        {
            projectfinance1.BindProjectFinance();
        }
        else if (TabContainer1.ActiveTabIndex == 5)
        {
            projectforms1.BindProjectForms();
        }
        else if (TabContainer1.ActiveTabIndex == 6)
        {

            projectstc1.BindProjectSTC();
        }
        else if (TabContainer1.ActiveTabIndex == 7)
        {
            //Response.Redirect(Request.Url.PathAndQuery);
            //projectpreinst1.BindProjectPreInst();
            projectpreinst1.GetPreInstClick();
        }
        else if (TabContainer1.ActiveTabIndex == 8)
        {
            projectpostinst1.BindProjectPostInst();
        }
        else if (TabContainer1.ActiveTabIndex == 9)
        {

            projectelecinv1.BindProjectElecInv();
        }
        else if (TabContainer1.ActiveTabIndex == 10)
        {
            //projectdocs1.BindDocs();
            if (Roles.IsUserInRole("PreInstaller"))
            {
                TabContainer1.ActiveTabIndex = 8;
                projectmtce1.Visible = true;
                //  projectdocs1.BindDocs();
            }
            projectmtce1.BindProjectMtce();
        }
        else if (TabContainer1.ActiveTabIndex == 11)
        {
            if (Roles.IsUserInRole("PreInstaller"))
            {
                TabContainer1.ActiveTabIndex = 9;
                projectdocs1.Visible = true;
            }
            projectdocs1.BindDocs();
        }
        else if (TabContainer1.ActiveTabIndex == 12)
        {
            if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Sales Manager"))
            {
                TabContainer1.ActiveTabIndex = 5;

            }
            if (Roles.IsUserInRole("Installation Manager"))
            {
                TabContainer1.ActiveTabIndex = 12;
                projectrefund1.Visible = true;
            }
            if (Roles.IsUserInRole("PreInstaller"))
            {
                TabContainer1.ActiveTabIndex = 10;
                projectmtce1.Visible = false;
            }
            if (Roles.IsUserInRole("Maintenance"))
            {
                TabContainer1.ActiveTabIndex = 11;
                projectrefund1.Visible = true;
            }
            if (Roles.IsUserInRole("Accountant"))
            {
                TabContainer1.ActiveTabIndex = 11;
                projectrefund1.Visible = true;
            }
            projectrefund1.BindProjectRefund();
        }
        else if (TabContainer1.ActiveTabIndex == 13)
        {
            if (Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("Sales Manager"))
            {
                TabContainer1.ActiveTabIndex = 6;
            }
            if (Roles.IsUserInRole("Installation Manager"))
            {
                TabContainer1.ActiveTabIndex = 12;
                projectrefund1.Visible = false;
            }
            if (Roles.IsUserInRole("PreInstaller"))
            {
                TabContainer1.ActiveTabIndex = 11;
                projectdocs1.Visible = false;
                //projectvicdoc1.BindVicDoc();
                //this.TabContainer1.Tabs[13].Visible = true;
            }
            //projectvicdoc1.BindProjectSale();
            //TabVicDoc.Visible = true;
            //projectvicdoc1.BindVicDoc();
        }
    }
    protected void btnCheckActive_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
        if (st.ElecRetailer != string.Empty)
        {
            lblEcRetailer.Text = "<span class='width60px'>Ec Retailer</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
        }
        else
        {
            lblEcRetailer.Text= "<span class='width60px'>Ec Retailer </span>:&nbsp;<i class='fa fa-close redclr'></i>";
        }
        if (st.MeterNumber1 != string.Empty)
        {
            lblMeterNo.Text = "<span class='width60px'>Meter Number</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
        }
        else
        {
            lblMeterNo.Text = "<span class='width60px'>Meter Number </span>:&nbsp;<i class='fa fa-close redclr'></i>";

        }
        if (st.SignedQuote != "False")
        {
            lblSignedQuote.Text = "<span class='width60px'>Signed Quote</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
        }
        else
        {
            lblSignedQuote.Text = "<span class='width60px'>Signed Quote </span>:&nbsp;<i class='fa fa-close redclr'></i>";
        }
        if (st.ElecDistributorID == string.Empty)
        {
            lblEleDest.Text = "Please select Elec Distributor.";
        }
        else
        {
            lblEleDest.Visible = false;
        }
        if (st.ElecDistributorID == "12")
        {
            if (st.ElecDistApprovelRef != string.Empty)
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref </span>:&nbsp; <i class='fa fa-close redclr'></i>";
            }
            if (st.NMINumber != string.Empty)
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if (st.meterupgrade != string.Empty)
            {
                lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
            {
                lblDocumentVerified.Visible = false;
                DocumentVerified.Visible = false;
            }
            else
            {
                lblDocumentVerified.Visible = true;
                DocumentVerified.Visible = true;
                if (st.DocumentVerified == "True")
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
                }
                else
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-close redclr'></i>";
                }
            }
            lblmeterupgrade.Visible = true;
            meterupgrade.Visible = true;
            lblApprovalRef.Visible = true;
            approveref.Visible = true;
            lblNMINumber.Visible = true;
            NMINumber.Visible = true;
        }
        else if (st.ElecDistributorID == "13")
        {
            if (st.RegPlanNo != string.Empty)
            {
                lblRegPlanNo.Text = "<span class='width60px'>Reg Plan No</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblRegPlanNo.Text = "<span class='width60px'>Reg Plan No</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if (st.LotNumber != string.Empty)
            {
                lblLotNum.Text = "<span class='width60px'>LotNumber</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblLotNum.Text = "<span class='width60px'>LotNumber</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if (st.ElecDistApprovelRef != string.Empty)
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if (st.meterupgrade != string.Empty)
            {
                lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if (st.NMINumber != string.Empty)
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
            {
                lblDocumentVerified.Visible = false;
                DocumentVerified.Visible = false;
            }
            
            else
            {
                lblDocumentVerified.Visible = true;
                DocumentVerified.Visible = true;
                if (st.DocumentVerified == "True")
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
                }
                else
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-close redclr'></i>";
                }
            }
            lblRegPlanNo.Visible = true;
            RegPlanNo.Visible = true;
            lblLotNum.Visible = true;
            LotNum.Visible = true;
            lblApprovalRef.Visible = true;
            approveref.Visible = true;
            lblmeterupgrade.Visible = true;
            meterupgrade.Visible = true;
            lblNMINumber.Visible = true;
            NMINumber.Visible = true;
        }
        else
        {
            if (st.ElecDistApprovelRef != string.Empty)
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblApprovalRef.Text = "<span class='width60px'>Approval Ref</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if (st.meterupgrade != string.Empty)
            {
                lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblmeterupgrade.Text = "<span class='width60px'>Meter Upgrade</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if (st.NMINumber != string.Empty)
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblNMINumber.Text = "<span class='width60px'>NMI Number</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
            if ((st.FinanceWithID == "1" || st.FinanceWithID == string.Empty))
            {
                lblDocumentVerified.Visible = false;
                DocumentVerified.Visible = false;
            }
            else
            {
                lblDocumentVerified.Visible = true;
                DocumentVerified.Visible = true;
                if (st.DocumentVerified == "True")
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
                }
                else
                {
                    lblDocumentVerified.Text = "<span class='width60px'>Document Verification</span>:&nbsp;<i class='fa fa-close redclr'></i>";
                }
            }
            lblRegPlanNo.Visible = false;
            RegPlanNo.Visible = false;
            lblLotNum.Visible = false;
            LotNum.Visible = false;
            lblApprovalRef.Visible = true;
            approveref.Visible = true;
            lblmeterupgrade.Visible = true;
            meterupgrade.Visible = true;
            lblNMINumber.Visible = true;
            NMINumber.Visible = true;
        }
        //else
        //{
        //    lblRegPlanNo.Visible = false;
        //    lblLotNum.Visible = false;
        //    lblApprovalRef.Visible = false;
        //    lblNMINumber.Visible = false;
        //}
        if (st.DepositReceived != string.Empty)
        {
            lblDepositeRec.Text = "<span class='width60px'>Deposit Rec</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
        }
        else
        {
            lblDepositeRec.Text = "<span class='width60px'>Deposit Rec</span>:&nbsp;<i class='fa fa-close redclr'></i>";
        }
        if (st.QuoteAccepted != string.Empty)
        {
            lblQuoteAccepted.Text = "<span class='width60px'>Quote Accepted</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
        }
        else
        {
            lblQuoteAccepted.Text = "<span class='width60px'>Quote Accepted</span>:&nbsp;<i class='fa fa-close redclr'></i>";
        }
        if(st.InstallState== "VIC")
        {
            VicAppRefference.Visible = true;
            lblVicAppRefference.Visible = true;
            if (!string.IsNullOrEmpty(st.VicAppReference))
            {
                lblVicAppRefference.Text = "<span class='width60px'>Vic Rebate App Ref</span>:&nbsp;<i class='fa fa-check greenclr'></i>";
            }
            else
            {
                lblVicAppRefference.Text = "<span class='width60px'>Vic Rebate App Ref</span>:&nbsp;<i class='fa fa-close redclr'></i>";
            }
        }
        else
        {
            VicAppRefference.Visible = false;
            lblVicAppRefference.Visible = false;
        }
      
        //if (st.SQ != "" && st.SignedQuote != "False" && st.DepositReceived != "" && st.RegPlanNo != "" && st.LotNumber != "" && st.ElecDistApprovelRef != "" && st.NMINumber != "")
        if (st.ProjectStatusID == "3" || st.ProjectStatusID == "5")
        {
            lblActive.Text = "Project is Active";
        }
        else
        {
            lblActive.Text = "Project is not Active";
        }

        ModalPopupExtender2.Show();
        projectquote1.BindProjectQuote();
    }
    protected void ibtnCancelActive_Onclick(object sender, EventArgs e)
    {
        ModalPopupExtender2.Hide();
        projectquote1.BindProjectQuote();
    }
    protected void ddlProjectTypeID_SelectedIndexChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        ListItem item2 = new ListItem();
        item2.Text = "Select";
        item2.Value = "";
        ddllinkprojectid.Items.Clear();
        ddllinkprojectid.Items.Add(item2);
        if (!string.IsNullOrEmpty(Request.QueryString["compid"]))
        {
            ddllinkprojectid.Visible = false;
            divoldproject.Visible = false;
            //if (ddlProjectTypeID.SelectedValue == "8")
            //{
            //    ddllinkprojectid.Visible = true;
            //    ddllinkprojectid.DataSource = ClstblProjects.tblProjects_GetProjectStatusComplete(Request.QueryString["compid"]);
            //    ddllinkprojectid.DataValueField = "ProjectID";
            //    ddllinkprojectid.DataTextField = "ProjectNumber";
            //    ddllinkprojectid.DataMember = "ProjectNumber";
            //    ddllinkprojectid.DataBind();
            //    RequiredFieldValidator8.Visible = true;
            //}
            //else
            //{
            //    ddllinkprojectid.Visible = false;
            //    RequiredFieldValidator8.Visible = false;
            //}
        }
    }
    protected void txtformbayUnitNo_TextChanged(object sender, EventArgs e)
    {
        streetaddress();
        //checkExistsAddress();
        txtformbayUnitNo.Focus();
    }
    protected void ddlformbayunittype_SelectedIndexChanged(object sender, EventArgs e)
    {
        streetaddress();
        //checkExistsAddress();
        ddlformbayunittype.Focus();
    }
    protected void txtformbayStreetNo_TextChanged(object sender, EventArgs e)
    {
        streetaddress();
        //checkExistsAddress();
        txtformbayStreetNo.Focus();
    }
    protected void txtformbaystreetname_TextChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction", "address();", true);
        Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>doMyAction();</script>");
        streetaddress();
        //checkExistsAddress();
        txtformbaystreetname.Focus();
        // ScriptManager.RegisterStartupScript(updatepanelgrid, this.GetType(), "MyAction1", "funddlformbaystreettypefocus();", true);
    }
    protected void ddlformbaystreettype_SelectedIndexChanged(object sender, EventArgs e)
    {
        streetaddress();
        //checkExistsAddress();
        ddlformbaystreettype.Focus();
    }
    public void streetaddress()
    {
        address = ddlformbayunittype.SelectedValue + " " + txtformbayUnitNo.Text + " " + txtformbayStreetNo.Text + " " + txtformbaystreetname.Text + " " + ddlformbaystreettype.SelectedValue;
        txtInstallAddress.Text = address;
    }
    public void checkExistsAddress()
    {
        string StreetCity = txtInstallCity.Text;
        string StreetState = txtInstallState.Text;
        string StreetPostCode = txtInstallPostCode.Text;
        string StreetAddress = txtInstallAddress.Text;
        int exist = ClstblCustomers.tblCustomers_Exits_Address(hndunittype.Value, hndunitno.Value, hndstreetno.Value, hndstreetname.Value, hndstreettype.Value, StreetCity, StreetState, StreetPostCode);
        DataTable dt1 = ClstblCustomers.tblCustomers_Exits_Select_StreetAddress(StreetAddress, StreetCity, StreetState, StreetPostCode);
        if (dt1.Rows.Count > 0)
        {
            //lbleror.Visible = true;
        }
        else
        {
            //lbleror.Visible = false;
        }
    }
    protected void ibtnupdate_Click(object sender, EventArgs e)
    {
    }
    protected void btnCancleAddress_Click(object sender, EventArgs e)
    {
    }
    protected void btnCancle_Click(object sender, EventArgs e)
    {
    }
    protected void btnYesAddress_Click(object sender, EventArgs e)
    {
        string companyId = Request.QueryString["compid"];
        Response.Redirect("/admin/adminfiles/company/company.aspx?companyid=" + companyId);
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.ToString();
        bool sucess1 = ClstblProjects.tblProjects_Delete(id);
        bool suc2 = ClstblProjects.tbl_StockPickUpDetail_DeleteByProjectID(id);
        bool suc3 = ClstblProjects.tbl_StockPickUpReturnDetail_DeleteByProjectID(id);
        bool suc4=ClstblProjects.tblStockUpdate_DeleteByProjectID(id);
        bool suc5= ClstblProjects.tbl_PickListLog_DeleteByProjectID(id);
        bool suc6= ClstblProjects.tblPickUpChangeReason_DeleteByProjectID(id);
        DataTable dt = ClstblProjects.tblProjects_SelectByUCustomerID(Request.QueryString["compid"]);
        if (dt.Rows.Count == 0)
        {
            ClstblCustomers.tblCustomers_UpdateCustType("1", Request.QueryString["compid"]);
            ClstblContacts.tblContacts_UpdateContLeadStatusByCustID("2", Request.QueryString["compid"]);
        }
        //--- do not chage this code start------
        if (sucess1)
        {
            SetAdd1();
            //PanSuccess.Visible = true;
        }
        else
        {
            SetError1();
        }
        Reset();
        divrightbtn.Visible = true;
        PanAddUpdate.Visible = false;
        GridView1.EditIndex = -1;
        BindGrid(0);
        divprojecttab.Visible = false;
        BindProjects();
        BindGrid(1);

    }
}