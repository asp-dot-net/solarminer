<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="contactinfo.ascx.cs" Inherits="includes_controls_contactinfo" %>


<script type="text/javascript">
    $(document).ready(function () {
        HighlightControlToValidate();
        $('#<%=btnUpdateDetail.ClientID %>').click(function () {
            formValidate();
        });
        loader();
    });
    function loader() {
        $(window).load(function () {
        });
    }
    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#b94a48");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                }
            }
        }
    }
    function HighlightControlToValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                $('#' + Page_Validators[i].controltovalidate).blur(function () {
                    var validatorctrl = getValidatorUsingControl($(This).attr("ID"));
                    if (validatorctrl != null && !validatorctrl.isvalid) {
                        $(This).css("border-color", "#b94a48");
                    }
                    else {
                        $(This).css("border-color", "#B5B5B5");
                    }
                });
            }
        }
    }
    function getValidatorUsingControl(controltovalidate) {
        var length = Page_Validators.length;
        for (var j = 0; j < length; j++) {
            if (Page_Validators[j].controltovalidate == controltovalidate) {
                return Page_Validators[j];
            }
        }
        return null;
    }
</script>
<section class="row m-b-md" runat="server" id="update11">
    <div class="col-sm-12">
        <div class="contactsarea">
            <div class="messesgarea">
                <div class="alert alert-success" id="PanSuccess" runat="server" visible="false">
                    <i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong>
                </div>
                <div class="alert alert-danger" id="PanError" runat="server" visible="false">
                    <i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong>
                </div>
                <div class="alert alert-danger" id="PanAlreadExists" runat="server" visible="false">
                    <i class="icon-remove-sign"></i><strong>&nbsp;Record with this name already exists.</strong>
                </div>
            </div>
            <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                <div class="row">
                    <div class="col-md-6">
                        <asp:Panel ID="Panel1" runat="server">
                          <div class="form-group textareaboxheight">
                            <span class="name">
                                <label class="control-label">
                                    Notes
                                </label>
                            </span><span>
                                <asp:TextBox ID="txtContNotes" runat="server" TextMode="MultiLine"
                                    CssClass="form-control"></asp:TextBox></span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="name">
                                <label class="control-label">
                                    Entered
                                </label>
                            </span><span>
                                <asp:TextBox ID="txtContactEntered" runat="server" Enabled="false" CssClass="form-control"
                                   ></asp:TextBox>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="name">
                                <label class="control-label">
                                    By
                                </label>
                            </span><span>
                                <asp:TextBox ID="txtContactEnteredBy" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="name">
                                <label class="control-label">
                                    ContactID
                                </label>
                            </span><span>
                                <asp:TextBox ID="txtContactID" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="name">
                                <label class="control-label">
                                    Ref BSB
                                </label>
                            </span><span>
                                <asp:TextBox ID="txtRefBSB" runat="server" MaxLength="12" CssClass="form-control"></asp:TextBox>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="name">
                                <label class="control-label">
                                    Ref Account
                                </label>
                            </span><span>
                                <asp:TextBox ID="txtRefAccount" runat="server"  MaxLength="30" CssClass="form-control"></asp:TextBox>
                            </span>
                            <div class="clear">
                            </div>
                        </div>
                        </asp:Panel>
                      
                    </div>
                    <div class="col-md-6" id="divInst" runat="server" visible="false">
                        <asp:Panel ID="Panel3" runat="server">
                          <div class="form-group">
                            <span class="name">
                                <label class="control-label">
                                    Elec Licence
                                </label>
                            </span><span>
                                <asp:TextBox ID="txtElecLicence" runat="server"  MaxLength="20" CssClass="form-control"></asp:TextBox>
                            </span>
                            <div class="clear">
                            </div>
                        </div>


                             <div class="form-group">
                            <span class="name">
                                <label class="control-label">
                                    Accreditation
                                </label>
                            </span><span>
                                <asp:TextBox ID="txtAccreditation" runat="server" MaxLength="20" CssClass="form-control"></asp:TextBox>
                            </span>
                            <div class="clear">
                            </div>
                        </div>

                            <div class="form-group checkboxarea">
                            <span class="name">
                                <label class="control-label">
                                    &nbsp;
                                </label>
                            </span><span class="spicaldivarea contactcheckbox cntcheckbox2">
                                <div>
                                    <div class="row">                                                    
                                        <div class="col-md-4 checkbox-info checkbox">
                                            <span class="fistname">
                                               <label for="<%=chkDocStoreDone.ClientID %>">
                                                    <asp:CheckBox ID="chkDocStoreDone" runat="server" AutoPostBack="true" OnCheckedChanged="chkDocStoreDone_CheckedChanged" />
                                                    <span class="text">   Stored&nbsp;</span>
                                                </label>
                                                <asp:HyperLink ID="lblAL" runat="server" Target="_blank" Visible="false"></asp:HyperLink>                                        
                                            </span>
                                        </div>                                       
                                    </div>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>



                           
                        <div class="form-group " id="divAL" runat="server" visible="false">
                            <span class="name">
                                <label class="control-label">
                                </label>
                            </span><span class="dateimg">
                                <asp:FileUpload ID="fuAL" runat="server" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorAL" runat="server" ErrorMessage="This value is required."
                                    ValidationGroup="contactinfo" SetFocusOnError="true" ControlToValidate="fuAL"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                            </span>
                            <div class="clear">
                            </div>
                        </div>


                        <div class="formainline">
                             <span class="name">
                                <label class="control-label">
                                    Lic Expires
                                </label>
                            </span>   
                         
                                <div class="input-group date datetimepicker1">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                    <asp:TextBox ID="txtElecLicenceExpires" runat="server" class="form-control" placeholder="Expires Date">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                        ControlToValidate="txtElecLicenceExpires" Display="Dynamic" ValidationGroup="instcal"></asp:RequiredFieldValidator>

                            </div>
                            <div class="clear"></div>
                       </div>
                            

                        </asp:Panel>
                      
                             <div class="form-group checkboxarea">
                            <span class="name">
                                <label class="control-label">
                                    &nbsp;
                                </label>
                            </span><span class="spicaldivarea contactcheckbox cntcheckbox2">
                                <div>
                                    <div class="row">
                                        <div class="checkbox">
                                        <div class="col-md-4 checkbox-info ">
                                            <span class="mrdiv">
                                                 <label for="<%=chkInstaller.ClientID %>">
                                                   <asp:CheckBox ID="chkInstaller" runat="server" />
                                                        <span class="text">   Installer&nbsp;</span>
                                                    </label>
                                            </span>
                                        </div>
                                            </div>
                                        <div class="col-md-4 checkbox-info checkbox">
                                            <span class="fistname">
                                               <label for="<%=chkDesigner.ClientID %>">
                                               <asp:CheckBox ID="chkDesigner" runat="server" />
                                                    <span class="text">  Designer&nbsp;</span>
                                                </label>
                                            </span>
                                        </div>
                                        <div class="col-md-4 checkbox-info checkbox">              
                                            
                                                <span class="lastname" style="width: 41%;">                                                
                                                     <label for="<%=chkElectrician.ClientID %>">
                                                       <asp:CheckBox ID="chkElectrician" runat="server" Checked="true" />
                                                            <span class="text"> Electrician&nbsp;</span>
                                                        </label>
                                                </span>                                           
                                        </div>                                        
                                    </div>

                            </div>
                                </span>
                                </div>
                       
                         

                            <div class="form-group checkboxarea">
                            <span class="name">
                                <label class="control-label">
                                    &nbsp;
                                </label>
                            </span><span class="spicaldivarea contactcheckbox cntcheckbox2">
                                <div>
                                    <div class="row">                                                    
                                       <div class="col-md-4 checkbox-info checkbox">
                                            <span class="fistname">
                                               <label for="<%=chkMeterElectrician.ClientID %>">
                                               <asp:CheckBox ID="chkMeterElectrician" runat="server" />
                                                    <span class="text">  MeterElectrician&nbsp;</span>
                                                </label>
                                            </span>
                                        </div>                                  
                                    </div>
                                </div>
                            </span>
                            <div class="clear">
                            </div>
                        </div>

                    </div>
                </div>
                <div class="row" id="divAddUpdate" runat="server">
                    <div class="col-md-12 textcenterbutton center-text">
                        <div>
                            <span class="name">&nbsp;</span><span>
                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdateDetail" runat="server" OnClick="btnUpdateDetail_Click"
                                    Text="Save" CausesValidation="true" ValidationGroup="contactinfo" />
                            </span>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</section>
