<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectdocs.ascx.cs" Inherits="includes_controls_projectdocs" %>
<%@ Register Src="../InvoicePayments.ascx" TagName="InvoicePayments" TagPrefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<script>
    //var prm = Sys.WebForms.PageRequestManager.getInstance();
    //prm.add_endRequest(endrequesthandler);
    //function endrequesthandler(sender, args) {
    //    $('input[type=file]').bootstrapFileInput();
    //}
</script>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <section class="row m-b-md">
            <div class="col-sm-12 minhegihtarea">
                <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                    <div class="contactsarea">
                        <div class="salepage">
                            <div class="row">
                                <div class="col-md-12">
                                    <div>
                                        <div class="widget flat radius-bordered borderone">
                                            <div class="widget-header bordered-bottom bordered-blue">
                                                <span class="widget-caption">Download Documents</span>
                                            </div>
                                            <div class="widget-body">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group" id="divInvoice" runat="server">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Invoice
                                                                </label>
                                                            </span><span class="dateimg" style="height: 40px; display: block;">

                                                                <asp:ImageButton ID="btnOpenInvoice" runat="server" OnClick="btnOpenInvoice_Click"
                                                                    ImageUrl="../../images/btn_openinvoice.png" CausesValidation="false" />
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group" id="divQuote" runat="server">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Quotation
                                                                </label>
                                                            </span><span class="dateimg" style="height: 40px; display: block;">
                                                                <asp:HyperLink ID="hypDoc" runat="server" Target="_blank">
                                                                    <asp:Image ID="imgDoc" runat="server" ImageUrl="~/admin/images/btn_quotation.png" />
                                                                </asp:HyperLink>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Meter Photo 
                                                                </label>
                                                            </span><span class="dateimg" style="height: 40px; display: block;">

                                                                <asp:HyperLink ID="hypMP" runat="server" Target="_blank">

                                                                    <asp:Image ID="Imgdownload4444" runat="server" ImageUrl="../../admin/images/btn_download.png" />
                                                                </asp:HyperLink>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Electricity Bill 
                                                                </label>
                                                            </span><span class="dateimg" style="height: 40px; display: block;">

                                                                <asp:HyperLink ID="hypEB" runat="server" Target="_blank">

                                                                    <asp:Image ID="Image5" runat="server" ImageUrl="../../admin/images/btn_download.png" />
                                                                </asp:HyperLink>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Payment Receipt
                                                                </label>
                                                            </span><span class="dateimg" style="height: 40px; display: block;">

                                                                <asp:HyperLink ID="hypPR" runat="server" Target="_blank">

                                                                    <asp:Image ID="Image3" runat="server" ImageUrl="../../admin/images/btn_download.png" />
                                                                </asp:HyperLink>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                 

                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Prop Design 
                                                                </label>
                                                            </span>

                                                            <span class="dateimg" style="height: 40px; display: block;">
                                                                <asp:HyperLink ID="hypProp" runat="server" Target="_blank">
                                                                    <asp:Image ID="Image1" runat="server" ImageUrl="../../admin/images/btn_download.png" />
                                                                </asp:HyperLink>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    STC From
                                                                </label>
                                                            </span><span class="dateimg">

                                                                <asp:ImageButton ID="btnCreateSTCForm" runat="server" ImageUrl="../../admin/images/btn_create_stc_form.png"
                                                                    CausesValidation="false" OnClick="btnCreateSTCForm_Click" />
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Pick List
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <asp:ImageButton ID="btnPickList" runat="server" OnClick="btnPickList_Click"
                                                                    Text="PickList" CausesValidation="false"
                                                                    ImageUrl="../../admin/images/btn_picklist.png" />

                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>

                                                          <div class="form-group ">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Beat Quote
                                                                </label>
                                                            </span><span class="dateimg" style="height: 40px; display: block;">

                                                                <asp:HyperLink ID="hypbeatQuote" runat="server" Target="_blank">

                                                                    <asp:Image ID="Image4" runat="server" ImageUrl="../../admin/images/btn_download.png" />
                                                                </asp:HyperLink>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>

                                                          <div class="form-group ">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Near Map Photo
                                                                </label>
                                                            </span><span class="dateimg" style="height: 40px; display: block;">

                                                                <asp:HyperLink ID="hypNearmap" runat="server" Target="_blank">

                                                                    <asp:Image ID="Image6" runat="server" ImageUrl="../../admin/images/btn_download.png" />
                                                                </asp:HyperLink>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    SQ Document 
                                                                </label>
                                                            </span>

                                                            <span class="dateimg" style="height: 40px; display: block;">
                                                                <asp:HyperLink ID="HypSQ" runat="server" Target="_blank">
                                                                    <asp:Image ID="Image2" runat="server" ImageUrl="../../admin/images/btn_download.png" />
                                                                </asp:HyperLink>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Customer Acknowledgement
                                                                </label>
                                                            </span><span class="dateimg">

                                                                <asp:ImageButton ID="imgCustAck" runat="server" OnClick="imgCustAck_Click" CausesValidation="false"
                                                                    ImageUrl="../../admin/images/btn_Ack.png" />
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Installer Notes
                                                                </label>
                                                            </span><span class="dateimg valuebox wdth230">
                                                                <asp:Label ID="lblInstallerNotes" runat="server" />
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12" id="divInstallerDocs" runat="server">
                                                        <div class="form-group ">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    <b>AusNet IES Form: </b>
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/default.aspx" Target="_blank">
                                                            <asp:Image runat="server" ImageUrl="~/images/icon_dwn.jpg" /></asp:HyperLink>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    <b>PV Form: </b>
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <asp:HyperLink ID="HyperLink2" runat="server" Target="_blank">
                                                            <asp:Image runat="server" ImageUrl="~/images/icon_dwn.jpg" /></asp:HyperLink>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    <b>AusNet Alteration or Upgrade of Inverter Energy System Form: </b>
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <asp:HyperLink ID="HyperLink3" runat="server" Target="_blank">
                                                            <asp:Image runat="server" ImageUrl="~/images/icon_dwn.jpg" /></asp:HyperLink>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    <b>UE - Upgrade Existing Solar PV_SCF UE Form: </b>
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <asp:HyperLink ID="HyperLink4" runat="server" Target="_blank">
                                                            <asp:Image runat="server" ImageUrl="~/images/icon_dwn.jpg" /></asp:HyperLink>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group ">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    <b>EWR Latest: </b>
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <asp:HyperLink ID="HyperLink5" runat="server" Target="_blank">
                                                            <asp:Image runat="server" ImageUrl="~/images/icon_dwn.jpg" /></asp:HyperLink>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div>
                                        <div class="widget flat radius-bordered borderone">
                                            <div class="widget-header bordered-bottom bordered-blue">
                                                <span class="widget-caption">Upload Documents</span>
                                            </div>
                                            <div class="widget-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <%-- <input type="file" title="Search for a file to add" class="btn-primary btn">   --%>
                                                        <div class="form-group dateimgarea fileuploadmain">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Signed Paperwork
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <asp:HyperLink ID="lblST" runat="server" Target="_blank" Visible="false"></asp:HyperLink>

                                                                <span class="file-input btn btn-azure btn-file">
                                                                    <asp:FileUpload ID="fuST" runat="server" />
                                                                </span>
                                                            </span>


                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group dateimgarea fileuploadmain" id="divcompliance" runat="server" visible="false">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Compliance Certi
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <asp:HyperLink ID="lblCE" runat="server" Target="_blank" Visible="false"></asp:HyperLink>

                                                                <span class="file-input btn btn-azure btn-file">
                                                                    <asp:FileUpload ID="fuCE" runat="server" />
                                                                </span>

                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group dateimgarea fileuploadmain">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Invoice Doc
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <asp:HyperLink ID="lblInstInvDoc" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                <span class="file-input btn btn-azure btn-file">
                                                                    <asp:FileUpload ID="fuInstInvDoc" runat="server" />
                                                                </span>


                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                        <div class="form-group dateimgarea fileuploadmain">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    Other Documents
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <span class="file-input btn btn-azure btn-file">

                                                                    <asp:FileUpload ID="FileUpload1" runat="server" />

                                                                </span>


                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6" id="divOtherDocs" runat="server" visible="false">
                                                        <div class="form-group dateimgarea">
                                                            <span class="name">
                                                                <label class="control-label">
                                                                    <b>Other Docs</b>
                                                                </label>
                                                            </span><span class="dateimg">
                                                                <table>
                                                                    <asp:Repeater ID="rptInstDocs" runat="server" OnItemCommand="rptInstDocs_ItemCommand" OnItemDataBound="rptInstDocs_ItemDataBound">
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td width="50px" style="padding-top: 5px">
                                                                                    <asp:HiddenField ID="hndID" runat="server" Value='<%#Eval("ID") %>' />
                                                                                    <asp:HyperLink ID="hypDoc" runat="server" Target="_blank">
                                                                                        <asp:Image ID="imgDoc" runat="server" ImageUrl="~/images/new/icon_document_downalod.png" />
                                                                                    </asp:HyperLink>
                                                                                </td>

                                                                                <td width="10px"></td>

                                                                                <td style="padding-top: 5px">
                                                                                    <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-xs" CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("ID")%>'>
                                                                         <i class="fa fa-trash"></i> 
                                                                                    </asp:LinkButton>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                    <asp:HiddenField ID="hdndelete" runat="server" />
                                                                </table>
                                                            </span>
                                                            <div class="clear">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 textcenterbutton center-text">
                                <asp:Button class="btn btn-primary savewhiteicon btnsaveicon" ID="btnSave" runat="server" OnClick="btnSave_Click"
                                    Text="Save" CausesValidation="true" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </section>

        <asp:Button ID="btndelete" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
            PopupControlID="modal_danger" DropShadow="false" CancelControlID="Button4" OkControlID="btnOKMobile" TargetControlID="btndelete">
        </cc1:ModalPopupExtender>
        <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

            <div class="modal-dialog " style="margin-top: -300px">
                <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                    <div class="modal-header text-center">
                        <i class="glyphicon glyphicon-fire"></i>
                    </div>


                    <div class="modal-title">Delete</div>
                    <label id="ghh" runat="server"></label>
                    <div class="modal-body ">Are You Sure Delete This Entry?</div>
                    <div class="modal-footer " style="text-align: center">
                        <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>


    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSave" />
        <asp:PostBackTrigger ControlID="btnOpenInvoice" />
        <asp:PostBackTrigger ControlID="btnCreateSTCForm" />
        <asp:PostBackTrigger ControlID="btnPickList" />
        <asp:PostBackTrigger ControlID="imgCustAck" />

    </Triggers>
</asp:UpdatePanel>
