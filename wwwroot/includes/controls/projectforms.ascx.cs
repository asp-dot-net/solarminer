﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.IO;
using System.Configuration;
using System.Data;
using System.Web.UI;

public partial class includes_controls_projectforms : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st2 = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            if ((Roles.IsUserInRole("Sales Manager"))) 
            {
                if (st2.ProjectStatusID == "3")
                {
                    btnUpdateForms.Visible = false;
                }
            }
            if (Roles.IsUserInRole("SalesRep"))
            {
                if (st2.DepositReceived != string.Empty)
                {
                    btnUpdateForms.Visible = false;
                }
            }

        }
    }

    public void BindProjectForms()
    {
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            string ProjectID = Request.QueryString["proid"];
            SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

            if (Roles.IsUserInRole("Administrator"))
            {

            }
            if (Roles.IsUserInRole("Finance"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Accountant"))
            {
                PanAddUpdate.Enabled = true;
                Panel1.Enabled = false;
                Panel2.Enabled = false;
                Panel3.Enabled = true;
            }
            if (Roles.IsUserInRole("BookInstallation"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Customer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Maintenance"))
            {
                PanAddUpdate.Enabled = true;
            }
            if (Roles.IsUserInRole("PostInstaller"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager") || Roles.IsUserInRole("SalesRep") || Roles.IsUserInRole("DSalesRep"))
            {
                if (st.ProjectStatusID == "5")
                {
                    PanAddUpdate.Enabled = false;
                }
            }
            if (Roles.IsUserInRole("SalesRep"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("STC"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Installer"))
            {
                PanAddUpdate.Enabled = false;
            }
            if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
            {
                string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
                SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
                string CEmpType = stEmpC.EmpType;
                //string CSalesTeamID = stEmpC.SalesTeamID;
                string CSalesTeamID = "";
                DataTable dt_empsale = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(stEmpC.EmployeeID);
                if (dt_empsale.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt_empsale.Rows)
                    {
                        CSalesTeamID += dr["SalesTeamID"].ToString() + ",";
                    }
                    CSalesTeamID = CSalesTeamID.Substring(0, CSalesTeamID.Length - 1);
                }

                if (Request.QueryString["proid"] != string.Empty)
                {
                    SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(st.EmployeeID);
                    string EmpType = stEmp.EmpType;
                    //string SalesTeamID = stEmp.SalesTeamID;
                    string SalesTeam = "";
                    DataTable dt_empsale1 = ClstblEmployeeTeam.tblEmployeeTeam_SelectbyEmployeeID(st.EmployeeID);
                    if (dt_empsale1.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt_empsale1.Rows)
                        {
                            SalesTeam += dr["SalesTeamID"].ToString() + ",";
                        }
                        SalesTeam = SalesTeam.Substring(0, SalesTeam.Length - 1);
                    }

                    if (CEmpType == EmpType && CSalesTeamID == SalesTeam)
                    {
                        PanAddUpdate.Enabled = true;
                    }
                    else
                    {
                        PanAddUpdate.Enabled = false;
                    }
                }
            }
            if (Roles.IsUserInRole("PreInstaller"))
            {
                PanAddUpdate.Enabled = true;
            }

            chkAdditionalSystem.Checked = Convert.ToBoolean(st.AdditionalSystem);
            chkGridConnected.Checked = Convert.ToBoolean(st.GridConnected);
            chkRebateApproved.Checked = Convert.ToBoolean(st.RebateApproved);
            chkReceivedCredits.Checked = Convert.ToBoolean(st.ReceivedCredits);
            chkCreditEligible.Checked = Convert.ToBoolean(st.CreditEligible);
            chkMoreThanOneInstall.Checked = Convert.ToBoolean(st.MoreThanOneInstall);
            chkRequiredCompliancePaperwork.Checked = Convert.ToBoolean(st.RequiredCompliancePaperwork);
            chkOutOfPocketDocs.Checked = Convert.ToBoolean(st.OutOfPocketDocs);
            chkOwnerGSTRegistered.Checked = Convert.ToBoolean(st.OwnerGSTRegistered);

            txtOwnerABN.Text = st.OwnerABN;
            txtInverterCert.Text = st.InverterCert;            

            txtSystemCapacity.Text = st.SystemCapKW;
            txtElectricalRetailer.Text = st.ElecRetailer;

            if (st.InstallBase != "")
            {
                //rblInstallBase.SelectedValue = st.InstallBase;
                if (st.InstallBase == "1")
                {
                    rblInstallBase1.Checked = true;
                }
                if (st.InstallBase == "2")
                {
                    rblInstallBase2.Checked = true;
                }
            }
        }
    }

    protected void btnUpdateForms_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        string AdditionalSystem = Convert.ToString(chkAdditionalSystem.Checked);
        string GridConnected = Convert.ToString(chkGridConnected.Checked);
        string RebateApproved = Convert.ToString(chkRebateApproved.Checked);
        string ReceivedCredits = Convert.ToString(chkReceivedCredits.Checked);
        string CreditEligible = Convert.ToString(chkCreditEligible.Checked);
        string MoreThanOneInstall = Convert.ToString(chkMoreThanOneInstall.Checked);
        string RequiredCompliancePaperwork = Convert.ToString(chkRequiredCompliancePaperwork.Checked);
        string OutOfPocketDocs = Convert.ToString(chkOutOfPocketDocs.Checked);
        string OwnerGSTRegistered = Convert.ToString(chkOwnerGSTRegistered.Checked);
        string OwnerABN = txtOwnerABN.Text;
        string InverterCert = txtInverterCert.Text;
        //string InstallBase = rblInstallBase.SelectedValue;
        string InstallBase = false.ToString();
        if (rblInstallBase1.Checked == true)
        {
            InstallBase = "1";
        }
        if (rblInstallBase2.Checked == true)
        {
            InstallBase = "2";
        }

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string UpdatedBy = stEmp.EmployeeID;

        bool sucForms = ClsProjectSale.tblProjects_UpdateForms(ProjectID, AdditionalSystem, GridConnected, RebateApproved, ReceivedCredits, CreditEligible, MoreThanOneInstall, RequiredCompliancePaperwork, OutOfPocketDocs, OwnerGSTRegistered, OwnerABN, InverterCert, InstallBase, UpdatedBy);

        /* -------------------------- Complete -------------------------- */ //txtSalesCommPaid.Text.Trim() != string.Empty
        if (st.PVDStatusID == "7" && st.InstallerPayDate != string.Empty)
        {
            ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "5");
            int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("5", ProjectID, stEmp.EmployeeID, st.NumberPanels);
        }
        /* -------------------------------------------------------------- */

        BindProjects();
        if (sucForms)
        {
            SetAdd1();
            //PanSuccess.Visible = true;
        }
        else
        {
            SetError1();
            //PanError.Visible = true;
        }
    }

    protected void chkAdditionalSystem_CheckedChanged(object sender, EventArgs e)
    {
        //ScriptManager.RegisterStartupScript(UpdatePanelGrid,
        //                                   this.GetType(),
        //                                   "MyAction",
        //                                   "doMyAction();",
        //                                   true);
        if (chkAdditionalSystem.Checked == true)
        {
            chkRebateApproved.Checked = true;
            chkReceivedCredits.Checked = true;
            chkCreditEligible.Checked = true;
            chkMoreThanOneInstall.Checked = true;
        }
        else
        {
            chkRebateApproved.Checked = false;
            chkReceivedCredits.Checked = false;
            chkCreditEligible.Checked = true;
            chkMoreThanOneInstall.Checked = false;
        }
    }

    protected void btnCreateSTCForm_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];
        SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        try
        {
            TextWriter txtWriter = new StringWriter() as TextWriter;
            if (st.ProjectTypeID == "3")
            {
                Telerik_reports.generate_stcug(ProjectID);
                //  Server.Execute("~/mailtemplate/stcug.aspx?id=" + ProjectID, txtWriter);
            }
            else
            {
                Telerik_reports.generate_STCform(ProjectID);
                //Server.Execute("~/mailtemplate/STCform.aspx?id=" + ProjectID, txtWriter);
            }

            //String htmlText = txtWriter.ToString();
            ////Response.Write(htmlText);
            //HTMLExportToPDF(htmlText, ProjectID + "STCform.pdf");
        }
        catch
        {

        }
    }

    public void HTMLExportToPDF(string HTML, string filename)
    {
        System.Web.HttpResponse Response = System.Web.HttpContext.Current.Response;
        //try
        //{
        //    string pdfUser = ConfigurationManager.AppSettings["PDFUserName"];
        //    string pdfKey = ConfigurationManager.AppSettings["PDFUserKey"];
        //    // create an API client instance
        //    //pdfcrowd.Client client = new pdfcrowd.Client(pdfUser, pdfKey);
        //    // convert a web page and write the generated PDF to a memory stream
        //    MemoryStream Stream = new MemoryStream();
        //    //client.setVerticalMargin("66pt");

        //    TextWriter txtWriter = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/stcheader.aspx", txtWriter);

        //    TextWriter txtWriter2 = new StringWriter() as TextWriter;
        //    Server.Execute("~/mailtemplate/pdffooter.aspx", txtWriter2);

        //    string HeaderHtml = txtWriter.ToString();
        //    string FooterHtml = txtWriter2.ToString();

        //    //client.setHeaderHtml(HeaderHtml);
        //    //client.setFooterHtml(FooterHtml);

        //    client.convertHtml(HTML, Stream);

        //    // set HTTP response headers
        //    Response.Clear();
        //    Response.AddHeader("Content-Type", "application/pdf");
        //    Response.AddHeader("Cache-Control", "no-cache");
        //    Response.AddHeader("Accept-Ranges", "none");
        //    Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
        //    // send the generated PDF
        //    Stream.WriteTo(Response.OutputStream);
        //    Stream.Close();
        //    Response.Flush();
        //    Response.End();
        //}
        //catch (pdfcrowd.Error why)
        //{
        //    //Response.Write(why.ToString());
        //    //Response.End();
        //}
    }
    public void BindProjects()
    {
        GridView GridView1 = (GridView)this.Parent.Parent.FindControl("GridView1");
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblProjects.tblProjects_SelectByUserIdCust(userid, Request.QueryString["compid"]);
        if ((Roles.IsUserInRole("DSales Manager")) || (Roles.IsUserInRole("Sales Manager")) || (Roles.IsUserInRole("STC")) || (Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("InstallationManager")) || Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("PreInstaller") || Roles.IsUserInRole("PostInstaller"))
        {
            dt = ClstblProjects.tblProjects_SelectByUCustomerID(Request.QueryString["compid"]);
        }
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }

    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
}