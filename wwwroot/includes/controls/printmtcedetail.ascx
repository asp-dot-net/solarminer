<%@ Control Language="C#" AutoEventWireup="true" CodeFile="printmtcedetail.ascx.cs" Inherits="includes_controls_printmtcedetail" %>

<script type="text/javascript">
function printmaintainanceContent() {
    var PageHTML = document.getElementById('<%= (divPrintPage.ClientID) %>').innerHTML;
            var html = '<html><head>' +
             '<link href="../../css/print.css" rel="stylesheet" type="text/css" media="print"/>' +
        '</head><body style="background:#ffffff;">' +
        PageHTML +
        '</body></html>';
            var WindowObject = window.open("", "PrintWindow",
        "width=750,height=650,top=50,left=50,toolbars=no,scrollbars=yes,status=no,resizable=yes");
            WindowObject.document.writeln(html);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
            document.getElementById('print_link').style.display = 'block';
        }
    </script>
<div style="text-align: right; padding-right: 10px;" class="printpage">
    <a href="javascript:printmaintainanceContent();">
        <i class="icon-print printpage"></i>Print
    </a>
</div>
<div style="clear: both;"></div>
<div id="divPrintPage" runat="server">
    <div style="width: 600px; margin: 0px auto;">
        <div>
            <div class="toparea" style="font-family: Arial, Helvetica, sans-serif;">
                <div style="float: left; font-family: Arial, Helvetica, sans-serif; padding: 15px 190px 0px 0px;">
                    <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/eurosolarlogo.png") %>"
                         />
                </div>
                <div style="float: left; width: 30%; margin-top: 50px; text-align: right;">
                    <table style="border: 1px solid #ccc; border-collapse: collapse; font-size: 11px;">
                        <tr>
                            <td style="border-right: 1px solid #ccc; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 5px 15px;">Project No:</td>
                            <td style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 5px 15px;">
                                <asp:Label ID="lblProjectNumber" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="border-right: 1px solid #ccc; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 5px 15px;">Manual No:</td>
                            <td style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 5px 15px;">
                                <asp:Label ID="lblManualQuoteNumber" runat="server"></asp:Label></td>
                        </tr>
                        <tr>
                            <td style="border-right: 1px solid #ccc; border-collapse: collapse; padding: 5px 15px;">Call Date:</td>
                            <td style="padding: 5px 15px; border-collapse: collapse;">
                                <asp:Label ID="lblOpenDate" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div style="clear: both;"></div>
            <div style="background-color: #000; color: #fff; font-weight: bold; font-size: 13px; text-align: center; margin: 5px 0px; padding: 3px; font-weight: bold;">
                MAINTENANCE CONTRACT
            </div>
            <div style="width: 65%; float: left;">
                <div style="background-color: #8C8C8C; color: #fff; font-size: 13px; font-weight: bold; text-align: center; padding: 3px;">CUSTOMER DETAILS</div>
                <table style="border: 1px solid #ccc; border-collapse: collapse; width: 100%; font-size: 11px;">
                    <tr>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Name:</td>
                        <td colspan="10" style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:Label ID="lblName" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Address:</td>
                        <td colspan="10" style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:Label ID="lblAddress" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Suburb:</td>
                        <td colspan="8" style="border-bottom: 1px solid #ccc; border-right: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:Label ID="lblCity" runat="server"></asp:Label></td>
                        <td colspan="1" style="border-bottom: 1px solid #ccc; border-right: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:Label ID="lblState" runat="server"></asp:Label></td>
                        <td colspan="1" style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 5px 15px;">
                            <asp:Label ID="lblPCode" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Contact:</td>
                        <td colspan="10" style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:Label ID="lblContact" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Email:</td>
                        <td colspan="10" style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:Label ID="lblEmail" runat="server"></asp:Label></td>
                    </tr>
                </table>
                <div style="background-color: #8C8C8C; color: #fff; font-size: 13px; font-weight: bold; text-align: center; padding: 3px;">SYSTEM INSTALLED</div>
                <table width="100%" style="border: 1px solid #ccc; border-collapse: collapse; font-size: 11px;">
                    <tr>
                        <td width="70" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Panels:</td>
                        <td colspan="10" style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:Label ID="lblPanelDetails" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td width="70" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Inverter:</td>
                        <td colspan="10" style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:Label ID="lblInverterDetails" runat="server"></asp:Label></td>
                    </tr>
                </table>
                <table style="border: 1px solid #ccc; border-collapse: collapse; width: 100%; font-size: 11px;">
                    <tr>
                        <td width="120px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Roof Type:</td>
                        <td style="border-bottom: 1px solid #ccc; border-right: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:Label ID="lblRoofType" runat="server"></asp:Label></td>
                        <td width="80px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Roof Slope:</td>
                        <td style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:Label ID="lblRoofSlope" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td width="120px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">House Type:</td>
                        <td style="border-bottom: 1px solid #ccc; border-right: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:Label ID="lblHouseType" runat="server"></asp:Label></td>
                        <td width="10px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Inst Date:</td>
                        <td style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:Label ID="lblInstDate" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td width="120px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Assigned To:</td>
                        <td colspan="3" style="border-bottom: 1px solid #ccc; border-collapse: collapse; padding: 1px 5px;">
                            <asp:Label ID="lblAssignedTo" runat="server"></asp:Label></td>
                    </tr>
                </table>
                <div style="background-color: #8C8C8C; color: #fff; font-size: 13px; font-weight: bold; text-align: center; padding: 3px;">PAYMENT FOR SERVICE</div>
                <table width="100%" style="border: 1px solid #ccc; border-collapse: collapse; font-size: 11px;">
                    <tr>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Amount</td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Discount</td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Balance</td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Paid By</td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Received By</td>
                    </tr>
                    <tr>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="5" height="10" algn="absmiddle" /><asp:Label ID="lblAmount" runat="server"></asp:Label></td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="5" height="10" algn="absmiddle" /><asp:Label ID="lblDiscount" runat="server"></asp:Label></td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <img src="<%=MakeImageSrcData( Request.PhysicalApplicationPath+"/images/img_dollar.png") %>" width="5" height="10" algn="absmiddle" /><asp:Label ID="lblBalance" runat="server"></asp:Label></td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <asp:Label ID="lblPaidBy" runat="server"></asp:Label></td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <asp:Label ID="lblReceivedBy" runat="server"></asp:Label></td>
                    </tr>
                </table>
            </div>
            <div style="width: 33%; float: right;">
                <div style="background-color: #8C8C8C; color: #fff; font-size: 13px; font-weight: bold; text-align: center; padding: 3px;">SITE DIAGRAM</div>
                <table style="border: 1px solid #ccc; border-collapse: collapse; width: 100%; font-size: 11px;">
                    <tr>
                        <td width="30%;" style="height: 160px;">&nbsp;</td>
                    </tr>
                </table>
                <div style="background-color: #8C8C8C; color: #fff; font-size: 13px; font-weight: bold; text-align: center; padding: 3px;">PANEL CONFIGURATION</div>
                <table style="border: 1px solid #ccc; border-collapse: collapse; width: 100%; font-size: 11px;">
                    <tr>
                        <td width="100px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">North Side</td>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <asp:Label ID="lblNorth" runat="server"></asp:Label></td>
                        <td width="30px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Volts</td>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <asp:Label ID="lblNVolts" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">West Side</td>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <asp:Label ID="lblWest" runat="server"></asp:Label></td>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Volts</td>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <asp:Label ID="lblWVolts" runat="server"></asp:Label></td>
                    </tr>
                    <tr>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Other</td>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <asp:Label ID="lblOther" runat="server"></asp:Label></td>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">Volts</td>
                        <td width="50px;" style="border-right: 1px solid #ccc; font-weight: bold; border-collapse: collapse; border-bottom: 1px solid #ccc; padding: 1px 5px;">
                            <asp:Label ID="lblOVolts" runat="server"></asp:Label></td>
                    </tr>
                </table>
            </div>
            <div style="clear: both;"></div>
            <div style="margin: 10px 0px 0px 0px; width: 100%; height: 310px; border: 1px solid #ccc;">
                <div style="font-weight: bold; padding: 10px 0px 0px 10px;">Customer Input:</div>
                <div style="height: 50px; padding: 2px 0px 0px 10px;">
                    <asp:Label ID="lblCustomerInput" runat="server"></asp:Label>
                </div>
                <div style="font-weight: bold; padding: 10px 0px 0px 10px;">Fault Identified:</div>
                <div style="height: 50px; padding: 2px 0px 0px 10px;">
                    <asp:Label ID="lblFault" runat="server"></asp:Label>
                </div>
                <div style="font-weight: bold; padding: 10px 0px 0px 10px;">Action Required:</div>
                <div style="height: 50px; padding: 2px 0px 0px 10px;">
                    <asp:Label ID="lblActionRequired" runat="server"></asp:Label>
                </div>
                <div style="font-weight: bold; padding: 10px 0px 0px 10px;">Work Done:</div>
                <div style="padding: 2px 0px 0px 10px; height: 50px;">
                    <asp:Label ID="lblWorkDone" runat="server"></asp:Label>
                </div>
            </div>
            <div style="clear: both;"></div>

            <table width="100%;" style="font-size: 11px; margin-top: 10px;">

                <tr>
                    <td>I ......................................................................... confirm that the details above are correct and the Think Green Solar Assistant has resolved and serviced the solar system in regards to the details in Customer Input.</td>
                </tr>
            </table>
            <table width="100%;" style="font-size: 11px;">
                <tr>
                    <td colspan="3" valign="top" style="padding: 5px; display: block;">Customer Signature:_________________________________</td>
                    <td colspan="3" valign="top" style="padding: 5px;">Date:_______________ </td>
                </tr>
            </table>
            <table width="100%;" style="font-size: 11px;">
                <tr style="width: 100%; text-align: center;">
                    <td>P & N Pty Ltd T/A Think Green Solar. ABN 95 130 845 199 Phone: 1300 959 013 <%=Siteurl%></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<asp:HiddenField ID="HiddenField1" runat="server" />