﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="companysummary.ascx.cs"
    Inherits="includes_companysummary" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

    

<div class="panel-body paddnoneall">
    <div class="row">
        <div class="col-md-6">
            <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-striped table-hover table table-bordered">
                <tr id="trD2DEmployee" runat="server" visible="false">
                    <td width="200px" valign="top">Out Door Employee
                    </td>
                    <td>
                        <asp:Label ID="lblD2DEmployee" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr id="trD2DAppDate" runat="server" visible="false">
                    <td width="200px" valign="top">Appointment Date
                    </td>
                    <td>
                        <asp:Label ID="lblD2DAppDate" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr id="trD2DAppTime" runat="server" visible="false">
                    <td width="200px" valign="top">Appointment Time
                    </td>
                    <td>
                        <asp:Label ID="lblD2DAppTime" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Customer
                    </td>
                    <td>
                        <asp:Label ID="lblCustomer" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="display:none;">
                    <td width="200px" valign="top">CompanyNumber
                    </td>
                    <td>
                        <asp:Label ID="lblCompanyNumber" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Name
                    </td>
                    <td>
                        <asp:Label ID="lblMrMs" runat="server"></asp:Label>
                        <asp:Label ID="lblFirstName" runat="server"></asp:Label>
                        <asp:Label ID="lblLastName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Mobile
                    </td>
                    <td>
                        <asp:Label ID="lblMobile" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Email
                    </td>
                    <td>
                        <asp:Label ID="lblEmail" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Phone
                    </td>
                    <td>
                        <asp:Label ID="lblPhone" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Alt Phone
                    </td>
                    <td>
                        <asp:Label ID="lblAltPhone" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Type
                    </td>
                    <td>
                        <asp:Label ID="lblType" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;(<asp:Label ID="lblRes" runat="server"></asp:Label>)
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Source
                    </td>
                    <td>
                        <asp:Label ID="lblSource" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr id="trSubSource" runat="server" visible="false">
                    <td width="200px" valign="top">SubSource
                    </td>
                    <td>
                        <asp:Label ID="lblSubSource" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Notes
                    </td>
                    <td>
                        <asp:Label ID="lblNotes" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-6">
            <table width="100%" border="0" cellspacing="0" cellpadding="5" class="table table-striped table-hover table table-bordered">
                <tr>
                    <td width="200px" valign="top">Street Address
                    </td>
                    <td>
                        <asp:Label ID="lblStreetAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Street City
                    </td>
                    <td>
                        <asp:Label ID="lblStreetCity" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Street State
                    </td>
                    <td>
                        <asp:Label ID="lblStreetState" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Street PostCode
                    </td>
                    <td>
                        <asp:Label ID="lblStreetPostCode" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Postal Address
                    </td>
                    <td>
                        <asp:Label ID="lblPostalAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Postal City
                    </td>
                    <td>
                        <asp:Label ID="lblPostalCity" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Postal State
                    </td>
                    <td>
                        <asp:Label ID="lblPostalState" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Postal PostCode
                    </td>
                    <td>
                        <asp:Label ID="lblPostalPostCode" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Area
                    </td>
                    <td>
                        <asp:Label ID="lblArea" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="200px" valign="top">Country
                    </td>
                    <td>
                        <asp:Label ID="lblCountry" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="visibility:hidden;display:none">
                    <td width="200px" valign="top">Website
                    </td>
                    <td>
                        <asp:Label ID="lblWebsite" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr style="visibility:hidden;display:none">
                    <td width="200px" valign="top">Fax
                    </td>
                    <td>
                        <asp:Label ID="lblFax" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>

    </div>
</div>
</ContentTemplate>
</asp:UpdatePanel>


 <div class="loaderPopUP">
    <script>
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(pageLoadedpro);
        //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
        prm.add_beginRequest(beginrequesthandler);
        // raised after an asynchronous postback is finished and control has been returned to the browser.
        prm.add_endRequest(endrequesthandler);

        function beginrequesthandler(sender, args) {
            //shows the modal popup - the update progress
            $('.loading-container').css('display', 'block');
          
        }
        function endrequesthandler(sender, args) {
            //hide the modal popup - the update progress
        }

        function pageLoadedpro() {

            $('.loading-container').css('display', 'none');
           
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            
        }
    </script>
   <%-- <asp:UpdateProgress ID="updateprogress2" runat="server" DisplayAfter="0">
        <ProgressTemplate>
        <div class="loading-container">
        <div class="loader"></div>
        </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <cc1:ModalPopupExtender ID="newmodalpopup" runat="server" TargetControlID="updateprogress2"
        PopupControlID="updateprogress2" BackgroundCssClass="modalPopup" />--%>
</div>



<%--<script type="text/javascript">
    $(document).ready(function () {
        loader();
    });
    function loader() {
        $(window).load(function () {
        });
    }
</script>--%>
