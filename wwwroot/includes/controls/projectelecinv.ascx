<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeFile="projectelecinv.ascx.cs"
    Inherits="includes_controls_projectelecinv" %>
<script>
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoadedINV);
    //raised before processing of an asynchronous postback starts and the postback request is sent to the server.
    prm.add_beginRequest(beginrequesthandler);
    // raised after an asynchronous postback is finished and control has been returned to the browser.
    prm.add_endRequest(endrequesthandler);
    function beginrequesthandler(sender, args) {
        //shows the modal popup - the update progress
        //$('.loading-container').css('display', 'block');
    }
    function endrequesthandler(sender, args) {
        //hide the modal popup - the update progress
        $('.loading-container').css('display', 'none');
        //  $('input[type=file]').bootstrapFileInput();
    }

    function pageLoadedINV() {
        $('.datetimepicker1').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        // $('.loading-container').css('display', 'none');

        $(".myval").select2({
            //placeholder: "select",
            allowclear: true
        });
    }
</script>

<style type="text/css">
    .selected_row {
        background-color: #A1DCF2 !important;
    }
</style>
<script type="text/javascript">
    $(function () {
        $("[id*=GridView1] td").bind("click", function () {
            var row = $(this).parent();
            $("[id*=GridView1] tr").each(function () {
                if ($(this)[0] != row[0]) {
                    $("td", this).removeClass("selected_row");
                }
            });
            $("td", row).each(function () {
                if (!$(this).hasClass("selected_row")) {
                    $(this).addClass("selected_row");
                } else {
                    $(this).removeClass("selected_row");
                }
            });
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.loading-container').css('display', 'none');
       // HighlightControlToValidate();
        $('#<%=btnUpdateElecInv.ClientID %>').click(function () {
            formValidate();
        });
    });
    function formValidate() {
        if (typeof (Page_Validators) != "undefined") {
            for (var i = 0; i < Page_Validators.length; i++) {
                if (!Page_Validators[i].isvalid) {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#b94a48");
                }
                else {
                    $('#' + Page_Validators[i].controltovalidate).css("border-color", "#B5B5B5");
                }
            }
        }
    }
    //function HighlightControlToValidate() {
    //    if (typeof (Page_Validators) != "undefined") {
    //        for (var i = 0; i < Page_Validators.length; i++) {
    //            $('#' + Page_Validators[i].controltovalidate).blur(function () {
    //                var validatorctrl = getValidatorUsingControl($(This).attr("ID"));
    //                if (validatorctrl != null && !validatorctrl.isvalid) {
    //                    $(This).css("border-color", "#b94a48");
    //                }
    //                else {
    //                    $(This).css("border-color", "#B5B5B5");
    //                }
    //            });
    //        }
    //    }
    //}
    function getValidatorUsingControl(controltovalidate) {
        var length = Page_Validators.length;
        for (var j = 0; j < length; j++) {
            if (Page_Validators[j].controltovalidate == controltovalidate) {
                return Page_Validators[j];
            }
        }
        return null;
    }
</script>
<script>



    function ComfirmDelete(event, ctl) {
        event.preventDefault();
        var defaultAction = $(ctl).prop("href");

        swal({
            title: "Are you sure you want to delete this Record?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: true,
            closeOnCancel: true
        },

            function (isConfirm) {
                if (isConfirm) {
                    // swal("Deleted!", "Your imaginary file has been deleted.", "success");
                    eval(defaultAction);

                    //swal("Deleted!", "Your imaginary file has been deleted.", "success");
                    return true;
                } else {
                    // swal("Cancelled", "Your imaginary file is safe :)", "error");
                    return false;
                }
            });
    }


</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>


        <section class="row m-b-md">
            <div class="col-sm-12 minhegihtarea elecpage">
                <div class="contactsarea">
                    <div class="messesgarea">
                        <div class="alert alert-success" id="PanSuccess" runat="server" visible="false"><i class="icon-ok-sign"></i><strong>&nbsp;Transaction Successful!</strong> </div>
                        <div class="alert alert-danger" id="PanError" runat="server" visible="false"><i class="icon-remove-sign"></i><strong>&nbsp;Transaction Failed.</strong> </div>
                        <div class="alert alert-danger" id="DivDocNotVerified" runat="server" visible="false"><i class="icon-remove-sign"></i><strong>&nbsp;Your documents are not verified.</strong></div>
                        <div class="alert alert-danger" id="divNoMQN" runat="server" visible="false"><i class="icon-remove-sign"></i><strong>&nbsp;Please Enter Manual Quote No.</strong></div>
                        <div class="alert alert-danger" id="divActive" runat="server" visible="false"><i class="icon-remove-sign"></i><strong>&nbsp;Your Project is not Active.</strong></div>
                    </div>

                    <asp:Panel ID="PanAddUpdate" runat="server" CssClass="PanAddUpdate">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">

                                        <asp:Panel ID="Panel1" runat="server">
                                            <div>
                                                <div class="widget flat radius-bordered borderone">
                                                    <div class="widget-header bordered-bottom bordered-blue">
                                                        <span class="widget-caption">System Details</span>
                                                    </div>
                                                    <div class="widget-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <span class="name">
                                                                                <label class="control-label">Roof Angle </label>
                                                                            </span><span>
                                                                                <asp:TextBox ID="txtElevRoofAngle" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                            </span>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <span class="name">
                                                                                <label class="control-label">Other Variations </label>
                                                                            </span><span>
                                                                                <asp:TextBox ID="txtElecOtherVariations" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                            </span>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group dateimgarea">
                                                                            <span class="name">
                                                                                <label class="control-label">House Type </label>
                                                                            </span><span class="dateimg">
                                                                                <asp:TextBox ID="txtElevHouseType" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                            </span>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <span class="name">
                                                                                <label class="control-label">Roof Type </label>
                                                                            </span><span class="dateimg">
                                                                                <asp:TextBox ID="txtElevRoofType" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                            </span>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group dateimgarea">
                                                                            <span class="name">
                                                                                <label class="control-label">Panel Details </label>
                                                                            </span><span class="dateimg">
                                                                                <asp:TextBox ID="txtElecPanelDetails" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                            </span>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <span class="name">
                                                                                <label class="control-label">Inverter Details </label>
                                                                            </span><span>
                                                                                <asp:TextBox ID="txtElecInverterDetails" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                            </span>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="form-group dateimgarea col-md-3" style="padding-top: 10px;">

                                                                        <label class="control-label"></label>
                                                                        <span class="dateimg">Travel Dist
                                                                    <asp:TextBox ID="txtTravelDist" runat="server" Enabled="false" CssClass="form-control"
                                                                        Width="80px"></asp:TextBox>
                                                                            &nbsp;KM </span>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                    <div class="form-group col-md-2 checkboxmain2" style="padding-top: 10px;">
                                                                        <div class=" checkbox-info checkbox">
                                                                            <span class="fistname">
                                                                                <label for="<%=chkElecSplitSystem.ClientID %>">
                                                                                    <asp:CheckBox ID="chkElecSplitSystem" runat="server" />
                                                                                    <span class="text">Split System  &nbsp;</span>
                                                                                </label>
                                                                            </span>
                                                                        </div>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                    <div class="form-group dateimgarea col-md-2 checkboxmain2" style="padding-top: 10px;">
                                                                        <div class=" checkbox-info checkbox">
                                                                            <span class="fistname">
                                                                                <label id="Label9" for="<%=chkElecCheryPicker.ClientID %>" runat="server">
                                                                                    <asp:CheckBox ID="chkElecCheryPicker" runat="server" />
                                                                                    <span class="text">Chery Picker  &nbsp;</span>
                                                                                </label>
                                                                            </span>
                                                                        </div>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                    <div class="form-group dateimgarea col-md-2 checkboxmain2" style="padding-top: 10px;">
                                                                        <div class=" checkbox-info checkbox">
                                                                            <span class="fistname">
                                                                                <label id="Label10" for="<%=chkElecSTCSaved.ClientID %>" runat="server">
                                                                                    <asp:CheckBox ID="chkElecSTCSaved" runat="server" />
                                                                                    <span class="text">STC Saved &nbsp;</span>
                                                                                </label>
                                                                            </span>
                                                                        </div>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                    <div class="form-group col-md-2 checkboxmain2" style="padding-top: 10px;">
                                                                        <div class=" checkbox-info checkbox">
                                                                            <span class="fistname">
                                                                                <label for="<%=chkElecCertSaved.ClientID %>">
                                                                                    <asp:CheckBox ID="chkElecCertSaved" runat="server" />
                                                                                    <span class="text">Cert Saved &nbsp;</span>
                                                                                </label>
                                                                            </span>
                                                                        </div>

                                                                        <div class="clear"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel ID="Panel3" runat="server">
                                            <div>
                                                <div class="widget flat radius-bordered borderone">
                                                    <div class="widget-header bordered-bottom bordered-blue">
                                                        <span class="widget-caption">Sales Commission Paid</span>
                                                    </div>
                                                    <div class="widget-body">
                                                        <div>
                                                            <div class="row">
                                                                <div class="form-group col-md-4">
                                                                    <span class="name">
                                                                        <asp:Label ID="Label23" runat="server" class="name disblock  control-label">
                                                Sales Comm Paid Date</asp:Label></span>
                                                                    <div class="input-group date datetimepicker1 col-sm-12">
                                                                        <span class="input-group-addon">
                                                                            <span class="fa fa-calendar"></span>
                                                                        </span>
                                                                        <asp:TextBox ID="txtSalesCommPaid" runat="server" class="form-control" Width="195px">
                                                                        </asp:TextBox>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-md-4" style="padding-top: 17px;">
                                                                    <div class=" checkbox-info checkbox" id="divSalesComm" runat="server">
                                                                        <span class="fistname">
                                                                            <label for="<%=chkSalesComm.ClientID %>">
                                                                                <asp:CheckBox ID="chkSalesComm" runat="server" />
                                                                                <span class="text">Sales Comm Paid &nbsp;</span>
                                                                            </label>
                                                                        </span>
                                                                    </div>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group col-md-4" style="padding-top: 17px;">

                                                                    <div class="" id="div1" runat="server">
                                                                        <div class="form-group">
                                                                            <div class=" checkbox-info checkbox">
                                                                                <span class="fistname">
                                                                                    <label for="<%=chkPanelComm.ClientID %>">
                                                                                        <asp:CheckBox ID="chkPanelComm" runat="server" />
                                                                                        <span class="text">Panel Comm Paid &nbsp;</span>
                                                                                    </label>
                                                                                </span>
                                                                            </div>
                                                                            <div class="clear"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>


                                                            <%-- <div class="col-md-4">
                                                                <div class="form-group dateimgarea">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">
                                                                            
                                                                        </label>
                                                                    </span><span class="dateimg">
                                                                        <asp:TextBox ID="txtSalesCommPaid" runat="server" CssClass="form-control"
                                                                            Width="150px"></asp:TextBox>
                                                                        <asp:ImageButton ID="Image17" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                        <cc1:CalendarExtender ID="CalendarExtender17" runat="server" PopupButtonID="Image17"
                                                                            TargetControlID="txtSalesCommPaid" Format="dd/MM/yyyy">
                                                                        </cc1:CalendarExtender>
                                                                        <asp:RegularExpressionValidator ValidationGroup="forms" ControlToValidate="txtSalesCommPaid" ID="RegularExpressionValidator14" runat="server" ErrorMessage="Enter valid date"
                                                                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                    </span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                            </div>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel ID="Panel2" runat="server">
                                            <div>
                                                <div class="widget flat radius-bordered borderone">
                                                    <div class="widget-header bordered-bottom bordered-blue">
                                                        <span class="widget-caption">Solar Installer Payment</span>
                                                    </div>
                                                    <div class="widget-body">
                                                        <div>
                                                            <div class="row">
                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <span class="name disblock">
                                                                            <label class="control-label">Select Type</label>
                                                                        </span><span>
                                                                            <asp:DropDownList ID="ddlInvType" runat="server" AutoPostBack="true" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval form-control"
                                                                                OnSelectedIndexChanged="ddlInvType_SelectedIndexChanged">
                                                                                <asp:ListItem Value="">Select</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </span>
                                                                        <div class="clear"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6" id="divInstaller1" runat="server" visible="false">
                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <label class="control-label">Installer Inv No </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtInstallerInvNo" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group ">
                                                                    <span class="name">
                                                                        <label class="control-label">Installer Inv Amnt </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtInstallerAmnt" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server"
                                                                            ValidationGroup="elecinv" ControlToValidate="txtInstallerAmnt" Display="Dynamic"
                                                                            ErrorMessage="Please enter a number" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group dateimgarea">
                                                                    <span class="name">
                                                                        <label class="control-label">Installer Inv Date </label>
                                                                    </span><span class="dateimg">
                                                                        <asp:TextBox ID="txtInstallerInvDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                                        <asp:ImageButton ID="Image30" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" CausesValidation="false" />
                                                                        <cc1:CalendarExtender ID="CalendarExtender30" runat="server" PopupButtonID="Image30"
                                                                            TargetControlID="txtInstallerInvDate" Format="dd/MM/yyyy">
                                                                        </cc1:CalendarExtender>
                                                                        <asp:RegularExpressionValidator ValidationGroup="elecinv" ControlToValidate="txtInstallerInvDate" ID="RegularExpressionValidator1" runat="server" ErrorMessage="Enter valid date"
                                                                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group dateimgarea">
                                                                    <span class="name">
                                                                        <label class="control-label">Installer Pay Date </label>
                                                                    </span><span class="dateimg">
                                                                        <asp:TextBox ID="txtInstallerPayDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                                        <asp:Image ID="Image31" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                        <cc1:CalendarExtender ID="CalendarExtender31" runat="server" PopupButtonID="Image31"
                                                                            TargetControlID="txtInstallerPayDate" Format="dd/MM/yyyy">
                                                                        </cc1:CalendarExtender>
                                                                        <asp:RegularExpressionValidator ValidationGroup="elecinv" ControlToValidate="txtInstallerPayDate" ID="RegularExpressionValidator4" runat="server" ErrorMessage="Enter valid date"
                                                                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group ">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">
                                                                            Approved By
                                                                        </label>
                                                                    </span><span>
                                                                        <asp:DropDownList ID="ddlApprovedBy" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group ">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Amount
                                                                        </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtAmount"
                                                                            ValidationGroup="preinst" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                                    </span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6" id="divInstaller" runat="server" visible="false">
                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <label class="control-label">Notes </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtElectricianInvoiceNotes" runat="server" TextMode="MultiLine"
                                                                            Height="50px" CssClass="form-control"></asp:TextBox>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group" runat="server" visible="false">
                                                                    <span class="name">
                                                                        <label class="control-label">Upload Inv Doc </label>
                                                                    </span><span>
                                                                        <asp:HyperLink ID="lblInvDoc" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                        <asp:FileUpload ID="fuInvDoc" runat="server" />
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group" runat="server" visible="false">
                                                                    <span class="name">
                                                                        <label class="control-label">
                                                                            Extra Work
                                                                        </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtExtraWork" runat="server" CssClass="form-control" TextMode="MultiLine"></asp:TextBox>
                                                                    </span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group " runat="server" visible="false">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">
                                                                            Installer
                                                                        </label>
                                                                    </span><span>
                                                                        <asp:DropDownList ID="ddlInstaller" runat="server" AppendDataBoundItems="true" Enabled="false"
                                                                            aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Installer</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="" id="divDoorToDoor" runat="server" visible="false">
                                                            <div class="col-md-6" id="basecost" runat="server" visible="false">
                                                                <div class="form-group dateimgarea">
                                                                    <span class="name">
                                                                        <label class="control-label">Base Cost Price</label>
                                                                    </span><span class="dateimg">
                                                                        <asp:TextBox ID="txtBaseCostPrice" runat="server" Text="0" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group " id="marketprice" runat="server" visible="false">
                                                                    <span class="name">
                                                                        <label class="control-label">Market Price</label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtSalesMarketPrice" Text="0" runat="server" CssClass="form-control"></asp:TextBox><%--AutoPostBack="true" OnTextChanged="txtSalesMarketPrice_TextChanged"--%>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtSalesMarketPrice"
                                                                            ValidationGroup="elecinv" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group " id="landingprice" runat="server" visible="false">
                                                                    <span class="name">
                                                                        <label class="control-label">Landing Price</label>
                                                                    </span><span class="dateimg">
                                                                        <asp:TextBox ID="txtSalesLandingPrice" MaxLength="15" Text="0" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <%--OnTextChanged="txtSalesLandingPrice_TextChanged" AutoPostBack="true"--%>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtSalesLandingPrice"
                                                                            ValidationGroup="elecinv" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group ">
                                                                    <span class="name">
                                                                        <label class="control-label">Invoice Amnt</label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtSalesInvAmnt" Enabled="true" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtSalesInvAmnt"
                                                                            ValidationGroup="elecinv" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <%--   <div class="form-group" id="divAdvAmount" runat="server" visible="false">
                                                                <span class="name">
                                                                    <label class="control-label">Advance Amount</label>
                                                                </span><span>
                                                                    <asp:TextBox ID="txtAdvanceAmount" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                                    <asp:Label ID="lblAdvanceAmount" runat="server"></asp:Label>
                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server" ControlToValidate="txtAdvanceAmount"
                                                                        ValidationGroup="elecinv" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                                    <asp:CompareValidator ID="CompareValidatorAdvance" Type="Double" runat="server" ErrorMessage="Invalid Amount"
                                                                        ControlToValidate="txtAdvanceAmount" Operator="LessThanEqual"
                                                                        Display="Dynamic" ValidationGroup="elecinv"></asp:CompareValidator>
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="form-group dateimgarea" id="divAdvDate" runat="server" visible="false">
                                                                <span class="name">
                                                                    <label class="control-label">Advance Pay Date</label>
                                                                </span><span class="dateimg">
                                                                    <asp:TextBox ID="txtAdvanceDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                                    <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                    <cc1:CalendarExtender ID="CalendarExtender5" runat="server" PopupButtonID="ImageButton2"
                                                                        TargetControlID="txtAdvanceDate" Format="dd/MM/yyyy">
                                                                    </cc1:CalendarExtender>
                                                                    <asp:RegularExpressionValidator ValidationGroup="elecinv" ControlToValidate="txtAdvanceDate" ID="RegularExpressionValidator13" runat="server" ErrorMessage="Enter valid date"
                                                                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>--%>
                                                                <div class="form-group ">
                                                                    <span class="name">
                                                                        <label class="control-label">Sales Invoice No</label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtSalesInvNo" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group  dateimgarea">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">Invoice Date</label>
                                                                    </span><span class="dateimg">
                                                                        <asp:TextBox ID="txtSalesInvDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                                        <asp:ImageButton ID="Image3" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                        <cc1:CalendarExtender ID="CalendarExtender4" runat="server" PopupButtonID="Image3"
                                                                            TargetControlID="txtSalesInvDate" Format="dd/MM/yyyy">
                                                                        </cc1:CalendarExtender>
                                                                        <asp:RegularExpressionValidator ValidationGroup="elecinv" ControlToValidate="txtSalesInvDate" ID="RegularExpressionValidator12" runat="server" ErrorMessage="Enter valid date"
                                                                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <label class="control-label">Notes </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtSalesPayNotes" runat="server" TextMode="MultiLine"
                                                                            Height="50px" CssClass="form-control"></asp:TextBox>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group dateimgarea">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">Invoice Pay Date</label>
                                                                    </span><span class="dateimg">
                                                                        <asp:TextBox ID="txtSalesPayDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                                        <asp:ImageButton ID="Image1" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                        <cc1:CalendarExtender ID="CalendarExtender1" runat="server" PopupButtonID="Image1"
                                                                            TargetControlID="txtSalesPayDate" Format="dd/MM/yyyy">
                                                                        </cc1:CalendarExtender>
                                                                        <asp:RegularExpressionValidator ValidationGroup="elecinv" ControlToValidate="txtSalesPayDate" ID="RegularExpressionValidator2" runat="server" ErrorMessage="Enter valid date"
                                                                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <label class="control-label">Upload Inv Doc </label>
                                                                    </span><span>
                                                                        <asp:HyperLink ID="lblInvDocDoor" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                        <asp:FileUpload ID="fuInvDocDoor" runat="server" />
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <label class="control-label"></label>
                                                                    </span><span>
                                                                        <%--<asp:Button ID="btnCreateTaxInvoice" runat="server" OnClick="btnCreateTaxInvoice_Click"
                                                                        Text="Create Tax Invoice" CausesValidation="false" Visible="false" />
                                                                <asp:Button ID="btnOpenTaxInvoice" runat="server" OnClick="btnOpenTaxInvoice_Click"
                                                                    Text="Open Tax Invoice" CausesValidation="false" />--%></span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row" id="divInspMtce" runat="server" visible="false">
                                                            <div class="col-md-4">
                                                                <div class="form-group wdth230">
                                                                    <span class="name">
                                                                        <label class="control-label">Inv No </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtInvNo" runat="server" MaxLength="50" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="This value is required."
                                                                            SetFocusOnError="true" ControlToValidate="txtInvNo" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group wdth230" id="divAdvAmount" runat="server">
                                                                    <%--   <span class="control-label">--%>
                                                                    <label class="control-label">Advance Amount</label>
                                                                    <%-- </span>--%>
                                                                    <span>
                                                                        <asp:TextBox ID="txtAdvanceAmount" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtAdvanceAmount_TextChanged"></asp:TextBox>
                                                                        <asp:Label ID="lblAdvanceAmount" runat="server"></asp:Label>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator11" runat="server" ControlToValidate="txtAdvanceAmount"
                                                                            ValidationGroup="elecinv" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                                        <%--<asp:CompareValidator ID="CompareValidatorAdvance" Type="Double" runat="server" ErrorMessage="Invalid Amount"
                                                                        ControlToValidate="txtAdvanceAmount" Operator="LessThanEqual"
                                                                        Display="Dynamic" ValidationGroup="elecinv"></asp:CompareValidator>--%>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <%--<div class="form-group dateimgarea" id="divAdvDate" runat="server">
                                                                <span class="name">
                                                                    <label class="control-label">Advance Pay Date</label>
                                                                </span><span class="dateimg">
                                                                    <asp:TextBox ID="txtAdvanceDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                                    <asp:ImageButton ID="ImageButton2" runat="server" CausesValidation="false" ImageUrl="~/admin/images/Calendar_scheduleHS.png" />
                                                                    <cc1:CalendarExtender ID="CalendarExtender5" runat="server" PopupButtonID="ImageButton2"
                                                                        TargetControlID="txtAdvanceDate" Format="dd/MM/yyyy">
                                                                    </cc1:CalendarExtender>
                                                                 
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>--%>


                                                                <div class="form-group wdth230" id="divAdvDate" runat="server">
                                                                    <asp:Label ID="Label4" runat="server" class="control-label">
                                               Advance Pay Date</asp:Label>
                                                                    <div class="">
                                                                        <div class="input-group date datetimepicker1 ">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtAdvanceDate" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                            <%-- <asp:RegularExpressionValidator ValidationGroup="elecinv" ControlToValidate="txtAdvanceDate" ID="RegularExpressionValidator13" runat="server" ErrorMessage="Enter valid date"
                                                                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>--%>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group wdth230">
                                                                    <span class="name disblock">
                                                                        <label class="control-label">
                                                                            Installer
                                                                        </label>
                                                                    </span><span>
                                                                        <asp:DropDownList ID="ddlInstallerMtce" runat="server" AppendDataBoundItems="true"
                                                                            aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Installer</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </span>
                                                                    <div class="clear">
                                                                    </div>
                                                                </div>

                                                                <div class="form-group wdth230" id="divpanelcost" runat="server" visible="false">
                                                                    <%--   <span class="control-label">--%>
                                                                    <label class="control-label">Panel/Cost</label>
                                                                    <%-- </span>--%>
                                                                    <span>
                                                                        <asp:TextBox ID="txtpanelcost" Text="0" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:Label ID="Label6" runat="server"></asp:Label>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server" ControlToValidate="txtpanelcost"
                                                                            ValidationGroup="elecinv" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                                        <%--<asp:CompareValidator ID="CompareValidatorAdvance" Type="Double" runat="server" ErrorMessage="Invalid Amount"
                                                                        ControlToValidate="txtAdvanceAmount" Operator="LessThanEqual"
                                                                        Display="Dynamic" ValidationGroup="elecinv"></asp:CompareValidator>--%>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="form-group wdth230">
                                                                    <span class="name">
                                                                        <label class="control-label">Inv Amnt </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtInvAmnt" runat="server" MaxLength="10" CssClass="form-control"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server"
                                                                            ControlToValidate="txtInvAmnt" Display="Dynamic"
                                                                            ErrorMessage="Please enter a number" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="This value is required."
                                                                            SetFocusOnError="true" ControlToValidate="txtInvAmnt" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group dateimgarea wdth230" id="div2" runat="server">
                                                                    <span class="name">
                                                                        <label class="control-label">Less Deducted Amount</label>
                                                                    </span><span class="dateimg">
                                                                        <asp:TextBox ID="txtLessDeductedAmount" runat="server" CssClass="form-control" MaxLength="10" AutoPostBack="true" OnTextChanged="txtLessDeductedAmount_TextChanged"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server" ControlToValidate="txtLessDeductedAmount"
                                                                            ValidationGroup="price" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                                        <asp:RangeValidator ID="rangelessamount" ValidationGroup="elecinv"
                                                                            runat="server" ControlToValidate="txtLessDeductedAmount"
                                                                            Type="Double"></asp:RangeValidator>
                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server"
                                                                            Enabled="True" FilterType="Numbers,Custom" TargetControlID="txtLessDeductedAmount" ValidChars=".">
                                                                        </cc1:FilteredTextBoxExtender>

                                                                    </span>
                                                                    <%-- <asp:LinkButton ID="lnkprodetail" runat="server" CausesValidation="false" OnClick="lnkprodetail_Click" Visible="false" CssClass="btn btn-info">Detail</asp:LinkButton>--%>
                                                                    <div class="clear"></div>
                                                                </div>

                                                                <div class="form-group wdth230">
                                                                    <asp:Label ID="Label5" runat="server" class="  control-label">
                                             Pay Date</asp:Label>
                                                                    <div class="">
                                                                        <div class="input-group date datetimepicker1 ">
                                                                            <span class="input-group-addon">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtPayDate" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="This value is required."
                                                                                SetFocusOnError="true" ControlToValidate="txtPayDate" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                            <%--     <asp:RegularExpressionValidator ControlToValidate="txtPayDate" ID="RegularExpressionValidator10" runat="server" ErrorMessage="Enter valid date"
                                                                        ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>--%>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <%--<div class="form-group dateimgarea">
                                                                <span class="name">
                                                                    <label class="control-label">Pay Date </label>
                                                                </span><span class="dateimg">
                                                                    <asp:TextBox ID="txtPayDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                                    <asp:ImageButton ID="Image2" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" CausesValidation="false" />
                                                                    <cc1:CalendarExtender ID="CalendarExtender3" runat="server" PopupButtonID="Image2"
                                                                        TargetControlID="txtPayDate" Format="dd/MM/yyyy">
                                                                    </cc1:CalendarExtender>
                                                                  
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>--%>
                                                                <div class="form-group wdth230">
                                                                    <span class="name">
                                                                        <label class="control-label">Upload Inv Doc </label>
                                                                    </span><span>
                                                                        <asp:FileUpload ID="fuInvDocCom" runat="server" />
                                                                        <asp:HyperLink ID="lblInvDocCom" runat="server" Target="_blank" Visible="false"></asp:HyperLink>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group" id="divflatjob" runat="server" visible="false">
                                                                    <%--   <span class="control-label">--%>
                                                                    <label class="control-label">Flate Job</label>
                                                                    <%-- </span>--%>
                                                                    <span>
                                                                        <asp:TextBox ID="txtflatejob" Text="0" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:Label ID="Label7" runat="server"></asp:Label>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server" ControlToValidate="txtflatejob"
                                                                            ValidationGroup="elecinv" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                                        <%--<asp:CompareValidator ID="CompareValidatorAdvance" Type="Double" runat="server" ErrorMessage="Invalid Amount"
                                                                        ControlToValidate="txtAdvanceAmount" Operator="LessThanEqual"
                                                                        Display="Dynamic" ValidationGroup="elecinv"></asp:CompareValidator>--%>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">

                                                                <div class="form-group wdth230">
                                                                    <asp:Label ID="Label3" runat="server" class="  control-label">
                                                Inv Date</asp:Label>
                                                                    <div class="">
                                                                        <div class="input-group date datetimepicker1 ">
                                                                            <span class="input-group-addon ">
                                                                                <span class="fa fa-calendar"></span>
                                                                            </span>
                                                                            <asp:TextBox ID="txtInvDate" runat="server" class="form-control">
                                                                            </asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="This value is required."
                                                                                SetFocusOnError="true" ControlToValidate="txtInvDate" Display="Dynamic"></asp:RequiredFieldValidator>
                                                                            <%-- <asp:RegularExpressionValidator ControlToValidate="txtInvDate" ID="RegularExpressionValidator9" runat="server" ErrorMessage="Enter valid date"
                                                                            ValidationExpression="^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d+$"></asp:RegularExpressionValidator>--%>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <%--<div class="form-group dateimgarea">
                                                                <span class="name">
                                                                    <label class="control-label">Inv Date </label>
                                                                </span><span class="dateimg">
                                                                    <asp:TextBox ID="txtInvDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/admin/images/Calendar_scheduleHS.png" CausesValidation="false" />
                                                                    <cc1:CalendarExtender ID="CalendarExtender2" runat="server" PopupButtonID="ImageButton1"
                                                                        TargetControlID="txtInvDate" Format="dd/MM/yyyy">
                                                                    </cc1:CalendarExtender>
                                                                  
                                                                </span>
                                                                <div class="clear"></div>
                                                            </div>--%>
                                                                <div class="form-group wdth230 dateimgarea" id="div3" runat="server">
                                                                    <span class="name">
                                                                        <label class="control-label">Total Amount</label>
                                                                    </span><span class="dateimg">
                                                                        <asp:TextBox ID="txtTotalAmount" runat="server" CssClass="form-control" MaxLength="10" Enabled="false"></asp:TextBox>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server" ControlToValidate="txtTotalAmount"
                                                                            ValidationGroup="price" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>

                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                                                            Enabled="True" FilterType="Numbers,Custom" TargetControlID="txtTotalAmount" ValidChars=".">
                                                                        </cc1:FilteredTextBoxExtender>

                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group wdth230 dateimgarea" id="div4" runat="server">
                                                                    <span class="name">
                                                                        <label class="control-label">Type</label>
                                                                    </span><span>
                                                                        <asp:DropDownList ID="ddlAdvType" runat="server" AppendDataBoundItems="true" aria-controls="DataTables_Table_0" class="myval">
                                                                            <asp:ListItem Value="">Select</asp:ListItem>
                                                                            <asp:ListItem Value="1">Cash</asp:ListItem>
                                                                            <asp:ListItem Value="2">Finance</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <span class="name">
                                                                        <label class="control-label">Notes </label>
                                                                    </span><span>
                                                                        <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine"
                                                                            Height="50px" CssClass="form-control"></asp:TextBox>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="form-group wdth230" id="divpanelno" runat="server" visible="false">
                                                                    <%--   <span class="control-label">--%>
                                                                    <label class="control-label">Panel No</label>
                                                                    <%-- </span>--%>
                                                                    <span>
                                                                        <asp:TextBox ID="txtpanelno" Text="0" runat="server" CssClass="form-control"></asp:TextBox>
                                                                        <asp:Label ID="Label8" runat="server"></asp:Label>
                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server" ControlToValidate="txtpanelno"
                                                                            ValidationGroup="elecinv" Display="Dynamic" ErrorMessage="Number Only" ValidationExpression="^\d*\.?\d+$"></asp:RegularExpressionValidator>
                                                                        <%--<asp:CompareValidator ID="CompareValidatorAdvance" Type="Double" runat="server" ErrorMessage="Invalid Amount"
                                                                        ControlToValidate="txtAdvanceAmount" Operator="LessThanEqual"
                                                                        Display="Dynamic" ValidationGroup="elecinv"></asp:CompareValidator>--%>
                                                                    </span>
                                                                    <div class="clear"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                    </div>
                                </div>
                                <div class="row">&nbsp; </div>
                                <div class="row" id="divAddUpdate" runat="server">
                                    <div class="col-md-12 text-center">
                                        <asp:Button CssClass="btn btn-primary savewhiteicon btnsaveicon" ID="btnUpdateElecInv" runat="server" OnClick="btnUpdateElecInv_Click"
                                            Text="Save" ValidationGroup="elecinv" />
                                    </div>
                                </div>
                                <div class="row">&nbsp; </div>
                              
                                    <div class="row" id="grdInvoice" runat="server" visible="false" >
                                        <div class="content padtopzero marbtm50 finalgrid" id="divgrid" runat="server">

                                            <div style="margin-left: 20px;margin-right:20px; overflow:auto">
                                                <asp:GridView ID="GridView1" DataKeyNames="InvoiceID" runat="server" OnRowDataBound="GridView1_RowDataBound"
                                                    OnSelectedIndexChanging="GridView1_SelectedIndexChanging" OnRowDeleting="GridView1_RowDeleting"
                                                    PagerStyle-CssClass="gridpagination" CssClass="table table-bordered table-hover" AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="InvType" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" HeaderStyle-Width="80px">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hndInvType" runat="server" Value='<%# Eval("InvType") %>' />
                                                                <%#Eval("InvTypeName")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="InvNo" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="110px" HeaderStyle-Width="110px">
                                                            <ItemTemplate>
                                                                <%#Eval("InvNo")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="InvAmnt" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" HeaderStyle-Width="10px">
                                                            <ItemTemplate>
                                                                <%#Eval("InvAmnt","{0:0.00}")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="InvDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px" HeaderStyle-Width="100px">
                                                            <ItemTemplate>
                                                                <%#Eval("InvDate","{0:dd-MMM-yyyy}")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="PayDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px" HeaderStyle-Width="100px">
                                                            <ItemTemplate>
                                                                <%#Eval("PayDate","{0:dd-MMM-yyyy}")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="AdvAmnt" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" HeaderStyle-Width="100px">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hndadvamt" runat="server" Value='<%# Eval("AdvanceAmount") %>' />
                                                                <asp:HiddenField ID="hndadvancedate" runat="server" Value='<%# Eval("AdvanceDate") %>' />
                                                                <%#Eval("AdvanceAmount","{0:0.00}")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Less Deduct Amt" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="35px" HeaderStyle-Width="35px">
                                                            <ItemTemplate>
                                                                <%#Eval("AdvanceLessAmount","{0:0.00}")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Adv Total Amt" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="80px" HeaderStyle-Width="80px">
                                                            <ItemTemplate>

                                                                <%#Eval("AdvanceTotalAmount","{0:0.00}")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="AdvDate" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" HeaderStyle-Width="80px">
                                                            <ItemTemplate>
                                                                <%#Eval("AdvanceDate","{0:dd-MMM-yyyy}")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Adv Type" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="100px" HeaderStyle-Width="100px">
                                                            <ItemTemplate>
                                                                <asp:Label runat="server" ID="lbltype1" Text="Cash" Visible='<%#Eval("AdvanceType").ToString() =="1"?true:false%>'></asp:Label>
                                                                <asp:Label runat="server" ID="Label2" Text="Finance" Visible='<%#Eval("AdvanceType").ToString() =="2"?true:false%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Document" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="70px" HeaderStyle-Width="70px">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hndInvoiceID" runat="server" Value='<%#Eval("InvoiceID") %>' />
                                                                <asp:Label ID="Label1" Text="-" runat="server" Visible='<%#Eval("InvDoc").ToString()!=""?false:true %>'></asp:Label>
                                                                <asp:HyperLink ID="hypdocument" runat="server" Target="_blank" Visible='<%#Eval("InvDoc").ToString()!=""?true:false %>'>
                                                                    <asp:Image ID="imgDwn" runat="server" />
                                                                </asp:HyperLink>
                                                                <%--<a id="hypdocument" target="_blank" href='<%#"~/userfiles/commoninvdoc/"+Eval("InvDoc") %>'
                                                runat="server" visible='<%#Eval("InvDoc").ToString()!=""?true:false %>'>
                                                <img src="~/../../images/icon_dwn.jpg" alt="" title="" /></a>--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Notes" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Left"
                                                            ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <%#Eval("Notes").ToString() == "" ? "-" : Eval("Notes")%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="" ItemStyle-VerticalAlign="Top" HeaderStyle-HorizontalAlign="Center"
                                                            ItemStyle-HorizontalAlign="Center" ItemStyle-Width="35px" HeaderStyle-Width="35px">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="gvbtnUpdate" runat="server" CommandName="Select" CssClass="btn btn-info btn-xs"
                                                                    CausesValidation="false"><i class="fa fa-edit"></i> Edit</asp:LinkButton>

                                                                <asp:LinkButton ID="gvbtnDelete" runat="server" CssClass="btn btn-danger btn-xs"
                                                                    CausesValidation="false" CommandName="Delete" CommandArgument='<%#Eval("InvoiceID")%>'><i class="fa fa-trash"></i> Delete</asp:LinkButton>


                                                            </ItemTemplate>

                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>

                                    </div>

                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </section>

        <!--Danger Modal Templates-->
        <asp:Button ID="btndelete" Style="display: none;" runat="server" />
        <cc1:ModalPopupExtender ID="ModalPopupExtenderDelete" runat="server" BackgroundCssClass="modalbackground"
            PopupControlID="modal_danger" DropShadow="false" CancelControlID="lnkcancel" OkControlID="btnOKMobile" TargetControlID="btndelete">
        </cc1:ModalPopupExtender>
        <div id="modal_danger" runat="server" style="display: none" class="modal_popup modal-danger modal-message ">

            <div class="modal-dialog " style="margin-top: -300px">
                <div class=" modal-content ">
                    <%--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">close</button>--%>
                    <div class="modal-header text-center">
                        <i class="glyphicon glyphicon-fire"></i>
                    </div>


                    <div class="modal-title">Delete</div>
                    <label id="ghh" runat="server"></label>
                    <div class="modal-body ">Are You Sure Delete This Entry?</div>
                    <div class="modal-footer " style="text-align: center">
                        <asp:LinkButton ID="lnkdelete" runat="server" OnClick="lnkdelete_Click" class="btn btn-danger" CommandName="deleteRow">OK</asp:LinkButton>
                        <asp:LinkButton ID="lnkcancel" runat="server" class="btn" data-dismiss="modal">Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>

        </div>

        <asp:HiddenField ID="hdndelete" runat="server" />
        <asp:HiddenField ID="hdndelete1" runat="server" />
        <!--End Danger Modal Templates-->


        <cc1:ModalPopupExtender ID="ModalPopupExtenderprojectdetail" runat="server" BackgroundCssClass="modalbackground statuspopup"
            CancelControlID="ibtnCancelStatus" DropShadow="false" PopupControlID="divprojectdetail"
            OkControlID="btnOKStatus" TargetControlID="btnNULLStatus">
        </cc1:ModalPopupExtender>
        <div id="divprojectdetail" runat="server" style="display: none;">
            <div class="modal-dialog" style="width: 350px;">
                <div class="modal-content">
                    <div class="color-line"></div>
                    <div class="modal-header">
                        <div style="float: right">
                            <asp:LinkButton ID="ibtnCancelStatus" CausesValidation="false" runat="server" class="btn btn-danger btncancelicon" data-dismiss="modal">
                  Close
                            </asp:LinkButton>
                        </div>
                        <h4 class="modal-title" id="myModalLabel">
                            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                            Project Detail</h4>
                    </div>
                    <div class="modal-body paddnone">
                        <div class="panel-body">
                            <div class="formainline">
                                <div class="form-group">
                                    <label class="control-label">
                                        Less Deducted Amount:
                                    </label>
                                    <asp:TextBox ID="txtdeductamount" runat="server" Width="200px" Enabled="false" CssClass="form-control"></asp:TextBox>

                                </div>

                                <div class="form-group">
                                    <label class="control-label">
                                        Project No:
                                    </label>
                                    <asp:TextBox ID="txtprojno" runat="server" Width="200px" Enabled="true" CssClass="form-control"></asp:TextBox>

                                </div>

                                <div class="form-group">
                                    <label class="control-label">
                                        Amount:
                                    </label>
                                    <asp:TextBox ID="txtproamount" runat="server" Width="200px" CssClass="form-control"></asp:TextBox>
                                    <asp:RangeValidator ID="cmpNumbers" ValidationGroup="save" class="datecmp"
                                        runat="server" ControlToValidate="txtproamount" MinimumValue="0" MaximumValue="1000000000000000000"
                                        Type="Double" ErrorMessage="The Amount should be smaller than the Less Deducted Amount !"></asp:RangeValidator>
                                    <asp:TextBox runat="server" ID="txtamu" Visible="false"></asp:TextBox>
                                    <%--<asp:CompareValidator runat="server" ID="cmpNumbers" ControlToValidate="txtproamount" ValidationGroup="save" ControlToCompare="txtamu" Operator="LessThan" Type="Double" ErrorMessage="The Amount should be smaller than the Less Deducted Amount !" />--%>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="This value is required." CssClass="reqerror"
                                        ControlToValidate="txtproamount" Display="Dynamic" ValidationGroup="save"></asp:RequiredFieldValidator>




                                </div>


                                <div class="form-group marginleft center-text">
                                    <asp:Button ID="btnsavedetail" runat="server" Text="Update" OnClick="btnsavedetail_Click"
                                        class="btn btn-primary savewhiteicon btnsaveicon" ValidationGroup="save" />
                                    <asp:Button ID="btnOKStatus" Style="display: none; visible: false;" runat="server"
                                        CssClass="btn" Text=" OK " />
                                </div>
                            </div>

                            <div class="table-responsive">
                                <div style="margin-left: 15px; margin-right: 15px;" class="tableblack tableminpadd">
                                    <asp:GridView ID="rptName" DataKeyNames="id" runat="server" ShowFooter="true" OnSelectedIndexChanging="rptName_SelectedIndexChanging"
                                        OnRowDeleting="rptName_RowDeleting" PagerStyle-CssClass="gridpagination" AutoGenerateColumns="false" CssClass="tooltip-demo text-center table table-striped table-bordered table-hover" PagerStyle-HorizontalAlign="Right">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Project No">
                                                <ItemTemplate><%# Eval("projectno")%> </ItemTemplate>
                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate><%# Eval("amount")%> </ItemTemplate>
                                                <ItemStyle Width="150px" HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="" HeaderStyle-CssClass="center-text">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label11" runat="server" Width="20px" CssClass="btnicondelete">
                                                        <asp:LinkButton ID="gvbtn_Update" runat="server" CommandName="Select" CausesValidation="false" CssClass="btn btn-primary btn-xs"
                                                            data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit">
                                                            <i class="fa fa-edit"></i> Edit
                                                        </asp:LinkButton>

                                                        <asp:LinkButton ID="gvbtnDelete" runat="server" CommandName="Delete" CommandArgument='<%#Eval("id")%>'><i class="fa fa-trash"></i> Delete</asp:LinkButton>


                                                    </asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" CssClass="verticaaline" />
                                            </asp:TemplateField>

                                        </Columns>
                                        <HeaderStyle CssClass="Title GridviewScrollHeader" />
                                        <PagerStyle CssClass="paginationmargin paginationnew GridviewScrollPager" />
                                        <EmptyDataRowStyle Font-Bold="True" />
                                        <RowStyle CssClass="GridviewScrollItem" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:Button ID="btnNULLStatus" Style="display: none;" runat="server" />
        <asp:HiddenField runat="server" ID="hdnGrid" />
        <asp:HiddenField runat="server" ID="hdntotalamount" />
        <asp:HiddenField runat="server" ID="hdndeductamount" />
        <asp:HiddenField runat="server" ID="hdnamountsum" />
        <asp:HiddenField runat="server" ID="hdnproamount" />

    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnUpdateElecInv" />
        <asp:PostBackTrigger ControlID="btnsavedetail" />
        <%--<asp:PostBackTrigger ControlID="btnOpenTaxInvoice" />--%>
    </Triggers>
</asp:UpdatePanel>


