﻿using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Web.UI;

public partial class includes_controls_projectmtce : System.Web.UI.UserControl
{
    protected string pagecontent;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Profile.eurosolar.maintenanceid = "";
            if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
            {
                string ProjectID = Request.QueryString["proid"];
                if (!string.IsNullOrEmpty(ProjectID))
                {
                    SttblProjects st = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
                    if ((Roles.IsUserInRole("Sales Manager")))
                    {

                        if (st.ProjectStatusID == "3")
                        {
                            btnAdd.Visible = false;
                            btnUpdate.Visible = false;
                            btnReset.Visible = false;
                            btnCancel.Visible = false;
                        }
                    }
                    if (Roles.IsUserInRole("SalesRep"))
                    {
                        if (st.DepositReceived != string.Empty)
                        {
                            btnAdd.Visible = false;
                            btnUpdate.Visible = false;
                            btnReset.Visible = false;
                            btnCancel.Visible = false;
                        }
                    }
                }
            }
            //if(!IsPostBack)
            //{
            //    BindProjectMtce();
            //}            
        }
    }

    public void BindProjectMtce()
    {
        HidePanels();
        if ((Roles.IsUserInRole("PreInstaller")) || (Roles.IsUserInRole("Maintenance")) || (Roles.IsUserInRole("Administrator")) || (Roles.IsUserInRole("Installation Manager")))
        {
            divrightbtn.Visible = true;
        }
        else
        {
            divrightbtn.Visible = false;
        }

        ddlSelectRecords.DataSource = SiteConfiguration.GetArray().Split(',');
        ddlSelectRecords.DataBind();

        //if (Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("DSales Manager"))
        //{
        //    string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        //    SttblEmployees stEmpC = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        //    string CEmpType = stEmpC.EmpType;
        //    string CSalesTeamID = stEmpC.SalesTeamID;

        //    if (Request.QueryString["proid"] != string.Empty)
        //    {
        //        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
        //        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByEmployeeID(stPro.EmployeeID);
        //        string EmpType = stEmp.EmpType;
        //        string SalesTeamID = stEmp.SalesTeamID;

        //        if (CEmpType == EmpType && CSalesTeamID == SalesTeamID)
        //        {
        //            PanAddUpdate.Enabled = true;
        //        }
        //        else
        //        {
        //            PanAddUpdate.Enabled = false;
        //        }
        //    }
        //}
        BindDropDown();
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {

            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            string CustomerID = stPro.CustomerID;
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(CustomerID);
            lblCustomer.Text = stCust.Customer;
            lblProject.Text = stPro.Project;
            DataTable dtContact = ClstblContacts.tblContacts_SelectTop1ByCustId(CustomerID);
            if (dtContact.Rows.Count > 0)
            {
                lblContact.Text = dtContact.Rows[0]["ContFirst"].ToString() + " " + dtContact.Rows[0]["ContLast"].ToString();
                lblMobile.Text = dtContact.Rows[0]["ContMobile"].ToString();
            }
            try
            {
                lblInstallDate.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(stPro.InstallBookingDate));
            }
            catch { }

            lblSalesRep.Text = stPro.SalesRepName;
            if (stPro.Installer != string.Empty)
            {
                lblInstaller.Text = stPro.InstallerName;
            }
            else
            {
                lblInstaller.Text = "N/A";
            }
            if (stPro.Electrician != string.Empty)
            {
                lblMeterElec.Text = stPro.ElectricianName;
            }
            else
            {
                lblMeterElec.Text = "N/A";
            }
            if (stPro.PanelBrandID != string.Empty && stPro.InverterDetailsID != string.Empty)
            {
                lblSystem.Text = stPro.SystemDetails;
            }

            BindGrid(0);
        }
    }
    public void BindDropDown()
    {
        ListItem item1 = new ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlProjectMtceReasonID.Items.Clear();
        ddlProjectMtceReasonID.Items.Add(item1);

        ddlProjectMtceReasonID.DataSource = ClstblProjectMtceReason.tblProjectMtceReason_SelectASC();
        ddlProjectMtceReasonID.DataValueField = "ProjectMtceReasonID";
        ddlProjectMtceReasonID.DataMember = "ProjectMtceReason";
        ddlProjectMtceReasonID.DataTextField = "ProjectMtceReason";
        ddlProjectMtceReasonID.DataBind();

        ddlProjectMtceReasonID.SelectedValue = "2";

        ListItem item2 = new ListItem();
        item2.Text = "Select";
        item2.Value = "";
        ddlProjectMtceReasonSubID.Items.Clear();
        ddlProjectMtceReasonSubID.Items.Add(item2);

        ddlProjectMtceReasonSubID.DataSource = ClstblProjectMtceReasonSub.tblProjectMtceReasonSub_SelectASC();
        ddlProjectMtceReasonSubID.DataValueField = "ProjectMtceReasonSubID";
        ddlProjectMtceReasonSubID.DataMember = "ProjectMtceReasonSub";
        ddlProjectMtceReasonSubID.DataTextField = "ProjectMtceReasonSub";
        ddlProjectMtceReasonSubID.DataBind();

        ddlProjectMtceReasonSubID.SelectedValue = "2";

        ListItem item3 = new ListItem();
        item3.Text = "Select";
        item3.Value = "";
        ddlEmployee.Items.Clear();
        ddlEmployee.Items.Add(item3);

        ddlEmployee.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlEmployee.DataValueField = "EmployeeID";
        ddlEmployee.DataMember = "fullname";
        ddlEmployee.DataTextField = "fullname";
        ddlEmployee.DataBind();

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        ddlEmployee.SelectedValue = stEmp.EmployeeID;

        ListItem item4 = new ListItem();
        item4.Text = "Select";
        item4.Value = "";
        ddlInstallerAssigned.Items.Clear();
        ddlInstallerAssigned.Items.Add(item4);

        ddlInstallerAssigned.DataSource = ClstblContacts.tblContacts_SelectInverter();
        ddlInstallerAssigned.DataValueField = "ContactID";
        ddlInstallerAssigned.DataMember = "Contact";
        ddlInstallerAssigned.DataTextField = "Contact";
        ddlInstallerAssigned.DataBind();

        ListItem item5 = new ListItem();
        item5.Text = "Select";
        item5.Value = "";
        ddlProjectMtceCallID.Items.Clear();
        ddlProjectMtceCallID.Items.Add(item5);

        ddlProjectMtceCallID.DataSource = ClstblProjects.tblProjectMtceCall_SelectASC();
        ddlProjectMtceCallID.DataValueField = "ProjectMtceCallID";
        ddlProjectMtceCallID.DataMember = "ProjectMtceCall";
        ddlProjectMtceCallID.DataTextField = "ProjectMtceCall";
        ddlProjectMtceCallID.DataBind();

        ddlProjectMtceCallID.SelectedValue = "4";

        ListItem item6 = new ListItem();
        item6.Text = "Select";
        item6.Value = "";
        ddlProjectMtceStatusID.Items.Clear();
        ddlProjectMtceStatusID.Items.Add(item6);

        ddlProjectMtceStatusID.DataSource = ClstblProjects.tblProjectMtceStatus_SelectASC();
        ddlProjectMtceStatusID.DataValueField = "ProjectMtceStatusID";
        ddlProjectMtceStatusID.DataMember = "ProjectMtceStatus";
        ddlProjectMtceStatusID.DataTextField = "ProjectMtceStatus";
        ddlProjectMtceStatusID.DataBind();

        ddlProjectMtceStatusID.SelectedValue = "2";

        ListItem item7 = new ListItem();
        item7.Text = "Select";
        item7.Value = "";
        ddlFPTransTypeID.Items.Clear();
        ddlFPTransTypeID.Items.Add(item7);

        ddlFPTransTypeID.DataSource = ClstblFPTransType.tblFPTransType_SelectActive();
        ddlFPTransTypeID.DataValueField = "FPTransTypeID";
        ddlFPTransTypeID.DataMember = "FPTransType";
        ddlFPTransTypeID.DataTextField = "FPTransType";
        ddlFPTransTypeID.DataBind();

        ListItem item8 = new ListItem();
        item8.Text = "Select";
        item8.Value = "";
        ddlMtceRecBy.Items.Clear();
        ddlMtceRecBy.Items.Add(item8);

        ddlMtceRecBy.DataSource = ClstblEmployees.tblEmployees_SelectASC();
        ddlMtceRecBy.DataValueField = "EmployeeID";
        ddlMtceRecBy.DataMember = "EmpNicName";
        ddlMtceRecBy.DataTextField = "EmpNicName";
        ddlMtceRecBy.DataBind();
    }

    protected DataTable GetGridData()
    {
        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        DataTable dt = ClstblProjectMaintenance.tblProjectMaintenance_SelectByProjectID(Request.QueryString["proid"]);
        return dt;
    }

    public void BindGrid(int deleteFlag)
    {
        PanGrid.Visible = true;

        DataTable dt = GetGridData();
        if (dt.Rows.Count == 0)
        {
            SetNoRecords();
            //PanNoRecord.Visible = true;
            PanGrid.Visible = false;
        }
        else
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            //PanNoRecord.Visible = false;
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        string ProjectID = Request.QueryString["proid"];

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = stEmp.EmployeeID;

        string ProjectMtceReasonID = ddlProjectMtceReasonID.SelectedValue;
        string ProjectMtceReasonSubID = ddlProjectMtceReasonSubID.SelectedValue;
        string ProjectMtceCallID = ddlProjectMtceCallID.SelectedValue;
        string ProjectMtceStatusID = ddlProjectMtceStatusID.SelectedValue;
        string OpenDate = txtOpenDate.Text.Trim();
        string CustomerInput = txtCustomerInput.Text;
        string FaultIdentified = txtFaultIdentified.Text;
        string ActionRequired = txtActionRequired.Text;
        string PropResolutionDate = txtPropResolutionDate.Text.Trim();
        string CompletionDate = txtCompletionDate.Text.Trim();
        string WorkDone = txtWorkDone.Text;
        string CurrentPhoneContact = txtCurrentPhoneContact.Text;
        string Warranty = chkWarranty.Checked.ToString();
        string MtceCost = txtMtceCost.Text;
        string ServiceCost = txtServiceCost.Text;
        string MtceDiscount = txtMtceDiscount.Text;
        string MtceBalance = txtMtceBalance.Text;
        string FPTransTypeID = ddlFPTransTypeID.SelectedValue;
        string MtceRecBy = ddlMtceRecBy.SelectedValue;
        string InstallerID = ddlInstallerAssigned.SelectedValue;

        int success = ClstblProjectMaintenance.tblProjectMaintenance_Insert(ProjectID, EmployeeID, ProjectMtceReasonID, ProjectMtceReasonSubID, ProjectMtceCallID, ProjectMtceStatusID, OpenDate, CustomerInput, "", InstallerID, FaultIdentified, ActionRequired, PropResolutionDate, CompletionDate, WorkDone, CurrentPhoneContact, Warranty, "False", "", "", "", "", "", "", "", "", "", "", "", "", MtceCost, MtceDiscount, MtceBalance, FPTransTypeID, MtceRecBy, ServiceCost);
        //--- do not chage this code start------
        if (Convert.ToString(success) != "")
        {
            Reset();
            SetAdd();
        }
        else
        {
            SetError();
        }
        BindGrid(0);
        //--- do not chage this code end------
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string id1 = GridView1.SelectedDataKey.Value.ToString();
        string ProjectID = Request.QueryString["proid"];

        string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
        SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
        string EmployeeID = stEmp.EmployeeID;

        string ProjectMtceReasonID = ddlProjectMtceReasonID.SelectedValue;
        string ProjectMtceReasonSubID = ddlProjectMtceReasonSubID.SelectedValue;
        string ProjectMtceCallID = ddlProjectMtceCallID.SelectedValue;
        string ProjectMtceStatusID = ddlProjectMtceStatusID.SelectedValue;
        string FPTransTypeID = ddlFPTransTypeID.SelectedValue;
        string MtceRecBy = ddlMtceRecBy.SelectedValue;

        string OpenDate = txtOpenDate.Text;
        string CustomerInput = txtCustomerInput.Text;
        string FaultIdentified = txtFaultIdentified.Text;
        string ActionRequired = txtActionRequired.Text;
        string PropResolutionDate = txtPropResolutionDate.Text;
        string CompletionDate = txtCompletionDate.Text;
        string WorkDone = txtWorkDone.Text;
        string CurrentPhoneContact = txtCurrentPhoneContact.Text;
        string Warranty = chkWarranty.Checked.ToString();
        string MtceCost = txtMtceCost.Text;
        string ServiceCost = txtMtceCost.Text;
        string MtceDiscount = txtMtceDiscount.Text;
        string MtceBalance = txtMtceBalance.Text;
        string InstallerID = ddlInstallerAssigned.SelectedValue;

        bool success = ClstblProjectMaintenance.tblProjectMaintenance_Update(id1, ProjectID, EmployeeID, ProjectMtceReasonID, ProjectMtceReasonSubID, ProjectMtceCallID, ProjectMtceStatusID, OpenDate, CustomerInput, "", InstallerID, FaultIdentified, ActionRequired, PropResolutionDate, CompletionDate, WorkDone, CurrentPhoneContact, Warranty, "False", "", "", "", "", "", "", "", "", "", "", "", "", MtceCost, MtceDiscount, MtceBalance, FPTransTypeID, MtceRecBy, ServiceCost);
        //--- do not chage this code Start------
        if (success)
        {
            SetUpdate();
        }
        else
        {
            SetError();
        }
        BindGrid(0);
        //--- do not chage this code end------
    }
    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string id = GridView1.DataKeys[e.RowIndex].Value.ToString();
        hdndelete.Value = id;
        ModalPopupExtenderDelete.Show();
        BindGrid(0);

    }
    protected void GridView1_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {

        string id = GridView1.DataKeys[e.NewSelectedIndex].Value.ToString();

        SttblProjectMaintenance st = ClstblProjectMaintenance.tblProjectMaintenance_SelectByProjectMaintenanceID(id);
        try
        {
            ddlProjectMtceReasonID.SelectedValue = st.ProjectMtceReasonID;
        }
        catch
        {
        }
        try
        {
            ddlProjectMtceReasonSubID.SelectedValue = st.ProjectMtceReasonSubID;
        }
        catch { }
        try
        {
            ddlProjectMtceCallID.SelectedValue = st.ProjectMtceCallID;
        }
        catch { }
        try
        {
            ddlProjectMtceStatusID.SelectedValue = st.ProjectMtceStatusID;
        }
        catch { }
        try
        {
            ddlInstallerAssigned.SelectedValue = st.InstallerID;
        }
        catch { }

        try
        {
            txtOpenDate.Text = Convert.ToDateTime(st.OpenDate).ToShortDateString();
        }
        catch { }

        try
        {
            txtPropResolutionDate.Text = Convert.ToDateTime(st.PropResolutionDate).ToShortDateString();
        }
        catch { }
        try
        {
            txtCompletionDate.Text = Convert.ToDateTime(st.CompletionDate).ToShortDateString();
        }
        catch { }


        txtCustomerInput.Text = st.CustomerInput;
        txtFaultIdentified.Text = st.FaultIdentified;
        txtActionRequired.Text = st.ActionRequired;
        txtWorkDone.Text = st.WorkDone;
        txtCurrentPhoneContact.Text = st.CurrentPhoneContact;
        chkWarranty.Checked = Convert.ToBoolean(st.Warranty);
        try
        {
            txtMtceCost.Text = SiteConfiguration.ChangeCurrencyVal(st.MtceCost);
        }
        catch { }
        try
        {
            txtServiceCost.Text = SiteConfiguration.ChangeCurrencyVal(st.ServiceCost);
        }
        catch { }

        try
        {
            txtMtceDiscount.Text = SiteConfiguration.ChangeCurrencyVal(st.MtceDiscount);
        }
        catch { }
        try
        {
            txtMtceBalance.Text = SiteConfiguration.ChangeCurrencyVal(st.MtceBalance);
        }
        catch { }

        ddlMtceRecBy.SelectedValue = st.MtceRecBy;
        ddlFPTransTypeID.SelectedValue = st.FPTransTypeID;
        makeEnable((Control)this);
        txtMtceBalance.Enabled = false;

        //--- do not chage this code start------
        InitUpdate();

        PanGrid.Visible = false;
        //--- do not chage this code end------
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGrid(0);
    }
    protected void ddlSelectRecords_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Convert.ToString(ddlSelectRecords.SelectedValue) == "All")
        {
            GridView1.AllowPaging = false;
            BindGrid(0);
        }
        else
        {
            GridView1.AllowPaging = true;
            GridView1.PageSize = Convert.ToInt32(ddlSelectRecords.SelectedValue);
            BindGrid(0);
        }
    }
    protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView1.EditIndex = -1;
        BindGrid(0);
    }

    protected void lnkBack_Click(object sender, EventArgs e)
    {
        SetCancel();
        PanAddUpdate.Visible = false;
        BindGrid(0);
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = GetGridData();

        ////////////// Don't Change Start
        string SortDir = string.Empty;
        if (dir == SortDirection.Ascending)
        {
            dir = SortDirection.Descending;
            SortDir = "Desc";
        }
        else
        {
            dir = SortDirection.Ascending;
            SortDir = "Asc";
        }
        DataView sortedView = new DataView(dt);
        sortedView.Sort = e.SortExpression + " " + SortDir;
        //////////////////////End

        GridView1.DataSource = sortedView;
        GridView1.DataBind();
    }
    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }
    }
    protected void lnkAdd_Click(object sender, EventArgs e)
    {

        divrightbtn.Visible = true;
        InitAdd();
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        Reset();
        lnkAdd.Visible = false;
        lnkBack.Visible = true;
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        lnkBack.Visible = false;
        Reset();
        PanAddUpdate.Visible = false;
        lnkAdd.Visible = true; ;
    }


    public void SetAdd1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunSuccess();", true);

    }

    public void SetError1()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunError();", true);
    }

    public void SetExist()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunWaring();", true);
    }

    public void SetNoRecords()
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "ntmtch", "MyfunInfo();", true);
    }
    public void SetAdd()
    {
        Reset();
        HidePanels();
        lnkBack.Visible = false;
        lnkAdd.Visible = true;
        SetAdd1();
        //PanSuccess.Visible = true;
        //PanAlreadExists.Visible = false;
    }
    public void SetUpdate()
    {
        Reset();
        HidePanels();
        SetAdd();
        //PanSuccess.Visible = true;
    }
    public void SetDelete()
    {
        Reset();
        HidePanels();
        SetAdd();
        //PanSuccess.Visible = true;
    }
    public void SetCancel()
    {
        Reset();
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        lnkBack.Visible = false;
        lnkmaintainance.Visible = false;
    }
    public void SetError()
    {
        Reset();
        HidePanels();
        SetError();
        //PanError.Visible = true;
    }
    public void InitAdd()
    {
        HidePanels();
        PanAddUpdate.Visible = true;
        lnkAdd.Visible = false;
        btnAdd.Visible = true;
        btnUpdate.Visible = false;
        btnReset.Visible = true;
        lnkBack.Visible = true;
      //  btnUpdate.Visible = true;
    }
    public void InitUpdate()
    {
        HidePanels();
        PanAddUpdate.Visible = true;
        btnAdd.Visible = false;
        btnUpdate.Visible = true;
        btnReset.Visible = false;
        divrightbtn.Visible = true;
        btnCopy.Visible = true;
        lnkBack.Visible = true;
        lnkAdd.Visible = false;
    }
    private void HidePanels()
    {
        //PanAlreadExists.Visible = false;
        //PanSuccess.Visible = false;
        //PanError.Visible = false;
        //PanNoRecord.Visible = false;
        PanAddUpdate.Visible = false;
        lnkBack.Visible = false;
    }
    public void Reset()
    {
        PanAddUpdate.Visible = true;
        txtOpenDate.Text = string.Empty; ;
        txtCustomerInput.Text = string.Empty; ;
        txtFaultIdentified.Text = string.Empty; ;
        txtActionRequired.Text = string.Empty; ;
        txtPropResolutionDate.Text = string.Empty; ;
        txtCompletionDate.Text = string.Empty; ;
        txtWorkDone.Text = string.Empty; ;
        txtCurrentPhoneContact.Text = string.Empty; ;
        chkWarranty.Checked = false; ;
        txtMtceCost.Text = string.Empty; ;
        txtMtceDiscount.Text = string.Empty; ;
        txtMtceBalance.Text = string.Empty; ;

        ddlFPTransTypeID.SelectedValue = "";
        ddlEmployee.SelectedValue = "";
        ddlInstallerAssigned.SelectedValue = "";
        ddlMtceRecBy.SelectedValue = "";
        ddlProjectMtceCallID.SelectedValue = "";
        ddlProjectMtceReasonID.SelectedValue = "";
        ddlProjectMtceReasonSubID.SelectedValue = "";
        ddlProjectMtceStatusID.SelectedValue = "";

        divrightbtn.Visible = true;
    }
    protected void txtMtceDiscount_TextChanged(object sender, EventArgs e)
    {
        PanAddUpdate.Visible = true;
        if (txtMtceDiscount.Text != string.Empty)
        {
            decimal balance = Convert.ToDecimal(txtMtceCost.Text) - Convert.ToDecimal(txtMtceDiscount.Text);
            txtMtceBalance.Text = Convert.ToString(balance);
        }
    }
    protected void btnCopy_Click(object sender, EventArgs e)
    {
        if (txtActionRequired.Text != string.Empty)
        {
            string ProjectID = Request.QueryString["proid"];

            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);
            string EmployeeID = stEmp.EmployeeID;

            string ProjectMtceReasonID = ddlProjectMtceReasonID.SelectedValue;
            string ProjectMtceReasonSubID = ddlProjectMtceReasonSubID.SelectedValue;
            string ProjectMtceCallID = ddlProjectMtceCallID.SelectedValue;
            string ProjectMtceStatusID = ddlProjectMtceStatusID.SelectedValue;
            string OpenDate = txtOpenDate.Text;
            string CustomerInput = txtActionRequired.Text;
            string FaultIdentified = txtFaultIdentified.Text;
            string ActionRequired = txtActionRequired.Text;
            string PropResolutionDate = txtPropResolutionDate.Text;
            string CompletionDate = txtCompletionDate.Text;
            string WorkDone = txtWorkDone.Text;
            string CurrentPhoneContact = txtCurrentPhoneContact.Text;
            string Warranty = chkWarranty.Checked.ToString();
            string MtceCost = txtMtceCost.Text;
            string ServiceCost = txtServiceCost.Text;
            string MtceDiscount = txtMtceDiscount.Text;
            string MtceBalance = txtMtceBalance.Text;
            string FPTransTypeID = ddlFPTransTypeID.SelectedValue;
            string MtceRecBy = ddlMtceRecBy.SelectedValue;

            int success = ClstblProjectMaintenance.tblProjectMaintenance_Insert(ProjectID, EmployeeID, ProjectMtceReasonID, ProjectMtceReasonSubID, ProjectMtceCallID, ProjectMtceStatusID, OpenDate, CustomerInput, "", "", FaultIdentified, ActionRequired, PropResolutionDate, CompletionDate, WorkDone, CurrentPhoneContact, Warranty, "False", "", "", "", "", "", "", "", "", "", "", "", "", MtceCost, MtceDiscount, MtceBalance, FPTransTypeID, MtceRecBy, ServiceCost);
            //--- do not chage this code Start------
            if (success > 0)
            {
                SetUpdate();
            }
            else
            {
                SetError();
            }
            BindGrid(0);
        }
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        lnkmaintainance.Visible = false;
        if (e.CommandName == "Detail")
        {
            ddlSelectRecords.Visible = false;
            //divMtceDetail.Visible = true;
            PanAddUpdate.Visible = true;
            string ProjectMaintenanceID = e.CommandArgument.ToString();
            string id = ProjectMaintenanceID;
            hdnmtceid.Text = id;
            SttblProjectMaintenance st = ClstblProjectMaintenance.tblProjectMaintenance_SelectByProjectMaintenanceID(id);
            try
            {
                ddlProjectMtceReasonID.SelectedValue = st.ProjectMtceReasonID;
            }
            catch
            {
            }
            try
            {
                ddlProjectMtceReasonSubID.SelectedValue = st.ProjectMtceReasonSubID;
            }
            catch { }
            try
            {
                ddlProjectMtceCallID.SelectedValue = st.ProjectMtceCallID;
            }
            catch { }
            try
            {
                ddlProjectMtceStatusID.SelectedValue = st.ProjectMtceStatusID;
            }
            catch { }
            try
            {
                ddlInstallerAssigned.SelectedValue = st.InstallerID;
            }
            catch { }

            try
            {
                txtOpenDate.Text = Convert.ToDateTime(st.OpenDate).ToShortDateString();
            }
            catch { }

            try
            {
                txtPropResolutionDate.Text = Convert.ToDateTime(st.PropResolutionDate).ToShortDateString();
            }
            catch { }
            try
            {
                txtCompletionDate.Text = Convert.ToDateTime(st.CompletionDate).ToShortDateString();
            }
            catch { }


            txtCustomerInput.Text = st.CustomerInput;
            txtFaultIdentified.Text = st.FaultIdentified;
            txtActionRequired.Text = st.ActionRequired;
            txtWorkDone.Text = st.WorkDone;
            txtCurrentPhoneContact.Text = st.CurrentPhoneContact;
            chkWarranty.Checked = Convert.ToBoolean(st.Warranty);
            try
            {
                txtMtceCost.Text = SiteConfiguration.ChangeCurrencyVal(st.MtceCost);
            }
            catch { }
            try
            {
                txtServiceCost.Text = SiteConfiguration.ChangeCurrencyVal(st.ServiceCost);
            }
            catch { }

            try
            {
                txtMtceDiscount.Text = SiteConfiguration.ChangeCurrencyVal(st.MtceDiscount);
            }
            catch { }
            try
            {
                txtMtceBalance.Text = SiteConfiguration.ChangeCurrencyVal(st.MtceBalance);
            }
            catch { }

            ddlMtceRecBy.SelectedValue = st.MtceRecBy;
            ddlFPTransTypeID.SelectedValue = st.FPTransTypeID;
            makeReadOnly((Control)this);
            txtMtceBalance.Enabled = false;


            btnAdd.Visible = false;
            btnReset.Visible = false;
            btnUpdate.Visible = false;
            btnCancel.Visible = false;
            PanGrid.Visible = false;
            lnkBack.Visible = true;
            lnkAdd.Visible = false;
            btnCopy.Visible = false;
            lnkmaintainance.Visible = true;


        }
        //if (e.CommandName == "print")
        //{
        //    hndProjectMaintenanceID.Value = e.CommandArgument.ToString();

        //    string ProjectMaintenanceID = e.CommandArgument.ToString();
        //    TextWriter txtWriter1 = new StringWriter() as TextWriter;
        //    if (!string.IsNullOrEmpty(ProjectMaintenanceID))
        //    {
        //        SttblProjectMaintenance stMtce = ClstblProjectMaintenance.tblProjectMaintenance_SelectByProjectMaintenanceID(ProjectMaintenanceID);
        //        Server.Execute("~/mailtemplate/printmtce.aspx?id=" + ProjectMaintenanceID, txtWriter1);
        //    }
        //    divContent.InnerHtml = txtWriter1.ToString();

        //    string script = "<script language='javascript'>printContent();</script>";
        //    //string script = "<script language='javascript'>function f(){printContent(); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>";
        //    Literal1.Text = script;
        //    //Page.ClientScript.RegisterStartupScript(this.GetType(), "openPopUp", script);
        //    //Response.Redirect(Request.Url.PathAndQuery);
        //}
        //Profile.eurosolar.maintenanceid = "";
    }
    protected void lnkclose_Click(object sender, EventArgs e)
    {
        divMtceDetail.Visible = false;
        //Profile.eurosolar.maintenanceid = "";
    }
    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.ToString();
        bool sucess1 = ClstblProjectMaintenance.tblProjectMaintenance_Delete(id);
        //--- do not chage this code start------
        if (sucess1)
        {
            SetAdd1();
            //PanSuccess.Visible = true;
        }
        else
        {
            SetError1();
            //PanError.Visible = true;
        }

        Reset();
        PanAddUpdate.Visible = false;
        //--- do not chage this code end------
        //--- do not chage this code start------
        GridView1.EditIndex = -1;
        BindGrid(0);
        BindGrid(1);
        //--- do not chage this code end------
    }

    public void makeReadOnly(Control c)
    {
        foreach (Control childControl in c.Controls)
        {
            if (childControl.GetType() == typeof(TextBox))
            {
                ((TextBox)childControl).Enabled = false;
            }
            else if (childControl.GetType() == typeof(CheckBox))
            {
                ((CheckBox)childControl).Enabled = false;
            }
            else if (childControl.GetType() == typeof(DropDownList))
            {
                ((DropDownList)childControl).Enabled = false;
            }


            if (childControl.Controls.Count > 0)
            {
                makeReadOnly(childControl);
            }
        }
    }
    public void makeEnable(Control c)
    {
        foreach (Control childControl in c.Controls)
        {
            if (childControl.GetType() == typeof(TextBox))
            {
                ((TextBox)childControl).Enabled = true;
            }
            else if (childControl.GetType() == typeof(CheckBox))
            {
                ((CheckBox)childControl).Enabled = true;
            }
            else if (childControl.GetType() == typeof(DropDownList))
            {
                ((DropDownList)childControl).Enabled = true;
            }


            if (childControl.Controls.Count > 0)
            {
                makeEnable(childControl);
            }
        }
    }

    protected void lnkmaintainance_Click(object sender, EventArgs e)
    {

        string ProjectID = Request.QueryString["proid"];
        string mtceid = hdnmtceid.Text;
        //Response.Write(mtceid);
        //Response.End();
        if (ProjectID != "")
        {
            Telerik_reports.generate_maintenanceForSolarMIner(ProjectID, mtceid);
        }

    }
}