﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class includes_employeemenu : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string pagename = Utilities.GetPageName(Request.Url.PathAndQuery).ToLower();

        if (pagename == "empdetails")
        {
            li_empdetails.Attributes.Add("class", "selected");
        }
        if (pagename == "emppermissions")
        {
            li_emppermissions.Attributes.Add("class", "selected");
        }
        if (pagename == "empreferences")
        {
            li_empreferences.Attributes.Add("class", "selected");
        }

        if (!IsPostBack)
        {
            hypDetail.NavigateUrl = "~/admin/adminfiles/master/empdetails.aspx?id=" + Request.QueryString["id"];
            hypPermissions.NavigateUrl = "~/admin/adminfiles/master/emppermissions.aspx?id=" + Request.QueryString["id"];
            hypReferences.NavigateUrl = "~/admin/adminfiles/master/empreferences.aspx?id=" + Request.QueryString["id"];
        }
    }
}