﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Web.Security;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

public partial class includes_InvoicePayments : System.Web.UI.UserControl
{
    protected string SiteURL;
    protected DataTable rpttable;
    protected static int MaxAttribute = 0;
    decimal newtotalcost;
    protected string mode;
    static string Operationmode;
    public string orderamount;
    public string projectno;
    protected void Page_Load(object sender, EventArgs e)
    {

        StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
        SiteURL = st.siteurl;
        //  ModalPopupExtenderInvPay.Show();
        GridView GridView1 = (GridView)this.Parent.FindControl("GridView1");
        if (GridView1 != null)
        {
            btnInvPay.Visible = false;
        }
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField1.Value;
        }
        orderamount = hdnorderamount.Value;
        projectno = hdnpronum.Value;

        Session["rderamount"] = hdnorderamount.Value;
        Session[" projectno"] = hdnpronum.Value;
        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "MyAction", "checkopen();", true);
    }

    public void BindProjectInvoice(string proid)
    {
        if (proid != string.Empty)
        {
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(proid);
            string ProjectID = proid;
            hdnpronum.Value = stPro.ProjectNumber;
            Operationmode = "Add";
            MaxAttribute = 1;
            DataTable dt_inv = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(proid);
            rpttable = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(proid);

            rptPayment.DataSource = dt_inv;
            rptPayment.DataBind();

            if (dt_inv.Rows.Count > 1)
            {
                MaxAttribute = dt_inv.Rows.Count;
            }
            if (stPro.InvoiceNumber != string.Empty && stPro.InvoiceDoc != string.Empty)
            {
                btnInvPay.Enabled = true;
            }
            else
            {
                btnInvPay.Enabled = false;
            }
            //  GridView GridView1 = (GridView)this.ch.FindControl("GridView1");
            bindrepeater();
            BindAddedAttribute();
            SttblCustomers stCust = ClstblCustomers.tblCustomers_SelectByCustomerID(stPro.CustomerID);
            ltcustdetail.Text = stCust.Customer + " " + stPro.InstallAddress;// +"<br/>" + stPro.InstallAddress;
            if (stPro.SystemDetails != string.Empty)
            {
                divsysdetail.Visible = true;
                ltsysdetail.Text = stPro.SystemDetails;
            }
            else
            {
                divsysdetail.Visible = false;
            }

            lblProjectNumber.Text = stPro.ProjectNumber;
            txtInvoiceNotes.Text = stPro.InvoiceNotes;
            //Response.Write(stPro.InvoiceStatus);
            //Response.End();
            lblStatus.Text = stPro.InvoiceStatus;

            DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
            try
            {
                decimal totalcostnew = Convert.ToDecimal(stPro.TotalQuotePrice) + Convert.ToDecimal(stPro.RECRebate);
                txtTotalCostNew.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(totalcostnew));
            }
            catch { }
            try
            {
                txtLessRebate.Text = SiteConfiguration.ChangeCurrencyVal(stPro.RECRebate);
            }
            catch { }
            try
            {
                txtTotalCost.Text = SiteConfiguration.ChangeCurrencyVal(stPro.TotalQuotePrice);
            }
            catch { }

            decimal paiddate = 0;
            decimal balown = 0;
            if (dtCount.Rows[0]["InvoicePayTotal"].ToString() != "")
            {
                paiddate = Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
                balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(paiddate);
                try
                {
                    txtPaidDate.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(paiddate));
                }
                catch { }
                try
                {
                    txtBalOwing.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(balown));
                }
                catch { }
            }
            else
            {
                txtPaidDate.Text = "0";
                try
                {
                    txtBalOwing.Text = SiteConfiguration.ChangeCurrencyVal(stPro.TotalQuotePrice);
                }
                catch { }
            }

            BindGrid();
        }
    }
    protected void lnkBack_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            string ProjectID = Request.QueryString["id"];
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            Response.Redirect("~/admin/adminfiles/company/projects.aspx?id=" + stPro.CustomerID);
        }
    }
    public void BindGrid()
    {
        bindrepeater();
    }
    protected void txtLessRebate_TextChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderInvPay.Show();
        if (txtLessRebate.Text != string.Empty && txtLessRebate.Text != "0.0000")//0.0000
        {
            newtotalcost = Convert.ToDecimal(txtLessRebate.Text) + Convert.ToDecimal(txtTotalCost.Text);
        }
        txtTotalCostNew.Text = Convert.ToString(newtotalcost);
    }
    protected void txtInvoicePayTotal_TextChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderInvPay.Show();
        foreach (RepeaterItem item in rptPayment.Items)
        {
            TextBox txtInvoicePayTotal = (TextBox)item.FindControl("txtInvoicePayTotal");
            TextBox txtInvoicePayGST = (TextBox)item.FindControl("txtInvoicePayGST");
            TextBox txtCCSurcharge = (TextBox)item.FindControl("txtCCSurcharge");
            hdnorderamount.Value = txtInvoicePayTotal.Text;
            if (Convert.ToDecimal(txtInvoicePayTotal.Text) > 0)
            {
                decimal invoicepayGST = Convert.ToDecimal(txtInvoicePayTotal.Text) / 11;
                try
                {
                    txtInvoicePayGST.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(invoicepayGST));
                }
                catch { }
            }
            else
            {
                txtInvoicePayGST.Text = "0";
                txtCCSurcharge.Text = "0";
            }
        }
    }
    protected void ddlInvoicePayMethodID_SelectedIndexChanged(object sender, EventArgs e)
    {

        ModalPopupExtenderInvPay.Show();
        //foreach (RepeaterItem item in rptPayment.Items)
        int index = GetControlIndex(((DropDownList)sender).ClientID);
        RepeaterItem item = rptPayment.Items[index];
        {
            TextBox txtReceiptNumber = (TextBox)item.FindControl("txtReceiptNumber");
            TextBox txtInvoicePayTotal = (TextBox)item.FindControl("txtInvoicePayTotal");
            TextBox txtCCSurcharge = (TextBox)item.FindControl("txtCCSurcharge");
            DropDownList ddlInvoicePayMethodID = (DropDownList)item.FindControl("ddlInvoicePayMethodID");
            DropDownList ddlEft = (DropDownList)item.FindControl("ddlEft");
            RequiredFieldValidator RequiredFieldValidatorPayBy = (RequiredFieldValidator)item.FindControl("RequiredFieldValidatorPayBy");
            if (ddlInvoicePayMethodID.SelectedValue == "3")
            {
                ModalPopupExtender2.Show();

            }
            if (ddlInvoicePayMethodID.SelectedValue == "0")
            {
                RequiredFieldValidatorPayBy.Visible = true;
            }
            else
            {
                RequiredFieldValidatorPayBy.Visible = false;
            }

            if (ddlInvoicePayMethodID.SelectedValue == "7" || ddlInvoicePayMethodID.SelectedValue == "3" || ddlInvoicePayMethodID.SelectedValue == "2")
            {
                txtReceiptNumber.Visible = true;
            }
            else
            {
                txtReceiptNumber.Visible = false;
            }

            if (item.ItemIndex == 0)
            {
                txtCCSurcharge.Text = string.Empty;
                txtCCSurcharge.Enabled = false;
            }
            else
            {
                if (ddlInvoicePayMethodID.SelectedValue == "3")
                {
                    decimal ccsurcharge = 0;
                    if (txtInvoicePayTotal.Text != string.Empty)
                    {
                        ccsurcharge = ((Convert.ToDecimal(txtInvoicePayTotal.Text)) * Convert.ToDecimal(1)) / 100;
                    }
                    txtCCSurcharge.Text = SiteConfiguration.ChangeCurrencyVal(Convert.ToString(ccsurcharge));
                    txtCCSurcharge.Enabled = false;
                    if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("PostInstaller"))
                    {
                        txtCCSurcharge.Enabled = true;
                    }
                }
                else
                {
                    if (ddlInvoicePayMethodID.SelectedValue == "6")
                    {
                        txtCCSurcharge.Text = "0";
                        txtCCSurcharge.Enabled = true;
                        ddlEft.Visible = true;
                    }
                    else
                    {
                        txtCCSurcharge.Text = string.Empty;
                        txtCCSurcharge.Enabled = false;
                        ddlEft.Visible = false;
                    }
                }
            }
        }
    }
    protected void AddmoreAttribute()
    {
        //Response.Write(MaxAttribute + "<br>");
        MaxAttribute = MaxAttribute + 1;
        //Response.Write(MaxAttribute + "<br>");
        BindAddedAttribute();
        bindrepeater();
        //Response.Write(MaxAttribute + "<br>");
    }
    protected void BindAddedAttribute()
    {
        rpttable = new DataTable();
        rpttable.Columns.Add("InvoicePaymentID", Type.GetType("System.Int32"));
        rpttable.Columns.Add("IsVerified", Type.GetType("System.String"));
        rpttable.Columns.Add("InvoicePayDate", Type.GetType("System.String"));
        rpttable.Columns.Add("InvoicePayTotal", Type.GetType("System.String"));
        rpttable.Columns.Add("InvoicePayGST", Type.GetType("System.String"));
        rpttable.Columns.Add("InvoicePayMethodID", Type.GetType("System.String"));
        rpttable.Columns.Add("CCSurcharge", Type.GetType("System.String"));
        rpttable.Columns.Add("EmployeeID", Type.GetType("System.String"));
        rpttable.Columns.Add("RowNumber", Type.GetType("System.String"));
        rpttable.Columns.Add("UpdatedName", Type.GetType("System.String"));
        rpttable.Columns.Add("ReceiptNumber", Type.GetType("System.String"));
        rpttable.Columns.Add("EftPaymentId", Type.GetType("System.Int32"));

        foreach (RepeaterItem item in rptPayment.Items)
        {
            HiddenField hndIsVerified = (HiddenField)item.FindControl("hndIsVerified");
            HiddenField hndid = (HiddenField)item.FindControl("hndid");
            HiddenField hndRowNumber = (HiddenField)item.FindControl("hndRowNumber");
            HiddenField hndInvoicePayMethodID = (HiddenField)item.FindControl("hndInvoicePayMethodID");
            HiddenField hndRecBy = (HiddenField)item.FindControl("hndRecBy");
            TextBox txtInvoicePayDate = (TextBox)item.FindControl("txtInvoicePayDate");
            TextBox txtReceiptNumber = (TextBox)item.FindControl("txtReceiptNumber");
            TextBox txtInvoicePayTotal = (TextBox)item.FindControl("txtInvoicePayTotal");
            TextBox txtInvoicePayGST = (TextBox)item.FindControl("txtInvoicePayGST");
            DropDownList ddlInvoicePayMethodID = (DropDownList)item.FindControl("ddlInvoicePayMethodID");
            TextBox txtCCSurcharge = (TextBox)item.FindControl("txtCCSurcharge");
            DropDownList ddlRecBy = (DropDownList)item.FindControl("ddlRecBy");
            Label lblUpdateBy = (Label)item.FindControl("lblUpdateBy");
            HiddenField hdnddlEft = (HiddenField)item.FindControl("hdnddlEft");
            DropDownList ddlEft = (DropDownList)item.FindControl("ddlEft");

            DataRow dr = rpttable.NewRow();
            dr["IsVerified"] = hndIsVerified.Value;
            dr["InvoicePaymentID"] = hndid.Value;
            dr["RowNumber"] = hndRowNumber.Value;
            dr["InvoicePayDate"] = txtInvoicePayDate.Text;
            dr["ReceiptNumber"] = txtReceiptNumber.Text;
            dr["InvoicePayTotal"] = txtInvoicePayTotal.Text;
            dr["InvoicePayGST"] = txtInvoicePayGST.Text;
            dr["InvoicePayMethodID"] = ddlInvoicePayMethodID.SelectedValue;
            dr["CCSurcharge"] = txtCCSurcharge.Text;
            dr["EmployeeID"] = ddlRecBy.SelectedValue;
            dr["UpdatedName"] = lblUpdateBy.Text;
            dr["EftPaymentId"] = ddlEft.SelectedValue;
            rpttable.Rows.Add(dr);
        }
    }
    protected void bindrepeater()
    {
        if (rpttable == null)
        {
            rpttable = new DataTable();
            rpttable.Columns.Add("IsVerified", Type.GetType("System.Boolean"));
            rpttable.Columns.Add("InvoicePaymentID", Type.GetType("System.Int32"));
            rpttable.Columns.Add("InvoicePayDate", Type.GetType("System.String"));
            rpttable.Columns.Add("ReceiptNumber", Type.GetType("System.String"));
            rpttable.Columns.Add("InvoicePayTotal", Type.GetType("System.String"));
            rpttable.Columns.Add("InvoicePayGST", Type.GetType("System.String"));
            rpttable.Columns.Add("InvoicePayMethodID", Type.GetType("System.String"));
            rpttable.Columns.Add("CCSurcharge", Type.GetType("System.String"));
            rpttable.Columns.Add("EmployeeID", Type.GetType("System.String"));
            rpttable.Columns.Add("RowNumber", Type.GetType("System.String"));
            rpttable.Columns.Add("EftPaymentId", Type.GetType("System.Int32"));
        }
        for (int i = rpttable.Rows.Count; i < MaxAttribute; i++)
        {
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

            DataRow dr = rpttable.NewRow();
            dr["IsVerified"] = i;
            dr["InvoicePaymentID"] = -(i + 1);
            dr["RowNumber"] = -(i + 1);
            dr["InvoicePayDate"] = DateTime.Now.AddHours(14).ToShortDateString();
            dr["ReceiptNumber"] = DBNull.Value;
            dr["InvoicePayTotal"] = DBNull.Value;
            dr["InvoicePayGST"] = DBNull.Value;
            dr["InvoicePayMethodID"] = DBNull.Value;
            dr["CCSurcharge"] = DBNull.Value;
            dr["EmployeeID"] = stEmp.EmployeeID;
            dr["EftPaymentId"] = "0";
            try
            {
                rpttable.Rows.Add(dr);
            }
            catch(Exception ex) { }
        }

        rptPayment.DataSource = rpttable;
        rptPayment.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {

        //ModalPopupExtenderInvPay.Show();
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField1.Value;
        }
        if (ProjectID != string.Empty)
        {
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);
            string userid = Membership.GetUser(HttpContext.Current.User.Identity.Name).ProviderUserKey.ToString();
            SttblEmployees stEmp = ClstblEmployees.tblEmployees_SelectByUserId(userid);

            //bool suc1 = ClstblInvoicePayments.tblInvoicePayments_DeleteProjectID(ProjectID);
            foreach (RepeaterItem item in rptPayment.Items)
            {
                TextBox txtInvoicePayDate = (TextBox)item.FindControl("txtInvoicePayDate");
                TextBox txtReceiptNumber = (TextBox)item.FindControl("txtReceiptNumber");
                TextBox txtInvoicePayTotal = (TextBox)item.FindControl("txtInvoicePayTotal");
                TextBox txtInvoicePayGST = (TextBox)item.FindControl("txtInvoicePayGST");
                TextBox txtCCSurcharge = (TextBox)item.FindControl("txtCCSurcharge");
                DropDownList ddlInvoicePayMethodID = (DropDownList)item.FindControl("ddlInvoicePayMethodID");
                DropDownList ddlEft = (DropDownList)item.FindControl("ddlEft");
                DropDownList ddlRecBy = (DropDownList)item.FindControl("ddlRecBy");
                HiddenField hndid = (HiddenField)item.FindControl("hndid");
                HiddenField hndRowNumber = (HiddenField)item.FindControl("hndRowNumber");
                Label lblUpdateBy = (Label)item.FindControl("lblUpdateBy");
                //HiddenField hndVerifiedBy = (HiddenField)item.FindControl("hndVerifiedBy");

                if (txtInvoicePayTotal.Text != string.Empty)
                {
                    decimal invoicepayGST = Convert.ToDecimal(txtInvoicePayTotal.Text) / 10;

                    decimal invoicepayExGST = (Convert.ToDecimal(txtInvoicePayTotal.Text) / 10) * 10;
                    string InvoicePayExGST = Convert.ToString(invoicepayExGST);
                    string InvoicePayGST = Convert.ToString(invoicepayGST);
                    string InvoicePayTotal = txtInvoicePayTotal.Text;
                    string InvoicePayDate = txtInvoicePayDate.Text;
                    string ReceiptNumber = txtReceiptNumber.Text;
                    string InvoicePayMethodID = ddlInvoicePayMethodID.SelectedValue;
                    string EmployeeID = ddlRecBy.SelectedValue;
                    string CCSurcharge = txtCCSurcharge.Text;
                    string eftPmtId = ddlEft.SelectedValue;

                    string Refund = "False";
                    string RefundType = "0";
                    string UpdatedBy = stEmp.EmployeeID;

                    DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
                    if (txtInvoicePayDate.Text != string.Empty || txtInvoicePayTotal.Text != string.Empty)
                    {
                        if (Convert.ToDecimal(hndRowNumber.Value) < 0)
                        {
                            int success = ClstblInvoicePayments.tblInvoicePayments_Insert(ProjectID, InvoicePayExGST, InvoicePayGST, InvoicePayTotal, InvoicePayDate, InvoicePayMethodID, CCSurcharge, EmployeeID, "", "", "", Refund, RefundType, "", "", "", ReceiptNumber);
                            bool s1 = ClstblInvoicePayments.tblInvoicePayments_UpdateEftpatmentId(Convert.ToString(success), eftPmtId);
                            if (dtCount.Rows.Count > 0)
                            {
                                if (stPro.TotalQuotePrice != string.Empty && dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
                                {
                                    decimal balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
                                    decimal BalanceOwing = balown / 10;
                                    string BalanceGST = Convert.ToString(BalanceOwing);
                                    string pInvoicePayTotal = dtCount.Rows[0]["InvoicePayTotal"].ToString();
                                    bool suc = ClstblInvoicePayments.tblProjects_UpdateInvoice(ProjectID, BalanceGST, txtInvoiceNotes.Text, InvoicePayDate);
                                }
                                //if (suc)
                                //{
                                //    if (Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString()) <= 0)
                                //    {
                                //        bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "0");
                                //    }
                                //    if (Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString()) < Convert.ToDecimal(stPro.TotalQuotePrice))
                                //    {
                                //        bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "1");
                                //    }
                                //    if (Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString()) >= Convert.ToDecimal(stPro.TotalQuotePrice))
                                //    {
                                //        string EmpID = stEmp.EmployeeID;
                                //        if (stPro.ProjectStatusID == "3")
                                //        {
                                //            bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "2");
                                //            ClstblInvoicePayments.tblProjects_UpdateInvoiceFU(ProjectID);

                                //            if (stPro.InstallCompleted != string.Empty && stPro.InstallDocsReceived != string.Empty && stPro.STCFormsDone == "True" && stPro.CertificateIssued != string.Empty)
                                //            {
                                //                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("5", ProjectID, EmpID, stPro.NumberPanels);
                                //                bool suc3 = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "5");
                                //            }
                                //        }
                                //    }
                                //}
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(hndid.Value))
                            {
                                bool success = ClstblInvoicePayments.tblInvoicePayments_Update(hndid.Value, InvoicePayExGST, InvoicePayGST, InvoicePayTotal, InvoicePayDate, InvoicePayMethodID, CCSurcharge, EmployeeID, UpdatedBy, ReceiptNumber);
                                bool s1 = ClstblInvoicePayments.tblInvoicePayments_UpdateEftpatmentId(hndid.Value, eftPmtId);
                            }
                            if (dtCount.Rows.Count > 0)
                            {
                                decimal balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
                                decimal BalanceOwing = balown / 10;
                                string BalanceGST = Convert.ToString(BalanceOwing);
                                string pInvoicePayTotal = dtCount.Rows[0]["InvoicePayTotal"].ToString();
                                bool suc = ClstblInvoicePayments.tblProjects_UpdateInvoice(ProjectID, BalanceGST, txtInvoiceNotes.Text, InvoicePayDate);

                                //if (suc)
                                //{
                                //    if (Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString()) <= 0)
                                //    {
                                //        bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "0");
                                //    }
                                //    if (Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString()) < Convert.ToDecimal(stPro.TotalQuotePrice))
                                //    {
                                //        bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "1");
                                //    }
                                //    if (Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString()) >= Convert.ToDecimal(stPro.TotalQuotePrice))
                                //    {
                                //        string EmpID = stEmp.EmployeeID;
                                //        if (stPro.ProjectStatusID == "3")
                                //        {
                                //            bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "2");
                                //            ClstblInvoicePayments.tblProjects_UpdateInvoiceFU(ProjectID);

                                //            if (stPro.InstallCompleted != string.Empty && stPro.InstallDocsReceived != string.Empty && stPro.STCFormsDone == "True" && stPro.CertificateIssued != string.Empty)
                                //            {
                                //                int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("5", ProjectID, EmpID, stPro.NumberPanels);
                                //                bool suc3 = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "5");
                                //            }
                                //        }
                                //    }
                                //}
                            }
                        }

                        if (dtCount.Rows.Count > 0)
                        {
                            if (stPro.TotalQuotePrice != string.Empty && dtCount.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
                            {
                                decimal balown = Convert.ToDecimal(stPro.TotalQuotePrice) - Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString());
                                decimal BalanceOwing = balown / 10;
                                string BalanceGST = Convert.ToString(BalanceOwing);
                                string pInvoicePayTotal = dtCount.Rows[0]["InvoicePayTotal"].ToString();
                                bool suc = ClstblInvoicePayments.tblProjects_UpdateInvoice(ProjectID, BalanceGST, txtInvoiceNotes.Text, InvoicePayDate);

                                if (Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString()) <= 0)
                                {
                                    bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "0");
                                }
                                if (Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString()) < Convert.ToDecimal(stPro.TotalQuotePrice))
                                {
                                    bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "1");
                                }
                                if (Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString()) >= Convert.ToDecimal(stPro.TotalQuotePrice))
                                {
                                    string EmpID = stEmp.EmployeeID;
                                    //if (stPro.ProjectStatusID == "13")
                                    //{
                                    bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "2");
                                    ClstblInvoicePayments.tblProjects_UpdateInvoiceFU(ProjectID);

                                    //if (stPro.InstallCompleted != string.Empty && stPro.InstallDocsReceived != string.Empty && stPro.STCFormsDone == "True" && stPro.CertificateIssued != string.Empty)
                                    //{
                                    //int sucTrack = ClstblProjects.tblTrackProjStatus_Insert("5", ProjectID, EmpID, stPro.NumberPanels);
                                }
                                //bool suc3 = ClsProjectSale.tblProjects_UpdateProjectStatusID(ProjectID, "5");
                                //}
                                //}
                            }
                        }
                    }
                    //-----------------------------------------------------------------------------------

                    //try
                    //{
                    //    lblTotalPaidAmount.Text = dtCount.Rows[0]["InvoicePayTotal"].ToString();
                    //}
                    //catch { }

                    //string totalprice = st.TotalQuotePrice;


                    /* ----------------- Send Mail ----------------- */

                    StUtilities st = ClsAdminUtilities.StUtilitiesGetDataStructById("1");
                    SiteURL = st.siteurl;
                    string from = st.from;
                    string ProjectNumber = stPro.ProjectNumber;
                    string PayDate = txtInvoicePayDate.Text;
                    string PaidAmount = txtInvoicePayTotal.Text;
                    string PaidBy = stEmp.EmpFirst + " " + stEmp.EmpLast;
                    ReceiptNumber = txtReceiptNumber.Text;
                    TextBox txtInvoiceFU = this.Parent.FindControl("txtInvoiceFU") as TextBox;

                    if (txtInvoiceFU != null)// txtBalanceOwing != null)
                    {
                        //if (stPro.InvoiceStatusID == "2")
                        //{
                        //try
                        //{
                        //Response.Write(stPro.InvoiceFU);
                        //Response.End();
                        txtInvoiceFU.Text = string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(PayDate));
                        //}
                        //catch { }
                        // }
                    }
                    if (ddlInvoicePayMethodID.SelectedValue == "7")
                    {
                        TextWriter txtWriter = new StringWriter() as TextWriter;
                        string subject = "Cash Transaction";

                        Server.Execute("~/mailtemplate/cashmail.aspx?ProjectNumber=" + ProjectNumber + "&PayDate=" + string.Format("{0:dd MMM yyyy}", Convert.ToDateTime(PayDate)) + "&PaidAmount=" + PaidAmount + "&ReceiptNumber=" + ReceiptNumber + "&PaidBy=" + PaidBy, txtWriter);
                        try
                        {
                            //Utilities.SendMail(from, "peter.eurosolar@gmail.com", subject, txtWriter.ToString(), "cash@eurosolar.com.au");
                        }
                        catch
                        { }
                    }

                    /* --------------- Send Mail End --------------- */
                }
            }
            string InvoiceNotes = txtInvoiceNotes.Text;
            string RECRebate = txtLessRebate.Text;
            bool suc2 = ClsProjectSale.tblProjects_UpdateInvoicePayments(ProjectID, InvoiceNotes, RECRebate);
            AddmoreAttribute();
        }

        status();
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            BindProjectInvoice(Request.QueryString["proid"]);
            findcontrol();
        }
        else
        {
            BindProjectInvoice(HiddenField1.Value);
        }
        ModalPopupExtenderInvPay.Hide();
    }

    public void status()
    {
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField1.Value;
        }
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

        DataTable dt_Count = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
        string totalprice = stPro.TotalQuotePrice;
        decimal TotalPrice = 0;
        if (totalprice != string.Empty)
        {
            TotalPrice = Convert.ToDecimal(totalprice);
        }
        decimal totalpay = 0;
        if (dt_Count.Rows.Count > 0)
        {
            if (dt_Count.Rows[0]["InvoicePayTotal"].ToString() != string.Empty)
            {
                totalpay = Convert.ToDecimal(dt_Count.Rows[0]["InvoicePayTotal"].ToString());
                decimal balown = Convert.ToDecimal(totalprice) - Convert.ToDecimal(totalpay);
                TextBox txtBalanceOwing = this.Parent.FindControl("txtBalanceOwing") as TextBox;
                if (txtBalanceOwing != null)
                {

                    try
                    {
                        txtBalanceOwing.Text = SiteConfiguration.ChangeCurrency_Val(Convert.ToString(balown));
                    }
                    catch { }

                }
            }
        }
        if (TotalPrice - totalpay > 0)
        {
            // lblpaymentstatus.Text=
            bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "1");
        }
        else
        {
            bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "2");
        }
        Label lblpaymentstatus = this.Parent.FindControl("lblpaymentstatus") as Label;
        if (lblpaymentstatus != null)
        {
            if (TotalPrice == 0)
            {
                lblpaymentstatus.Text = " ";
            }
            else
            {
                if (TotalPrice - totalpay > 0)
                {
                    lblpaymentstatus.Text = "Owing";
                }
                else
                {
                    lblpaymentstatus.Text = "Fully Paid";
                }
            }
        }




    }
    public void findcontrol()
    {
        Label lblreceipt = this.Parent.FindControl("lblreceipt") as Label;
        System.Web.UI.HtmlControls.HtmlContainerControl divrecept = (System.Web.UI.HtmlControls.HtmlContainerControl)Parent.FindControl("divrecept");
        DataTable dt_inv_recpt = ClstblInvoicePayments.tblInvoicePayments_SelectByProjectID(Request.QueryString["proid"]);
        if (dt_inv_recpt.Rows[0]["ReceiptNumber"].ToString() != string.Empty)
        {
            if (lblreceipt != null && divrecept != null)
            {
                divrecept.Visible = true;
                lblreceipt.Text = dt_inv_recpt.Rows[0]["ReceiptNumber"].ToString();
            }
        }
        Label lblTotalPaidAmount = this.Parent.FindControl("lblTotalPaidAmount") as Label;//lblpaymentstatus

        DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(Request.QueryString["proid"]);
        try
        {
            if (lblTotalPaidAmount != null)
            {
                lblTotalPaidAmount.Text = dtCount.Rows[0]["InvoicePayTotal"].ToString();
            }
        }
        catch { }//txtInvoiceFU//txtBalanceOwing
        SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);

        //TextBox txtActiveDate = this.Parent.FindControl("txtActiveDate") as TextBox;
        //TextBox txtDepositReceived = this.Parent.FindControl("txtDepositReceived") as TextBox;
        //if (txtActiveDate != null )
        //{

        //  //  txtActiveDate.Visible = false;
        //    txtActiveDate.Enabled = true;
        //}
        //if (txtDepositReceived != null)
        //{
        //    txtDepositReceived.Enabled = true;
        //}
    }
    protected void rptPayment_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        HiddenField hndIsVerified = (HiddenField)e.Item.FindControl("hndIsVerified");
        HiddenField hndid = (HiddenField)e.Item.FindControl("hndid");
        HiddenField hndRowNumber = (HiddenField)e.Item.FindControl("hndRowNumber");
        HiddenField hndInvoicePayMethodID = (HiddenField)e.Item.FindControl("hndInvoicePayMethodID");
        HiddenField hndRecBy = (HiddenField)e.Item.FindControl("hndRecBy");
        TextBox txtInvoicePayDate = (TextBox)e.Item.FindControl("txtInvoicePayDate");
        TextBox txtReceiptNumber = (TextBox)e.Item.FindControl("txtReceiptNumber");
        TextBox txtInvoicePayTotal = (TextBox)e.Item.FindControl("txtInvoicePayTotal");
        TextBox txtInvoicePayGST = (TextBox)e.Item.FindControl("txtInvoicePayGST");
        DropDownList ddlInvoicePayMethodID = (DropDownList)e.Item.FindControl("ddlInvoicePayMethodID");
        TextBox txtCCSurcharge = (TextBox)e.Item.FindControl("txtCCSurcharge");
        DropDownList ddlRecBy = (DropDownList)e.Item.FindControl("ddlRecBy");
        LinkButton lbtnDelete = (LinkButton)e.Item.FindControl("lbtnDelete");
        Label lblUpdateBy = (Label)e.Item.FindControl("lblUpdateBy");
        //CompareValidator CompareValidatorPayDate = (CompareValidator)e.Item.FindControl("CompareValidatorPayDate");
        CompareValidator CompareValidator1 = (CompareValidator)e.Item.FindControl("CompareValidator1");
        HiddenField hiddenSentDate = (HiddenField)e.Item.FindControl("hiddenSentDate");

        DropDownList ddlEft = (DropDownList)e.Item.FindControl("ddlEft");
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(Request.QueryString["proid"]);
            try
            {
                if (!Roles.IsUserInRole("Administrator"))
                {
                    //CompareValidatorPayDate.ValueToCompare = Convert.ToDateTime(stPro.InvoiceSent).ToShortDateString();
                    hiddenSentDate.Value = Convert.ToDateTime(stPro.InvoiceSent).AddDays(-1).ToString();//
                    CompareValidator1.ValueToCompare = Convert.ToDateTime(stPro.InvoiceSent).ToShortDateString();
                }
            }
            catch { }
        }
        else
        {
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(HiddenField1.Value);
            try
            {
                if (!Roles.IsUserInRole("Administrator"))
                {
                    //CompareValidatorPayDate.ValueToCompare = Convert.ToDateTime(stPro.InvoiceSent).ToShortDateString();
                    hiddenSentDate.Value = Convert.ToDateTime(stPro.InvoiceSent).AddDays(-1).ToString();//
                    CompareValidator1.ValueToCompare = Convert.ToDateTime(stPro.InvoiceSent).ToShortDateString();
                }

            }
            catch { }
        }
        //System.Web.UI.WebControls.ListItem item0 = new System.Web.UI.WebControls.ListItem();
        //item0.Text = "Select";
        //item0.Value = "0";
        //ddlEft.Items.Clear();
        //ddlEft.Items.Add(item0);

        //System.Web.UI.WebControls.ListItem item9 = new System.Web.UI.WebControls.ListItem();
        //item9.Text = "Bank Transfer";
        //item9.Value = "1";
        //ddlEft.Items.Clear();
        //ddlEft.Items.Add(item9);

        //System.Web.UI.WebControls.ListItem item19 = new System.Web.UI.WebControls.ListItem();
        //item19.Text = "Bank Transfer";
        //item19.Value = "1";
        //ddlEft.Items.Clear();
        //ddlEft.Items.Add(item19);

        System.Web.UI.WebControls.ListItem item1 = new System.Web.UI.WebControls.ListItem();
        item1.Text = "Select";
        item1.Value = "";
        ddlInvoicePayMethodID.Items.Clear();
        ddlInvoicePayMethodID.Items.Add(item1);

        ddlInvoicePayMethodID.DataSource = ClsProjectSale.tblFPTransType_getInv();
        ddlInvoicePayMethodID.DataValueField = "FPTransTypeID";
        ddlInvoicePayMethodID.DataTextField = "FPTransTypeAB";
        ddlInvoicePayMethodID.DataMember = "FPTransTypeAB";
        ddlInvoicePayMethodID.DataBind();

        System.Web.UI.WebControls.ListItem item2 = new System.Web.UI.WebControls.ListItem();
        item2.Text = "Select";
        item2.Value = "";
        ddlRecBy.Items.Clear();
        ddlRecBy.Items.Add(item2);

        ddlRecBy.DataSource = ClstblEmployees.tblEmployees_Select();
        ddlRecBy.DataValueField = "EmployeeID";
        ddlRecBy.DataTextField = "EmpNicName";
        ddlRecBy.DataMember = "EmpNicName";
        ddlRecBy.DataBind();

        ddlInvoicePayMethodID.SelectedValue = hndInvoicePayMethodID.Value;
        ddlRecBy.SelectedValue = hndRecBy.Value;

        if (hndIsVerified.Value == "True")
        {
            txtInvoicePayDate.Enabled = false;
            txtInvoicePayTotal.Enabled = false;
            txtInvoicePayGST.Enabled = false;
            ddlInvoicePayMethodID.Enabled = false;
            txtCCSurcharge.Enabled = false;
            ddlRecBy.Enabled = false;
        }
        else
        {
            if (Roles.IsUserInRole("Administrator"))
            {
                ddlRecBy.Enabled = true;
            }
            else
            {
                ddlRecBy.Enabled = false;
            }
            txtInvoicePayDate.Enabled = true;
            txtInvoicePayTotal.Enabled = true;
            //txtInvoicePayGST.Enabled = true;
            ddlInvoicePayMethodID.Enabled = true;
            //txtCCSurcharge.Enabled = true;
        }

        if (ddlInvoicePayMethodID.SelectedValue == "7" || ddlInvoicePayMethodID.SelectedValue == "3")
        {
            txtReceiptNumber.Visible = true;
        }
        else
        {
            txtReceiptNumber.Visible = false;
        }

        if (e.Item.ItemIndex == 0)
        {
            txtCCSurcharge.Text = string.Empty;
            txtCCSurcharge.Enabled = false;
        }
        else
        {
            if (ddlInvoicePayMethodID.SelectedValue == "3" || ddlInvoicePayMethodID.SelectedValue == "6")
            {
                txtCCSurcharge.Enabled = false;
                if (Roles.IsUserInRole("Administrator") || Roles.IsUserInRole("Sales Manager") || Roles.IsUserInRole("PostInstaller"))
                {
                    txtCCSurcharge.Enabled = true;
                }
            }
            else
            {
                txtCCSurcharge.Text = string.Empty;
                txtCCSurcharge.Enabled = false;
            }
        }

        if (Roles.IsUserInRole("Administrator"))
        {
            // lbtnDelete.Visible = true;
            if (e.Item.ItemIndex == 0 || hndIsVerified.Value == "True")
            {
                lbtnDelete.Visible = false;
            }
            else
            {
                lbtnDelete.Visible = true;
            }
        }
        else
        {
            lbtnDelete.Visible = false;
        }

        if (Roles.IsUserInRole("Accountant") || Roles.IsUserInRole("Verification"))
        {
            CompareValidator1.Visible = false;
            txtCCSurcharge.Enabled = true;
        }
        else
        {
            CompareValidator1.Visible = true;
        }
    }
    protected void btnAddRow_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderInvPay.Show();
        AddmoreAttribute();
    }

    //protected void btnInvPay_Click(object sender, EventArgs e)
    //{


    //}

    public void GetInvPayClick()
    {
        MaxAttribute = 1;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            HiddenField1.Value = Request.QueryString["proid"];
            BindProjectInvoice(Request.QueryString["proid"]);
        }

        ModalPopupExtenderInvPay.Show();
    }
    public void GetInvPayClickByProject(string proid)
    {
        HiddenField1.Value = proid;
        MaxAttribute = 1;
        BindProjectInvoice(proid);
        ModalPopupExtenderInvPay.Show();
    }
    protected void btnPrintReceipt_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderInvPay.Show();
        string ProjectID = "";
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            ProjectID = Request.QueryString["proid"];
        }
        else
        {
            ProjectID = HiddenField1.Value;
        }
        if (ProjectID != string.Empty)
        {
            Telerik_reports.generate_PaymentReceiptForSolarMiner(ProjectID);
            //TextWriter txtWriter = new StringWriter() as TextWriter;
            //Server.Execute("~/mailtemplate/receipt.aspx?id=" + ProjectID, txtWriter);
            //String htmlText = txtWriter.ToString();
            //Response.Write(htmlText);
            //HTMLExportToPDF(htmlText, ProjectID + "Receipt.pdf");
        }
    }
    //public void HTMLExportToPDF(string HTML, string filename)
    //{
    //    System.Web.HttpResponse Response = System.Web.HttpContext.Current.Response;
    //    try
    //    {
    //        string pdfUser = ConfigurationManager.AppSettings["PDFUserName"];
    //        string pdfKey = ConfigurationManager.AppSettings["PDFUserKey"];
    //        // create an API client instance
    //        pdfcrowd.Client client = new pdfcrowd.Client(pdfUser, pdfKey);
    //        // convert a web page and write the generated PDF to a memory stream
    //        MemoryStream Stream = new MemoryStream();

    //        client.setVerticalMargin("66pt");

    //        TextWriter txtWriter = new StringWriter() as TextWriter;
    //        Server.Execute("~/mailtemplate/pdfheader.aspx", txtWriter);

    //        TextWriter txtWriter2 = new StringWriter() as TextWriter;
    //        Server.Execute("~/mailtemplate/pdffooter.aspx", txtWriter2);

    //        string HeaderHtml = txtWriter.ToString();
    //        string FooterHtml = txtWriter2.ToString();

    //        client.setHeaderHtml(HeaderHtml);
    //        client.setFooterHtml(FooterHtml);
    //        client.convertHtml(HTML, Stream);

    //        // set HTTP response headers
    //        Response.Clear();
    //        Response.AddHeader("Content-Type", "application/pdf");
    //        Response.AddHeader("Cache-Control", "no-cache");
    //        Response.AddHeader("Accept-Ranges", "none");
    //        Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
    //        // send the generated PDF
    //        Stream.WriteTo(Response.OutputStream);
    //        Stream.Close();
    //        Response.Flush();
    //        Response.End();
    //    }
    //    catch (pdfcrowd.Error why)
    //    {
    //        //Response.Write(why.ToString());
    //    }
    //}
    public void HTMLExportToPDF(string HTML, string filename)
    {
        System.Web.HttpResponse Response = System.Web.HttpContext.Current.Response;
        try
        {
            string pdfUser = ConfigurationManager.AppSettings["PDFUserName"];
            string pdfKey = ConfigurationManager.AppSettings["PDFUserKey"];
            // create an API client instance
            pdfcrowd.Client client = new pdfcrowd.Client(pdfUser, pdfKey);
            // convert a web page and write the generated PDF to a memory stream
            MemoryStream Stream = new MemoryStream();

            client.setVerticalMargin("66pt");

            //TextWriter txtWriter = new StringWriter() as TextWriter;
            //Server.Execute("~/mailtemplate/newpdfheader.aspx", txtWriter);

            //TextWriter txtWriter2 = new StringWriter() as TextWriter;
            //Server.Execute("~/mailtemplate/newpdffooter.aspx", txtWriter2);

            //string HeaderHtml = txtWriter.ToString();
            //string FooterHtml = txtWriter2.ToString();

            //client.setHeaderHtml("");
            //client.setFooterHtml("");
            client.setVerticalMargin("10pt");
            client.setPageWidth("842pt");
            client.setPageHeight("-1pt");

            //client.setPageMargins("0px", "20px", "0px", "20px");
            client.convertHtml(HTML, Stream);

            // set HTTP response headers
            Response.Clear();
            Response.AddHeader("Content-Type", "application/pdf");
            Response.AddHeader("Cache-Control", "no-cache");
            Response.AddHeader("Accept-Ranges", "none");
            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
            // send the generated PDF
            Stream.WriteTo(Response.OutputStream);
            Stream.Close();
            Response.Flush();
            // Response.End();
        }
        catch (pdfcrowd.Error why)
        {
            //Response.Write(why.ToString());
        }
    }
    protected void rptPayment_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string InvoicePaymentID = e.CommandArgument.ToString();
        if (e.CommandName.ToLower() == "delete")
        {

            ModalPopupExtenderDelete.Show();
            SttblInvoicePayments st = ClstblInvoicePayments.tblInvoicePayments_SelectByInvoicePaymentID(InvoicePaymentID);
            hdndelete.Value = InvoicePaymentID;
        }
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        ModalPopupExtenderInvPay.Hide();
    }
    public int GetControlIndex(String controlID)
    {
        Regex regex = new Regex("([0-9.*])", RegexOptions.RightToLeft);
        Match match = regex.Match(controlID);
        return Convert.ToInt32(match.Value);
    }
    protected void btnInvPay_Click1(object sender, EventArgs e)
    {
        MaxAttribute = 1;
        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            BindProjectInvoice(Request.QueryString["proid"]);
        }
        else
        {
            BindProjectInvoice(HiddenField1.Value);
        }
        ModalPopupExtenderInvPay.Show();
        //Response.Write(ModalPopupExtenderInvPay);
        //Response.End();
    }
    protected void btnPrintReceipt_Click1(object sender, EventArgs e)
    {

    }
    protected void ibtnCancelActive_Click1(object sender, EventArgs e)
    {

    }

    protected void lnkdelete_Click(object sender, EventArgs e)
    {
        string id = hdndelete.Value.ToString();
        bool sucess1 = ClstblCustInfo.tblCustInfo_Delete(id);
        //Response.Write("id"+id);
        SttblInvoicePayments st = ClstblInvoicePayments.tblInvoicePayments_SelectByInvoicePaymentID(id);
        string ProjectID = st.ProjectID;
        if (st.Refund == "True")
        {
            DataTable dtStatus = ClstblProjects.tblTrackProjStatus_ForRefund(ProjectID);
            ClstblInvoicePayments.tblProjects_UpdateRefund(HiddenField1.Value, dtStatus.Rows[0]["ProjectStatusID"].ToString(), "", "", "");

        }
        bool suc = ClstblInvoicePayments.tblInvoicePayments_Delete(id);
        if (suc)
        {
            SttblProjects stPro = ClstblProjects.tblProjects_SelectByProjectID(ProjectID);

            DataTable dtCount = ClstblInvoicePayments.tblInvoicePayments_CountTotal(ProjectID);
            if (dtCount.Rows.Count > 0)
            {
                try
                {
                    if (Convert.ToDecimal(dtCount.Rows[0]["InvoicePayTotal"].ToString()) < Convert.ToDecimal(stPro.TotalQuotePrice))
                    {
                        bool susses = ClstblInvoicePayments.tblProjects_UpdateInvoiceStatusID(ProjectID, "1");
                    }
                }
                catch { }
            }
        }
        AddmoreAttribute();
        bindrepeater();

        if (!string.IsNullOrEmpty(Request.QueryString["proid"]))
        {
            BindProjectInvoice(Request.QueryString["proid"]);
        }
        else
        {
            BindProjectInvoice(HiddenField1.Value);
        }
        ModalPopupExtenderInvPay.Show();
    }

    protected void ddlEft_SelectedIndexChanged(object sender, EventArgs e)
    {
        ModalPopupExtenderInvPay.Show();
    }
}

