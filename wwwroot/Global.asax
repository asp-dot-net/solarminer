<%@ Application Language="C#" %>
<%@ Import Namespace="Qvalent.PayWay" %>

<script RunAt="server">

    void Application_Start(object sender, EventArgs e)
    {
        //String initParams =
        //    @"certificateFile=E:\arisesolar\wwwroot\payway\ccapi.q0" + "&" +
        //    @"logDirectory=E:\arisesolar\wwwroot\payway";

        //PayWayAPI payWayAPI = new PayWayAPI();
        //payWayAPI.Initialise(initParams);
        //Application.Add("PayWayAPI", payWayAPI);
    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown

    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs

        // Transfer the user to the appropriate custom error page
        //HttpException lastErrorWrapper =
        //    Server.GetLastError() as HttpException;

        //if (lastErrorWrapper.GetHttpCode() == 404)
        //{           
        //    Server.Transfer("~/Error/Error404.aspx");
        //}
        //else
        //{
        //    Server.Transfer("~/Error/Error404.aspx");
        //}
    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started
    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
    }

    //void Application_AcquireRequestState(object sender, EventArgs e)
    //{
    //    //HttpContext context = HttpContext.Current;
    //    //// CheckSession() inlined
    //    //if (context.Session["LoggedIn"] != "true")
    //    //{
    //    //  context.Response.Redirect("default.aspx");
    //    //}
    //}

</script>
